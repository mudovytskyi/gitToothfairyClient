package toothfairy
{
	public final class LoggedUserType 
	{ 
		public static const DOCTOR:String = "doctor"; 
		public static const ASSISTANT:String = "assistant"; 
		public static const ADMINISTRATOR:String = "administrator"; 
	}
}