package toothfairy
{
	public class LicenseInfo
	{
		public static var _HARDWARE_KEY:String = "-";//hardware key
		public static var _HARDWARE_IP:String = "-";//hardware ip
		public static var _DATABASE_VERSION:String = "-";
		public static var _PHP_VERSION:String = "-";
		public static var _CLIENT_ID:String = "-";
		public static var _LOCATION:String = "uk_UA";
	}
}