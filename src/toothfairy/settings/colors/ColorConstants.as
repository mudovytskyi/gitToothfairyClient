package toothfairy.settings.colors
{
	public final class ColorConstants
	{
		//----------------------------------
		// Constants
		//----------------------------------
		public static const HARLEQUIN_COLOR:uint = 0x24C100;
		public static const BLUE_COLOR:uint = 0x0000FF;
		
		public static const MALACHITE_COLOR:uint = 0x33E667;
		public static const WHITE_COLOR:uint = 0xFFFFFF;
		public static const ALTO_COLOR:uint = 0xDBDBDB;
		public static const DUSTY_GRAY_COLOR:uint = 0x999999;

		public static const JAPANESE_LAUREL_COLOR:uint = 0x288D00;
		public static const RED_COLOR:uint = 0xE30000;
	}
}