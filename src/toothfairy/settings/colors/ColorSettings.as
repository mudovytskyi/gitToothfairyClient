package toothfairy.settings.colors
{
	import mx.collections.ArrayCollection;
	
	
	
	
	public class ColorSettings
	{
		public static function setDefault():void
		{
			
			_SearchBox_Color1 = 0xC4DF9B;
			
			_SearchBox_Color2 = 0xA3D39C;
			
			_SearchBox_Color3 = 0x259428;
			
			_GreenRect_Color1 = 0x3dc143;
			
			_GreenRect_Color2 = 0x47aa39;
			
			_GreenRect_Color3 = 0x8e8e8e;
			
			_BlueRect_Color1 = 0x7fc7ff;
			
			_BlueRect_Color2 = 0x42aaff;
			
			_BlueRect_Color3 = 0x8e8e8e;
			
			_LightGreenRect_Color1 = 0x9AFEA3;
			
			_LightGreenRect_Color2 = 0xA4E799;
			
			_LightGreenRect_Color3 = 0x8e8e8e;
			
			_LightGreenRect_Color4 = 0x4aab3c;
			
			_LightGreenRect_Color5 = 0x193C14;
			
			_ChiefLightGreenRect_Color1 = 0x9AFEA3;
			
			_ChiefLightGreenRect_Color2 = 0xA4E799;
			
			_ChiefLightGreenRect_Color3 = 0x8e8e8e;
			
			_ChiefLightGreenRect_Color4 = 0x4aab3c;
			
			_ChiefLightGreenRect_Color5 = 0x193C14;
			

		}
	
		
		
		
		// account mamangers start
		public static const ACCOUNT_MANAGER_GREEN:uint = 0x0a5400;
		public static const ACCOUNT_MANAGER_RED:uint = 0xd50000;
		public static const ACCOUNT_MANAGER_BLACK:uint = 0x000000;
		// account mamangers end
		
		public static const COLORPICKERCURSOR:String = "cursorColorPicker";
		
		public static var SearchBox_ColorSettings:Array=["SearchBox_Color1","SearchBox_Color2","SearchBox_Color3"];
		
		private static var _SearchBox_Color1:uint=0xC4DF9B;

		private static var _SearchBox_Color2:uint=0xA3D39C;
		
		private static var _SearchBox_Color3:uint=0x259428;


		[Bindable]
		public static function get SearchBox_Color3():uint
		{
			return _SearchBox_Color3;
		}

		public static function set SearchBox_Color3(value:uint):void
		{
			_SearchBox_Color3 = value;
		}

		[Bindable]
		public static function get SearchBox_Color2():uint
		{
			return _SearchBox_Color2;
		}

		public static function set SearchBox_Color2(value:uint):void
		{
			_SearchBox_Color2 = value;
		}

		[Bindable]
		public static function get SearchBox_Color1():uint
		{
			return _SearchBox_Color1;
		}

		public static function set SearchBox_Color1(value:uint):void
		{
			_SearchBox_Color1 = value;
		}
		
		public static var FarmDocumentsModule_ColorSettings:Array=["GreenRect_Color1","GreenRect_Color2","GreenRect_Color3"];
		
		private static var _GreenRect_Color1:uint=0x3dc143;
		
		private static var _GreenRect_Color2:uint=0x47aa39;
		
		private static var _GreenRect_Color3:uint=0x8e8e8e;
		
		[Bindable]
		public static function get GreenRect_Color3():uint
		{
			return _GreenRect_Color3;
		}
		
		public static function set GreenRect_Color3(value:uint):void
		{
			_GreenRect_Color3 = value;
		}
		
		[Bindable]
		public static function get GreenRect_Color2():uint
		{
			return _GreenRect_Color2;
		}
		
		public static function set GreenRect_Color2(value:uint):void
		{
			_GreenRect_Color2 = value;
		}
		
		[Bindable]
		public static function get GreenRect_Color1():uint
		{
			return _GreenRect_Color1;
		}
		
		public static function set GreenRect_Color1(value:uint):void
		{
			_GreenRect_Color1 = value;
		}

		public static var ManageFarmDocumentWindow_ColorSettings:Array=["BlueRect_Color1","BlueRect_Color2","BlueRect_Color3"];
		
		private static var _BlueRect_Color1:uint=0x7fc7ff;
		
		private static var _BlueRect_Color2:uint=0x42aaff;
		
		private static var _BlueRect_Color3:uint=0x8e8e8e;
		
		[Bindable]
		public static function get BlueRect_Color3():uint
		{
			return _BlueRect_Color3;
		}
		
		public static function set BlueRect_Color3(value:uint):void
		{
			_BlueRect_Color3 = value;
		}
		
		[Bindable]
		public static function get BlueRect_Color2():uint
		{
			return _BlueRect_Color2;
		}
		
		public static function set BlueRect_Color2(value:uint):void
		{
			_BlueRect_Color2 = value;
		}
		
		[Bindable]
		public static function get BlueRect_Color1():uint
		{
			return _BlueRect_Color1;
		}
		
		public static function set BlueRect_Color1(value:uint):void
		{
			_BlueRect_Color1 = value;
		}
		
		
		public static var WarehouseOLAPModule_ColorSettings:Array=["LightGreenRect_Color1","LightGreenRect_Color2","LightGreenRect_Color3","LightGreenRect_Color4","LightGreenRect_Color5"];
		public static var OlapFilterPanelSkin_ColorSettings:Array=["LightGreenRect_Color1","LightGreenRect_Color2","LightGreenRect_Color3","LightGreenRect_Color4","LightGreenRect_Color5"];

		private static var _LightGreenRect_Color1:uint=0x9AFEA3;
		
		private static var _LightGreenRect_Color2:uint=0xA4E799;
		
		private static var _LightGreenRect_Color3:uint=0x8e8e8e;
		
		private static var _LightGreenRect_Color4:uint=0x4aab3c;
		
		private static var _LightGreenRect_Color5:uint=0x193C14;

		[Bindable]
		public static function get LightGreenRect_Color5():uint
		{
			return _LightGreenRect_Color5;
		}

		public static function set LightGreenRect_Color5(value:uint):void
		{
			_LightGreenRect_Color5 = value;
		}

		[Bindable]	
		public static function get LightGreenRect_Color4():uint
		{
			return _LightGreenRect_Color4;
		}
		
		public static function set LightGreenRect_Color4(value:uint):void
		{
			_LightGreenRect_Color4 = value;
		}
		
		[Bindable]
		public static function get LightGreenRect_Color3():uint
		{
			return _LightGreenRect_Color3;
		}
		
		public static function set LightGreenRect_Color3(value:uint):void
		{
			_LightGreenRect_Color3 = value;
		}
		
		[Bindable]
		public static function get LightGreenRect_Color2():uint
		{
			return _LightGreenRect_Color2;
		}
		
		public static function set LightGreenRect_Color2(value:uint):void
		{
			_LightGreenRect_Color2 = value;
		}
		
		[Bindable]
		public static function get LightGreenRect_Color1():uint
		{
			return _LightGreenRect_Color1;
		}
		
		public static function set LightGreenRect_Color1(value:uint):void
		{
			_LightGreenRect_Color1 = value;
		}
		
		public static var ChiefOLAPModule_ColorSettings:Array=["ChiefLightGreenRect_Color1","ChiefLightGreenRect_Color2","ChiefLightGreenRect_Color3","ChiefLightGreenRect_Color4","ChiefLightGreenRect_Color5"];
		public static var ChiefOlapFilterPanelSkin_ColorSettings:Array=["ChiefLightGreenRect_Color1","ChiefLightGreenRect_Color2","ChiefLightGreenRect_Color3","ChiefLightGreenRect_Color4","ChiefLightGreenRect_Color5"];
		
		private static var _ChiefLightGreenRect_Color1:uint=0x9AFEA3;
		
		private static var _ChiefLightGreenRect_Color2:uint=0xA4E799;
		
		private static var _ChiefLightGreenRect_Color3:uint=0x8e8e8e;
		
		private static var _ChiefLightGreenRect_Color4:uint=0x4aab3c;
		
		private static var _ChiefLightGreenRect_Color5:uint=0x193C14;
		
		[Bindable]
		public static function get ChiefLightGreenRect_Color5():uint
		{
			return _ChiefLightGreenRect_Color5;
		}
		
		public static function set ChiefLightGreenRect_Color5(value:uint):void
		{
			_ChiefLightGreenRect_Color5 = value;
		}
		
		[Bindable]	
		public static function get ChiefLightGreenRect_Color4():uint
		{
			return _ChiefLightGreenRect_Color4;
		}
		
		public static function set ChiefLightGreenRect_Color4(value:uint):void
		{
			_ChiefLightGreenRect_Color4 = value;
		}
		
		[Bindable]
		public static function get ChiefLightGreenRect_Color3():uint
		{
			return _ChiefLightGreenRect_Color3;
		}
		
		public static function set ChiefLightGreenRect_Color3(value:uint):void
		{
			_ChiefLightGreenRect_Color3 = value;
		}
		
		[Bindable]
		public static function get ChiefLightGreenRect_Color2():uint
		{
			return _ChiefLightGreenRect_Color2;
		}
		
		public static function set ChiefLightGreenRect_Color2(value:uint):void
		{
			_ChiefLightGreenRect_Color2 = value;
		}
		
		[Bindable]
		public static function get ChiefLightGreenRect_Color1():uint
		{
			return _ChiefLightGreenRect_Color1;
		}
		
		public static function set ChiefLightGreenRect_Color1(value:uint):void
		{
			_ChiefLightGreenRect_Color1 = value;
		}
		
		
		// Налаштування кольорів подій у календарі гугл для кімнат під час синхронізації
		private static var _roomGoogleEventColors:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public static function get roomGoogleEventColors():ArrayCollection
		{
			return _roomGoogleEventColors;
		}
		
		public static function set roomGoogleEventColors(value:ArrayCollection):void
		{
			_roomGoogleEventColors = value;
		}
		
		
		// Налаштування кольорів календаря гугл
		private static var _settingsGoogleCalendarColors:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public static function get settingsGoogleCalendarColors():ArrayCollection
		{
			return _settingsGoogleCalendarColors;
		}
		
		public static function set settingsGoogleCalendarColors(value:ArrayCollection):void
		{
			_settingsGoogleCalendarColors = value;
		}

	}
}