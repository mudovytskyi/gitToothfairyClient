package toothfairy.settings
{
	import flash.data.EncryptedLocalStore;
	import flash.net.SharedObject;
	import flash.utils.ByteArray;
	
	import modules.DirectorModules.modules.olap.ChiefOLAPModule;
	import modules.WarehouseModule.WarehouseOLAPModule;
	
	import rko.unisystem.T400ME;
	
	import toothfairy.configurations.Setting;

	public class ToothFairyDefaultSettingsInitializer
	{
		public static function SetDefaultSettings():String
		{
			if(EncryptedLocalStore.getItem("tf.vikisoft.login") == null)
			{
				var defaultlogin:String = "admin";
				var byteslogin:ByteArray = new ByteArray();
				byteslogin.writeUTFBytes(defaultlogin);
				EncryptedLocalStore.setItem("tf.vikisoft.login", byteslogin);
			}
			if(EncryptedLocalStore.getItem("tf.vikisoft.pass") == null)
			{
				var defaultpassword:String = "admin";
				var bytespassword:ByteArray = new ByteArray();
				bytespassword.writeUTFBytes(defaultpassword);
				EncryptedLocalStore.setItem("tf.vikisoft.pass", bytespassword);
			}

			if (ClientSettings.locale == null)
			{		
				ClientSettings.locale = "uk_UA";
			}
			
			if(SharedObject.getLocal("toothfairy").data["ChiefOLAPColumns"] == null)
			{
				SharedObject.getLocal("toothfairy").data["ChiefOLAPColumns"] = ChiefOLAPModule.defaultOLAPColumns;
			}
			if(SharedObject.getLocal("toothfairy").data["ChiefOLAPGroupFields"] == null)
			{
				SharedObject.getLocal("toothfairy").data["ChiefOLAPGroupFields"] = ChiefOLAPModule.defaultOLAPGroupFields;
			}
			
			if(SharedObject.getLocal("toothfairy").data["OLAPColumns"] == null)
			{
				SharedObject.getLocal("toothfairy").data["OLAPColumns"] = WarehouseOLAPModule.defaultOLAPColumns;
			}
			if(SharedObject.getLocal("toothfairy").data["OLAPGroupFields"] == null)
			{
				SharedObject.getLocal("toothfairy").data["OLAPGroupFields"] = WarehouseOLAPModule.defaultOLAPGroupFields;
			}
			
			// TO-DO Registrator start
			if(SharedObject.getLocal("toothfairy").data["isAccountRegistrator"] == null)
			{
				SharedObject.getLocal("toothfairy").data["isAccountRegistrator"] = false;
				ClientSettings.isAccountRegistrator = false;
			}
			else
			{
				ClientSettings.isAccountRegistrator = SharedObject.getLocal("toothfairy").data["isAccountRegistrator"];
			}
			if(SharedObject.getLocal("toothfairy").data["isAccountRegistratorDiscount"] == null)
			{
				SharedObject.getLocal("toothfairy").data["isAccountRegistratorDiscount"] = false;
				ClientSettings.isAccountRegistrator_Discount = false;
			}
			else
			{
				ClientSettings.isAccountRegistrator_Discount = SharedObject.getLocal("toothfairy").data["isAccountRegistratorDiscount"]; 
			}
			
			if(SharedObject.getLocal("toothfairy").data["isAccountRegistratorCopyReceipt"] == null)
			{
				SharedObject.getLocal("toothfairy").data["isAccountRegistratorCopyReceipt"] = true;
				ClientSettings.isAccountRegistrator_CopyReceipt = true;
			}
			else
			{
				ClientSettings.isAccountRegistrator_CopyReceipt = SharedObject.getLocal("toothfairy").data["isAccountRegistratorCopyReceipt"]; 
			}
			
			
			
			if(SharedObject.getLocal("toothfairy").data["isAccountRegistratorCOM"] == null)
			{
				SharedObject.getLocal("toothfairy").data["isAccountRegistratorCOM"] = "3";
				T400ME.PORT_NUMBER = "3";
			}
			else
			{
				T400ME.PORT_NUMBER = SharedObject.getLocal("toothfairy").data["isAccountRegistratorCOM"];
			}
			
			if(SharedObject.getLocal("toothfairy").data["isOddMoneyRoundReceipt"] == null)
			{
				SharedObject.getLocal("toothfairy").data["isOddMoneyRoundReceipt"] = false;
				ClientSettings.isAccountRegistrator_isOddMoneyRoundReceipt = false;
			}
			else
			{
				ClientSettings.isAccountRegistrator_isOddMoneyRoundReceipt = SharedObject.getLocal("toothfairy").data["isOddMoneyRoundReceipt"]; 
			}
			
			if(SharedObject.getLocal("toothfairy").data["isAutoPayReceipt"] == null)
			{
				SharedObject.getLocal("toothfairy").data["isAutoPayReceipt"] = false;
				ClientSettings.isAccountRegistrator_isAutoPayReceipt = false;
			}
			else
			{
				ClientSettings.isAccountRegistrator_isAutoPayReceipt = SharedObject.getLocal("toothfairy").data["isAutoPayReceipt"]; 
			}
			// TO-Do Registrator end
			
			// server ip adresses
			if (SharedObject.getLocal("toothfairy").data["serverDataProvider"] == null)
			{
				var currentIP:String = SharedObject.getLocal("toothfairy").data["serverip"];
				if(currentIP == "localhost:81")
				{
					SharedObject.getLocal("toothfairy").data["serverDataProvider"] = [{id:0, isActive:true, serverIp:"localhost:81"},
						{id:1, isActive:false, serverIp:"localhost"}];
				}
				else if(currentIP == "localhost")
				{
					SharedObject.getLocal("toothfairy").data["serverDataProvider"] = [{id:0, isActive:false, serverIp:"localhost:81"},
						{id:1, isActive:true, serverIp:"localhost"}];
				}
				else
				{
					SharedObject.getLocal("toothfairy").data["serverDataProvider"] = [{id:0, isActive:false, serverIp:"localhost:81"},
						{id:1, isActive:false, serverIp:"localhost"},
						{id:2, isActive:true, serverIp:currentIP}];
				}
			}
			
			Setting.locale = ClientSettings.locale;
			return ClientSettings.locale;
			
		}
	}
}