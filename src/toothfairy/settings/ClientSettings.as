package toothfairy.settings
{
	import mx.resources.ResourceManager;
	
	import toothfairy.configurations.ConfigurationSettings;
	import toothfairy.configurations.ConstantSettings;
	import toothfairy.configurations.DateFormatsTypes;
	import toothfairy.configurations.Setting;
	import mx.collections.ArrayCollection;
	
	
	public class ClientSettings
	{
		//Psorolite ***START***
		[Bindable]	
		public static var Psorolite_EnableSeconds:Boolean = false;
		[Bindable]
		public static var Psorolite_PortNumber:String = '';
		//Psorolite ***END***
		
		//Orthanc *** STRAT ***
		//TODO не добавлено в настройки, автоматическая проверка и загрузка с Ортанг сервера при входе в файловый менеджер
		[Bindable]
		public static var Orthanc_isAutoUpdateOnSFLoad:Boolean = true;
		//Orthanc *** END ***
		
		
		/*[Bindable]
		public static var PRICE1_LABEL:String;
		[Bindable]
		public static var PRICE2_LABEL:String;
		[Bindable]
		public static var PRICE3_LABEL:String;*/
		
		private static var _PRICE1_LABEL:String;
		public static function get PRICE1_LABEL():String
		{
			return _PRICE1_LABEL;
		}
		private static var _PRICE2_LABEL:String;
		public static function get PRICE2_LABEL():String
		{
			return _PRICE2_LABEL;
		}
		private static var _PRICE3_LABEL:String;
		public static function get PRICE3_LABEL():String
		{
			return _PRICE3_LABEL;
		}
		
		
		/*[Bindable]
		public static var PRICE1_ENABLED:Boolean;
		[Bindable]
		public static var PRICE2_ENABLED:Boolean;
		[Bindable]
		public static var PRICE3_ENABLED:Boolean;*/
		
		private static var _PRICE2_ENABLED:Boolean;
		public static function get PRICE2_ENABLED():Boolean
		{
			return _PRICE2_ENABLED;
		}
		private static var _PRICE3_ENABLED:Boolean;
		public static function get PRICE3_ENABLED():Boolean
		{
			return _PRICE3_ENABLED;
		}
		
		
		private static var _PRICE1_SETTINGS:String;
		public static function get PRICE1_SETTINGS():String
		{
			return _PRICE1_SETTINGS;
		}
		public static function set PRICE1_SETTINGS(value:String):void
		{
			_PRICE1_SETTINGS = value;
			var settings_array:Array = _PRICE1_SETTINGS.split("|");
			_PRICE1_LABEL = settings_array[0];
		}
		
		
		private static var _PRICE2_SETTINGS:String;
		public static function get PRICE2_SETTINGS():String
		{
			return _PRICE2_SETTINGS;
		}
		public static function set PRICE2_SETTINGS(value:String):void
		{
			_PRICE2_SETTINGS = value;
			var settings_array:Array = _PRICE2_SETTINGS.split("|");
			_PRICE2_LABEL = settings_array[0];
			_PRICE2_ENABLED = (settings_array[1] == "1" ? true : false);
		}
		
		private static var _PRICE3_SETTINGS:String;
		public static function get PRICE3_SETTINGS():String
		{
			return _PRICE3_SETTINGS;
		}
		public static function set PRICE3_SETTINGS(value:String):void
		{
			_PRICE3_SETTINGS = value;
			var settings_array:Array = _PRICE3_SETTINGS.split("|");
			_PRICE3_LABEL = settings_array[0];
			_PRICE3_ENABLED = (settings_array[1] == "1" ? true : false);
		}
		
		private static var _GOOGLE_CALENDAR_ACCOUNT_ID_AUTHED:String;
		public static function get GOOGLE_CALENDAR_ACCOUNT_ID_AUTHED():String {
			return _GOOGLE_CALENDAR_ACCOUNT_ID_AUTHED;
		}
		public static function set GOOGLE_CALENDAR_ACCOUNT_ID_AUTHED(value:String):void {
			_GOOGLE_CALENDAR_ACCOUNT_ID_AUTHED = value;
		}
		
		private static var _GOOGLE_CALENDAR_ID_SYNCED:String;
		public static function get GOOGLE_CALENDAR_ID_SYNCED():String {
			return _GOOGLE_CALENDAR_ID_SYNCED;
		}
		public static function set GOOGLE_CALENDAR_ID_SYNCED(value:String):void {
			_GOOGLE_CALENDAR_ID_SYNCED = value;
		}
		
		
		private static var _GOOGLE_CALENDAR_TIME_ZONE:String;
		public static function get GOOGLE_CALENDAR_TIME_ZONE():String {
			return _GOOGLE_CALENDAR_TIME_ZONE;
		}
		public static function set GOOGLE_CALENDAR_TIME_ZONE(value:String):void {
			_GOOGLE_CALENDAR_TIME_ZONE = value;
		}
		
		
		
		public static var SCROLL_SPEED:Number = 20;
		
		public static function setDefault():void
		{	
			
			trace('Default Settings');
			
			if (ClientSettings.locale == null)
			{		
				ClientSettings.locale = "uk_UA";
			}
			
			ClientSettings.enableFixingTimeInTasks=true;//режим с большим количеством пациентов
			
			ClientSettings.lazyLoadingOnNumbersSymbolInTasks=2;//поиск начиная с введенных первых символов
			
			ClientSettings.alphabetPanelInPatCards=true;//режим с большим количеством пациентов
			
			ClientSettings.lazyLoadingOnNumbersSymbolInPatCards=3;//поиск начиная с введенных первых символов
			
			ClientSettings.toothfairyDateFormat= "DD.MM.YYYY";
			
			ClientSettings.toothfairyDateTypeFormat=1;
			
			ClientSettings.toothfairyDateSeporatorFormat= ".";
			
			ClientSettings.toothfairyDocsFormat="docx";
			
			ClientSettings.isClearToothFairyDocuments=true;
			
			ClientSettings.isDefaultCourseNamesOfProcedure=false;
			
			ClientSettings.exitWithoutPromt=false;
			
			ClientSettings.isLockMainMenuPanel=false;
			
			ClientSettings.lazyLoadingGeneral=2;
			
			ClientSettings.f043WindowScale=1;
			
			ClientSettings.calendarWindowScale=1;
			
			ClientSettings.treatmentWindowScale=1;
			
			ClientSettings.globalScale=1;
			
			ClientSettings.autoSaveComplaintsDiagnosisDiseasehistory = false;
			
			ClientSettings.toothfairyTablesFormat="xlsx";
			
			ClientSettings.showSubdivision= false;
			
			ClientSettings.showPriceLabel= false;
			
			ClientSettings.AutoReloadEnabled= true;//автообновление
			
			ClientSettings.isFarmsChangeCompleteEnabled = false;
			
			//AccountModule
			ClientSettings.includePDVInInvoice= false;
			ClientSettings.receiptPageCount=1;
			ClientSettings.receiptIsPrintClinicName = true;		
			ClientSettings.receiptIsPrintClinicLogo = true;	
			ClientSettings.receiptIsPrintMaxWidth = 300;
			ClientSettings.isRecieptProcedurFullNames = false;
			ClientSettings.invoiceShowAfterCreateFlag= true;
			ClientSettings.isAllBalance_Boolean= true;
			ClientSettings.isEnableAttention= false;
			
			//MaterialsModule
			ClientSettings.materialShowUnderMinimumQuantityMessageOnStart= true;
			ClientSettings.materialCheckForDoctorsWithNegativeFarmBalanceOnStart= true;
			
			
			//CalendarModule
			// инициализация настроек календаря по умолчанию
			//набор цветов тасков для обычных пациентов
			ClientSettings.CalendarModule_UnforewarnedColor=0x0018ff;//не предупрежденный
			ClientSettings.CalendarModule_WarnColor=0xa903ff;//предупрежденный
			ClientSettings.CalendarModule_AdditionalWarnColor=0xffff99;//Додаткове попередження
			ClientSettings.CalendarModule_UncomeColor=0xff0000;//Не прийшов
			ClientSettings.CalendarModule_ComeColor=0x555555;//Прийшов
			ClientSettings.CalendarModule_NowColor=0x58cf00;//текущая задача
			//набор цветов тасков для страовых пациентов
			ClientSettings.CalendarModule_InsuranceUnforewarnedColor=0x00ff96;//не предупрежденный
			ClientSettings.CalendarModule_InsuranceWarnColor=0x00ff96;//предупрежденный
			ClientSettings.CalendarModule_InsuranceAdditionalWarnColor=0xff00f6;//Додаткове попередження
			ClientSettings.CalendarModule_InsuranceUncomeColor=0xff0000;//Не прийшов
			ClientSettings.CalendarModule_InsuranceComeColor=0x603e3e;//Прийшов
			ClientSettings.CalendarModule_InsuranceNowColor=0x00cf83;//текущая задача
			
			ClientSettings.CalendarModule_fromSiteColor=0x00c6ff;//задача добавленная с сайта
			ClientSettings.CalendarModule_fromSiteAutosubstitutionColor=0xFE7B05;//задача добавленная с сайта c автоподменой
			ClientSettings.CalendarModule_NonWorkColor=0xceefdb;//не рабочее время
			ClientSettings.CalendarModule_TextItemColor=0x000000;//цвет текста в тасках
			
			ClientSettings.CalendarModule_WorkTime= [
				{days: [1], rangeStart: '9:00', rangeEnd: '18:00'}, 
				{days: [2], rangeStart: '9:00', rangeEnd: '18:00'},  
				{days: [3], rangeStart: '9:00', rangeEnd: '18:00'}, 
				{days: [4], rangeStart: '9:00', rangeEnd: '18:00'}, 
				{days: [5], rangeStart: '9:00', rangeEnd: '18:00'}];//график работы клиники
			ClientSettings.CalendarModule_NonWorkDays= [0,6];//выходные дни клиники массив, формат [0,1,2,3,4,5,6,0]
			
			ClientSettings.CalendarModule_StartShowTime="8:00";
			ClientSettings.CalendarModule_EndShowTime="18:00";
			
			ClientSettings.CalendarModule_BubbleHelp={cabinet:true,
				doctor:true,
				fio:true,
				mobilephone:true,
				phone:true,
				remarks:true,
				workdescription:true};//настройка всплывающей подсказки объект
			
			ClientSettings.CalendarModule_DobReminder= true;//напоминания о днях рождениях
			//ClientSettings.CalendarModule_AutoReloadValue=5;
			ClientSettings.CalendarModule_FullFIO= true;//вывод полного ФИО
			ClientSettings.CalendarModule_LegendEnabled= true;//показывать легенду
			ClientSettings.CalendarModule_DaysCount=1;//количество отображаемых дней в календаре [1-7]
			ClientSettings.CalendarModule_CalendarRenderer_ToolTip_byDate= true;//как показывать график в органайзере Датам(true)/Врачам(false)
			ClientSettings.CalendarModule_CalendarRenderer_calendarWorkTimesByDoctors= true;//Нерабочее время по врачам либо по клинике
			ClientSettings.CalendarModule_CalendarRenderer_noticedWindow= true;//отображение окна предупреждений
			// инициализация настроек календаря по умолчанию
			ClientSettings.CalendarModule_Wizard_minDiapMinutes=30;//Минимальный диапазон, учитываемый в Визарде (в минутах)
			// инициализация массива смен
			ClientSettings.CalendarModule_changesDataProvider= [{uid:"-1", startTime:"-1", endTime:"-1"}];
			
			ClientSettings.CalendarModule_visitedByDefault= false;
			
			ClientSettings.CalendarModule_useWizardForDispanser= true;
			
			//PatientCardModule
			ClientSettings.cardBeforNumber="";
			ClientSettings.cardAfterNumber="_";
			ClientSettings.cardAfterNumberDateFormat="YYYY";
			ClientSettings.cardAfterNumberDateFlag= true;
			ClientSettings.channelsZoomerFlag= true;
			ClientSettings.patientscardsFlag= true;
			ClientSettings.patientsCardsNumbersFlag= true;
			
			ClientSettings.printTestPage= true;
			ClientSettings.includePriceInReport= false;
			ClientSettings.includeMaterialsInReport= false;
			ClientSettings.includeDoctorSignatureInReport= false;
			ClientSettings.includeNoncompleteProcedures043= false;
			ClientSettings.includeShifrProcedures043Flag= false;
			ClientSettings.includeCountProcedures043Flag= true;
			ClientSettings.autoSaveComplaintsDiagnosisDiseasehistory= false;
			
			ClientSettings.isAdditionalFilters_Boolean= false;
			ClientSettings.isNoGroupingTretmentCoursePrice= false;
			ClientSettings.openTreatmentCourseAfterDiagnos2Save= false;
			ClientSettings.pagingTreatmentCourse = false;
			
			ClientSettings.patientCardDefaultCountry="Україна";
			ClientSettings.patientCardDefaultState="";
			ClientSettings.patientCardDefaultRegion="";
			ClientSettings.patientCardDefaultCity="";
			
			// Налаштування секції первинного запису пацієнта на прийом
			ClientSettings.patientPrimaryCardDefaultRequieredFields = "primlname,primfname";
			ClientSettings.patientPrimaryCardAutoFocusedField = "primlname";
			ClientSettings.patientPrimaryCardAutoFocusEnabled = true;
			
			ClientSettings.patientCardModulePageSize=20;
			ClientSettings.patientCardModuleByPages= true;
			
			//AWDoctorModule
			ClientSettings.AWDoctorModuletaskTimeMinutes = 30;
			ClientSettings.AWDoctorModulestartWorkWeekend = "08:00:00";
			ClientSettings.AWDoctorModuleendWorkWeekend = "23:00:00";
			
			//SMSModule
			ClientSettings.SMSModuleDefaultSendingInTasks= false;//SMS TO-DO
			ClientSettings.SMSModuleDefaultSendingInDispanser= false;
			
			ClientSettings.unvisibleRooms =[];
			
			ClientSettings.isColorChangeEnable= true;
			
			ClientSettings.isSQLiteEnable= true;
			
			
			//Admin
			ClientSettings.isVocabulariesTab = true;
			ClientSettings.isTestConstructorEnabled = true;
			ClientSettings.isAdministratorTab = true;
			ClientSettings.isEnableHRModules = true;
			ClientSettings.isHRModuleEnabled = true;
			ClientSettings.isOnlyOwnerTasksEdit = false;
			ClientSettings.isEnableDiscountCardModule = true;
			ClientSettings.isEnableCertificateModule = true;
			ClientSettings.isEnableDiscountCardSumChange = false;
			
			ClientSettings.isEnableAllPatientsARM = false;
			ClientSettings.isCalendarTab = false;
			ClientSettings.isConstructorTab = true;
			ClientSettings.isShowContactData = true;
			ClientSettings.isWizardChangeDoctorEnabled = false;
			
			ClientSettings.isARMVocabularies = true;
			ClientSettings.isARMTreatbalanceHistorySettings = true;
			ClientSettings.isShowFinanceData = true;
			
			ClientSettings.isARMTasksHistorySettings = true;
			ClientSettings.isARMDoctorsHistorySettings = true;
			ClientSettings.isTreatmentCourseModule = true;
			ClientSettings.isToothMapModule = true;
			ClientSettings.isTestModule = true;
			ClientSettings.isExport043Module = true;
			ClientSettings.isAddPatientEnable = true;
			ClientSettings.isAmbulatoryModule = true;
			ClientSettings.isEnableCardNumberChange = true;
			ClientSettings.isEnableTestChange = true;
			ClientSettings.isLeadDoctorEnabled = true;
			ClientSettings.isQuickTreatmentCourse = false;
			
			ClientSettings.isReportsInTreatment = true;
			ClientSettings.isEnablePriceChangeInNewTreatment = false;
			ClientSettings.isEnableTimeDateChangeInNewTreatment = false;
			ClientSettings.isConfirmTreatmentProceduresEnabled = false;
			ClientSettings.isEnableMoneyInTreatmentCourse = true;
			ClientSettings.isAfterExamsEnabled = true;
			
			ClientSettings.isAccountFlowDataChangeEnabled = false;
			ClientSettings.isAccountInvoiceDateNumberChangabel = false;
			ClientSettings.isPriceInInvoiceCreationEditable = false;
			ClientSettings.isEnableDiscountChangeInAccountModule = true;
			ClientSettings.isSalaryInAccountEnabled = false;
			ClientSettings.isOnlyOwnerInvoiceAndFlowEdit = false;
			ClientSettings.isDiscountChangeEnabled=true;
			
			ClientSettings.isShowOnlyCurrentSubdivision = false;
			ClientSettings.isShowOnlyCurrentSubdivisionAccount = false;
			
			ClientSettings.isHRDataEnabled = true;
			ClientSettings.isEnableSalaryManager = false;
			ClientSettings.isPriceListSalarySettings = false;
			ClientSettings.isControlExpensesInPriceList = false;
			ClientSettings.isWizardPrimaryEnabled = true;
			ClientSettings.isEnableMassMailingInAdministratorPanel = false;
			ClientSettings.diagnos2_isToothSort = true;
			
			ClientSettings.isEnableCertificateSumChange=false;
			ClientSettings.isNegativeDiscount=false;
			ClientSettings.isPricePrintEnable=true;
			ClientSettings.isPriceEditEnable=true;
			ClientSettings.isAccountShowOnlyUserOperation=false;
			ClientSettings.isAccoundLendDebPrevDate=true;
			
		}
		
		[Bindable]
		public static var isFarmsChangeCompleteEnabled:Boolean = false;
		
		[Bindable]
		public static var isRememberTreatmentPos:Boolean = true;
		
		
		[Bindable]
		public static var diagnos2_isToothSort:Boolean = true;
		
		
		[Bindable]
		public static var toothmap_isTopAdditionals:Boolean = true;
		
		// Account registrator
		[Bindable]
		public static var isAccountRegistrator:Boolean = false;
		[Bindable]
		public static var isAccountRegistrator_ALLOWED:Boolean = false;
		[Bindable]
		public static var isAccountRegistrator_Discount:Boolean = false;
		[Bindable]
		public static var isAccountRegistrator_CopyReceipt:Boolean = true;
		[Bindable]
		public static var isAccountRegistrator_isOddMoneyRoundReceipt:Boolean = false;
		[Bindable]
		public static var isAccountRegistrator_isAutoPayReceipt:Boolean = false;
		
		
		//[Bindable]
		//public static var isAccountCashierRegged:Boolean = false;
		//[Bindable]
		//public static var isAccount0printed:Boolean = false;
		// Account registrator
		
		//main
		
		private static var _locale:String=null;
		
		[Bindable]
		public static var enableFixingTimeInTasks:Boolean=false;//режим с фиксацией времени прихода и ухода
		
		public static var lazyLoadingOnNumbersSymbolInTasks:int=2;//поиск начиная с введенных первых символов
		
		[Bindable]
		public static var alphabetPanelInPatCards:Boolean=true;//режим с алфавитной панелью в пациентах
		
		public static var lazyLoadingOnNumbersSymbolInPatCards:int=3;//поиск начиная с введенных первых символов
		
		private static var _toothfairyDateFormat:String= "DD.MM.YYYY";
		
		private static var _toothfairyDateTypeFormat:int=1;
		
		private static var _toothfairyDateSeporatorFormat:String= ".";
		
		public static var toothfairyDocsFormat:String="docx";
		
		public static var isClearToothFairyDocuments:Boolean=true;
		
		public static var isDefaultCourseNamesOfProcedure:Boolean=false;
		
		public static var exitWithoutPromt:Boolean=false;
		
		public static var isLockMainMenuPanel:Boolean=false;
		
		public static var lazyLoadingGeneral:Number=2;
		
		private static var _f043WindowScale:Number=1;
		
		[Bindable]
		public static var autoSaveComplaintsDiagnosisDiseasehistory:Boolean = false
		
		private static var _calendarWindowScale:Number=1;
		
		private static var _treatmentWindowScale:Number=1;
		
		private static var _globalScale:Number=1;
		
		public static var toothfairyTablesFormat:String="xlsx";
		
		private static var _showSubdivision:Boolean=false;

		[Bindable]
		public static var showPriceLabel:Boolean=false;
		
		//SYNC Settings
		public static var isSyncEnabled:Boolean = true;
		
		//AccountModule
		public static var includePDVInInvoice:Boolean = false;
		public static var receiptPageCount:int = 1;
		public static var receiptIsPrintClinicName:Boolean = true;
		public static var receiptIsPrintClinicLogo:Boolean = true;
		[Bindable]
		public static var receiptIsPrintMaxWidth:int = 300;
		public static var isRecieptProcedurFullNames:Boolean = false;
		public static var invoiceShowAfterCreateFlag:Boolean = true;
		public static var isAllBalance_Boolean:Boolean = false;
		public static var isEnableAttention:Boolean = false;
		public static var isNullProceduresInCheck:Boolean = true;
		
		//MaterialsModule
		public static var materialShowUnderMinimumQuantityMessageOnStart:Boolean = true;
		public static var materialCheckForDoctorsWithNegativeFarmBalanceOnStart:Boolean = true;
		
		
		//CalendarModule
		// инициализация настроек календаря по умолчанию
		//набор цветов тасков для обычных пациентов
		public static var CalendarModule_UnforewarnedColor:uint=0x0018ff;//не предупрежденный
		public static var CalendarModule_WarnColor:uint=0xa903ff;//предупрежденный
		public static var CalendarModule_AdditionalWarnColor:uint=0xffff99;//Додаткове попередження
		public static var CalendarModule_UncomeColor:uint=0xff0000;//Не прийшов
		public static var CalendarModule_ComeColor:uint=0x555555;//Прийшов
		public static var CalendarModule_NowColor:uint=0x58cf00;//текущая задача
		//набор цветов тасков для страовых пациентов
		public static var CalendarModule_InsuranceUnforewarnedColor:uint=0x00ff96;//не предупрежденный
		public static var CalendarModule_InsuranceWarnColor:uint=0x00ff96;//предупрежденный
		public static var CalendarModule_InsuranceAdditionalWarnColor:uint=0xff00f6;//Додаткове попередження
		public static var CalendarModule_InsuranceUncomeColor:uint=0xff0000;//Не прийшов
		public static var CalendarModule_InsuranceComeColor:uint=0x603e3e;//Прийшов
		public static var CalendarModule_InsuranceNowColor:uint=0x00cf83;//текущая задача
		
		public static var CalendarModule_fromSiteColor:uint=0x00c6ff;//задача добавленная с сайта
		public static var CalendarModule_fromSiteAutosubstitutionColor:uint=0xFE7B05;//задача добавленная с сайта c автоподменой
		public static var CalendarModule_NonWorkColor:uint=0xceefdb;//не рабочее время
		public static var CalendarModule_TextItemColor:uint=0x000000;//цвет текста в тасках
		public static var CalendarModule_showGroupsInSheduleTasks:Boolean=true;//отображать группы при записи пациента
		
		
		
		public static var CalendarModule_WorkTime:Array= [
			{days: [1], rangeStart: '9:00', rangeEnd: '18:00'}, 
			{days: [2], rangeStart: '9:00', rangeEnd: '18:00'},  
			{days: [3], rangeStart: '9:00', rangeEnd: '18:00'}, 
			{days: [4], rangeStart: '9:00', rangeEnd: '18:00'}, 
			{days: [5], rangeStart: '9:00', rangeEnd: '18:00'}];//график работы клиники
		public static var CalendarModule_NonWorkDays:Array= [0,6];//выходные дни клиники массив, формат [0,1,2,3,4,5,6,0]
		
		public static var CalendarModule_StartShowTime:String="8:00";
		public static var CalendarModule_EndShowTime:String="18:00";
		
		public static var CalendarModule_BubbleHelp:Object={cabinet:true,
			doctor:true,
			fio:true,
			mobilephone:true,
			phone:true,
			remarks:true,
			workdescription:true,
			card:true};//настройка всплывающей подсказки объект
		
		public static var CalendarModule_DobReminder:Boolean = true;//напоминания о днях рождениях
		public static var AutoReloadEnabled:Boolean = false;//автообновление
		//public static var CalendarModule_AutoReloadValue:int = 5;
		public static var CalendarModule_FullFIO:Boolean = true;//вывод полного ФИО
		public static var CalendarModule_LegendEnabled:Boolean = true;//показывать легенду
		public static var CalendarModule_DaysCount:int = 1;//количество отображаемых дней в календаре [1-7]
		public static var CalendarModule_CalendarRenderer_ToolTip_byDate:Boolean = true;//как показывать график в органайзере Датам(true)/Врачам(false)
		public static var CalendarModule_CalendarRenderer_calendarWorkTimesByDoctors:Boolean = true;//Нерабочее время по врачам либо по клинике
		public static var CalendarModule_CalendarRenderer_noticedWindow:Boolean = true;//отображение окна предупреждений
		// инициализация настроек календаря по умолчанию
		public static var CalendarModule_Wizard_minDiapMinutes:int = 30;//Минимальный диапазон, учитываемый в Визарде (в минутах)
		// инициализация массива смен
		public static var CalendarModule_changesDataProvider:Array= [{uid:"-1", startTime:"-1", endTime:"-1"}];
		
		public static var CalendarModule_visitedByDefault:Boolean = false;
		
		public static var CalendarModule_useWizardForDispanser:Boolean = true;
		
		//PatientCardModule
		public static var cardBeforNumber:String="";
		public static var cardAfterNumber:String="_";
		public static var cardAfterNumberDateFormat:String="YYYY";
		public static var cardAfterNumberDateFlag:Boolean = true;
		public static var channelsZoomerFlag:Boolean = true;
		public static var patientscardsFlag:Boolean = true;
		public static var patientsCardsNumbersFlag:Boolean = true;
		
		public static var printTestPage:Boolean = true;
		public static var includePriceInReport:Boolean = false;
		public static var includeMaterialsInReport:Boolean = false;
		public static var includeDoctorSignatureInReport:Boolean = false;
		public static var includeNoncompleteProcedures043:Boolean = false;
		public static var includeShifrProcedures043Flag:Boolean = false;
		public static var includeCountProcedures043Flag:Boolean = true;
		
		public static var isAdditionalFilters_Boolean:Boolean = false;
		public static var isNoGroupingTretmentCoursePrice:Boolean = false;
		public static var openTreatmentCourseAfterDiagnos2Save:Boolean = false;
		public static var pagingTreatmentCourse:Boolean = false;
		
		public static var patientCardDefaultCountry:String="";
		public static var patientCardDefaultState:String="";
		public static var patientCardDefaultRegion:String="";
		public static var patientCardDefaultCity:String="";
		
		public static var patientPrimaryCardDefaultRequieredFields:String = "";
		public static var patientPrimaryCardAutoFocusEnabled:Boolean = true;
		public static var patientPrimaryCardAutoFocusedField:String = "";
		
		public static var patientCardModulePageSize:int = 20;
		public static var patientCardModuleByPages:Boolean = true;
		
		//AWDoctorModule
		public static var AWDoctorModuletaskTimeMinutes:Number = 30;
		public static var AWDoctorModulestartWorkWeekend:String = "08:00:00";
		public static var AWDoctorModuleendWorkWeekend:String = "23:00:00";
		public static var AWDoctorModuleshowPatientNotice:Boolean = false;
		
		//SMSModule
		public static var SMSModuleDefaultSendingInTasks:Boolean = false;//SMS TO-DO
		public static var SMSModuleDefaultSendingInDispanser:Boolean = false;
		
		
		public static var unvisibleRooms:Array=[];
		
		public static var isColorChangeEnable:Boolean = true;
		
		[Bindable]
		public static var isSQLiteEnable:Boolean = true;
		
		//admin
		public static var isVocabulariesTab:Boolean = true;
		public static var isTestConstructorEnabled:Boolean = true;
		public static var isAdministratorTab:Boolean = true;
		public static var isEnableHRModules:Boolean = true;
		public static var isHRModuleEnabled:Boolean = true;
		private static var _isOnlyOwnerTasksEdit:Boolean = false;
		public static var isEnableDiscountCardModule:Boolean = true;
		private static var _isEnableCertificateModule:Boolean = true;	
		private static var _isEnableCertificateSumChange:Boolean=false;
		private static var _isNegativeDiscount:Boolean=false;
		private static var _isPricePrintEnable:Boolean=true;
		private static var _isPriceEditEnable:Boolean=true;
		private static var _isAccountShowOnlyUserOperation:Boolean=false;
		
		private static var _isAccoundLendDebPrevDate:Boolean=true;
		
		

		private static var _isEnableDiscountCardSumChange:Boolean = false;
		
		public static var isEnableAllPatientsARM:Boolean = false;
		public static var isCalendarTab:Boolean = false;
		public static var isConstructorTab:Boolean = true;
		private static var _isShowContactData:Boolean = true;
		public static var isWizardChangeDoctorEnabled:Boolean = false;
		
		private static var _isARMVocabularies:Boolean = true;
		private static var _isARMDispancer:Boolean = false;
		private static var _isQuickTreatmentCourse:Boolean = false;
		private static var _isARMTreatbalanceHistorySettings:Boolean = true;
		private static var _isShowFinanceData:Boolean = true;
		
		private static var _isARMTasksHistorySettings:Boolean = true;
		private static var _isARMDoctorsHistorySettings:Boolean = true;
		private static var _isTreatmentCourseModule:Boolean = true;
		private static var _isToothMapModule:Boolean = true;
		private static var _isTestModule:Boolean = true;
		private static var _isExport043Module:Boolean = true;
		private static var _isAddPatientEnable:Boolean = true;
		
		
		private static var _isAmbulatoryModule:Boolean = true;
		
		
		public static var isEnableCardNumberChange:Boolean = true;
		private static var _isEnableTestChange:Boolean = true;
		private static var _isLeadDoctorEnabled:Boolean = true;
		private static var _isSubdivisonEditorEnabled:Boolean = false;
		
		private static var _isReportsInTreatment:Boolean = true;
		public static var isEnablePriceChangeInNewTreatment:Boolean = false;
		private static var _isEnableTimeDateChangeInNewTreatment:Boolean = false;
		public static var isConfirmTreatmentProceduresEnabled:Boolean = false;
		private static var _isEnableMoneyInTreatmentCourse:Boolean = true;
		private static var _isAfterExamsEnabled:Boolean = true;
		
		[Bindable]
		public static var isAccountFlowDataChangeEnabled:Boolean = false;
		
		private static var _isAccountInvoiceDateNumberChangabel:Boolean = false;
		private static var _isPriceInInvoiceCreationEditable:Boolean = false;
		private static var _isEnableDiscountChangeInAccountModule:Boolean = true;
		public static var isSalaryInAccountEnabled:Boolean = false;
		private static var _isOnlyOwnerInvoiceAndFlowEdit:Boolean = false;
		private static var _isDiscountChangeEnabled:Boolean=true;
		[Bindable]
		public static var isShowOnlyCurrentSubdivision:Boolean = false;
		[Bindable]
		public static var isShowOnlyCurrentSubdivisionAccount:Boolean = false;

		private static var _isHRDataEnabled:Boolean = true;
		private static var _isEnableSalaryManager:Boolean = false;
		private static var _isPriceListSalarySettings:Boolean = false;
		private static var _isControlExpensesInPriceList:Boolean = false;
		[Bindable]
		public static var isWizardPrimaryEnabled:Boolean = true;
		public static var isEnableMassMailingInAdministratorPanel:Boolean = false;

		
		[Bindable]
		public static function get isARMTasksHistorySettings():Boolean
		{
			return _isARMTasksHistorySettings;
		}
		
		public static function set isARMTasksHistorySettings(value:Boolean):void
		{
			_isARMTasksHistorySettings = value;
		}
		
		[Bindable]
		public static function get isARMDoctorsHistorySettings():Boolean
		{
			return _isARMDoctorsHistorySettings;
		}
		
		public static function set isARMDoctorsHistorySettings(value:Boolean):void
		{
			_isARMDoctorsHistorySettings = value;
		}
		
		[Bindable]
		public static function get isAddPatientEnable():Boolean
		{
			return _isAddPatientEnable;
		}
		
		public static function set isAddPatientEnable(value:Boolean):void
		{
			_isAddPatientEnable = value;
		}
		
		public static function get globalScale():Number
		{
			return _globalScale;
		}
		[Bindable]
		public static function get isAmbulatoryModule():Boolean
		{
			return _isAmbulatoryModule;
		}
		
		public static function set isAmbulatoryModule(value:Boolean):void
		{
			_isAmbulatoryModule = value;
		}
		public static function set globalScale(value:Number):void
		{
			_globalScale = value;
			ConfigurationSettings.GLOBAL_SCALE=value;
		}
		
		public static function get treatmentWindowScale():Number
		{
			return _treatmentWindowScale;
		}
		
		public static function set treatmentWindowScale(value:Number):void
		{
			_treatmentWindowScale = value;
			ConfigurationSettings.WIN_TREATMENT_SCALE=value;
		}
		
		public static function get calendarWindowScale():Number
		{
			return _calendarWindowScale;
		}
		
		public static function set calendarWindowScale(value:Number):void
		{
			_calendarWindowScale = value;
			ConfigurationSettings.WIN_CALENDAR_SCALE=value;
			
		}
		
		public static function get f043WindowScale():Number
		{
			return _f043WindowScale;
		}
		
		public static function set f043WindowScale(value:Number):void
		{
			_f043WindowScale = value;
			ConfigurationSettings.WIN_TOOTH_SCALE=value;
		}
		
		public static function get locale():String
		{
			return _locale;
		}
		
		public static function set locale(value:String):void
		{
			_locale = value;
			ResourceManager.getInstance().localeChain = [ value ];
			Setting.locale = value;
		}
		
		
		
		
		
		public static function get toothfairyDateSeporatorFormat():String
		{
			return _toothfairyDateSeporatorFormat;
		}
		
		public static function set toothfairyDateSeporatorFormat(value:String):void
		{
			_toothfairyDateSeporatorFormat = value;
			ConstantSettings.TOOTHFAIRY_DATA_SEPARATOR = value;
		}
		
		public static function get toothfairyDateTypeFormat():int
		{
			return _toothfairyDateTypeFormat;
		}
		
		public static function set toothfairyDateTypeFormat(value:int):void
		{
			_toothfairyDateTypeFormat = value;
			ConstantSettings.TOOTHFAIRY_DATA_TYPE = value;
			
		}
		
		public static function get toothfairyDateFormat():String
		{
			return _toothfairyDateFormat;
		}
		
		public static function set toothfairyDateFormat(value:String):void
		{
			_toothfairyDateFormat = value;
			ConstantSettings.TOOTHFAIRY_DATA_FORMAT = value;
			ConstantSettings.TOOTHFAIRY_TIMESTAMP_FORMAT = value + ' ' + DateFormatsTypes.timeformat;
		}
		
		public static function settingFromString(name:String, value:String):void
		{
			var setting:Object=ClientSettings[name];
			if(setting is String)
			{
				ClientSettings[name]=value;
			}
			else if(setting is Boolean)
			{
				ClientSettings[name]=(value=="1");
			}
			else if(setting is Number)
			{
				ClientSettings[name]=parseFloat(value);
			}
			else if(setting is int)
			{
				ClientSettings[name]=parseInt(value);
			}
			else if(setting is uint)
			{
				ClientSettings[name]=uint(parseInt(value));
			}
			else if(setting is Array)
			{
				ClientSettings[name]=xmlStringToArray(value as String);
			}
			else
			{
				ClientSettings[name]=xmlToObject(new XML(value));
			}
		}
		
		public static function settingToString(name:String):String
		{
			var setting:Object=ClientSettings[name];
			if(setting is String||setting is Number||setting is int||setting is uint)
			{
				return String(setting);
			}
			else if(setting is Boolean)
			{
				return (setting as Boolean)?"1":"0";
			}
			else if(setting is Array)
			{
				return arrayToXmlString(setting as Array);
			}
			else
			{
				return objectToXml(setting).toXMLString();
			}
		}
		
		public static function arrayToXmlString(arr:Array):String
		{
			if(arr==null)
			{
				return '';
			}
			var xmlItems:XML=<items/>;
			var xmlItem:XML;
			for each (var item:Object in arr) 
			{
				xmlItem=<item/>;
				if(item is String||item is Boolean||item is Number||item is int||item is uint)
				{
					xmlItem.appendChild(item);
				}
				else
				{
					xmlItem=objectToXml(item);
				}
				xmlItems.appendChild(xmlItem);
				
			}
			return xmlItems.toXMLString();
		}
		
		public static function xmlStringToArray(xmlStr:String):Array
		{
			var arr:Array=[];
			var xmlItems:XML=new XML(xmlStr);
			
			for each (var xmlItem:XML in xmlItems.item) 
			{
				if(xmlItem.attributes().length()==0)
				{
					
					arr.push(xmlItem.toString());
				}
				else
				{
					arr.push(xmlToObject(xmlItem));
				}
				
			}
			return arr;
		}
		
		public static function xmlToObject(xmlItem:XML):Object
		{
			var item:Object=new Object;
			var strItem:String;
			for each (var att:XML in xmlItem.@*) 
			{
				strItem=att.valueOf();
				if(strItem.indexOf("[")==0&&strItem.lastIndexOf("]")==strItem.length-1)
				{
					item[att.name().localName]=	strItem.substring(1,strItem.length-1).split(',');
				}
				else
				{
					item[att.name().localName]=	att.valueOf();
				}
			}
			
			return item;
		}
		
		public static function objectToXml(item:Object):XML
		{
			if(item==null)
			{
				return new XML;
			}
			var xmlItem:XML=<item/>;
			
			for (var param:String in item)
			{
				if(item[param] is Array)
				{
					xmlItem.@[param]="["+(item[param] as Array).join(',')+"]";
				}
				else
				{
					xmlItem.@[param]=item[param];
					
				}
			}
			
			return xmlItem;
		}
		
		[Bindable]
		public static function get isShowFinanceData():Boolean
		{
			return _isShowFinanceData;
		}
		
		public static function set isShowFinanceData(value:Boolean):void
		{
			_isShowFinanceData = value;
		}
		
		
		[Bindable]
		public static function get isARMTreatbalanceHistorySettings():Boolean
		{
			return _isARMTreatbalanceHistorySettings;
		}
		
		public static function set isARMTreatbalanceHistorySettings(value:Boolean):void
		{
			_isARMTreatbalanceHistorySettings = value;
		}
		
		[Bindable]
		public static function get isControlExpensesInPriceList():Boolean
		{
			return _isControlExpensesInPriceList;
		}
		
		public static function set isControlExpensesInPriceList(value:Boolean):void
		{
			_isControlExpensesInPriceList = value;
		}
		
		[Bindable]
		public static function get isEnableDiscountChangeInAccountModule():Boolean
		{
			return _isEnableDiscountChangeInAccountModule;
		}
		
		public static function set isEnableDiscountChangeInAccountModule(value:Boolean):void
		{
			_isEnableDiscountChangeInAccountModule = value;
		}
		
		[Bindable]
		public static function get isExport043Module():Boolean
		{
			return _isExport043Module;
		}
		
		public static function set isExport043Module(value:Boolean):void
		{
			_isExport043Module = value;
		}
		
		[Bindable]
		public static function get isLeadDoctorEnabled():Boolean
		{
			return _isLeadDoctorEnabled;
		}
		
		public static function set isLeadDoctorEnabled(value:Boolean):void
		{
			_isLeadDoctorEnabled = value;
		}
		
		[Bindable]
		public static function get isSubdivisonEditorEnabled():Boolean
		{
			return _isSubdivisonEditorEnabled;
		}
		
		public static function set isSubdivisonEditorEnabled(value:Boolean):void
		{
			_isSubdivisonEditorEnabled = value;
		}
		
		
		
		[Bindable]
		public static function get isPriceListSalarySettings():Boolean
		{
			return _isPriceListSalarySettings;
		}
		
		public static function set isPriceListSalarySettings(value:Boolean):void
		{
			_isPriceListSalarySettings = value;
		}
		
		[Bindable]
		public static function get isShowContactData():Boolean
		{
			return _isShowContactData;
		}
		
		public static function set isShowContactData(value:Boolean):void
		{
			_isShowContactData = value;
		}
		
		[Bindable]
		public static function get isTestModule():Boolean
		{
			return _isTestModule;
		}
		
		public static function set isTestModule(value:Boolean):void
		{
			_isTestModule = value;
		}
		
		[Bindable]
		public static function get isToothMapModule():Boolean
		{
			return _isToothMapModule;
		}
		
		public static function set isToothMapModule(value:Boolean):void
		{
			_isToothMapModule = value;
		}
		
		[Bindable]
		public static function get isTreatmentCourseModule():Boolean
		{
			return _isTreatmentCourseModule;
		}
		
		public static function set isTreatmentCourseModule(value:Boolean):void
		{
			_isTreatmentCourseModule = value;
		}
		
		[Bindable]
		public static function get isAfterExamsEnabled():Boolean
		{
			return _isAfterExamsEnabled;
		}
		
		public static function set isAfterExamsEnabled(value:Boolean):void
		{
			_isAfterExamsEnabled = value;
		}
		
		[Bindable]
		public static function get isEnableSalaryManager():Boolean
		{
			return _isEnableSalaryManager;
		}
		
		public static function set isEnableSalaryManager(value:Boolean):void
		{
			_isEnableSalaryManager = value;
		}
		
		[Bindable]
		public static function get isHRDataEnabled():Boolean
		{
			return _isHRDataEnabled;
		}
		
		public static function set isHRDataEnabled(value:Boolean):void
		{
			_isHRDataEnabled = value;
		}
		
		[Bindable]
		public static function get isAccountInvoiceDateNumberChangabel():Boolean
		{
			return _isAccountInvoiceDateNumberChangabel;
		}
		
		public static function set isAccountInvoiceDateNumberChangabel(value:Boolean):void
		{
			_isAccountInvoiceDateNumberChangabel = value;
		}
		
		[Bindable]
		public static function get isEnableMoneyInTreatmentCourse():Boolean
		{
			return _isEnableMoneyInTreatmentCourse;
		}
		
		public static function set isEnableMoneyInTreatmentCourse(value:Boolean):void
		{
			_isEnableMoneyInTreatmentCourse = value;
		}
		
		[Bindable]
		public static function get isEnableTimeDateChangeInNewTreatment():Boolean
		{
			return _isEnableTimeDateChangeInNewTreatment;
		}
		
		public static function set isEnableTimeDateChangeInNewTreatment(value:Boolean):void
		{
			_isEnableTimeDateChangeInNewTreatment = value;
		}
		
		[Bindable]
		public static function get isPriceInInvoiceCreationEditable():Boolean
		{
			return _isPriceInInvoiceCreationEditable;
		}
		
		public static function set isPriceInInvoiceCreationEditable(value:Boolean):void
		{
			_isPriceInInvoiceCreationEditable = value;
		}
		
		[Bindable]
		public static function get isReportsInTreatment():Boolean
		{
			return _isReportsInTreatment;
		}
		
		public static function set isReportsInTreatment(value:Boolean):void
		{
			_isReportsInTreatment = value;
		}
		
		[Bindable]
		public static function get isOnlyOwnerTasksEdit():Boolean
		{
			return _isOnlyOwnerTasksEdit;
		}
		
		public static function set isOnlyOwnerTasksEdit(value:Boolean):void
		{
			_isOnlyOwnerTasksEdit = value;
		}
		
		[Bindable]
		public static function get isOnlyOwnerInvoiceAndFlowEdit():Boolean
		{
			return _isOnlyOwnerInvoiceAndFlowEdit;
		}
		
		public static function set isOnlyOwnerInvoiceAndFlowEdit(value:Boolean):void
		{
			_isOnlyOwnerInvoiceAndFlowEdit = value;
		}
		
		public static function get isARMVocabularies():Boolean
		{
			return _isARMVocabularies;
		}
		
		public static function set isARMVocabularies(value:Boolean):void
		{
			_isARMVocabularies = value;
		}
		
		public static function get isARMDispancer():Boolean
		{
			return _isARMDispancer;
		}
		
		public static function set isARMDispancer(value:Boolean):void
		{
			_isARMDispancer = value;
		}
		
		
		
		[Bindable]
		public static function get isDiscountChangeEnabled():Boolean
		{
			return _isDiscountChangeEnabled;
		}
		
		public static function set isDiscountChangeEnabled(value:Boolean):void
		{
			_isDiscountChangeEnabled = value;
		}
		
		[Bindable]
		public static function get isEnableDiscountCardSumChange():Boolean
		{
			return _isEnableDiscountCardSumChange;
		}
		
		public static function set isEnableDiscountCardSumChange(value:Boolean):void
		{
			_isEnableDiscountCardSumChange = value;
		}
		
		[Bindable]
		public static function get isQuickTreatmentCourse():Boolean
		{
			return _isQuickTreatmentCourse;
		}
		
		public static function set isQuickTreatmentCourse(value:Boolean):void
		{
			_isQuickTreatmentCourse = value;
		}
		
		[Bindable]
		public static function get isEnableTestChange():Boolean
		{
			return _isEnableTestChange;
		}
		
		public static function set isEnableTestChange(value:Boolean):void
		{
			_isEnableTestChange = value;
		}
		
		[Bindable]
		public static function get showSubdivision():Boolean
		{
			return _showSubdivision;
		}
		
		public static function set showSubdivision(value:Boolean):void
		{
			_showSubdivision = value;
		}

		[Bindable]
		public static function get isEnableCertificateModule():Boolean
		{
			return _isEnableCertificateModule;
		}
		
		public static function set isEnableCertificateModule(value:Boolean):void
		{
			_isEnableCertificateModule = value;
		}
		
		
		public static function get isEnableCertificateSumChange():Boolean
		{
			return _isEnableCertificateSumChange;
		}
		
		[Bindable]
		public static function set isEnableCertificateSumChange(value:Boolean):void
		{
			_isEnableCertificateSumChange = value;
		}
		
		
		
		
		public static function get isNegativeDiscount():Boolean
		{
			return _isNegativeDiscount;
		}
		[Bindable]
		public static function set isNegativeDiscount(value:Boolean):void
		{
			_isNegativeDiscount = value;
		}
		
		
		public static function get isPricePrintEnable():Boolean
		{
			return _isPricePrintEnable;
		}
		[Bindable]
		public static function set isPricePrintEnable(value:Boolean):void
		{
			_isPricePrintEnable = value;
		}
		
		public static function get isPriceEditEnable():Boolean
		{
			return _isPriceEditEnable;
		}
		[Bindable]
		public static function set isPriceEditEnable(value:Boolean):void
		{
			_isPriceEditEnable = value;
		}
		
		
		public static function get isAccountShowOnlyUserOperation():Boolean
		{
			return _isAccountShowOnlyUserOperation;
		}
		[Bindable]
		public static function set isAccountShowOnlyUserOperation(value:Boolean):void
		{
			_isAccountShowOnlyUserOperation = value;
		}
		
		
		public static function get isAccoundLendDebPrevDate():Boolean
		{
			return _isAccoundLendDebPrevDate;
		}
		[Bindable]
		public static function set isAccoundLendDebPrevDate(value:Boolean):void
		{
			_isAccoundLendDebPrevDate = value;
		}
		
	}
}