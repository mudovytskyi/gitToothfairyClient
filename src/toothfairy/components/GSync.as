package toothfairy.components
{
	import org.granite.gravity.Consumer;
	import org.granite.gravity.Producer;

	[Bindable]
	public class GSync
	{
		public static var SYNCS_CONSUMER:Consumer;
		public static var SYNCS_PRODUCER:Producer;
	}
}