package toothfairy.components
{
	import flash.events.Event;
	
	import spark.components.Panel;
	import spark.components.ToggleButton;
	
	[Event(name="lockPanelSelectedChange", type="flash.events.Event")]
	public class MainMenuPanel extends Panel
	{
		[SkinPart(required="false")]
		public var lockPanelButton:ToggleButton;
		
		private var _lockPanelSelected:Boolean;
		
		public function MainMenuPanel()
		{
			super();
		}
		
		
		[Bindable(event="lockPanelSelectedChange")]
		public function get lockPanelSelected():Boolean
		{
			return _lockPanelSelected;
		}

		public function set lockPanelSelected(value:Boolean):void
		{
			if( _lockPanelSelected !== value)
			{
				_lockPanelSelected = value;
				dispatchEvent(new Event("lockPanelSelectedChange"));
			}
		}

	}
}