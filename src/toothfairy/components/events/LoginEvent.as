package toothfairy.components.events
{
	import flash.events.Event;
	
	import mx.rpc.events.ResultEvent;
	
	public class LoginEvent extends Event
	{
		public static const LOGIN:String = "toothfairyLoginEvent";
		
		public var checkResult:ResultEvent;
		
		public function LoginEvent(checkResult:ResultEvent, type:String = LOGIN, bubbles:Boolean=false, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.checkResult = checkResult;
		}
		public override function clone():Event
		{
			return new LoginEvent(checkResult, type, bubbles, cancelable);
		}
	}
}