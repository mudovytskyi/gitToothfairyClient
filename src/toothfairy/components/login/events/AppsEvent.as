package toothfairy.components.login.events
{
	import flash.events.Event;
	
	public class AppsEvent extends Event
	{
		
		public static const RETRAY:String = "toothfairyRetray";
		public static const START:String = "toothfairyStart";
		public static const START_ARM:String = "toothfairyStartArm";
		public static const LOGOUT:String = "toothfairyLogout";
		
		
		public function AppsEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new AppsEvent(type);
		}
	}
}