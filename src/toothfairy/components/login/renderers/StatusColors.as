package toothfairy.components.login.renderers
{
	[Bindable]
	public class StatusColors
	{
		public static const GRAY:uint = 0xdddddd;
		public static const ONLINE:uint = 0x00b200;
		public static const OFFLINE:uint = 0xd10000;
		
	}
}