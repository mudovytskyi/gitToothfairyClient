package toothfairy
{

	[Bindable]
	public class LoggedUser
	{
		public static var user_id:String;
		public static var user_type:String;
		public static var user_login:String;
		public static var user_password:String;
		public static var user_configstring:String;
		public static var user_lname:String;
		public static var user_fname:String;
		public static var user_sname:String;
		
		public static var assistant_id:String;
		public static var assistant_lname:String;
		public static var assistant_fname:String;
		public static var assistant_sname:String;
		
		
		public static function logOut():void
		{
			//Setting.rememberflag = false;
			user_id = null;
			user_type = null;
			user_login = null;
			user_password = null;
			user_configstring = null;
			user_lname = null;
			user_fname = null;
			user_sname = null;
			assistant_id = null;
			assistant_lname = null;
			assistant_fname = null;
			assistant_sname = null;
		}
	}
}