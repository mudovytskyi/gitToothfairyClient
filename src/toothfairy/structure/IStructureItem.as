package toothfairy.structure
{
	import flash.events.Event;

	public interface IStructureItem
	{
		function refresh(event:Event=null):void; 
		function refreshToFirst(event:Event=null):void; 
		function clear(event:Event=null):void;
	}
}