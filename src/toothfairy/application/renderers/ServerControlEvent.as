package toothfairy.application.renderers
{
	import flash.events.Event;
	
	public class ServerControlEvent extends Event
	{
		
		public static const ACTIVE_CHANGE:String = "changeActiveServer";
		public static const DELETED_KEY_CHANGE:String = "deletedKeyServer";
		
		
		public var serverID:int;
		public var serverIP:String;
		
		public function ServerControlEvent(type:String, serverIP:String, serverID:int, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.serverIP = serverIP;
			this.serverID = serverID;
		}
		
		public override function clone():Event
		{
			return new ServerControlEvent(type, serverIP, serverID);
		}
	}
}