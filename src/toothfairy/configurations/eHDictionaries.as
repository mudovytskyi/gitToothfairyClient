package toothfairy.configurations
{
	import mx.collections.ArrayCollection;
	
	
	[Bindable]
	public class eHDictionaries
	{
		public static var eh_dictionaries:Object;
		public static var eh_Regions:ArrayCollection;
		
		
		public static const PHONE_TYPE:String = 'PHONE_TYPE';
		public static const DOCUMENT_TYPE:String = 'DOCUMENT_TYPE';
		public static const DOCUMENT_RELATIONSHIP_TYPE:String = 'DOCUMENT_RELATIONSHIP_TYPE';
		public static const EMPLOYEE_TYPE:String = 'EMPLOYEE_TYPE';
		public static const LEGAL_ENTITY_TYPE:String = 'LEGAL_ENTITY_TYPE';
		public static const OWNER_PROPERTY_TYPE:String = 'OWNER_PROPERTY_TYPE';
		public static const LEGAL_FORM:String = 'LEGAL_FORM';
		public static const ADDRESS_TYPE:String = 'ADDRESS_TYPE';
		public static const COUNTRY:String = 'COUNTRY';
		public static const SETTLEMENT_TYPE:String = 'SETTLEMENT_TYPE';
		public static const STREET_TYPE:String = 'STREET_TYPE';
		public static const ACCREDITATION_CATEGORY:String = 'ACCREDITATION_CATEGORY';
		public static const POSITION:String = 'POSITION';
		public static const EDUCATION_DEGREE:String = 'EDUCATION_DEGREE';
		public static const SPECIALITY_TYPE:String = 'SPECIALITY_TYPE';
		public static const QUALIFICATION_TYPE:String = 'QUALIFICATION_TYPE';
		public static const SPEC_QUALIFICATION_TYPE:String = 'SPEC_QUALIFICATION_TYPE';
		public static const SPECIALITY_LEVEL:String = 'SPECIALITY_LEVEL';
		public static const SCIENCE_DEGREE:String = 'SCIENCE_DEGREE';
		public static const DIVISION_TYPE:String = 'DIVISION_TYPE';
		public static const DIVISION_STATUS:String = 'DIVISION_STATUS';
		
		public static const AUTHENTICATION_METHOD:String = 'AUTHENTICATION_METHOD';
		
		
		
		
		
		
		
		
	}
}