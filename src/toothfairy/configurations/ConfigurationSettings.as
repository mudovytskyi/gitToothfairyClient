package toothfairy.configurations
{
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	

	[Bindable]
	public class ConfigurationSettings
	{
		public static var adultAge:int = 18;
		public static var patientAgreementsAdult:Boolean = false;
		public static var isSalaryPriceMode:Boolean = false;
		
		
		public static var DEBUG_MODE:Boolean = false;
		// add to edit patient window into others hospital|pregnancy panel 
		// add in menu panel patients two panels hospital patients and pregnancy panel (also in AWD)
		// make toothMap button invisible and diganos button visible

		//---- !!! UNDO <start> !!!  ----
		public static var NHEALTH_MODE:Boolean = false;
		//---- !!! UNDO  <end>  !!!  ----
		public static var EHEALTH_MODE:Boolean = false;
		public static var EHEALTHBRIDGE_MODE:Boolean = false;
		public static var EHEALTH_MIS_ACCESS_TOKEN:String = '';
		public static var EHEALTH_MIS_ACCESS_TOKEN_EXPIRY:Date;
		/** 
		 * This variable define what price enabled in local application. 
		 * 
		 * <p>0 - main price 1 with extra options to select price.</br>
		 * 1 - main price 1</br>
		 * 2 - price 2</br>
		 * 3 - price 3</p>
		 *  
		 */
		public static var PRICE:int = 0;
		public static var PRICE_INIT:int = 0;
		public static var CABINET_ID:String = 'empty';
		
		
		//PerioDnD
		public static var isPerioDnDTFsync:Boolean = false;
		// Google Calendar
		public static var isGoogleCalendarAuthed:Boolean = false;
		public static var isGoogleCalendarSynced:Boolean = false;
		
		
		public static var IS_AWD:Boolean = false;
		

		//additional functions
		public static var LAZY_NUMBERS:int = 2;
		
		public static var LOGO:ByteArray;
		
		public static var isExtendedDesktop:Boolean;
		
		public static var IS_STORE_ENABLE:Boolean;
		
		
		public static var GLOBAL_SCALE:Number = 1;
		
		public static var WIN_TOOTH_SCALE:Number = 1;
		public static var WIN_CALENDAR_SCALE:Number = 1;
		public static var WIN_TREATMENT_SCALE:Number = 1;
		
		public static var countryPhoneCode:String = "38";
		public static var isDiscountSchema:Boolean = false;
		public static var isDiscountCards:Boolean = false;
		public static var isCertificates:Boolean = false;
		
		// налаштування картки первинного запису на прийом
		public static var isAutoFocusForPrimaryPatientFieldEnabled:Boolean = true;
		public static var autoFocusedPrimaryPatientField:String = 'primfname';
		public static var requiredPrimaryPatientFields:String = 'primfname';
		
		public static var f39fields:ArrayCollection;//настройки отчетности
		
		//настройки склада с бд
		public static var isCommonWarehouse:Boolean = false;
		public static var isWarehouseWithRooms:Boolean = true;
		public static var writeOffOrder:String = 'farmpriceid asc';
		public static var autoWriteOff:int = 1;
		public static var storageIsAccountflow:Boolean = true;
		
		
		
		public static var  isEnableCardNumberChange:Boolean = true;
		//doctor cards
		public static var  isEnableSalaryManager:Boolean = true;
		//access control
		public static var  isEnableAccessControl:Boolean = true;
		
		
		//with newtreatment course
		/**
		* treatmentcoures
		* export
		*/
		public static var isNewTreatment:Boolean = true;
		//with viziofraph module
		public static var isViziographModule:Boolean = true;
		//with filemanager module
		public static var isFileManagerModule:Boolean = true;
		//with psorolite module
		public static var isPsoroliteModule:Boolean = false;
		
		//PAtientsCards, all tabs in AWDoctor
		public static var isXrayaccountanceModule:Boolean = true;
		
		//navigation menu tabs
		public static var isPatientTab:Boolean = true;
		public static var isAWDoctorTab:Boolean = true;
		public static var isDirectorTab:Boolean = true;
		public static var isWarehouseTab:Boolean = true;
		public static var isReportsTab:Boolean = true;
		
		public static var isDoctorARMState:Boolean = true;
		
		public static var isSMSmodule:Boolean = true;
		
		//in administrator isEnableHRModules
		public static var isTFLogin:Boolean = true;
		
		//budget
		public static var isOrderEnabled:Boolean = true;
		
		//cashflow
		public static var isAccountModuleEnabled:Boolean = true;
		
		//2.3
		public static var isOrganizerEnabled:Boolean = true;
		public static var isInsuranceEnabled:Boolean = true;
		
		// Передплата / subscription
		public static var isSubscriptionModuleEnabled:Boolean = true;
		
		// Відображення закладки "Вагітні" / pregnant
		public static var isPregnantPatientCardShown:Boolean = true;
		
		public static var isOnlineSettingsEnabled:Boolean = true;
		
		
		public static var isChiefAnalyticsEnabled:Boolean = true;
		
		
		public static var isNewWarehouse:Boolean=true;
		
		
		public static var isAdminTabEnabled:Boolean = false;
		public static var isStorageSale:Boolean = false;
		
		public static var isAllowCancelDebt:Boolean = false;
		public static var isPatientExport:Boolean = false;
		public static var isSettings19:Boolean = false;
		public static var isSettings20:Boolean = false;
		public static var isSettings21:Boolean = false;
		
		public static var calendarModuleDispancerDays:int=7;
		
		public static var isInvoiceSummFloorEnabled:Boolean = false;
		
		// для ЗФ, печать "Консультативное заключение", находится в tooth.mxml
		public static var isAdvisoryСonclusion:Boolean = false;
	}
}