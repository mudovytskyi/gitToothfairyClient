package toothfairy.configurations
{
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.resources.ResourceManager;

	[Bindable]
	public class ConstantSettings
	{
		/**
		 * ToothFairy: DD.MM.YYYY
		 * */
		public static var TOOTHFAIRY_DATA_FORMAT:String = "DD.MM.YYYY";
		
		/**
		 * ToothFairy character for separating Date: .
		 * */
		public static var TOOTHFAIRY_DATA_SEPARATOR:String = ".";
		
		/**
		 * ToothFairy type of date format: 
		 * 			1 - xx.xx.YYYY 
		 * 			2 - xx.YYYY.xx 
		 * 			3 - YYYY.xx.xx
		 * */
		public static var TOOTHFAIRY_DATA_TYPE:int = 1;
		
		/**
		 * ToothFairy: DD.MM.YYYY JJ:NN:SS
		 * */
		public static var TOOTHFAIRY_TIMESTAMP_FORMAT:String = "DD.MM.YYYY JJ:NN:SS";
		
		/**
		 * Firebird: YYYY-MM-DD
		 * */
		public static const FIREBIRD_DATA_FORMAT:String = "YYYY-MM-DD";
		
		/**
		 * Firebird: YYYY-MM-DD JJ:NN:SS
		 * */
		public static const FIREBIRD_TIMESTAMP_FORMAT:String = "YYYY-MM-DD JJ:NN:SS";
		
		
		/*
		 * Firebird: YYYY-MM-DD JJ:NN:SS NOT USE
		 * 
		public static const FIREBIRD_STRING_TIMESTAMP_PATTERN:String = "/(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/";
		*/
		/**
		 * ToothFairy currency label
		 * */
		public static var TOOTHFAIRY_CURRENCY_LABEL:String = "грн";
		
		/**
		 * ToothFairy currency decimal label
		 * */
		public static var TOOTHFAIRY_CURRENCYDECIMAL_LABEL:String = "коп";
		
		/**
		 * Minutes in one UOP
		 * */
		public static const MINUTES_UOP_RATE:Number = 16;
		
		/**
		 * Delay of timer in lazy loading inputs
		 * */
		public static const TOOTHFAIRY_LAZY_LOADING_TIMER_DELAY:Number = 500;
		
		/**
		 * Number decimal separator
		 * */
		public static const NUMBER_DECIMAL_SEPARATOR:String = ".";
		/**
		 * Number grouping separator
		 * */
		public static const NUMBER_GROUPING_SEPARATOR:String = "";
		
		public static const providerForPatientsLazyCombobox:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'FIOCARDNUMBERLabel'), searchdata:'FIOCARDNUMBER', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'patientTable.FIODataGridColumn'), searchdata:'FIO', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'LastNameDataGridColumn'), searchdata:'LASTNAME', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'FirstNameDataGridColumn'), searchdata:'FIRSTNAME', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'MiddleNameDataGridColumn'), searchdata:'MIDDLENAME', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'CardNumberDataGridColumn'), searchdata:'CARDNUMBER_FULL', datagridsearch:1},
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'CityLabel'), searchdata:'CITY', datagridsearch:0},
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'RegionLabel'), searchdata:'RAGION', datagridsearch:0},
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'StateLabel'), searchdata:'STATE', datagridsearch:0}
		]);
		
		public static const providerForCashControl:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('AccountModule', 'cashbank'), searchdata:-1}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'cash'), searchdata:0}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'bank'), searchdata:1}
			
		]);
		
		
		public static const providerCBForCashControl:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('AccountModule', 'cash'), searchdata:0}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'bank'), searchdata:1},
			{label:ResourceManager.getInstance().getString('AccountModule', 'cashplusbank'), searchdata:-1}
			
		]);
		public static const providerCBForCashControl2:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('AccountModule', 'cashplusbank'), searchdata:-1},
			{label:ResourceManager.getInstance().getString('AccountModule', 'cash'), searchdata:0}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'bank'), searchdata:1}
			
		]);
		
		/**
		 * global temporary folder
		 * */
		public static var FOLDER_TEMP:File;
		
		
		/**
		 * global toothfairydocument folder
		 * */
		public static var FOLDER_DOCUMENTS:File;
		
		
		/**
		 * alpha of first buttons
		 * */
		public static var ALPHA_PRIMARY:Number = 0.5;
		
		/**
		 * alpha of secondary buttons
		 * */
		public static var ALPHA_SECONDARY:Number = 0.3;
		
		/**
		 * Seconds in one day 60s * 60m * 24h
		 */		
		public static var SECONDS_IN_DAY:int = 60 * 60 * 24;
		
		/**
		 * Milliseconds in one day 1000ms * 60s * 60m * 24h
		 */		
		public static var MILLISECONDS_IN_DAY:int = 1000 * 60 * 60 * 24;
		
		/**
		 * Google Calendar: yyyy-MM-dd'T'HH:mm:ss+/-HH:mm
		 * */
		public static const GOOGLE_CALENDAR_TIMESTAMP_FORMAT:String = "yyyy-MM-dd'T'HH:mm:ss'{0}{1}:{2}'";
		
		/**
		 * Google Calendar: перелік часових поясів 
		 */		
		public static var GOOGLE_CALENDAR_TIME_ZONES:ArrayCollection = new ArrayCollection();
	}
}