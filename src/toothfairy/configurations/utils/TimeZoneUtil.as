package toothfairy.configurations.utils
{
	public final class TimeZoneUtil
	{
		private static const dateOffset:Number = new Date().timezoneOffset;
		private static const dateOffsetSign:int = dateOffset < 0 ? -1 : 1;
		public static function getTimeZoneSign():String
		{
			
			return dateOffset > 0 ? '-' : '+';	
		}
		
		public static function getTimeZoneHours():int
		{
			// Кількість годин, оскільки часовий пояс повертає у хвилинах
			return int(dateOffset * dateOffsetSign / 60);
		}
		
		public static function getTimeZoneMinutes():int
		{
			return int(dateOffset * dateOffsetSign % 60);
		}
	}
}