package toothfairy.configurations
{
	import mx.collections.ArrayList;
	[Bindable]
	public class DateFormatsTypes
	{
		public static const dateformatsAL:ArrayList = new ArrayList([
			{id:0, label:'DD-MM-YYYY', seporator:'-', data:'DD-MM-YYYY', type:1}, 
			{id:1, label:'DD.MM.YYYY', seporator:'.', data:'DD.MM.YYYY', type:1}, 
			{id:2, label:'DD/MM/YYYY', seporator:'/', data:'DD/MM/YYYY', type:1}, 
			
			{id:3, label:'MM-DD-YYYY', seporator:'-', data:'MM-DD-YYYY', type:1}, 
			{id:4, label:'MM.DD.YYYY', seporator:'.', data:'MM.DD.YYYY', type:1}, 
			{id:5, label:'MM/DD/YYYY', seporator:'/', data:'MM/DD/YYYY', type:1}, 
			
			{id:6, label:'YYYY-MM-DD', seporator:'-', data:'YYYY-MM-DD', type:3}, 
			{id:7, label:'YYYY.MM.DD', seporator:'.', data:'YYYY.MM.DD', type:3}, 
			{id:8, label:'YYYY/MM/DD', seporator:'/', data:'YYYY/MM/DD', type:3}, 
			
			{id:9, label:'YYYY-DD-MM', seporator:'-', data:'YYYY-DD-MM', type:3}, 
			{id:10, label:'YYYY.DD.MM', seporator:'.', data:'YYYY.DD.MM', type:3}, 
			{id:11, label:'YYYY/DD/MM', seporator:'/', data:'YYYY/DD/MM', type:3}
		]);
		
		public static const timeformat:String = 'JJ:NN:SS';
	}
}