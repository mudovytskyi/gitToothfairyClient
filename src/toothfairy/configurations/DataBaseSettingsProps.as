package toothfairy.configurations
{
	//----------------------------------
	//
	// Class : DataBaseSettingsProps.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements:
	 *   @use: Постійні назв полів налаштувань у базі даних,
	 * 			за якими відбувається доступ і збереження,
	 * 			аби уникнути описки і зайвих помилок. 
	 *
	 *   @author: Mykola Udovytskyi
	 *   @date: Jul 8, 2019 @time: 10:14:12 AM */
	//----------------------------------
	public final class DataBaseSettingsProps
	{
		public static const IS_GOOGLE_CALENDAR_AUTHED:String = "isGoogleCalendarAuthed";
		public static const IS_GOOGLE_CALENDAR_SYNCED:String = "isGoogleCalendarSynced";
		public static const GOOGLE_CALENDAR_ACCOUNT_ID_AUTHED:String = "googleAccountIdAuthed";
		public static const GOOGLE_CALENDAR_ID_SYNCED:String = "googleCalendarIdSynced";
		public static const GOOGLE_CALENDAR_TIME_ZONE:String = "googleCalendarTimeZone";
		
		public static const CALENDAR_MODULE_WORKTIME:String = "CalendarModule_WorkTime";
		public static const CALENDAR_MODULE_NON_WORKDAYS:String = "CalendarModule_NonWorkDays";
		
	}
}