package toothfairy.configurations
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	[ResourceBundle("configurations_HealthTypes")]
	
	[Bindable]
	public class HealthTypes
	{
		private static var _healthtypesAL:ArrayCollection = new ArrayCollection([
			{id:-1 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeNon'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeNon_sl')}, 
			{id:0 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeTherapy'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeTherapy_sl')}, 
			{id:1 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeSurgery'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeSurgery_sl')},
			{id:2 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeOrthopedics'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeOrthopedics_sl')},
			{id:3 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeОrthodontics'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeОrthodontics_sl')},
			{id:4 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeСhild'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeСhild_sl')},
			{id:5 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeAesthetic'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeAesthetic_sl')},
			{id:6 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeEndodontics'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeEndodontics_sl')},
			{id:7 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypePeriodontics'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypePeriodontics_sl')},
			{id:8 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeHygienics'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeHygienics_sl')},
			{id:9 , label:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeRadiology'), 
				shortlabel:ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeRadiology_sl')}
		]);
		
		public static function get healthtypesAL():ArrayCollection
		{
			return ObjectUtil.copy(_healthtypesAL) as ArrayCollection;
		}
		

		public static function set healthtypesAL(arr:ArrayCollection):void{
			_healthtypesAL = arr;			
		}
		
		public static function getLabel(healthtype_id:int, isShortlabels:Boolean = false):String
		{
			if(isShortlabels == true)
			{
				return HealthTypes.healthtypesAL.getItemAt(healthtype_id+1).shortlabel;
			}
			else
			{
				if((healthtype_id >= 0)&&(healthtype_id <= 9))
				{
					return HealthTypes.healthtypesAL.getItemAt(healthtype_id+1).label;
				}
				else
				{
					return ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeNotTyped');
				}
			}
		}
		
		public static function getLabels(healthtypes:String, isShortlabels:Boolean = false):String
		{
			if((healthtypes != null)&&(["", "-1"].indexOf(healthtypes) == -1))
			{
				var dataArray:Array = healthtypes.split("|");
				var resultArray:Array = [];
				for each (var item:int in dataArray) 
				{
					resultArray.push(getLabel(item,isShortlabels));
				}
				
				return resultArray.join(', ');
			}
			else
			{
				return ResourceManager.getInstance().getString('configurations_HealthTypes', 'HealthTypeNotTyped');
			}
		}
		
		public static function parseDataFromDB(healthtypes:String):ArrayCollection
		{
			if(healthtypes != null&&healthtypes.length>0)
			{
				var outArray:ArrayCollection = new ArrayCollection;
				var array:Array = healthtypes.split("|");
				for (var i:int = 0; i < array.length; i++) 
				{
					if(HealthTypes.healthtypesAL.length > int(array[i]))
					{
						outArray.addItem(HealthTypes.healthtypesAL.getItemAt((int(array[i])+1)));
					}
				}
				return outArray;
			}
			return null;
		}
		
		public static function parseDataToDB(healthtypes:ArrayCollection):String
		{
			var outString:String = '';
			for (var j:int = 0; j < healthtypes.length; j++) 
			{
				outString += healthtypes[j].id;
				if(j != healthtypes.length-1)
					outString += '|';
			}
			return outString;
		}
	}
}