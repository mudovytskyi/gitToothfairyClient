package toothfairy.configurations
{
	import mx.collections.ArrayList;
	import mx.resources.ResourceManager;
	
	[ResourceBundle("configurations_F43")]

	[Bindable]
	public class F43
	{
		public static var diagnosisAL:ArrayList = new ArrayList([
			{id:-1 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_nonSelectedData')}, 
			{id:0 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_NormData')}, 
			{id:1 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_CariesData')},
			{id:2 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_CrownData')},
			{id:3 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_StoppingData')},
			{id:4 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_FacetData')},
			{id:5 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_RestorationData')},
			{id:6 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_HemisectionData')},
			{id:7 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_PulpitisData')}, 
			{id:8 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_PeriodontitisData')}, 
			{id:9 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_ParodontitData')},
			{id:11 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_RootData')},
			{id:12 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_AbsentData')},
			{id:13 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_ArtificialData')},
			{id:14 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_AmputationData')},
			{id:15 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_ResectionData')},
			{id:16 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_PinData')},
			{id:17 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_ImplantWithCrownData')},
			{id:18 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_ReplantationData')},
			{id:19 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_DentalCalculusData')},
			{id:24 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_GingivitisData')},
			{id:25 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_ImplantWithoutCrownData')},
			{id:26 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_TempStoppingData')},
			{id:27 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_ParodontotzData')},
			{id:28 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_HardtdefData')},
			{id:29 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_FluorozData')},
			{id:30 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_TabData')},
			{id:31 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_OrtodontPatologyData')},
			{id:32 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_SCariesData')},
			{id:33 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_BitData')},
			{id:34 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_WedgeData')},
			{id:35 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_IntactData')},
			{id:36 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_CariesObsData')},
			{id:37 , label:ResourceManager.getInstance().getString('configurations_F43', 'zub2b_CrownDefData')}
			
		]);
		
		
		/**
		 * Function that return label of diagnos by diagnos F43Id number
		 */
		public static function diagnosF43ToString(id:int):String
		{
			var diagnosLabel:String = '';
			for (var i:int = 0; i < F43.diagnosisAL.length; i++) 
			{
				if(id == F43.diagnosisAL.getItemAt(i).id)
				{
					diagnosLabel = F43.diagnosisAL.getItemAt(i).label;
					break;
				}
			}
			return diagnosLabel;
		}
	}
}