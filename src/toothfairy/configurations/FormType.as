package toothfairy.configurations
{
	import mx.collections.ArrayCollection;

	public final class FormType
	{
		public static const INFOCONSENT_FORM:int = 1;
		public static const MOZ_43_FORM:int = 2;
		
		public static const formTypeArrayCollection:ArrayCollection = new ArrayCollection(
			[{name:"Форма інфозгоди", id:FormType.INFOCONSENT_FORM}, 
			 {name:"Форма 043/о", id:FormType.MOZ_43_FORM}]
		); 
	}
}