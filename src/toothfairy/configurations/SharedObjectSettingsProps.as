package toothfairy.configurations
{
	//----------------------------------
	//
	// Class : SharedObjectSettingsProps.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements:
	 *   @use: Постійні назв полів налаштувань у cпільному об'єкті пам'яти,
	 * 			за якими відбувається доступ і збереження,
	 * 			аби уникнути описки і зайвих помилок. 
	 *
	 *   @author: Mykola Udovytskyi
	 *   @date: Aug 4, 2019 @time: 12:58:12 PM */
	//----------------------------------
	public final class SharedObjectSettingsProps
	{
		// Назви об'єктів
		public static const SO_TOOTHFAIRY:String = "toothfairy";
		
		// Назви полів
		public static const REGISTRATOR_SUBSCRIPTION_REFRESH_BASE_DATE:String = "registratorSubscriptionRefreshBaseDate";

		public static const TITLE_WINDOW_FOR_NEW_HOSPITAL_CARD_WIDTH:String = "titleWindowForNewHospitalCard_width";
		public static const TITLE_WINDOW_FOR_NEW_HOSPITAL_CARD_HEIGHT:String = "titleWindowForNewHospitalCard_height";
		public static const TITLE_WINDOW_FOR_NEW_HOSPITAL_CARD_X:String = "titleWindowForNewHospitalCard_x";
		public static const TITLE_WINDOW_FOR_NEW_HOSPITAL_CARD_Y:String = "titleWindowForNewHospitalCard_y";

		public static const TITLE_WINDOW_FOR_PHYSICAL_EXAM_WIDTH:String = "titleWindowForPhysicalExam_width";
		public static const TITLE_WINDOW_FOR_PHYSICAL_EXAM_HEIGHT:String = "titleWindowForPhysicalExam_height";
		public static const TITLE_WINDOW_FOR_PHYSICAL_EXAM_X:String = "titleWindowForPhysicalExam_x";
		public static const TITLE_WINDOW_FOR_PHYSICAL_EXAM_Y:String = "titleWindowForPhysicalExam_y";
	}
}