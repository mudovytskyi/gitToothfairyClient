package toothfairy.configurations
{
	import mx.collections.ArrayList;

	[Bindable]
	public class DocsFormatsTypes
	{
		public static const formats:ArrayList = new ArrayList([
			{id:1, label:'DOCX', data:'docx'}, 
			{id:2, label:'ODT', data:'odt'}
		]);
		
		public static const formats_tables:ArrayList = new ArrayList([
			{id:0, label:'XLSX', data:'xlsx'}, 
			{id:1, label:'ODS', data:'ods'}
		]);
	}
	
}