package toothfairy.configurations
{
	import mx.collections.ArrayList;
	import mx.resources.ResourceManager;
	
	[ResourceBundle("toothfairy")]

	[Bindable]
	public class Genders
	{
		public static const MALE:int = 0;
		public static const FEMALE:int = 1;
		
		public static var list:ArrayList = new ArrayList([
			{id:0 , label:ResourceManager.getInstance().getString('toothfairy', 'MaleLabel')}, 
			{id:1 , label:ResourceManager.getInstance().getString('toothfairy', 'FemaleLabel')}
			
		]);
		
	}
}