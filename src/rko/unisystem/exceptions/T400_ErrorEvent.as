package rko.unisystem.exceptions
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;

	public class T400_ErrorEvent extends Event
	{
		public var message:String;
		
		private var e1_message:String = "Ошибка канала связи РРО.\nНет возможности запустить команду.";
		private var e2_message:String = "Ошибка канала связи РРО.\nНевозможно обработать команду.";
		private var e3_message:String = "Ошибка канала связи РРО.\nКод команды отсутствует.";
		private var e4_message:String = "Ошибка канала связи РРО.\nМного данных в команде.";
		private var e5_message:String = "Ошибка канала связи РРО.\nНедостаточно данных в команде.";
		private var e6_message:String = "Ошибка канала связи РРО.\nОшибка при приеме данных.";
		private var e7_message:String = "Ошибка канала связи РРО.\nНедопустимый идентификатор команды.";
		private var e8_message:String = "Ошибка канала связи РРО.\nНевозможно выполнить команду.";
		private var e10_message:String = "Ошибка канала связи РРО.\nОшибка ввода.";
		
		private var e11_message:String = "Ошибка обработки команды РРО.\nНеобходимо снять z1 отчет.";
		private var e12_message:String = "Ошибка обработки команды РРО.\nСкидки/наценки запрещены.";
		private var e13_message:String = "Ошибка обработки команды РРО.\nПереполнение по чеку.";
		private var e14_message:String = "Ошибка обработки команды РРО.\nКоманда запрещена.";
		private var e15_message:String = "Ошибка обработки команды РРО.\nКассир не зарегистрирован.";
		private var e16_message:String = "Ошибка обработки команды РРО.\nОтрицательная сумма.";
		private var e17_message:String = "Ошибка обработки команды РРО.\nКоличество товара отрицательное.";
		private var e18_message:String = "Ошибка обработки команды РРО.\nВремя смены исчерпано.";
		private var e19_message:String = "Ошибка обработки команды РРО.\nНеверный тип оплаты.";
		private var e20_message:String = "Ошибка обработки команды РРО.\nНеправильная или отсутствующая цена.";
		private var e21_message:String = "Ошибка обработки команды РРО.\nНеверный параметр на входе команды.";
		private var e22_message:String = "Ошибка обработки команды РРО.\nТовар находится в открытом чеке, нельзя редактировать.";
		private var e23_message:String = "Ошибка обработки команды РРО.\nНекорректно запрограммированный товар.";
		private var e24_message:String = "Ошибка обработки команды РРО.\nНеверный или отсутствующий штрих-код товара.";
		private var e27_message:String = "Ошибка обработки команды РРО.\nНеверный или отсутствующий код товара.";
		private var e28_message:String = "Ошибка обработки команды РРО.\nТовар не весовой(штучный).";
		private var e29_message:String = "Ошибка обработки команды РРО.\nФП почти заполнена.";
		private var e30_message:String = "Ошибка обработки команды РРО.\nФП заполнена.";
		private var e31_message:String = "Ошибка обработки команды РРО.\nПамять инициализаций заполнена.";
		private var e32_message:String = "Ошибка обработки команды РРО.\nЕсть отложенная операция, оплата запрещена.";
		private var e33_message:String = "Ошибка обработки команды РРО.\nКарточка клиента не принята.";
		private var e34_message:String = "Ошибка обработки команды РРО.\nНе хватает денег на сдачу.";
		private var e35_message:String = "Ошибка обработки команды РРО.\nЗапрещена комбинированная оплата.";
		private var e36_message:String = "Ошибка обработки команды РРО.\nНеправильный номер кассира.";
		private var e37_message:String = "Ошибка обработки команды РРО.\nМеста недостаточно.";
		private var e38_message:String = "Ошибка обработки команды РРО.\nНет места в Журнале.";
		private var e39_message:String = "Ошибка обработки команды РРО.\nНет места в базе товаров.";
		private var e40_message:String = "Ошибка обработки команды РРО.\nНет места в Архиве.";
		private var e41_message:String = "Ошибка обработки команды РРО.\nЗапрещено, товар является комплексом.";
		private var e42_message:String = "Ошибка обработки команды РРО.\nКод не принадлежит комплексу.";
		private var e43_message:String = "Ошибка обработки команды РРО.\nЭККА занят и не может выполнить команду.";
		private var e44_message:String = "Ошибка обработки команды РРО.\nНеобходимо снять 501 отчет.";
		private var e45_message:String = "Ошибка обработки команды РРО.\nНеправильный пароль кассира.";
		private var e46_message:String = "Ошибка обработки команды РРО.\nКомплекс невозможно продать.";
		private var e47_message:String = "Ошибка обработки команды РРО.\nЦена товара определена.";
		private var e48_message:String = "Ошибка обработки команды РРО.\nОтмена запрещена.";
		private var e49_message:String = "Ошибка обработки команды РРО.\nПродажа товара запрещена.";
		private var e50_message:String = "Ошибка обработки команды РРО.\nОшибка чтения ФП.";
		private var e51_message:String = "Ошибка обработки команды РРО.\nНомер производителя неверен.";
		private var e52_message:String = "Ошибка обработки команды РРО.\nОшибка записи во флеш.";
		private var e54_message:String = "Ошибка обработки команды РРО.\nУдаление товара запрещено.";
		private var e55_message:String = "Ошибка обработки команды РРО.\nНет данных в фискальной памяти.";
		private var e56_message:String = "Ошибка обработки команды РРО.\nНеправильный пароль налогового инспектора.";
		private var e57_message:String = "Ошибка обработки команды РРО.\nНе готов выполнить команду.";
		private var e58_message:String = "Ошибка обработки команды РРО.\nНеправильный пароль администратора.";
		private var e60_message:String = "Ошибка обработки команды РРО.\nРРО заблокирован (72 часа не было передачи данных).";
		private var e61_message:String = "Ошибка обработки команды РРО.\nРРО не персонализован.";
		private var e70_message:String = "Ошибка обработки команды РРО.\nОтсутсвует вал термоголовки.";
		private var e79_message:String = "Ошибка обработки команды РРО.\nДата сервисного обслуживания превышена.";
		private var e80_message:String = "Ошибка обработки команды РРО.\nОшибка записи в ФП.";
		private var e81_message:String = "Ошибка обработки команды РРО.\nОшибка часов.";
		private var e82_message:String = "Ошибка обработки команды РРО.\nОшибка данных в интерфейсе.";
		private var e86_message:String = "Ошибка обработки команды РРО.\nОтсутствует индикатор клиента.";
		private var e97_message:String = "Ошибка обработки команды РРО.\nОтсутствует бумага.";
		
		private var e300_message:String = "Ошибка DLL-библиотеки.\nНеверная команда.";
		private var e301_message:String = "Ошибка DLL-библиотеки.\nОшибка порта.";
		private var e302_message:String = "Ошибка DLL-библиотеки.\nНеверные параметры.";
		private var e303_message:String = "Ошибка DLL-библиотеки.\nНеверное количество параметров.";
		private var e304_message:String = "Ошибка DLL-библиотеки.\nОшибка данных.";
		private var e305_message:String = "Ошибка DLL-библиотеки.\nПорт закрыт.";
		private var e306_message:String = "Ошибка DLL-библиотеки.\nНет данных.";
		private var e307_message:String = "Ошибка DLL-библиотеки.\nНе поддерживается.";
		private var e308_message:String = "Ошибка DLL-библиотеки.\nКасса не доступна.";
		private var e309_message:String = "Ошибка DLL-библиотеки.\nНеизвестный код ошибки.";
		private var e310_message:String = "Ошибка DLL-библиотеки.\nЗапись запрещена.";
		private var e311_message:String = "Ошибка DLL-библиотеки.\nОшибка чтения.";
		private var e312_message:String = "Ошибка DLL-библиотеки.\nОшибка записи.";
		private var e313_message:String = "Ошибка DLL-библиотеки.\nНеправильная запись НДС.";
		private var e314_message:String = "Ошибка DLL-библиотеки.\nОперация отменена.";
		private var e315_message:String = "Ошибка DLL-библиотеки.\nНеправильная модель кассового аппарата.";
		private var e316_message:String = "Ошибка DLL-библиотеки.\nДанные не существуют.";
		private var e317_message:String = "Ошибка DLL-библиотеки.\nКассовый аппарат отключен.";
		private var e318_message:String = "Ошибка DLL-библиотеки.\nФайл не найден.";
		private var e319_message:String = "Ошибка DLL-библиотеки.\nПрисутствует ошибка.";
		private var e320_message:String = "Ошибка DLL-библиотеки.\nНеверные данные.";
		private var e321_message:String = "Ошибка DLL-библиотеки.\nКоманда отключена.";
		private var e322_message:String = "Ошибка DLL-библиотеки.\nБудет доступно в следующих версиях.";
		private var e323_message:String = "Ошибка DLL-библиотеки.\nПринтер не готов.";
		
		private var e700_message:String = "Системная ошибка.\nНеверное количество параметров библиотеки.";
		private var e701_message:String = "Системная ошибка.\nOEL не зарегистрирован.";
		private var e702_message:String = "Системная ошибка.\nОшибка открытия порта.";
		private var e703_message:String = "Системная ошибка.\nОшибка закрытия портаю";
		
		
		public function T400_ErrorEvent(data:String)
		{
			super(T400_Events.ERROR, true, false);
			switch(int(data))
			{
				
				case 1:
				{
					message = e1_message;
					break;
				}
				case 2:
				{
					message = e2_message;
					break;
				}
				case 3:
				{
					message = e3_message;
					break;
				}
				case 4:
				{
					message = e4_message;
					break;
				}
				case 5:
				{
					message = e5_message;
					break;
				}
				case 6:
				{
					message = e6_message;
					break;
				}
				case 7:
				{
					message = e7_message;
					break;
				}
				case 8:
				{
					message = e8_message;
					break;
				}
				case 10:
				{
					message = e10_message;
					break;
				}
				case 11:
				{
					message = e11_message;
					break;
				}
				case 12:
				{
					message = e12_message;
					break;
				}
				case 13:
				{
					message = e13_message;
					break;
				}
				case 14:
				{
					message = e14_message;
					break;
				}
				case 15:
				{
					message = e15_message;
					break;
				}
				case 16:
				{
					message = e16_message;
					break;
				}
				case 17:
				{
					message = e17_message;
					break;
				}
				case 18:
				{
					message = e18_message;
					break;
				}
				case 19:
				{
					message = e19_message;
					break;
				}
				case 20:
				{
					message = e20_message;
					break;
				}
				case 21:
				{
					message = e21_message;
					break;
				}
				case 22:
				{
					message = e22_message;
					break;
				}
				case 23:
				{
					message = e23_message;
					break;
				}
				case 24:
				{
					message = e24_message;
					break;
				}
				case 27:
				{
					message = e27_message;
					break;
				}
				case 28:
				{
					message = e28_message;
					break;
				}
				case 29:
				{
					message = e29_message;
					break;
				}
				case 30:
				{
					message = e30_message;
					break;
				}
				case 31:
				{
					message = e31_message;
					break;
				}
				case 32:
				{
					message = e32_message;
					break;
				}
				case 33:
				{
					message = e33_message;
					break;
				}
				case 34:
				{
					message = e34_message;
					break;
				}
				case 35:
				{
					message = e35_message;
					break;
				}
				case 36:
				{
					message = e36_message;
					break;
				}
				case 37:
				{
					message = e37_message;
					break;
				}
				case 38:
				{
					message = e38_message;
					break;
				}
				case 39:
				{
					message = e39_message;
					break;
				}
				case 40:
				{
					message = e40_message;
					break;
				}
				case 41:
				{
					message = e41_message;
					break;
				}
				case 42:
				{
					message = e42_message;
					break;
				}
				case 43:
				{
					message = e43_message;
					break;
				}
				case 44:
				{
					message = e44_message;
					break;
				}
				case 45:
				{
					message = e45_message;
					break;
				}
				case 46:
				{
					message = e46_message;
					break;
				}
				case 47:
				{
					message = e47_message;
					break;
				}
				case 48:
				{
					message = e48_message;
					break;
				}
				case 49:
				{
					message = e49_message;
					break;
				}
				case 50:
				{
					message = e50_message;
					break;
				}
				case 51:
				{
					message = e51_message;
					break;
				}
				case 52:
				{
					message = e52_message;
					break;
				}
				case 54:
				{
					message = e54_message;
					break;
				}
				case 55:
				{
					message = e55_message;
					break;
				}
				case 56:
				{
					message = e56_message;
					break;
				}
				case 57:
				{
					message = e57_message;
					break;
				}
				case 58:
				{
					message = e58_message;
					break;
				}
				case 60:
				{
					message = e60_message;
					break;
				}
				case 61:
				{
					message = e61_message;
					break;
				}
				case 70:
				{
					message = e70_message;
					break;
				}
				case 79:
				{
					message = e79_message;
					break;
				}
				case 80:
				{
					message = e80_message;
					break;
				}
				case 81:
				{
					message = e81_message;
					break;
				}
				case 82:
				{
					message = e82_message;
					break;
				}
				case 86:
				{
					message = e86_message;
					break;
				}
				case 97:
				{
					message = e97_message;
					break;
				}
					
					
					
					
				case 300:
				{
					message = e300_message;
					break;
				}
				case 301:
				{
					message = e301_message;
					break;
				}
				case 302:
				{
					message = e302_message;
					break;
				}
				case 303:
				{
					message = e303_message;
					break;
				}
				case 304:
				{
					message = e305_message;
					break;
				}
				case 306:
				{
					message = e306_message;
					break;
				}
				case 307:
				{
					message = e307_message;
					break;
				}
				case 308:
				{
					message = e308_message;
					break;
				}
				case 309:
				{
					message = e309_message;
					break;
				}
				case 310:
				{
					message = e310_message;
					break;
				}
				case 311:
				{
					message = e311_message;
					break;
				}
				case 312:
				{
					message = e312_message;
					break;
				}
				case 313:
				{
					message = e313_message;
					break;
				}
				case 314:
				{
					message = e314_message;
					break;
				}
				case 315:
				{
					message = e315_message;
					break;
				}
				case 316:
				{
					message = e316_message;
					break;
				}
				case 317:
				{
					message = e317_message;
					break;
				}
				case 318:
				{
					message = e318_message;
					break;
				}
				case 319:
				{
					message = e319_message;
					break;
				}
				case 320:
				{
					message = e320_message;
					break;
				}
				case 321:
				{
					message = e321_message;
					break;
				}
				case 322:
				{
					message = e322_message;
					break;
				}
				case 323:
				{
					message = e323_message;
					break;
				}
				
				
				
				case 700:
				{
					message = e700_message;
					break;
				}
				case 701:
				{
					message = e701_message;
					break;
				}
				case 702:
				{
					message = e702_message;
					break;
				}
				case 703:
				{
					message = e703_message;
					break;
				}
					
				default:
				{
					break;
				}
			}
		}
	}
}