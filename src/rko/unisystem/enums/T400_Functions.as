package rko.unisystem.enums
{
	public class T400_Functions
	{
		public static const GET_SOFT_VERSION:String = "get_soft_version;";
		public static const GET_DATE_TIME:String = "get_date_time;";
		public static const GET_HEADER:String = "get_header;";
		public static const SET_HEADER:String = "set_header;";
		public static const CASHIER_REGISTRATION:String = "cashier_registration;";
		public static const ADD_PLU:String = "add_plu;";
		public static const OPEN_RECEIPT:String = "open_receipt;";
		public static const SALE_PLU:String = "sale_plu;";
		public static const PAY:String = "pay;";
		public static const PRINT_EMPTY_RECEIPT:String = "print_empty_receipt;";
		public static const PRINT_RECEIPT_COPY:String = "print_receipt_copy;";
		public static const GET_LAST_RECEIPT_NUMBER:String = "get_last_receipt_number;";
		public static const GET_CASHBOX_SUM_NUMBER:String = "get_cashbox_sum;";
		public static const EXECUTE_X_REPORT:String = "execute_X_report;";
		public static const EXECUTE_Z_REPORT:String = "execute_Z_report;";
		public static const GET_STATUS:String = "get_status;";
		public static const IN_OUT:String = "in_out;";
		public static const CANCEL_RECEIPT:String = "cancel_receipt;";
		public static const DISCOUNT_SURCHARGE:String = "discount_surcharge;";
		public static const DPS:String = "dps;";
		
	}
}