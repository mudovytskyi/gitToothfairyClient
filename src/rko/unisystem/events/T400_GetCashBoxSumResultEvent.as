package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;
	import rko.unisystem.objects.T400_CashBox;

	public class T400_GetCashBoxSumResultEvent extends Event
	{
		public var result:Boolean;
		public var cashbox:T400_CashBox;
		
		public function T400_GetCashBoxSumResultEvent(data:String)
		{
			super(T400_Events.GET_CASHBOX_SUM_NUMBER, true, false);

			var dataArray:Array = (data as String).split(";");
			if(int(dataArray[0]) == 0)
				result = true;
			else
				result = false;
			
			cashbox = new T400_CashBox();
			cashbox.cash = dataArray[1];
			cashbox.check = dataArray[2];
			cashbox.creditCard = dataArray[3];
			
			cashbox.cashType1 = dataArray[4];
			cashbox.cashType2 = dataArray[5];
			cashbox.cashType3 = dataArray[6];
			cashbox.cashType4 = dataArray[7];
			cashbox.cashType5 = dataArray[8];
			
		}
	}
}