package rko.unisystem.events
{
	import flash.events.Event;
	
	import mx.utils.ObjectUtil;
	
	import rko.unisystem.enums.T400_Events;
	import rko.unisystem.objects.T400_Status;
	import rko.unisystem.utils.Utils;

	public class T400_GetStatusResultEvent extends Event
	{
		public var result:Boolean;
		public var status:T400_Status;
		
		public function T400_GetStatusResultEvent(data:String)
		{
			super(T400_Events.GET_STATUS, true, false);

			var dataArray:Array = (data as String).split(";");
			
			if(int(dataArray[0]) == 0)
				result = true;
			else
				result = false;
			
			status = new T400_Status();
			//1
			status._1_cahsierReggedNumber = int(dataArray[1]);
			//2
			if(int(dataArray[2]) == 1)
				status._2_isChangeOpen = true;
			else
				status._2_isChangeOpen = false;
			//3	
			status._3_checkStatus = int(dataArray[3]);
			//4
			if(int(dataArray[4]) == 0)
				status._4_is_X2_ReportCleared = true;
			else
				status._4_is_X2_ReportCleared = false;
			//5
			if(int(dataArray[5]) == 0)
				status._5_is_X3_ReportCleared = true;
			else
				status._5_is_X3_ReportCleared = false;
			//6
			if(int(dataArray[6]) == 0)
				status._6_is_X5_ReportCleared = true;
			else
				status._6_is_X5_ReportCleared = false;
			//7
			if(int(dataArray[7]) == 0)
				status._7_is_X6_ReportCleared = true;
			else
				status._7_is_X6_ReportCleared = false;
			//8
			if(int(dataArray[8]) == 0)
				status._8_isCheckoutTapePrinted = true;
			else
				status._8_isCheckoutTapePrinted = false;
			//9
			if(int(dataArray[9]) == 1)
				status._9_isChange23H = true;
			else
				status._9_isChange23H = false;
			//10
			if(int(dataArray[10]) == 1)
				status._10_isChange24H = true;
			else
				status._10_isChange24H = false;
			//11-12
			status._11_changeStartDate = Utils.dateParser(dataArray[11]+" "+dataArray[12]);
			//13
			status._13_changeNumber = int(dataArray[13]);
			//14
			status._14_changeNumberCurrent = int(dataArray[14]);
			//15
			status._15_checkLastNumber_InCurrentChange = int(dataArray[15]);
			//16
			status._16_checkLastNumber_InPrevChange = int(dataArray[16]);
			//17
			if(status._1_cahsierReggedNumber > 0)
				status._17_countUsedGoodsRecords = int(dataArray[17]);
			else
				status._17_countUsedGoodsRecords = -1;
			//18
			if(status._1_cahsierReggedNumber > 0)
				status._18_countMaxGoodsRecords = int(dataArray[18]);
			else
				status._18_countMaxGoodsRecords = -1;
			//19
			status._19_countAuthorizedCashiers = int(dataArray[19]);
			//20
			status._20_recordNumber = int(dataArray[20]);
			//21
			if(int(dataArray[21]) == 1)
				status._21_isBlockPoint72H = true;
			else
				status._21_isBlockPoint72H = false;
			//22
			if(int(dataArray[22]) == 1)
				status._22_isBlockExistAuthorization = true;
			else
				status._22_isBlockExistAuthorization = false;
			//23-24
			status._23_blockPoint72HDate = Utils.dateParser(dataArray[23]+" "+dataArray[24]);
			status._23_block72HDate = ObjectUtil.copy(status._23_blockPoint72HDate) as Date;
			status._23_block72HDate.date += 3;
			//25
			status._25_packsCount = int(dataArray[25]);
			//26
			status._26_packsCountEKKR = int(dataArray[26]);
			//27
			if(int(dataArray[27]) == 0)
				status._27_isPersonalized = true;
			else
				status._27_isPersonalized = false;
			//28
			status._28_id_sam = int(dataArray[28]);
			//29
			status._29_id_dev = int(dataArray[29]);
			
			/*status.cashType2 = dataArray[5];
			status.cashType3 = dataArray[6];
			status.cashType4 = dataArray[7];
			status.cashType5 = dataArray[8];
			status.cashType5 = dataArray[9];
			status.cashType5 = dataArray[10];
			status.cashType5 = dataArray[11];
			status.cashType5 = dataArray[12];
			status.cashType5 = dataArray[13];
			status.cashType5 = dataArray[14];
			status.cashType5 = dataArray[15];
			status.cashType5 = dataArray[16];
			status.cashType5 = dataArray[17];
			status.cashType5 = dataArray[18];
			status.cashType5 = dataArray[19];
			status.cashType5 = dataArray[20];
			status.cashType5 = dataArray[21];
			status.cashType5 = dataArray[22];
			status.cashType5 = dataArray[23];
			status.cashType5 = dataArray[24];
			status.cashType5 = dataArray[25];
			status.cashType5 = dataArray[26];*/
			
		}
	}
}