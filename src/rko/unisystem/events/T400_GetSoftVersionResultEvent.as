package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;
	import rko.unisystem.objects.T400_SoftVersion;
	
	public class T400_GetSoftVersionResultEvent extends Event
	{
		public var result:T400_SoftVersion;
		
		public function T400_GetSoftVersionResultEvent(data:String)
		{
			super(T400_Events.GET_SOFT_VERSION, true, false);
			
			var dataArray:Array = (data as String).split(";");
			
			var _result:T400_SoftVersion = new T400_SoftVersion();
			_result.result = dataArray[0] as int;
			_result.version = dataArray[1] as String;
			
			this.result = _result;
		}
	}
}

