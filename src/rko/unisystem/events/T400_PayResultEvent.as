package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;

	public class T400_PayResultEvent extends Event
	{
		public var result:Boolean;
		public var rest:Number;
		
		public function T400_PayResultEvent(data:String)
		{
			super(T400_Events.PAY, true, false);
			var dataArray:Array = (data as String).split(";");
			if(int(dataArray[0]) == 0)
				result = true;
			else
				result = false;
			
			rest = Number(dataArray[1]);
		}
	}
}