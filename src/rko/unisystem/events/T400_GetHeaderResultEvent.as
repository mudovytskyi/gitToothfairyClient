package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;
	import rko.unisystem.objects.T400_Header;
	import rko.unisystem.objects.T400_HeaderItem;

	public class T400_GetHeaderResultEvent extends Event
	{
		public var result:T400_Header;
		
		public function T400_GetHeaderResultEvent(data:String)
		{
			result = new T400_Header();
			super(T400_Events.GET_HEADER, true, false);
			
			var dataArray:Array = (data as String).split(";");
			
			
			var headerItem:T400_HeaderItem;
			var j:int = 1;
			for (var i:int=0; i<dataArray.length;i++)
			{ 
				if(i == 0)
				{
					result.result = int(dataArray[0]);
				}
				else if(i%3 == 1)
				{
					headerItem = new T400_HeaderItem(dataArray[i]);
				}
				else if(i%3 == 2)
				{
					headerItem.fontStyle = int(dataArray[i]);
				}
				else
				{
					if(int(dataArray[i]) == 0)
						headerItem.isPrint = false;
					else
						headerItem.isPrint = true;
					headerItem.index = j;
					j++;
					result.headers.push(headerItem);
				}
			}
		}
	}
}