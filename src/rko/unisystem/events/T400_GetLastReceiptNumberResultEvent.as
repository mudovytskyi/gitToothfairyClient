package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;

	public class T400_GetLastReceiptNumberResultEvent extends Event
	{
		public var result:Boolean;
		public var number:int;
		
		public function T400_GetLastReceiptNumberResultEvent(data:String)
		{
			super(T400_Events.GET_LAST_RECEIPT_NUMBER, true, false);
			var dataArray:Array = (data as String).split(";");
			if(int(dataArray[0]) == 0)
				result = true;
			else
				result = false;
			number = int(dataArray[1]);
		}
	}
}