package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;
	import rko.unisystem.objects.T400_DateTime;

	public class T400_GetDateTimeResultEvent extends Event
	{
		public var result:T400_DateTime;
		
		public function T400_GetDateTimeResultEvent(data:String)
		{
			super(T400_Events.GET_DATE_TIME, true, false);
			
			var dataArray:Array = (data as String).split(";");
			
			var _result:T400_DateTime = new T400_DateTime();
			_result.result = dataArray[0] as int;
			_result.date = dataArray[1] as String;
			_result.time = dataArray[2] as String;
				
			this.result = _result;
		}
	}
}