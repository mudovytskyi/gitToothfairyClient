package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;

	public class T400_SalePluResultEvent extends Event
	{
		public var result:Boolean;
		
		public function T400_SalePluResultEvent(data:String)
		{
			super(T400_Events.SALE_PLU, true, false);
			var dataArray:Array = (data as String).split(";");
			if(int(dataArray[0]) == 0)
				result = true;
			else
				result = false;
		}
	}
}