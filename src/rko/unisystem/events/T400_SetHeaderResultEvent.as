package rko.unisystem.events
{
	import flash.events.Event;
	
	import rko.unisystem.enums.T400_Events;

	public class T400_SetHeaderResultEvent extends Event
	{
		public var result:Boolean;
		
		public function T400_SetHeaderResultEvent(data:String)
		{
			super(T400_Events.SET_HEADER, true, false);
			var dataArray:Array = (data as String).split(";");
			if(int(dataArray[0]) == 0)
				result = true;
			else
				result = false;
		}
	}
}