package rko.unisystem.utils
{
	public class Utils
	{
		public static function dateParser(s:String):Date{
			var regexp:RegExp = /(\d{1,2})[\/|\.](\d{1,2})[\/|\.](\d{4}) (\d{2})\:(\d{2})\:(\d{2})/;
			//var regexp:RegExp = /(\d{4})\-(\d{1,2})\-(\d{1,2}) (\d{2})\:(\d{2})\:(\d{2})/;
			var _result:Object = regexp.exec(s);
			
			return new Date(
				parseInt(_result[3]),
				parseInt(_result[2])-1,
				parseInt(_result[1]),
				parseInt(_result[4]),
				parseInt(_result[5]),
				parseInt(_result[6])
			);
		}
		
		
		public static function clearDelimeters(formattedString:String):String {     
			return formattedString.replace(/[\u000d\u000a\u0008\u0020]+/g,""); 
		}
	}
}