package rko.unisystem.objects
{
	[Bindable]
	public class T400_Status
	{
		public var result:int;
		
		
		public var _1_cahsierReggedNumber:int;
		public var _2_isChangeOpen:Boolean;
		/*
		0 - чек закрыт
		1 - чек открыт для продажи
		2 - чек открыт только для оплаты
		3 - чек открыт для возврата
		*/
		public var _3_checkStatus:int;
		public var _4_is_X2_ReportCleared:Boolean;
		public var _5_is_X3_ReportCleared:Boolean;
		public var _6_is_X5_ReportCleared:Boolean;
		public var _7_is_X6_ReportCleared:Boolean;
		public var _8_isCheckoutTapePrinted:Boolean;
		public var _9_isChange23H:Boolean;
		public var _10_isChange24H:Boolean;
		public var _11_changeStartDate:Date;
		public var _13_changeNumber:int;
		public var _14_changeNumberCurrent:int;
		public var _15_checkLastNumber_InCurrentChange:int;
		public var _16_checkLastNumber_InPrevChange:int;
		public var _17_countUsedGoodsRecords:int;
		public var _18_countMaxGoodsRecords:int;
		public var _19_countAuthorizedCashiers:int;
		public var _20_recordNumber:int;
		public var _21_isBlockPoint72H:Boolean;
		public var _22_isBlockExistAuthorization:Boolean;
		
		
		public var _23_blockPoint72HDate:Date;
		public var _23_block72HDate:Date;
		public var _25_packsCount:int;
		public var _26_packsCountEKKR:int;
		public var _27_isPersonalized:Boolean;
		
		public var _28_id_sam:int;
		public var _29_id_dev:int;
		
		public function get IsChangeOpenLabel():String
		{
			if(this._2_isChangeOpen)
			{
				return "открыта";
			}
			else
			{
				return "закрыта";
			}
		}
		
		
		public function get IsChange23HLabel():String
		{
			if(this._9_isChange23H)
			{
				return "да";
			}
			else
			{
				return "нет";
			}
		}
		
		public function get CheckStatusLabel():String
		{
			switch(this._3_checkStatus)
			{
				case 0:
				{
					return "закрыт";
					break;
				}
				case 1:
				{
					return "открыт для продажи";
					break;
				}
				case 2:
				{
					return "открыт только для оплаты";
					break;
				}
				case 3:
				{
					return "открыт для возврата";
					break;
				}
				default:
				{
					return "";
					break;
				}
			}
		}
	}
}