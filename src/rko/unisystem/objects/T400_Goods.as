package rko.unisystem.objects
{
	public class T400_Goods
	{
		public var result:int;
		
		// 1 - 999999
		public var code:int = 1;
		/*
		0 - без НДС
		1 - А
		2 - Б
		3 - В
		4 - Г
		5 - Д
		*/
		public var taxRate:int = 0;
		public var isWeight:Boolean = false;
		public var isBanSell:Boolean = false;
		public var isBanCountCalculation:Boolean = true;
		public var isBanOnceSell:Boolean = false;
		// 1 - 64
		public var departmentIndex:int = 1;
		public var price:Number = 100;
		public var barcode:int = 0;
		// 48 chars
		public var label:String;
		public var count:Number=0;

		
		
		public function T400_Goods()
		{
		}
	}
}