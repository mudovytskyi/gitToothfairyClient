package rko.unisystem.objects
{
	[Bindable]
	public class T400_CashBox
	{
		public var result:int;
		
		public var cash:Number;
		public var check:Number;
		public var creditCard:Number;
		
		public var cashType1:Number;
		public var cashType2:Number;
		public var cashType3:Number;
		public var cashType4:Number;
		public var cashType5:Number;
	}
}