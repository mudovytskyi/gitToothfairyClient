package rko.unisystem.objects
{
	public class T400_Header
	{
		public var result:int;
		public var headers:Vector.<T400_HeaderItem>;
		
		public function T400_Header()
		{
			headers = new Vector.<T400_HeaderItem>();
		}
	}
}