package rko.unisystem.objects
{
	public class T400_HeaderItem
	{
		
		public var index:int;
		public var label:String;
		/*
		0 - normal
		1 - bold
		2 - normal double height
		3 - bold double height
		*/
		public var fontStyle:int;
		public var isPrint:Boolean;
		/*
		public function T400_HeaderItem()
		{
			label = '';
			fontStyle = 0;
			isPrint = false;
		}
		*/
		public function T400_HeaderItem(label:String = '', fontStyle:int = 0, isPrint:Boolean = false)
		{
			this.label = label;
			this.fontStyle = fontStyle;
			this.isPrint = isPrint;
		}
	}
}