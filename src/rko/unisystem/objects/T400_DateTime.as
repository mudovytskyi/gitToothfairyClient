package rko.unisystem.objects
{
	import rko.unisystem.utils.Utils;

	public class T400_DateTime
	{
		public var result:int;
		public var date:String;
		public var time:String;
		
		public function get FlexDate ():Date
		{
			return Utils.dateParser(date+" "+time); 
		}
	}
}