package rko.unisystem
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	
	import mx.controls.Alert;
	
	import rko.unisystem.enums.T400_Functions;
	import rko.unisystem.events.T400_AddPluResultEvent;
	import rko.unisystem.events.T400_CancelReceiptResultEvent;
	import rko.unisystem.events.T400_CashierRegistrationResultEvent;
	import rko.unisystem.events.T400_DPSResultEvent;
	import rko.unisystem.events.T400_DiscountResultEvent;
	import rko.unisystem.events.T400_ExecuteXReportResultEvent;
	import rko.unisystem.events.T400_ExecuteZReportResultEvent;
	import rko.unisystem.events.T400_GetCashBoxSumResultEvent;
	import rko.unisystem.events.T400_GetDateTimeResultEvent;
	import rko.unisystem.events.T400_GetHeaderResultEvent;
	import rko.unisystem.events.T400_GetLastReceiptNumberResultEvent;
	import rko.unisystem.events.T400_GetSoftVersionResultEvent;
	import rko.unisystem.events.T400_GetStatusResultEvent;
	import rko.unisystem.events.T400_InOutResultEvent;
	import rko.unisystem.events.T400_OpenReceiptResultEvent;
	import rko.unisystem.events.T400_PayResultEvent;
	import rko.unisystem.events.T400_PrintEmptyReceiptResultEvent;
	import rko.unisystem.events.T400_PrintReceiptCopyResultEvent;
	import rko.unisystem.events.T400_SalePluResultEvent;
	import rko.unisystem.exceptions.T400_ErrorEvent;
	import rko.unisystem.objects.T400_Goods;
	import rko.unisystem.objects.T400_HeaderItem;
	import rko.unisystem.utils.Utils;
	
	
	public class T400ME extends EventDispatcher
	{
		public static var PORT_NUMBER:String;
		
		private var libraryProcess:NativeProcess;
		
		private var library:File;
		
		private var portNumber:String;
		private var portSpeed:String;
		
		private var inProcess:Boolean;
		private var lastOperation:String;
		
		public function T400ME(libraryPath:String, portNumber:String = "3", portSpeed:String = "115200", target:IEventDispatcher=null)
		{
			if(PORT_NUMBER != null)
				portNumber = PORT_NUMBER;
			inProcess = false;
			this.portNumber = portNumber;
			this.portSpeed = portSpeed;
			library = new File(libraryPath);
			
			super(target);
		}
		
		// 4 START       get_soft_result
		public function GetSoftVersion():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.GET_SOFT_VERSION;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation);
			}
		}
		// 4 END         get_soft_result
		// 8 START       get_date_time
		public function GetDateTime():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.GET_DATE_TIME;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation);
			}
		}
		// 8 END         get_date_time
		// 12 START       set_header
		public function SetHeader(header:T400_HeaderItem):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.SET_HEADER;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation + 
					header.index + ";" + 
					header.label + ";" +
					header.fontStyle + ";" +
					((header.isPrint) ? 1 : 0) + ";" );
			}
		}
		// 12 END         set_header
		// 13 START       get_header
		public function GetHeader():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.GET_HEADER;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation);
			}
		}
		// 13 END         get_header
		// 17 START       cashier_registration
		public function CashierRegistration(cashierNumber:int = 0, cashierPass:int = 0):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.CASHIER_REGISTRATION;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation+cashierNumber+";"+cashierPass+";");
			}
		}
		// 17 END         cashier_registration
		// 18 START       add_plu
		public function AddPlu(goods:T400_Goods):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.ADD_PLU;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation+
					goods.code+";"+
					goods.taxRate+";"+
					((goods.isWeight) ? 1 : 0) +";"+
					((goods.isBanSell) ? 1 : 0) +";"+
					((goods.isBanCountCalculation) ? 1 : 0) +";"+
					((goods.isBanOnceSell) ? 0 : 1) +";"+
					goods.departmentIndex+";"+
					goods.price.toFixed(2)+";"+
					goods.barcode+";"+
					goods.label+";"+
					goods.count+";");
			}
		}
		// 18 END         add_plu
		// 27 START       open_receipt
		public function OpenReceipt(isReturn:Boolean = false):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.OPEN_RECEIPT;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation + ((isReturn) ? 1 : 0) + ";");
			}
		}
		// 27 END         open_receipt
		// 28 START       sale_plu
		public function SalePlu(	code:int,
									count:Number,
									price:Number,
										isRemove:Boolean = false,
										saleByBarcode:Boolean = false,
										saleByProgrammedPrice:Boolean = false):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.SALE_PLU;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation+
					((isRemove) ? 1 : 0) +";"+
					((saleByBarcode) ? 1 : 0) +";"+
					((saleByProgrammedPrice) ? 0 : 1) +";"+
					count.toFixed(3) +";"+
					code+";"+
					price.toFixed(2)+";");
			}
		}
		// 28 END         sale_plu
		// 30 START       pay
		/*
		0 - наличные
		1 - чек
		2 - магнитная карта
		3-7 - пользовательские
		*/
		public function Pay(payType:int = 0, summ:Number = 0):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.PAY;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation+
						payType +";"+
						summ.toFixed(2)+";");
			}
		}
		// 30 END         pay
		// 35 START       print_empty_receipt
		public function PrintEmptyReceipt():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.PRINT_EMPTY_RECEIPT;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation);
			}
		}
		// 35 END         print_empty_receipt
		// 36 START       print_receipt_copy
		public function PrintReceiptCopy():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.PRINT_RECEIPT_COPY;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation);
			}
		}
		// 36 END         print_receipt_copy
		// 39 START       get_last_receipt_number
		public function GetLastReceiptNumber():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.GET_LAST_RECEIPT_NUMBER;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation);
			}
		}
		// 39 END         get_last_receipt_number
		// 40 START       get_cashbox_sum
		public function GetCashboxSum():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.GET_CASHBOX_SUM_NUMBER;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation);
			}
		}
		// 40 END         get_cashbox_sum
		// 52 START       execute_X_report
		public function ExecuteXReport(mainCashierPass:int):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.EXECUTE_X_REPORT;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation + mainCashierPass +";");
			}
		}
		// 52 END         execute_X_report
		// 53 START       execute_Z_report
		public function ExecuteZReport(mainCashierPass:int):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.EXECUTE_Z_REPORT;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation + mainCashierPass +";");
			}
		}
		// 53 END         execute_Z_report
		// 7 START       get_status
		public function GetStatus():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.GET_STATUS;
				libraryProcess = new NativeProcess();
				execute(libraryProcess, lastOperation);
			}
		}
		// 7 END         get_status
		// 26 START       in_out
		// comment lenght 113
		// operation lenght 19
		// 
		public function InOut(payType:int, summ:Number, operation:String = "", comment:String = ""):void
		{
			if(!inProcess)
			{
				var textReplacingFlag:int = 0;
				var commentFlag:int = 0;
				var _servicePay:int = 0;
				if(summ > 0)
					_servicePay = 0;
				else
					_servicePay = 1;
				
				inProcess = true;
				lastOperation = T400_Functions.IN_OUT;
				libraryProcess = new NativeProcess();
				if(comment.length > 0)
				{
					textReplacingFlag = 1;
					if(comment.length <= 56)
						commentFlag=1;
					else if(comment.length <= 112)
						commentFlag=2;
					else
						commentFlag=2;
				}
				
				execute(libraryProcess, lastOperation + payType.toString() + ";" + textReplacingFlag + ";" + commentFlag + ";" + _servicePay + ";" +
								Math.abs(summ).toFixed(2) + ";" + comment + ";" + operation +";");
			}
		}
		// 26 END         in_out
		// 34 START       cancel_receipt
		public function CancelReceipt():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.CANCEL_RECEIPT;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation);
			}
		}
		// 34 END         cancel_receipt
		// 29 START       discount_surcharge
		public function Discount(discount:Number, isPrecent:Boolean = true, isDiscount:Boolean = true, isGeneral:Boolean = false):void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.DISCOUNT_SURCHARGE;
				libraryProcess = new NativeProcess();
				
				var _isPrecent:String = "1";
				if(isPrecent == false)
					_isPrecent = "0";
				
				var _isGeneral:String = "0";
				if(isGeneral == true)
					_isGeneral = "1";
				
				var _isDiscount:String = "1";
				if(isDiscount == false)
					_isDiscount = "0";
				
				trace(lastOperation + _isPrecent + ";" + _isGeneral + ";" + _isDiscount + ";" + discount.toFixed(2) + ";");
				execute(libraryProcess, lastOperation + _isPrecent + ";" + _isGeneral + ";" + _isDiscount + ";" + discount.toFixed(2) + ";");
			}
		}
		// 29 END         discount_surcharge
		// 82 START       cancel_receipt
		public function SendToDPS():void
		{
			if(!inProcess)
			{
				inProcess = true;
				lastOperation = T400_Functions.DPS;
				libraryProcess = new NativeProcess();
				
				execute(libraryProcess, lastOperation + "2;");
			}
		}
		// 82 END         cancel_receipt
		
		// EXECUTE PART
		public function execute(process:NativeProcess, command:String):void
		{
			if(command == null)
				return;
			
			// Start the process
			try
			{
				var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
				nativeProcessStartupInfo.executable = library;
				var processArgs:Vector.<String> = new Vector.<String>();
				processArgs[0] = portNumber;
				processArgs[1] = portSpeed;
				processArgs[2] = command;
				nativeProcessStartupInfo.arguments = processArgs;
				process = new NativeProcess();
				process.start(nativeProcessStartupInfo);
				process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA,
					outputDataHandler);
				process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA,
					errorOutputDataHandler);
			}
			catch (e:Error)
			{
				Alert.show(e.message, "Error");
			}
		}
		public function outputDataHandler(event:ProgressEvent):void
		{
			var process:NativeProcess = event.target as NativeProcess;
			var data:String = process.standardOutput.readUTFBytes(process.standardOutput.bytesAvailable);
			/*
			result - R| data
			error  - E| error code
			*/
			inProcess = false;
			var outputArray:Array = data.split("|");
			if(outputArray[0] == "R")
			{
				var resultString:String = Utils.clearDelimeters(outputArray[1]);
				var resultEvent:Event;
				switch(lastOperation)
				{
					case T400_Functions.GET_SOFT_VERSION:
					{
						resultEvent = new T400_GetSoftVersionResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.GET_DATE_TIME:
					{
						resultEvent = new T400_GetDateTimeResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.GET_HEADER:
					{
						resultEvent = new T400_GetHeaderResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.CASHIER_REGISTRATION:
					{
						resultEvent = new T400_CashierRegistrationResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.ADD_PLU:
					{
						resultEvent = new T400_AddPluResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.OPEN_RECEIPT:
					{
						resultEvent = new T400_OpenReceiptResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.SALE_PLU:
					{
						resultEvent = new T400_SalePluResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.PAY:
					{
						resultEvent = new T400_PayResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.PRINT_EMPTY_RECEIPT:
					{
						resultEvent = new T400_PrintEmptyReceiptResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.PRINT_RECEIPT_COPY:
					{
						resultEvent = new T400_PrintReceiptCopyResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.GET_LAST_RECEIPT_NUMBER:
					{
						resultEvent = new T400_GetLastReceiptNumberResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.GET_CASHBOX_SUM_NUMBER:
					{
						resultEvent = new T400_GetCashBoxSumResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.EXECUTE_X_REPORT:
					{
						resultEvent = new T400_ExecuteXReportResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.EXECUTE_Z_REPORT:
					{
						resultEvent = new T400_ExecuteZReportResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.GET_STATUS:
					{
						resultEvent = new T400_GetStatusResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.IN_OUT:
					{
						resultEvent = new T400_InOutResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.CANCEL_RECEIPT:
					{
						resultEvent = new T400_CancelReceiptResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.DISCOUNT_SURCHARGE:
					{
						resultEvent = new T400_DiscountResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
					case T400_Functions.DPS:
					{
						resultEvent = new T400_DPSResultEvent(resultString);
						dispatchEvent(resultEvent);
						break;
					}
						
						
					default:
					{
						break;
					}
				}
				
			}
			else if(outputArray[0] == "E")
			{
				var errorCode:String = Utils.clearDelimeters(outputArray[1]);
				var errorEvent:T400_ErrorEvent = new T400_ErrorEvent(errorCode);
				dispatchEvent(errorEvent);
			}
		}
		
		public function errorOutputDataHandler(event:ProgressEvent):void
		{
			var process:NativeProcess = event.target as NativeProcess;
			var data:String = process.standardError.readUTFBytes(libraryProcess.standardError.bytesAvailable);
			//log.text += data;
		}
		
	}
}