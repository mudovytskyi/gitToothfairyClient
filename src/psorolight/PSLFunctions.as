package psorolight
{
	public class PSLFunctions
	{
		public static const DURATION_SYMBOL:String = "T";
		public static const DOZE_SYMBOL:String = "D";
		public static const STOP:String = "0P";
		public static const WORKEDTIME:String = "1N";		
	}
}