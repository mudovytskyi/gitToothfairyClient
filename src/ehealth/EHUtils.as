package ehealth
{
	public class EHUtils
	{
		public static const ERROR_PHONE_VERIFICATION:String = 'The phone number is not verified';
		
		
		public static function getError(error:Object):String
		{
			var errorMessage:String = '';
			var errorIndex:int = 1;
			for each (var invalid:Object in error.invalid) 
			{
				errorMessage += errorIndex+') ';
				errorIndex++;
				for each (var rule:Object in invalid.rules) 
				{
					errorMessage += String(rule.description)+'; ';
				}
				errorMessage += '\n';
			}
			return errorMessage;
		}
	}
}