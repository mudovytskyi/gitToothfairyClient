/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - WarehouseModuleFarmDocument.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.WarehouseModuleDoctor;
import valueObjects.WarehouseModuleFarmFlow;
import valueObjects.WarehouseModuleRoom;
import valueObjects.WarehouseModuleSupplier;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_WarehouseModuleFarmDocument extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("WarehouseModuleFarmDocument") == null)
            {
                flash.net.registerClassAlias("WarehouseModuleFarmDocument", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("WarehouseModuleFarmDocument", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.WarehouseModuleSupplier.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleDoctor.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarmFlow.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarmPrice.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarm.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleTPFarm.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarmUnit.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleRoom.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _WarehouseModuleFarmDocumentEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_RTYPE : String;
    private var _internal_STYPE : String;
    private var _internal_TREATMENTPROCID : String;
    private var _internal_SUPPLIER : valueObjects.WarehouseModuleSupplier;
    private var _internal_SDOCTOR : valueObjects.WarehouseModuleDoctor;
    private var _internal_RDOCTOR : valueObjects.WarehouseModuleDoctor;
    private var _internal_DOCTIMESTAMP : String;
    private var _internal_DOCNUMBER : String;
    private var _internal_SUMM : Number;
    private var _internal_FARMFLOWS : ArrayCollection;
    model_internal var _internal_FARMFLOWS_leaf:valueObjects.WarehouseModuleFarmFlow;
    private var _internal_COMMENT : String;
    private var _internal_ROOM : valueObjects.WarehouseModuleRoom;
    private var _internal_STATUS : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_WarehouseModuleFarmDocument()
    {
        _model = new _WarehouseModuleFarmDocumentEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get RTYPE() : String
    {
        return _internal_RTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get STYPE() : String
    {
        return _internal_STYPE;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROCID() : String
    {
        return _internal_TREATMENTPROCID;
    }

    [Bindable(event="propertyChange")]
    public function get SUPPLIER() : valueObjects.WarehouseModuleSupplier
    {
        return _internal_SUPPLIER;
    }

    [Bindable(event="propertyChange")]
    public function get SDOCTOR() : valueObjects.WarehouseModuleDoctor
    {
        return _internal_SDOCTOR;
    }

    [Bindable(event="propertyChange")]
    public function get RDOCTOR() : valueObjects.WarehouseModuleDoctor
    {
        return _internal_RDOCTOR;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTIMESTAMP() : String
    {
        return _internal_DOCTIMESTAMP;
    }

    [Bindable(event="propertyChange")]
    public function get DOCNUMBER() : String
    {
        return _internal_DOCNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get SUMM() : Number
    {
        return _internal_SUMM;
    }

    [Bindable(event="propertyChange")]
    public function get FARMFLOWS() : ArrayCollection
    {
        return _internal_FARMFLOWS;
    }

    [Bindable(event="propertyChange")]
    public function get COMMENT() : String
    {
        return _internal_COMMENT;
    }

    [Bindable(event="propertyChange")]
    public function get ROOM() : valueObjects.WarehouseModuleRoom
    {
        return _internal_ROOM;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS() : String
    {
        return _internal_STATUS;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set RTYPE(value:String) : void
    {
        var oldValue:String = _internal_RTYPE;
        if (oldValue !== value)
        {
            _internal_RTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RTYPE", oldValue, _internal_RTYPE));
        }
    }

    public function set STYPE(value:String) : void
    {
        var oldValue:String = _internal_STYPE;
        if (oldValue !== value)
        {
            _internal_STYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STYPE", oldValue, _internal_STYPE));
        }
    }

    public function set TREATMENTPROCID(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROCID;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROCID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROCID", oldValue, _internal_TREATMENTPROCID));
        }
    }

    public function set SUPPLIER(value:valueObjects.WarehouseModuleSupplier) : void
    {
        var oldValue:valueObjects.WarehouseModuleSupplier = _internal_SUPPLIER;
        if (oldValue !== value)
        {
            _internal_SUPPLIER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUPPLIER", oldValue, _internal_SUPPLIER));
        }
    }

    public function set SDOCTOR(value:valueObjects.WarehouseModuleDoctor) : void
    {
        var oldValue:valueObjects.WarehouseModuleDoctor = _internal_SDOCTOR;
        if (oldValue !== value)
        {
            _internal_SDOCTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SDOCTOR", oldValue, _internal_SDOCTOR));
        }
    }

    public function set RDOCTOR(value:valueObjects.WarehouseModuleDoctor) : void
    {
        var oldValue:valueObjects.WarehouseModuleDoctor = _internal_RDOCTOR;
        if (oldValue !== value)
        {
            _internal_RDOCTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RDOCTOR", oldValue, _internal_RDOCTOR));
        }
    }

    public function set DOCTIMESTAMP(value:String) : void
    {
        var oldValue:String = _internal_DOCTIMESTAMP;
        if (oldValue !== value)
        {
            _internal_DOCTIMESTAMP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTIMESTAMP", oldValue, _internal_DOCTIMESTAMP));
        }
    }

    public function set DOCNUMBER(value:String) : void
    {
        var oldValue:String = _internal_DOCNUMBER;
        if (oldValue !== value)
        {
            _internal_DOCNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCNUMBER", oldValue, _internal_DOCNUMBER));
        }
    }

    public function set SUMM(value:Number) : void
    {
        var oldValue:Number = _internal_SUMM;
        if (isNaN(_internal_SUMM) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_SUMM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMM", oldValue, _internal_SUMM));
        }
    }

    public function set FARMFLOWS(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_FARMFLOWS;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_FARMFLOWS = value;
            }
            else if (value is Array)
            {
                _internal_FARMFLOWS = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_FARMFLOWS = null;
            }
            else
            {
                throw new Error("value of FARMFLOWS must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMFLOWS", oldValue, _internal_FARMFLOWS));
        }
    }

    public function set COMMENT(value:String) : void
    {
        var oldValue:String = _internal_COMMENT;
        if (oldValue !== value)
        {
            _internal_COMMENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COMMENT", oldValue, _internal_COMMENT));
        }
    }

    public function set ROOM(value:valueObjects.WarehouseModuleRoom) : void
    {
        var oldValue:valueObjects.WarehouseModuleRoom = _internal_ROOM;
        if (oldValue !== value)
        {
            _internal_ROOM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOM", oldValue, _internal_ROOM));
        }
    }

    public function set STATUS(value:String) : void
    {
        var oldValue:String = _internal_STATUS;
        if (oldValue !== value)
        {
            _internal_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS", oldValue, _internal_STATUS));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _WarehouseModuleFarmDocumentEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _WarehouseModuleFarmDocumentEntityMetadata) : void
    {
        var oldValue : _WarehouseModuleFarmDocumentEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
