/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefStatisticsModule_SMSStatistic.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.ChiefStatisticsModule_SMSData;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefStatisticsModule_SMSStatistic extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefStatisticsModule_SMSStatistic") == null)
            {
                flash.net.registerClassAlias("ChiefStatisticsModule_SMSStatistic", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefStatisticsModule_SMSStatistic", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ChiefStatisticsModule_SMSData.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _ChiefStatisticsModule_SMSStatisticEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_period : String;
    private var _internal_period_type : String;
    private var _internal_period_startDate : String;
    private var _internal_period_endDate : String;
    private var _internal_sms_count : int;
    private var _internal_sms_success_count : int;
    private var _internal_sms_unsuccess_count : int;
    private var _internal_sms_undifinied_count : int;
    private var _internal_smsData : ArrayCollection;
    model_internal var _internal_smsData_leaf:valueObjects.ChiefStatisticsModule_SMSData;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefStatisticsModule_SMSStatistic()
    {
        _model = new _ChiefStatisticsModule_SMSStatisticEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get period() : String
    {
        return _internal_period;
    }

    [Bindable(event="propertyChange")]
    public function get period_type() : String
    {
        return _internal_period_type;
    }

    [Bindable(event="propertyChange")]
    public function get period_startDate() : String
    {
        return _internal_period_startDate;
    }

    [Bindable(event="propertyChange")]
    public function get period_endDate() : String
    {
        return _internal_period_endDate;
    }

    [Bindable(event="propertyChange")]
    public function get sms_count() : int
    {
        return _internal_sms_count;
    }

    [Bindable(event="propertyChange")]
    public function get sms_success_count() : int
    {
        return _internal_sms_success_count;
    }

    [Bindable(event="propertyChange")]
    public function get sms_unsuccess_count() : int
    {
        return _internal_sms_unsuccess_count;
    }

    [Bindable(event="propertyChange")]
    public function get sms_undifinied_count() : int
    {
        return _internal_sms_undifinied_count;
    }

    [Bindable(event="propertyChange")]
    public function get smsData() : ArrayCollection
    {
        return _internal_smsData;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set period(value:String) : void
    {
        var oldValue:String = _internal_period;
        if (oldValue !== value)
        {
            _internal_period = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period", oldValue, _internal_period));
        }
    }

    public function set period_type(value:String) : void
    {
        var oldValue:String = _internal_period_type;
        if (oldValue !== value)
        {
            _internal_period_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period_type", oldValue, _internal_period_type));
        }
    }

    public function set period_startDate(value:String) : void
    {
        var oldValue:String = _internal_period_startDate;
        if (oldValue !== value)
        {
            _internal_period_startDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period_startDate", oldValue, _internal_period_startDate));
        }
    }

    public function set period_endDate(value:String) : void
    {
        var oldValue:String = _internal_period_endDate;
        if (oldValue !== value)
        {
            _internal_period_endDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period_endDate", oldValue, _internal_period_endDate));
        }
    }

    public function set sms_count(value:int) : void
    {
        var oldValue:int = _internal_sms_count;
        if (oldValue !== value)
        {
            _internal_sms_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sms_count", oldValue, _internal_sms_count));
        }
    }

    public function set sms_success_count(value:int) : void
    {
        var oldValue:int = _internal_sms_success_count;
        if (oldValue !== value)
        {
            _internal_sms_success_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sms_success_count", oldValue, _internal_sms_success_count));
        }
    }

    public function set sms_unsuccess_count(value:int) : void
    {
        var oldValue:int = _internal_sms_unsuccess_count;
        if (oldValue !== value)
        {
            _internal_sms_unsuccess_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sms_unsuccess_count", oldValue, _internal_sms_unsuccess_count));
        }
    }

    public function set sms_undifinied_count(value:int) : void
    {
        var oldValue:int = _internal_sms_undifinied_count;
        if (oldValue !== value)
        {
            _internal_sms_undifinied_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sms_undifinied_count", oldValue, _internal_sms_undifinied_count));
        }
    }

    public function set smsData(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_smsData;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_smsData = value;
            }
            else if (value is Array)
            {
                _internal_smsData = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_smsData = null;
            }
            else
            {
                throw new Error("value of smsData must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "smsData", oldValue, _internal_smsData));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefStatisticsModule_SMSStatisticEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefStatisticsModule_SMSStatisticEntityMetadata) : void
    {
        var oldValue : _ChiefStatisticsModule_SMSStatisticEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
