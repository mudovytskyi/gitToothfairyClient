/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefSalaryModule_SalaryProc.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.ChiefSalaryModule_SalaryProc;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefSalaryModule_SalaryProc extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefSalaryModule_SalaryProc") == null)
            {
                flash.net.registerClassAlias("ChiefSalaryModule_SalaryProc", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefSalaryModule_SalaryProc", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ChiefSalaryModule_SalaryProc.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _ChiefSalaryModule_SalaryProcEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_proc_id : String;
    private var _internal_proc_shifr : String;
    private var _internal_proc_planName : String;
    private var _internal_proc_label : String;
    private var _internal_proc_courseName : String;
    private var _internal_proc_price : Number;
    private var _internal_proc_time : int;
    private var _internal_proc_UOP : Number;
    private var _internal_proc_Healthtypes : String;
    private var _internal_proc_Expenses : Number;
    private var _internal_salary_type : int;
    private var _internal_salary_amount : Number;
    private var _internal_NODETYPE : String;
    private var _internal_children : ArrayCollection;
    model_internal var _internal_children_leaf:valueObjects.ChiefSalaryModule_SalaryProc;
    private var _internal_isOpen : Boolean;
    private var _internal_isChanged : Boolean;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefSalaryModule_SalaryProc()
    {
        _model = new _ChiefSalaryModule_SalaryProcEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get proc_id() : String
    {
        return _internal_proc_id;
    }

    [Bindable(event="propertyChange")]
    public function get proc_shifr() : String
    {
        return _internal_proc_shifr;
    }

    [Bindable(event="propertyChange")]
    public function get proc_planName() : String
    {
        return _internal_proc_planName;
    }

    [Bindable(event="propertyChange")]
    public function get proc_label() : String
    {
        return _internal_proc_label;
    }

    [Bindable(event="propertyChange")]
    public function get proc_courseName() : String
    {
        return _internal_proc_courseName;
    }

    [Bindable(event="propertyChange")]
    public function get proc_price() : Number
    {
        return _internal_proc_price;
    }

    [Bindable(event="propertyChange")]
    public function get proc_time() : int
    {
        return _internal_proc_time;
    }

    [Bindable(event="propertyChange")]
    public function get proc_UOP() : Number
    {
        return _internal_proc_UOP;
    }

    [Bindable(event="propertyChange")]
    public function get proc_Healthtypes() : String
    {
        return _internal_proc_Healthtypes;
    }

    [Bindable(event="propertyChange")]
    public function get proc_Expenses() : Number
    {
        return _internal_proc_Expenses;
    }

    [Bindable(event="propertyChange")]
    public function get salary_type() : int
    {
        return _internal_salary_type;
    }

    [Bindable(event="propertyChange")]
    public function get salary_amount() : Number
    {
        return _internal_salary_amount;
    }

    [Bindable(event="propertyChange")]
    public function get NODETYPE() : String
    {
        return _internal_NODETYPE;
    }

    [Bindable(event="propertyChange")]
    public function get children() : ArrayCollection
    {
        return _internal_children;
    }

    [Bindable(event="propertyChange")]
    public function get isOpen() : Boolean
    {
        return _internal_isOpen;
    }

    [Bindable(event="propertyChange")]
    public function get isChanged() : Boolean
    {
        return _internal_isChanged;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set proc_id(value:String) : void
    {
        var oldValue:String = _internal_proc_id;
        if (oldValue !== value)
        {
            _internal_proc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_id", oldValue, _internal_proc_id));
        }
    }

    public function set proc_shifr(value:String) : void
    {
        var oldValue:String = _internal_proc_shifr;
        if (oldValue !== value)
        {
            _internal_proc_shifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_shifr", oldValue, _internal_proc_shifr));
        }
    }

    public function set proc_planName(value:String) : void
    {
        var oldValue:String = _internal_proc_planName;
        if (oldValue !== value)
        {
            _internal_proc_planName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_planName", oldValue, _internal_proc_planName));
        }
    }

    public function set proc_label(value:String) : void
    {
        var oldValue:String = _internal_proc_label;
        if (oldValue !== value)
        {
            _internal_proc_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_label", oldValue, _internal_proc_label));
        }
    }

    public function set proc_courseName(value:String) : void
    {
        var oldValue:String = _internal_proc_courseName;
        if (oldValue !== value)
        {
            _internal_proc_courseName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_courseName", oldValue, _internal_proc_courseName));
        }
    }

    public function set proc_price(value:Number) : void
    {
        var oldValue:Number = _internal_proc_price;
        if (isNaN(_internal_proc_price) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_proc_price = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_price", oldValue, _internal_proc_price));
        }
    }

    public function set proc_time(value:int) : void
    {
        var oldValue:int = _internal_proc_time;
        if (oldValue !== value)
        {
            _internal_proc_time = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_time", oldValue, _internal_proc_time));
        }
    }

    public function set proc_UOP(value:Number) : void
    {
        var oldValue:Number = _internal_proc_UOP;
        if (isNaN(_internal_proc_UOP) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_proc_UOP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_UOP", oldValue, _internal_proc_UOP));
        }
    }

    public function set proc_Healthtypes(value:String) : void
    {
        var oldValue:String = _internal_proc_Healthtypes;
        if (oldValue !== value)
        {
            _internal_proc_Healthtypes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_Healthtypes", oldValue, _internal_proc_Healthtypes));
        }
    }

    public function set proc_Expenses(value:Number) : void
    {
        var oldValue:Number = _internal_proc_Expenses;
        if (isNaN(_internal_proc_Expenses) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_proc_Expenses = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_Expenses", oldValue, _internal_proc_Expenses));
        }
    }

    public function set salary_type(value:int) : void
    {
        var oldValue:int = _internal_salary_type;
        if (oldValue !== value)
        {
            _internal_salary_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "salary_type", oldValue, _internal_salary_type));
        }
    }

    public function set salary_amount(value:Number) : void
    {
        var oldValue:Number = _internal_salary_amount;
        if (isNaN(_internal_salary_amount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_salary_amount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "salary_amount", oldValue, _internal_salary_amount));
        }
    }

    public function set NODETYPE(value:String) : void
    {
        var oldValue:String = _internal_NODETYPE;
        if (oldValue !== value)
        {
            _internal_NODETYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NODETYPE", oldValue, _internal_NODETYPE));
        }
    }

    public function set children(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_children;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_children = value;
            }
            else if (value is Array)
            {
                _internal_children = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_children = null;
            }
            else
            {
                throw new Error("value of children must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "children", oldValue, _internal_children));
        }
    }

    public function set isOpen(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOpen;
        if (oldValue !== value)
        {
            _internal_isOpen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOpen", oldValue, _internal_isOpen));
        }
    }

    public function set isChanged(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isChanged;
        if (oldValue !== value)
        {
            _internal_isChanged = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isChanged", oldValue, _internal_isChanged));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefSalaryModule_SalaryProcEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefSalaryModule_SalaryProcEntityMetadata) : void
    {
        var oldValue : _ChiefSalaryModule_SalaryProcEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
