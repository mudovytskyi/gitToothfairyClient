
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.HealthPlanMKB10Diagnos2;
import valueObjects.HealthPlanMKB10ToothExam;
import valueObjects.HealthPlanMKB10TreatmentAdditions;
import valueObjects.HealthPlanMKB10TreatmentObject;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _HealthPlanMKB10TreatmentCourseEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ID", "addiotions_Anamnes", "addiotions_Xray", "addiotions_Status", "addiotions_Recomend", "addiotions_Epicrisis", "VISIT", "Befor", "After", "Diagnos", "PatientName", "TreatmentObjects", "ProtocolAnamnes", "ProtocolStatus", "ProtocolRecomend", "ProtocolRentgen", "ProtocolEpicrisis");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ID", "addiotions_Anamnes", "addiotions_Xray", "addiotions_Status", "addiotions_Recomend", "addiotions_Epicrisis", "VISIT", "Befor", "After", "Diagnos", "PatientName", "TreatmentObjects", "ProtocolAnamnes", "ProtocolStatus", "ProtocolRecomend", "ProtocolRentgen", "ProtocolEpicrisis");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ID", "addiotions_Anamnes", "addiotions_Xray", "addiotions_Status", "addiotions_Recomend", "addiotions_Epicrisis", "VISIT", "Befor", "After", "Diagnos", "PatientName", "TreatmentObjects", "ProtocolAnamnes", "ProtocolStatus", "ProtocolRecomend", "ProtocolRentgen", "ProtocolEpicrisis");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ID", "addiotions_Anamnes", "addiotions_Xray", "addiotions_Status", "addiotions_Recomend", "addiotions_Epicrisis", "VISIT", "Befor", "After", "Diagnos", "PatientName", "TreatmentObjects", "ProtocolAnamnes", "ProtocolStatus", "ProtocolRecomend", "ProtocolRentgen", "ProtocolEpicrisis");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("addiotions_Anamnes", "addiotions_Xray", "addiotions_Status", "addiotions_Recomend", "addiotions_Epicrisis", "Diagnos", "TreatmentObjects");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "HealthPlanMKB10TreatmentCourse";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_HealthPlanMKB10TreatmentCourse;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _HealthPlanMKB10TreatmentCourseEntityMetadata(value : _Super_HealthPlanMKB10TreatmentCourse)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["addiotions_Anamnes"] = new Array();
            model_internal::dependentsOnMap["addiotions_Xray"] = new Array();
            model_internal::dependentsOnMap["addiotions_Status"] = new Array();
            model_internal::dependentsOnMap["addiotions_Recomend"] = new Array();
            model_internal::dependentsOnMap["addiotions_Epicrisis"] = new Array();
            model_internal::dependentsOnMap["VISIT"] = new Array();
            model_internal::dependentsOnMap["Befor"] = new Array();
            model_internal::dependentsOnMap["After"] = new Array();
            model_internal::dependentsOnMap["Diagnos"] = new Array();
            model_internal::dependentsOnMap["PatientName"] = new Array();
            model_internal::dependentsOnMap["TreatmentObjects"] = new Array();
            model_internal::dependentsOnMap["ProtocolAnamnes"] = new Array();
            model_internal::dependentsOnMap["ProtocolStatus"] = new Array();
            model_internal::dependentsOnMap["ProtocolRecomend"] = new Array();
            model_internal::dependentsOnMap["ProtocolRentgen"] = new Array();
            model_internal::dependentsOnMap["ProtocolEpicrisis"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["addiotions_Anamnes"] = "valueObjects.HealthPlanMKB10TreatmentAdditions";
            model_internal::collectionBaseMap["addiotions_Xray"] = "valueObjects.HealthPlanMKB10TreatmentAdditions";
            model_internal::collectionBaseMap["addiotions_Status"] = "valueObjects.HealthPlanMKB10TreatmentAdditions";
            model_internal::collectionBaseMap["addiotions_Recomend"] = "valueObjects.HealthPlanMKB10TreatmentAdditions";
            model_internal::collectionBaseMap["addiotions_Epicrisis"] = "valueObjects.HealthPlanMKB10TreatmentAdditions";
            model_internal::collectionBaseMap["Diagnos"] = "valueObjects.HealthPlanMKB10Diagnos2";
            model_internal::collectionBaseMap["TreatmentObjects"] = "valueObjects.HealthPlanMKB10TreatmentObject";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ID"] = "int";
        model_internal::propertyTypeMap["addiotions_Anamnes"] = "ArrayCollection";
        model_internal::propertyTypeMap["addiotions_Xray"] = "ArrayCollection";
        model_internal::propertyTypeMap["addiotions_Status"] = "ArrayCollection";
        model_internal::propertyTypeMap["addiotions_Recomend"] = "ArrayCollection";
        model_internal::propertyTypeMap["addiotions_Epicrisis"] = "ArrayCollection";
        model_internal::propertyTypeMap["VISIT"] = "int";
        model_internal::propertyTypeMap["Befor"] = "valueObjects.HealthPlanMKB10ToothExam";
        model_internal::propertyTypeMap["After"] = "valueObjects.HealthPlanMKB10ToothExam";
        model_internal::propertyTypeMap["Diagnos"] = "ArrayCollection";
        model_internal::propertyTypeMap["PatientName"] = "String";
        model_internal::propertyTypeMap["TreatmentObjects"] = "ArrayCollection";
        model_internal::propertyTypeMap["ProtocolAnamnes"] = "String";
        model_internal::propertyTypeMap["ProtocolStatus"] = "String";
        model_internal::propertyTypeMap["ProtocolRecomend"] = "String";
        model_internal::propertyTypeMap["ProtocolRentgen"] = "String";
        model_internal::propertyTypeMap["ProtocolEpicrisis"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity HealthPlanMKB10TreatmentCourse");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity HealthPlanMKB10TreatmentCourse");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of HealthPlanMKB10TreatmentCourse");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity HealthPlanMKB10TreatmentCourse");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity HealthPlanMKB10TreatmentCourse");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity HealthPlanMKB10TreatmentCourse");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAddiotions_AnamnesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAddiotions_XrayAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAddiotions_StatusAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAddiotions_RecomendAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAddiotions_EpicrisisAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVISITAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBeforAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAfterAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiagnosAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentObjectsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProtocolAnamnesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProtocolStatusAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProtocolRecomendAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProtocolRentgenAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProtocolEpicrisisAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get addiotions_AnamnesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get addiotions_XrayStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get addiotions_StatusStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get addiotions_RecomendStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get addiotions_EpicrisisStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get VISITStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BeforStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AfterStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DiagnosStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TreatmentObjectsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ProtocolAnamnesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ProtocolStatusStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ProtocolRecomendStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ProtocolRentgenStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ProtocolEpicrisisStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
