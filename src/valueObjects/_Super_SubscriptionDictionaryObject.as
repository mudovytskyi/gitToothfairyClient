/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - SubscriptionDictionaryObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.SubscriptionProcedureObject;
import valueObjects.SubscriptionTypeObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_SubscriptionDictionaryObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("SubscriptionDictionaryObject") == null)
            {
                flash.net.registerClassAlias("SubscriptionDictionaryObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("SubscriptionDictionaryObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.SubscriptionTypeObject.initRemoteClassAliasSingleChild();
        valueObjects.SubscriptionProcedureObject.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _SubscriptionDictionaryObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_NAME : String;
    private var _internal_MAX_VISITS_PER_MONTH : int;
    private var _internal_PRICE : Number;
    private var _internal_REG_CODE : int;
    private var _internal_BAR_CODE : String;
    private var _internal_NAME48 : String;
    private var _internal_type : valueObjects.SubscriptionTypeObject;
    private var _internal_procedures : ArrayCollection;
    model_internal var _internal_procedures_leaf:valueObjects.SubscriptionProcedureObject;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_SubscriptionDictionaryObject()
    {
        _model = new _SubscriptionDictionaryObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get MAX_VISITS_PER_MONTH() : int
    {
        return _internal_MAX_VISITS_PER_MONTH;
    }

    [Bindable(event="propertyChange")]
    public function get PRICE() : Number
    {
        return _internal_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get REG_CODE() : int
    {
        return _internal_REG_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get BAR_CODE() : String
    {
        return _internal_BAR_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get NAME48() : String
    {
        return _internal_NAME48;
    }

    [Bindable(event="propertyChange")]
    public function get type() : valueObjects.SubscriptionTypeObject
    {
        return _internal_type;
    }

    [Bindable(event="propertyChange")]
    public function get procedures() : ArrayCollection
    {
        return _internal_procedures;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set MAX_VISITS_PER_MONTH(value:int) : void
    {
        var oldValue:int = _internal_MAX_VISITS_PER_MONTH;
        if (oldValue !== value)
        {
            _internal_MAX_VISITS_PER_MONTH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MAX_VISITS_PER_MONTH", oldValue, _internal_MAX_VISITS_PER_MONTH));
        }
    }

    public function set PRICE(value:Number) : void
    {
        var oldValue:Number = _internal_PRICE;
        if (isNaN(_internal_PRICE) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRICE", oldValue, _internal_PRICE));
        }
    }

    public function set REG_CODE(value:int) : void
    {
        var oldValue:int = _internal_REG_CODE;
        if (oldValue !== value)
        {
            _internal_REG_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REG_CODE", oldValue, _internal_REG_CODE));
        }
    }

    public function set BAR_CODE(value:String) : void
    {
        var oldValue:String = _internal_BAR_CODE;
        if (oldValue !== value)
        {
            _internal_BAR_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BAR_CODE", oldValue, _internal_BAR_CODE));
        }
    }

    public function set NAME48(value:String) : void
    {
        var oldValue:String = _internal_NAME48;
        if (oldValue !== value)
        {
            _internal_NAME48 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME48", oldValue, _internal_NAME48));
        }
    }

    public function set type(value:valueObjects.SubscriptionTypeObject) : void
    {
        var oldValue:valueObjects.SubscriptionTypeObject = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    public function set procedures(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_procedures;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_procedures = value;
            }
            else if (value is Array)
            {
                _internal_procedures = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_procedures = null;
            }
            else
            {
                throw new Error("value of procedures must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedures", oldValue, _internal_procedures));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _SubscriptionDictionaryObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _SubscriptionDictionaryObjectEntityMetadata) : void
    {
        var oldValue : _SubscriptionDictionaryObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
