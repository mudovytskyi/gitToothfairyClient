/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CommonClinicInformationModuleAddressData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CommonClinicInformationModuleAddressData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CommonClinicInformationModuleAddressData") == null)
            {
                flash.net.registerClassAlias("CommonClinicInformationModuleAddressData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CommonClinicInformationModuleAddressData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _CommonClinicInformationModuleAddressDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_ZIP : String;
    private var _internal_ADDRESS_TYPE : String;
    private var _internal_COUNTRY : String;
    private var _internal_AREA : String;
    private var _internal_REGION : String;
    private var _internal_SETTLEMENT : String;
    private var _internal_SETTLEMENT_TYPE : String;
    private var _internal_SETTLEMENT_ID : String;
    private var _internal_STREET_TYPE : String;
    private var _internal_STREET : String;
    private var _internal_BUILDING : String;
    private var _internal_APRT : String;
    private var _internal_FK_CLINICREGISTRATIONDATA : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CommonClinicInformationModuleAddressData()
    {
        _model = new _CommonClinicInformationModuleAddressDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get ZIP() : String
    {
        return _internal_ZIP;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS_TYPE() : String
    {
        return _internal_ADDRESS_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTRY() : String
    {
        return _internal_COUNTRY;
    }

    [Bindable(event="propertyChange")]
    public function get AREA() : String
    {
        return _internal_AREA;
    }

    [Bindable(event="propertyChange")]
    public function get REGION() : String
    {
        return _internal_REGION;
    }

    [Bindable(event="propertyChange")]
    public function get SETTLEMENT() : String
    {
        return _internal_SETTLEMENT;
    }

    [Bindable(event="propertyChange")]
    public function get SETTLEMENT_TYPE() : String
    {
        return _internal_SETTLEMENT_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get SETTLEMENT_ID() : String
    {
        return _internal_SETTLEMENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get STREET_TYPE() : String
    {
        return _internal_STREET_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get STREET() : String
    {
        return _internal_STREET;
    }

    [Bindable(event="propertyChange")]
    public function get BUILDING() : String
    {
        return _internal_BUILDING;
    }

    [Bindable(event="propertyChange")]
    public function get APRT() : String
    {
        return _internal_APRT;
    }

    [Bindable(event="propertyChange")]
    public function get FK_CLINICREGISTRATIONDATA() : String
    {
        return _internal_FK_CLINICREGISTRATIONDATA;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set ZIP(value:String) : void
    {
        var oldValue:String = _internal_ZIP;
        if (oldValue !== value)
        {
            _internal_ZIP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ZIP", oldValue, _internal_ZIP));
        }
    }

    public function set ADDRESS_TYPE(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS_TYPE;
        if (oldValue !== value)
        {
            _internal_ADDRESS_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS_TYPE", oldValue, _internal_ADDRESS_TYPE));
        }
    }

    public function set COUNTRY(value:String) : void
    {
        var oldValue:String = _internal_COUNTRY;
        if (oldValue !== value)
        {
            _internal_COUNTRY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTRY", oldValue, _internal_COUNTRY));
        }
    }

    public function set AREA(value:String) : void
    {
        var oldValue:String = _internal_AREA;
        if (oldValue !== value)
        {
            _internal_AREA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AREA", oldValue, _internal_AREA));
        }
    }

    public function set REGION(value:String) : void
    {
        var oldValue:String = _internal_REGION;
        if (oldValue !== value)
        {
            _internal_REGION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REGION", oldValue, _internal_REGION));
        }
    }

    public function set SETTLEMENT(value:String) : void
    {
        var oldValue:String = _internal_SETTLEMENT;
        if (oldValue !== value)
        {
            _internal_SETTLEMENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SETTLEMENT", oldValue, _internal_SETTLEMENT));
        }
    }

    public function set SETTLEMENT_TYPE(value:String) : void
    {
        var oldValue:String = _internal_SETTLEMENT_TYPE;
        if (oldValue !== value)
        {
            _internal_SETTLEMENT_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SETTLEMENT_TYPE", oldValue, _internal_SETTLEMENT_TYPE));
        }
    }

    public function set SETTLEMENT_ID(value:String) : void
    {
        var oldValue:String = _internal_SETTLEMENT_ID;
        if (oldValue !== value)
        {
            _internal_SETTLEMENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SETTLEMENT_ID", oldValue, _internal_SETTLEMENT_ID));
        }
    }

    public function set STREET_TYPE(value:String) : void
    {
        var oldValue:String = _internal_STREET_TYPE;
        if (oldValue !== value)
        {
            _internal_STREET_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STREET_TYPE", oldValue, _internal_STREET_TYPE));
        }
    }

    public function set STREET(value:String) : void
    {
        var oldValue:String = _internal_STREET;
        if (oldValue !== value)
        {
            _internal_STREET = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STREET", oldValue, _internal_STREET));
        }
    }

    public function set BUILDING(value:String) : void
    {
        var oldValue:String = _internal_BUILDING;
        if (oldValue !== value)
        {
            _internal_BUILDING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BUILDING", oldValue, _internal_BUILDING));
        }
    }

    public function set APRT(value:String) : void
    {
        var oldValue:String = _internal_APRT;
        if (oldValue !== value)
        {
            _internal_APRT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "APRT", oldValue, _internal_APRT));
        }
    }

    public function set FK_CLINICREGISTRATIONDATA(value:String) : void
    {
        var oldValue:String = _internal_FK_CLINICREGISTRATIONDATA;
        if (oldValue !== value)
        {
            _internal_FK_CLINICREGISTRATIONDATA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FK_CLINICREGISTRATIONDATA", oldValue, _internal_FK_CLINICREGISTRATIONDATA));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CommonClinicInformationModuleAddressDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CommonClinicInformationModuleAddressDataEntityMetadata) : void
    {
        var oldValue : _CommonClinicInformationModuleAddressDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
