/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefMapModuleData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefMapModuleData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefMapModuleData") == null)
            {
                flash.net.registerClassAlias("ChiefMapModuleData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefMapModuleData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefMapModuleDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_POSITIONS : ArrayCollection;
    private var _internal_ALLCOUNT : int;
    private var _internal_FINDCOUNT : int;
    private var _internal_NOTFINDCOUNT : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefMapModuleData()
    {
        _model = new _ChiefMapModuleDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get POSITIONS() : ArrayCollection
    {
        return _internal_POSITIONS;
    }

    [Bindable(event="propertyChange")]
    public function get ALLCOUNT() : int
    {
        return _internal_ALLCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get FINDCOUNT() : int
    {
        return _internal_FINDCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get NOTFINDCOUNT() : int
    {
        return _internal_NOTFINDCOUNT;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set POSITIONS(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_POSITIONS;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_POSITIONS = value;
            }
            else if (value is Array)
            {
                _internal_POSITIONS = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_POSITIONS = null;
            }
            else
            {
                throw new Error("value of POSITIONS must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "POSITIONS", oldValue, _internal_POSITIONS));
        }
    }

    public function set ALLCOUNT(value:int) : void
    {
        var oldValue:int = _internal_ALLCOUNT;
        if (oldValue !== value)
        {
            _internal_ALLCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ALLCOUNT", oldValue, _internal_ALLCOUNT));
        }
    }

    public function set FINDCOUNT(value:int) : void
    {
        var oldValue:int = _internal_FINDCOUNT;
        if (oldValue !== value)
        {
            _internal_FINDCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FINDCOUNT", oldValue, _internal_FINDCOUNT));
        }
    }

    public function set NOTFINDCOUNT(value:int) : void
    {
        var oldValue:int = _internal_NOTFINDCOUNT;
        if (oldValue !== value)
        {
            _internal_NOTFINDCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NOTFINDCOUNT", oldValue, _internal_NOTFINDCOUNT));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefMapModuleDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefMapModuleDataEntityMetadata) : void
    {
        var oldValue : _ChiefMapModuleDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
