
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.PatientCardPDFModuleAmbcard;
import valueObjects.PatientCardPDFModuleExam;
import valueObjects.PatientCardPDFModuleHygcontrol;
import valueObjects.PatientCardPDFModuleIllHistoryRecord;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _PatientCardPDFModulePatientCardDataEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("CLINICNAME", "CLINICLEGALNAME", "CLINICADDRESS", "PATIENTCARDNUMBER", "PATIENTCARDDATE", "PATIENTFIRSTNAME", "PATIENTMIDDLENAME", "PATIENTLASTNAME", "PATIENTBIRTHDAY", "PATIENTSEX", "PATIENTPOSTALCODE", "PATIENTCOUNTRY", "PATIENTSTATE", "PATIENTCITY", "PATIENTRAGION", "PATIENTADDRESS", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "PATIENTEMPLOYMENTPLACE", "PATIENTJOB", "PATIENTWORKPHONE", "PATIENTTESTTESTDATE", "PATIENTTESTCARDIACDISEASE", "PATIENTTESTKIDNEYDISEASE", "PATIENTTESTLIVERDISEASE", "PATIENTTESTOTHERDISEASE", "PATIENTTESTARTERIALPRESSURE", "PATIENTTESTRHEUMATISM", "PATIENTTESTHEPATITIS", "PATIENTTESTDIABETES", "PATIENTTESTFIT", "PATIENTTESTBLEEDING", "PATIENTTESTALLERGY", "PATIENTTESTPREGNANCY", "PATIENTTESTMEDICATIONS", "PATIENTTESTBLOODGROUP", "PATIENTTESTRHESUSFACTOR", "PATIENTTESTMOUTHULCER", "PATIENTTESTFUNGUS", "PATIENTTESTFEVER", "PATIENTTESTTHROATACHE", "PATIENTTESTLYMPHNODES", "PATIENTTESTREDSKINAREA", "PATIENTTESTHYPERHIDROSIS", "PATIENTTESTDIARRHEA", "PATIENTTESTAMNESIA", "PATIENTTESTWEIGHTLOSS", "PATIENTTESTHEADACHE", "PATIENTTESTAIDS", "PATIENTTESTWORKWITHBLOOD", "PATIENTTESTNARCOMANIA", "TOOTHEXAM", "AMBCARD", "HYGCONTROL", "ILLHISTORY");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("CLINICNAME", "CLINICLEGALNAME", "CLINICADDRESS", "PATIENTCARDNUMBER", "PATIENTCARDDATE", "PATIENTFIRSTNAME", "PATIENTMIDDLENAME", "PATIENTLASTNAME", "PATIENTBIRTHDAY", "PATIENTSEX", "PATIENTPOSTALCODE", "PATIENTCOUNTRY", "PATIENTSTATE", "PATIENTCITY", "PATIENTRAGION", "PATIENTADDRESS", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "PATIENTEMPLOYMENTPLACE", "PATIENTJOB", "PATIENTWORKPHONE", "PATIENTTESTTESTDATE", "PATIENTTESTCARDIACDISEASE", "PATIENTTESTKIDNEYDISEASE", "PATIENTTESTLIVERDISEASE", "PATIENTTESTOTHERDISEASE", "PATIENTTESTARTERIALPRESSURE", "PATIENTTESTRHEUMATISM", "PATIENTTESTHEPATITIS", "PATIENTTESTDIABETES", "PATIENTTESTFIT", "PATIENTTESTBLEEDING", "PATIENTTESTALLERGY", "PATIENTTESTPREGNANCY", "PATIENTTESTMEDICATIONS", "PATIENTTESTBLOODGROUP", "PATIENTTESTRHESUSFACTOR", "PATIENTTESTMOUTHULCER", "PATIENTTESTFUNGUS", "PATIENTTESTFEVER", "PATIENTTESTTHROATACHE", "PATIENTTESTLYMPHNODES", "PATIENTTESTREDSKINAREA", "PATIENTTESTHYPERHIDROSIS", "PATIENTTESTDIARRHEA", "PATIENTTESTAMNESIA", "PATIENTTESTWEIGHTLOSS", "PATIENTTESTHEADACHE", "PATIENTTESTAIDS", "PATIENTTESTWORKWITHBLOOD", "PATIENTTESTNARCOMANIA", "TOOTHEXAM", "AMBCARD", "HYGCONTROL", "ILLHISTORY");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("CLINICNAME", "CLINICLEGALNAME", "CLINICADDRESS", "PATIENTCARDNUMBER", "PATIENTCARDDATE", "PATIENTFIRSTNAME", "PATIENTMIDDLENAME", "PATIENTLASTNAME", "PATIENTBIRTHDAY", "PATIENTSEX", "PATIENTPOSTALCODE", "PATIENTCOUNTRY", "PATIENTSTATE", "PATIENTCITY", "PATIENTRAGION", "PATIENTADDRESS", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "PATIENTEMPLOYMENTPLACE", "PATIENTJOB", "PATIENTWORKPHONE", "PATIENTTESTTESTDATE", "PATIENTTESTCARDIACDISEASE", "PATIENTTESTKIDNEYDISEASE", "PATIENTTESTLIVERDISEASE", "PATIENTTESTOTHERDISEASE", "PATIENTTESTARTERIALPRESSURE", "PATIENTTESTRHEUMATISM", "PATIENTTESTHEPATITIS", "PATIENTTESTDIABETES", "PATIENTTESTFIT", "PATIENTTESTBLEEDING", "PATIENTTESTALLERGY", "PATIENTTESTPREGNANCY", "PATIENTTESTMEDICATIONS", "PATIENTTESTBLOODGROUP", "PATIENTTESTRHESUSFACTOR", "PATIENTTESTMOUTHULCER", "PATIENTTESTFUNGUS", "PATIENTTESTFEVER", "PATIENTTESTTHROATACHE", "PATIENTTESTLYMPHNODES", "PATIENTTESTREDSKINAREA", "PATIENTTESTHYPERHIDROSIS", "PATIENTTESTDIARRHEA", "PATIENTTESTAMNESIA", "PATIENTTESTWEIGHTLOSS", "PATIENTTESTHEADACHE", "PATIENTTESTAIDS", "PATIENTTESTWORKWITHBLOOD", "PATIENTTESTNARCOMANIA", "TOOTHEXAM", "AMBCARD", "HYGCONTROL", "ILLHISTORY");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("CLINICNAME", "CLINICLEGALNAME", "CLINICADDRESS", "PATIENTCARDNUMBER", "PATIENTCARDDATE", "PATIENTFIRSTNAME", "PATIENTMIDDLENAME", "PATIENTLASTNAME", "PATIENTBIRTHDAY", "PATIENTSEX", "PATIENTPOSTALCODE", "PATIENTCOUNTRY", "PATIENTSTATE", "PATIENTCITY", "PATIENTRAGION", "PATIENTADDRESS", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "PATIENTEMPLOYMENTPLACE", "PATIENTJOB", "PATIENTWORKPHONE", "PATIENTTESTTESTDATE", "PATIENTTESTCARDIACDISEASE", "PATIENTTESTKIDNEYDISEASE", "PATIENTTESTLIVERDISEASE", "PATIENTTESTOTHERDISEASE", "PATIENTTESTARTERIALPRESSURE", "PATIENTTESTRHEUMATISM", "PATIENTTESTHEPATITIS", "PATIENTTESTDIABETES", "PATIENTTESTFIT", "PATIENTTESTBLEEDING", "PATIENTTESTALLERGY", "PATIENTTESTPREGNANCY", "PATIENTTESTMEDICATIONS", "PATIENTTESTBLOODGROUP", "PATIENTTESTRHESUSFACTOR", "PATIENTTESTMOUTHULCER", "PATIENTTESTFUNGUS", "PATIENTTESTFEVER", "PATIENTTESTTHROATACHE", "PATIENTTESTLYMPHNODES", "PATIENTTESTREDSKINAREA", "PATIENTTESTHYPERHIDROSIS", "PATIENTTESTDIARRHEA", "PATIENTTESTAMNESIA", "PATIENTTESTWEIGHTLOSS", "PATIENTTESTHEADACHE", "PATIENTTESTAIDS", "PATIENTTESTWORKWITHBLOOD", "PATIENTTESTNARCOMANIA", "TOOTHEXAM", "AMBCARD", "HYGCONTROL", "ILLHISTORY");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("TOOTHEXAM", "AMBCARD", "HYGCONTROL", "ILLHISTORY");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "PatientCardPDFModulePatientCardData";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_PatientCardPDFModulePatientCardData;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _PatientCardPDFModulePatientCardDataEntityMetadata(value : _Super_PatientCardPDFModulePatientCardData)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["CLINICNAME"] = new Array();
            model_internal::dependentsOnMap["CLINICLEGALNAME"] = new Array();
            model_internal::dependentsOnMap["CLINICADDRESS"] = new Array();
            model_internal::dependentsOnMap["PATIENTCARDNUMBER"] = new Array();
            model_internal::dependentsOnMap["PATIENTCARDDATE"] = new Array();
            model_internal::dependentsOnMap["PATIENTFIRSTNAME"] = new Array();
            model_internal::dependentsOnMap["PATIENTMIDDLENAME"] = new Array();
            model_internal::dependentsOnMap["PATIENTLASTNAME"] = new Array();
            model_internal::dependentsOnMap["PATIENTBIRTHDAY"] = new Array();
            model_internal::dependentsOnMap["PATIENTSEX"] = new Array();
            model_internal::dependentsOnMap["PATIENTPOSTALCODE"] = new Array();
            model_internal::dependentsOnMap["PATIENTCOUNTRY"] = new Array();
            model_internal::dependentsOnMap["PATIENTSTATE"] = new Array();
            model_internal::dependentsOnMap["PATIENTCITY"] = new Array();
            model_internal::dependentsOnMap["PATIENTRAGION"] = new Array();
            model_internal::dependentsOnMap["PATIENTADDRESS"] = new Array();
            model_internal::dependentsOnMap["PATIENTTELEPHONENUMBER"] = new Array();
            model_internal::dependentsOnMap["PATIENTMOBILENUMBER"] = new Array();
            model_internal::dependentsOnMap["PATIENTEMPLOYMENTPLACE"] = new Array();
            model_internal::dependentsOnMap["PATIENTJOB"] = new Array();
            model_internal::dependentsOnMap["PATIENTWORKPHONE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTTESTDATE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTCARDIACDISEASE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTKIDNEYDISEASE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTLIVERDISEASE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTOTHERDISEASE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTARTERIALPRESSURE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTRHEUMATISM"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTHEPATITIS"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTDIABETES"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTFIT"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTBLEEDING"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTALLERGY"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTPREGNANCY"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTMEDICATIONS"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTBLOODGROUP"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTRHESUSFACTOR"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTMOUTHULCER"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTFUNGUS"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTFEVER"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTTHROATACHE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTLYMPHNODES"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTREDSKINAREA"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTHYPERHIDROSIS"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTDIARRHEA"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTAMNESIA"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTWEIGHTLOSS"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTHEADACHE"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTAIDS"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTWORKWITHBLOOD"] = new Array();
            model_internal::dependentsOnMap["PATIENTTESTNARCOMANIA"] = new Array();
            model_internal::dependentsOnMap["TOOTHEXAM"] = new Array();
            model_internal::dependentsOnMap["AMBCARD"] = new Array();
            model_internal::dependentsOnMap["HYGCONTROL"] = new Array();
            model_internal::dependentsOnMap["ILLHISTORY"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["TOOTHEXAM"] = "valueObjects.PatientCardPDFModuleExam";
            model_internal::collectionBaseMap["AMBCARD"] = "valueObjects.PatientCardPDFModuleAmbcard";
            model_internal::collectionBaseMap["HYGCONTROL"] = "valueObjects.PatientCardPDFModuleHygcontrol";
            model_internal::collectionBaseMap["ILLHISTORY"] = "valueObjects.PatientCardPDFModuleIllHistoryRecord";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["CLINICNAME"] = "String";
        model_internal::propertyTypeMap["CLINICLEGALNAME"] = "String";
        model_internal::propertyTypeMap["CLINICADDRESS"] = "String";
        model_internal::propertyTypeMap["PATIENTCARDNUMBER"] = "String";
        model_internal::propertyTypeMap["PATIENTCARDDATE"] = "String";
        model_internal::propertyTypeMap["PATIENTFIRSTNAME"] = "String";
        model_internal::propertyTypeMap["PATIENTMIDDLENAME"] = "String";
        model_internal::propertyTypeMap["PATIENTLASTNAME"] = "String";
        model_internal::propertyTypeMap["PATIENTBIRTHDAY"] = "String";
        model_internal::propertyTypeMap["PATIENTSEX"] = "String";
        model_internal::propertyTypeMap["PATIENTPOSTALCODE"] = "String";
        model_internal::propertyTypeMap["PATIENTCOUNTRY"] = "String";
        model_internal::propertyTypeMap["PATIENTSTATE"] = "String";
        model_internal::propertyTypeMap["PATIENTCITY"] = "String";
        model_internal::propertyTypeMap["PATIENTRAGION"] = "String";
        model_internal::propertyTypeMap["PATIENTADDRESS"] = "String";
        model_internal::propertyTypeMap["PATIENTTELEPHONENUMBER"] = "String";
        model_internal::propertyTypeMap["PATIENTMOBILENUMBER"] = "String";
        model_internal::propertyTypeMap["PATIENTEMPLOYMENTPLACE"] = "String";
        model_internal::propertyTypeMap["PATIENTJOB"] = "String";
        model_internal::propertyTypeMap["PATIENTWORKPHONE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTTESTDATE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTCARDIACDISEASE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTKIDNEYDISEASE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTLIVERDISEASE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTOTHERDISEASE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTARTERIALPRESSURE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTRHEUMATISM"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTHEPATITIS"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTDIABETES"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTFIT"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTBLEEDING"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTALLERGY"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTPREGNANCY"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTMEDICATIONS"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTBLOODGROUP"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTRHESUSFACTOR"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTMOUTHULCER"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTFUNGUS"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTFEVER"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTTHROATACHE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTLYMPHNODES"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTREDSKINAREA"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTHYPERHIDROSIS"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTDIARRHEA"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTAMNESIA"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTWEIGHTLOSS"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTHEADACHE"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTAIDS"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTWORKWITHBLOOD"] = "String";
        model_internal::propertyTypeMap["PATIENTTESTNARCOMANIA"] = "String";
        model_internal::propertyTypeMap["TOOTHEXAM"] = "ArrayCollection";
        model_internal::propertyTypeMap["AMBCARD"] = "ArrayCollection";
        model_internal::propertyTypeMap["HYGCONTROL"] = "ArrayCollection";
        model_internal::propertyTypeMap["ILLHISTORY"] = "ArrayCollection";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity PatientCardPDFModulePatientCardData");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity PatientCardPDFModulePatientCardData");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of PatientCardPDFModulePatientCardData");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardPDFModulePatientCardData");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity PatientCardPDFModulePatientCardData");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardPDFModulePatientCardData");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isCLINICNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCLINICLEGALNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCLINICADDRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTCARDNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTCARDDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTFIRSTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTMIDDLENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTLASTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTBIRTHDAYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTSEXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTPOSTALCODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTCOUNTRYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTSTATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTCITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTRAGIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTADDRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTELEPHONENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTMOBILENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTEMPLOYMENTPLACEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTJOBAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTWORKPHONEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTTESTDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTCARDIACDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTKIDNEYDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTLIVERDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTOTHERDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTARTERIALPRESSUREAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTRHEUMATISMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTHEPATITISAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTDIABETESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTFITAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTBLEEDINGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTALLERGYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTPREGNANCYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTMEDICATIONSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTBLOODGROUPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTRHESUSFACTORAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTMOUTHULCERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTFUNGUSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTFEVERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTTHROATACHEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTLYMPHNODESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTREDSKINAREAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTHYPERHIDROSISAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTDIARRHEAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTAMNESIAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTWEIGHTLOSSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTHEADACHEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTAIDSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTWORKWITHBLOODAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTESTNARCOMANIAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTOOTHEXAMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAMBCARDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHYGCONTROLAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isILLHISTORYAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get CLINICNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CLINICLEGALNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CLINICADDRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTCARDNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTCARDDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTFIRSTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTMIDDLENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTLASTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTBIRTHDAYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTSEXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTPOSTALCODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTCOUNTRYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTSTATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTCITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTRAGIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTADDRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTELEPHONENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTMOBILENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTEMPLOYMENTPLACEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTJOBStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTWORKPHONEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTTESTDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTCARDIACDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTKIDNEYDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTLIVERDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTOTHERDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTARTERIALPRESSUREStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTRHEUMATISMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTHEPATITISStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTDIABETESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTFITStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTBLEEDINGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTALLERGYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTPREGNANCYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTMEDICATIONSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTBLOODGROUPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTRHESUSFACTORStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTMOUTHULCERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTFUNGUSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTFEVERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTTHROATACHEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTLYMPHNODESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTREDSKINAREAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTHYPERHIDROSISStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTDIARRHEAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTAMNESIAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTWEIGHTLOSSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTHEADACHEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTAIDSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTWORKWITHBLOODStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTESTNARCOMANIAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TOOTHEXAMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AMBCARDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HYGCONTROLStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ILLHISTORYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
