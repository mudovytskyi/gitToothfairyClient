/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PatientCardModuleDataGridPatientObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.PatientCardModule_mobilephone;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PatientCardModuleDataGridPatientObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PatientCardModuleDataGridPatientObject") == null)
            {
                flash.net.registerClassAlias("PatientCardModuleDataGridPatientObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PatientCardModuleDataGridPatientObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.PatientCardModule_mobilephone.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _PatientCardModuleDataGridPatientObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_CARDNUMBER_FULL : String;
    private var _internal_CARDNUMBER : Number;
    private var _internal_CARDBEFORNUMBER : String;
    private var _internal_CARDAFTERNUMBER : String;
    private var _internal_CARDDATE : String;
    private var _internal_FIRSTNAME : String;
    private var _internal_MIDDLENAME : String;
    private var _internal_LASTNAME : String;
    private var _internal_BIRTHDAY : String;
    private var _internal_SEX : int;
    private var _internal_TELEPHONENUMBER : String;
    private var _internal_MOBILENUMBER : String;
    private var _internal_EMAIL : String;
    private var _internal_ADDRESS : String;
    private var _internal_INSURANCEID : int;
    private var _internal_TESTPATOLOGYFLAG : int;
    private var _internal_COUNTS_TOOTHEXAMS : int;
    private var _internal_COUNTS_PROCEDURES_CLOSED : int;
    private var _internal_COUNTS_PROCEDURES_NOTCLOSED : int;
    private var _internal_COUNTS_FILES : int;
    private var _internal_discount_amount : Number;
    private var _internal_discount_amount_auto : Number;
    private var _internal_INDDISCOUNT : Boolean;
    private var _internal_DISCOUNTCARDNUMBER : String;
    private var _internal_full_addres : String;
    private var _internal_full_fio : String;
    private var _internal_is_returned : int;
    private var _internal_patient_ages : int;
    private var _internal_patient_notes : String;
    private var _internal_doctors : String;
    private var _internal_doctor_lead : String;
    private var _internal_agreementTimestamp : String;
    private var _internal_group_descript : String;
    private var _internal_group_color : int;
    private var _internal_mobiles : ArrayCollection;
    model_internal var _internal_mobiles_leaf:valueObjects.PatientCardModule_mobilephone;
    private var _internal_patientsCount : int;
    private var _internal_is_archived : Boolean;
    private var _internal_subdivision_id : String;
    private var _internal_subdivision_label : String;
    private var _internal_NH_TREATTYPE : int;
    private var _internal_agreementtype : int;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PatientCardModuleDataGridPatientObject()
    {
        _model = new _PatientCardModuleDataGridPatientObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get CARDNUMBER_FULL() : String
    {
        return _internal_CARDNUMBER_FULL;
    }

    [Bindable(event="propertyChange")]
    public function get CARDNUMBER() : Number
    {
        return _internal_CARDNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CARDBEFORNUMBER() : String
    {
        return _internal_CARDBEFORNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CARDAFTERNUMBER() : String
    {
        return _internal_CARDAFTERNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CARDDATE() : String
    {
        return _internal_CARDDATE;
    }

    [Bindable(event="propertyChange")]
    public function get FIRSTNAME() : String
    {
        return _internal_FIRSTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get MIDDLENAME() : String
    {
        return _internal_MIDDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get LASTNAME() : String
    {
        return _internal_LASTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get BIRTHDAY() : String
    {
        return _internal_BIRTHDAY;
    }

    [Bindable(event="propertyChange")]
    public function get SEX() : int
    {
        return _internal_SEX;
    }

    [Bindable(event="propertyChange")]
    public function get TELEPHONENUMBER() : String
    {
        return _internal_TELEPHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get MOBILENUMBER() : String
    {
        return _internal_MOBILENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get EMAIL() : String
    {
        return _internal_EMAIL;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS() : String
    {
        return _internal_ADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get INSURANCEID() : int
    {
        return _internal_INSURANCEID;
    }

    [Bindable(event="propertyChange")]
    public function get TESTPATOLOGYFLAG() : int
    {
        return _internal_TESTPATOLOGYFLAG;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_TOOTHEXAMS() : int
    {
        return _internal_COUNTS_TOOTHEXAMS;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_PROCEDURES_CLOSED() : int
    {
        return _internal_COUNTS_PROCEDURES_CLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_PROCEDURES_NOTCLOSED() : int
    {
        return _internal_COUNTS_PROCEDURES_NOTCLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_FILES() : int
    {
        return _internal_COUNTS_FILES;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount() : Number
    {
        return _internal_discount_amount;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount_auto() : Number
    {
        return _internal_discount_amount_auto;
    }

    [Bindable(event="propertyChange")]
    public function get INDDISCOUNT() : Boolean
    {
        return _internal_INDDISCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get DISCOUNTCARDNUMBER() : String
    {
        return _internal_DISCOUNTCARDNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get full_addres() : String
    {
        return _internal_full_addres;
    }

    [Bindable(event="propertyChange")]
    public function get full_fio() : String
    {
        return _internal_full_fio;
    }

    [Bindable(event="propertyChange")]
    public function get is_returned() : int
    {
        return _internal_is_returned;
    }

    [Bindable(event="propertyChange")]
    public function get patient_ages() : int
    {
        return _internal_patient_ages;
    }

    [Bindable(event="propertyChange")]
    public function get patient_notes() : String
    {
        return _internal_patient_notes;
    }

    [Bindable(event="propertyChange")]
    public function get doctors() : String
    {
        return _internal_doctors;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lead() : String
    {
        return _internal_doctor_lead;
    }

    [Bindable(event="propertyChange")]
    public function get agreementTimestamp() : String
    {
        return _internal_agreementTimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get group_descript() : String
    {
        return _internal_group_descript;
    }

    [Bindable(event="propertyChange")]
    public function get group_color() : int
    {
        return _internal_group_color;
    }

    [Bindable(event="propertyChange")]
    public function get mobiles() : ArrayCollection
    {
        return _internal_mobiles;
    }

    [Bindable(event="propertyChange")]
    public function get patientsCount() : int
    {
        return _internal_patientsCount;
    }

    [Bindable(event="propertyChange")]
    public function get is_archived() : Boolean
    {
        return _internal_is_archived;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_id() : String
    {
        return _internal_subdivision_id;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_label() : String
    {
        return _internal_subdivision_label;
    }

    [Bindable(event="propertyChange")]
    public function get NH_TREATTYPE() : int
    {
        return _internal_NH_TREATTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get agreementtype() : int
    {
        return _internal_agreementtype;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set CARDNUMBER_FULL(value:String) : void
    {
        var oldValue:String = _internal_CARDNUMBER_FULL;
        if (oldValue !== value)
        {
            _internal_CARDNUMBER_FULL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDNUMBER_FULL", oldValue, _internal_CARDNUMBER_FULL));
        }
    }

    public function set CARDNUMBER(value:Number) : void
    {
        var oldValue:Number = _internal_CARDNUMBER;
        if (isNaN(_internal_CARDNUMBER) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_CARDNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDNUMBER", oldValue, _internal_CARDNUMBER));
        }
    }

    public function set CARDBEFORNUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARDBEFORNUMBER;
        if (oldValue !== value)
        {
            _internal_CARDBEFORNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDBEFORNUMBER", oldValue, _internal_CARDBEFORNUMBER));
        }
    }

    public function set CARDAFTERNUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARDAFTERNUMBER;
        if (oldValue !== value)
        {
            _internal_CARDAFTERNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDAFTERNUMBER", oldValue, _internal_CARDAFTERNUMBER));
        }
    }

    public function set CARDDATE(value:String) : void
    {
        var oldValue:String = _internal_CARDDATE;
        if (oldValue !== value)
        {
            _internal_CARDDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDDATE", oldValue, _internal_CARDDATE));
        }
    }

    public function set FIRSTNAME(value:String) : void
    {
        var oldValue:String = _internal_FIRSTNAME;
        if (oldValue !== value)
        {
            _internal_FIRSTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FIRSTNAME", oldValue, _internal_FIRSTNAME));
        }
    }

    public function set MIDDLENAME(value:String) : void
    {
        var oldValue:String = _internal_MIDDLENAME;
        if (oldValue !== value)
        {
            _internal_MIDDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MIDDLENAME", oldValue, _internal_MIDDLENAME));
        }
    }

    public function set LASTNAME(value:String) : void
    {
        var oldValue:String = _internal_LASTNAME;
        if (oldValue !== value)
        {
            _internal_LASTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LASTNAME", oldValue, _internal_LASTNAME));
        }
    }

    public function set BIRTHDAY(value:String) : void
    {
        var oldValue:String = _internal_BIRTHDAY;
        if (oldValue !== value)
        {
            _internal_BIRTHDAY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BIRTHDAY", oldValue, _internal_BIRTHDAY));
        }
    }

    public function set SEX(value:int) : void
    {
        var oldValue:int = _internal_SEX;
        if (oldValue !== value)
        {
            _internal_SEX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SEX", oldValue, _internal_SEX));
        }
    }

    public function set TELEPHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_TELEPHONENUMBER;
        if (oldValue !== value)
        {
            _internal_TELEPHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TELEPHONENUMBER", oldValue, _internal_TELEPHONENUMBER));
        }
    }

    public function set MOBILENUMBER(value:String) : void
    {
        var oldValue:String = _internal_MOBILENUMBER;
        if (oldValue !== value)
        {
            _internal_MOBILENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOBILENUMBER", oldValue, _internal_MOBILENUMBER));
        }
    }

    public function set EMAIL(value:String) : void
    {
        var oldValue:String = _internal_EMAIL;
        if (oldValue !== value)
        {
            _internal_EMAIL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMAIL", oldValue, _internal_EMAIL));
        }
    }

    public function set ADDRESS(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS;
        if (oldValue !== value)
        {
            _internal_ADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS", oldValue, _internal_ADDRESS));
        }
    }

    public function set INSURANCEID(value:int) : void
    {
        var oldValue:int = _internal_INSURANCEID;
        if (oldValue !== value)
        {
            _internal_INSURANCEID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INSURANCEID", oldValue, _internal_INSURANCEID));
        }
    }

    public function set TESTPATOLOGYFLAG(value:int) : void
    {
        var oldValue:int = _internal_TESTPATOLOGYFLAG;
        if (oldValue !== value)
        {
            _internal_TESTPATOLOGYFLAG = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TESTPATOLOGYFLAG", oldValue, _internal_TESTPATOLOGYFLAG));
        }
    }

    public function set COUNTS_TOOTHEXAMS(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_TOOTHEXAMS;
        if (oldValue !== value)
        {
            _internal_COUNTS_TOOTHEXAMS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_TOOTHEXAMS", oldValue, _internal_COUNTS_TOOTHEXAMS));
        }
    }

    public function set COUNTS_PROCEDURES_CLOSED(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_PROCEDURES_CLOSED;
        if (oldValue !== value)
        {
            _internal_COUNTS_PROCEDURES_CLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_PROCEDURES_CLOSED", oldValue, _internal_COUNTS_PROCEDURES_CLOSED));
        }
    }

    public function set COUNTS_PROCEDURES_NOTCLOSED(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_PROCEDURES_NOTCLOSED;
        if (oldValue !== value)
        {
            _internal_COUNTS_PROCEDURES_NOTCLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_PROCEDURES_NOTCLOSED", oldValue, _internal_COUNTS_PROCEDURES_NOTCLOSED));
        }
    }

    public function set COUNTS_FILES(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_FILES;
        if (oldValue !== value)
        {
            _internal_COUNTS_FILES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_FILES", oldValue, _internal_COUNTS_FILES));
        }
    }

    public function set discount_amount(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount;
        if (isNaN(_internal_discount_amount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount", oldValue, _internal_discount_amount));
        }
    }

    public function set discount_amount_auto(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount_auto;
        if (isNaN(_internal_discount_amount_auto) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount_auto = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount_auto", oldValue, _internal_discount_amount_auto));
        }
    }

    public function set INDDISCOUNT(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_INDDISCOUNT;
        if (oldValue !== value)
        {
            _internal_INDDISCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INDDISCOUNT", oldValue, _internal_INDDISCOUNT));
        }
    }

    public function set DISCOUNTCARDNUMBER(value:String) : void
    {
        var oldValue:String = _internal_DISCOUNTCARDNUMBER;
        if (oldValue !== value)
        {
            _internal_DISCOUNTCARDNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DISCOUNTCARDNUMBER", oldValue, _internal_DISCOUNTCARDNUMBER));
        }
    }

    public function set full_addres(value:String) : void
    {
        var oldValue:String = _internal_full_addres;
        if (oldValue !== value)
        {
            _internal_full_addres = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "full_addres", oldValue, _internal_full_addres));
        }
    }

    public function set full_fio(value:String) : void
    {
        var oldValue:String = _internal_full_fio;
        if (oldValue !== value)
        {
            _internal_full_fio = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "full_fio", oldValue, _internal_full_fio));
        }
    }

    public function set is_returned(value:int) : void
    {
        var oldValue:int = _internal_is_returned;
        if (oldValue !== value)
        {
            _internal_is_returned = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_returned", oldValue, _internal_is_returned));
        }
    }

    public function set patient_ages(value:int) : void
    {
        var oldValue:int = _internal_patient_ages;
        if (oldValue !== value)
        {
            _internal_patient_ages = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_ages", oldValue, _internal_patient_ages));
        }
    }

    public function set patient_notes(value:String) : void
    {
        var oldValue:String = _internal_patient_notes;
        if (oldValue !== value)
        {
            _internal_patient_notes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_notes", oldValue, _internal_patient_notes));
        }
    }

    public function set doctors(value:String) : void
    {
        var oldValue:String = _internal_doctors;
        if (oldValue !== value)
        {
            _internal_doctors = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctors", oldValue, _internal_doctors));
        }
    }

    public function set doctor_lead(value:String) : void
    {
        var oldValue:String = _internal_doctor_lead;
        if (oldValue !== value)
        {
            _internal_doctor_lead = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lead", oldValue, _internal_doctor_lead));
        }
    }

    public function set agreementTimestamp(value:String) : void
    {
        var oldValue:String = _internal_agreementTimestamp;
        if (oldValue !== value)
        {
            _internal_agreementTimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "agreementTimestamp", oldValue, _internal_agreementTimestamp));
        }
    }

    public function set group_descript(value:String) : void
    {
        var oldValue:String = _internal_group_descript;
        if (oldValue !== value)
        {
            _internal_group_descript = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_descript", oldValue, _internal_group_descript));
        }
    }

    public function set group_color(value:int) : void
    {
        var oldValue:int = _internal_group_color;
        if (oldValue !== value)
        {
            _internal_group_color = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_color", oldValue, _internal_group_color));
        }
    }

    public function set mobiles(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_mobiles;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_mobiles = value;
            }
            else if (value is Array)
            {
                _internal_mobiles = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_mobiles = null;
            }
            else
            {
                throw new Error("value of mobiles must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "mobiles", oldValue, _internal_mobiles));
        }
    }

    public function set patientsCount(value:int) : void
    {
        var oldValue:int = _internal_patientsCount;
        if (oldValue !== value)
        {
            _internal_patientsCount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientsCount", oldValue, _internal_patientsCount));
        }
    }

    public function set is_archived(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_is_archived;
        if (oldValue !== value)
        {
            _internal_is_archived = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_archived", oldValue, _internal_is_archived));
        }
    }

    public function set subdivision_id(value:String) : void
    {
        var oldValue:String = _internal_subdivision_id;
        if (oldValue !== value)
        {
            _internal_subdivision_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_id", oldValue, _internal_subdivision_id));
        }
    }

    public function set subdivision_label(value:String) : void
    {
        var oldValue:String = _internal_subdivision_label;
        if (oldValue !== value)
        {
            _internal_subdivision_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_label", oldValue, _internal_subdivision_label));
        }
    }

    public function set NH_TREATTYPE(value:int) : void
    {
        var oldValue:int = _internal_NH_TREATTYPE;
        if (oldValue !== value)
        {
            _internal_NH_TREATTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NH_TREATTYPE", oldValue, _internal_NH_TREATTYPE));
        }
    }

    public function set agreementtype(value:int) : void
    {
        var oldValue:int = _internal_agreementtype;
        if (oldValue !== value)
        {
            _internal_agreementtype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "agreementtype", oldValue, _internal_agreementtype));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PatientCardModuleDataGridPatientObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PatientCardModuleDataGridPatientObjectEntityMetadata) : void
    {
        var oldValue : _PatientCardModuleDataGridPatientObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
