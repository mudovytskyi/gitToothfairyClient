/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - DispanserModule_Tasks.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.DispanserModule_Task;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_DispanserModule_Tasks extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("DispanserModule_Tasks") == null)
            {
                flash.net.registerClassAlias("DispanserModule_Tasks", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("DispanserModule_Tasks", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.DispanserModule_Task.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10TreatmentAdditions.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Doctor.initRemoteClassAliasSingleChild();
        valueObjects.DispanserModule_mobilephone.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _DispanserModule_TasksEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_remined : ArrayCollection;
    model_internal var _internal_remined_leaf:valueObjects.DispanserModule_Task;
    private var _internal_reminedCount : int;
    private var _internal_complete : ArrayCollection;
    model_internal var _internal_complete_leaf:valueObjects.DispanserModule_Task;
    private var _internal_completeCount : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_DispanserModule_Tasks()
    {
        _model = new _DispanserModule_TasksEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get remined() : ArrayCollection
    {
        return _internal_remined;
    }

    [Bindable(event="propertyChange")]
    public function get reminedCount() : int
    {
        return _internal_reminedCount;
    }

    [Bindable(event="propertyChange")]
    public function get complete() : ArrayCollection
    {
        return _internal_complete;
    }

    [Bindable(event="propertyChange")]
    public function get completeCount() : int
    {
        return _internal_completeCount;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set remined(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_remined;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_remined = value;
            }
            else if (value is Array)
            {
                _internal_remined = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_remined = null;
            }
            else
            {
                throw new Error("value of remined must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "remined", oldValue, _internal_remined));
        }
    }

    public function set reminedCount(value:int) : void
    {
        var oldValue:int = _internal_reminedCount;
        if (oldValue !== value)
        {
            _internal_reminedCount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "reminedCount", oldValue, _internal_reminedCount));
        }
    }

    public function set complete(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_complete;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_complete = value;
            }
            else if (value is Array)
            {
                _internal_complete = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_complete = null;
            }
            else
            {
                throw new Error("value of complete must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "complete", oldValue, _internal_complete));
        }
    }

    public function set completeCount(value:int) : void
    {
        var oldValue:int = _internal_completeCount;
        if (oldValue !== value)
        {
            _internal_completeCount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "completeCount", oldValue, _internal_completeCount));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _DispanserModule_TasksEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _DispanserModule_TasksEntityMetadata) : void
    {
        var oldValue : _DispanserModule_TasksEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
