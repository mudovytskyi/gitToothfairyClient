
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.HospitalCardDiagnosis;
import valueObjects.HospitalCardDisabilityCertificate;
import valueObjects.HospitalCardProcedure;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _HospitalCardDataFullEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ID", "PATIENT_ID", "CARD_NUMBER", "CREATED", "OPENED", "CLOSED", "CLINIC_NAME", "CLINIC_ADDRESS", "CLINIC_REGISTRATION_CODE", "PATIENT_SEX", "PATIENT_FULL_NAME", "PATIENT_BIRTH", "PATIENT_AGE", "PATIENT_DOCUMENT", "PATIENT_DOCUMENT_NUMBER", "PATIENT_CITIZENSHIP", "HABITATION_TYPE", "ADDRESS_PART_1", "ADDRESS_PART_2", "POSTAL_CODE", "WORKPLACE_PART_1", "WORKPLACE_PART_2", "REFFERENT_CLINIC", "REFFERENT_REGISTRATION_CODE", "INCOME_DIAGNOSIS", "INCOME_DIAGNOSIS_MKH", "DEPARTMENT_CODE_INCOME", "DEPARTMENT_CODE_OUTCOME", "HOSPITALIZATION_TYPE", "LAST_HIV_PROBE_DATE", "BLOOD_TYPE", "BLOOD_RH", "LAST_VASSERMAN_REACTION_DATE", "FARM_ALLERGY_REACTIONS_PART_1", "FARM_ALLERGY_REACTIONS_PART_2", "HOSPITALIZATION_REPEATED_IN_YEAR", "HOSPITALIZATION_REPEATED_IN_MONTH", "TREATED_BED_DAYS", "INJURY_TYPE", "ADDITIONAL_OUTCOME_DIAGNOSIS", "RESISTANTION_CATEGORY", "DOCTOR_FULL_NAME_DIAGNOS", "DOCTOR_SIGN_DIAGNOS", "DOCTOR_REGISTRATION_DIAGNOS", "DOCTOR_DATE_DIAGNOS", "OTHER_TREATMENTS", "CANCER_TREATMENT_TYPE", "EMPLOYABILITY", "EXPERTISE_CONCLUSION", "TREATMENT_RESULT", "ONCOLOGY_CHECKUP_DATE", "CHEST_ORGANS_CHECKUP_DATE", "INSURANCY", "DOCTOR_FULL_NAME_OUTCOME", "DOCTOR_SIGN_OUTCOME", "DOCTOR_REGISTRATION_OUTCOME", "HEADDEPARTMENT_FULL_NAME", "HEADDEPARTMENT_SIGN", "HEADDEPARTMENT_REGISTRATION", "hospitalProcedueres", "hospitalDiagnosis", "hospitalDisabilityCerts");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ID", "PATIENT_ID", "CARD_NUMBER", "CREATED", "OPENED", "CLOSED", "CLINIC_NAME", "CLINIC_ADDRESS", "CLINIC_REGISTRATION_CODE", "PATIENT_SEX", "PATIENT_FULL_NAME", "PATIENT_BIRTH", "PATIENT_AGE", "PATIENT_DOCUMENT", "PATIENT_DOCUMENT_NUMBER", "PATIENT_CITIZENSHIP", "HABITATION_TYPE", "ADDRESS_PART_1", "ADDRESS_PART_2", "POSTAL_CODE", "WORKPLACE_PART_1", "WORKPLACE_PART_2", "REFFERENT_CLINIC", "REFFERENT_REGISTRATION_CODE", "INCOME_DIAGNOSIS", "INCOME_DIAGNOSIS_MKH", "DEPARTMENT_CODE_INCOME", "DEPARTMENT_CODE_OUTCOME", "HOSPITALIZATION_TYPE", "LAST_HIV_PROBE_DATE", "BLOOD_TYPE", "BLOOD_RH", "LAST_VASSERMAN_REACTION_DATE", "FARM_ALLERGY_REACTIONS_PART_1", "FARM_ALLERGY_REACTIONS_PART_2", "HOSPITALIZATION_REPEATED_IN_YEAR", "HOSPITALIZATION_REPEATED_IN_MONTH", "TREATED_BED_DAYS", "INJURY_TYPE", "ADDITIONAL_OUTCOME_DIAGNOSIS", "RESISTANTION_CATEGORY", "DOCTOR_FULL_NAME_DIAGNOS", "DOCTOR_SIGN_DIAGNOS", "DOCTOR_REGISTRATION_DIAGNOS", "DOCTOR_DATE_DIAGNOS", "OTHER_TREATMENTS", "CANCER_TREATMENT_TYPE", "EMPLOYABILITY", "EXPERTISE_CONCLUSION", "TREATMENT_RESULT", "ONCOLOGY_CHECKUP_DATE", "CHEST_ORGANS_CHECKUP_DATE", "INSURANCY", "DOCTOR_FULL_NAME_OUTCOME", "DOCTOR_SIGN_OUTCOME", "DOCTOR_REGISTRATION_OUTCOME", "HEADDEPARTMENT_FULL_NAME", "HEADDEPARTMENT_SIGN", "HEADDEPARTMENT_REGISTRATION", "hospitalProcedueres", "hospitalDiagnosis", "hospitalDisabilityCerts");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ID", "PATIENT_ID", "CARD_NUMBER", "CREATED", "OPENED", "CLOSED", "CLINIC_NAME", "CLINIC_ADDRESS", "CLINIC_REGISTRATION_CODE", "PATIENT_SEX", "PATIENT_FULL_NAME", "PATIENT_BIRTH", "PATIENT_AGE", "PATIENT_DOCUMENT", "PATIENT_DOCUMENT_NUMBER", "PATIENT_CITIZENSHIP", "HABITATION_TYPE", "ADDRESS_PART_1", "ADDRESS_PART_2", "POSTAL_CODE", "WORKPLACE_PART_1", "WORKPLACE_PART_2", "REFFERENT_CLINIC", "REFFERENT_REGISTRATION_CODE", "INCOME_DIAGNOSIS", "INCOME_DIAGNOSIS_MKH", "DEPARTMENT_CODE_INCOME", "DEPARTMENT_CODE_OUTCOME", "HOSPITALIZATION_TYPE", "LAST_HIV_PROBE_DATE", "BLOOD_TYPE", "BLOOD_RH", "LAST_VASSERMAN_REACTION_DATE", "FARM_ALLERGY_REACTIONS_PART_1", "FARM_ALLERGY_REACTIONS_PART_2", "HOSPITALIZATION_REPEATED_IN_YEAR", "HOSPITALIZATION_REPEATED_IN_MONTH", "TREATED_BED_DAYS", "INJURY_TYPE", "ADDITIONAL_OUTCOME_DIAGNOSIS", "RESISTANTION_CATEGORY", "DOCTOR_FULL_NAME_DIAGNOS", "DOCTOR_SIGN_DIAGNOS", "DOCTOR_REGISTRATION_DIAGNOS", "DOCTOR_DATE_DIAGNOS", "OTHER_TREATMENTS", "CANCER_TREATMENT_TYPE", "EMPLOYABILITY", "EXPERTISE_CONCLUSION", "TREATMENT_RESULT", "ONCOLOGY_CHECKUP_DATE", "CHEST_ORGANS_CHECKUP_DATE", "INSURANCY", "DOCTOR_FULL_NAME_OUTCOME", "DOCTOR_SIGN_OUTCOME", "DOCTOR_REGISTRATION_OUTCOME", "HEADDEPARTMENT_FULL_NAME", "HEADDEPARTMENT_SIGN", "HEADDEPARTMENT_REGISTRATION", "hospitalProcedueres", "hospitalDiagnosis", "hospitalDisabilityCerts");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ID", "PATIENT_ID", "CARD_NUMBER", "CREATED", "OPENED", "CLOSED", "CLINIC_NAME", "CLINIC_ADDRESS", "CLINIC_REGISTRATION_CODE", "PATIENT_SEX", "PATIENT_FULL_NAME", "PATIENT_BIRTH", "PATIENT_AGE", "PATIENT_DOCUMENT", "PATIENT_DOCUMENT_NUMBER", "PATIENT_CITIZENSHIP", "HABITATION_TYPE", "ADDRESS_PART_1", "ADDRESS_PART_2", "POSTAL_CODE", "WORKPLACE_PART_1", "WORKPLACE_PART_2", "REFFERENT_CLINIC", "REFFERENT_REGISTRATION_CODE", "INCOME_DIAGNOSIS", "INCOME_DIAGNOSIS_MKH", "DEPARTMENT_CODE_INCOME", "DEPARTMENT_CODE_OUTCOME", "HOSPITALIZATION_TYPE", "LAST_HIV_PROBE_DATE", "BLOOD_TYPE", "BLOOD_RH", "LAST_VASSERMAN_REACTION_DATE", "FARM_ALLERGY_REACTIONS_PART_1", "FARM_ALLERGY_REACTIONS_PART_2", "HOSPITALIZATION_REPEATED_IN_YEAR", "HOSPITALIZATION_REPEATED_IN_MONTH", "TREATED_BED_DAYS", "INJURY_TYPE", "ADDITIONAL_OUTCOME_DIAGNOSIS", "RESISTANTION_CATEGORY", "DOCTOR_FULL_NAME_DIAGNOS", "DOCTOR_SIGN_DIAGNOS", "DOCTOR_REGISTRATION_DIAGNOS", "DOCTOR_DATE_DIAGNOS", "OTHER_TREATMENTS", "CANCER_TREATMENT_TYPE", "EMPLOYABILITY", "EXPERTISE_CONCLUSION", "TREATMENT_RESULT", "ONCOLOGY_CHECKUP_DATE", "CHEST_ORGANS_CHECKUP_DATE", "INSURANCY", "DOCTOR_FULL_NAME_OUTCOME", "DOCTOR_SIGN_OUTCOME", "DOCTOR_REGISTRATION_OUTCOME", "HEADDEPARTMENT_FULL_NAME", "HEADDEPARTMENT_SIGN", "HEADDEPARTMENT_REGISTRATION", "hospitalProcedueres", "hospitalDiagnosis", "hospitalDisabilityCerts");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("hospitalProcedueres", "hospitalDiagnosis", "hospitalDisabilityCerts");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "HospitalCardDataFull";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_HospitalCardDataFull;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _HospitalCardDataFullEntityMetadata(value : _Super_HospitalCardDataFull)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["PATIENT_ID"] = new Array();
            model_internal::dependentsOnMap["CARD_NUMBER"] = new Array();
            model_internal::dependentsOnMap["CREATED"] = new Array();
            model_internal::dependentsOnMap["OPENED"] = new Array();
            model_internal::dependentsOnMap["CLOSED"] = new Array();
            model_internal::dependentsOnMap["CLINIC_NAME"] = new Array();
            model_internal::dependentsOnMap["CLINIC_ADDRESS"] = new Array();
            model_internal::dependentsOnMap["CLINIC_REGISTRATION_CODE"] = new Array();
            model_internal::dependentsOnMap["PATIENT_SEX"] = new Array();
            model_internal::dependentsOnMap["PATIENT_FULL_NAME"] = new Array();
            model_internal::dependentsOnMap["PATIENT_BIRTH"] = new Array();
            model_internal::dependentsOnMap["PATIENT_AGE"] = new Array();
            model_internal::dependentsOnMap["PATIENT_DOCUMENT"] = new Array();
            model_internal::dependentsOnMap["PATIENT_DOCUMENT_NUMBER"] = new Array();
            model_internal::dependentsOnMap["PATIENT_CITIZENSHIP"] = new Array();
            model_internal::dependentsOnMap["HABITATION_TYPE"] = new Array();
            model_internal::dependentsOnMap["ADDRESS_PART_1"] = new Array();
            model_internal::dependentsOnMap["ADDRESS_PART_2"] = new Array();
            model_internal::dependentsOnMap["POSTAL_CODE"] = new Array();
            model_internal::dependentsOnMap["WORKPLACE_PART_1"] = new Array();
            model_internal::dependentsOnMap["WORKPLACE_PART_2"] = new Array();
            model_internal::dependentsOnMap["REFFERENT_CLINIC"] = new Array();
            model_internal::dependentsOnMap["REFFERENT_REGISTRATION_CODE"] = new Array();
            model_internal::dependentsOnMap["INCOME_DIAGNOSIS"] = new Array();
            model_internal::dependentsOnMap["INCOME_DIAGNOSIS_MKH"] = new Array();
            model_internal::dependentsOnMap["DEPARTMENT_CODE_INCOME"] = new Array();
            model_internal::dependentsOnMap["DEPARTMENT_CODE_OUTCOME"] = new Array();
            model_internal::dependentsOnMap["HOSPITALIZATION_TYPE"] = new Array();
            model_internal::dependentsOnMap["LAST_HIV_PROBE_DATE"] = new Array();
            model_internal::dependentsOnMap["BLOOD_TYPE"] = new Array();
            model_internal::dependentsOnMap["BLOOD_RH"] = new Array();
            model_internal::dependentsOnMap["LAST_VASSERMAN_REACTION_DATE"] = new Array();
            model_internal::dependentsOnMap["FARM_ALLERGY_REACTIONS_PART_1"] = new Array();
            model_internal::dependentsOnMap["FARM_ALLERGY_REACTIONS_PART_2"] = new Array();
            model_internal::dependentsOnMap["HOSPITALIZATION_REPEATED_IN_YEAR"] = new Array();
            model_internal::dependentsOnMap["HOSPITALIZATION_REPEATED_IN_MONTH"] = new Array();
            model_internal::dependentsOnMap["TREATED_BED_DAYS"] = new Array();
            model_internal::dependentsOnMap["INJURY_TYPE"] = new Array();
            model_internal::dependentsOnMap["ADDITIONAL_OUTCOME_DIAGNOSIS"] = new Array();
            model_internal::dependentsOnMap["RESISTANTION_CATEGORY"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_FULL_NAME_DIAGNOS"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_SIGN_DIAGNOS"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_REGISTRATION_DIAGNOS"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_DATE_DIAGNOS"] = new Array();
            model_internal::dependentsOnMap["OTHER_TREATMENTS"] = new Array();
            model_internal::dependentsOnMap["CANCER_TREATMENT_TYPE"] = new Array();
            model_internal::dependentsOnMap["EMPLOYABILITY"] = new Array();
            model_internal::dependentsOnMap["EXPERTISE_CONCLUSION"] = new Array();
            model_internal::dependentsOnMap["TREATMENT_RESULT"] = new Array();
            model_internal::dependentsOnMap["ONCOLOGY_CHECKUP_DATE"] = new Array();
            model_internal::dependentsOnMap["CHEST_ORGANS_CHECKUP_DATE"] = new Array();
            model_internal::dependentsOnMap["INSURANCY"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_FULL_NAME_OUTCOME"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_SIGN_OUTCOME"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_REGISTRATION_OUTCOME"] = new Array();
            model_internal::dependentsOnMap["HEADDEPARTMENT_FULL_NAME"] = new Array();
            model_internal::dependentsOnMap["HEADDEPARTMENT_SIGN"] = new Array();
            model_internal::dependentsOnMap["HEADDEPARTMENT_REGISTRATION"] = new Array();
            model_internal::dependentsOnMap["hospitalProcedueres"] = new Array();
            model_internal::dependentsOnMap["hospitalDiagnosis"] = new Array();
            model_internal::dependentsOnMap["hospitalDisabilityCerts"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["hospitalProcedueres"] = "valueObjects.HospitalCardProcedure";
            model_internal::collectionBaseMap["hospitalDiagnosis"] = "valueObjects.HospitalCardDiagnosis";
            model_internal::collectionBaseMap["hospitalDisabilityCerts"] = "valueObjects.HospitalCardDisabilityCertificate";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ID"] = "String";
        model_internal::propertyTypeMap["PATIENT_ID"] = "String";
        model_internal::propertyTypeMap["CARD_NUMBER"] = "String";
        model_internal::propertyTypeMap["CREATED"] = "String";
        model_internal::propertyTypeMap["OPENED"] = "String";
        model_internal::propertyTypeMap["CLOSED"] = "String";
        model_internal::propertyTypeMap["CLINIC_NAME"] = "String";
        model_internal::propertyTypeMap["CLINIC_ADDRESS"] = "String";
        model_internal::propertyTypeMap["CLINIC_REGISTRATION_CODE"] = "String";
        model_internal::propertyTypeMap["PATIENT_SEX"] = "String";
        model_internal::propertyTypeMap["PATIENT_FULL_NAME"] = "String";
        model_internal::propertyTypeMap["PATIENT_BIRTH"] = "String";
        model_internal::propertyTypeMap["PATIENT_AGE"] = "String";
        model_internal::propertyTypeMap["PATIENT_DOCUMENT"] = "String";
        model_internal::propertyTypeMap["PATIENT_DOCUMENT_NUMBER"] = "String";
        model_internal::propertyTypeMap["PATIENT_CITIZENSHIP"] = "String";
        model_internal::propertyTypeMap["HABITATION_TYPE"] = "String";
        model_internal::propertyTypeMap["ADDRESS_PART_1"] = "String";
        model_internal::propertyTypeMap["ADDRESS_PART_2"] = "String";
        model_internal::propertyTypeMap["POSTAL_CODE"] = "String";
        model_internal::propertyTypeMap["WORKPLACE_PART_1"] = "String";
        model_internal::propertyTypeMap["WORKPLACE_PART_2"] = "String";
        model_internal::propertyTypeMap["REFFERENT_CLINIC"] = "String";
        model_internal::propertyTypeMap["REFFERENT_REGISTRATION_CODE"] = "String";
        model_internal::propertyTypeMap["INCOME_DIAGNOSIS"] = "String";
        model_internal::propertyTypeMap["INCOME_DIAGNOSIS_MKH"] = "String";
        model_internal::propertyTypeMap["DEPARTMENT_CODE_INCOME"] = "String";
        model_internal::propertyTypeMap["DEPARTMENT_CODE_OUTCOME"] = "String";
        model_internal::propertyTypeMap["HOSPITALIZATION_TYPE"] = "String";
        model_internal::propertyTypeMap["LAST_HIV_PROBE_DATE"] = "String";
        model_internal::propertyTypeMap["BLOOD_TYPE"] = "String";
        model_internal::propertyTypeMap["BLOOD_RH"] = "String";
        model_internal::propertyTypeMap["LAST_VASSERMAN_REACTION_DATE"] = "String";
        model_internal::propertyTypeMap["FARM_ALLERGY_REACTIONS_PART_1"] = "String";
        model_internal::propertyTypeMap["FARM_ALLERGY_REACTIONS_PART_2"] = "String";
        model_internal::propertyTypeMap["HOSPITALIZATION_REPEATED_IN_YEAR"] = "String";
        model_internal::propertyTypeMap["HOSPITALIZATION_REPEATED_IN_MONTH"] = "String";
        model_internal::propertyTypeMap["TREATED_BED_DAYS"] = "String";
        model_internal::propertyTypeMap["INJURY_TYPE"] = "String";
        model_internal::propertyTypeMap["ADDITIONAL_OUTCOME_DIAGNOSIS"] = "String";
        model_internal::propertyTypeMap["RESISTANTION_CATEGORY"] = "String";
        model_internal::propertyTypeMap["DOCTOR_FULL_NAME_DIAGNOS"] = "String";
        model_internal::propertyTypeMap["DOCTOR_SIGN_DIAGNOS"] = "String";
        model_internal::propertyTypeMap["DOCTOR_REGISTRATION_DIAGNOS"] = "String";
        model_internal::propertyTypeMap["DOCTOR_DATE_DIAGNOS"] = "String";
        model_internal::propertyTypeMap["OTHER_TREATMENTS"] = "String";
        model_internal::propertyTypeMap["CANCER_TREATMENT_TYPE"] = "String";
        model_internal::propertyTypeMap["EMPLOYABILITY"] = "String";
        model_internal::propertyTypeMap["EXPERTISE_CONCLUSION"] = "String";
        model_internal::propertyTypeMap["TREATMENT_RESULT"] = "String";
        model_internal::propertyTypeMap["ONCOLOGY_CHECKUP_DATE"] = "String";
        model_internal::propertyTypeMap["CHEST_ORGANS_CHECKUP_DATE"] = "String";
        model_internal::propertyTypeMap["INSURANCY"] = "String";
        model_internal::propertyTypeMap["DOCTOR_FULL_NAME_OUTCOME"] = "String";
        model_internal::propertyTypeMap["DOCTOR_SIGN_OUTCOME"] = "String";
        model_internal::propertyTypeMap["DOCTOR_REGISTRATION_OUTCOME"] = "String";
        model_internal::propertyTypeMap["HEADDEPARTMENT_FULL_NAME"] = "String";
        model_internal::propertyTypeMap["HEADDEPARTMENT_SIGN"] = "String";
        model_internal::propertyTypeMap["HEADDEPARTMENT_REGISTRATION"] = "String";
        model_internal::propertyTypeMap["hospitalProcedueres"] = "ArrayCollection";
        model_internal::propertyTypeMap["hospitalDiagnosis"] = "ArrayCollection";
        model_internal::propertyTypeMap["hospitalDisabilityCerts"] = "ArrayCollection";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity HospitalCardDataFull");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity HospitalCardDataFull");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of HospitalCardDataFull");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity HospitalCardDataFull");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity HospitalCardDataFull");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity HospitalCardDataFull");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARD_NUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCREATEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOPENEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCLOSEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCLINIC_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCLINIC_ADDRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCLINIC_REGISTRATION_CODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_SEXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_FULL_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_BIRTHAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_AGEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_DOCUMENTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_DOCUMENT_NUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_CITIZENSHIPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHABITATION_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESS_PART_1Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESS_PART_2Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPOSTAL_CODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWORKPLACE_PART_1Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWORKPLACE_PART_2Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isREFFERENT_CLINICAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isREFFERENT_REGISTRATION_CODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINCOME_DIAGNOSISAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINCOME_DIAGNOSIS_MKHAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDEPARTMENT_CODE_INCOMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDEPARTMENT_CODE_OUTCOMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHOSPITALIZATION_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLAST_HIV_PROBE_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBLOOD_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBLOOD_RHAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLAST_VASSERMAN_REACTION_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_ALLERGY_REACTIONS_PART_1Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_ALLERGY_REACTIONS_PART_2Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHOSPITALIZATION_REPEATED_IN_YEARAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHOSPITALIZATION_REPEATED_IN_MONTHAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATED_BED_DAYSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINJURY_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDITIONAL_OUTCOME_DIAGNOSISAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRESISTANTION_CATEGORYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_FULL_NAME_DIAGNOSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_SIGN_DIAGNOSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_REGISTRATION_DIAGNOSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_DATE_DIAGNOSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOTHER_TREATMENTSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCANCER_TREATMENT_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEMPLOYABILITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEXPERTISE_CONCLUSIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENT_RESULTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isONCOLOGY_CHECKUP_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCHEST_ORGANS_CHECKUP_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINSURANCYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_FULL_NAME_OUTCOMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_SIGN_OUTCOMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_REGISTRATION_OUTCOMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEADDEPARTMENT_FULL_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEADDEPARTMENT_SIGNAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEADDEPARTMENT_REGISTRATIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHospitalProcedueresAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHospitalDiagnosisAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHospitalDisabilityCertsAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARD_NUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CREATEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get OPENEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CLOSEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CLINIC_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CLINIC_ADDRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CLINIC_REGISTRATION_CODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_SEXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_FULL_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_BIRTHStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_AGEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_DOCUMENTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_DOCUMENT_NUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_CITIZENSHIPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HABITATION_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESS_PART_1Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESS_PART_2Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get POSTAL_CODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WORKPLACE_PART_1Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WORKPLACE_PART_2Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get REFFERENT_CLINICStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get REFFERENT_REGISTRATION_CODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INCOME_DIAGNOSISStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INCOME_DIAGNOSIS_MKHStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DEPARTMENT_CODE_INCOMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DEPARTMENT_CODE_OUTCOMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HOSPITALIZATION_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LAST_HIV_PROBE_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BLOOD_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BLOOD_RHStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LAST_VASSERMAN_REACTION_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_ALLERGY_REACTIONS_PART_1Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_ALLERGY_REACTIONS_PART_2Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HOSPITALIZATION_REPEATED_IN_YEARStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HOSPITALIZATION_REPEATED_IN_MONTHStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATED_BED_DAYSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INJURY_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDITIONAL_OUTCOME_DIAGNOSISStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get RESISTANTION_CATEGORYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_FULL_NAME_DIAGNOSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_SIGN_DIAGNOSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_REGISTRATION_DIAGNOSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_DATE_DIAGNOSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get OTHER_TREATMENTSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CANCER_TREATMENT_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EMPLOYABILITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EXPERTISE_CONCLUSIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENT_RESULTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ONCOLOGY_CHECKUP_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CHEST_ORGANS_CHECKUP_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INSURANCYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_FULL_NAME_OUTCOMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_SIGN_OUTCOMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_REGISTRATION_OUTCOMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEADDEPARTMENT_FULL_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEADDEPARTMENT_SIGNStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEADDEPARTMENT_REGISTRATIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get hospitalProcedueresStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get hospitalDiagnosisStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get hospitalDisabilityCertsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
