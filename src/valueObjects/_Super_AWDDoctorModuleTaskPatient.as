/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AWDDoctorModuleTaskPatient.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.AWDDoctorModule_mobilephone;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AWDDoctorModuleTaskPatient extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AWDDoctorModuleTaskPatient") == null)
            {
                flash.net.registerClassAlias("AWDDoctorModuleTaskPatient", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AWDDoctorModuleTaskPatient", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.AWDDoctorModule_mobilephone.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _AWDDoctorModuleTaskPatientEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_TaskID : String;
    private var _internal_TaskBeginTime : String;
    private var _internal_TaskEndTime : String;
    private var _internal_TaskFactVisit : String;
    private var _internal_TaskFactOutVisit : String;
    private var _internal_TaskNoticed : String;
    private var _internal_TaskWorkDescription : String;
    private var _internal_TaskComment : String;
    private var _internal_PatientID : String;
    private var _internal_PatientLastname : String;
    private var _internal_PatientFirstname : String;
    private var _internal_PatientMiddlename : String;
    private var _internal_PatientFullCardNumber : String;
    private var _internal_PatientPrimaryFlag : String;
    private var _internal_PatientTelephone : String;
    private var _internal_PatientMobile : String;
    private var _internal_PatientBirthday : String;
    private var _internal_PatientSex : String;
    private var _internal_PatientTestFlag : String;
    private var _internal_InvoiceToSaveEnableFlag : int;
    private var _internal_COUNTS_TOOTHEXAMS : int;
    private var _internal_COUNTS_PROCEDURES_CLOSED : int;
    private var _internal_COUNTS_PROCEDURES_NOTCLOSED : int;
    private var _internal_COUNTS_FILES : int;
    private var _internal_task_range : String;
    private var _internal_full_addres : String;
    private var _internal_full_fio : String;
    private var _internal_discount_amount : Number;
    private var _internal_is_returned : int;
    private var _internal_patient_ages : int;
    private var _internal_patient_notes : String;
    private var _internal_patient_leaddoctor : String;
    private var _internal_mobiles : ArrayCollection;
    model_internal var _internal_mobiles_leaf:valueObjects.AWDDoctorModule_mobilephone;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AWDDoctorModuleTaskPatient()
    {
        _model = new _AWDDoctorModuleTaskPatientEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get TaskID() : String
    {
        return _internal_TaskID;
    }

    [Bindable(event="propertyChange")]
    public function get TaskBeginTime() : String
    {
        return _internal_TaskBeginTime;
    }

    [Bindable(event="propertyChange")]
    public function get TaskEndTime() : String
    {
        return _internal_TaskEndTime;
    }

    [Bindable(event="propertyChange")]
    public function get TaskFactVisit() : String
    {
        return _internal_TaskFactVisit;
    }

    [Bindable(event="propertyChange")]
    public function get TaskFactOutVisit() : String
    {
        return _internal_TaskFactOutVisit;
    }

    [Bindable(event="propertyChange")]
    public function get TaskNoticed() : String
    {
        return _internal_TaskNoticed;
    }

    [Bindable(event="propertyChange")]
    public function get TaskWorkDescription() : String
    {
        return _internal_TaskWorkDescription;
    }

    [Bindable(event="propertyChange")]
    public function get TaskComment() : String
    {
        return _internal_TaskComment;
    }

    [Bindable(event="propertyChange")]
    public function get PatientID() : String
    {
        return _internal_PatientID;
    }

    [Bindable(event="propertyChange")]
    public function get PatientLastname() : String
    {
        return _internal_PatientLastname;
    }

    [Bindable(event="propertyChange")]
    public function get PatientFirstname() : String
    {
        return _internal_PatientFirstname;
    }

    [Bindable(event="propertyChange")]
    public function get PatientMiddlename() : String
    {
        return _internal_PatientMiddlename;
    }

    [Bindable(event="propertyChange")]
    public function get PatientFullCardNumber() : String
    {
        return _internal_PatientFullCardNumber;
    }

    [Bindable(event="propertyChange")]
    public function get PatientPrimaryFlag() : String
    {
        return _internal_PatientPrimaryFlag;
    }

    [Bindable(event="propertyChange")]
    public function get PatientTelephone() : String
    {
        return _internal_PatientTelephone;
    }

    [Bindable(event="propertyChange")]
    public function get PatientMobile() : String
    {
        return _internal_PatientMobile;
    }

    [Bindable(event="propertyChange")]
    public function get PatientBirthday() : String
    {
        return _internal_PatientBirthday;
    }

    [Bindable(event="propertyChange")]
    public function get PatientSex() : String
    {
        return _internal_PatientSex;
    }

    [Bindable(event="propertyChange")]
    public function get PatientTestFlag() : String
    {
        return _internal_PatientTestFlag;
    }

    [Bindable(event="propertyChange")]
    public function get InvoiceToSaveEnableFlag() : int
    {
        return _internal_InvoiceToSaveEnableFlag;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_TOOTHEXAMS() : int
    {
        return _internal_COUNTS_TOOTHEXAMS;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_PROCEDURES_CLOSED() : int
    {
        return _internal_COUNTS_PROCEDURES_CLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_PROCEDURES_NOTCLOSED() : int
    {
        return _internal_COUNTS_PROCEDURES_NOTCLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_FILES() : int
    {
        return _internal_COUNTS_FILES;
    }

    [Bindable(event="propertyChange")]
    public function get task_range() : String
    {
        return _internal_task_range;
    }

    [Bindable(event="propertyChange")]
    public function get full_addres() : String
    {
        return _internal_full_addres;
    }

    [Bindable(event="propertyChange")]
    public function get full_fio() : String
    {
        return _internal_full_fio;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount() : Number
    {
        return _internal_discount_amount;
    }

    [Bindable(event="propertyChange")]
    public function get is_returned() : int
    {
        return _internal_is_returned;
    }

    [Bindable(event="propertyChange")]
    public function get patient_ages() : int
    {
        return _internal_patient_ages;
    }

    [Bindable(event="propertyChange")]
    public function get patient_notes() : String
    {
        return _internal_patient_notes;
    }

    [Bindable(event="propertyChange")]
    public function get patient_leaddoctor() : String
    {
        return _internal_patient_leaddoctor;
    }

    [Bindable(event="propertyChange")]
    public function get mobiles() : ArrayCollection
    {
        return _internal_mobiles;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set TaskID(value:String) : void
    {
        var oldValue:String = _internal_TaskID;
        if (oldValue !== value)
        {
            _internal_TaskID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskID", oldValue, _internal_TaskID));
        }
    }

    public function set TaskBeginTime(value:String) : void
    {
        var oldValue:String = _internal_TaskBeginTime;
        if (oldValue !== value)
        {
            _internal_TaskBeginTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskBeginTime", oldValue, _internal_TaskBeginTime));
        }
    }

    public function set TaskEndTime(value:String) : void
    {
        var oldValue:String = _internal_TaskEndTime;
        if (oldValue !== value)
        {
            _internal_TaskEndTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskEndTime", oldValue, _internal_TaskEndTime));
        }
    }

    public function set TaskFactVisit(value:String) : void
    {
        var oldValue:String = _internal_TaskFactVisit;
        if (oldValue !== value)
        {
            _internal_TaskFactVisit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskFactVisit", oldValue, _internal_TaskFactVisit));
        }
    }

    public function set TaskFactOutVisit(value:String) : void
    {
        var oldValue:String = _internal_TaskFactOutVisit;
        if (oldValue !== value)
        {
            _internal_TaskFactOutVisit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskFactOutVisit", oldValue, _internal_TaskFactOutVisit));
        }
    }

    public function set TaskNoticed(value:String) : void
    {
        var oldValue:String = _internal_TaskNoticed;
        if (oldValue !== value)
        {
            _internal_TaskNoticed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskNoticed", oldValue, _internal_TaskNoticed));
        }
    }

    public function set TaskWorkDescription(value:String) : void
    {
        var oldValue:String = _internal_TaskWorkDescription;
        if (oldValue !== value)
        {
            _internal_TaskWorkDescription = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskWorkDescription", oldValue, _internal_TaskWorkDescription));
        }
    }

    public function set TaskComment(value:String) : void
    {
        var oldValue:String = _internal_TaskComment;
        if (oldValue !== value)
        {
            _internal_TaskComment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskComment", oldValue, _internal_TaskComment));
        }
    }

    public function set PatientID(value:String) : void
    {
        var oldValue:String = _internal_PatientID;
        if (oldValue !== value)
        {
            _internal_PatientID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientID", oldValue, _internal_PatientID));
        }
    }

    public function set PatientLastname(value:String) : void
    {
        var oldValue:String = _internal_PatientLastname;
        if (oldValue !== value)
        {
            _internal_PatientLastname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientLastname", oldValue, _internal_PatientLastname));
        }
    }

    public function set PatientFirstname(value:String) : void
    {
        var oldValue:String = _internal_PatientFirstname;
        if (oldValue !== value)
        {
            _internal_PatientFirstname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientFirstname", oldValue, _internal_PatientFirstname));
        }
    }

    public function set PatientMiddlename(value:String) : void
    {
        var oldValue:String = _internal_PatientMiddlename;
        if (oldValue !== value)
        {
            _internal_PatientMiddlename = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientMiddlename", oldValue, _internal_PatientMiddlename));
        }
    }

    public function set PatientFullCardNumber(value:String) : void
    {
        var oldValue:String = _internal_PatientFullCardNumber;
        if (oldValue !== value)
        {
            _internal_PatientFullCardNumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientFullCardNumber", oldValue, _internal_PatientFullCardNumber));
        }
    }

    public function set PatientPrimaryFlag(value:String) : void
    {
        var oldValue:String = _internal_PatientPrimaryFlag;
        if (oldValue !== value)
        {
            _internal_PatientPrimaryFlag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientPrimaryFlag", oldValue, _internal_PatientPrimaryFlag));
        }
    }

    public function set PatientTelephone(value:String) : void
    {
        var oldValue:String = _internal_PatientTelephone;
        if (oldValue !== value)
        {
            _internal_PatientTelephone = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientTelephone", oldValue, _internal_PatientTelephone));
        }
    }

    public function set PatientMobile(value:String) : void
    {
        var oldValue:String = _internal_PatientMobile;
        if (oldValue !== value)
        {
            _internal_PatientMobile = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientMobile", oldValue, _internal_PatientMobile));
        }
    }

    public function set PatientBirthday(value:String) : void
    {
        var oldValue:String = _internal_PatientBirthday;
        if (oldValue !== value)
        {
            _internal_PatientBirthday = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientBirthday", oldValue, _internal_PatientBirthday));
        }
    }

    public function set PatientSex(value:String) : void
    {
        var oldValue:String = _internal_PatientSex;
        if (oldValue !== value)
        {
            _internal_PatientSex = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientSex", oldValue, _internal_PatientSex));
        }
    }

    public function set PatientTestFlag(value:String) : void
    {
        var oldValue:String = _internal_PatientTestFlag;
        if (oldValue !== value)
        {
            _internal_PatientTestFlag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientTestFlag", oldValue, _internal_PatientTestFlag));
        }
    }

    public function set InvoiceToSaveEnableFlag(value:int) : void
    {
        var oldValue:int = _internal_InvoiceToSaveEnableFlag;
        if (oldValue !== value)
        {
            _internal_InvoiceToSaveEnableFlag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "InvoiceToSaveEnableFlag", oldValue, _internal_InvoiceToSaveEnableFlag));
        }
    }

    public function set COUNTS_TOOTHEXAMS(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_TOOTHEXAMS;
        if (oldValue !== value)
        {
            _internal_COUNTS_TOOTHEXAMS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_TOOTHEXAMS", oldValue, _internal_COUNTS_TOOTHEXAMS));
        }
    }

    public function set COUNTS_PROCEDURES_CLOSED(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_PROCEDURES_CLOSED;
        if (oldValue !== value)
        {
            _internal_COUNTS_PROCEDURES_CLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_PROCEDURES_CLOSED", oldValue, _internal_COUNTS_PROCEDURES_CLOSED));
        }
    }

    public function set COUNTS_PROCEDURES_NOTCLOSED(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_PROCEDURES_NOTCLOSED;
        if (oldValue !== value)
        {
            _internal_COUNTS_PROCEDURES_NOTCLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_PROCEDURES_NOTCLOSED", oldValue, _internal_COUNTS_PROCEDURES_NOTCLOSED));
        }
    }

    public function set COUNTS_FILES(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_FILES;
        if (oldValue !== value)
        {
            _internal_COUNTS_FILES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_FILES", oldValue, _internal_COUNTS_FILES));
        }
    }

    public function set task_range(value:String) : void
    {
        var oldValue:String = _internal_task_range;
        if (oldValue !== value)
        {
            _internal_task_range = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_range", oldValue, _internal_task_range));
        }
    }

    public function set full_addres(value:String) : void
    {
        var oldValue:String = _internal_full_addres;
        if (oldValue !== value)
        {
            _internal_full_addres = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "full_addres", oldValue, _internal_full_addres));
        }
    }

    public function set full_fio(value:String) : void
    {
        var oldValue:String = _internal_full_fio;
        if (oldValue !== value)
        {
            _internal_full_fio = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "full_fio", oldValue, _internal_full_fio));
        }
    }

    public function set discount_amount(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount;
        if (isNaN(_internal_discount_amount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount", oldValue, _internal_discount_amount));
        }
    }

    public function set is_returned(value:int) : void
    {
        var oldValue:int = _internal_is_returned;
        if (oldValue !== value)
        {
            _internal_is_returned = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_returned", oldValue, _internal_is_returned));
        }
    }

    public function set patient_ages(value:int) : void
    {
        var oldValue:int = _internal_patient_ages;
        if (oldValue !== value)
        {
            _internal_patient_ages = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_ages", oldValue, _internal_patient_ages));
        }
    }

    public function set patient_notes(value:String) : void
    {
        var oldValue:String = _internal_patient_notes;
        if (oldValue !== value)
        {
            _internal_patient_notes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_notes", oldValue, _internal_patient_notes));
        }
    }

    public function set patient_leaddoctor(value:String) : void
    {
        var oldValue:String = _internal_patient_leaddoctor;
        if (oldValue !== value)
        {
            _internal_patient_leaddoctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_leaddoctor", oldValue, _internal_patient_leaddoctor));
        }
    }

    public function set mobiles(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_mobiles;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_mobiles = value;
            }
            else if (value is Array)
            {
                _internal_mobiles = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_mobiles = null;
            }
            else
            {
                throw new Error("value of mobiles must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "mobiles", oldValue, _internal_mobiles));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AWDDoctorModuleTaskPatientEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AWDDoctorModuleTaskPatientEntityMetadata) : void
    {
        var oldValue : _AWDDoctorModuleTaskPatientEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
