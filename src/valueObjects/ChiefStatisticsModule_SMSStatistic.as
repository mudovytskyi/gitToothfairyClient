/**
 * This is a generated sub-class of _ChiefStatisticsModule_SMSStatistic.as and is intended for behavior
 * customization.  This class is only generated when there is no file already present
 * at its target location.  Thus custom behavior that you add here will survive regeneration
 * of the super-class. 
 *
 * NOTE: Do not manually modify the RemoteClass mapping unless
 * your server representation of this class has changed and you've 
 * updated your ActionScriptGeneration,RemoteClass annotation on the
 * corresponding entity 
 **/ 
 
package valueObjects
{

import com.adobe.fiber.core.model_internal;

import components.date.ToolsForFirebird;
import components.date.ToothFairyDate;

import mx.formatters.DateFormatter;

public class ChiefStatisticsModule_SMSStatistic extends _Super_ChiefStatisticsModule_SMSStatistic
{
    /** 
     * DO NOT MODIFY THIS STATIC INITIALIZER - IT IS NECESSARY
     * FOR PROPERLY SETTING UP THE REMOTE CLASS ALIAS FOR THIS CLASS
     *
     **/
     
    /**
     * Calling this static function will initialize RemoteClass aliases
     * for this value object as well as all of the value objects corresponding
     * to entities associated to this value object's entity.  
     */     
    public static function _initRemoteClassAlias() : void
    {
        _Super_ChiefStatisticsModule_SMSStatistic.model_internal::initRemoteClassAliasSingle(valueObjects.ChiefStatisticsModule_SMSStatistic);
        _Super_ChiefStatisticsModule_SMSStatistic.model_internal::initRemoteClassAliasAllRelated();
    }
     
    model_internal static function initRemoteClassAliasSingleChild() : void
    {
        _Super_ChiefStatisticsModule_SMSStatistic.model_internal::initRemoteClassAliasSingle(valueObjects.ChiefStatisticsModule_SMSStatistic);
    }
    
    {
        _Super_ChiefStatisticsModule_SMSStatistic.model_internal::initRemoteClassAliasSingle(valueObjects.ChiefStatisticsModule_SMSStatistic);
    }
    /** 
     * END OF DO NOT MODIFY SECTION
     *
     **/  
	
	public function get itemDateRangeLabel():String
	{
		var dateFormatter:DateFormatter;
		switch(this.period_type)
		{
			case 'd1d':
			{
				return ToothFairyDate.stringFirebirdDateToToothFairyStringDate(this.period_startDate);
				break;
			}
			case 'd7d':
			{
				return ToothFairyDate.stringFirebirdDateToToothFairyStringDate(this.period_startDate)+' - '+
					ToothFairyDate.stringFirebirdDateToToothFairyStringDate(this.period_endDate);
				break;
			}
			case 'm1m':
			{
				//MM.YYYY TODO
				dateFormatter = new DateFormatter();
				dateFormatter.formatString = 'MM.YYYY';
				return dateFormatter.format(ToolsForFirebird.stringFirebirdDateToFlexDate(this.period_startDate));
				break;
			}
			case 'm12m':
			{
				//YYYY TODO
				dateFormatter = new DateFormatter();
				dateFormatter.formatString = 'YYYY';
				return dateFormatter.format(ToolsForFirebird.stringFirebirdDateToFlexDate(this.period_startDate));
				break;
			}
			default:
			{
				return ToothFairyDate.stringFirebirdDateToToothFairyStringDate(this.period_startDate)+' - '+
					ToothFairyDate.stringFirebirdDateToToothFairyStringDate(this.period_endDate);
				break;
			}
		}
	}
	
	
	
}

}