/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - MaterialsDictionaryModuleFarm.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.MaterialsDictionaryModuleFarm;
import valueObjects.MaterialsDictionaryModuleFarmSupplier;
import valueObjects.MaterialsDictionaryModuleUnitOfMeasure;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_MaterialsDictionaryModuleFarm extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("MaterialsDictionaryModuleFarm") == null)
            {
                flash.net.registerClassAlias("MaterialsDictionaryModuleFarm", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("MaterialsDictionaryModuleFarm", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.MaterialsDictionaryModuleUnitOfMeasure.initRemoteClassAliasSingleChild();
        valueObjects.MaterialsDictionaryModuleFarmSupplier.initRemoteClassAliasSingleChild();
        valueObjects.MaterialsDictionaryModuleFarm.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _MaterialsDictionaryModuleFarmEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_label : String;
    private var _internal_isOpen : Boolean;
    private var _internal_isBranch : Boolean;
    private var _internal_ID : String;
    private var _internal_PARENTID : String;
    private var _internal_NODETYPE : String;
    private var _internal_NAME : String;
    private var _internal_BASEUNITOFMEASUREID : String;
    private var _internal_BASEUNITOFMEASURENAME : String;
    private var _internal_NOMENCLATUREARTICLENUMBER : String;
    private var _internal_REMARK : String;
    private var _internal_FARMGROUPID : String;
    private var _internal_FARMGROUPCODE : String;
    private var _internal_FARMGROUPNAME : String;
    private var _internal_MINIMUMQUANTITY : String;
    private var _internal_OPTIMALQUANTITY : String;
    private var _internal_unitsOfMeasure : ArrayCollection;
    model_internal var _internal_unitsOfMeasure_leaf:valueObjects.MaterialsDictionaryModuleUnitOfMeasure;
    private var _internal_farmSuppliers : ArrayCollection;
    model_internal var _internal_farmSuppliers_leaf:valueObjects.MaterialsDictionaryModuleFarmSupplier;
    private var _internal_children : ArrayCollection;
    model_internal var _internal_children_leaf:valueObjects.MaterialsDictionaryModuleFarm;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_MaterialsDictionaryModuleFarm()
    {
        _model = new _MaterialsDictionaryModuleFarmEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get label() : String
    {
        return _internal_label;
    }

    [Bindable(event="propertyChange")]
    public function get isOpen() : Boolean
    {
        return _internal_isOpen;
    }

    [Bindable(event="propertyChange")]
    public function get isBranch() : Boolean
    {
        return _internal_isBranch;
    }

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PARENTID() : String
    {
        return _internal_PARENTID;
    }

    [Bindable(event="propertyChange")]
    public function get NODETYPE() : String
    {
        return _internal_NODETYPE;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get BASEUNITOFMEASUREID() : String
    {
        return _internal_BASEUNITOFMEASUREID;
    }

    [Bindable(event="propertyChange")]
    public function get BASEUNITOFMEASURENAME() : String
    {
        return _internal_BASEUNITOFMEASURENAME;
    }

    [Bindable(event="propertyChange")]
    public function get NOMENCLATUREARTICLENUMBER() : String
    {
        return _internal_NOMENCLATUREARTICLENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get REMARK() : String
    {
        return _internal_REMARK;
    }

    [Bindable(event="propertyChange")]
    public function get FARMGROUPID() : String
    {
        return _internal_FARMGROUPID;
    }

    [Bindable(event="propertyChange")]
    public function get FARMGROUPCODE() : String
    {
        return _internal_FARMGROUPCODE;
    }

    [Bindable(event="propertyChange")]
    public function get FARMGROUPNAME() : String
    {
        return _internal_FARMGROUPNAME;
    }

    [Bindable(event="propertyChange")]
    public function get MINIMUMQUANTITY() : String
    {
        return _internal_MINIMUMQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get OPTIMALQUANTITY() : String
    {
        return _internal_OPTIMALQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get unitsOfMeasure() : ArrayCollection
    {
        return _internal_unitsOfMeasure;
    }

    [Bindable(event="propertyChange")]
    public function get farmSuppliers() : ArrayCollection
    {
        return _internal_farmSuppliers;
    }

    [Bindable(event="propertyChange")]
    public function get children() : ArrayCollection
    {
        return _internal_children;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set label(value:String) : void
    {
        var oldValue:String = _internal_label;
        if (oldValue !== value)
        {
            _internal_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "label", oldValue, _internal_label));
        }
    }

    public function set isOpen(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOpen;
        if (oldValue !== value)
        {
            _internal_isOpen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOpen", oldValue, _internal_isOpen));
        }
    }

    public function set isBranch(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isBranch;
        if (oldValue !== value)
        {
            _internal_isBranch = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isBranch", oldValue, _internal_isBranch));
        }
    }

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set PARENTID(value:String) : void
    {
        var oldValue:String = _internal_PARENTID;
        if (oldValue !== value)
        {
            _internal_PARENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PARENTID", oldValue, _internal_PARENTID));
        }
    }

    public function set NODETYPE(value:String) : void
    {
        var oldValue:String = _internal_NODETYPE;
        if (oldValue !== value)
        {
            _internal_NODETYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NODETYPE", oldValue, _internal_NODETYPE));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set BASEUNITOFMEASUREID(value:String) : void
    {
        var oldValue:String = _internal_BASEUNITOFMEASUREID;
        if (oldValue !== value)
        {
            _internal_BASEUNITOFMEASUREID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BASEUNITOFMEASUREID", oldValue, _internal_BASEUNITOFMEASUREID));
        }
    }

    public function set BASEUNITOFMEASURENAME(value:String) : void
    {
        var oldValue:String = _internal_BASEUNITOFMEASURENAME;
        if (oldValue !== value)
        {
            _internal_BASEUNITOFMEASURENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BASEUNITOFMEASURENAME", oldValue, _internal_BASEUNITOFMEASURENAME));
        }
    }

    public function set NOMENCLATUREARTICLENUMBER(value:String) : void
    {
        var oldValue:String = _internal_NOMENCLATUREARTICLENUMBER;
        if (oldValue !== value)
        {
            _internal_NOMENCLATUREARTICLENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NOMENCLATUREARTICLENUMBER", oldValue, _internal_NOMENCLATUREARTICLENUMBER));
        }
    }

    public function set REMARK(value:String) : void
    {
        var oldValue:String = _internal_REMARK;
        if (oldValue !== value)
        {
            _internal_REMARK = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REMARK", oldValue, _internal_REMARK));
        }
    }

    public function set FARMGROUPID(value:String) : void
    {
        var oldValue:String = _internal_FARMGROUPID;
        if (oldValue !== value)
        {
            _internal_FARMGROUPID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMGROUPID", oldValue, _internal_FARMGROUPID));
        }
    }

    public function set FARMGROUPCODE(value:String) : void
    {
        var oldValue:String = _internal_FARMGROUPCODE;
        if (oldValue !== value)
        {
            _internal_FARMGROUPCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMGROUPCODE", oldValue, _internal_FARMGROUPCODE));
        }
    }

    public function set FARMGROUPNAME(value:String) : void
    {
        var oldValue:String = _internal_FARMGROUPNAME;
        if (oldValue !== value)
        {
            _internal_FARMGROUPNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMGROUPNAME", oldValue, _internal_FARMGROUPNAME));
        }
    }

    public function set MINIMUMQUANTITY(value:String) : void
    {
        var oldValue:String = _internal_MINIMUMQUANTITY;
        if (oldValue !== value)
        {
            _internal_MINIMUMQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MINIMUMQUANTITY", oldValue, _internal_MINIMUMQUANTITY));
        }
    }

    public function set OPTIMALQUANTITY(value:String) : void
    {
        var oldValue:String = _internal_OPTIMALQUANTITY;
        if (oldValue !== value)
        {
            _internal_OPTIMALQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OPTIMALQUANTITY", oldValue, _internal_OPTIMALQUANTITY));
        }
    }

    public function set unitsOfMeasure(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_unitsOfMeasure;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_unitsOfMeasure = value;
            }
            else if (value is Array)
            {
                _internal_unitsOfMeasure = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_unitsOfMeasure = null;
            }
            else
            {
                throw new Error("value of unitsOfMeasure must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "unitsOfMeasure", oldValue, _internal_unitsOfMeasure));
        }
    }

    public function set farmSuppliers(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_farmSuppliers;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_farmSuppliers = value;
            }
            else if (value is Array)
            {
                _internal_farmSuppliers = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_farmSuppliers = null;
            }
            else
            {
                throw new Error("value of farmSuppliers must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmSuppliers", oldValue, _internal_farmSuppliers));
        }
    }

    public function set children(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_children;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_children = value;
            }
            else if (value is Array)
            {
                _internal_children = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_children = null;
            }
            else
            {
                throw new Error("value of children must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "children", oldValue, _internal_children));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _MaterialsDictionaryModuleFarmEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _MaterialsDictionaryModuleFarmEntityMetadata) : void
    {
        var oldValue : _MaterialsDictionaryModuleFarmEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
