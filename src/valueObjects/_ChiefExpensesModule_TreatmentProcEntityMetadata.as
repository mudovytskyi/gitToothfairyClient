
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _ChiefExpensesModule_TreatmentProcEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("num", "type", "proc_id", "proc_name", "proc_shifr", "proc_closedate", "proc_expenses_plane", "proc_expenses_fact", "doc_id", "doc_lname", "doc_fname", "doc_sname", "pat_id", "pat_lname", "pat_fname", "pat_sname", "ass_id", "ass_lname", "ass_fname", "ass_sname", "accflow_id", "accflow_summ", "accflow_note", "accflow_is_cashpayment", "accflow_operationtimestamp", "accflow_createdate", "accflow_group_id", "accflow_group_name", "accflow_group_color", "accflow_group_gridsequence");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("num", "type", "proc_id", "proc_name", "proc_shifr", "proc_closedate", "proc_expenses_plane", "proc_expenses_fact", "doc_id", "doc_lname", "doc_fname", "doc_sname", "pat_id", "pat_lname", "pat_fname", "pat_sname", "ass_id", "ass_lname", "ass_fname", "ass_sname", "accflow_id", "accflow_summ", "accflow_note", "accflow_is_cashpayment", "accflow_operationtimestamp", "accflow_createdate", "accflow_group_id", "accflow_group_name", "accflow_group_color", "accflow_group_gridsequence");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("num", "type", "proc_id", "proc_name", "proc_shifr", "proc_closedate", "proc_expenses_plane", "proc_expenses_fact", "doc_id", "doc_lname", "doc_fname", "doc_sname", "pat_id", "pat_lname", "pat_fname", "pat_sname", "ass_id", "ass_lname", "ass_fname", "ass_sname", "accflow_id", "accflow_summ", "accflow_note", "accflow_is_cashpayment", "accflow_operationtimestamp", "accflow_createdate", "accflow_group_id", "accflow_group_name", "accflow_group_color", "accflow_group_gridsequence");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("num", "type", "proc_id", "proc_name", "proc_shifr", "proc_closedate", "proc_expenses_plane", "proc_expenses_fact", "doc_id", "doc_lname", "doc_fname", "doc_sname", "pat_id", "pat_lname", "pat_fname", "pat_sname", "ass_id", "ass_lname", "ass_fname", "ass_sname", "accflow_id", "accflow_summ", "accflow_note", "accflow_is_cashpayment", "accflow_operationtimestamp", "accflow_createdate", "accflow_group_id", "accflow_group_name", "accflow_group_color", "accflow_group_gridsequence");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "ChiefExpensesModule_TreatmentProc";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_ChiefExpensesModule_TreatmentProc;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _ChiefExpensesModule_TreatmentProcEntityMetadata(value : _Super_ChiefExpensesModule_TreatmentProc)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["num"] = new Array();
            model_internal::dependentsOnMap["type"] = new Array();
            model_internal::dependentsOnMap["proc_id"] = new Array();
            model_internal::dependentsOnMap["proc_name"] = new Array();
            model_internal::dependentsOnMap["proc_shifr"] = new Array();
            model_internal::dependentsOnMap["proc_closedate"] = new Array();
            model_internal::dependentsOnMap["proc_expenses_plane"] = new Array();
            model_internal::dependentsOnMap["proc_expenses_fact"] = new Array();
            model_internal::dependentsOnMap["doc_id"] = new Array();
            model_internal::dependentsOnMap["doc_lname"] = new Array();
            model_internal::dependentsOnMap["doc_fname"] = new Array();
            model_internal::dependentsOnMap["doc_sname"] = new Array();
            model_internal::dependentsOnMap["pat_id"] = new Array();
            model_internal::dependentsOnMap["pat_lname"] = new Array();
            model_internal::dependentsOnMap["pat_fname"] = new Array();
            model_internal::dependentsOnMap["pat_sname"] = new Array();
            model_internal::dependentsOnMap["ass_id"] = new Array();
            model_internal::dependentsOnMap["ass_lname"] = new Array();
            model_internal::dependentsOnMap["ass_fname"] = new Array();
            model_internal::dependentsOnMap["ass_sname"] = new Array();
            model_internal::dependentsOnMap["accflow_id"] = new Array();
            model_internal::dependentsOnMap["accflow_summ"] = new Array();
            model_internal::dependentsOnMap["accflow_note"] = new Array();
            model_internal::dependentsOnMap["accflow_is_cashpayment"] = new Array();
            model_internal::dependentsOnMap["accflow_operationtimestamp"] = new Array();
            model_internal::dependentsOnMap["accflow_createdate"] = new Array();
            model_internal::dependentsOnMap["accflow_group_id"] = new Array();
            model_internal::dependentsOnMap["accflow_group_name"] = new Array();
            model_internal::dependentsOnMap["accflow_group_color"] = new Array();
            model_internal::dependentsOnMap["accflow_group_gridsequence"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["num"] = "int";
        model_internal::propertyTypeMap["type"] = "String";
        model_internal::propertyTypeMap["proc_id"] = "String";
        model_internal::propertyTypeMap["proc_name"] = "String";
        model_internal::propertyTypeMap["proc_shifr"] = "String";
        model_internal::propertyTypeMap["proc_closedate"] = "String";
        model_internal::propertyTypeMap["proc_expenses_plane"] = "Number";
        model_internal::propertyTypeMap["proc_expenses_fact"] = "Number";
        model_internal::propertyTypeMap["doc_id"] = "String";
        model_internal::propertyTypeMap["doc_lname"] = "String";
        model_internal::propertyTypeMap["doc_fname"] = "String";
        model_internal::propertyTypeMap["doc_sname"] = "String";
        model_internal::propertyTypeMap["pat_id"] = "String";
        model_internal::propertyTypeMap["pat_lname"] = "String";
        model_internal::propertyTypeMap["pat_fname"] = "String";
        model_internal::propertyTypeMap["pat_sname"] = "String";
        model_internal::propertyTypeMap["ass_id"] = "String";
        model_internal::propertyTypeMap["ass_lname"] = "String";
        model_internal::propertyTypeMap["ass_fname"] = "String";
        model_internal::propertyTypeMap["ass_sname"] = "String";
        model_internal::propertyTypeMap["accflow_id"] = "String";
        model_internal::propertyTypeMap["accflow_summ"] = "Number";
        model_internal::propertyTypeMap["accflow_note"] = "String";
        model_internal::propertyTypeMap["accflow_is_cashpayment"] = "int";
        model_internal::propertyTypeMap["accflow_operationtimestamp"] = "String";
        model_internal::propertyTypeMap["accflow_createdate"] = "String";
        model_internal::propertyTypeMap["accflow_group_id"] = "String";
        model_internal::propertyTypeMap["accflow_group_name"] = "String";
        model_internal::propertyTypeMap["accflow_group_color"] = "int";
        model_internal::propertyTypeMap["accflow_group_gridsequence"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity ChiefExpensesModule_TreatmentProc");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity ChiefExpensesModule_TreatmentProc");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of ChiefExpensesModule_TreatmentProc");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ChiefExpensesModule_TreatmentProc");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity ChiefExpensesModule_TreatmentProc");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ChiefExpensesModule_TreatmentProc");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isNumAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTypeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProc_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProc_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProc_shifrAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProc_closedateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProc_expenses_planeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProc_expenses_factAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoc_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoc_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoc_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoc_snameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPat_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPat_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPat_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPat_snameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAss_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAss_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAss_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAss_snameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_summAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_noteAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_is_cashpaymentAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_operationtimestampAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_createdateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_group_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_group_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_group_colorAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccflow_group_gridsequenceAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get numStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get typeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get proc_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get proc_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get proc_shifrStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get proc_closedateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get proc_expenses_planeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get proc_expenses_factStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doc_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doc_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doc_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doc_snameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get pat_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get pat_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get pat_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get pat_snameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ass_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ass_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ass_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ass_snameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_summStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_noteStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_is_cashpaymentStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_operationtimestampStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_createdateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_group_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_group_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_group_colorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accflow_group_gridsequenceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
