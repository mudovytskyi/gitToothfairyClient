/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - OnlineRegistrationRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_OnlineRegistrationRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("OnlineRegistrationRecord") == null)
            {
                flash.net.registerClassAlias("OnlineRegistrationRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("OnlineRegistrationRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _OnlineRegistrationRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_SURNAME : String;
    private var _internal_NAME : String;
    private var _internal_MIDLENAME : String;
    private var _internal_FULLNAME : String;
    private var _internal_SHORTNAME : String;
    private var _internal_PHONE : String;
    private var _internal_EMAIL : String;
    private var _internal_TASKTASKDATE : String;
    private var _internal_TASKBEGINOFTHEINTERVAL : String;
    private var _internal_TASKENDOFTHEINTERVAL : String;
    private var _internal_ROOMID : int;
    private var _internal_ROOMNUMBER : int;
    private var _internal_ROOMNAME : String;
    private var _internal_DOCTORID : int;
    private var _internal_TASKWORKDESCRIPTION : String;
    private var _internal_SUBDIVISIONID : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_OnlineRegistrationRecord()
    {
        _model = new _OnlineRegistrationRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get SURNAME() : String
    {
        return _internal_SURNAME;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get MIDLENAME() : String
    {
        return _internal_MIDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get FULLNAME() : String
    {
        return _internal_FULLNAME;
    }

    [Bindable(event="propertyChange")]
    public function get SHORTNAME() : String
    {
        return _internal_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PHONE() : String
    {
        return _internal_PHONE;
    }

    [Bindable(event="propertyChange")]
    public function get EMAIL() : String
    {
        return _internal_EMAIL;
    }

    [Bindable(event="propertyChange")]
    public function get TASKTASKDATE() : String
    {
        return _internal_TASKTASKDATE;
    }

    [Bindable(event="propertyChange")]
    public function get TASKBEGINOFTHEINTERVAL() : String
    {
        return _internal_TASKBEGINOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get TASKENDOFTHEINTERVAL() : String
    {
        return _internal_TASKENDOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMID() : int
    {
        return _internal_ROOMID;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMNUMBER() : int
    {
        return _internal_ROOMNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMNAME() : String
    {
        return _internal_ROOMNAME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTORID() : int
    {
        return _internal_DOCTORID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKWORKDESCRIPTION() : String
    {
        return _internal_TASKWORKDESCRIPTION;
    }

    [Bindable(event="propertyChange")]
    public function get SUBDIVISIONID() : String
    {
        return _internal_SUBDIVISIONID;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set SURNAME(value:String) : void
    {
        var oldValue:String = _internal_SURNAME;
        if (oldValue !== value)
        {
            _internal_SURNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SURNAME", oldValue, _internal_SURNAME));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set MIDLENAME(value:String) : void
    {
        var oldValue:String = _internal_MIDLENAME;
        if (oldValue !== value)
        {
            _internal_MIDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MIDLENAME", oldValue, _internal_MIDLENAME));
        }
    }

    public function set FULLNAME(value:String) : void
    {
        var oldValue:String = _internal_FULLNAME;
        if (oldValue !== value)
        {
            _internal_FULLNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FULLNAME", oldValue, _internal_FULLNAME));
        }
    }

    public function set SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SHORTNAME", oldValue, _internal_SHORTNAME));
        }
    }

    public function set PHONE(value:String) : void
    {
        var oldValue:String = _internal_PHONE;
        if (oldValue !== value)
        {
            _internal_PHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PHONE", oldValue, _internal_PHONE));
        }
    }

    public function set EMAIL(value:String) : void
    {
        var oldValue:String = _internal_EMAIL;
        if (oldValue !== value)
        {
            _internal_EMAIL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMAIL", oldValue, _internal_EMAIL));
        }
    }

    public function set TASKTASKDATE(value:String) : void
    {
        var oldValue:String = _internal_TASKTASKDATE;
        if (oldValue !== value)
        {
            _internal_TASKTASKDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKTASKDATE", oldValue, _internal_TASKTASKDATE));
        }
    }

    public function set TASKBEGINOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TASKBEGINOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TASKBEGINOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKBEGINOFTHEINTERVAL", oldValue, _internal_TASKBEGINOFTHEINTERVAL));
        }
    }

    public function set TASKENDOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TASKENDOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TASKENDOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKENDOFTHEINTERVAL", oldValue, _internal_TASKENDOFTHEINTERVAL));
        }
    }

    public function set ROOMID(value:int) : void
    {
        var oldValue:int = _internal_ROOMID;
        if (oldValue !== value)
        {
            _internal_ROOMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMID", oldValue, _internal_ROOMID));
        }
    }

    public function set ROOMNUMBER(value:int) : void
    {
        var oldValue:int = _internal_ROOMNUMBER;
        if (oldValue !== value)
        {
            _internal_ROOMNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMNUMBER", oldValue, _internal_ROOMNUMBER));
        }
    }

    public function set ROOMNAME(value:String) : void
    {
        var oldValue:String = _internal_ROOMNAME;
        if (oldValue !== value)
        {
            _internal_ROOMNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMNAME", oldValue, _internal_ROOMNAME));
        }
    }

    public function set DOCTORID(value:int) : void
    {
        var oldValue:int = _internal_DOCTORID;
        if (oldValue !== value)
        {
            _internal_DOCTORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTORID", oldValue, _internal_DOCTORID));
        }
    }

    public function set TASKWORKDESCRIPTION(value:String) : void
    {
        var oldValue:String = _internal_TASKWORKDESCRIPTION;
        if (oldValue !== value)
        {
            _internal_TASKWORKDESCRIPTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKWORKDESCRIPTION", oldValue, _internal_TASKWORKDESCRIPTION));
        }
    }

    public function set SUBDIVISIONID(value:String) : void
    {
        var oldValue:String = _internal_SUBDIVISIONID;
        if (oldValue !== value)
        {
            _internal_SUBDIVISIONID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUBDIVISIONID", oldValue, _internal_SUBDIVISIONID));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _OnlineRegistrationRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _OnlineRegistrationRecordEntityMetadata) : void
    {
        var oldValue : _OnlineRegistrationRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
