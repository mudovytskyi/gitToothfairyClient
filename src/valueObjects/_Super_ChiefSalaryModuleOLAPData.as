/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefSalaryModuleOLAPData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefSalaryModuleOLAPData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefSalaryModuleOLAPData") == null)
            {
                flash.net.registerClassAlias("ChiefSalaryModuleOLAPData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefSalaryModuleOLAPData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefSalaryModuleOLAPDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_doctorId : int;
    private var _internal_doctorName : String;
    private var _internal_doctorSalaryPercent : Number;
    private var _internal_doctorSalaryStabil : Number;
    private var _internal_patientId : int;
    private var _internal_patientName : String;
    private var _internal_procedureId : int;
    private var _internal_procedureName : String;
    private var _internal_procedurDate : String;
    private var _internal_procedureSales : Number;
    private var _internal_procedureCount : Number;
    private var _internal_procedurExpenses : Number;
    private var _internal_procedurExpensesPlane : Number;
    private var _internal_procedurType : int;
    private var _internal_procedurSalary : Number;
    private var _internal_doctorRewardPlus : Number;
    private var _internal_doctorRewardMinus : Number;
    private var _internal_procedurMaterialsExpenses : Number;
    private var _internal_invoiceNumber : String;
    private var _internal_invoiceCreatedate : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefSalaryModuleOLAPData()
    {
        _model = new _ChiefSalaryModuleOLAPDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get doctorId() : int
    {
        return _internal_doctorId;
    }

    [Bindable(event="propertyChange")]
    public function get doctorName() : String
    {
        return _internal_doctorName;
    }

    [Bindable(event="propertyChange")]
    public function get doctorSalaryPercent() : Number
    {
        return _internal_doctorSalaryPercent;
    }

    [Bindable(event="propertyChange")]
    public function get doctorSalaryStabil() : Number
    {
        return _internal_doctorSalaryStabil;
    }

    [Bindable(event="propertyChange")]
    public function get patientId() : int
    {
        return _internal_patientId;
    }

    [Bindable(event="propertyChange")]
    public function get patientName() : String
    {
        return _internal_patientName;
    }

    [Bindable(event="propertyChange")]
    public function get procedureId() : int
    {
        return _internal_procedureId;
    }

    [Bindable(event="propertyChange")]
    public function get procedureName() : String
    {
        return _internal_procedureName;
    }

    [Bindable(event="propertyChange")]
    public function get procedurDate() : String
    {
        return _internal_procedurDate;
    }

    [Bindable(event="propertyChange")]
    public function get procedureSales() : Number
    {
        return _internal_procedureSales;
    }

    [Bindable(event="propertyChange")]
    public function get procedureCount() : Number
    {
        return _internal_procedureCount;
    }

    [Bindable(event="propertyChange")]
    public function get procedurExpenses() : Number
    {
        return _internal_procedurExpenses;
    }

    [Bindable(event="propertyChange")]
    public function get procedurExpensesPlane() : Number
    {
        return _internal_procedurExpensesPlane;
    }

    [Bindable(event="propertyChange")]
    public function get procedurType() : int
    {
        return _internal_procedurType;
    }

    [Bindable(event="propertyChange")]
    public function get procedurSalary() : Number
    {
        return _internal_procedurSalary;
    }

    [Bindable(event="propertyChange")]
    public function get doctorRewardPlus() : Number
    {
        return _internal_doctorRewardPlus;
    }

    [Bindable(event="propertyChange")]
    public function get doctorRewardMinus() : Number
    {
        return _internal_doctorRewardMinus;
    }

    [Bindable(event="propertyChange")]
    public function get procedurMaterialsExpenses() : Number
    {
        return _internal_procedurMaterialsExpenses;
    }

    [Bindable(event="propertyChange")]
    public function get invoiceNumber() : String
    {
        return _internal_invoiceNumber;
    }

    [Bindable(event="propertyChange")]
    public function get invoiceCreatedate() : String
    {
        return _internal_invoiceCreatedate;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set doctorId(value:int) : void
    {
        var oldValue:int = _internal_doctorId;
        if (oldValue !== value)
        {
            _internal_doctorId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorId", oldValue, _internal_doctorId));
        }
    }

    public function set doctorName(value:String) : void
    {
        var oldValue:String = _internal_doctorName;
        if (oldValue !== value)
        {
            _internal_doctorName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorName", oldValue, _internal_doctorName));
        }
    }

    public function set doctorSalaryPercent(value:Number) : void
    {
        var oldValue:Number = _internal_doctorSalaryPercent;
        if (isNaN(_internal_doctorSalaryPercent) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctorSalaryPercent = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorSalaryPercent", oldValue, _internal_doctorSalaryPercent));
        }
    }

    public function set doctorSalaryStabil(value:Number) : void
    {
        var oldValue:Number = _internal_doctorSalaryStabil;
        if (isNaN(_internal_doctorSalaryStabil) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctorSalaryStabil = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorSalaryStabil", oldValue, _internal_doctorSalaryStabil));
        }
    }

    public function set patientId(value:int) : void
    {
        var oldValue:int = _internal_patientId;
        if (oldValue !== value)
        {
            _internal_patientId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientId", oldValue, _internal_patientId));
        }
    }

    public function set patientName(value:String) : void
    {
        var oldValue:String = _internal_patientName;
        if (oldValue !== value)
        {
            _internal_patientName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientName", oldValue, _internal_patientName));
        }
    }

    public function set procedureId(value:int) : void
    {
        var oldValue:int = _internal_procedureId;
        if (oldValue !== value)
        {
            _internal_procedureId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedureId", oldValue, _internal_procedureId));
        }
    }

    public function set procedureName(value:String) : void
    {
        var oldValue:String = _internal_procedureName;
        if (oldValue !== value)
        {
            _internal_procedureName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedureName", oldValue, _internal_procedureName));
        }
    }

    public function set procedurDate(value:String) : void
    {
        var oldValue:String = _internal_procedurDate;
        if (oldValue !== value)
        {
            _internal_procedurDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurDate", oldValue, _internal_procedurDate));
        }
    }

    public function set procedureSales(value:Number) : void
    {
        var oldValue:Number = _internal_procedureSales;
        if (isNaN(_internal_procedureSales) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procedureSales = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedureSales", oldValue, _internal_procedureSales));
        }
    }

    public function set procedureCount(value:Number) : void
    {
        var oldValue:Number = _internal_procedureCount;
        if (isNaN(_internal_procedureCount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procedureCount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedureCount", oldValue, _internal_procedureCount));
        }
    }

    public function set procedurExpenses(value:Number) : void
    {
        var oldValue:Number = _internal_procedurExpenses;
        if (isNaN(_internal_procedurExpenses) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procedurExpenses = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurExpenses", oldValue, _internal_procedurExpenses));
        }
    }

    public function set procedurExpensesPlane(value:Number) : void
    {
        var oldValue:Number = _internal_procedurExpensesPlane;
        if (isNaN(_internal_procedurExpensesPlane) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procedurExpensesPlane = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurExpensesPlane", oldValue, _internal_procedurExpensesPlane));
        }
    }

    public function set procedurType(value:int) : void
    {
        var oldValue:int = _internal_procedurType;
        if (oldValue !== value)
        {
            _internal_procedurType = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurType", oldValue, _internal_procedurType));
        }
    }

    public function set procedurSalary(value:Number) : void
    {
        var oldValue:Number = _internal_procedurSalary;
        if (isNaN(_internal_procedurSalary) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procedurSalary = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurSalary", oldValue, _internal_procedurSalary));
        }
    }

    public function set doctorRewardPlus(value:Number) : void
    {
        var oldValue:Number = _internal_doctorRewardPlus;
        if (isNaN(_internal_doctorRewardPlus) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctorRewardPlus = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorRewardPlus", oldValue, _internal_doctorRewardPlus));
        }
    }

    public function set doctorRewardMinus(value:Number) : void
    {
        var oldValue:Number = _internal_doctorRewardMinus;
        if (isNaN(_internal_doctorRewardMinus) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctorRewardMinus = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorRewardMinus", oldValue, _internal_doctorRewardMinus));
        }
    }

    public function set procedurMaterialsExpenses(value:Number) : void
    {
        var oldValue:Number = _internal_procedurMaterialsExpenses;
        if (isNaN(_internal_procedurMaterialsExpenses) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procedurMaterialsExpenses = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurMaterialsExpenses", oldValue, _internal_procedurMaterialsExpenses));
        }
    }

    public function set invoiceNumber(value:String) : void
    {
        var oldValue:String = _internal_invoiceNumber;
        if (oldValue !== value)
        {
            _internal_invoiceNumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoiceNumber", oldValue, _internal_invoiceNumber));
        }
    }

    public function set invoiceCreatedate(value:String) : void
    {
        var oldValue:String = _internal_invoiceCreatedate;
        if (oldValue !== value)
        {
            _internal_invoiceCreatedate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoiceCreatedate", oldValue, _internal_invoiceCreatedate));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefSalaryModuleOLAPDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefSalaryModuleOLAPDataEntityMetadata) : void
    {
        var oldValue : _ChiefSalaryModuleOLAPDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
