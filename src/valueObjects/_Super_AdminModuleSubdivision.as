/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AdminModuleSubdivision.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AdminModuleSubdivision extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AdminModuleSubdivision") == null)
            {
                flash.net.registerClassAlias("AdminModuleSubdivision", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AdminModuleSubdivision", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AdminModuleSubdivisionEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_CASHNUMBER : int;
    private var _internal_NCASHNUMBER : int;
    private var _internal_ID : String;
    private var _internal_NAME : String;
    private var _internal_CITY : String;
    private var _internal_ADDRESS : String;
    private var _internal_ORDERNUMBER_CASH : int;
    private var _internal_ORDERNUMBER_CLEARING : int;
    private var _internal_INVOICENUMBER : int;
    private var _internal_namefull : String;
    private var _internal_email : String;
    private var _internal_eh_type : String;
    private var _internal_eh_mountain_group : String;
    private var _internal_eh_status : String;
    private var _internal_eh_id : String;
    private var _internal_LONGTITUDE : String;
    private var _internal_LATITUDE : String;
    private var _internal_is_main : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AdminModuleSubdivision()
    {
        _model = new _AdminModuleSubdivisionEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get CASHNUMBER() : int
    {
        return _internal_CASHNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get NCASHNUMBER() : int
    {
        return _internal_NCASHNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get CITY() : String
    {
        return _internal_CITY;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS() : String
    {
        return _internal_ADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get ORDERNUMBER_CASH() : int
    {
        return _internal_ORDERNUMBER_CASH;
    }

    [Bindable(event="propertyChange")]
    public function get ORDERNUMBER_CLEARING() : int
    {
        return _internal_ORDERNUMBER_CLEARING;
    }

    [Bindable(event="propertyChange")]
    public function get INVOICENUMBER() : int
    {
        return _internal_INVOICENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get namefull() : String
    {
        return _internal_namefull;
    }

    [Bindable(event="propertyChange")]
    public function get email() : String
    {
        return _internal_email;
    }

    [Bindable(event="propertyChange")]
    public function get eh_type() : String
    {
        return _internal_eh_type;
    }

    [Bindable(event="propertyChange")]
    public function get eh_mountain_group() : String
    {
        return _internal_eh_mountain_group;
    }

    [Bindable(event="propertyChange")]
    public function get eh_status() : String
    {
        return _internal_eh_status;
    }

    [Bindable(event="propertyChange")]
    public function get eh_id() : String
    {
        return _internal_eh_id;
    }

    [Bindable(event="propertyChange")]
    public function get LONGTITUDE() : String
    {
        return _internal_LONGTITUDE;
    }

    [Bindable(event="propertyChange")]
    public function get LATITUDE() : String
    {
        return _internal_LATITUDE;
    }

    [Bindable(event="propertyChange")]
    public function get is_main() : Boolean
    {
        return _internal_is_main;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set CASHNUMBER(value:int) : void
    {
        var oldValue:int = _internal_CASHNUMBER;
        if (oldValue !== value)
        {
            _internal_CASHNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CASHNUMBER", oldValue, _internal_CASHNUMBER));
        }
    }

    public function set NCASHNUMBER(value:int) : void
    {
        var oldValue:int = _internal_NCASHNUMBER;
        if (oldValue !== value)
        {
            _internal_NCASHNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NCASHNUMBER", oldValue, _internal_NCASHNUMBER));
        }
    }

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set CITY(value:String) : void
    {
        var oldValue:String = _internal_CITY;
        if (oldValue !== value)
        {
            _internal_CITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CITY", oldValue, _internal_CITY));
        }
    }

    public function set ADDRESS(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS;
        if (oldValue !== value)
        {
            _internal_ADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS", oldValue, _internal_ADDRESS));
        }
    }

    public function set ORDERNUMBER_CASH(value:int) : void
    {
        var oldValue:int = _internal_ORDERNUMBER_CASH;
        if (oldValue !== value)
        {
            _internal_ORDERNUMBER_CASH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ORDERNUMBER_CASH", oldValue, _internal_ORDERNUMBER_CASH));
        }
    }

    public function set ORDERNUMBER_CLEARING(value:int) : void
    {
        var oldValue:int = _internal_ORDERNUMBER_CLEARING;
        if (oldValue !== value)
        {
            _internal_ORDERNUMBER_CLEARING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ORDERNUMBER_CLEARING", oldValue, _internal_ORDERNUMBER_CLEARING));
        }
    }

    public function set INVOICENUMBER(value:int) : void
    {
        var oldValue:int = _internal_INVOICENUMBER;
        if (oldValue !== value)
        {
            _internal_INVOICENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INVOICENUMBER", oldValue, _internal_INVOICENUMBER));
        }
    }

    public function set namefull(value:String) : void
    {
        var oldValue:String = _internal_namefull;
        if (oldValue !== value)
        {
            _internal_namefull = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "namefull", oldValue, _internal_namefull));
        }
    }

    public function set email(value:String) : void
    {
        var oldValue:String = _internal_email;
        if (oldValue !== value)
        {
            _internal_email = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "email", oldValue, _internal_email));
        }
    }

    public function set eh_type(value:String) : void
    {
        var oldValue:String = _internal_eh_type;
        if (oldValue !== value)
        {
            _internal_eh_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "eh_type", oldValue, _internal_eh_type));
        }
    }

    public function set eh_mountain_group(value:String) : void
    {
        var oldValue:String = _internal_eh_mountain_group;
        if (oldValue !== value)
        {
            _internal_eh_mountain_group = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "eh_mountain_group", oldValue, _internal_eh_mountain_group));
        }
    }

    public function set eh_status(value:String) : void
    {
        var oldValue:String = _internal_eh_status;
        if (oldValue !== value)
        {
            _internal_eh_status = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "eh_status", oldValue, _internal_eh_status));
        }
    }

    public function set eh_id(value:String) : void
    {
        var oldValue:String = _internal_eh_id;
        if (oldValue !== value)
        {
            _internal_eh_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "eh_id", oldValue, _internal_eh_id));
        }
    }

    public function set LONGTITUDE(value:String) : void
    {
        var oldValue:String = _internal_LONGTITUDE;
        if (oldValue !== value)
        {
            _internal_LONGTITUDE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LONGTITUDE", oldValue, _internal_LONGTITUDE));
        }
    }

    public function set LATITUDE(value:String) : void
    {
        var oldValue:String = _internal_LATITUDE;
        if (oldValue !== value)
        {
            _internal_LATITUDE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LATITUDE", oldValue, _internal_LATITUDE));
        }
    }

    public function set is_main(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_is_main;
        if (oldValue !== value)
        {
            _internal_is_main = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_main", oldValue, _internal_is_main));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AdminModuleSubdivisionEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AdminModuleSubdivisionEntityMetadata) : void
    {
        var oldValue : _AdminModuleSubdivisionEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
