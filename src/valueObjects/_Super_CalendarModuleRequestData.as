/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CalendarModuleRequestData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CalendarModuleRequestData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CalendarModuleRequestData") == null)
            {
                flash.net.registerClassAlias("CalendarModuleRequestData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CalendarModuleRequestData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _CalendarModuleRequestDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_types : ArrayCollection;
    private var _internal_ids : ArrayCollection;
    private var _internal_taskdatestart : String;
    private var _internal_taskdateend : String;
    private var _internal_patientid : String;
    private var _internal_noticed : String;
    private var _internal_factofvisit : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CalendarModuleRequestData()
    {
        _model = new _CalendarModuleRequestDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get types() : ArrayCollection
    {
        return _internal_types;
    }

    [Bindable(event="propertyChange")]
    public function get ids() : ArrayCollection
    {
        return _internal_ids;
    }

    [Bindable(event="propertyChange")]
    public function get taskdatestart() : String
    {
        return _internal_taskdatestart;
    }

    [Bindable(event="propertyChange")]
    public function get taskdateend() : String
    {
        return _internal_taskdateend;
    }

    [Bindable(event="propertyChange")]
    public function get patientid() : String
    {
        return _internal_patientid;
    }

    [Bindable(event="propertyChange")]
    public function get noticed() : String
    {
        return _internal_noticed;
    }

    [Bindable(event="propertyChange")]
    public function get factofvisit() : String
    {
        return _internal_factofvisit;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set types(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_types;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_types = value;
            }
            else if (value is Array)
            {
                _internal_types = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_types = null;
            }
            else
            {
                throw new Error("value of types must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "types", oldValue, _internal_types));
        }
    }

    public function set ids(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_ids;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_ids = value;
            }
            else if (value is Array)
            {
                _internal_ids = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_ids = null;
            }
            else
            {
                throw new Error("value of ids must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ids", oldValue, _internal_ids));
        }
    }

    public function set taskdatestart(value:String) : void
    {
        var oldValue:String = _internal_taskdatestart;
        if (oldValue !== value)
        {
            _internal_taskdatestart = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "taskdatestart", oldValue, _internal_taskdatestart));
        }
    }

    public function set taskdateend(value:String) : void
    {
        var oldValue:String = _internal_taskdateend;
        if (oldValue !== value)
        {
            _internal_taskdateend = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "taskdateend", oldValue, _internal_taskdateend));
        }
    }

    public function set patientid(value:String) : void
    {
        var oldValue:String = _internal_patientid;
        if (oldValue !== value)
        {
            _internal_patientid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientid", oldValue, _internal_patientid));
        }
    }

    public function set noticed(value:String) : void
    {
        var oldValue:String = _internal_noticed;
        if (oldValue !== value)
        {
            _internal_noticed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "noticed", oldValue, _internal_noticed));
        }
    }

    public function set factofvisit(value:String) : void
    {
        var oldValue:String = _internal_factofvisit;
        if (oldValue !== value)
        {
            _internal_factofvisit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "factofvisit", oldValue, _internal_factofvisit));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CalendarModuleRequestDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CalendarModuleRequestDataEntityMetadata) : void
    {
        var oldValue : _CalendarModuleRequestDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
