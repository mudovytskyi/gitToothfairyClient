/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HealthPlanMKB10ModuleTreatmentProc.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.Form039Module_F39_Val;
import valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc;
import valueObjects.HealthPlanMKB10ModuleTreatmentProc;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HealthPlanMKB10ModuleTreatmentProc extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HealthPlanMKB10ModuleTreatmentProc") == null)
            {
                flash.net.registerClassAlias("HealthPlanMKB10ModuleTreatmentProc", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HealthPlanMKB10ModuleTreatmentProc", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc.initRemoteClassAliasSingleChild();
        valueObjects.Form039Module_F39_Val.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10ModuleTreatmentProc.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _HealthPlanMKB10ModuleTreatmentProcEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_procID : String;
    private var _internal_visit : int;
    private var _internal_procShifr : String;
    private var _internal_planName : String;
    private var _internal_label : String;
    private var _internal_courseName : String;
    private var _internal_procPrice : Number;
    private var _internal_procTime : int;
    private var _internal_procUOP : Number;
    private var _internal_farms : ArrayCollection;
    model_internal var _internal_farms_leaf:valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc;
    private var _internal_assistantID : String;
    private var _internal_assistantShortName : String;
    private var _internal_doctorID : String;
    private var _internal_doctorShortName : String;
    private var _internal_dateClose : String;
    private var _internal_toothUse : int;
    private var _internal_procHealthtype : int;
    private var _internal_procExpenses : Number;
    private var _internal_proc_count : Number;
    private var _internal_isSaved : Boolean;
    private var _internal_isComplete : Boolean;
    private var _internal_isChanged : Boolean;
    private var _internal_invoice_id : Number;
    private var _internal_invoice_number : String;
    private var _internal_invoice_createdate : String;
    private var _internal_discount_flag : int;
    private var _internal_salary_settings : int;
    private var _internal_healthproc_fk : String;
    private var _internal_order_fk : String;
    private var _internal_order_fixed : Boolean;
    private var _internal_order_fixed_date : String;
    private var _internal_complete_examdiag : String;
    private var _internal_procHealthtypes : String;
    private var _internal_NODETYPE : String;
    private var _internal_F39 : ArrayCollection;
    model_internal var _internal_F39_leaf:valueObjects.Form039Module_F39_Val;
    private var _internal_F39OUT : ArrayCollection;
    model_internal var _internal_F39OUT_leaf:valueObjects.Form039Module_F39_Val;
    private var _internal_isOpen : Boolean;
    private var _internal_children : ArrayCollection;
    model_internal var _internal_children_leaf:valueObjects.HealthPlanMKB10ModuleTreatmentProc;
    private var _internal_diagnos_proceduresCount : int;
    private var _internal_diagnos_factSumm : Number;
    private var _internal_diagnos_planSumm : Number;
    private var _internal_subscriptionCardID : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HealthPlanMKB10ModuleTreatmentProc()
    {
        _model = new _HealthPlanMKB10ModuleTreatmentProcEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get procID() : String
    {
        return _internal_procID;
    }

    [Bindable(event="propertyChange")]
    public function get visit() : int
    {
        return _internal_visit;
    }

    [Bindable(event="propertyChange")]
    public function get procShifr() : String
    {
        return _internal_procShifr;
    }

    [Bindable(event="propertyChange")]
    public function get planName() : String
    {
        return _internal_planName;
    }

    [Bindable(event="propertyChange")]
    public function get label() : String
    {
        return _internal_label;
    }

    [Bindable(event="propertyChange")]
    public function get courseName() : String
    {
        return _internal_courseName;
    }

    [Bindable(event="propertyChange")]
    public function get procPrice() : Number
    {
        return _internal_procPrice;
    }

    [Bindable(event="propertyChange")]
    public function get procTime() : int
    {
        return _internal_procTime;
    }

    [Bindable(event="propertyChange")]
    public function get procUOP() : Number
    {
        return _internal_procUOP;
    }

    [Bindable(event="propertyChange")]
    public function get farms() : ArrayCollection
    {
        return _internal_farms;
    }

    [Bindable(event="propertyChange")]
    public function get assistantID() : String
    {
        return _internal_assistantID;
    }

    [Bindable(event="propertyChange")]
    public function get assistantShortName() : String
    {
        return _internal_assistantShortName;
    }

    [Bindable(event="propertyChange")]
    public function get doctorID() : String
    {
        return _internal_doctorID;
    }

    [Bindable(event="propertyChange")]
    public function get doctorShortName() : String
    {
        return _internal_doctorShortName;
    }

    [Bindable(event="propertyChange")]
    public function get dateClose() : String
    {
        return _internal_dateClose;
    }

    [Bindable(event="propertyChange")]
    public function get toothUse() : int
    {
        return _internal_toothUse;
    }

    [Bindable(event="propertyChange")]
    public function get procHealthtype() : int
    {
        return _internal_procHealthtype;
    }

    [Bindable(event="propertyChange")]
    public function get procExpenses() : Number
    {
        return _internal_procExpenses;
    }

    [Bindable(event="propertyChange")]
    public function get proc_count() : Number
    {
        return _internal_proc_count;
    }

    [Bindable(event="propertyChange")]
    public function get isSaved() : Boolean
    {
        return _internal_isSaved;
    }

    [Bindable(event="propertyChange")]
    public function get isComplete() : Boolean
    {
        return _internal_isComplete;
    }

    [Bindable(event="propertyChange")]
    public function get isChanged() : Boolean
    {
        return _internal_isChanged;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_id() : Number
    {
        return _internal_invoice_id;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_number() : String
    {
        return _internal_invoice_number;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_createdate() : String
    {
        return _internal_invoice_createdate;
    }

    [Bindable(event="propertyChange")]
    public function get discount_flag() : int
    {
        return _internal_discount_flag;
    }

    [Bindable(event="propertyChange")]
    public function get salary_settings() : int
    {
        return _internal_salary_settings;
    }

    [Bindable(event="propertyChange")]
    public function get healthproc_fk() : String
    {
        return _internal_healthproc_fk;
    }

    [Bindable(event="propertyChange")]
    public function get order_fk() : String
    {
        return _internal_order_fk;
    }

    [Bindable(event="propertyChange")]
    public function get order_fixed() : Boolean
    {
        return _internal_order_fixed;
    }

    [Bindable(event="propertyChange")]
    public function get order_fixed_date() : String
    {
        return _internal_order_fixed_date;
    }

    [Bindable(event="propertyChange")]
    public function get complete_examdiag() : String
    {
        return _internal_complete_examdiag;
    }

    [Bindable(event="propertyChange")]
    public function get procHealthtypes() : String
    {
        return _internal_procHealthtypes;
    }

    [Bindable(event="propertyChange")]
    public function get NODETYPE() : String
    {
        return _internal_NODETYPE;
    }

    [Bindable(event="propertyChange")]
    public function get F39() : ArrayCollection
    {
        return _internal_F39;
    }

    [Bindable(event="propertyChange")]
    public function get F39OUT() : ArrayCollection
    {
        return _internal_F39OUT;
    }

    [Bindable(event="propertyChange")]
    public function get isOpen() : Boolean
    {
        return _internal_isOpen;
    }

    [Bindable(event="propertyChange")]
    public function get children() : ArrayCollection
    {
        return _internal_children;
    }

    [Bindable(event="propertyChange")]
    public function get diagnos_proceduresCount() : int
    {
        return _internal_diagnos_proceduresCount;
    }

    [Bindable(event="propertyChange")]
    public function get diagnos_factSumm() : Number
    {
        return _internal_diagnos_factSumm;
    }

    [Bindable(event="propertyChange")]
    public function get diagnos_planSumm() : Number
    {
        return _internal_diagnos_planSumm;
    }

    [Bindable(event="propertyChange")]
    public function get subscriptionCardID() : String
    {
        return _internal_subscriptionCardID;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set procID(value:String) : void
    {
        var oldValue:String = _internal_procID;
        if (oldValue !== value)
        {
            _internal_procID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procID", oldValue, _internal_procID));
        }
    }

    public function set visit(value:int) : void
    {
        var oldValue:int = _internal_visit;
        if (oldValue !== value)
        {
            _internal_visit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "visit", oldValue, _internal_visit));
        }
    }

    public function set procShifr(value:String) : void
    {
        var oldValue:String = _internal_procShifr;
        if (oldValue !== value)
        {
            _internal_procShifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procShifr", oldValue, _internal_procShifr));
        }
    }

    public function set planName(value:String) : void
    {
        var oldValue:String = _internal_planName;
        if (oldValue !== value)
        {
            _internal_planName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "planName", oldValue, _internal_planName));
        }
    }

    public function set label(value:String) : void
    {
        var oldValue:String = _internal_label;
        if (oldValue !== value)
        {
            _internal_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "label", oldValue, _internal_label));
        }
    }

    public function set courseName(value:String) : void
    {
        var oldValue:String = _internal_courseName;
        if (oldValue !== value)
        {
            _internal_courseName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "courseName", oldValue, _internal_courseName));
        }
    }

    public function set procPrice(value:Number) : void
    {
        var oldValue:Number = _internal_procPrice;
        if (isNaN(_internal_procPrice) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procPrice = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procPrice", oldValue, _internal_procPrice));
        }
    }

    public function set procTime(value:int) : void
    {
        var oldValue:int = _internal_procTime;
        if (oldValue !== value)
        {
            _internal_procTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procTime", oldValue, _internal_procTime));
        }
    }

    public function set procUOP(value:Number) : void
    {
        var oldValue:Number = _internal_procUOP;
        if (isNaN(_internal_procUOP) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procUOP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procUOP", oldValue, _internal_procUOP));
        }
    }

    public function set farms(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_farms;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_farms = value;
            }
            else if (value is Array)
            {
                _internal_farms = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_farms = null;
            }
            else
            {
                throw new Error("value of farms must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farms", oldValue, _internal_farms));
        }
    }

    public function set assistantID(value:String) : void
    {
        var oldValue:String = _internal_assistantID;
        if (oldValue !== value)
        {
            _internal_assistantID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistantID", oldValue, _internal_assistantID));
        }
    }

    public function set assistantShortName(value:String) : void
    {
        var oldValue:String = _internal_assistantShortName;
        if (oldValue !== value)
        {
            _internal_assistantShortName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistantShortName", oldValue, _internal_assistantShortName));
        }
    }

    public function set doctorID(value:String) : void
    {
        var oldValue:String = _internal_doctorID;
        if (oldValue !== value)
        {
            _internal_doctorID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorID", oldValue, _internal_doctorID));
        }
    }

    public function set doctorShortName(value:String) : void
    {
        var oldValue:String = _internal_doctorShortName;
        if (oldValue !== value)
        {
            _internal_doctorShortName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorShortName", oldValue, _internal_doctorShortName));
        }
    }

    public function set dateClose(value:String) : void
    {
        var oldValue:String = _internal_dateClose;
        if (oldValue !== value)
        {
            _internal_dateClose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dateClose", oldValue, _internal_dateClose));
        }
    }

    public function set toothUse(value:int) : void
    {
        var oldValue:int = _internal_toothUse;
        if (oldValue !== value)
        {
            _internal_toothUse = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "toothUse", oldValue, _internal_toothUse));
        }
    }

    public function set procHealthtype(value:int) : void
    {
        var oldValue:int = _internal_procHealthtype;
        if (oldValue !== value)
        {
            _internal_procHealthtype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procHealthtype", oldValue, _internal_procHealthtype));
        }
    }

    public function set procExpenses(value:Number) : void
    {
        var oldValue:Number = _internal_procExpenses;
        if (isNaN(_internal_procExpenses) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procExpenses = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procExpenses", oldValue, _internal_procExpenses));
        }
    }

    public function set proc_count(value:Number) : void
    {
        var oldValue:Number = _internal_proc_count;
        if (isNaN(_internal_proc_count) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_proc_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_count", oldValue, _internal_proc_count));
        }
    }

    public function set isSaved(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isSaved;
        if (oldValue !== value)
        {
            _internal_isSaved = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isSaved", oldValue, _internal_isSaved));
        }
    }

    public function set isComplete(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isComplete;
        if (oldValue !== value)
        {
            _internal_isComplete = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isComplete", oldValue, _internal_isComplete));
        }
    }

    public function set isChanged(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isChanged;
        if (oldValue !== value)
        {
            _internal_isChanged = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isChanged", oldValue, _internal_isChanged));
        }
    }

    public function set invoice_id(value:Number) : void
    {
        var oldValue:Number = _internal_invoice_id;
        if (isNaN(_internal_invoice_id) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_invoice_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_id", oldValue, _internal_invoice_id));
        }
    }

    public function set invoice_number(value:String) : void
    {
        var oldValue:String = _internal_invoice_number;
        if (oldValue !== value)
        {
            _internal_invoice_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_number", oldValue, _internal_invoice_number));
        }
    }

    public function set invoice_createdate(value:String) : void
    {
        var oldValue:String = _internal_invoice_createdate;
        if (oldValue !== value)
        {
            _internal_invoice_createdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_createdate", oldValue, _internal_invoice_createdate));
        }
    }

    public function set discount_flag(value:int) : void
    {
        var oldValue:int = _internal_discount_flag;
        if (oldValue !== value)
        {
            _internal_discount_flag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_flag", oldValue, _internal_discount_flag));
        }
    }

    public function set salary_settings(value:int) : void
    {
        var oldValue:int = _internal_salary_settings;
        if (oldValue !== value)
        {
            _internal_salary_settings = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "salary_settings", oldValue, _internal_salary_settings));
        }
    }

    public function set healthproc_fk(value:String) : void
    {
        var oldValue:String = _internal_healthproc_fk;
        if (oldValue !== value)
        {
            _internal_healthproc_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "healthproc_fk", oldValue, _internal_healthproc_fk));
        }
    }

    public function set order_fk(value:String) : void
    {
        var oldValue:String = _internal_order_fk;
        if (oldValue !== value)
        {
            _internal_order_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "order_fk", oldValue, _internal_order_fk));
        }
    }

    public function set order_fixed(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_order_fixed;
        if (oldValue !== value)
        {
            _internal_order_fixed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "order_fixed", oldValue, _internal_order_fixed));
        }
    }

    public function set order_fixed_date(value:String) : void
    {
        var oldValue:String = _internal_order_fixed_date;
        if (oldValue !== value)
        {
            _internal_order_fixed_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "order_fixed_date", oldValue, _internal_order_fixed_date));
        }
    }

    public function set complete_examdiag(value:String) : void
    {
        var oldValue:String = _internal_complete_examdiag;
        if (oldValue !== value)
        {
            _internal_complete_examdiag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "complete_examdiag", oldValue, _internal_complete_examdiag));
        }
    }

    public function set procHealthtypes(value:String) : void
    {
        var oldValue:String = _internal_procHealthtypes;
        if (oldValue !== value)
        {
            _internal_procHealthtypes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procHealthtypes", oldValue, _internal_procHealthtypes));
        }
    }

    public function set NODETYPE(value:String) : void
    {
        var oldValue:String = _internal_NODETYPE;
        if (oldValue !== value)
        {
            _internal_NODETYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NODETYPE", oldValue, _internal_NODETYPE));
        }
    }

    public function set F39(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_F39;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_F39 = value;
            }
            else if (value is Array)
            {
                _internal_F39 = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_F39 = null;
            }
            else
            {
                throw new Error("value of F39 must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "F39", oldValue, _internal_F39));
        }
    }

    public function set F39OUT(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_F39OUT;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_F39OUT = value;
            }
            else if (value is Array)
            {
                _internal_F39OUT = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_F39OUT = null;
            }
            else
            {
                throw new Error("value of F39OUT must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "F39OUT", oldValue, _internal_F39OUT));
        }
    }

    public function set isOpen(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOpen;
        if (oldValue !== value)
        {
            _internal_isOpen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOpen", oldValue, _internal_isOpen));
        }
    }

    public function set children(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_children;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_children = value;
            }
            else if (value is Array)
            {
                _internal_children = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_children = null;
            }
            else
            {
                throw new Error("value of children must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "children", oldValue, _internal_children));
        }
    }

    public function set diagnos_proceduresCount(value:int) : void
    {
        var oldValue:int = _internal_diagnos_proceduresCount;
        if (oldValue !== value)
        {
            _internal_diagnos_proceduresCount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diagnos_proceduresCount", oldValue, _internal_diagnos_proceduresCount));
        }
    }

    public function set diagnos_factSumm(value:Number) : void
    {
        var oldValue:Number = _internal_diagnos_factSumm;
        if (isNaN(_internal_diagnos_factSumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_diagnos_factSumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diagnos_factSumm", oldValue, _internal_diagnos_factSumm));
        }
    }

    public function set diagnos_planSumm(value:Number) : void
    {
        var oldValue:Number = _internal_diagnos_planSumm;
        if (isNaN(_internal_diagnos_planSumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_diagnos_planSumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diagnos_planSumm", oldValue, _internal_diagnos_planSumm));
        }
    }

    public function set subscriptionCardID(value:String) : void
    {
        var oldValue:String = _internal_subscriptionCardID;
        if (oldValue !== value)
        {
            _internal_subscriptionCardID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subscriptionCardID", oldValue, _internal_subscriptionCardID));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HealthPlanMKB10ModuleTreatmentProcEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HealthPlanMKB10ModuleTreatmentProcEntityMetadata) : void
    {
        var oldValue : _HealthPlanMKB10ModuleTreatmentProcEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
