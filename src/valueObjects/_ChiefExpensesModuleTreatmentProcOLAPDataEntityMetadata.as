
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _ChiefExpensesModuleTreatmentProcOLAPDataEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("treatmentproc_id", "treatmentproc_shifr", "treatmentproc_name", "treatmentproc_nameforplan", "treatmentproc_price", "doctor_id", "doctor_lastname", "doctor_firstname", "doctor_middlename", "doctor_shortname", "treatmentproc_dateclose", "treatmentproc_invoiceid", "treatmentproc_expenses", "assistant_id", "assistant_lastname", "assistant_firstname", "assistant_middlename", "assistant_shortname", "treatmentproc_proc_count", "treatmentproc_healthproc_fk", "treatmentproc_expenses_fact", "accountflow_id", "patient_id", "patient_lastname", "patient_firstname", "patient_middlename", "patient_shortname", "accountflow_summ", "accountflow_operationnote", "accountflow_createdate", "accountflow_operationtimestamp", "accountflow_is_cashpayment", "texp_group_id", "texp_group_name", "texp_group_color", "texp_group_gridsequence");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("treatmentproc_id", "treatmentproc_shifr", "treatmentproc_name", "treatmentproc_nameforplan", "treatmentproc_price", "doctor_id", "doctor_lastname", "doctor_firstname", "doctor_middlename", "doctor_shortname", "treatmentproc_dateclose", "treatmentproc_invoiceid", "treatmentproc_expenses", "assistant_id", "assistant_lastname", "assistant_firstname", "assistant_middlename", "assistant_shortname", "treatmentproc_proc_count", "treatmentproc_healthproc_fk", "treatmentproc_expenses_fact", "accountflow_id", "patient_id", "patient_lastname", "patient_firstname", "patient_middlename", "patient_shortname", "accountflow_summ", "accountflow_operationnote", "accountflow_createdate", "accountflow_operationtimestamp", "accountflow_is_cashpayment", "texp_group_id", "texp_group_name", "texp_group_color", "texp_group_gridsequence");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("treatmentproc_id", "treatmentproc_shifr", "treatmentproc_name", "treatmentproc_nameforplan", "treatmentproc_price", "doctor_id", "doctor_lastname", "doctor_firstname", "doctor_middlename", "doctor_shortname", "treatmentproc_dateclose", "treatmentproc_invoiceid", "treatmentproc_expenses", "assistant_id", "assistant_lastname", "assistant_firstname", "assistant_middlename", "assistant_shortname", "treatmentproc_proc_count", "treatmentproc_healthproc_fk", "treatmentproc_expenses_fact", "accountflow_id", "patient_id", "patient_lastname", "patient_firstname", "patient_middlename", "patient_shortname", "accountflow_summ", "accountflow_operationnote", "accountflow_createdate", "accountflow_operationtimestamp", "accountflow_is_cashpayment", "texp_group_id", "texp_group_name", "texp_group_color", "texp_group_gridsequence");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("treatmentproc_id", "treatmentproc_shifr", "treatmentproc_name", "treatmentproc_nameforplan", "treatmentproc_price", "doctor_id", "doctor_lastname", "doctor_firstname", "doctor_middlename", "doctor_shortname", "treatmentproc_dateclose", "treatmentproc_invoiceid", "treatmentproc_expenses", "assistant_id", "assistant_lastname", "assistant_firstname", "assistant_middlename", "assistant_shortname", "treatmentproc_proc_count", "treatmentproc_healthproc_fk", "treatmentproc_expenses_fact", "accountflow_id", "patient_id", "patient_lastname", "patient_firstname", "patient_middlename", "patient_shortname", "accountflow_summ", "accountflow_operationnote", "accountflow_createdate", "accountflow_operationtimestamp", "accountflow_is_cashpayment", "texp_group_id", "texp_group_name", "texp_group_color", "texp_group_gridsequence");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "ChiefExpensesModuleTreatmentProcOLAPData";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_ChiefExpensesModuleTreatmentProcOLAPData;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _ChiefExpensesModuleTreatmentProcOLAPDataEntityMetadata(value : _Super_ChiefExpensesModuleTreatmentProcOLAPData)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["treatmentproc_id"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_shifr"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_name"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_nameforplan"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_price"] = new Array();
            model_internal::dependentsOnMap["doctor_id"] = new Array();
            model_internal::dependentsOnMap["doctor_lastname"] = new Array();
            model_internal::dependentsOnMap["doctor_firstname"] = new Array();
            model_internal::dependentsOnMap["doctor_middlename"] = new Array();
            model_internal::dependentsOnMap["doctor_shortname"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_dateclose"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_invoiceid"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_expenses"] = new Array();
            model_internal::dependentsOnMap["assistant_id"] = new Array();
            model_internal::dependentsOnMap["assistant_lastname"] = new Array();
            model_internal::dependentsOnMap["assistant_firstname"] = new Array();
            model_internal::dependentsOnMap["assistant_middlename"] = new Array();
            model_internal::dependentsOnMap["assistant_shortname"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_proc_count"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_healthproc_fk"] = new Array();
            model_internal::dependentsOnMap["treatmentproc_expenses_fact"] = new Array();
            model_internal::dependentsOnMap["accountflow_id"] = new Array();
            model_internal::dependentsOnMap["patient_id"] = new Array();
            model_internal::dependentsOnMap["patient_lastname"] = new Array();
            model_internal::dependentsOnMap["patient_firstname"] = new Array();
            model_internal::dependentsOnMap["patient_middlename"] = new Array();
            model_internal::dependentsOnMap["patient_shortname"] = new Array();
            model_internal::dependentsOnMap["accountflow_summ"] = new Array();
            model_internal::dependentsOnMap["accountflow_operationnote"] = new Array();
            model_internal::dependentsOnMap["accountflow_createdate"] = new Array();
            model_internal::dependentsOnMap["accountflow_operationtimestamp"] = new Array();
            model_internal::dependentsOnMap["accountflow_is_cashpayment"] = new Array();
            model_internal::dependentsOnMap["texp_group_id"] = new Array();
            model_internal::dependentsOnMap["texp_group_name"] = new Array();
            model_internal::dependentsOnMap["texp_group_color"] = new Array();
            model_internal::dependentsOnMap["texp_group_gridsequence"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["treatmentproc_id"] = "String";
        model_internal::propertyTypeMap["treatmentproc_shifr"] = "String";
        model_internal::propertyTypeMap["treatmentproc_name"] = "String";
        model_internal::propertyTypeMap["treatmentproc_nameforplan"] = "String";
        model_internal::propertyTypeMap["treatmentproc_price"] = "Number";
        model_internal::propertyTypeMap["doctor_id"] = "String";
        model_internal::propertyTypeMap["doctor_lastname"] = "String";
        model_internal::propertyTypeMap["doctor_firstname"] = "String";
        model_internal::propertyTypeMap["doctor_middlename"] = "String";
        model_internal::propertyTypeMap["doctor_shortname"] = "String";
        model_internal::propertyTypeMap["treatmentproc_dateclose"] = "String";
        model_internal::propertyTypeMap["treatmentproc_invoiceid"] = "String";
        model_internal::propertyTypeMap["treatmentproc_expenses"] = "Number";
        model_internal::propertyTypeMap["assistant_id"] = "String";
        model_internal::propertyTypeMap["assistant_lastname"] = "String";
        model_internal::propertyTypeMap["assistant_firstname"] = "String";
        model_internal::propertyTypeMap["assistant_middlename"] = "String";
        model_internal::propertyTypeMap["assistant_shortname"] = "String";
        model_internal::propertyTypeMap["treatmentproc_proc_count"] = "int";
        model_internal::propertyTypeMap["treatmentproc_healthproc_fk"] = "String";
        model_internal::propertyTypeMap["treatmentproc_expenses_fact"] = "Number";
        model_internal::propertyTypeMap["accountflow_id"] = "String";
        model_internal::propertyTypeMap["patient_id"] = "String";
        model_internal::propertyTypeMap["patient_lastname"] = "String";
        model_internal::propertyTypeMap["patient_firstname"] = "String";
        model_internal::propertyTypeMap["patient_middlename"] = "String";
        model_internal::propertyTypeMap["patient_shortname"] = "String";
        model_internal::propertyTypeMap["accountflow_summ"] = "Number";
        model_internal::propertyTypeMap["accountflow_operationnote"] = "String";
        model_internal::propertyTypeMap["accountflow_createdate"] = "String";
        model_internal::propertyTypeMap["accountflow_operationtimestamp"] = "String";
        model_internal::propertyTypeMap["accountflow_is_cashpayment"] = "String";
        model_internal::propertyTypeMap["texp_group_id"] = "String";
        model_internal::propertyTypeMap["texp_group_name"] = "String";
        model_internal::propertyTypeMap["texp_group_color"] = "int";
        model_internal::propertyTypeMap["texp_group_gridsequence"] = "int";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity ChiefExpensesModuleTreatmentProcOLAPData");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity ChiefExpensesModuleTreatmentProcOLAPData");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of ChiefExpensesModuleTreatmentProcOLAPData");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ChiefExpensesModuleTreatmentProcOLAPData");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity ChiefExpensesModuleTreatmentProcOLAPData");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ChiefExpensesModuleTreatmentProcOLAPData");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_shifrAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_nameforplanAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_priceAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctor_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctor_lastnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctor_firstnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctor_middlenameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctor_shortnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_datecloseAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_invoiceidAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_expensesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAssistant_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAssistant_lastnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAssistant_firstnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAssistant_middlenameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAssistant_shortnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_proc_countAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_healthproc_fkAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreatmentproc_expenses_factAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccountflow_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_lastnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_firstnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_middlenameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_shortnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccountflow_summAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccountflow_operationnoteAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccountflow_createdateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccountflow_operationtimestampAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccountflow_is_cashpaymentAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTexp_group_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTexp_group_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTexp_group_colorAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTexp_group_gridsequenceAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_shifrStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_nameforplanStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_priceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctor_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctor_lastnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctor_firstnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctor_middlenameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctor_shortnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_datecloseStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_invoiceidStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_expensesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get assistant_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get assistant_lastnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get assistant_firstnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get assistant_middlenameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get assistant_shortnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_proc_countStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_healthproc_fkStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get treatmentproc_expenses_factStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accountflow_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_lastnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_firstnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_middlenameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_shortnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accountflow_summStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accountflow_operationnoteStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accountflow_createdateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accountflow_operationtimestampStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get accountflow_is_cashpaymentStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get texp_group_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get texp_group_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get texp_group_colorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get texp_group_gridsequenceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
