/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefSalaryModuleDoctorSalarySummaryObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefSalaryModuleDoctorSalarySummaryObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefSalaryModuleDoctorSalarySummaryObject") == null)
            {
                flash.net.registerClassAlias("ChiefSalaryModuleDoctorSalarySummaryObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefSalaryModuleDoctorSalarySummaryObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefSalaryModuleDoctorSalarySummaryObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_doctor_id : String;
    private var _internal_doctor_work_sum : Number;
    private var _internal_doctor_precent_work_sum : Number;
    private var _internal_doctor_salary : Number;
    private var _internal_doctor_in_salary_sum : Number;
    private var _internal_doctor_out_salary_sum : Number;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefSalaryModuleDoctorSalarySummaryObject()
    {
        _model = new _ChiefSalaryModuleDoctorSalarySummaryObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_work_sum() : Number
    {
        return _internal_doctor_work_sum;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_precent_work_sum() : Number
    {
        return _internal_doctor_precent_work_sum;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_salary() : Number
    {
        return _internal_doctor_salary;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_in_salary_sum() : Number
    {
        return _internal_doctor_in_salary_sum;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_out_salary_sum() : Number
    {
        return _internal_doctor_out_salary_sum;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_work_sum(value:Number) : void
    {
        var oldValue:Number = _internal_doctor_work_sum;
        if (isNaN(_internal_doctor_work_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctor_work_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_work_sum", oldValue, _internal_doctor_work_sum));
        }
    }

    public function set doctor_precent_work_sum(value:Number) : void
    {
        var oldValue:Number = _internal_doctor_precent_work_sum;
        if (isNaN(_internal_doctor_precent_work_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctor_precent_work_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_precent_work_sum", oldValue, _internal_doctor_precent_work_sum));
        }
    }

    public function set doctor_salary(value:Number) : void
    {
        var oldValue:Number = _internal_doctor_salary;
        if (isNaN(_internal_doctor_salary) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctor_salary = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_salary", oldValue, _internal_doctor_salary));
        }
    }

    public function set doctor_in_salary_sum(value:Number) : void
    {
        var oldValue:Number = _internal_doctor_in_salary_sum;
        if (isNaN(_internal_doctor_in_salary_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctor_in_salary_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_in_salary_sum", oldValue, _internal_doctor_in_salary_sum));
        }
    }

    public function set doctor_out_salary_sum(value:Number) : void
    {
        var oldValue:Number = _internal_doctor_out_salary_sum;
        if (isNaN(_internal_doctor_out_salary_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_doctor_out_salary_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_out_salary_sum", oldValue, _internal_doctor_out_salary_sum));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefSalaryModuleDoctorSalarySummaryObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefSalaryModuleDoctorSalarySummaryObjectEntityMetadata) : void
    {
        var oldValue : _ChiefSalaryModuleDoctorSalarySummaryObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
