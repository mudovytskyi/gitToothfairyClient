/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HealthPlanPrintModule_Procedure.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HealthPlanPrintModule_Procedure extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HealthPlanPrintModule_Procedure") == null)
            {
                flash.net.registerClassAlias("HealthPlanPrintModule_Procedure", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HealthPlanPrintModule_Procedure", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _HealthPlanPrintModule_ProcedureEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_name : String;
    private var _internal_name_plan : String;
    private var _internal_shifr : String;
    private var _internal_visit : int;
    private var _internal_price : Number;
    private var _internal_proc_count : int;
    private var _internal_dateclose : String;
    private var _internal_isClose : Boolean;
    private var _internal_price_full : Number;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HealthPlanPrintModule_Procedure()
    {
        _model = new _HealthPlanPrintModule_ProcedureEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get name() : String
    {
        return _internal_name;
    }

    [Bindable(event="propertyChange")]
    public function get name_plan() : String
    {
        return _internal_name_plan;
    }

    [Bindable(event="propertyChange")]
    public function get shifr() : String
    {
        return _internal_shifr;
    }

    [Bindable(event="propertyChange")]
    public function get visit() : int
    {
        return _internal_visit;
    }

    [Bindable(event="propertyChange")]
    public function get price() : Number
    {
        return _internal_price;
    }

    [Bindable(event="propertyChange")]
    public function get proc_count() : int
    {
        return _internal_proc_count;
    }

    [Bindable(event="propertyChange")]
    public function get dateclose() : String
    {
        return _internal_dateclose;
    }

    [Bindable(event="propertyChange")]
    public function get isClose() : Boolean
    {
        return _internal_isClose;
    }

    [Bindable(event="propertyChange")]
    public function get price_full() : Number
    {
        return _internal_price_full;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set name(value:String) : void
    {
        var oldValue:String = _internal_name;
        if (oldValue !== value)
        {
            _internal_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name", oldValue, _internal_name));
        }
    }

    public function set name_plan(value:String) : void
    {
        var oldValue:String = _internal_name_plan;
        if (oldValue !== value)
        {
            _internal_name_plan = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name_plan", oldValue, _internal_name_plan));
        }
    }

    public function set shifr(value:String) : void
    {
        var oldValue:String = _internal_shifr;
        if (oldValue !== value)
        {
            _internal_shifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "shifr", oldValue, _internal_shifr));
        }
    }

    public function set visit(value:int) : void
    {
        var oldValue:int = _internal_visit;
        if (oldValue !== value)
        {
            _internal_visit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "visit", oldValue, _internal_visit));
        }
    }

    public function set price(value:Number) : void
    {
        var oldValue:Number = _internal_price;
        if (isNaN(_internal_price) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_price = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "price", oldValue, _internal_price));
        }
    }

    public function set proc_count(value:int) : void
    {
        var oldValue:int = _internal_proc_count;
        if (oldValue !== value)
        {
            _internal_proc_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_count", oldValue, _internal_proc_count));
        }
    }

    public function set dateclose(value:String) : void
    {
        var oldValue:String = _internal_dateclose;
        if (oldValue !== value)
        {
            _internal_dateclose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dateclose", oldValue, _internal_dateclose));
        }
    }

    public function set isClose(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isClose;
        if (oldValue !== value)
        {
            _internal_isClose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isClose", oldValue, _internal_isClose));
        }
    }

    public function set price_full(value:Number) : void
    {
        var oldValue:Number = _internal_price_full;
        if (isNaN(_internal_price_full) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_price_full = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "price_full", oldValue, _internal_price_full));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HealthPlanPrintModule_ProcedureEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HealthPlanPrintModule_ProcedureEntityMetadata) : void
    {
        var oldValue : _HealthPlanPrintModule_ProcedureEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
