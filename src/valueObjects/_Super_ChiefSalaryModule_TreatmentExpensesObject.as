/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefSalaryModule_TreatmentExpensesObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefSalaryModule_TreatmentExpensesObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefSalaryModule_TreatmentExpensesObject") == null)
            {
                flash.net.registerClassAlias("ChiefSalaryModule_TreatmentExpensesObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefSalaryModule_TreatmentExpensesObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefSalaryModule_TreatmentExpensesObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_accountflow_id : String;
    private var _internal_accountflow_sum : Number;
    private var _internal_accountflow_note : String;
    private var _internal_is_cashpayment : int;
    private var _internal_accountflow_opertimestamp : String;
    private var _internal_accountflow_createdate : String;
    private var _internal_TEXP_TREATMENTPROC_FK : String;
    private var _internal_TEXP_GROUP_FK : String;
    private var _internal_TEXP_GROUP_NAME : String;
    private var _internal_TEXP_GROUP_COLOR : int;
    private var _internal_TEXP_GROUP_GRIDSEQUENCE : int;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefSalaryModule_TreatmentExpensesObject()
    {
        _model = new _ChiefSalaryModule_TreatmentExpensesObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get accountflow_id() : String
    {
        return _internal_accountflow_id;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_sum() : Number
    {
        return _internal_accountflow_sum;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_note() : String
    {
        return _internal_accountflow_note;
    }

    [Bindable(event="propertyChange")]
    public function get is_cashpayment() : int
    {
        return _internal_is_cashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_opertimestamp() : String
    {
        return _internal_accountflow_opertimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_createdate() : String
    {
        return _internal_accountflow_createdate;
    }

    [Bindable(event="propertyChange")]
    public function get TEXP_TREATMENTPROC_FK() : String
    {
        return _internal_TEXP_TREATMENTPROC_FK;
    }

    [Bindable(event="propertyChange")]
    public function get TEXP_GROUP_FK() : String
    {
        return _internal_TEXP_GROUP_FK;
    }

    [Bindable(event="propertyChange")]
    public function get TEXP_GROUP_NAME() : String
    {
        return _internal_TEXP_GROUP_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get TEXP_GROUP_COLOR() : int
    {
        return _internal_TEXP_GROUP_COLOR;
    }

    [Bindable(event="propertyChange")]
    public function get TEXP_GROUP_GRIDSEQUENCE() : int
    {
        return _internal_TEXP_GROUP_GRIDSEQUENCE;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set accountflow_id(value:String) : void
    {
        var oldValue:String = _internal_accountflow_id;
        if (oldValue !== value)
        {
            _internal_accountflow_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_id", oldValue, _internal_accountflow_id));
        }
    }

    public function set accountflow_sum(value:Number) : void
    {
        var oldValue:Number = _internal_accountflow_sum;
        if (isNaN(_internal_accountflow_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_accountflow_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_sum", oldValue, _internal_accountflow_sum));
        }
    }

    public function set accountflow_note(value:String) : void
    {
        var oldValue:String = _internal_accountflow_note;
        if (oldValue !== value)
        {
            _internal_accountflow_note = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_note", oldValue, _internal_accountflow_note));
        }
    }

    public function set is_cashpayment(value:int) : void
    {
        var oldValue:int = _internal_is_cashpayment;
        if (oldValue !== value)
        {
            _internal_is_cashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_cashpayment", oldValue, _internal_is_cashpayment));
        }
    }

    public function set accountflow_opertimestamp(value:String) : void
    {
        var oldValue:String = _internal_accountflow_opertimestamp;
        if (oldValue !== value)
        {
            _internal_accountflow_opertimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_opertimestamp", oldValue, _internal_accountflow_opertimestamp));
        }
    }

    public function set accountflow_createdate(value:String) : void
    {
        var oldValue:String = _internal_accountflow_createdate;
        if (oldValue !== value)
        {
            _internal_accountflow_createdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_createdate", oldValue, _internal_accountflow_createdate));
        }
    }

    public function set TEXP_TREATMENTPROC_FK(value:String) : void
    {
        var oldValue:String = _internal_TEXP_TREATMENTPROC_FK;
        if (oldValue !== value)
        {
            _internal_TEXP_TREATMENTPROC_FK = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TEXP_TREATMENTPROC_FK", oldValue, _internal_TEXP_TREATMENTPROC_FK));
        }
    }

    public function set TEXP_GROUP_FK(value:String) : void
    {
        var oldValue:String = _internal_TEXP_GROUP_FK;
        if (oldValue !== value)
        {
            _internal_TEXP_GROUP_FK = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TEXP_GROUP_FK", oldValue, _internal_TEXP_GROUP_FK));
        }
    }

    public function set TEXP_GROUP_NAME(value:String) : void
    {
        var oldValue:String = _internal_TEXP_GROUP_NAME;
        if (oldValue !== value)
        {
            _internal_TEXP_GROUP_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TEXP_GROUP_NAME", oldValue, _internal_TEXP_GROUP_NAME));
        }
    }

    public function set TEXP_GROUP_COLOR(value:int) : void
    {
        var oldValue:int = _internal_TEXP_GROUP_COLOR;
        if (oldValue !== value)
        {
            _internal_TEXP_GROUP_COLOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TEXP_GROUP_COLOR", oldValue, _internal_TEXP_GROUP_COLOR));
        }
    }

    public function set TEXP_GROUP_GRIDSEQUENCE(value:int) : void
    {
        var oldValue:int = _internal_TEXP_GROUP_GRIDSEQUENCE;
        if (oldValue !== value)
        {
            _internal_TEXP_GROUP_GRIDSEQUENCE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TEXP_GROUP_GRIDSEQUENCE", oldValue, _internal_TEXP_GROUP_GRIDSEQUENCE));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefSalaryModule_TreatmentExpensesObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefSalaryModule_TreatmentExpensesObjectEntityMetadata) : void
    {
        var oldValue : _ChiefSalaryModule_TreatmentExpensesObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
