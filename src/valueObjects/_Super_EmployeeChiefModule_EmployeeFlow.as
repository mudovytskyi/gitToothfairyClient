/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - EmployeeChiefModule_EmployeeFlow.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_EmployeeChiefModule_EmployeeFlow extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("EmployeeChiefModule_EmployeeFlow") == null)
            {
                flash.net.registerClassAlias("EmployeeChiefModule_EmployeeFlow", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("EmployeeChiefModule_EmployeeFlow", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _EmployeeChiefModule_EmployeeFlowEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_summ : Number;
    private var _internal_comment : String;
    private var _internal_operationTimestamp : String;
    private var _internal_isCashpayment : int;
    private var _internal_group_fk : String;
    private var _internal_employee_fk : String;
    private var _internal_authorid : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_EmployeeChiefModule_EmployeeFlow()
    {
        _model = new _EmployeeChiefModule_EmployeeFlowEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get summ() : Number
    {
        return _internal_summ;
    }

    [Bindable(event="propertyChange")]
    public function get comment() : String
    {
        return _internal_comment;
    }

    [Bindable(event="propertyChange")]
    public function get operationTimestamp() : String
    {
        return _internal_operationTimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get isCashpayment() : int
    {
        return _internal_isCashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get group_fk() : String
    {
        return _internal_group_fk;
    }

    [Bindable(event="propertyChange")]
    public function get employee_fk() : String
    {
        return _internal_employee_fk;
    }

    [Bindable(event="propertyChange")]
    public function get authorid() : String
    {
        return _internal_authorid;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set summ(value:Number) : void
    {
        var oldValue:Number = _internal_summ;
        if (isNaN(_internal_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "summ", oldValue, _internal_summ));
        }
    }

    public function set comment(value:String) : void
    {
        var oldValue:String = _internal_comment;
        if (oldValue !== value)
        {
            _internal_comment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "comment", oldValue, _internal_comment));
        }
    }

    public function set operationTimestamp(value:String) : void
    {
        var oldValue:String = _internal_operationTimestamp;
        if (oldValue !== value)
        {
            _internal_operationTimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "operationTimestamp", oldValue, _internal_operationTimestamp));
        }
    }

    public function set isCashpayment(value:int) : void
    {
        var oldValue:int = _internal_isCashpayment;
        if (oldValue !== value)
        {
            _internal_isCashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isCashpayment", oldValue, _internal_isCashpayment));
        }
    }

    public function set group_fk(value:String) : void
    {
        var oldValue:String = _internal_group_fk;
        if (oldValue !== value)
        {
            _internal_group_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_fk", oldValue, _internal_group_fk));
        }
    }

    public function set employee_fk(value:String) : void
    {
        var oldValue:String = _internal_employee_fk;
        if (oldValue !== value)
        {
            _internal_employee_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "employee_fk", oldValue, _internal_employee_fk));
        }
    }

    public function set authorid(value:String) : void
    {
        var oldValue:String = _internal_authorid;
        if (oldValue !== value)
        {
            _internal_authorid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "authorid", oldValue, _internal_authorid));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _EmployeeChiefModule_EmployeeFlowEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _EmployeeChiefModule_EmployeeFlowEntityMetadata) : void
    {
        var oldValue : _EmployeeChiefModule_EmployeeFlowEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
