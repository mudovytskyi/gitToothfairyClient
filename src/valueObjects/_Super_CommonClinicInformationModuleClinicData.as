/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CommonClinicInformationModuleClinicData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CommonClinicInformationModuleClinicData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CommonClinicInformationModuleClinicData") == null)
            {
                flash.net.registerClassAlias("CommonClinicInformationModuleClinicData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CommonClinicInformationModuleClinicData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _CommonClinicInformationModuleClinicDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ADDRESS_INDEX : String;
    private var _internal_ID : String;
    private var _internal_NAME : String;
    private var _internal_LEGALNAME : String;
    private var _internal_ADDRESS : String;
    private var _internal_ORGANIZATIONTYPE : String;
    private var _internal_CODE : String;
    private var _internal_DIRECTOR : String;
    private var _internal_TELEPHONENUMBER : String;
    private var _internal_FAX : String;
    private var _internal_SITENAME : String;
    private var _internal_EMAIL : String;
    private var _internal_ICQ : String;
    private var _internal_SKYPE : String;
    private var _internal_LOGO : Object;
    private var _internal_LEGALADDRESS : String;
    private var _internal_BANK_NAME : String;
    private var _internal_BANK_MFO : String;
    private var _internal_BANK_CURRENTACCOUNT : String;
    private var _internal_LICENSE_NUMBER : String;
    private var _internal_LICENSE_SERIES : String;
    private var _internal_LICENSE_ISSUED : String;
    private var _internal_CHIEFACCOUNTANT : String;
    private var _internal_LICENSE_STARTDATE : String;
    private var _internal_LICENSE_ENDDATE : String;
    private var _internal_AUTHORITY : String;
    private var _internal_CITY : String;
    private var _internal_SHORT_NAME : String;
    private var _internal_MSP_TYPE : String;
    private var _internal_OWNER_PROPERTY_TYPE : String;
    private var _internal_LEGAL_FORM : String;
    private var _internal_KVEDS : String;
    private var _internal_ADDRESSLEGAL_ZIP : String;
    private var _internal_ADDRESSLEGAL_TYPE : String;
    private var _internal_ADDRESSLEGAL_COUNTRY : String;
    private var _internal_ADDRESSLEGAL_AREA : String;
    private var _internal_ADDRESSLEGAL_REGION : String;
    private var _internal_ADDRESSLEGAL_SETTLEMENT : String;
    private var _internal_ADDRESSLEGAL_SETTLEMENT_TYPE : String;
    private var _internal_ADDRESSLEGAL_SETTLEMENT_ID : String;
    private var _internal_ADDRESSLEGAL_STREET_TYPE : String;
    private var _internal_ADDRESSLEGAL_STREET : String;
    private var _internal_ADDRESSLEGAL_BUILDING : String;
    private var _internal_ADDRESSLEGAL_APRT : String;
    private var _internal_PHONE_TYPE : String;
    private var _internal_PHONE : String;
    private var _internal_ACR_CATEGORY : String;
    private var _internal_ACR_ISSUED_DATE : String;
    private var _internal_ACR_EXPIRY_DATE : String;
    private var _internal_ACR_ORDER_NO : String;
    private var _internal_ACR_ORDER_DATE : String;
    private var _internal_CONSENT_SIGN : Boolean;
    private var _internal_EH_ID : String;
    private var _internal_EH_STATUS : String;
    private var _internal_EH_CREATED_BY_MIS_CLIENT_ID : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CommonClinicInformationModuleClinicData()
    {
        _model = new _CommonClinicInformationModuleClinicDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ADDRESS_INDEX() : String
    {
        return _internal_ADDRESS_INDEX;
    }

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get LEGALNAME() : String
    {
        return _internal_LEGALNAME;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS() : String
    {
        return _internal_ADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get ORGANIZATIONTYPE() : String
    {
        return _internal_ORGANIZATIONTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get CODE() : String
    {
        return _internal_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get DIRECTOR() : String
    {
        return _internal_DIRECTOR;
    }

    [Bindable(event="propertyChange")]
    public function get TELEPHONENUMBER() : String
    {
        return _internal_TELEPHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get FAX() : String
    {
        return _internal_FAX;
    }

    [Bindable(event="propertyChange")]
    public function get SITENAME() : String
    {
        return _internal_SITENAME;
    }

    [Bindable(event="propertyChange")]
    public function get EMAIL() : String
    {
        return _internal_EMAIL;
    }

    [Bindable(event="propertyChange")]
    public function get ICQ() : String
    {
        return _internal_ICQ;
    }

    [Bindable(event="propertyChange")]
    public function get SKYPE() : String
    {
        return _internal_SKYPE;
    }

    [Bindable(event="propertyChange")]
    public function get LOGO() : Object
    {
        return _internal_LOGO;
    }

    [Bindable(event="propertyChange")]
    public function get LEGALADDRESS() : String
    {
        return _internal_LEGALADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get BANK_NAME() : String
    {
        return _internal_BANK_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get BANK_MFO() : String
    {
        return _internal_BANK_MFO;
    }

    [Bindable(event="propertyChange")]
    public function get BANK_CURRENTACCOUNT() : String
    {
        return _internal_BANK_CURRENTACCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get LICENSE_NUMBER() : String
    {
        return _internal_LICENSE_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get LICENSE_SERIES() : String
    {
        return _internal_LICENSE_SERIES;
    }

    [Bindable(event="propertyChange")]
    public function get LICENSE_ISSUED() : String
    {
        return _internal_LICENSE_ISSUED;
    }

    [Bindable(event="propertyChange")]
    public function get CHIEFACCOUNTANT() : String
    {
        return _internal_CHIEFACCOUNTANT;
    }

    [Bindable(event="propertyChange")]
    public function get LICENSE_STARTDATE() : String
    {
        return _internal_LICENSE_STARTDATE;
    }

    [Bindable(event="propertyChange")]
    public function get LICENSE_ENDDATE() : String
    {
        return _internal_LICENSE_ENDDATE;
    }

    [Bindable(event="propertyChange")]
    public function get AUTHORITY() : String
    {
        return _internal_AUTHORITY;
    }

    [Bindable(event="propertyChange")]
    public function get CITY() : String
    {
        return _internal_CITY;
    }

    [Bindable(event="propertyChange")]
    public function get SHORT_NAME() : String
    {
        return _internal_SHORT_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get MSP_TYPE() : String
    {
        return _internal_MSP_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get OWNER_PROPERTY_TYPE() : String
    {
        return _internal_OWNER_PROPERTY_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get LEGAL_FORM() : String
    {
        return _internal_LEGAL_FORM;
    }

    [Bindable(event="propertyChange")]
    public function get KVEDS() : String
    {
        return _internal_KVEDS;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_ZIP() : String
    {
        return _internal_ADDRESSLEGAL_ZIP;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_TYPE() : String
    {
        return _internal_ADDRESSLEGAL_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_COUNTRY() : String
    {
        return _internal_ADDRESSLEGAL_COUNTRY;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_AREA() : String
    {
        return _internal_ADDRESSLEGAL_AREA;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_REGION() : String
    {
        return _internal_ADDRESSLEGAL_REGION;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_SETTLEMENT() : String
    {
        return _internal_ADDRESSLEGAL_SETTLEMENT;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_SETTLEMENT_TYPE() : String
    {
        return _internal_ADDRESSLEGAL_SETTLEMENT_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_SETTLEMENT_ID() : String
    {
        return _internal_ADDRESSLEGAL_SETTLEMENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_STREET_TYPE() : String
    {
        return _internal_ADDRESSLEGAL_STREET_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_STREET() : String
    {
        return _internal_ADDRESSLEGAL_STREET;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_BUILDING() : String
    {
        return _internal_ADDRESSLEGAL_BUILDING;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESSLEGAL_APRT() : String
    {
        return _internal_ADDRESSLEGAL_APRT;
    }

    [Bindable(event="propertyChange")]
    public function get PHONE_TYPE() : String
    {
        return _internal_PHONE_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get PHONE() : String
    {
        return _internal_PHONE;
    }

    [Bindable(event="propertyChange")]
    public function get ACR_CATEGORY() : String
    {
        return _internal_ACR_CATEGORY;
    }

    [Bindable(event="propertyChange")]
    public function get ACR_ISSUED_DATE() : String
    {
        return _internal_ACR_ISSUED_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get ACR_EXPIRY_DATE() : String
    {
        return _internal_ACR_EXPIRY_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get ACR_ORDER_NO() : String
    {
        return _internal_ACR_ORDER_NO;
    }

    [Bindable(event="propertyChange")]
    public function get ACR_ORDER_DATE() : String
    {
        return _internal_ACR_ORDER_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get CONSENT_SIGN() : Boolean
    {
        return _internal_CONSENT_SIGN;
    }

    [Bindable(event="propertyChange")]
    public function get EH_ID() : String
    {
        return _internal_EH_ID;
    }

    [Bindable(event="propertyChange")]
    public function get EH_STATUS() : String
    {
        return _internal_EH_STATUS;
    }

    [Bindable(event="propertyChange")]
    public function get EH_CREATED_BY_MIS_CLIENT_ID() : String
    {
        return _internal_EH_CREATED_BY_MIS_CLIENT_ID;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ADDRESS_INDEX(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS_INDEX;
        if (oldValue !== value)
        {
            _internal_ADDRESS_INDEX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS_INDEX", oldValue, _internal_ADDRESS_INDEX));
        }
    }

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set LEGALNAME(value:String) : void
    {
        var oldValue:String = _internal_LEGALNAME;
        if (oldValue !== value)
        {
            _internal_LEGALNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LEGALNAME", oldValue, _internal_LEGALNAME));
        }
    }

    public function set ADDRESS(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS;
        if (oldValue !== value)
        {
            _internal_ADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS", oldValue, _internal_ADDRESS));
        }
    }

    public function set ORGANIZATIONTYPE(value:String) : void
    {
        var oldValue:String = _internal_ORGANIZATIONTYPE;
        if (oldValue !== value)
        {
            _internal_ORGANIZATIONTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ORGANIZATIONTYPE", oldValue, _internal_ORGANIZATIONTYPE));
        }
    }

    public function set CODE(value:String) : void
    {
        var oldValue:String = _internal_CODE;
        if (oldValue !== value)
        {
            _internal_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CODE", oldValue, _internal_CODE));
        }
    }

    public function set DIRECTOR(value:String) : void
    {
        var oldValue:String = _internal_DIRECTOR;
        if (oldValue !== value)
        {
            _internal_DIRECTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DIRECTOR", oldValue, _internal_DIRECTOR));
        }
    }

    public function set TELEPHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_TELEPHONENUMBER;
        if (oldValue !== value)
        {
            _internal_TELEPHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TELEPHONENUMBER", oldValue, _internal_TELEPHONENUMBER));
        }
    }

    public function set FAX(value:String) : void
    {
        var oldValue:String = _internal_FAX;
        if (oldValue !== value)
        {
            _internal_FAX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FAX", oldValue, _internal_FAX));
        }
    }

    public function set SITENAME(value:String) : void
    {
        var oldValue:String = _internal_SITENAME;
        if (oldValue !== value)
        {
            _internal_SITENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SITENAME", oldValue, _internal_SITENAME));
        }
    }

    public function set EMAIL(value:String) : void
    {
        var oldValue:String = _internal_EMAIL;
        if (oldValue !== value)
        {
            _internal_EMAIL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMAIL", oldValue, _internal_EMAIL));
        }
    }

    public function set ICQ(value:String) : void
    {
        var oldValue:String = _internal_ICQ;
        if (oldValue !== value)
        {
            _internal_ICQ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ICQ", oldValue, _internal_ICQ));
        }
    }

    public function set SKYPE(value:String) : void
    {
        var oldValue:String = _internal_SKYPE;
        if (oldValue !== value)
        {
            _internal_SKYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SKYPE", oldValue, _internal_SKYPE));
        }
    }

    public function set LOGO(value:Object) : void
    {
        var oldValue:Object = _internal_LOGO;
        if (oldValue !== value)
        {
            _internal_LOGO = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LOGO", oldValue, _internal_LOGO));
        }
    }

    public function set LEGALADDRESS(value:String) : void
    {
        var oldValue:String = _internal_LEGALADDRESS;
        if (oldValue !== value)
        {
            _internal_LEGALADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LEGALADDRESS", oldValue, _internal_LEGALADDRESS));
        }
    }

    public function set BANK_NAME(value:String) : void
    {
        var oldValue:String = _internal_BANK_NAME;
        if (oldValue !== value)
        {
            _internal_BANK_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BANK_NAME", oldValue, _internal_BANK_NAME));
        }
    }

    public function set BANK_MFO(value:String) : void
    {
        var oldValue:String = _internal_BANK_MFO;
        if (oldValue !== value)
        {
            _internal_BANK_MFO = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BANK_MFO", oldValue, _internal_BANK_MFO));
        }
    }

    public function set BANK_CURRENTACCOUNT(value:String) : void
    {
        var oldValue:String = _internal_BANK_CURRENTACCOUNT;
        if (oldValue !== value)
        {
            _internal_BANK_CURRENTACCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BANK_CURRENTACCOUNT", oldValue, _internal_BANK_CURRENTACCOUNT));
        }
    }

    public function set LICENSE_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_LICENSE_NUMBER;
        if (oldValue !== value)
        {
            _internal_LICENSE_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LICENSE_NUMBER", oldValue, _internal_LICENSE_NUMBER));
        }
    }

    public function set LICENSE_SERIES(value:String) : void
    {
        var oldValue:String = _internal_LICENSE_SERIES;
        if (oldValue !== value)
        {
            _internal_LICENSE_SERIES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LICENSE_SERIES", oldValue, _internal_LICENSE_SERIES));
        }
    }

    public function set LICENSE_ISSUED(value:String) : void
    {
        var oldValue:String = _internal_LICENSE_ISSUED;
        if (oldValue !== value)
        {
            _internal_LICENSE_ISSUED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LICENSE_ISSUED", oldValue, _internal_LICENSE_ISSUED));
        }
    }

    public function set CHIEFACCOUNTANT(value:String) : void
    {
        var oldValue:String = _internal_CHIEFACCOUNTANT;
        if (oldValue !== value)
        {
            _internal_CHIEFACCOUNTANT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CHIEFACCOUNTANT", oldValue, _internal_CHIEFACCOUNTANT));
        }
    }

    public function set LICENSE_STARTDATE(value:String) : void
    {
        var oldValue:String = _internal_LICENSE_STARTDATE;
        if (oldValue !== value)
        {
            _internal_LICENSE_STARTDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LICENSE_STARTDATE", oldValue, _internal_LICENSE_STARTDATE));
        }
    }

    public function set LICENSE_ENDDATE(value:String) : void
    {
        var oldValue:String = _internal_LICENSE_ENDDATE;
        if (oldValue !== value)
        {
            _internal_LICENSE_ENDDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LICENSE_ENDDATE", oldValue, _internal_LICENSE_ENDDATE));
        }
    }

    public function set AUTHORITY(value:String) : void
    {
        var oldValue:String = _internal_AUTHORITY;
        if (oldValue !== value)
        {
            _internal_AUTHORITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AUTHORITY", oldValue, _internal_AUTHORITY));
        }
    }

    public function set CITY(value:String) : void
    {
        var oldValue:String = _internal_CITY;
        if (oldValue !== value)
        {
            _internal_CITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CITY", oldValue, _internal_CITY));
        }
    }

    public function set SHORT_NAME(value:String) : void
    {
        var oldValue:String = _internal_SHORT_NAME;
        if (oldValue !== value)
        {
            _internal_SHORT_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SHORT_NAME", oldValue, _internal_SHORT_NAME));
        }
    }

    public function set MSP_TYPE(value:String) : void
    {
        var oldValue:String = _internal_MSP_TYPE;
        if (oldValue !== value)
        {
            _internal_MSP_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MSP_TYPE", oldValue, _internal_MSP_TYPE));
        }
    }

    public function set OWNER_PROPERTY_TYPE(value:String) : void
    {
        var oldValue:String = _internal_OWNER_PROPERTY_TYPE;
        if (oldValue !== value)
        {
            _internal_OWNER_PROPERTY_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OWNER_PROPERTY_TYPE", oldValue, _internal_OWNER_PROPERTY_TYPE));
        }
    }

    public function set LEGAL_FORM(value:String) : void
    {
        var oldValue:String = _internal_LEGAL_FORM;
        if (oldValue !== value)
        {
            _internal_LEGAL_FORM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LEGAL_FORM", oldValue, _internal_LEGAL_FORM));
        }
    }

    public function set KVEDS(value:String) : void
    {
        var oldValue:String = _internal_KVEDS;
        if (oldValue !== value)
        {
            _internal_KVEDS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "KVEDS", oldValue, _internal_KVEDS));
        }
    }

    public function set ADDRESSLEGAL_ZIP(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_ZIP;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_ZIP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_ZIP", oldValue, _internal_ADDRESSLEGAL_ZIP));
        }
    }

    public function set ADDRESSLEGAL_TYPE(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_TYPE;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_TYPE", oldValue, _internal_ADDRESSLEGAL_TYPE));
        }
    }

    public function set ADDRESSLEGAL_COUNTRY(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_COUNTRY;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_COUNTRY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_COUNTRY", oldValue, _internal_ADDRESSLEGAL_COUNTRY));
        }
    }

    public function set ADDRESSLEGAL_AREA(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_AREA;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_AREA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_AREA", oldValue, _internal_ADDRESSLEGAL_AREA));
        }
    }

    public function set ADDRESSLEGAL_REGION(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_REGION;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_REGION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_REGION", oldValue, _internal_ADDRESSLEGAL_REGION));
        }
    }

    public function set ADDRESSLEGAL_SETTLEMENT(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_SETTLEMENT;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_SETTLEMENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_SETTLEMENT", oldValue, _internal_ADDRESSLEGAL_SETTLEMENT));
        }
    }

    public function set ADDRESSLEGAL_SETTLEMENT_TYPE(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_SETTLEMENT_TYPE;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_SETTLEMENT_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_SETTLEMENT_TYPE", oldValue, _internal_ADDRESSLEGAL_SETTLEMENT_TYPE));
        }
    }

    public function set ADDRESSLEGAL_SETTLEMENT_ID(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_SETTLEMENT_ID;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_SETTLEMENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_SETTLEMENT_ID", oldValue, _internal_ADDRESSLEGAL_SETTLEMENT_ID));
        }
    }

    public function set ADDRESSLEGAL_STREET_TYPE(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_STREET_TYPE;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_STREET_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_STREET_TYPE", oldValue, _internal_ADDRESSLEGAL_STREET_TYPE));
        }
    }

    public function set ADDRESSLEGAL_STREET(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_STREET;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_STREET = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_STREET", oldValue, _internal_ADDRESSLEGAL_STREET));
        }
    }

    public function set ADDRESSLEGAL_BUILDING(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_BUILDING;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_BUILDING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_BUILDING", oldValue, _internal_ADDRESSLEGAL_BUILDING));
        }
    }

    public function set ADDRESSLEGAL_APRT(value:String) : void
    {
        var oldValue:String = _internal_ADDRESSLEGAL_APRT;
        if (oldValue !== value)
        {
            _internal_ADDRESSLEGAL_APRT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESSLEGAL_APRT", oldValue, _internal_ADDRESSLEGAL_APRT));
        }
    }

    public function set PHONE_TYPE(value:String) : void
    {
        var oldValue:String = _internal_PHONE_TYPE;
        if (oldValue !== value)
        {
            _internal_PHONE_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PHONE_TYPE", oldValue, _internal_PHONE_TYPE));
        }
    }

    public function set PHONE(value:String) : void
    {
        var oldValue:String = _internal_PHONE;
        if (oldValue !== value)
        {
            _internal_PHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PHONE", oldValue, _internal_PHONE));
        }
    }

    public function set ACR_CATEGORY(value:String) : void
    {
        var oldValue:String = _internal_ACR_CATEGORY;
        if (oldValue !== value)
        {
            _internal_ACR_CATEGORY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACR_CATEGORY", oldValue, _internal_ACR_CATEGORY));
        }
    }

    public function set ACR_ISSUED_DATE(value:String) : void
    {
        var oldValue:String = _internal_ACR_ISSUED_DATE;
        if (oldValue !== value)
        {
            _internal_ACR_ISSUED_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACR_ISSUED_DATE", oldValue, _internal_ACR_ISSUED_DATE));
        }
    }

    public function set ACR_EXPIRY_DATE(value:String) : void
    {
        var oldValue:String = _internal_ACR_EXPIRY_DATE;
        if (oldValue !== value)
        {
            _internal_ACR_EXPIRY_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACR_EXPIRY_DATE", oldValue, _internal_ACR_EXPIRY_DATE));
        }
    }

    public function set ACR_ORDER_NO(value:String) : void
    {
        var oldValue:String = _internal_ACR_ORDER_NO;
        if (oldValue !== value)
        {
            _internal_ACR_ORDER_NO = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACR_ORDER_NO", oldValue, _internal_ACR_ORDER_NO));
        }
    }

    public function set ACR_ORDER_DATE(value:String) : void
    {
        var oldValue:String = _internal_ACR_ORDER_DATE;
        if (oldValue !== value)
        {
            _internal_ACR_ORDER_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACR_ORDER_DATE", oldValue, _internal_ACR_ORDER_DATE));
        }
    }

    public function set CONSENT_SIGN(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_CONSENT_SIGN;
        if (oldValue !== value)
        {
            _internal_CONSENT_SIGN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CONSENT_SIGN", oldValue, _internal_CONSENT_SIGN));
        }
    }

    public function set EH_ID(value:String) : void
    {
        var oldValue:String = _internal_EH_ID;
        if (oldValue !== value)
        {
            _internal_EH_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_ID", oldValue, _internal_EH_ID));
        }
    }

    public function set EH_STATUS(value:String) : void
    {
        var oldValue:String = _internal_EH_STATUS;
        if (oldValue !== value)
        {
            _internal_EH_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_STATUS", oldValue, _internal_EH_STATUS));
        }
    }

    public function set EH_CREATED_BY_MIS_CLIENT_ID(value:String) : void
    {
        var oldValue:String = _internal_EH_CREATED_BY_MIS_CLIENT_ID;
        if (oldValue !== value)
        {
            _internal_EH_CREATED_BY_MIS_CLIENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_CREATED_BY_MIS_CLIENT_ID", oldValue, _internal_EH_CREATED_BY_MIS_CLIENT_ID));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CommonClinicInformationModuleClinicDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CommonClinicInformationModuleClinicDataEntityMetadata) : void
    {
        var oldValue : _CommonClinicInformationModuleClinicDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
