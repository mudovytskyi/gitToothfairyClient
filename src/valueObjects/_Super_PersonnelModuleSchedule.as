/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PersonnelModuleSchedule.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.PersonnelModuleRule;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PersonnelModuleSchedule extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PersonnelModuleSchedule") == null)
            {
                flash.net.registerClassAlias("PersonnelModuleSchedule", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PersonnelModuleSchedule", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.PersonnelModuleRule.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _PersonnelModuleScheduleEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_DOCTORID : String;
    private var _internal_NAME : String;
    private var _internal_STARTDATE : String;
    private var _internal_ENDDATE : String;
    private var _internal_ISCURRENT : String;
    private var _internal_rulesInForSaveFormat : ArrayCollection;
    model_internal var _internal_rulesInForSaveFormat_leaf:valueObjects.PersonnelModuleRule;
    private var _internal_rules : ArrayCollection;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PersonnelModuleSchedule()
    {
        _model = new _PersonnelModuleScheduleEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTORID() : String
    {
        return _internal_DOCTORID;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get STARTDATE() : String
    {
        return _internal_STARTDATE;
    }

    [Bindable(event="propertyChange")]
    public function get ENDDATE() : String
    {
        return _internal_ENDDATE;
    }

    [Bindable(event="propertyChange")]
    public function get ISCURRENT() : String
    {
        return _internal_ISCURRENT;
    }

    [Bindable(event="propertyChange")]
    public function get rulesInForSaveFormat() : ArrayCollection
    {
        return _internal_rulesInForSaveFormat;
    }

    [Bindable(event="propertyChange")]
    public function get rules() : ArrayCollection
    {
        return _internal_rules;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set DOCTORID(value:String) : void
    {
        var oldValue:String = _internal_DOCTORID;
        if (oldValue !== value)
        {
            _internal_DOCTORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTORID", oldValue, _internal_DOCTORID));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set STARTDATE(value:String) : void
    {
        var oldValue:String = _internal_STARTDATE;
        if (oldValue !== value)
        {
            _internal_STARTDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STARTDATE", oldValue, _internal_STARTDATE));
        }
    }

    public function set ENDDATE(value:String) : void
    {
        var oldValue:String = _internal_ENDDATE;
        if (oldValue !== value)
        {
            _internal_ENDDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ENDDATE", oldValue, _internal_ENDDATE));
        }
    }

    public function set ISCURRENT(value:String) : void
    {
        var oldValue:String = _internal_ISCURRENT;
        if (oldValue !== value)
        {
            _internal_ISCURRENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ISCURRENT", oldValue, _internal_ISCURRENT));
        }
    }

    public function set rulesInForSaveFormat(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_rulesInForSaveFormat;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_rulesInForSaveFormat = value;
            }
            else if (value is Array)
            {
                _internal_rulesInForSaveFormat = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_rulesInForSaveFormat = null;
            }
            else
            {
                throw new Error("value of rulesInForSaveFormat must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rulesInForSaveFormat", oldValue, _internal_rulesInForSaveFormat));
        }
    }

    public function set rules(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_rules;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_rules = value;
            }
            else if (value is Array)
            {
                _internal_rules = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_rules = null;
            }
            else
            {
                throw new Error("value of rules must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rules", oldValue, _internal_rules));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PersonnelModuleScheduleEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PersonnelModuleScheduleEntityMetadata) : void
    {
        var oldValue : _PersonnelModuleScheduleEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
