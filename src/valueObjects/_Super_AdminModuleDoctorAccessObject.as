/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AdminModuleDoctorAccessObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AdminModuleDoctorAccessObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AdminModuleDoctorAccessObject") == null)
            {
                flash.net.registerClassAlias("AdminModuleDoctorAccessObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AdminModuleDoctorAccessObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AdminModuleDoctorAccessObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_DoctorID : String;
    private var _internal_DoctorLname : String;
    private var _internal_DoctorFname : String;
    private var _internal_DoctorMname : String;
    private var _internal_DoctorAdminID : String;
    private var _internal_DoctorAdminLogin : String;
    private var _internal_DoctorAdminPassword : String;
    private var _internal_DoctorAdimAccessAllow : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AdminModuleDoctorAccessObject()
    {
        _model = new _AdminModuleDoctorAccessObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get DoctorID() : String
    {
        return _internal_DoctorID;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorLname() : String
    {
        return _internal_DoctorLname;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorFname() : String
    {
        return _internal_DoctorFname;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorMname() : String
    {
        return _internal_DoctorMname;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorAdminID() : String
    {
        return _internal_DoctorAdminID;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorAdminLogin() : String
    {
        return _internal_DoctorAdminLogin;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorAdminPassword() : String
    {
        return _internal_DoctorAdminPassword;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorAdimAccessAllow() : int
    {
        return _internal_DoctorAdimAccessAllow;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set DoctorID(value:String) : void
    {
        var oldValue:String = _internal_DoctorID;
        if (oldValue !== value)
        {
            _internal_DoctorID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorID", oldValue, _internal_DoctorID));
        }
    }

    public function set DoctorLname(value:String) : void
    {
        var oldValue:String = _internal_DoctorLname;
        if (oldValue !== value)
        {
            _internal_DoctorLname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorLname", oldValue, _internal_DoctorLname));
        }
    }

    public function set DoctorFname(value:String) : void
    {
        var oldValue:String = _internal_DoctorFname;
        if (oldValue !== value)
        {
            _internal_DoctorFname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorFname", oldValue, _internal_DoctorFname));
        }
    }

    public function set DoctorMname(value:String) : void
    {
        var oldValue:String = _internal_DoctorMname;
        if (oldValue !== value)
        {
            _internal_DoctorMname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorMname", oldValue, _internal_DoctorMname));
        }
    }

    public function set DoctorAdminID(value:String) : void
    {
        var oldValue:String = _internal_DoctorAdminID;
        if (oldValue !== value)
        {
            _internal_DoctorAdminID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorAdminID", oldValue, _internal_DoctorAdminID));
        }
    }

    public function set DoctorAdminLogin(value:String) : void
    {
        var oldValue:String = _internal_DoctorAdminLogin;
        if (oldValue !== value)
        {
            _internal_DoctorAdminLogin = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorAdminLogin", oldValue, _internal_DoctorAdminLogin));
        }
    }

    public function set DoctorAdminPassword(value:String) : void
    {
        var oldValue:String = _internal_DoctorAdminPassword;
        if (oldValue !== value)
        {
            _internal_DoctorAdminPassword = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorAdminPassword", oldValue, _internal_DoctorAdminPassword));
        }
    }

    public function set DoctorAdimAccessAllow(value:int) : void
    {
        var oldValue:int = _internal_DoctorAdimAccessAllow;
        if (oldValue !== value)
        {
            _internal_DoctorAdimAccessAllow = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorAdimAccessAllow", oldValue, _internal_DoctorAdimAccessAllow));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AdminModuleDoctorAccessObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AdminModuleDoctorAccessObjectEntityMetadata) : void
    {
        var oldValue : _AdminModuleDoctorAccessObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
