/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PersonnelModuleRule.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PersonnelModuleRule extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PersonnelModuleRule") == null)
            {
                flash.net.registerClassAlias("PersonnelModuleRule", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PersonnelModuleRule", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _PersonnelModuleRuleEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_OPERATINGSCHEDULEID : String;
    private var _internal_NAME : String;
    private var _internal_ISWORKING : String;
    private var _internal_DAYSRANGE : String;
    private var _internal_REPEATTYPE : String;
    private var _internal_SKIPDAYSCOUNT : String;
    private var _internal_STARTTIME : String;
    private var _internal_ENDTIME : String;
    private var _internal_CABINETID : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PersonnelModuleRule()
    {
        _model = new _PersonnelModuleRuleEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get OPERATINGSCHEDULEID() : String
    {
        return _internal_OPERATINGSCHEDULEID;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get ISWORKING() : String
    {
        return _internal_ISWORKING;
    }

    [Bindable(event="propertyChange")]
    public function get DAYSRANGE() : String
    {
        return _internal_DAYSRANGE;
    }

    [Bindable(event="propertyChange")]
    public function get REPEATTYPE() : String
    {
        return _internal_REPEATTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get SKIPDAYSCOUNT() : String
    {
        return _internal_SKIPDAYSCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get STARTTIME() : String
    {
        return _internal_STARTTIME;
    }

    [Bindable(event="propertyChange")]
    public function get ENDTIME() : String
    {
        return _internal_ENDTIME;
    }

    [Bindable(event="propertyChange")]
    public function get CABINETID() : String
    {
        return _internal_CABINETID;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set OPERATINGSCHEDULEID(value:String) : void
    {
        var oldValue:String = _internal_OPERATINGSCHEDULEID;
        if (oldValue !== value)
        {
            _internal_OPERATINGSCHEDULEID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OPERATINGSCHEDULEID", oldValue, _internal_OPERATINGSCHEDULEID));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set ISWORKING(value:String) : void
    {
        var oldValue:String = _internal_ISWORKING;
        if (oldValue !== value)
        {
            _internal_ISWORKING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ISWORKING", oldValue, _internal_ISWORKING));
        }
    }

    public function set DAYSRANGE(value:String) : void
    {
        var oldValue:String = _internal_DAYSRANGE;
        if (oldValue !== value)
        {
            _internal_DAYSRANGE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DAYSRANGE", oldValue, _internal_DAYSRANGE));
        }
    }

    public function set REPEATTYPE(value:String) : void
    {
        var oldValue:String = _internal_REPEATTYPE;
        if (oldValue !== value)
        {
            _internal_REPEATTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REPEATTYPE", oldValue, _internal_REPEATTYPE));
        }
    }

    public function set SKIPDAYSCOUNT(value:String) : void
    {
        var oldValue:String = _internal_SKIPDAYSCOUNT;
        if (oldValue !== value)
        {
            _internal_SKIPDAYSCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SKIPDAYSCOUNT", oldValue, _internal_SKIPDAYSCOUNT));
        }
    }

    public function set STARTTIME(value:String) : void
    {
        var oldValue:String = _internal_STARTTIME;
        if (oldValue !== value)
        {
            _internal_STARTTIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STARTTIME", oldValue, _internal_STARTTIME));
        }
    }

    public function set ENDTIME(value:String) : void
    {
        var oldValue:String = _internal_ENDTIME;
        if (oldValue !== value)
        {
            _internal_ENDTIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ENDTIME", oldValue, _internal_ENDTIME));
        }
    }

    public function set CABINETID(value:String) : void
    {
        var oldValue:String = _internal_CABINETID;
        if (oldValue !== value)
        {
            _internal_CABINETID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CABINETID", oldValue, _internal_CABINETID));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PersonnelModuleRuleEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PersonnelModuleRuleEntityMetadata) : void
    {
        var oldValue : _PersonnelModuleRuleEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
