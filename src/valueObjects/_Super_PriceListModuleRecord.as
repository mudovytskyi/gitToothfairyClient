/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PriceListModuleRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.Form039Module_F39_Val;
import valueObjects.PriceListModuleRecord;
import valueObjects.PriceListModuleRecordFarm;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PriceListModuleRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PriceListModuleRecord") == null)
            {
                flash.net.registerClassAlias("PriceListModuleRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PriceListModuleRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.PriceListModuleRecordFarm.initRemoteClassAliasSingleChild();
        valueObjects.PriceListModuleRecord.initRemoteClassAliasSingleChild();
        valueObjects.Form039Module_F39_Val.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _PriceListModuleRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_label : String;
    private var _internal_isOpen : Boolean;
    private var _internal_isBranch : Boolean;
    private var _internal_ID : String;
    private var _internal_PARENTID : String;
    private var _internal_NODETYPE : String;
    private var _internal_SHIFR : String;
    private var _internal_NAME : String;
    private var _internal_PLANNAME : String;
    private var _internal_UOP : String;
    private var _internal_PROCTYPE : String;
    private var _internal_TOOTHUSE : String;
    private var _internal_PRICE : String;
    private var _internal_PRICE2 : String;
    private var _internal_PRICE3 : String;
    private var _internal_HEALTHTYPE : String;
    private var _internal_PROCTIME : String;
    private var _internal_EXPENSES : Number;
    private var _internal_discount_flag : int;
    private var _internal_farms : ArrayCollection;
    model_internal var _internal_farms_leaf:valueObjects.PriceListModuleRecordFarm;
    private var _internal_CAPTIONLEVEL : String;
    private var _internal_SEQUINCENUMBER : String;
    private var _internal_FARMSCOUNT : int;
    private var _internal_children : ArrayCollection;
    model_internal var _internal_children_leaf:valueObjects.PriceListModuleRecord;
    private var _internal_salary_settings : int;
    private var _internal_complete_examdiag : String;
    private var _internal_HEALTHTYPES : String;
    private var _internal_F39 : ArrayCollection;
    model_internal var _internal_F39_leaf:valueObjects.Form039Module_F39_Val;
    private var _internal_NAME48 : String;
    private var _internal_regCode : int;
    private var _internal_BARCODE : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PriceListModuleRecord()
    {
        _model = new _PriceListModuleRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get label() : String
    {
        return _internal_label;
    }

    [Bindable(event="propertyChange")]
    public function get isOpen() : Boolean
    {
        return _internal_isOpen;
    }

    [Bindable(event="propertyChange")]
    public function get isBranch() : Boolean
    {
        return _internal_isBranch;
    }

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PARENTID() : String
    {
        return _internal_PARENTID;
    }

    [Bindable(event="propertyChange")]
    public function get NODETYPE() : String
    {
        return _internal_NODETYPE;
    }

    [Bindable(event="propertyChange")]
    public function get SHIFR() : String
    {
        return _internal_SHIFR;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get PLANNAME() : String
    {
        return _internal_PLANNAME;
    }

    [Bindable(event="propertyChange")]
    public function get UOP() : String
    {
        return _internal_UOP;
    }

    [Bindable(event="propertyChange")]
    public function get PROCTYPE() : String
    {
        return _internal_PROCTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get TOOTHUSE() : String
    {
        return _internal_TOOTHUSE;
    }

    [Bindable(event="propertyChange")]
    public function get PRICE() : String
    {
        return _internal_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get PRICE2() : String
    {
        return _internal_PRICE2;
    }

    [Bindable(event="propertyChange")]
    public function get PRICE3() : String
    {
        return _internal_PRICE3;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHTYPE() : String
    {
        return _internal_HEALTHTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get PROCTIME() : String
    {
        return _internal_PROCTIME;
    }

    [Bindable(event="propertyChange")]
    public function get EXPENSES() : Number
    {
        return _internal_EXPENSES;
    }

    [Bindable(event="propertyChange")]
    public function get discount_flag() : int
    {
        return _internal_discount_flag;
    }

    [Bindable(event="propertyChange")]
    public function get farms() : ArrayCollection
    {
        return _internal_farms;
    }

    [Bindable(event="propertyChange")]
    public function get CAPTIONLEVEL() : String
    {
        return _internal_CAPTIONLEVEL;
    }

    [Bindable(event="propertyChange")]
    public function get SEQUINCENUMBER() : String
    {
        return _internal_SEQUINCENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get FARMSCOUNT() : int
    {
        return _internal_FARMSCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get children() : ArrayCollection
    {
        return _internal_children;
    }

    [Bindable(event="propertyChange")]
    public function get salary_settings() : int
    {
        return _internal_salary_settings;
    }

    [Bindable(event="propertyChange")]
    public function get complete_examdiag() : String
    {
        return _internal_complete_examdiag;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHTYPES() : String
    {
        return _internal_HEALTHTYPES;
    }

    [Bindable(event="propertyChange")]
    public function get F39() : ArrayCollection
    {
        return _internal_F39;
    }

    [Bindable(event="propertyChange")]
    public function get NAME48() : String
    {
        return _internal_NAME48;
    }

    [Bindable(event="propertyChange")]
    public function get regCode() : int
    {
        return _internal_regCode;
    }

    [Bindable(event="propertyChange")]
    public function get BARCODE() : String
    {
        return _internal_BARCODE;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set label(value:String) : void
    {
        var oldValue:String = _internal_label;
        if (oldValue !== value)
        {
            _internal_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "label", oldValue, _internal_label));
        }
    }

    public function set isOpen(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOpen;
        if (oldValue !== value)
        {
            _internal_isOpen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOpen", oldValue, _internal_isOpen));
        }
    }

    public function set isBranch(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isBranch;
        if (oldValue !== value)
        {
            _internal_isBranch = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isBranch", oldValue, _internal_isBranch));
        }
    }

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set PARENTID(value:String) : void
    {
        var oldValue:String = _internal_PARENTID;
        if (oldValue !== value)
        {
            _internal_PARENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PARENTID", oldValue, _internal_PARENTID));
        }
    }

    public function set NODETYPE(value:String) : void
    {
        var oldValue:String = _internal_NODETYPE;
        if (oldValue !== value)
        {
            _internal_NODETYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NODETYPE", oldValue, _internal_NODETYPE));
        }
    }

    public function set SHIFR(value:String) : void
    {
        var oldValue:String = _internal_SHIFR;
        if (oldValue !== value)
        {
            _internal_SHIFR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SHIFR", oldValue, _internal_SHIFR));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set PLANNAME(value:String) : void
    {
        var oldValue:String = _internal_PLANNAME;
        if (oldValue !== value)
        {
            _internal_PLANNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PLANNAME", oldValue, _internal_PLANNAME));
        }
    }

    public function set UOP(value:String) : void
    {
        var oldValue:String = _internal_UOP;
        if (oldValue !== value)
        {
            _internal_UOP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "UOP", oldValue, _internal_UOP));
        }
    }

    public function set PROCTYPE(value:String) : void
    {
        var oldValue:String = _internal_PROCTYPE;
        if (oldValue !== value)
        {
            _internal_PROCTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PROCTYPE", oldValue, _internal_PROCTYPE));
        }
    }

    public function set TOOTHUSE(value:String) : void
    {
        var oldValue:String = _internal_TOOTHUSE;
        if (oldValue !== value)
        {
            _internal_TOOTHUSE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TOOTHUSE", oldValue, _internal_TOOTHUSE));
        }
    }

    public function set PRICE(value:String) : void
    {
        var oldValue:String = _internal_PRICE;
        if (oldValue !== value)
        {
            _internal_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRICE", oldValue, _internal_PRICE));
        }
    }

    public function set PRICE2(value:String) : void
    {
        var oldValue:String = _internal_PRICE2;
        if (oldValue !== value)
        {
            _internal_PRICE2 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRICE2", oldValue, _internal_PRICE2));
        }
    }

    public function set PRICE3(value:String) : void
    {
        var oldValue:String = _internal_PRICE3;
        if (oldValue !== value)
        {
            _internal_PRICE3 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRICE3", oldValue, _internal_PRICE3));
        }
    }

    public function set HEALTHTYPE(value:String) : void
    {
        var oldValue:String = _internal_HEALTHTYPE;
        if (oldValue !== value)
        {
            _internal_HEALTHTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHTYPE", oldValue, _internal_HEALTHTYPE));
        }
    }

    public function set PROCTIME(value:String) : void
    {
        var oldValue:String = _internal_PROCTIME;
        if (oldValue !== value)
        {
            _internal_PROCTIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PROCTIME", oldValue, _internal_PROCTIME));
        }
    }

    public function set EXPENSES(value:Number) : void
    {
        var oldValue:Number = _internal_EXPENSES;
        if (isNaN(_internal_EXPENSES) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_EXPENSES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXPENSES", oldValue, _internal_EXPENSES));
        }
    }

    public function set discount_flag(value:int) : void
    {
        var oldValue:int = _internal_discount_flag;
        if (oldValue !== value)
        {
            _internal_discount_flag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_flag", oldValue, _internal_discount_flag));
        }
    }

    public function set farms(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_farms;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_farms = value;
            }
            else if (value is Array)
            {
                _internal_farms = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_farms = null;
            }
            else
            {
                throw new Error("value of farms must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farms", oldValue, _internal_farms));
        }
    }

    public function set CAPTIONLEVEL(value:String) : void
    {
        var oldValue:String = _internal_CAPTIONLEVEL;
        if (oldValue !== value)
        {
            _internal_CAPTIONLEVEL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CAPTIONLEVEL", oldValue, _internal_CAPTIONLEVEL));
        }
    }

    public function set SEQUINCENUMBER(value:String) : void
    {
        var oldValue:String = _internal_SEQUINCENUMBER;
        if (oldValue !== value)
        {
            _internal_SEQUINCENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SEQUINCENUMBER", oldValue, _internal_SEQUINCENUMBER));
        }
    }

    public function set FARMSCOUNT(value:int) : void
    {
        var oldValue:int = _internal_FARMSCOUNT;
        if (oldValue !== value)
        {
            _internal_FARMSCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMSCOUNT", oldValue, _internal_FARMSCOUNT));
        }
    }

    public function set children(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_children;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_children = value;
            }
            else if (value is Array)
            {
                _internal_children = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_children = null;
            }
            else
            {
                throw new Error("value of children must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "children", oldValue, _internal_children));
        }
    }

    public function set salary_settings(value:int) : void
    {
        var oldValue:int = _internal_salary_settings;
        if (oldValue !== value)
        {
            _internal_salary_settings = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "salary_settings", oldValue, _internal_salary_settings));
        }
    }

    public function set complete_examdiag(value:String) : void
    {
        var oldValue:String = _internal_complete_examdiag;
        if (oldValue !== value)
        {
            _internal_complete_examdiag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "complete_examdiag", oldValue, _internal_complete_examdiag));
        }
    }

    public function set HEALTHTYPES(value:String) : void
    {
        var oldValue:String = _internal_HEALTHTYPES;
        if (oldValue !== value)
        {
            _internal_HEALTHTYPES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHTYPES", oldValue, _internal_HEALTHTYPES));
        }
    }

    public function set F39(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_F39;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_F39 = value;
            }
            else if (value is Array)
            {
                _internal_F39 = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_F39 = null;
            }
            else
            {
                throw new Error("value of F39 must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "F39", oldValue, _internal_F39));
        }
    }

    public function set NAME48(value:String) : void
    {
        var oldValue:String = _internal_NAME48;
        if (oldValue !== value)
        {
            _internal_NAME48 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME48", oldValue, _internal_NAME48));
        }
    }

    public function set regCode(value:int) : void
    {
        var oldValue:int = _internal_regCode;
        if (oldValue !== value)
        {
            _internal_regCode = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "regCode", oldValue, _internal_regCode));
        }
    }

    public function set BARCODE(value:String) : void
    {
        var oldValue:String = _internal_BARCODE;
        if (oldValue !== value)
        {
            _internal_BARCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BARCODE", oldValue, _internal_BARCODE));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PriceListModuleRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PriceListModuleRecordEntityMetadata) : void
    {
        var oldValue : _PriceListModuleRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
