/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - OrderRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.OrderProcedureRecord;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_OrderRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("OrderRecord") == null)
            {
                flash.net.registerClassAlias("OrderRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("OrderRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.OrderProcedureRecord.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _OrderRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : int;
    private var _internal_tooth : ArrayCollection;
    private var _internal_toothside : ArrayCollection;
    private var _internal_garantdate : String;
    private var _internal_diagnos : String;
    private var _internal_procedures : ArrayCollection;
    model_internal var _internal_procedures_leaf:valueObjects.OrderProcedureRecord;
    private var _internal_paiding_flag : Boolean;
    private var _internal_order_shifr : String;
    private var _internal_paid_date : String;
    private var _internal_paidfix_date : String;
    private var _internal_patient_fname : String;
    private var _internal_patient_sname : String;
    private var _internal_patient_lname : String;
    private var _internal_patient_mobile : String;
    private var _internal_patient_address : String;
    private var _internal_doctor_fname : String;
    private var _internal_doctor_sname : String;
    private var _internal_doctor_lname : String;
    private var _internal_template : String;
    private var _internal_orderdate : String;
    private var _internal_assistent_fname : String;
    private var _internal_assistent_sname : String;
    private var _internal_assistent_lname : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_OrderRecord()
    {
        _model = new _OrderRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : int
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get tooth() : ArrayCollection
    {
        return _internal_tooth;
    }

    [Bindable(event="propertyChange")]
    public function get toothside() : ArrayCollection
    {
        return _internal_toothside;
    }

    [Bindable(event="propertyChange")]
    public function get garantdate() : String
    {
        return _internal_garantdate;
    }

    [Bindable(event="propertyChange")]
    public function get diagnos() : String
    {
        return _internal_diagnos;
    }

    [Bindable(event="propertyChange")]
    public function get procedures() : ArrayCollection
    {
        return _internal_procedures;
    }

    [Bindable(event="propertyChange")]
    public function get paiding_flag() : Boolean
    {
        return _internal_paiding_flag;
    }

    [Bindable(event="propertyChange")]
    public function get order_shifr() : String
    {
        return _internal_order_shifr;
    }

    [Bindable(event="propertyChange")]
    public function get paid_date() : String
    {
        return _internal_paid_date;
    }

    [Bindable(event="propertyChange")]
    public function get paidfix_date() : String
    {
        return _internal_paidfix_date;
    }

    [Bindable(event="propertyChange")]
    public function get patient_fname() : String
    {
        return _internal_patient_fname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_sname() : String
    {
        return _internal_patient_sname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_lname() : String
    {
        return _internal_patient_lname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_mobile() : String
    {
        return _internal_patient_mobile;
    }

    [Bindable(event="propertyChange")]
    public function get patient_address() : String
    {
        return _internal_patient_address;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_fname() : String
    {
        return _internal_doctor_fname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_sname() : String
    {
        return _internal_doctor_sname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lname() : String
    {
        return _internal_doctor_lname;
    }

    [Bindable(event="propertyChange")]
    public function get template() : String
    {
        return _internal_template;
    }

    [Bindable(event="propertyChange")]
    public function get orderdate() : String
    {
        return _internal_orderdate;
    }

    [Bindable(event="propertyChange")]
    public function get assistent_fname() : String
    {
        return _internal_assistent_fname;
    }

    [Bindable(event="propertyChange")]
    public function get assistent_sname() : String
    {
        return _internal_assistent_sname;
    }

    [Bindable(event="propertyChange")]
    public function get assistent_lname() : String
    {
        return _internal_assistent_lname;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:int) : void
    {
        var oldValue:int = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set tooth(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_tooth;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_tooth = value;
            }
            else if (value is Array)
            {
                _internal_tooth = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_tooth = null;
            }
            else
            {
                throw new Error("value of tooth must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tooth", oldValue, _internal_tooth));
        }
    }

    public function set toothside(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_toothside;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_toothside = value;
            }
            else if (value is Array)
            {
                _internal_toothside = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_toothside = null;
            }
            else
            {
                throw new Error("value of toothside must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "toothside", oldValue, _internal_toothside));
        }
    }

    public function set garantdate(value:String) : void
    {
        var oldValue:String = _internal_garantdate;
        if (oldValue !== value)
        {
            _internal_garantdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "garantdate", oldValue, _internal_garantdate));
        }
    }

    public function set diagnos(value:String) : void
    {
        var oldValue:String = _internal_diagnos;
        if (oldValue !== value)
        {
            _internal_diagnos = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diagnos", oldValue, _internal_diagnos));
        }
    }

    public function set procedures(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_procedures;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_procedures = value;
            }
            else if (value is Array)
            {
                _internal_procedures = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_procedures = null;
            }
            else
            {
                throw new Error("value of procedures must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedures", oldValue, _internal_procedures));
        }
    }

    public function set paiding_flag(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_paiding_flag;
        if (oldValue !== value)
        {
            _internal_paiding_flag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "paiding_flag", oldValue, _internal_paiding_flag));
        }
    }

    public function set order_shifr(value:String) : void
    {
        var oldValue:String = _internal_order_shifr;
        if (oldValue !== value)
        {
            _internal_order_shifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "order_shifr", oldValue, _internal_order_shifr));
        }
    }

    public function set paid_date(value:String) : void
    {
        var oldValue:String = _internal_paid_date;
        if (oldValue !== value)
        {
            _internal_paid_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "paid_date", oldValue, _internal_paid_date));
        }
    }

    public function set paidfix_date(value:String) : void
    {
        var oldValue:String = _internal_paidfix_date;
        if (oldValue !== value)
        {
            _internal_paidfix_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "paidfix_date", oldValue, _internal_paidfix_date));
        }
    }

    public function set patient_fname(value:String) : void
    {
        var oldValue:String = _internal_patient_fname;
        if (oldValue !== value)
        {
            _internal_patient_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_fname", oldValue, _internal_patient_fname));
        }
    }

    public function set patient_sname(value:String) : void
    {
        var oldValue:String = _internal_patient_sname;
        if (oldValue !== value)
        {
            _internal_patient_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_sname", oldValue, _internal_patient_sname));
        }
    }

    public function set patient_lname(value:String) : void
    {
        var oldValue:String = _internal_patient_lname;
        if (oldValue !== value)
        {
            _internal_patient_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_lname", oldValue, _internal_patient_lname));
        }
    }

    public function set patient_mobile(value:String) : void
    {
        var oldValue:String = _internal_patient_mobile;
        if (oldValue !== value)
        {
            _internal_patient_mobile = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_mobile", oldValue, _internal_patient_mobile));
        }
    }

    public function set patient_address(value:String) : void
    {
        var oldValue:String = _internal_patient_address;
        if (oldValue !== value)
        {
            _internal_patient_address = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_address", oldValue, _internal_patient_address));
        }
    }

    public function set doctor_fname(value:String) : void
    {
        var oldValue:String = _internal_doctor_fname;
        if (oldValue !== value)
        {
            _internal_doctor_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_fname", oldValue, _internal_doctor_fname));
        }
    }

    public function set doctor_sname(value:String) : void
    {
        var oldValue:String = _internal_doctor_sname;
        if (oldValue !== value)
        {
            _internal_doctor_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_sname", oldValue, _internal_doctor_sname));
        }
    }

    public function set doctor_lname(value:String) : void
    {
        var oldValue:String = _internal_doctor_lname;
        if (oldValue !== value)
        {
            _internal_doctor_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lname", oldValue, _internal_doctor_lname));
        }
    }

    public function set template(value:String) : void
    {
        var oldValue:String = _internal_template;
        if (oldValue !== value)
        {
            _internal_template = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "template", oldValue, _internal_template));
        }
    }

    public function set orderdate(value:String) : void
    {
        var oldValue:String = _internal_orderdate;
        if (oldValue !== value)
        {
            _internal_orderdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "orderdate", oldValue, _internal_orderdate));
        }
    }

    public function set assistent_fname(value:String) : void
    {
        var oldValue:String = _internal_assistent_fname;
        if (oldValue !== value)
        {
            _internal_assistent_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistent_fname", oldValue, _internal_assistent_fname));
        }
    }

    public function set assistent_sname(value:String) : void
    {
        var oldValue:String = _internal_assistent_sname;
        if (oldValue !== value)
        {
            _internal_assistent_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistent_sname", oldValue, _internal_assistent_sname));
        }
    }

    public function set assistent_lname(value:String) : void
    {
        var oldValue:String = _internal_assistent_lname;
        if (oldValue !== value)
        {
            _internal_assistent_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistent_lname", oldValue, _internal_assistent_lname));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _OrderRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _OrderRecordEntityMetadata) : void
    {
        var oldValue : _OrderRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
