/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - InsuranceModule_InsuranceObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.InsuranceModule_InvoiceObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_InsuranceModule_InsuranceObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("InsuranceModule_InsuranceObject") == null)
            {
                flash.net.registerClassAlias("InsuranceModule_InsuranceObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("InsuranceModule_InsuranceObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.InsuranceModule_InvoiceObject.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _InsuranceModule_InsuranceObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_name : String;
    private var _internal_type : String;
    private var _internal_insurance_number : String;
    private var _internal_insurance_fromDate : String;
    private var _internal_insurance_toDate : String;
    private var _internal_patient_id : String;
    private var _internal_patient_lname : String;
    private var _internal_patient_fname : String;
    private var _internal_patient_sname : String;
    private var _internal_summ : Number;
    private var _internal_summClosed : Number;
    private var _internal_children : ArrayCollection;
    model_internal var _internal_children_leaf:valueObjects.InsuranceModule_InvoiceObject;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_InsuranceModule_InsuranceObject()
    {
        _model = new _InsuranceModule_InsuranceObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get name() : String
    {
        return _internal_name;
    }

    [Bindable(event="propertyChange")]
    public function get type() : String
    {
        return _internal_type;
    }

    [Bindable(event="propertyChange")]
    public function get insurance_number() : String
    {
        return _internal_insurance_number;
    }

    [Bindable(event="propertyChange")]
    public function get insurance_fromDate() : String
    {
        return _internal_insurance_fromDate;
    }

    [Bindable(event="propertyChange")]
    public function get insurance_toDate() : String
    {
        return _internal_insurance_toDate;
    }

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_lname() : String
    {
        return _internal_patient_lname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_fname() : String
    {
        return _internal_patient_fname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_sname() : String
    {
        return _internal_patient_sname;
    }

    [Bindable(event="propertyChange")]
    public function get summ() : Number
    {
        return _internal_summ;
    }

    [Bindable(event="propertyChange")]
    public function get summClosed() : Number
    {
        return _internal_summClosed;
    }

    [Bindable(event="propertyChange")]
    public function get children() : ArrayCollection
    {
        return _internal_children;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set name(value:String) : void
    {
        var oldValue:String = _internal_name;
        if (oldValue !== value)
        {
            _internal_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name", oldValue, _internal_name));
        }
    }

    public function set type(value:String) : void
    {
        var oldValue:String = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    public function set insurance_number(value:String) : void
    {
        var oldValue:String = _internal_insurance_number;
        if (oldValue !== value)
        {
            _internal_insurance_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "insurance_number", oldValue, _internal_insurance_number));
        }
    }

    public function set insurance_fromDate(value:String) : void
    {
        var oldValue:String = _internal_insurance_fromDate;
        if (oldValue !== value)
        {
            _internal_insurance_fromDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "insurance_fromDate", oldValue, _internal_insurance_fromDate));
        }
    }

    public function set insurance_toDate(value:String) : void
    {
        var oldValue:String = _internal_insurance_toDate;
        if (oldValue !== value)
        {
            _internal_insurance_toDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "insurance_toDate", oldValue, _internal_insurance_toDate));
        }
    }

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set patient_lname(value:String) : void
    {
        var oldValue:String = _internal_patient_lname;
        if (oldValue !== value)
        {
            _internal_patient_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_lname", oldValue, _internal_patient_lname));
        }
    }

    public function set patient_fname(value:String) : void
    {
        var oldValue:String = _internal_patient_fname;
        if (oldValue !== value)
        {
            _internal_patient_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_fname", oldValue, _internal_patient_fname));
        }
    }

    public function set patient_sname(value:String) : void
    {
        var oldValue:String = _internal_patient_sname;
        if (oldValue !== value)
        {
            _internal_patient_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_sname", oldValue, _internal_patient_sname));
        }
    }

    public function set summ(value:Number) : void
    {
        var oldValue:Number = _internal_summ;
        if (isNaN(_internal_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "summ", oldValue, _internal_summ));
        }
    }

    public function set summClosed(value:Number) : void
    {
        var oldValue:Number = _internal_summClosed;
        if (isNaN(_internal_summClosed) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_summClosed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "summClosed", oldValue, _internal_summClosed));
        }
    }

    public function set children(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_children;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_children = value;
            }
            else if (value is Array)
            {
                _internal_children = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_children = null;
            }
            else
            {
                throw new Error("value of children must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "children", oldValue, _internal_children));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _InsuranceModule_InsuranceObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _InsuranceModule_InsuranceObjectEntityMetadata) : void
    {
        var oldValue : _InsuranceModule_InsuranceObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
