/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PatientCardModulePatientTestCard.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PatientCardModulePatientTestCard extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PatientCardModulePatientTestCard") == null)
            {
                flash.net.registerClassAlias("PatientCardModulePatientTestCard", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PatientCardModulePatientTestCard", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _PatientCardModulePatientTestCardEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_PATIENTID : int;
    private var _internal_TESTDATE : Object;
    private var _internal_CARDIACDISEASE : int;
    private var _internal_KIDNEYDISEASE : int;
    private var _internal_LIVERDISEASE : int;
    private var _internal_OTHERDISEASE : int;
    private var _internal_ARTERIALPRESSURE : int;
    private var _internal_RHEUMATISM : int;
    private var _internal_HEPATITIS : int;
    private var _internal_HEPATITISDATE : String;
    private var _internal_HEPATITISTYPE : int;
    private var _internal_EPILEPSIA : int;
    private var _internal_DIABETES : int;
    private var _internal_FIT : int;
    private var _internal_BLEEDING : int;
    private var _internal_ALLERGYFLAG : int;
    private var _internal_ALLERGY : String;
    private var _internal_PREGNANCY : int;
    private var _internal_MEDICATIONS : String;
    private var _internal_BLOODGROUP : int;
    private var _internal_RHESUSFACTOR : int;
    private var _internal_MOUTHULCER : int;
    private var _internal_FUNGUS : int;
    private var _internal_FEVER : int;
    private var _internal_THROATACHE : int;
    private var _internal_LYMPHNODES : int;
    private var _internal_REDSKINAREA : int;
    private var _internal_HYPERHIDROSIS : int;
    private var _internal_DIARRHEA : int;
    private var _internal_AMNESIA : int;
    private var _internal_WEIGHTLOSS : int;
    private var _internal_HEADACHE : int;
    private var _internal_AIDS : int;
    private var _internal_WORKWITHBLOOD : int;
    private var _internal_NARCOMANIA : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PatientCardModulePatientTestCard()
    {
        _model = new _PatientCardModulePatientTestCardEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTID() : int
    {
        return _internal_PATIENTID;
    }

    [Bindable(event="propertyChange")]
    public function get TESTDATE() : Object
    {
        return _internal_TESTDATE;
    }

    [Bindable(event="propertyChange")]
    public function get CARDIACDISEASE() : int
    {
        return _internal_CARDIACDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get KIDNEYDISEASE() : int
    {
        return _internal_KIDNEYDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get LIVERDISEASE() : int
    {
        return _internal_LIVERDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get OTHERDISEASE() : int
    {
        return _internal_OTHERDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get ARTERIALPRESSURE() : int
    {
        return _internal_ARTERIALPRESSURE;
    }

    [Bindable(event="propertyChange")]
    public function get RHEUMATISM() : int
    {
        return _internal_RHEUMATISM;
    }

    [Bindable(event="propertyChange")]
    public function get HEPATITIS() : int
    {
        return _internal_HEPATITIS;
    }

    [Bindable(event="propertyChange")]
    public function get HEPATITISDATE() : String
    {
        return _internal_HEPATITISDATE;
    }

    [Bindable(event="propertyChange")]
    public function get HEPATITISTYPE() : int
    {
        return _internal_HEPATITISTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get EPILEPSIA() : int
    {
        return _internal_EPILEPSIA;
    }

    [Bindable(event="propertyChange")]
    public function get DIABETES() : int
    {
        return _internal_DIABETES;
    }

    [Bindable(event="propertyChange")]
    public function get FIT() : int
    {
        return _internal_FIT;
    }

    [Bindable(event="propertyChange")]
    public function get BLEEDING() : int
    {
        return _internal_BLEEDING;
    }

    [Bindable(event="propertyChange")]
    public function get ALLERGYFLAG() : int
    {
        return _internal_ALLERGYFLAG;
    }

    [Bindable(event="propertyChange")]
    public function get ALLERGY() : String
    {
        return _internal_ALLERGY;
    }

    [Bindable(event="propertyChange")]
    public function get PREGNANCY() : int
    {
        return _internal_PREGNANCY;
    }

    [Bindable(event="propertyChange")]
    public function get MEDICATIONS() : String
    {
        return _internal_MEDICATIONS;
    }

    [Bindable(event="propertyChange")]
    public function get BLOODGROUP() : int
    {
        return _internal_BLOODGROUP;
    }

    [Bindable(event="propertyChange")]
    public function get RHESUSFACTOR() : int
    {
        return _internal_RHESUSFACTOR;
    }

    [Bindable(event="propertyChange")]
    public function get MOUTHULCER() : int
    {
        return _internal_MOUTHULCER;
    }

    [Bindable(event="propertyChange")]
    public function get FUNGUS() : int
    {
        return _internal_FUNGUS;
    }

    [Bindable(event="propertyChange")]
    public function get FEVER() : int
    {
        return _internal_FEVER;
    }

    [Bindable(event="propertyChange")]
    public function get THROATACHE() : int
    {
        return _internal_THROATACHE;
    }

    [Bindable(event="propertyChange")]
    public function get LYMPHNODES() : int
    {
        return _internal_LYMPHNODES;
    }

    [Bindable(event="propertyChange")]
    public function get REDSKINAREA() : int
    {
        return _internal_REDSKINAREA;
    }

    [Bindable(event="propertyChange")]
    public function get HYPERHIDROSIS() : int
    {
        return _internal_HYPERHIDROSIS;
    }

    [Bindable(event="propertyChange")]
    public function get DIARRHEA() : int
    {
        return _internal_DIARRHEA;
    }

    [Bindable(event="propertyChange")]
    public function get AMNESIA() : int
    {
        return _internal_AMNESIA;
    }

    [Bindable(event="propertyChange")]
    public function get WEIGHTLOSS() : int
    {
        return _internal_WEIGHTLOSS;
    }

    [Bindable(event="propertyChange")]
    public function get HEADACHE() : int
    {
        return _internal_HEADACHE;
    }

    [Bindable(event="propertyChange")]
    public function get AIDS() : int
    {
        return _internal_AIDS;
    }

    [Bindable(event="propertyChange")]
    public function get WORKWITHBLOOD() : int
    {
        return _internal_WORKWITHBLOOD;
    }

    [Bindable(event="propertyChange")]
    public function get NARCOMANIA() : int
    {
        return _internal_NARCOMANIA;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set PATIENTID(value:int) : void
    {
        var oldValue:int = _internal_PATIENTID;
        if (oldValue !== value)
        {
            _internal_PATIENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTID", oldValue, _internal_PATIENTID));
        }
    }

    public function set TESTDATE(value:Object) : void
    {
        var oldValue:Object = _internal_TESTDATE;
        if (oldValue !== value)
        {
            _internal_TESTDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TESTDATE", oldValue, _internal_TESTDATE));
        }
    }

    public function set CARDIACDISEASE(value:int) : void
    {
        var oldValue:int = _internal_CARDIACDISEASE;
        if (oldValue !== value)
        {
            _internal_CARDIACDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDIACDISEASE", oldValue, _internal_CARDIACDISEASE));
        }
    }

    public function set KIDNEYDISEASE(value:int) : void
    {
        var oldValue:int = _internal_KIDNEYDISEASE;
        if (oldValue !== value)
        {
            _internal_KIDNEYDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "KIDNEYDISEASE", oldValue, _internal_KIDNEYDISEASE));
        }
    }

    public function set LIVERDISEASE(value:int) : void
    {
        var oldValue:int = _internal_LIVERDISEASE;
        if (oldValue !== value)
        {
            _internal_LIVERDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LIVERDISEASE", oldValue, _internal_LIVERDISEASE));
        }
    }

    public function set OTHERDISEASE(value:int) : void
    {
        var oldValue:int = _internal_OTHERDISEASE;
        if (oldValue !== value)
        {
            _internal_OTHERDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OTHERDISEASE", oldValue, _internal_OTHERDISEASE));
        }
    }

    public function set ARTERIALPRESSURE(value:int) : void
    {
        var oldValue:int = _internal_ARTERIALPRESSURE;
        if (oldValue !== value)
        {
            _internal_ARTERIALPRESSURE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ARTERIALPRESSURE", oldValue, _internal_ARTERIALPRESSURE));
        }
    }

    public function set RHEUMATISM(value:int) : void
    {
        var oldValue:int = _internal_RHEUMATISM;
        if (oldValue !== value)
        {
            _internal_RHEUMATISM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RHEUMATISM", oldValue, _internal_RHEUMATISM));
        }
    }

    public function set HEPATITIS(value:int) : void
    {
        var oldValue:int = _internal_HEPATITIS;
        if (oldValue !== value)
        {
            _internal_HEPATITIS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEPATITIS", oldValue, _internal_HEPATITIS));
        }
    }

    public function set HEPATITISDATE(value:String) : void
    {
        var oldValue:String = _internal_HEPATITISDATE;
        if (oldValue !== value)
        {
            _internal_HEPATITISDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEPATITISDATE", oldValue, _internal_HEPATITISDATE));
        }
    }

    public function set HEPATITISTYPE(value:int) : void
    {
        var oldValue:int = _internal_HEPATITISTYPE;
        if (oldValue !== value)
        {
            _internal_HEPATITISTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEPATITISTYPE", oldValue, _internal_HEPATITISTYPE));
        }
    }

    public function set EPILEPSIA(value:int) : void
    {
        var oldValue:int = _internal_EPILEPSIA;
        if (oldValue !== value)
        {
            _internal_EPILEPSIA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EPILEPSIA", oldValue, _internal_EPILEPSIA));
        }
    }

    public function set DIABETES(value:int) : void
    {
        var oldValue:int = _internal_DIABETES;
        if (oldValue !== value)
        {
            _internal_DIABETES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DIABETES", oldValue, _internal_DIABETES));
        }
    }

    public function set FIT(value:int) : void
    {
        var oldValue:int = _internal_FIT;
        if (oldValue !== value)
        {
            _internal_FIT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FIT", oldValue, _internal_FIT));
        }
    }

    public function set BLEEDING(value:int) : void
    {
        var oldValue:int = _internal_BLEEDING;
        if (oldValue !== value)
        {
            _internal_BLEEDING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BLEEDING", oldValue, _internal_BLEEDING));
        }
    }

    public function set ALLERGYFLAG(value:int) : void
    {
        var oldValue:int = _internal_ALLERGYFLAG;
        if (oldValue !== value)
        {
            _internal_ALLERGYFLAG = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ALLERGYFLAG", oldValue, _internal_ALLERGYFLAG));
        }
    }

    public function set ALLERGY(value:String) : void
    {
        var oldValue:String = _internal_ALLERGY;
        if (oldValue !== value)
        {
            _internal_ALLERGY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ALLERGY", oldValue, _internal_ALLERGY));
        }
    }

    public function set PREGNANCY(value:int) : void
    {
        var oldValue:int = _internal_PREGNANCY;
        if (oldValue !== value)
        {
            _internal_PREGNANCY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PREGNANCY", oldValue, _internal_PREGNANCY));
        }
    }

    public function set MEDICATIONS(value:String) : void
    {
        var oldValue:String = _internal_MEDICATIONS;
        if (oldValue !== value)
        {
            _internal_MEDICATIONS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MEDICATIONS", oldValue, _internal_MEDICATIONS));
        }
    }

    public function set BLOODGROUP(value:int) : void
    {
        var oldValue:int = _internal_BLOODGROUP;
        if (oldValue !== value)
        {
            _internal_BLOODGROUP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BLOODGROUP", oldValue, _internal_BLOODGROUP));
        }
    }

    public function set RHESUSFACTOR(value:int) : void
    {
        var oldValue:int = _internal_RHESUSFACTOR;
        if (oldValue !== value)
        {
            _internal_RHESUSFACTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RHESUSFACTOR", oldValue, _internal_RHESUSFACTOR));
        }
    }

    public function set MOUTHULCER(value:int) : void
    {
        var oldValue:int = _internal_MOUTHULCER;
        if (oldValue !== value)
        {
            _internal_MOUTHULCER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOUTHULCER", oldValue, _internal_MOUTHULCER));
        }
    }

    public function set FUNGUS(value:int) : void
    {
        var oldValue:int = _internal_FUNGUS;
        if (oldValue !== value)
        {
            _internal_FUNGUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FUNGUS", oldValue, _internal_FUNGUS));
        }
    }

    public function set FEVER(value:int) : void
    {
        var oldValue:int = _internal_FEVER;
        if (oldValue !== value)
        {
            _internal_FEVER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FEVER", oldValue, _internal_FEVER));
        }
    }

    public function set THROATACHE(value:int) : void
    {
        var oldValue:int = _internal_THROATACHE;
        if (oldValue !== value)
        {
            _internal_THROATACHE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "THROATACHE", oldValue, _internal_THROATACHE));
        }
    }

    public function set LYMPHNODES(value:int) : void
    {
        var oldValue:int = _internal_LYMPHNODES;
        if (oldValue !== value)
        {
            _internal_LYMPHNODES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LYMPHNODES", oldValue, _internal_LYMPHNODES));
        }
    }

    public function set REDSKINAREA(value:int) : void
    {
        var oldValue:int = _internal_REDSKINAREA;
        if (oldValue !== value)
        {
            _internal_REDSKINAREA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REDSKINAREA", oldValue, _internal_REDSKINAREA));
        }
    }

    public function set HYPERHIDROSIS(value:int) : void
    {
        var oldValue:int = _internal_HYPERHIDROSIS;
        if (oldValue !== value)
        {
            _internal_HYPERHIDROSIS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HYPERHIDROSIS", oldValue, _internal_HYPERHIDROSIS));
        }
    }

    public function set DIARRHEA(value:int) : void
    {
        var oldValue:int = _internal_DIARRHEA;
        if (oldValue !== value)
        {
            _internal_DIARRHEA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DIARRHEA", oldValue, _internal_DIARRHEA));
        }
    }

    public function set AMNESIA(value:int) : void
    {
        var oldValue:int = _internal_AMNESIA;
        if (oldValue !== value)
        {
            _internal_AMNESIA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AMNESIA", oldValue, _internal_AMNESIA));
        }
    }

    public function set WEIGHTLOSS(value:int) : void
    {
        var oldValue:int = _internal_WEIGHTLOSS;
        if (oldValue !== value)
        {
            _internal_WEIGHTLOSS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WEIGHTLOSS", oldValue, _internal_WEIGHTLOSS));
        }
    }

    public function set HEADACHE(value:int) : void
    {
        var oldValue:int = _internal_HEADACHE;
        if (oldValue !== value)
        {
            _internal_HEADACHE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEADACHE", oldValue, _internal_HEADACHE));
        }
    }

    public function set AIDS(value:int) : void
    {
        var oldValue:int = _internal_AIDS;
        if (oldValue !== value)
        {
            _internal_AIDS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AIDS", oldValue, _internal_AIDS));
        }
    }

    public function set WORKWITHBLOOD(value:int) : void
    {
        var oldValue:int = _internal_WORKWITHBLOOD;
        if (oldValue !== value)
        {
            _internal_WORKWITHBLOOD = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WORKWITHBLOOD", oldValue, _internal_WORKWITHBLOOD));
        }
    }

    public function set NARCOMANIA(value:int) : void
    {
        var oldValue:int = _internal_NARCOMANIA;
        if (oldValue !== value)
        {
            _internal_NARCOMANIA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NARCOMANIA", oldValue, _internal_NARCOMANIA));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PatientCardModulePatientTestCardEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PatientCardModulePatientTestCardEntityMetadata) : void
    {
        var oldValue : _PatientCardModulePatientTestCardEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
