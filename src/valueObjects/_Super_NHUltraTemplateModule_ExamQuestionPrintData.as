/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - NHUltraTemplateModule_ExamQuestionPrintData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_NHUltraTemplateModule_ExamQuestionPrintData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("NHUltraTemplateModule_ExamQuestionPrintData") == null)
            {
                flash.net.registerClassAlias("NHUltraTemplateModule_ExamQuestionPrintData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("NHUltraTemplateModule_ExamQuestionPrintData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _NHUltraTemplateModule_ExamQuestionPrintDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_number : String;
    private var _internal_template_id : int;
    private var _internal_question : String;
    private var _internal_answers : ArrayCollection;
    private var _internal_results : String;
    private var _internal_examdate : String;
    private var _internal_type : String;
    private var _internal_answers_count : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_NHUltraTemplateModule_ExamQuestionPrintData()
    {
        _model = new _NHUltraTemplateModule_ExamQuestionPrintDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get number() : String
    {
        return _internal_number;
    }

    [Bindable(event="propertyChange")]
    public function get template_id() : int
    {
        return _internal_template_id;
    }

    [Bindable(event="propertyChange")]
    public function get question() : String
    {
        return _internal_question;
    }

    [Bindable(event="propertyChange")]
    public function get answers() : ArrayCollection
    {
        return _internal_answers;
    }

    [Bindable(event="propertyChange")]
    public function get results() : String
    {
        return _internal_results;
    }

    [Bindable(event="propertyChange")]
    public function get examdate() : String
    {
        return _internal_examdate;
    }

    [Bindable(event="propertyChange")]
    public function get type() : String
    {
        return _internal_type;
    }

    [Bindable(event="propertyChange")]
    public function get answers_count() : int
    {
        return _internal_answers_count;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set number(value:String) : void
    {
        var oldValue:String = _internal_number;
        if (oldValue !== value)
        {
            _internal_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "number", oldValue, _internal_number));
        }
    }

    public function set template_id(value:int) : void
    {
        var oldValue:int = _internal_template_id;
        if (oldValue !== value)
        {
            _internal_template_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "template_id", oldValue, _internal_template_id));
        }
    }

    public function set question(value:String) : void
    {
        var oldValue:String = _internal_question;
        if (oldValue !== value)
        {
            _internal_question = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "question", oldValue, _internal_question));
        }
    }

    public function set answers(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_answers;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_answers = value;
            }
            else if (value is Array)
            {
                _internal_answers = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_answers = null;
            }
            else
            {
                throw new Error("value of answers must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "answers", oldValue, _internal_answers));
        }
    }

    public function set results(value:String) : void
    {
        var oldValue:String = _internal_results;
        if (oldValue !== value)
        {
            _internal_results = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "results", oldValue, _internal_results));
        }
    }

    public function set examdate(value:String) : void
    {
        var oldValue:String = _internal_examdate;
        if (oldValue !== value)
        {
            _internal_examdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examdate", oldValue, _internal_examdate));
        }
    }

    public function set type(value:String) : void
    {
        var oldValue:String = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    public function set answers_count(value:int) : void
    {
        var oldValue:int = _internal_answers_count;
        if (oldValue !== value)
        {
            _internal_answers_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "answers_count", oldValue, _internal_answers_count));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _NHUltraTemplateModule_ExamQuestionPrintDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _NHUltraTemplateModule_ExamQuestionPrintDataEntityMetadata) : void
    {
        var oldValue : _NHUltraTemplateModule_ExamQuestionPrintDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
