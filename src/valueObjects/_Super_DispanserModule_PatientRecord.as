/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - DispanserModule_PatientRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.DispanserModule_PatientDispancerRecord;
import valueObjects.DispanserModule_mobilephone;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_DispanserModule_PatientRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("DispanserModule_PatientRecord") == null)
            {
                flash.net.registerClassAlias("DispanserModule_PatientRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("DispanserModule_PatientRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.DispanserModule_mobilephone.initRemoteClassAliasSingleChild();
        valueObjects.DispanserModule_PatientDispancerRecord.initRemoteClassAliasSingleChild();
        valueObjects.DispanserModule_Disptype.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _DispanserModule_PatientRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_patient_id : String;
    private var _internal_patient_sname : String;
    private var _internal_patient_lname : String;
    private var _internal_patient_fname : String;
    private var _internal_patient_cardnum : String;
    private var _internal_mobiles : ArrayCollection;
    model_internal var _internal_mobiles_leaf:valueObjects.DispanserModule_mobilephone;
    private var _internal_dispTasks : ArrayCollection;
    model_internal var _internal_dispTasks_leaf:valueObjects.DispanserModule_PatientDispancerRecord;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_DispanserModule_PatientRecord()
    {
        _model = new _DispanserModule_PatientRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_sname() : String
    {
        return _internal_patient_sname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_lname() : String
    {
        return _internal_patient_lname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_fname() : String
    {
        return _internal_patient_fname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_cardnum() : String
    {
        return _internal_patient_cardnum;
    }

    [Bindable(event="propertyChange")]
    public function get mobiles() : ArrayCollection
    {
        return _internal_mobiles;
    }

    [Bindable(event="propertyChange")]
    public function get dispTasks() : ArrayCollection
    {
        return _internal_dispTasks;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set patient_sname(value:String) : void
    {
        var oldValue:String = _internal_patient_sname;
        if (oldValue !== value)
        {
            _internal_patient_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_sname", oldValue, _internal_patient_sname));
        }
    }

    public function set patient_lname(value:String) : void
    {
        var oldValue:String = _internal_patient_lname;
        if (oldValue !== value)
        {
            _internal_patient_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_lname", oldValue, _internal_patient_lname));
        }
    }

    public function set patient_fname(value:String) : void
    {
        var oldValue:String = _internal_patient_fname;
        if (oldValue !== value)
        {
            _internal_patient_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_fname", oldValue, _internal_patient_fname));
        }
    }

    public function set patient_cardnum(value:String) : void
    {
        var oldValue:String = _internal_patient_cardnum;
        if (oldValue !== value)
        {
            _internal_patient_cardnum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_cardnum", oldValue, _internal_patient_cardnum));
        }
    }

    public function set mobiles(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_mobiles;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_mobiles = value;
            }
            else if (value is Array)
            {
                _internal_mobiles = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_mobiles = null;
            }
            else
            {
                throw new Error("value of mobiles must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "mobiles", oldValue, _internal_mobiles));
        }
    }

    public function set dispTasks(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_dispTasks;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_dispTasks = value;
            }
            else if (value is Array)
            {
                _internal_dispTasks = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_dispTasks = null;
            }
            else
            {
                throw new Error("value of dispTasks must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dispTasks", oldValue, _internal_dispTasks));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _DispanserModule_PatientRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _DispanserModule_PatientRecordEntityMetadata) : void
    {
        var oldValue : _DispanserModule_PatientRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
