/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AccountModule_FlowObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AccountModule_FlowObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AccountModule_FlowObject") == null)
            {
                flash.net.registerClassAlias("AccountModule_FlowObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AccountModule_FlowObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AccountModule_FlowObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_is_invoice : Boolean;
    private var _internal_invoice_id : String;
    private var _internal_invoice_timestamp : String;
    private var _internal_invoice_number : String;
    private var _internal_invoice_sum : Number;
    private var _internal_invoice_discountsum : Number;
    private var _internal_invoice_paidincash : Number;
    private var _internal_invoice_discount : Number;
    private var _internal_invoice_isCashpayment : int;
    private var _internal_patient_id : String;
    private var _internal_patient_insuranceId : String;
    private var _internal_patient_insuranceNumber : String;
    private var _internal_patient_insuranceCompanyName : String;
    private var _internal_patient_insuranceCompanyId : String;
    private var _internal_patient_fname : String;
    private var _internal_patient_lname : String;
    private var _internal_patient_sname : String;
    private var _internal_patient_cardnumber : String;
    private var _internal_patient_discount : Number;
    private var _internal_discount_amount_auto : Number;
    private var _internal_INDDISCOUNT : Boolean;
    private var _internal_patient_balance : Number;
    private var _internal_patient_paidincash : Number;
    private var _internal_patient_balanceNoncash : Number;
    private var _internal_patient_paidincashNoncash : Number;
    private var _internal_patient_balanceAll : Number;
    private var _internal_patient_paidincashAll : Number;
    private var _internal_patient_label : String;
    private var _internal_procedures_MinDate : String;
    private var _internal_procedures_MaxDate : String;
    private var _internal_author_id : String;
    private var _internal_author_shortname : String;
    private var _internal_subdivision_id : String;
    private var _internal_subdivision_name : String;
    private var _internal_discount_cash : Number;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AccountModule_FlowObject()
    {
        _model = new _AccountModule_FlowObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get is_invoice() : Boolean
    {
        return _internal_is_invoice;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_id() : String
    {
        return _internal_invoice_id;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_timestamp() : String
    {
        return _internal_invoice_timestamp;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_number() : String
    {
        return _internal_invoice_number;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_sum() : Number
    {
        return _internal_invoice_sum;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_discountsum() : Number
    {
        return _internal_invoice_discountsum;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_paidincash() : Number
    {
        return _internal_invoice_paidincash;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_discount() : Number
    {
        return _internal_invoice_discount;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_isCashpayment() : int
    {
        return _internal_invoice_isCashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_insuranceId() : String
    {
        return _internal_patient_insuranceId;
    }

    [Bindable(event="propertyChange")]
    public function get patient_insuranceNumber() : String
    {
        return _internal_patient_insuranceNumber;
    }

    [Bindable(event="propertyChange")]
    public function get patient_insuranceCompanyName() : String
    {
        return _internal_patient_insuranceCompanyName;
    }

    [Bindable(event="propertyChange")]
    public function get patient_insuranceCompanyId() : String
    {
        return _internal_patient_insuranceCompanyId;
    }

    [Bindable(event="propertyChange")]
    public function get patient_fname() : String
    {
        return _internal_patient_fname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_lname() : String
    {
        return _internal_patient_lname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_sname() : String
    {
        return _internal_patient_sname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_cardnumber() : String
    {
        return _internal_patient_cardnumber;
    }

    [Bindable(event="propertyChange")]
    public function get patient_discount() : Number
    {
        return _internal_patient_discount;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount_auto() : Number
    {
        return _internal_discount_amount_auto;
    }

    [Bindable(event="propertyChange")]
    public function get INDDISCOUNT() : Boolean
    {
        return _internal_INDDISCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get patient_balance() : Number
    {
        return _internal_patient_balance;
    }

    [Bindable(event="propertyChange")]
    public function get patient_paidincash() : Number
    {
        return _internal_patient_paidincash;
    }

    [Bindable(event="propertyChange")]
    public function get patient_balanceNoncash() : Number
    {
        return _internal_patient_balanceNoncash;
    }

    [Bindable(event="propertyChange")]
    public function get patient_paidincashNoncash() : Number
    {
        return _internal_patient_paidincashNoncash;
    }

    [Bindable(event="propertyChange")]
    public function get patient_balanceAll() : Number
    {
        return _internal_patient_balanceAll;
    }

    [Bindable(event="propertyChange")]
    public function get patient_paidincashAll() : Number
    {
        return _internal_patient_paidincashAll;
    }

    [Bindable(event="propertyChange")]
    public function get patient_label() : String
    {
        return _internal_patient_label;
    }

    [Bindable(event="propertyChange")]
    public function get procedures_MinDate() : String
    {
        return _internal_procedures_MinDate;
    }

    [Bindable(event="propertyChange")]
    public function get procedures_MaxDate() : String
    {
        return _internal_procedures_MaxDate;
    }

    [Bindable(event="propertyChange")]
    public function get author_id() : String
    {
        return _internal_author_id;
    }

    [Bindable(event="propertyChange")]
    public function get author_shortname() : String
    {
        return _internal_author_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_id() : String
    {
        return _internal_subdivision_id;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_name() : String
    {
        return _internal_subdivision_name;
    }

    [Bindable(event="propertyChange")]
    public function get discount_cash() : Number
    {
        return _internal_discount_cash;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set is_invoice(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_is_invoice;
        if (oldValue !== value)
        {
            _internal_is_invoice = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_invoice", oldValue, _internal_is_invoice));
        }
    }

    public function set invoice_id(value:String) : void
    {
        var oldValue:String = _internal_invoice_id;
        if (oldValue !== value)
        {
            _internal_invoice_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_id", oldValue, _internal_invoice_id));
        }
    }

    public function set invoice_timestamp(value:String) : void
    {
        var oldValue:String = _internal_invoice_timestamp;
        if (oldValue !== value)
        {
            _internal_invoice_timestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_timestamp", oldValue, _internal_invoice_timestamp));
        }
    }

    public function set invoice_number(value:String) : void
    {
        var oldValue:String = _internal_invoice_number;
        if (oldValue !== value)
        {
            _internal_invoice_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_number", oldValue, _internal_invoice_number));
        }
    }

    public function set invoice_sum(value:Number) : void
    {
        var oldValue:Number = _internal_invoice_sum;
        if (isNaN(_internal_invoice_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_invoice_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_sum", oldValue, _internal_invoice_sum));
        }
    }

    public function set invoice_discountsum(value:Number) : void
    {
        var oldValue:Number = _internal_invoice_discountsum;
        if (isNaN(_internal_invoice_discountsum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_invoice_discountsum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_discountsum", oldValue, _internal_invoice_discountsum));
        }
    }

    public function set invoice_paidincash(value:Number) : void
    {
        var oldValue:Number = _internal_invoice_paidincash;
        if (isNaN(_internal_invoice_paidincash) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_invoice_paidincash = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_paidincash", oldValue, _internal_invoice_paidincash));
        }
    }

    public function set invoice_discount(value:Number) : void
    {
        var oldValue:Number = _internal_invoice_discount;
        if (isNaN(_internal_invoice_discount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_invoice_discount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_discount", oldValue, _internal_invoice_discount));
        }
    }

    public function set invoice_isCashpayment(value:int) : void
    {
        var oldValue:int = _internal_invoice_isCashpayment;
        if (oldValue !== value)
        {
            _internal_invoice_isCashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_isCashpayment", oldValue, _internal_invoice_isCashpayment));
        }
    }

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set patient_insuranceId(value:String) : void
    {
        var oldValue:String = _internal_patient_insuranceId;
        if (oldValue !== value)
        {
            _internal_patient_insuranceId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_insuranceId", oldValue, _internal_patient_insuranceId));
        }
    }

    public function set patient_insuranceNumber(value:String) : void
    {
        var oldValue:String = _internal_patient_insuranceNumber;
        if (oldValue !== value)
        {
            _internal_patient_insuranceNumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_insuranceNumber", oldValue, _internal_patient_insuranceNumber));
        }
    }

    public function set patient_insuranceCompanyName(value:String) : void
    {
        var oldValue:String = _internal_patient_insuranceCompanyName;
        if (oldValue !== value)
        {
            _internal_patient_insuranceCompanyName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_insuranceCompanyName", oldValue, _internal_patient_insuranceCompanyName));
        }
    }

    public function set patient_insuranceCompanyId(value:String) : void
    {
        var oldValue:String = _internal_patient_insuranceCompanyId;
        if (oldValue !== value)
        {
            _internal_patient_insuranceCompanyId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_insuranceCompanyId", oldValue, _internal_patient_insuranceCompanyId));
        }
    }

    public function set patient_fname(value:String) : void
    {
        var oldValue:String = _internal_patient_fname;
        if (oldValue !== value)
        {
            _internal_patient_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_fname", oldValue, _internal_patient_fname));
        }
    }

    public function set patient_lname(value:String) : void
    {
        var oldValue:String = _internal_patient_lname;
        if (oldValue !== value)
        {
            _internal_patient_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_lname", oldValue, _internal_patient_lname));
        }
    }

    public function set patient_sname(value:String) : void
    {
        var oldValue:String = _internal_patient_sname;
        if (oldValue !== value)
        {
            _internal_patient_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_sname", oldValue, _internal_patient_sname));
        }
    }

    public function set patient_cardnumber(value:String) : void
    {
        var oldValue:String = _internal_patient_cardnumber;
        if (oldValue !== value)
        {
            _internal_patient_cardnumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_cardnumber", oldValue, _internal_patient_cardnumber));
        }
    }

    public function set patient_discount(value:Number) : void
    {
        var oldValue:Number = _internal_patient_discount;
        if (isNaN(_internal_patient_discount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_patient_discount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_discount", oldValue, _internal_patient_discount));
        }
    }

    public function set discount_amount_auto(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount_auto;
        if (isNaN(_internal_discount_amount_auto) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount_auto = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount_auto", oldValue, _internal_discount_amount_auto));
        }
    }

    public function set INDDISCOUNT(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_INDDISCOUNT;
        if (oldValue !== value)
        {
            _internal_INDDISCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INDDISCOUNT", oldValue, _internal_INDDISCOUNT));
        }
    }

    public function set patient_balance(value:Number) : void
    {
        var oldValue:Number = _internal_patient_balance;
        if (isNaN(_internal_patient_balance) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_patient_balance = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_balance", oldValue, _internal_patient_balance));
        }
    }

    public function set patient_paidincash(value:Number) : void
    {
        var oldValue:Number = _internal_patient_paidincash;
        if (isNaN(_internal_patient_paidincash) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_patient_paidincash = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_paidincash", oldValue, _internal_patient_paidincash));
        }
    }

    public function set patient_balanceNoncash(value:Number) : void
    {
        var oldValue:Number = _internal_patient_balanceNoncash;
        if (isNaN(_internal_patient_balanceNoncash) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_patient_balanceNoncash = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_balanceNoncash", oldValue, _internal_patient_balanceNoncash));
        }
    }

    public function set patient_paidincashNoncash(value:Number) : void
    {
        var oldValue:Number = _internal_patient_paidincashNoncash;
        if (isNaN(_internal_patient_paidincashNoncash) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_patient_paidincashNoncash = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_paidincashNoncash", oldValue, _internal_patient_paidincashNoncash));
        }
    }

    public function set patient_balanceAll(value:Number) : void
    {
        var oldValue:Number = _internal_patient_balanceAll;
        if (isNaN(_internal_patient_balanceAll) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_patient_balanceAll = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_balanceAll", oldValue, _internal_patient_balanceAll));
        }
    }

    public function set patient_paidincashAll(value:Number) : void
    {
        var oldValue:Number = _internal_patient_paidincashAll;
        if (isNaN(_internal_patient_paidincashAll) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_patient_paidincashAll = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_paidincashAll", oldValue, _internal_patient_paidincashAll));
        }
    }

    public function set patient_label(value:String) : void
    {
        var oldValue:String = _internal_patient_label;
        if (oldValue !== value)
        {
            _internal_patient_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_label", oldValue, _internal_patient_label));
        }
    }

    public function set procedures_MinDate(value:String) : void
    {
        var oldValue:String = _internal_procedures_MinDate;
        if (oldValue !== value)
        {
            _internal_procedures_MinDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedures_MinDate", oldValue, _internal_procedures_MinDate));
        }
    }

    public function set procedures_MaxDate(value:String) : void
    {
        var oldValue:String = _internal_procedures_MaxDate;
        if (oldValue !== value)
        {
            _internal_procedures_MaxDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedures_MaxDate", oldValue, _internal_procedures_MaxDate));
        }
    }

    public function set author_id(value:String) : void
    {
        var oldValue:String = _internal_author_id;
        if (oldValue !== value)
        {
            _internal_author_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_id", oldValue, _internal_author_id));
        }
    }

    public function set author_shortname(value:String) : void
    {
        var oldValue:String = _internal_author_shortname;
        if (oldValue !== value)
        {
            _internal_author_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_shortname", oldValue, _internal_author_shortname));
        }
    }

    public function set subdivision_id(value:String) : void
    {
        var oldValue:String = _internal_subdivision_id;
        if (oldValue !== value)
        {
            _internal_subdivision_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_id", oldValue, _internal_subdivision_id));
        }
    }

    public function set subdivision_name(value:String) : void
    {
        var oldValue:String = _internal_subdivision_name;
        if (oldValue !== value)
        {
            _internal_subdivision_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_name", oldValue, _internal_subdivision_name));
        }
    }

    public function set discount_cash(value:Number) : void
    {
        var oldValue:Number = _internal_discount_cash;
        if (isNaN(_internal_discount_cash) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_cash = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_cash", oldValue, _internal_discount_cash));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AccountModule_FlowObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AccountModule_FlowObjectEntityMetadata) : void
    {
        var oldValue : _AccountModule_FlowObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
