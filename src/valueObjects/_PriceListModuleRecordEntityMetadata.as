
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.Form039Module_F39_Val;
import valueObjects.PriceListModuleRecord;
import valueObjects.PriceListModuleRecordFarm;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _PriceListModuleRecordEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("label", "isOpen", "isBranch", "ID", "PARENTID", "NODETYPE", "SHIFR", "NAME", "PLANNAME", "UOP", "PROCTYPE", "TOOTHUSE", "PRICE", "PRICE2", "PRICE3", "HEALTHTYPE", "PROCTIME", "EXPENSES", "discount_flag", "farms", "CAPTIONLEVEL", "SEQUINCENUMBER", "FARMSCOUNT", "children", "salary_settings", "complete_examdiag", "HEALTHTYPES", "F39", "NAME48", "regCode", "BARCODE");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("label", "isOpen", "isBranch", "ID", "PARENTID", "NODETYPE", "SHIFR", "NAME", "PLANNAME", "UOP", "PROCTYPE", "TOOTHUSE", "PRICE", "PRICE2", "PRICE3", "HEALTHTYPE", "PROCTIME", "EXPENSES", "discount_flag", "farms", "CAPTIONLEVEL", "SEQUINCENUMBER", "FARMSCOUNT", "children", "salary_settings", "complete_examdiag", "HEALTHTYPES", "F39", "NAME48", "regCode", "BARCODE");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("label", "isOpen", "isBranch", "ID", "PARENTID", "NODETYPE", "SHIFR", "NAME", "PLANNAME", "UOP", "PROCTYPE", "TOOTHUSE", "PRICE", "PRICE2", "PRICE3", "HEALTHTYPE", "PROCTIME", "EXPENSES", "discount_flag", "farms", "CAPTIONLEVEL", "SEQUINCENUMBER", "FARMSCOUNT", "children", "salary_settings", "complete_examdiag", "HEALTHTYPES", "F39", "NAME48", "regCode", "BARCODE");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("label", "isOpen", "isBranch", "ID", "PARENTID", "NODETYPE", "SHIFR", "NAME", "PLANNAME", "UOP", "PROCTYPE", "TOOTHUSE", "PRICE", "PRICE2", "PRICE3", "HEALTHTYPE", "PROCTIME", "EXPENSES", "discount_flag", "farms", "CAPTIONLEVEL", "SEQUINCENUMBER", "FARMSCOUNT", "children", "salary_settings", "complete_examdiag", "HEALTHTYPES", "F39", "NAME48", "regCode", "BARCODE");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("farms", "children", "F39");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "PriceListModuleRecord";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_PriceListModuleRecord;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _PriceListModuleRecordEntityMetadata(value : _Super_PriceListModuleRecord)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["label"] = new Array();
            model_internal::dependentsOnMap["isOpen"] = new Array();
            model_internal::dependentsOnMap["isBranch"] = new Array();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["PARENTID"] = new Array();
            model_internal::dependentsOnMap["NODETYPE"] = new Array();
            model_internal::dependentsOnMap["SHIFR"] = new Array();
            model_internal::dependentsOnMap["NAME"] = new Array();
            model_internal::dependentsOnMap["PLANNAME"] = new Array();
            model_internal::dependentsOnMap["UOP"] = new Array();
            model_internal::dependentsOnMap["PROCTYPE"] = new Array();
            model_internal::dependentsOnMap["TOOTHUSE"] = new Array();
            model_internal::dependentsOnMap["PRICE"] = new Array();
            model_internal::dependentsOnMap["PRICE2"] = new Array();
            model_internal::dependentsOnMap["PRICE3"] = new Array();
            model_internal::dependentsOnMap["HEALTHTYPE"] = new Array();
            model_internal::dependentsOnMap["PROCTIME"] = new Array();
            model_internal::dependentsOnMap["EXPENSES"] = new Array();
            model_internal::dependentsOnMap["discount_flag"] = new Array();
            model_internal::dependentsOnMap["farms"] = new Array();
            model_internal::dependentsOnMap["CAPTIONLEVEL"] = new Array();
            model_internal::dependentsOnMap["SEQUINCENUMBER"] = new Array();
            model_internal::dependentsOnMap["FARMSCOUNT"] = new Array();
            model_internal::dependentsOnMap["children"] = new Array();
            model_internal::dependentsOnMap["salary_settings"] = new Array();
            model_internal::dependentsOnMap["complete_examdiag"] = new Array();
            model_internal::dependentsOnMap["HEALTHTYPES"] = new Array();
            model_internal::dependentsOnMap["F39"] = new Array();
            model_internal::dependentsOnMap["NAME48"] = new Array();
            model_internal::dependentsOnMap["regCode"] = new Array();
            model_internal::dependentsOnMap["BARCODE"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["farms"] = "valueObjects.PriceListModuleRecordFarm";
            model_internal::collectionBaseMap["children"] = "valueObjects.PriceListModuleRecord";
            model_internal::collectionBaseMap["F39"] = "valueObjects.Form039Module_F39_Val";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["label"] = "String";
        model_internal::propertyTypeMap["isOpen"] = "Boolean";
        model_internal::propertyTypeMap["isBranch"] = "Boolean";
        model_internal::propertyTypeMap["ID"] = "String";
        model_internal::propertyTypeMap["PARENTID"] = "String";
        model_internal::propertyTypeMap["NODETYPE"] = "String";
        model_internal::propertyTypeMap["SHIFR"] = "String";
        model_internal::propertyTypeMap["NAME"] = "String";
        model_internal::propertyTypeMap["PLANNAME"] = "String";
        model_internal::propertyTypeMap["UOP"] = "String";
        model_internal::propertyTypeMap["PROCTYPE"] = "String";
        model_internal::propertyTypeMap["TOOTHUSE"] = "String";
        model_internal::propertyTypeMap["PRICE"] = "String";
        model_internal::propertyTypeMap["PRICE2"] = "String";
        model_internal::propertyTypeMap["PRICE3"] = "String";
        model_internal::propertyTypeMap["HEALTHTYPE"] = "String";
        model_internal::propertyTypeMap["PROCTIME"] = "String";
        model_internal::propertyTypeMap["EXPENSES"] = "Number";
        model_internal::propertyTypeMap["discount_flag"] = "int";
        model_internal::propertyTypeMap["farms"] = "ArrayCollection";
        model_internal::propertyTypeMap["CAPTIONLEVEL"] = "String";
        model_internal::propertyTypeMap["SEQUINCENUMBER"] = "String";
        model_internal::propertyTypeMap["FARMSCOUNT"] = "int";
        model_internal::propertyTypeMap["children"] = "ArrayCollection";
        model_internal::propertyTypeMap["salary_settings"] = "int";
        model_internal::propertyTypeMap["complete_examdiag"] = "String";
        model_internal::propertyTypeMap["HEALTHTYPES"] = "String";
        model_internal::propertyTypeMap["F39"] = "ArrayCollection";
        model_internal::propertyTypeMap["NAME48"] = "String";
        model_internal::propertyTypeMap["regCode"] = "int";
        model_internal::propertyTypeMap["BARCODE"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity PriceListModuleRecord");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity PriceListModuleRecord");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of PriceListModuleRecord");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PriceListModuleRecord");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity PriceListModuleRecord");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PriceListModuleRecord");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isLabelAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsOpenAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsBranchAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPARENTIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNODETYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSHIFRAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPLANNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isUOPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPROCTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTOOTHUSEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPRICEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPRICE2Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPRICE3Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPROCTIMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEXPENSESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_flagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFarmsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCAPTIONLEVELAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSEQUINCENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMSCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isChildrenAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSalary_settingsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isComplete_examdiagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHTYPESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isF39Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNAME48Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRegCodeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBARCODEAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get labelStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isOpenStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isBranchStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PARENTIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NODETYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SHIFRStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PLANNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get UOPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PROCTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TOOTHUSEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PRICEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PRICE2Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PRICE3Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PROCTIMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EXPENSESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_flagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get farmsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CAPTIONLEVELStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SEQUINCENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMSCOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get childrenStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get salary_settingsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get complete_examdiagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHTYPESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get F39Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NAME48Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get regCodeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BARCODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
