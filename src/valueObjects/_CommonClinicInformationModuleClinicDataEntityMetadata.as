
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _CommonClinicInformationModuleClinicDataEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ADDRESS_INDEX", "ID", "NAME", "LEGALNAME", "ADDRESS", "ORGANIZATIONTYPE", "CODE", "DIRECTOR", "TELEPHONENUMBER", "FAX", "SITENAME", "EMAIL", "ICQ", "SKYPE", "LOGO", "LEGALADDRESS", "BANK_NAME", "BANK_MFO", "BANK_CURRENTACCOUNT", "LICENSE_NUMBER", "LICENSE_SERIES", "LICENSE_ISSUED", "CHIEFACCOUNTANT", "LICENSE_STARTDATE", "LICENSE_ENDDATE", "AUTHORITY", "CITY", "SHORT_NAME", "MSP_TYPE", "OWNER_PROPERTY_TYPE", "LEGAL_FORM", "KVEDS", "ADDRESSLEGAL_ZIP", "ADDRESSLEGAL_TYPE", "ADDRESSLEGAL_COUNTRY", "ADDRESSLEGAL_AREA", "ADDRESSLEGAL_REGION", "ADDRESSLEGAL_SETTLEMENT", "ADDRESSLEGAL_SETTLEMENT_TYPE", "ADDRESSLEGAL_SETTLEMENT_ID", "ADDRESSLEGAL_STREET_TYPE", "ADDRESSLEGAL_STREET", "ADDRESSLEGAL_BUILDING", "ADDRESSLEGAL_APRT", "PHONE_TYPE", "PHONE", "ACR_CATEGORY", "ACR_ISSUED_DATE", "ACR_EXPIRY_DATE", "ACR_ORDER_NO", "ACR_ORDER_DATE", "CONSENT_SIGN", "EH_ID", "EH_STATUS", "EH_CREATED_BY_MIS_CLIENT_ID");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ADDRESS_INDEX", "ID", "NAME", "LEGALNAME", "ADDRESS", "ORGANIZATIONTYPE", "CODE", "DIRECTOR", "TELEPHONENUMBER", "FAX", "SITENAME", "EMAIL", "ICQ", "SKYPE", "LOGO", "LEGALADDRESS", "BANK_NAME", "BANK_MFO", "BANK_CURRENTACCOUNT", "LICENSE_NUMBER", "LICENSE_SERIES", "LICENSE_ISSUED", "CHIEFACCOUNTANT", "LICENSE_STARTDATE", "LICENSE_ENDDATE", "AUTHORITY", "CITY", "SHORT_NAME", "MSP_TYPE", "OWNER_PROPERTY_TYPE", "LEGAL_FORM", "KVEDS", "ADDRESSLEGAL_ZIP", "ADDRESSLEGAL_TYPE", "ADDRESSLEGAL_COUNTRY", "ADDRESSLEGAL_AREA", "ADDRESSLEGAL_REGION", "ADDRESSLEGAL_SETTLEMENT", "ADDRESSLEGAL_SETTLEMENT_TYPE", "ADDRESSLEGAL_SETTLEMENT_ID", "ADDRESSLEGAL_STREET_TYPE", "ADDRESSLEGAL_STREET", "ADDRESSLEGAL_BUILDING", "ADDRESSLEGAL_APRT", "PHONE_TYPE", "PHONE", "ACR_CATEGORY", "ACR_ISSUED_DATE", "ACR_EXPIRY_DATE", "ACR_ORDER_NO", "ACR_ORDER_DATE", "CONSENT_SIGN", "EH_ID", "EH_STATUS", "EH_CREATED_BY_MIS_CLIENT_ID");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ADDRESS_INDEX", "ID", "NAME", "LEGALNAME", "ADDRESS", "ORGANIZATIONTYPE", "CODE", "DIRECTOR", "TELEPHONENUMBER", "FAX", "SITENAME", "EMAIL", "ICQ", "SKYPE", "LOGO", "LEGALADDRESS", "BANK_NAME", "BANK_MFO", "BANK_CURRENTACCOUNT", "LICENSE_NUMBER", "LICENSE_SERIES", "LICENSE_ISSUED", "CHIEFACCOUNTANT", "LICENSE_STARTDATE", "LICENSE_ENDDATE", "AUTHORITY", "CITY", "SHORT_NAME", "MSP_TYPE", "OWNER_PROPERTY_TYPE", "LEGAL_FORM", "KVEDS", "ADDRESSLEGAL_ZIP", "ADDRESSLEGAL_TYPE", "ADDRESSLEGAL_COUNTRY", "ADDRESSLEGAL_AREA", "ADDRESSLEGAL_REGION", "ADDRESSLEGAL_SETTLEMENT", "ADDRESSLEGAL_SETTLEMENT_TYPE", "ADDRESSLEGAL_SETTLEMENT_ID", "ADDRESSLEGAL_STREET_TYPE", "ADDRESSLEGAL_STREET", "ADDRESSLEGAL_BUILDING", "ADDRESSLEGAL_APRT", "PHONE_TYPE", "PHONE", "ACR_CATEGORY", "ACR_ISSUED_DATE", "ACR_EXPIRY_DATE", "ACR_ORDER_NO", "ACR_ORDER_DATE", "CONSENT_SIGN", "EH_ID", "EH_STATUS", "EH_CREATED_BY_MIS_CLIENT_ID");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ADDRESS_INDEX", "ID", "NAME", "LEGALNAME", "ADDRESS", "ORGANIZATIONTYPE", "CODE", "DIRECTOR", "TELEPHONENUMBER", "FAX", "SITENAME", "EMAIL", "ICQ", "SKYPE", "LOGO", "LEGALADDRESS", "BANK_NAME", "BANK_MFO", "BANK_CURRENTACCOUNT", "LICENSE_NUMBER", "LICENSE_SERIES", "LICENSE_ISSUED", "CHIEFACCOUNTANT", "LICENSE_STARTDATE", "LICENSE_ENDDATE", "AUTHORITY", "CITY", "SHORT_NAME", "MSP_TYPE", "OWNER_PROPERTY_TYPE", "LEGAL_FORM", "KVEDS", "ADDRESSLEGAL_ZIP", "ADDRESSLEGAL_TYPE", "ADDRESSLEGAL_COUNTRY", "ADDRESSLEGAL_AREA", "ADDRESSLEGAL_REGION", "ADDRESSLEGAL_SETTLEMENT", "ADDRESSLEGAL_SETTLEMENT_TYPE", "ADDRESSLEGAL_SETTLEMENT_ID", "ADDRESSLEGAL_STREET_TYPE", "ADDRESSLEGAL_STREET", "ADDRESSLEGAL_BUILDING", "ADDRESSLEGAL_APRT", "PHONE_TYPE", "PHONE", "ACR_CATEGORY", "ACR_ISSUED_DATE", "ACR_EXPIRY_DATE", "ACR_ORDER_NO", "ACR_ORDER_DATE", "CONSENT_SIGN", "EH_ID", "EH_STATUS", "EH_CREATED_BY_MIS_CLIENT_ID");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "CommonClinicInformationModuleClinicData";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_CommonClinicInformationModuleClinicData;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _CommonClinicInformationModuleClinicDataEntityMetadata(value : _Super_CommonClinicInformationModuleClinicData)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ADDRESS_INDEX"] = new Array();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["NAME"] = new Array();
            model_internal::dependentsOnMap["LEGALNAME"] = new Array();
            model_internal::dependentsOnMap["ADDRESS"] = new Array();
            model_internal::dependentsOnMap["ORGANIZATIONTYPE"] = new Array();
            model_internal::dependentsOnMap["CODE"] = new Array();
            model_internal::dependentsOnMap["DIRECTOR"] = new Array();
            model_internal::dependentsOnMap["TELEPHONENUMBER"] = new Array();
            model_internal::dependentsOnMap["FAX"] = new Array();
            model_internal::dependentsOnMap["SITENAME"] = new Array();
            model_internal::dependentsOnMap["EMAIL"] = new Array();
            model_internal::dependentsOnMap["ICQ"] = new Array();
            model_internal::dependentsOnMap["SKYPE"] = new Array();
            model_internal::dependentsOnMap["LOGO"] = new Array();
            model_internal::dependentsOnMap["LEGALADDRESS"] = new Array();
            model_internal::dependentsOnMap["BANK_NAME"] = new Array();
            model_internal::dependentsOnMap["BANK_MFO"] = new Array();
            model_internal::dependentsOnMap["BANK_CURRENTACCOUNT"] = new Array();
            model_internal::dependentsOnMap["LICENSE_NUMBER"] = new Array();
            model_internal::dependentsOnMap["LICENSE_SERIES"] = new Array();
            model_internal::dependentsOnMap["LICENSE_ISSUED"] = new Array();
            model_internal::dependentsOnMap["CHIEFACCOUNTANT"] = new Array();
            model_internal::dependentsOnMap["LICENSE_STARTDATE"] = new Array();
            model_internal::dependentsOnMap["LICENSE_ENDDATE"] = new Array();
            model_internal::dependentsOnMap["AUTHORITY"] = new Array();
            model_internal::dependentsOnMap["CITY"] = new Array();
            model_internal::dependentsOnMap["SHORT_NAME"] = new Array();
            model_internal::dependentsOnMap["MSP_TYPE"] = new Array();
            model_internal::dependentsOnMap["OWNER_PROPERTY_TYPE"] = new Array();
            model_internal::dependentsOnMap["LEGAL_FORM"] = new Array();
            model_internal::dependentsOnMap["KVEDS"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_ZIP"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_TYPE"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_COUNTRY"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_AREA"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_REGION"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_SETTLEMENT"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_SETTLEMENT_TYPE"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_SETTLEMENT_ID"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_STREET_TYPE"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_STREET"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_BUILDING"] = new Array();
            model_internal::dependentsOnMap["ADDRESSLEGAL_APRT"] = new Array();
            model_internal::dependentsOnMap["PHONE_TYPE"] = new Array();
            model_internal::dependentsOnMap["PHONE"] = new Array();
            model_internal::dependentsOnMap["ACR_CATEGORY"] = new Array();
            model_internal::dependentsOnMap["ACR_ISSUED_DATE"] = new Array();
            model_internal::dependentsOnMap["ACR_EXPIRY_DATE"] = new Array();
            model_internal::dependentsOnMap["ACR_ORDER_NO"] = new Array();
            model_internal::dependentsOnMap["ACR_ORDER_DATE"] = new Array();
            model_internal::dependentsOnMap["CONSENT_SIGN"] = new Array();
            model_internal::dependentsOnMap["EH_ID"] = new Array();
            model_internal::dependentsOnMap["EH_STATUS"] = new Array();
            model_internal::dependentsOnMap["EH_CREATED_BY_MIS_CLIENT_ID"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ADDRESS_INDEX"] = "String";
        model_internal::propertyTypeMap["ID"] = "String";
        model_internal::propertyTypeMap["NAME"] = "String";
        model_internal::propertyTypeMap["LEGALNAME"] = "String";
        model_internal::propertyTypeMap["ADDRESS"] = "String";
        model_internal::propertyTypeMap["ORGANIZATIONTYPE"] = "String";
        model_internal::propertyTypeMap["CODE"] = "String";
        model_internal::propertyTypeMap["DIRECTOR"] = "String";
        model_internal::propertyTypeMap["TELEPHONENUMBER"] = "String";
        model_internal::propertyTypeMap["FAX"] = "String";
        model_internal::propertyTypeMap["SITENAME"] = "String";
        model_internal::propertyTypeMap["EMAIL"] = "String";
        model_internal::propertyTypeMap["ICQ"] = "String";
        model_internal::propertyTypeMap["SKYPE"] = "String";
        model_internal::propertyTypeMap["LOGO"] = "Object";
        model_internal::propertyTypeMap["LEGALADDRESS"] = "String";
        model_internal::propertyTypeMap["BANK_NAME"] = "String";
        model_internal::propertyTypeMap["BANK_MFO"] = "String";
        model_internal::propertyTypeMap["BANK_CURRENTACCOUNT"] = "String";
        model_internal::propertyTypeMap["LICENSE_NUMBER"] = "String";
        model_internal::propertyTypeMap["LICENSE_SERIES"] = "String";
        model_internal::propertyTypeMap["LICENSE_ISSUED"] = "String";
        model_internal::propertyTypeMap["CHIEFACCOUNTANT"] = "String";
        model_internal::propertyTypeMap["LICENSE_STARTDATE"] = "String";
        model_internal::propertyTypeMap["LICENSE_ENDDATE"] = "String";
        model_internal::propertyTypeMap["AUTHORITY"] = "String";
        model_internal::propertyTypeMap["CITY"] = "String";
        model_internal::propertyTypeMap["SHORT_NAME"] = "String";
        model_internal::propertyTypeMap["MSP_TYPE"] = "String";
        model_internal::propertyTypeMap["OWNER_PROPERTY_TYPE"] = "String";
        model_internal::propertyTypeMap["LEGAL_FORM"] = "String";
        model_internal::propertyTypeMap["KVEDS"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_ZIP"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_TYPE"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_COUNTRY"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_AREA"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_REGION"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_SETTLEMENT"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_SETTLEMENT_TYPE"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_SETTLEMENT_ID"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_STREET_TYPE"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_STREET"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_BUILDING"] = "String";
        model_internal::propertyTypeMap["ADDRESSLEGAL_APRT"] = "String";
        model_internal::propertyTypeMap["PHONE_TYPE"] = "String";
        model_internal::propertyTypeMap["PHONE"] = "String";
        model_internal::propertyTypeMap["ACR_CATEGORY"] = "String";
        model_internal::propertyTypeMap["ACR_ISSUED_DATE"] = "String";
        model_internal::propertyTypeMap["ACR_EXPIRY_DATE"] = "String";
        model_internal::propertyTypeMap["ACR_ORDER_NO"] = "String";
        model_internal::propertyTypeMap["ACR_ORDER_DATE"] = "String";
        model_internal::propertyTypeMap["CONSENT_SIGN"] = "Boolean";
        model_internal::propertyTypeMap["EH_ID"] = "String";
        model_internal::propertyTypeMap["EH_STATUS"] = "String";
        model_internal::propertyTypeMap["EH_CREATED_BY_MIS_CLIENT_ID"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity CommonClinicInformationModuleClinicData");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity CommonClinicInformationModuleClinicData");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of CommonClinicInformationModuleClinicData");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity CommonClinicInformationModuleClinicData");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity CommonClinicInformationModuleClinicData");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity CommonClinicInformationModuleClinicData");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESS_INDEXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLEGALNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isORGANIZATIONTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDIRECTORAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTELEPHONENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFAXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSITENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEMAILAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isICQAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSKYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLOGOAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLEGALADDRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBANK_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBANK_MFOAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBANK_CURRENTACCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLICENSE_NUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLICENSE_SERIESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLICENSE_ISSUEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCHIEFACCOUNTANTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLICENSE_STARTDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLICENSE_ENDDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAUTHORITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSHORT_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMSP_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOWNER_PROPERTY_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLEGAL_FORMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isKVEDSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_ZIPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_COUNTRYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_AREAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_REGIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_SETTLEMENTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_SETTLEMENT_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_SETTLEMENT_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_STREET_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_STREETAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_BUILDINGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSLEGAL_APRTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPHONE_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPHONEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACR_CATEGORYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACR_ISSUED_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACR_EXPIRY_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACR_ORDER_NOAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACR_ORDER_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCONSENT_SIGNAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_STATUSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_CREATED_BY_MIS_CLIENT_IDAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESS_INDEXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LEGALNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ORGANIZATIONTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DIRECTORStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TELEPHONENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FAXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SITENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EMAILStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ICQStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SKYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LOGOStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LEGALADDRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BANK_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BANK_MFOStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BANK_CURRENTACCOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LICENSE_NUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LICENSE_SERIESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LICENSE_ISSUEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CHIEFACCOUNTANTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LICENSE_STARTDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LICENSE_ENDDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AUTHORITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SHORT_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MSP_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get OWNER_PROPERTY_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LEGAL_FORMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get KVEDSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_ZIPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_COUNTRYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_AREAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_REGIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_SETTLEMENTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_SETTLEMENT_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_SETTLEMENT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_STREET_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_STREETStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_BUILDINGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSLEGAL_APRTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PHONE_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PHONEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACR_CATEGORYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACR_ISSUED_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACR_EXPIRY_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACR_ORDER_NOStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACR_ORDER_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CONSENT_SIGNStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_STATUSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_CREATED_BY_MIS_CLIENT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
