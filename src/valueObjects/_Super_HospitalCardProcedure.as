/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HospitalCardProcedure.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HospitalCardProcedure extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HospitalCardProcedure") == null)
            {
                flash.net.registerClassAlias("HospitalCardProcedure", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HospitalCardProcedure", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _HospitalCardProcedureEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_HOSPITAL_CARD_ID : String;
    private var _internal_DATE : String;
    private var _internal_TIME : String;
    private var _internal_CODE : String;
    private var _internal_NAME : String;
    private var _internal_CO_SPECIALIST : String;
    private var _internal_ANAESTHESIA_TYPE : String;
    private var _internal_SEQUELA_MKH_CODE : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HospitalCardProcedure()
    {
        _model = new _HospitalCardProcedureEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get HOSPITAL_CARD_ID() : String
    {
        return _internal_HOSPITAL_CARD_ID;
    }

    [Bindable(event="propertyChange")]
    public function get DATE() : String
    {
        return _internal_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get TIME() : String
    {
        return _internal_TIME;
    }

    [Bindable(event="propertyChange")]
    public function get CODE() : String
    {
        return _internal_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get CO_SPECIALIST() : String
    {
        return _internal_CO_SPECIALIST;
    }

    [Bindable(event="propertyChange")]
    public function get ANAESTHESIA_TYPE() : String
    {
        return _internal_ANAESTHESIA_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get SEQUELA_MKH_CODE() : String
    {
        return _internal_SEQUELA_MKH_CODE;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set HOSPITAL_CARD_ID(value:String) : void
    {
        var oldValue:String = _internal_HOSPITAL_CARD_ID;
        if (oldValue !== value)
        {
            _internal_HOSPITAL_CARD_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HOSPITAL_CARD_ID", oldValue, _internal_HOSPITAL_CARD_ID));
        }
    }

    public function set DATE(value:String) : void
    {
        var oldValue:String = _internal_DATE;
        if (oldValue !== value)
        {
            _internal_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATE", oldValue, _internal_DATE));
        }
    }

    public function set TIME(value:String) : void
    {
        var oldValue:String = _internal_TIME;
        if (oldValue !== value)
        {
            _internal_TIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TIME", oldValue, _internal_TIME));
        }
    }

    public function set CODE(value:String) : void
    {
        var oldValue:String = _internal_CODE;
        if (oldValue !== value)
        {
            _internal_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CODE", oldValue, _internal_CODE));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set CO_SPECIALIST(value:String) : void
    {
        var oldValue:String = _internal_CO_SPECIALIST;
        if (oldValue !== value)
        {
            _internal_CO_SPECIALIST = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CO_SPECIALIST", oldValue, _internal_CO_SPECIALIST));
        }
    }

    public function set ANAESTHESIA_TYPE(value:String) : void
    {
        var oldValue:String = _internal_ANAESTHESIA_TYPE;
        if (oldValue !== value)
        {
            _internal_ANAESTHESIA_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ANAESTHESIA_TYPE", oldValue, _internal_ANAESTHESIA_TYPE));
        }
    }

    public function set SEQUELA_MKH_CODE(value:String) : void
    {
        var oldValue:String = _internal_SEQUELA_MKH_CODE;
        if (oldValue !== value)
        {
            _internal_SEQUELA_MKH_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SEQUELA_MKH_CODE", oldValue, _internal_SEQUELA_MKH_CODE));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HospitalCardProcedureEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HospitalCardProcedureEntityMetadata) : void
    {
        var oldValue : _HospitalCardProcedureEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
