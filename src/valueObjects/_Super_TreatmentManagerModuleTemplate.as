/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - TreatmentManagerModuleTemplate.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.TreatmentManagerModuleDoctor;
import valueObjects.TreatmentManagerModuleTemplateProcedure;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_TreatmentManagerModuleTemplate extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("TreatmentManagerModuleTemplate") == null)
            {
                flash.net.registerClassAlias("TreatmentManagerModuleTemplate", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("TreatmentManagerModuleTemplate", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.TreatmentManagerModuleDoctor.initRemoteClassAliasSingleChild();
        valueObjects.TreatmentManagerModuleTemplateProcedure.initRemoteClassAliasSingleChild();
        valueObjects.TreatmentManagerModuleTemplateProcedurFarm.initRemoteClassAliasSingleChild();
        valueObjects.Form039Module_F39_Val.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _TreatmentManagerModuleTemplateEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_templateID : String;
    private var _internal_templateName : String;
    private var _internal_protocolID : String;
    private var _internal_protocolType : String;
    private var _internal_templateCreateDate : String;
    private var _internal_templateF43 : int;
    private var _internal_templateSum : Number;
    private var _internal_doctor : valueObjects.TreatmentManagerModuleDoctor;
    private var _internal_procedures : ArrayCollection;
    model_internal var _internal_procedures_leaf:valueObjects.TreatmentManagerModuleTemplateProcedure;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_TreatmentManagerModuleTemplate()
    {
        _model = new _TreatmentManagerModuleTemplateEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get templateID() : String
    {
        return _internal_templateID;
    }

    [Bindable(event="propertyChange")]
    public function get templateName() : String
    {
        return _internal_templateName;
    }

    [Bindable(event="propertyChange")]
    public function get protocolID() : String
    {
        return _internal_protocolID;
    }

    [Bindable(event="propertyChange")]
    public function get protocolType() : String
    {
        return _internal_protocolType;
    }

    [Bindable(event="propertyChange")]
    public function get templateCreateDate() : String
    {
        return _internal_templateCreateDate;
    }

    [Bindable(event="propertyChange")]
    public function get templateF43() : int
    {
        return _internal_templateF43;
    }

    [Bindable(event="propertyChange")]
    public function get templateSum() : Number
    {
        return _internal_templateSum;
    }

    [Bindable(event="propertyChange")]
    public function get doctor() : valueObjects.TreatmentManagerModuleDoctor
    {
        return _internal_doctor;
    }

    [Bindable(event="propertyChange")]
    public function get procedures() : ArrayCollection
    {
        return _internal_procedures;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set templateID(value:String) : void
    {
        var oldValue:String = _internal_templateID;
        if (oldValue !== value)
        {
            _internal_templateID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templateID", oldValue, _internal_templateID));
        }
    }

    public function set templateName(value:String) : void
    {
        var oldValue:String = _internal_templateName;
        if (oldValue !== value)
        {
            _internal_templateName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templateName", oldValue, _internal_templateName));
        }
    }

    public function set protocolID(value:String) : void
    {
        var oldValue:String = _internal_protocolID;
        if (oldValue !== value)
        {
            _internal_protocolID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolID", oldValue, _internal_protocolID));
        }
    }

    public function set protocolType(value:String) : void
    {
        var oldValue:String = _internal_protocolType;
        if (oldValue !== value)
        {
            _internal_protocolType = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolType", oldValue, _internal_protocolType));
        }
    }

    public function set templateCreateDate(value:String) : void
    {
        var oldValue:String = _internal_templateCreateDate;
        if (oldValue !== value)
        {
            _internal_templateCreateDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templateCreateDate", oldValue, _internal_templateCreateDate));
        }
    }

    public function set templateF43(value:int) : void
    {
        var oldValue:int = _internal_templateF43;
        if (oldValue !== value)
        {
            _internal_templateF43 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templateF43", oldValue, _internal_templateF43));
        }
    }

    public function set templateSum(value:Number) : void
    {
        var oldValue:Number = _internal_templateSum;
        if (isNaN(_internal_templateSum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_templateSum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templateSum", oldValue, _internal_templateSum));
        }
    }

    public function set doctor(value:valueObjects.TreatmentManagerModuleDoctor) : void
    {
        var oldValue:valueObjects.TreatmentManagerModuleDoctor = _internal_doctor;
        if (oldValue !== value)
        {
            _internal_doctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor", oldValue, _internal_doctor));
        }
    }

    public function set procedures(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_procedures;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_procedures = value;
            }
            else if (value is Array)
            {
                _internal_procedures = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_procedures = null;
            }
            else
            {
                throw new Error("value of procedures must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedures", oldValue, _internal_procedures));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _TreatmentManagerModuleTemplateEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _TreatmentManagerModuleTemplateEntityMetadata) : void
    {
        var oldValue : _TreatmentManagerModuleTemplateEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
