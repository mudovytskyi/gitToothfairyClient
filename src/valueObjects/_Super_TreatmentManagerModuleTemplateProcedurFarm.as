/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - TreatmentManagerModuleTemplateProcedurFarm.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_TreatmentManagerModuleTemplateProcedurFarm extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("TreatmentManagerModuleTemplateProcedurFarm") == null)
            {
                flash.net.registerClassAlias("TreatmentManagerModuleTemplateProcedurFarm", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("TreatmentManagerModuleTemplateProcedurFarm", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _TreatmentManagerModuleTemplateProcedurFarmEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_farmID : String;
    private var _internal_farmGroup : String;
    private var _internal_farmName : String;
    private var _internal_farmQuantity : int;
    private var _internal_farmPrice : Number;
    private var _internal_farmUnitofmeasure : String;
    private var _internal_farmHealthproc : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_TreatmentManagerModuleTemplateProcedurFarm()
    {
        _model = new _TreatmentManagerModuleTemplateProcedurFarmEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get farmID() : String
    {
        return _internal_farmID;
    }

    [Bindable(event="propertyChange")]
    public function get farmGroup() : String
    {
        return _internal_farmGroup;
    }

    [Bindable(event="propertyChange")]
    public function get farmName() : String
    {
        return _internal_farmName;
    }

    [Bindable(event="propertyChange")]
    public function get farmQuantity() : int
    {
        return _internal_farmQuantity;
    }

    [Bindable(event="propertyChange")]
    public function get farmPrice() : Number
    {
        return _internal_farmPrice;
    }

    [Bindable(event="propertyChange")]
    public function get farmUnitofmeasure() : String
    {
        return _internal_farmUnitofmeasure;
    }

    [Bindable(event="propertyChange")]
    public function get farmHealthproc() : String
    {
        return _internal_farmHealthproc;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set farmID(value:String) : void
    {
        var oldValue:String = _internal_farmID;
        if (oldValue !== value)
        {
            _internal_farmID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmID", oldValue, _internal_farmID));
        }
    }

    public function set farmGroup(value:String) : void
    {
        var oldValue:String = _internal_farmGroup;
        if (oldValue !== value)
        {
            _internal_farmGroup = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmGroup", oldValue, _internal_farmGroup));
        }
    }

    public function set farmName(value:String) : void
    {
        var oldValue:String = _internal_farmName;
        if (oldValue !== value)
        {
            _internal_farmName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmName", oldValue, _internal_farmName));
        }
    }

    public function set farmQuantity(value:int) : void
    {
        var oldValue:int = _internal_farmQuantity;
        if (oldValue !== value)
        {
            _internal_farmQuantity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmQuantity", oldValue, _internal_farmQuantity));
        }
    }

    public function set farmPrice(value:Number) : void
    {
        var oldValue:Number = _internal_farmPrice;
        if (isNaN(_internal_farmPrice) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_farmPrice = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmPrice", oldValue, _internal_farmPrice));
        }
    }

    public function set farmUnitofmeasure(value:String) : void
    {
        var oldValue:String = _internal_farmUnitofmeasure;
        if (oldValue !== value)
        {
            _internal_farmUnitofmeasure = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmUnitofmeasure", oldValue, _internal_farmUnitofmeasure));
        }
    }

    public function set farmHealthproc(value:String) : void
    {
        var oldValue:String = _internal_farmHealthproc;
        if (oldValue !== value)
        {
            _internal_farmHealthproc = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmHealthproc", oldValue, _internal_farmHealthproc));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _TreatmentManagerModuleTemplateProcedurFarmEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _TreatmentManagerModuleTemplateProcedurFarmEntityMetadata) : void
    {
        var oldValue : _TreatmentManagerModuleTemplateProcedurFarmEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
