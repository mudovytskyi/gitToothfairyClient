
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.PatientCardModule_mobilephone;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _PatientCardModuleDataGridPatientObjectEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "CARDDATE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "BIRTHDAY", "SEX", "TELEPHONENUMBER", "MOBILENUMBER", "EMAIL", "ADDRESS", "INSURANCEID", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "full_addres", "full_fio", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "group_descript", "group_color", "mobiles", "patientsCount", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "agreementtype");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "CARDDATE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "BIRTHDAY", "SEX", "TELEPHONENUMBER", "MOBILENUMBER", "EMAIL", "ADDRESS", "INSURANCEID", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "full_addres", "full_fio", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "group_descript", "group_color", "mobiles", "patientsCount", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "agreementtype");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "CARDDATE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "BIRTHDAY", "SEX", "TELEPHONENUMBER", "MOBILENUMBER", "EMAIL", "ADDRESS", "INSURANCEID", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "full_addres", "full_fio", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "group_descript", "group_color", "mobiles", "patientsCount", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "agreementtype");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "CARDDATE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "BIRTHDAY", "SEX", "TELEPHONENUMBER", "MOBILENUMBER", "EMAIL", "ADDRESS", "INSURANCEID", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "full_addres", "full_fio", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "group_descript", "group_color", "mobiles", "patientsCount", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "agreementtype");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("mobiles");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "PatientCardModuleDataGridPatientObject";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_PatientCardModuleDataGridPatientObject;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _PatientCardModuleDataGridPatientObjectEntityMetadata(value : _Super_PatientCardModuleDataGridPatientObject)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["CARDNUMBER_FULL"] = new Array();
            model_internal::dependentsOnMap["CARDNUMBER"] = new Array();
            model_internal::dependentsOnMap["CARDBEFORNUMBER"] = new Array();
            model_internal::dependentsOnMap["CARDAFTERNUMBER"] = new Array();
            model_internal::dependentsOnMap["CARDDATE"] = new Array();
            model_internal::dependentsOnMap["FIRSTNAME"] = new Array();
            model_internal::dependentsOnMap["MIDDLENAME"] = new Array();
            model_internal::dependentsOnMap["LASTNAME"] = new Array();
            model_internal::dependentsOnMap["BIRTHDAY"] = new Array();
            model_internal::dependentsOnMap["SEX"] = new Array();
            model_internal::dependentsOnMap["TELEPHONENUMBER"] = new Array();
            model_internal::dependentsOnMap["MOBILENUMBER"] = new Array();
            model_internal::dependentsOnMap["EMAIL"] = new Array();
            model_internal::dependentsOnMap["ADDRESS"] = new Array();
            model_internal::dependentsOnMap["INSURANCEID"] = new Array();
            model_internal::dependentsOnMap["TESTPATOLOGYFLAG"] = new Array();
            model_internal::dependentsOnMap["COUNTS_TOOTHEXAMS"] = new Array();
            model_internal::dependentsOnMap["COUNTS_PROCEDURES_CLOSED"] = new Array();
            model_internal::dependentsOnMap["COUNTS_PROCEDURES_NOTCLOSED"] = new Array();
            model_internal::dependentsOnMap["COUNTS_FILES"] = new Array();
            model_internal::dependentsOnMap["discount_amount"] = new Array();
            model_internal::dependentsOnMap["discount_amount_auto"] = new Array();
            model_internal::dependentsOnMap["INDDISCOUNT"] = new Array();
            model_internal::dependentsOnMap["DISCOUNTCARDNUMBER"] = new Array();
            model_internal::dependentsOnMap["full_addres"] = new Array();
            model_internal::dependentsOnMap["full_fio"] = new Array();
            model_internal::dependentsOnMap["is_returned"] = new Array();
            model_internal::dependentsOnMap["patient_ages"] = new Array();
            model_internal::dependentsOnMap["patient_notes"] = new Array();
            model_internal::dependentsOnMap["doctors"] = new Array();
            model_internal::dependentsOnMap["doctor_lead"] = new Array();
            model_internal::dependentsOnMap["agreementTimestamp"] = new Array();
            model_internal::dependentsOnMap["group_descript"] = new Array();
            model_internal::dependentsOnMap["group_color"] = new Array();
            model_internal::dependentsOnMap["mobiles"] = new Array();
            model_internal::dependentsOnMap["patientsCount"] = new Array();
            model_internal::dependentsOnMap["is_archived"] = new Array();
            model_internal::dependentsOnMap["subdivision_id"] = new Array();
            model_internal::dependentsOnMap["subdivision_label"] = new Array();
            model_internal::dependentsOnMap["NH_TREATTYPE"] = new Array();
            model_internal::dependentsOnMap["agreementtype"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["mobiles"] = "valueObjects.PatientCardModule_mobilephone";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ID"] = "String";
        model_internal::propertyTypeMap["CARDNUMBER_FULL"] = "String";
        model_internal::propertyTypeMap["CARDNUMBER"] = "Number";
        model_internal::propertyTypeMap["CARDBEFORNUMBER"] = "String";
        model_internal::propertyTypeMap["CARDAFTERNUMBER"] = "String";
        model_internal::propertyTypeMap["CARDDATE"] = "String";
        model_internal::propertyTypeMap["FIRSTNAME"] = "String";
        model_internal::propertyTypeMap["MIDDLENAME"] = "String";
        model_internal::propertyTypeMap["LASTNAME"] = "String";
        model_internal::propertyTypeMap["BIRTHDAY"] = "String";
        model_internal::propertyTypeMap["SEX"] = "int";
        model_internal::propertyTypeMap["TELEPHONENUMBER"] = "String";
        model_internal::propertyTypeMap["MOBILENUMBER"] = "String";
        model_internal::propertyTypeMap["EMAIL"] = "String";
        model_internal::propertyTypeMap["ADDRESS"] = "String";
        model_internal::propertyTypeMap["INSURANCEID"] = "int";
        model_internal::propertyTypeMap["TESTPATOLOGYFLAG"] = "int";
        model_internal::propertyTypeMap["COUNTS_TOOTHEXAMS"] = "int";
        model_internal::propertyTypeMap["COUNTS_PROCEDURES_CLOSED"] = "int";
        model_internal::propertyTypeMap["COUNTS_PROCEDURES_NOTCLOSED"] = "int";
        model_internal::propertyTypeMap["COUNTS_FILES"] = "int";
        model_internal::propertyTypeMap["discount_amount"] = "Number";
        model_internal::propertyTypeMap["discount_amount_auto"] = "Number";
        model_internal::propertyTypeMap["INDDISCOUNT"] = "Boolean";
        model_internal::propertyTypeMap["DISCOUNTCARDNUMBER"] = "String";
        model_internal::propertyTypeMap["full_addres"] = "String";
        model_internal::propertyTypeMap["full_fio"] = "String";
        model_internal::propertyTypeMap["is_returned"] = "int";
        model_internal::propertyTypeMap["patient_ages"] = "int";
        model_internal::propertyTypeMap["patient_notes"] = "String";
        model_internal::propertyTypeMap["doctors"] = "String";
        model_internal::propertyTypeMap["doctor_lead"] = "String";
        model_internal::propertyTypeMap["agreementTimestamp"] = "String";
        model_internal::propertyTypeMap["group_descript"] = "String";
        model_internal::propertyTypeMap["group_color"] = "int";
        model_internal::propertyTypeMap["mobiles"] = "ArrayCollection";
        model_internal::propertyTypeMap["patientsCount"] = "int";
        model_internal::propertyTypeMap["is_archived"] = "Boolean";
        model_internal::propertyTypeMap["subdivision_id"] = "String";
        model_internal::propertyTypeMap["subdivision_label"] = "String";
        model_internal::propertyTypeMap["NH_TREATTYPE"] = "int";
        model_internal::propertyTypeMap["agreementtype"] = "int";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity PatientCardModuleDataGridPatientObject");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity PatientCardModuleDataGridPatientObject");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of PatientCardModuleDataGridPatientObject");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardModuleDataGridPatientObject");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity PatientCardModuleDataGridPatientObject");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardModuleDataGridPatientObject");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDNUMBER_FULLAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDBEFORNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDAFTERNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFIRSTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMIDDLENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLASTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBIRTHDAYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSEXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTELEPHONENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOBILENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEMAILAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINSURANCEIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTESTPATOLOGYFLAGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_TOOTHEXAMSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_PROCEDURES_CLOSEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_PROCEDURES_NOTCLOSEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_FILESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_amountAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_amount_autoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINDDISCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDISCOUNTCARDNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFull_addresAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFull_fioAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIs_returnedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_agesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_notesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctorsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctor_leadAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAgreementTimestampAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGroup_descriptAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGroup_colorAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMobilesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientsCountAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIs_archivedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_labelAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNH_TREATTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAgreementtypeAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDNUMBER_FULLStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDBEFORNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDAFTERNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FIRSTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MIDDLENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LASTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BIRTHDAYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SEXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TELEPHONENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOBILENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EMAILStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INSURANCEIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TESTPATOLOGYFLAGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_TOOTHEXAMSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_PROCEDURES_CLOSEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_PROCEDURES_NOTCLOSEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_FILESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_amountStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_amount_autoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INDDISCOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DISCOUNTCARDNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get full_addresStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get full_fioStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get is_returnedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_agesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_notesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctorsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctor_leadStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get agreementTimestampStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get group_descriptStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get group_colorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get mobilesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patientsCountStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get is_archivedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_labelStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NH_TREATTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get agreementtypeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
