/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CalendarModuleTableTask.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.CalendarModuleInsurancePolisObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CalendarModuleTableTask extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CalendarModuleTableTask") == null)
            {
                flash.net.registerClassAlias("CalendarModuleTableTask", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CalendarModuleTableTask", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.CalendarModuleInsurancePolisObject.initRemoteClassAliasSingleChild();
        valueObjects.CalendarModuleInsuranceCompanyObject.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _CalendarModuleTableTaskEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_TASKID : String;
    private var _internal_TASKTASKDATE : String;
    private var _internal_TASKBEGINOFTHEINTERVAL : String;
    private var _internal_TASKENDOFTHEINTERVAL : String;
    private var _internal_TASKPATIENTID : String;
    private var _internal_TASKDOCTORID : String;
    private var _internal_TASKROOMID : String;
    private var _internal_TASKWORKPLACEID : String;
    private var _internal_TASKNOTICED : String;
    private var _internal_TASKFACTOFVISIT : String;
    private var _internal_TASKWORKDESCRIPTION : String;
    private var _internal_TASKCOMMENT : String;
    private var _internal_PATIENTSHORTNAME : String;
    private var _internal_PATIENTFULLNAME : String;
    private var _internal_PATIENTTELEPHONENUMBER : String;
    private var _internal_PATIENTMOBILENUMBER : String;
    private var _internal_DOCTORSHORTNAME : String;
    private var _internal_ROOMNUMBER : String;
    private var _internal_ROOMNAME : String;
    private var _internal_ROOMCOLORID : int;
    private var _internal_ROOMGOOGLECOLORID : int;
    private var _internal_WORKPLACENUMBER : String;
    private var _internal_PRIMARYPATIENT : int;
    private var _internal_FNAME : String;
    private var _internal_LNAME : String;
    private var _internal_MNAME : String;
    private var _internal_CARDNUMBER : String;
    private var _internal_PSHORTNAME : String;
    private var _internal_insurance : valueObjects.CalendarModuleInsurancePolisObject;
    private var _internal_AUTHORID : String;
    private var _internal_AUTHOR_SHORTANME : String;
    private var _internal_TASKTYPE : int;
    private var _internal_SMSSTATUS : int;
    private var _internal_SMS_SEND_STATUS : int;
    private var _internal_xml_item : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CalendarModuleTableTask()
    {
        _model = new _CalendarModuleTableTaskEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get TASKID() : String
    {
        return _internal_TASKID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKTASKDATE() : String
    {
        return _internal_TASKTASKDATE;
    }

    [Bindable(event="propertyChange")]
    public function get TASKBEGINOFTHEINTERVAL() : String
    {
        return _internal_TASKBEGINOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get TASKENDOFTHEINTERVAL() : String
    {
        return _internal_TASKENDOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get TASKPATIENTID() : String
    {
        return _internal_TASKPATIENTID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKDOCTORID() : String
    {
        return _internal_TASKDOCTORID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKROOMID() : String
    {
        return _internal_TASKROOMID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKWORKPLACEID() : String
    {
        return _internal_TASKWORKPLACEID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKNOTICED() : String
    {
        return _internal_TASKNOTICED;
    }

    [Bindable(event="propertyChange")]
    public function get TASKFACTOFVISIT() : String
    {
        return _internal_TASKFACTOFVISIT;
    }

    [Bindable(event="propertyChange")]
    public function get TASKWORKDESCRIPTION() : String
    {
        return _internal_TASKWORKDESCRIPTION;
    }

    [Bindable(event="propertyChange")]
    public function get TASKCOMMENT() : String
    {
        return _internal_TASKCOMMENT;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSHORTNAME() : String
    {
        return _internal_PATIENTSHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTFULLNAME() : String
    {
        return _internal_PATIENTFULLNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTELEPHONENUMBER() : String
    {
        return _internal_PATIENTTELEPHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTMOBILENUMBER() : String
    {
        return _internal_PATIENTMOBILENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTORSHORTNAME() : String
    {
        return _internal_DOCTORSHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMNUMBER() : String
    {
        return _internal_ROOMNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMNAME() : String
    {
        return _internal_ROOMNAME;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMCOLORID() : int
    {
        return _internal_ROOMCOLORID;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMGOOGLECOLORID() : int
    {
        return _internal_ROOMGOOGLECOLORID;
    }

    [Bindable(event="propertyChange")]
    public function get WORKPLACENUMBER() : String
    {
        return _internal_WORKPLACENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PRIMARYPATIENT() : int
    {
        return _internal_PRIMARYPATIENT;
    }

    [Bindable(event="propertyChange")]
    public function get FNAME() : String
    {
        return _internal_FNAME;
    }

    [Bindable(event="propertyChange")]
    public function get LNAME() : String
    {
        return _internal_LNAME;
    }

    [Bindable(event="propertyChange")]
    public function get MNAME() : String
    {
        return _internal_MNAME;
    }

    [Bindable(event="propertyChange")]
    public function get CARDNUMBER() : String
    {
        return _internal_CARDNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PSHORTNAME() : String
    {
        return _internal_PSHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get insurance() : valueObjects.CalendarModuleInsurancePolisObject
    {
        return _internal_insurance;
    }

    [Bindable(event="propertyChange")]
    public function get AUTHORID() : String
    {
        return _internal_AUTHORID;
    }

    [Bindable(event="propertyChange")]
    public function get AUTHOR_SHORTANME() : String
    {
        return _internal_AUTHOR_SHORTANME;
    }

    [Bindable(event="propertyChange")]
    public function get TASKTYPE() : int
    {
        return _internal_TASKTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get SMSSTATUS() : int
    {
        return _internal_SMSSTATUS;
    }

    [Bindable(event="propertyChange")]
    public function get SMS_SEND_STATUS() : int
    {
        return _internal_SMS_SEND_STATUS;
    }

    [Bindable(event="propertyChange")]
    public function get xml_item() : String
    {
        return _internal_xml_item;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set TASKID(value:String) : void
    {
        var oldValue:String = _internal_TASKID;
        if (oldValue !== value)
        {
            _internal_TASKID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKID", oldValue, _internal_TASKID));
        }
    }

    public function set TASKTASKDATE(value:String) : void
    {
        var oldValue:String = _internal_TASKTASKDATE;
        if (oldValue !== value)
        {
            _internal_TASKTASKDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKTASKDATE", oldValue, _internal_TASKTASKDATE));
        }
    }

    public function set TASKBEGINOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TASKBEGINOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TASKBEGINOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKBEGINOFTHEINTERVAL", oldValue, _internal_TASKBEGINOFTHEINTERVAL));
        }
    }

    public function set TASKENDOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TASKENDOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TASKENDOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKENDOFTHEINTERVAL", oldValue, _internal_TASKENDOFTHEINTERVAL));
        }
    }

    public function set TASKPATIENTID(value:String) : void
    {
        var oldValue:String = _internal_TASKPATIENTID;
        if (oldValue !== value)
        {
            _internal_TASKPATIENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKPATIENTID", oldValue, _internal_TASKPATIENTID));
        }
    }

    public function set TASKDOCTORID(value:String) : void
    {
        var oldValue:String = _internal_TASKDOCTORID;
        if (oldValue !== value)
        {
            _internal_TASKDOCTORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKDOCTORID", oldValue, _internal_TASKDOCTORID));
        }
    }

    public function set TASKROOMID(value:String) : void
    {
        var oldValue:String = _internal_TASKROOMID;
        if (oldValue !== value)
        {
            _internal_TASKROOMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKROOMID", oldValue, _internal_TASKROOMID));
        }
    }

    public function set TASKWORKPLACEID(value:String) : void
    {
        var oldValue:String = _internal_TASKWORKPLACEID;
        if (oldValue !== value)
        {
            _internal_TASKWORKPLACEID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKWORKPLACEID", oldValue, _internal_TASKWORKPLACEID));
        }
    }

    public function set TASKNOTICED(value:String) : void
    {
        var oldValue:String = _internal_TASKNOTICED;
        if (oldValue !== value)
        {
            _internal_TASKNOTICED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKNOTICED", oldValue, _internal_TASKNOTICED));
        }
    }

    public function set TASKFACTOFVISIT(value:String) : void
    {
        var oldValue:String = _internal_TASKFACTOFVISIT;
        if (oldValue !== value)
        {
            _internal_TASKFACTOFVISIT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKFACTOFVISIT", oldValue, _internal_TASKFACTOFVISIT));
        }
    }

    public function set TASKWORKDESCRIPTION(value:String) : void
    {
        var oldValue:String = _internal_TASKWORKDESCRIPTION;
        if (oldValue !== value)
        {
            _internal_TASKWORKDESCRIPTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKWORKDESCRIPTION", oldValue, _internal_TASKWORKDESCRIPTION));
        }
    }

    public function set TASKCOMMENT(value:String) : void
    {
        var oldValue:String = _internal_TASKCOMMENT;
        if (oldValue !== value)
        {
            _internal_TASKCOMMENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKCOMMENT", oldValue, _internal_TASKCOMMENT));
        }
    }

    public function set PATIENTSHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSHORTNAME;
        if (oldValue !== value)
        {
            _internal_PATIENTSHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSHORTNAME", oldValue, _internal_PATIENTSHORTNAME));
        }
    }

    public function set PATIENTFULLNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENTFULLNAME;
        if (oldValue !== value)
        {
            _internal_PATIENTFULLNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTFULLNAME", oldValue, _internal_PATIENTFULLNAME));
        }
    }

    public function set PATIENTTELEPHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTELEPHONENUMBER;
        if (oldValue !== value)
        {
            _internal_PATIENTTELEPHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTELEPHONENUMBER", oldValue, _internal_PATIENTTELEPHONENUMBER));
        }
    }

    public function set PATIENTMOBILENUMBER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTMOBILENUMBER;
        if (oldValue !== value)
        {
            _internal_PATIENTMOBILENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTMOBILENUMBER", oldValue, _internal_PATIENTMOBILENUMBER));
        }
    }

    public function set DOCTORSHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_DOCTORSHORTNAME;
        if (oldValue !== value)
        {
            _internal_DOCTORSHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTORSHORTNAME", oldValue, _internal_DOCTORSHORTNAME));
        }
    }

    public function set ROOMNUMBER(value:String) : void
    {
        var oldValue:String = _internal_ROOMNUMBER;
        if (oldValue !== value)
        {
            _internal_ROOMNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMNUMBER", oldValue, _internal_ROOMNUMBER));
        }
    }

    public function set ROOMNAME(value:String) : void
    {
        var oldValue:String = _internal_ROOMNAME;
        if (oldValue !== value)
        {
            _internal_ROOMNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMNAME", oldValue, _internal_ROOMNAME));
        }
    }

    public function set ROOMCOLORID(value:int) : void
    {
        var oldValue:int = _internal_ROOMCOLORID;
        if (oldValue !== value)
        {
            _internal_ROOMCOLORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMCOLORID", oldValue, _internal_ROOMCOLORID));
        }
    }

    public function set ROOMGOOGLECOLORID(value:int) : void
    {
        var oldValue:int = _internal_ROOMGOOGLECOLORID;
        if (oldValue !== value)
        {
            _internal_ROOMGOOGLECOLORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMGOOGLECOLORID", oldValue, _internal_ROOMGOOGLECOLORID));
        }
    }

    public function set WORKPLACENUMBER(value:String) : void
    {
        var oldValue:String = _internal_WORKPLACENUMBER;
        if (oldValue !== value)
        {
            _internal_WORKPLACENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WORKPLACENUMBER", oldValue, _internal_WORKPLACENUMBER));
        }
    }

    public function set PRIMARYPATIENT(value:int) : void
    {
        var oldValue:int = _internal_PRIMARYPATIENT;
        if (oldValue !== value)
        {
            _internal_PRIMARYPATIENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRIMARYPATIENT", oldValue, _internal_PRIMARYPATIENT));
        }
    }

    public function set FNAME(value:String) : void
    {
        var oldValue:String = _internal_FNAME;
        if (oldValue !== value)
        {
            _internal_FNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FNAME", oldValue, _internal_FNAME));
        }
    }

    public function set LNAME(value:String) : void
    {
        var oldValue:String = _internal_LNAME;
        if (oldValue !== value)
        {
            _internal_LNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LNAME", oldValue, _internal_LNAME));
        }
    }

    public function set MNAME(value:String) : void
    {
        var oldValue:String = _internal_MNAME;
        if (oldValue !== value)
        {
            _internal_MNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MNAME", oldValue, _internal_MNAME));
        }
    }

    public function set CARDNUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARDNUMBER;
        if (oldValue !== value)
        {
            _internal_CARDNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDNUMBER", oldValue, _internal_CARDNUMBER));
        }
    }

    public function set PSHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_PSHORTNAME;
        if (oldValue !== value)
        {
            _internal_PSHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PSHORTNAME", oldValue, _internal_PSHORTNAME));
        }
    }

    public function set insurance(value:valueObjects.CalendarModuleInsurancePolisObject) : void
    {
        var oldValue:valueObjects.CalendarModuleInsurancePolisObject = _internal_insurance;
        if (oldValue !== value)
        {
            _internal_insurance = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "insurance", oldValue, _internal_insurance));
        }
    }

    public function set AUTHORID(value:String) : void
    {
        var oldValue:String = _internal_AUTHORID;
        if (oldValue !== value)
        {
            _internal_AUTHORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AUTHORID", oldValue, _internal_AUTHORID));
        }
    }

    public function set AUTHOR_SHORTANME(value:String) : void
    {
        var oldValue:String = _internal_AUTHOR_SHORTANME;
        if (oldValue !== value)
        {
            _internal_AUTHOR_SHORTANME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AUTHOR_SHORTANME", oldValue, _internal_AUTHOR_SHORTANME));
        }
    }

    public function set TASKTYPE(value:int) : void
    {
        var oldValue:int = _internal_TASKTYPE;
        if (oldValue !== value)
        {
            _internal_TASKTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKTYPE", oldValue, _internal_TASKTYPE));
        }
    }

    public function set SMSSTATUS(value:int) : void
    {
        var oldValue:int = _internal_SMSSTATUS;
        if (oldValue !== value)
        {
            _internal_SMSSTATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSSTATUS", oldValue, _internal_SMSSTATUS));
        }
    }

    public function set SMS_SEND_STATUS(value:int) : void
    {
        var oldValue:int = _internal_SMS_SEND_STATUS;
        if (oldValue !== value)
        {
            _internal_SMS_SEND_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMS_SEND_STATUS", oldValue, _internal_SMS_SEND_STATUS));
        }
    }

    public function set xml_item(value:String) : void
    {
        var oldValue:String = _internal_xml_item;
        if (oldValue !== value)
        {
            _internal_xml_item = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "xml_item", oldValue, _internal_xml_item));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CalendarModuleTableTaskEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CalendarModuleTableTaskEntityMetadata) : void
    {
        var oldValue : _CalendarModuleTableTaskEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
