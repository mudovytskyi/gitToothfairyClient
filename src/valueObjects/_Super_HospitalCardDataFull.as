/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HospitalCardDataFull.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.HospitalCardDiagnosis;
import valueObjects.HospitalCardDisabilityCertificate;
import valueObjects.HospitalCardProcedure;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HospitalCardDataFull extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HospitalCardDataFull") == null)
            {
                flash.net.registerClassAlias("HospitalCardDataFull", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HospitalCardDataFull", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HospitalCardProcedure.initRemoteClassAliasSingleChild();
        valueObjects.HospitalCardDiagnosis.initRemoteClassAliasSingleChild();
        valueObjects.HospitalCardDisabilityCertificate.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _HospitalCardDataFullEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_PATIENT_ID : String;
    private var _internal_CARD_NUMBER : String;
    private var _internal_CREATED : String;
    private var _internal_OPENED : String;
    private var _internal_CLOSED : String;
    private var _internal_CLINIC_NAME : String;
    private var _internal_CLINIC_ADDRESS : String;
    private var _internal_CLINIC_REGISTRATION_CODE : String;
    private var _internal_PATIENT_SEX : String;
    private var _internal_PATIENT_FULL_NAME : String;
    private var _internal_PATIENT_BIRTH : String;
    private var _internal_PATIENT_AGE : String;
    private var _internal_PATIENT_DOCUMENT : String;
    private var _internal_PATIENT_DOCUMENT_NUMBER : String;
    private var _internal_PATIENT_CITIZENSHIP : String;
    private var _internal_HABITATION_TYPE : String;
    private var _internal_ADDRESS_PART_1 : String;
    private var _internal_ADDRESS_PART_2 : String;
    private var _internal_POSTAL_CODE : String;
    private var _internal_WORKPLACE_PART_1 : String;
    private var _internal_WORKPLACE_PART_2 : String;
    private var _internal_REFFERENT_CLINIC : String;
    private var _internal_REFFERENT_REGISTRATION_CODE : String;
    private var _internal_INCOME_DIAGNOSIS : String;
    private var _internal_INCOME_DIAGNOSIS_MKH : String;
    private var _internal_DEPARTMENT_CODE_INCOME : String;
    private var _internal_DEPARTMENT_CODE_OUTCOME : String;
    private var _internal_HOSPITALIZATION_TYPE : String;
    private var _internal_LAST_HIV_PROBE_DATE : String;
    private var _internal_BLOOD_TYPE : String;
    private var _internal_BLOOD_RH : String;
    private var _internal_LAST_VASSERMAN_REACTION_DATE : String;
    private var _internal_FARM_ALLERGY_REACTIONS_PART_1 : String;
    private var _internal_FARM_ALLERGY_REACTIONS_PART_2 : String;
    private var _internal_HOSPITALIZATION_REPEATED_IN_YEAR : String;
    private var _internal_HOSPITALIZATION_REPEATED_IN_MONTH : String;
    private var _internal_TREATED_BED_DAYS : String;
    private var _internal_INJURY_TYPE : String;
    private var _internal_ADDITIONAL_OUTCOME_DIAGNOSIS : String;
    private var _internal_RESISTANTION_CATEGORY : String;
    private var _internal_DOCTOR_FULL_NAME_DIAGNOS : String;
    private var _internal_DOCTOR_SIGN_DIAGNOS : String;
    private var _internal_DOCTOR_REGISTRATION_DIAGNOS : String;
    private var _internal_DOCTOR_DATE_DIAGNOS : String;
    private var _internal_OTHER_TREATMENTS : String;
    private var _internal_CANCER_TREATMENT_TYPE : String;
    private var _internal_EMPLOYABILITY : String;
    private var _internal_EXPERTISE_CONCLUSION : String;
    private var _internal_TREATMENT_RESULT : String;
    private var _internal_ONCOLOGY_CHECKUP_DATE : String;
    private var _internal_CHEST_ORGANS_CHECKUP_DATE : String;
    private var _internal_INSURANCY : String;
    private var _internal_DOCTOR_FULL_NAME_OUTCOME : String;
    private var _internal_DOCTOR_SIGN_OUTCOME : String;
    private var _internal_DOCTOR_REGISTRATION_OUTCOME : String;
    private var _internal_HEADDEPARTMENT_FULL_NAME : String;
    private var _internal_HEADDEPARTMENT_SIGN : String;
    private var _internal_HEADDEPARTMENT_REGISTRATION : String;
    private var _internal_hospitalProcedueres : ArrayCollection;
    model_internal var _internal_hospitalProcedueres_leaf:valueObjects.HospitalCardProcedure;
    private var _internal_hospitalDiagnosis : ArrayCollection;
    model_internal var _internal_hospitalDiagnosis_leaf:valueObjects.HospitalCardDiagnosis;
    private var _internal_hospitalDisabilityCerts : ArrayCollection;
    model_internal var _internal_hospitalDisabilityCerts_leaf:valueObjects.HospitalCardDisabilityCertificate;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HospitalCardDataFull()
    {
        _model = new _HospitalCardDataFullEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_ID() : String
    {
        return _internal_PATIENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get CARD_NUMBER() : String
    {
        return _internal_CARD_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CREATED() : String
    {
        return _internal_CREATED;
    }

    [Bindable(event="propertyChange")]
    public function get OPENED() : String
    {
        return _internal_OPENED;
    }

    [Bindable(event="propertyChange")]
    public function get CLOSED() : String
    {
        return _internal_CLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get CLINIC_NAME() : String
    {
        return _internal_CLINIC_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get CLINIC_ADDRESS() : String
    {
        return _internal_CLINIC_ADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get CLINIC_REGISTRATION_CODE() : String
    {
        return _internal_CLINIC_REGISTRATION_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_SEX() : String
    {
        return _internal_PATIENT_SEX;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_FULL_NAME() : String
    {
        return _internal_PATIENT_FULL_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_BIRTH() : String
    {
        return _internal_PATIENT_BIRTH;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_AGE() : String
    {
        return _internal_PATIENT_AGE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_DOCUMENT() : String
    {
        return _internal_PATIENT_DOCUMENT;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_DOCUMENT_NUMBER() : String
    {
        return _internal_PATIENT_DOCUMENT_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_CITIZENSHIP() : String
    {
        return _internal_PATIENT_CITIZENSHIP;
    }

    [Bindable(event="propertyChange")]
    public function get HABITATION_TYPE() : String
    {
        return _internal_HABITATION_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS_PART_1() : String
    {
        return _internal_ADDRESS_PART_1;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS_PART_2() : String
    {
        return _internal_ADDRESS_PART_2;
    }

    [Bindable(event="propertyChange")]
    public function get POSTAL_CODE() : String
    {
        return _internal_POSTAL_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get WORKPLACE_PART_1() : String
    {
        return _internal_WORKPLACE_PART_1;
    }

    [Bindable(event="propertyChange")]
    public function get WORKPLACE_PART_2() : String
    {
        return _internal_WORKPLACE_PART_2;
    }

    [Bindable(event="propertyChange")]
    public function get REFFERENT_CLINIC() : String
    {
        return _internal_REFFERENT_CLINIC;
    }

    [Bindable(event="propertyChange")]
    public function get REFFERENT_REGISTRATION_CODE() : String
    {
        return _internal_REFFERENT_REGISTRATION_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get INCOME_DIAGNOSIS() : String
    {
        return _internal_INCOME_DIAGNOSIS;
    }

    [Bindable(event="propertyChange")]
    public function get INCOME_DIAGNOSIS_MKH() : String
    {
        return _internal_INCOME_DIAGNOSIS_MKH;
    }

    [Bindable(event="propertyChange")]
    public function get DEPARTMENT_CODE_INCOME() : String
    {
        return _internal_DEPARTMENT_CODE_INCOME;
    }

    [Bindable(event="propertyChange")]
    public function get DEPARTMENT_CODE_OUTCOME() : String
    {
        return _internal_DEPARTMENT_CODE_OUTCOME;
    }

    [Bindable(event="propertyChange")]
    public function get HOSPITALIZATION_TYPE() : String
    {
        return _internal_HOSPITALIZATION_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get LAST_HIV_PROBE_DATE() : String
    {
        return _internal_LAST_HIV_PROBE_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get BLOOD_TYPE() : String
    {
        return _internal_BLOOD_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get BLOOD_RH() : String
    {
        return _internal_BLOOD_RH;
    }

    [Bindable(event="propertyChange")]
    public function get LAST_VASSERMAN_REACTION_DATE() : String
    {
        return _internal_LAST_VASSERMAN_REACTION_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_ALLERGY_REACTIONS_PART_1() : String
    {
        return _internal_FARM_ALLERGY_REACTIONS_PART_1;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_ALLERGY_REACTIONS_PART_2() : String
    {
        return _internal_FARM_ALLERGY_REACTIONS_PART_2;
    }

    [Bindable(event="propertyChange")]
    public function get HOSPITALIZATION_REPEATED_IN_YEAR() : String
    {
        return _internal_HOSPITALIZATION_REPEATED_IN_YEAR;
    }

    [Bindable(event="propertyChange")]
    public function get HOSPITALIZATION_REPEATED_IN_MONTH() : String
    {
        return _internal_HOSPITALIZATION_REPEATED_IN_MONTH;
    }

    [Bindable(event="propertyChange")]
    public function get TREATED_BED_DAYS() : String
    {
        return _internal_TREATED_BED_DAYS;
    }

    [Bindable(event="propertyChange")]
    public function get INJURY_TYPE() : String
    {
        return _internal_INJURY_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get ADDITIONAL_OUTCOME_DIAGNOSIS() : String
    {
        return _internal_ADDITIONAL_OUTCOME_DIAGNOSIS;
    }

    [Bindable(event="propertyChange")]
    public function get RESISTANTION_CATEGORY() : String
    {
        return _internal_RESISTANTION_CATEGORY;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_FULL_NAME_DIAGNOS() : String
    {
        return _internal_DOCTOR_FULL_NAME_DIAGNOS;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_SIGN_DIAGNOS() : String
    {
        return _internal_DOCTOR_SIGN_DIAGNOS;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_REGISTRATION_DIAGNOS() : String
    {
        return _internal_DOCTOR_REGISTRATION_DIAGNOS;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_DATE_DIAGNOS() : String
    {
        return _internal_DOCTOR_DATE_DIAGNOS;
    }

    [Bindable(event="propertyChange")]
    public function get OTHER_TREATMENTS() : String
    {
        return _internal_OTHER_TREATMENTS;
    }

    [Bindable(event="propertyChange")]
    public function get CANCER_TREATMENT_TYPE() : String
    {
        return _internal_CANCER_TREATMENT_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get EMPLOYABILITY() : String
    {
        return _internal_EMPLOYABILITY;
    }

    [Bindable(event="propertyChange")]
    public function get EXPERTISE_CONCLUSION() : String
    {
        return _internal_EXPERTISE_CONCLUSION;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENT_RESULT() : String
    {
        return _internal_TREATMENT_RESULT;
    }

    [Bindable(event="propertyChange")]
    public function get ONCOLOGY_CHECKUP_DATE() : String
    {
        return _internal_ONCOLOGY_CHECKUP_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get CHEST_ORGANS_CHECKUP_DATE() : String
    {
        return _internal_CHEST_ORGANS_CHECKUP_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get INSURANCY() : String
    {
        return _internal_INSURANCY;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_FULL_NAME_OUTCOME() : String
    {
        return _internal_DOCTOR_FULL_NAME_OUTCOME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_SIGN_OUTCOME() : String
    {
        return _internal_DOCTOR_SIGN_OUTCOME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_REGISTRATION_OUTCOME() : String
    {
        return _internal_DOCTOR_REGISTRATION_OUTCOME;
    }

    [Bindable(event="propertyChange")]
    public function get HEADDEPARTMENT_FULL_NAME() : String
    {
        return _internal_HEADDEPARTMENT_FULL_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get HEADDEPARTMENT_SIGN() : String
    {
        return _internal_HEADDEPARTMENT_SIGN;
    }

    [Bindable(event="propertyChange")]
    public function get HEADDEPARTMENT_REGISTRATION() : String
    {
        return _internal_HEADDEPARTMENT_REGISTRATION;
    }

    [Bindable(event="propertyChange")]
    public function get hospitalProcedueres() : ArrayCollection
    {
        return _internal_hospitalProcedueres;
    }

    [Bindable(event="propertyChange")]
    public function get hospitalDiagnosis() : ArrayCollection
    {
        return _internal_hospitalDiagnosis;
    }

    [Bindable(event="propertyChange")]
    public function get hospitalDisabilityCerts() : ArrayCollection
    {
        return _internal_hospitalDisabilityCerts;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set PATIENT_ID(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_ID;
        if (oldValue !== value)
        {
            _internal_PATIENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_ID", oldValue, _internal_PATIENT_ID));
        }
    }

    public function set CARD_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARD_NUMBER;
        if (oldValue !== value)
        {
            _internal_CARD_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARD_NUMBER", oldValue, _internal_CARD_NUMBER));
        }
    }

    public function set CREATED(value:String) : void
    {
        var oldValue:String = _internal_CREATED;
        if (oldValue !== value)
        {
            _internal_CREATED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CREATED", oldValue, _internal_CREATED));
        }
    }

    public function set OPENED(value:String) : void
    {
        var oldValue:String = _internal_OPENED;
        if (oldValue !== value)
        {
            _internal_OPENED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OPENED", oldValue, _internal_OPENED));
        }
    }

    public function set CLOSED(value:String) : void
    {
        var oldValue:String = _internal_CLOSED;
        if (oldValue !== value)
        {
            _internal_CLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLOSED", oldValue, _internal_CLOSED));
        }
    }

    public function set CLINIC_NAME(value:String) : void
    {
        var oldValue:String = _internal_CLINIC_NAME;
        if (oldValue !== value)
        {
            _internal_CLINIC_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLINIC_NAME", oldValue, _internal_CLINIC_NAME));
        }
    }

    public function set CLINIC_ADDRESS(value:String) : void
    {
        var oldValue:String = _internal_CLINIC_ADDRESS;
        if (oldValue !== value)
        {
            _internal_CLINIC_ADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLINIC_ADDRESS", oldValue, _internal_CLINIC_ADDRESS));
        }
    }

    public function set CLINIC_REGISTRATION_CODE(value:String) : void
    {
        var oldValue:String = _internal_CLINIC_REGISTRATION_CODE;
        if (oldValue !== value)
        {
            _internal_CLINIC_REGISTRATION_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLINIC_REGISTRATION_CODE", oldValue, _internal_CLINIC_REGISTRATION_CODE));
        }
    }

    public function set PATIENT_SEX(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_SEX;
        if (oldValue !== value)
        {
            _internal_PATIENT_SEX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_SEX", oldValue, _internal_PATIENT_SEX));
        }
    }

    public function set PATIENT_FULL_NAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_FULL_NAME;
        if (oldValue !== value)
        {
            _internal_PATIENT_FULL_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_FULL_NAME", oldValue, _internal_PATIENT_FULL_NAME));
        }
    }

    public function set PATIENT_BIRTH(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_BIRTH;
        if (oldValue !== value)
        {
            _internal_PATIENT_BIRTH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_BIRTH", oldValue, _internal_PATIENT_BIRTH));
        }
    }

    public function set PATIENT_AGE(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_AGE;
        if (oldValue !== value)
        {
            _internal_PATIENT_AGE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_AGE", oldValue, _internal_PATIENT_AGE));
        }
    }

    public function set PATIENT_DOCUMENT(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_DOCUMENT;
        if (oldValue !== value)
        {
            _internal_PATIENT_DOCUMENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_DOCUMENT", oldValue, _internal_PATIENT_DOCUMENT));
        }
    }

    public function set PATIENT_DOCUMENT_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_DOCUMENT_NUMBER;
        if (oldValue !== value)
        {
            _internal_PATIENT_DOCUMENT_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_DOCUMENT_NUMBER", oldValue, _internal_PATIENT_DOCUMENT_NUMBER));
        }
    }

    public function set PATIENT_CITIZENSHIP(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_CITIZENSHIP;
        if (oldValue !== value)
        {
            _internal_PATIENT_CITIZENSHIP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_CITIZENSHIP", oldValue, _internal_PATIENT_CITIZENSHIP));
        }
    }

    public function set HABITATION_TYPE(value:String) : void
    {
        var oldValue:String = _internal_HABITATION_TYPE;
        if (oldValue !== value)
        {
            _internal_HABITATION_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HABITATION_TYPE", oldValue, _internal_HABITATION_TYPE));
        }
    }

    public function set ADDRESS_PART_1(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS_PART_1;
        if (oldValue !== value)
        {
            _internal_ADDRESS_PART_1 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS_PART_1", oldValue, _internal_ADDRESS_PART_1));
        }
    }

    public function set ADDRESS_PART_2(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS_PART_2;
        if (oldValue !== value)
        {
            _internal_ADDRESS_PART_2 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS_PART_2", oldValue, _internal_ADDRESS_PART_2));
        }
    }

    public function set POSTAL_CODE(value:String) : void
    {
        var oldValue:String = _internal_POSTAL_CODE;
        if (oldValue !== value)
        {
            _internal_POSTAL_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "POSTAL_CODE", oldValue, _internal_POSTAL_CODE));
        }
    }

    public function set WORKPLACE_PART_1(value:String) : void
    {
        var oldValue:String = _internal_WORKPLACE_PART_1;
        if (oldValue !== value)
        {
            _internal_WORKPLACE_PART_1 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WORKPLACE_PART_1", oldValue, _internal_WORKPLACE_PART_1));
        }
    }

    public function set WORKPLACE_PART_2(value:String) : void
    {
        var oldValue:String = _internal_WORKPLACE_PART_2;
        if (oldValue !== value)
        {
            _internal_WORKPLACE_PART_2 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WORKPLACE_PART_2", oldValue, _internal_WORKPLACE_PART_2));
        }
    }

    public function set REFFERENT_CLINIC(value:String) : void
    {
        var oldValue:String = _internal_REFFERENT_CLINIC;
        if (oldValue !== value)
        {
            _internal_REFFERENT_CLINIC = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REFFERENT_CLINIC", oldValue, _internal_REFFERENT_CLINIC));
        }
    }

    public function set REFFERENT_REGISTRATION_CODE(value:String) : void
    {
        var oldValue:String = _internal_REFFERENT_REGISTRATION_CODE;
        if (oldValue !== value)
        {
            _internal_REFFERENT_REGISTRATION_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REFFERENT_REGISTRATION_CODE", oldValue, _internal_REFFERENT_REGISTRATION_CODE));
        }
    }

    public function set INCOME_DIAGNOSIS(value:String) : void
    {
        var oldValue:String = _internal_INCOME_DIAGNOSIS;
        if (oldValue !== value)
        {
            _internal_INCOME_DIAGNOSIS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INCOME_DIAGNOSIS", oldValue, _internal_INCOME_DIAGNOSIS));
        }
    }

    public function set INCOME_DIAGNOSIS_MKH(value:String) : void
    {
        var oldValue:String = _internal_INCOME_DIAGNOSIS_MKH;
        if (oldValue !== value)
        {
            _internal_INCOME_DIAGNOSIS_MKH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INCOME_DIAGNOSIS_MKH", oldValue, _internal_INCOME_DIAGNOSIS_MKH));
        }
    }

    public function set DEPARTMENT_CODE_INCOME(value:String) : void
    {
        var oldValue:String = _internal_DEPARTMENT_CODE_INCOME;
        if (oldValue !== value)
        {
            _internal_DEPARTMENT_CODE_INCOME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DEPARTMENT_CODE_INCOME", oldValue, _internal_DEPARTMENT_CODE_INCOME));
        }
    }

    public function set DEPARTMENT_CODE_OUTCOME(value:String) : void
    {
        var oldValue:String = _internal_DEPARTMENT_CODE_OUTCOME;
        if (oldValue !== value)
        {
            _internal_DEPARTMENT_CODE_OUTCOME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DEPARTMENT_CODE_OUTCOME", oldValue, _internal_DEPARTMENT_CODE_OUTCOME));
        }
    }

    public function set HOSPITALIZATION_TYPE(value:String) : void
    {
        var oldValue:String = _internal_HOSPITALIZATION_TYPE;
        if (oldValue !== value)
        {
            _internal_HOSPITALIZATION_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HOSPITALIZATION_TYPE", oldValue, _internal_HOSPITALIZATION_TYPE));
        }
    }

    public function set LAST_HIV_PROBE_DATE(value:String) : void
    {
        var oldValue:String = _internal_LAST_HIV_PROBE_DATE;
        if (oldValue !== value)
        {
            _internal_LAST_HIV_PROBE_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LAST_HIV_PROBE_DATE", oldValue, _internal_LAST_HIV_PROBE_DATE));
        }
    }

    public function set BLOOD_TYPE(value:String) : void
    {
        var oldValue:String = _internal_BLOOD_TYPE;
        if (oldValue !== value)
        {
            _internal_BLOOD_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BLOOD_TYPE", oldValue, _internal_BLOOD_TYPE));
        }
    }

    public function set BLOOD_RH(value:String) : void
    {
        var oldValue:String = _internal_BLOOD_RH;
        if (oldValue !== value)
        {
            _internal_BLOOD_RH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BLOOD_RH", oldValue, _internal_BLOOD_RH));
        }
    }

    public function set LAST_VASSERMAN_REACTION_DATE(value:String) : void
    {
        var oldValue:String = _internal_LAST_VASSERMAN_REACTION_DATE;
        if (oldValue !== value)
        {
            _internal_LAST_VASSERMAN_REACTION_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LAST_VASSERMAN_REACTION_DATE", oldValue, _internal_LAST_VASSERMAN_REACTION_DATE));
        }
    }

    public function set FARM_ALLERGY_REACTIONS_PART_1(value:String) : void
    {
        var oldValue:String = _internal_FARM_ALLERGY_REACTIONS_PART_1;
        if (oldValue !== value)
        {
            _internal_FARM_ALLERGY_REACTIONS_PART_1 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_ALLERGY_REACTIONS_PART_1", oldValue, _internal_FARM_ALLERGY_REACTIONS_PART_1));
        }
    }

    public function set FARM_ALLERGY_REACTIONS_PART_2(value:String) : void
    {
        var oldValue:String = _internal_FARM_ALLERGY_REACTIONS_PART_2;
        if (oldValue !== value)
        {
            _internal_FARM_ALLERGY_REACTIONS_PART_2 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_ALLERGY_REACTIONS_PART_2", oldValue, _internal_FARM_ALLERGY_REACTIONS_PART_2));
        }
    }

    public function set HOSPITALIZATION_REPEATED_IN_YEAR(value:String) : void
    {
        var oldValue:String = _internal_HOSPITALIZATION_REPEATED_IN_YEAR;
        if (oldValue !== value)
        {
            _internal_HOSPITALIZATION_REPEATED_IN_YEAR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HOSPITALIZATION_REPEATED_IN_YEAR", oldValue, _internal_HOSPITALIZATION_REPEATED_IN_YEAR));
        }
    }

    public function set HOSPITALIZATION_REPEATED_IN_MONTH(value:String) : void
    {
        var oldValue:String = _internal_HOSPITALIZATION_REPEATED_IN_MONTH;
        if (oldValue !== value)
        {
            _internal_HOSPITALIZATION_REPEATED_IN_MONTH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HOSPITALIZATION_REPEATED_IN_MONTH", oldValue, _internal_HOSPITALIZATION_REPEATED_IN_MONTH));
        }
    }

    public function set TREATED_BED_DAYS(value:String) : void
    {
        var oldValue:String = _internal_TREATED_BED_DAYS;
        if (oldValue !== value)
        {
            _internal_TREATED_BED_DAYS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATED_BED_DAYS", oldValue, _internal_TREATED_BED_DAYS));
        }
    }

    public function set INJURY_TYPE(value:String) : void
    {
        var oldValue:String = _internal_INJURY_TYPE;
        if (oldValue !== value)
        {
            _internal_INJURY_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INJURY_TYPE", oldValue, _internal_INJURY_TYPE));
        }
    }

    public function set ADDITIONAL_OUTCOME_DIAGNOSIS(value:String) : void
    {
        var oldValue:String = _internal_ADDITIONAL_OUTCOME_DIAGNOSIS;
        if (oldValue !== value)
        {
            _internal_ADDITIONAL_OUTCOME_DIAGNOSIS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDITIONAL_OUTCOME_DIAGNOSIS", oldValue, _internal_ADDITIONAL_OUTCOME_DIAGNOSIS));
        }
    }

    public function set RESISTANTION_CATEGORY(value:String) : void
    {
        var oldValue:String = _internal_RESISTANTION_CATEGORY;
        if (oldValue !== value)
        {
            _internal_RESISTANTION_CATEGORY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RESISTANTION_CATEGORY", oldValue, _internal_RESISTANTION_CATEGORY));
        }
    }

    public function set DOCTOR_FULL_NAME_DIAGNOS(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_FULL_NAME_DIAGNOS;
        if (oldValue !== value)
        {
            _internal_DOCTOR_FULL_NAME_DIAGNOS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_FULL_NAME_DIAGNOS", oldValue, _internal_DOCTOR_FULL_NAME_DIAGNOS));
        }
    }

    public function set DOCTOR_SIGN_DIAGNOS(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_SIGN_DIAGNOS;
        if (oldValue !== value)
        {
            _internal_DOCTOR_SIGN_DIAGNOS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_SIGN_DIAGNOS", oldValue, _internal_DOCTOR_SIGN_DIAGNOS));
        }
    }

    public function set DOCTOR_REGISTRATION_DIAGNOS(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_REGISTRATION_DIAGNOS;
        if (oldValue !== value)
        {
            _internal_DOCTOR_REGISTRATION_DIAGNOS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_REGISTRATION_DIAGNOS", oldValue, _internal_DOCTOR_REGISTRATION_DIAGNOS));
        }
    }

    public function set DOCTOR_DATE_DIAGNOS(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_DATE_DIAGNOS;
        if (oldValue !== value)
        {
            _internal_DOCTOR_DATE_DIAGNOS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_DATE_DIAGNOS", oldValue, _internal_DOCTOR_DATE_DIAGNOS));
        }
    }

    public function set OTHER_TREATMENTS(value:String) : void
    {
        var oldValue:String = _internal_OTHER_TREATMENTS;
        if (oldValue !== value)
        {
            _internal_OTHER_TREATMENTS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OTHER_TREATMENTS", oldValue, _internal_OTHER_TREATMENTS));
        }
    }

    public function set CANCER_TREATMENT_TYPE(value:String) : void
    {
        var oldValue:String = _internal_CANCER_TREATMENT_TYPE;
        if (oldValue !== value)
        {
            _internal_CANCER_TREATMENT_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CANCER_TREATMENT_TYPE", oldValue, _internal_CANCER_TREATMENT_TYPE));
        }
    }

    public function set EMPLOYABILITY(value:String) : void
    {
        var oldValue:String = _internal_EMPLOYABILITY;
        if (oldValue !== value)
        {
            _internal_EMPLOYABILITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMPLOYABILITY", oldValue, _internal_EMPLOYABILITY));
        }
    }

    public function set EXPERTISE_CONCLUSION(value:String) : void
    {
        var oldValue:String = _internal_EXPERTISE_CONCLUSION;
        if (oldValue !== value)
        {
            _internal_EXPERTISE_CONCLUSION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXPERTISE_CONCLUSION", oldValue, _internal_EXPERTISE_CONCLUSION));
        }
    }

    public function set TREATMENT_RESULT(value:String) : void
    {
        var oldValue:String = _internal_TREATMENT_RESULT;
        if (oldValue !== value)
        {
            _internal_TREATMENT_RESULT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENT_RESULT", oldValue, _internal_TREATMENT_RESULT));
        }
    }

    public function set ONCOLOGY_CHECKUP_DATE(value:String) : void
    {
        var oldValue:String = _internal_ONCOLOGY_CHECKUP_DATE;
        if (oldValue !== value)
        {
            _internal_ONCOLOGY_CHECKUP_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ONCOLOGY_CHECKUP_DATE", oldValue, _internal_ONCOLOGY_CHECKUP_DATE));
        }
    }

    public function set CHEST_ORGANS_CHECKUP_DATE(value:String) : void
    {
        var oldValue:String = _internal_CHEST_ORGANS_CHECKUP_DATE;
        if (oldValue !== value)
        {
            _internal_CHEST_ORGANS_CHECKUP_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CHEST_ORGANS_CHECKUP_DATE", oldValue, _internal_CHEST_ORGANS_CHECKUP_DATE));
        }
    }

    public function set INSURANCY(value:String) : void
    {
        var oldValue:String = _internal_INSURANCY;
        if (oldValue !== value)
        {
            _internal_INSURANCY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INSURANCY", oldValue, _internal_INSURANCY));
        }
    }

    public function set DOCTOR_FULL_NAME_OUTCOME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_FULL_NAME_OUTCOME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_FULL_NAME_OUTCOME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_FULL_NAME_OUTCOME", oldValue, _internal_DOCTOR_FULL_NAME_OUTCOME));
        }
    }

    public function set DOCTOR_SIGN_OUTCOME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_SIGN_OUTCOME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_SIGN_OUTCOME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_SIGN_OUTCOME", oldValue, _internal_DOCTOR_SIGN_OUTCOME));
        }
    }

    public function set DOCTOR_REGISTRATION_OUTCOME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_REGISTRATION_OUTCOME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_REGISTRATION_OUTCOME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_REGISTRATION_OUTCOME", oldValue, _internal_DOCTOR_REGISTRATION_OUTCOME));
        }
    }

    public function set HEADDEPARTMENT_FULL_NAME(value:String) : void
    {
        var oldValue:String = _internal_HEADDEPARTMENT_FULL_NAME;
        if (oldValue !== value)
        {
            _internal_HEADDEPARTMENT_FULL_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEADDEPARTMENT_FULL_NAME", oldValue, _internal_HEADDEPARTMENT_FULL_NAME));
        }
    }

    public function set HEADDEPARTMENT_SIGN(value:String) : void
    {
        var oldValue:String = _internal_HEADDEPARTMENT_SIGN;
        if (oldValue !== value)
        {
            _internal_HEADDEPARTMENT_SIGN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEADDEPARTMENT_SIGN", oldValue, _internal_HEADDEPARTMENT_SIGN));
        }
    }

    public function set HEADDEPARTMENT_REGISTRATION(value:String) : void
    {
        var oldValue:String = _internal_HEADDEPARTMENT_REGISTRATION;
        if (oldValue !== value)
        {
            _internal_HEADDEPARTMENT_REGISTRATION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEADDEPARTMENT_REGISTRATION", oldValue, _internal_HEADDEPARTMENT_REGISTRATION));
        }
    }

    public function set hospitalProcedueres(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_hospitalProcedueres;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_hospitalProcedueres = value;
            }
            else if (value is Array)
            {
                _internal_hospitalProcedueres = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_hospitalProcedueres = null;
            }
            else
            {
                throw new Error("value of hospitalProcedueres must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "hospitalProcedueres", oldValue, _internal_hospitalProcedueres));
        }
    }

    public function set hospitalDiagnosis(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_hospitalDiagnosis;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_hospitalDiagnosis = value;
            }
            else if (value is Array)
            {
                _internal_hospitalDiagnosis = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_hospitalDiagnosis = null;
            }
            else
            {
                throw new Error("value of hospitalDiagnosis must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "hospitalDiagnosis", oldValue, _internal_hospitalDiagnosis));
        }
    }

    public function set hospitalDisabilityCerts(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_hospitalDisabilityCerts;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_hospitalDisabilityCerts = value;
            }
            else if (value is Array)
            {
                _internal_hospitalDisabilityCerts = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_hospitalDisabilityCerts = null;
            }
            else
            {
                throw new Error("value of hospitalDisabilityCerts must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "hospitalDisabilityCerts", oldValue, _internal_hospitalDisabilityCerts));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HospitalCardDataFullEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HospitalCardDataFullEntityMetadata) : void
    {
        var oldValue : _HospitalCardDataFullEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
