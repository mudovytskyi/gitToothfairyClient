/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - EHealthModule_Declaration.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_EHealthModule_Declaration extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("EHealthModule_Declaration") == null)
            {
                flash.net.registerClassAlias("EHealthModule_Declaration", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("EHealthModule_Declaration", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _EHealthModule_DeclarationEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_json : String;
    private var _internal_id : String;
    private var _internal_ptn_id : String;
    private var _internal_ptn_fname : String;
    private var _internal_ptn_lname : String;
    private var _internal_ptn_mname : String;
    private var _internal_ptn_sex : int;
    private var _internal_ptn_birthday : String;
    private var _internal_ptn_email : String;
    private var _internal_ptn_taxid : String;
    private var _internal_dec_id : String;
    private var _internal_FK_SUBDIVISION : String;
    private var _internal_FK_ACCOUNT : String;
    private var _internal_ptn_birth_country : String;
    private var _internal_ptn_birth_settlement : String;
    private var _internal_ptn_secret : String;
    private var _internal_ptn_doc_type : String;
    private var _internal_ptn_doc_num : String;
    private var _internal_emg_first_name : String;
    private var _internal_emg_last_name : String;
    private var _internal_emg_second_name : String;
    private var _internal_cnf_enable : Boolean;
    private var _internal_cnf_rel_type : String;
    private var _internal_cnf_first_name : String;
    private var _internal_cnf_last_name : String;
    private var _internal_cnf_second_name : String;
    private var _internal_cnf_birthday : String;
    private var _internal_cnf_birth_country : String;
    private var _internal_cnf_birth_settlement : String;
    private var _internal_cnf_sex : int;
    private var _internal_cnf_taxid : String;
    private var _internal_cnf_secret : String;
    private var _internal_cnf_doc_type : String;
    private var _internal_cnf_doc_num : String;
    private var _internal_cnf_doc_rel_type : String;
    private var _internal_cnf_doc_rel_num : String;
    private var _internal_PATIENT_SIGNED : Boolean;
    private var _internal_PATIENT_CONSENT : Boolean;
    private var _internal_EH_DEC_ID : String;
    private var _internal_AUTH_TYPE : String;
    private var _internal_AUTH_PHONENUMBER : String;
    private var _internal_STATUS : String;
    private var _internal_account_eh_id : String;
    private var _internal_subdivision_eh_id : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_EHealthModule_Declaration()
    {
        _model = new _EHealthModule_DeclarationEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get json() : String
    {
        return _internal_json;
    }

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_id() : String
    {
        return _internal_ptn_id;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_fname() : String
    {
        return _internal_ptn_fname;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_lname() : String
    {
        return _internal_ptn_lname;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_mname() : String
    {
        return _internal_ptn_mname;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_sex() : int
    {
        return _internal_ptn_sex;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_birthday() : String
    {
        return _internal_ptn_birthday;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_email() : String
    {
        return _internal_ptn_email;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_taxid() : String
    {
        return _internal_ptn_taxid;
    }

    [Bindable(event="propertyChange")]
    public function get dec_id() : String
    {
        return _internal_dec_id;
    }

    [Bindable(event="propertyChange")]
    public function get FK_SUBDIVISION() : String
    {
        return _internal_FK_SUBDIVISION;
    }

    [Bindable(event="propertyChange")]
    public function get FK_ACCOUNT() : String
    {
        return _internal_FK_ACCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_birth_country() : String
    {
        return _internal_ptn_birth_country;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_birth_settlement() : String
    {
        return _internal_ptn_birth_settlement;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_secret() : String
    {
        return _internal_ptn_secret;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_doc_type() : String
    {
        return _internal_ptn_doc_type;
    }

    [Bindable(event="propertyChange")]
    public function get ptn_doc_num() : String
    {
        return _internal_ptn_doc_num;
    }

    [Bindable(event="propertyChange")]
    public function get emg_first_name() : String
    {
        return _internal_emg_first_name;
    }

    [Bindable(event="propertyChange")]
    public function get emg_last_name() : String
    {
        return _internal_emg_last_name;
    }

    [Bindable(event="propertyChange")]
    public function get emg_second_name() : String
    {
        return _internal_emg_second_name;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_enable() : Boolean
    {
        return _internal_cnf_enable;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_rel_type() : String
    {
        return _internal_cnf_rel_type;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_first_name() : String
    {
        return _internal_cnf_first_name;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_last_name() : String
    {
        return _internal_cnf_last_name;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_second_name() : String
    {
        return _internal_cnf_second_name;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_birthday() : String
    {
        return _internal_cnf_birthday;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_birth_country() : String
    {
        return _internal_cnf_birth_country;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_birth_settlement() : String
    {
        return _internal_cnf_birth_settlement;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_sex() : int
    {
        return _internal_cnf_sex;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_taxid() : String
    {
        return _internal_cnf_taxid;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_secret() : String
    {
        return _internal_cnf_secret;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_doc_type() : String
    {
        return _internal_cnf_doc_type;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_doc_num() : String
    {
        return _internal_cnf_doc_num;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_doc_rel_type() : String
    {
        return _internal_cnf_doc_rel_type;
    }

    [Bindable(event="propertyChange")]
    public function get cnf_doc_rel_num() : String
    {
        return _internal_cnf_doc_rel_num;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_SIGNED() : Boolean
    {
        return _internal_PATIENT_SIGNED;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_CONSENT() : Boolean
    {
        return _internal_PATIENT_CONSENT;
    }

    [Bindable(event="propertyChange")]
    public function get EH_DEC_ID() : String
    {
        return _internal_EH_DEC_ID;
    }

    [Bindable(event="propertyChange")]
    public function get AUTH_TYPE() : String
    {
        return _internal_AUTH_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get AUTH_PHONENUMBER() : String
    {
        return _internal_AUTH_PHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS() : String
    {
        return _internal_STATUS;
    }

    [Bindable(event="propertyChange")]
    public function get account_eh_id() : String
    {
        return _internal_account_eh_id;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_eh_id() : String
    {
        return _internal_subdivision_eh_id;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set json(value:String) : void
    {
        var oldValue:String = _internal_json;
        if (oldValue !== value)
        {
            _internal_json = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "json", oldValue, _internal_json));
        }
    }

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set ptn_id(value:String) : void
    {
        var oldValue:String = _internal_ptn_id;
        if (oldValue !== value)
        {
            _internal_ptn_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_id", oldValue, _internal_ptn_id));
        }
    }

    public function set ptn_fname(value:String) : void
    {
        var oldValue:String = _internal_ptn_fname;
        if (oldValue !== value)
        {
            _internal_ptn_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_fname", oldValue, _internal_ptn_fname));
        }
    }

    public function set ptn_lname(value:String) : void
    {
        var oldValue:String = _internal_ptn_lname;
        if (oldValue !== value)
        {
            _internal_ptn_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_lname", oldValue, _internal_ptn_lname));
        }
    }

    public function set ptn_mname(value:String) : void
    {
        var oldValue:String = _internal_ptn_mname;
        if (oldValue !== value)
        {
            _internal_ptn_mname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_mname", oldValue, _internal_ptn_mname));
        }
    }

    public function set ptn_sex(value:int) : void
    {
        var oldValue:int = _internal_ptn_sex;
        if (oldValue !== value)
        {
            _internal_ptn_sex = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_sex", oldValue, _internal_ptn_sex));
        }
    }

    public function set ptn_birthday(value:String) : void
    {
        var oldValue:String = _internal_ptn_birthday;
        if (oldValue !== value)
        {
            _internal_ptn_birthday = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_birthday", oldValue, _internal_ptn_birthday));
        }
    }

    public function set ptn_email(value:String) : void
    {
        var oldValue:String = _internal_ptn_email;
        if (oldValue !== value)
        {
            _internal_ptn_email = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_email", oldValue, _internal_ptn_email));
        }
    }

    public function set ptn_taxid(value:String) : void
    {
        var oldValue:String = _internal_ptn_taxid;
        if (oldValue !== value)
        {
            _internal_ptn_taxid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_taxid", oldValue, _internal_ptn_taxid));
        }
    }

    public function set dec_id(value:String) : void
    {
        var oldValue:String = _internal_dec_id;
        if (oldValue !== value)
        {
            _internal_dec_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dec_id", oldValue, _internal_dec_id));
        }
    }

    public function set FK_SUBDIVISION(value:String) : void
    {
        var oldValue:String = _internal_FK_SUBDIVISION;
        if (oldValue !== value)
        {
            _internal_FK_SUBDIVISION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FK_SUBDIVISION", oldValue, _internal_FK_SUBDIVISION));
        }
    }

    public function set FK_ACCOUNT(value:String) : void
    {
        var oldValue:String = _internal_FK_ACCOUNT;
        if (oldValue !== value)
        {
            _internal_FK_ACCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FK_ACCOUNT", oldValue, _internal_FK_ACCOUNT));
        }
    }

    public function set ptn_birth_country(value:String) : void
    {
        var oldValue:String = _internal_ptn_birth_country;
        if (oldValue !== value)
        {
            _internal_ptn_birth_country = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_birth_country", oldValue, _internal_ptn_birth_country));
        }
    }

    public function set ptn_birth_settlement(value:String) : void
    {
        var oldValue:String = _internal_ptn_birth_settlement;
        if (oldValue !== value)
        {
            _internal_ptn_birth_settlement = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_birth_settlement", oldValue, _internal_ptn_birth_settlement));
        }
    }

    public function set ptn_secret(value:String) : void
    {
        var oldValue:String = _internal_ptn_secret;
        if (oldValue !== value)
        {
            _internal_ptn_secret = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_secret", oldValue, _internal_ptn_secret));
        }
    }

    public function set ptn_doc_type(value:String) : void
    {
        var oldValue:String = _internal_ptn_doc_type;
        if (oldValue !== value)
        {
            _internal_ptn_doc_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_doc_type", oldValue, _internal_ptn_doc_type));
        }
    }

    public function set ptn_doc_num(value:String) : void
    {
        var oldValue:String = _internal_ptn_doc_num;
        if (oldValue !== value)
        {
            _internal_ptn_doc_num = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ptn_doc_num", oldValue, _internal_ptn_doc_num));
        }
    }

    public function set emg_first_name(value:String) : void
    {
        var oldValue:String = _internal_emg_first_name;
        if (oldValue !== value)
        {
            _internal_emg_first_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "emg_first_name", oldValue, _internal_emg_first_name));
        }
    }

    public function set emg_last_name(value:String) : void
    {
        var oldValue:String = _internal_emg_last_name;
        if (oldValue !== value)
        {
            _internal_emg_last_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "emg_last_name", oldValue, _internal_emg_last_name));
        }
    }

    public function set emg_second_name(value:String) : void
    {
        var oldValue:String = _internal_emg_second_name;
        if (oldValue !== value)
        {
            _internal_emg_second_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "emg_second_name", oldValue, _internal_emg_second_name));
        }
    }

    public function set cnf_enable(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_cnf_enable;
        if (oldValue !== value)
        {
            _internal_cnf_enable = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_enable", oldValue, _internal_cnf_enable));
        }
    }

    public function set cnf_rel_type(value:String) : void
    {
        var oldValue:String = _internal_cnf_rel_type;
        if (oldValue !== value)
        {
            _internal_cnf_rel_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_rel_type", oldValue, _internal_cnf_rel_type));
        }
    }

    public function set cnf_first_name(value:String) : void
    {
        var oldValue:String = _internal_cnf_first_name;
        if (oldValue !== value)
        {
            _internal_cnf_first_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_first_name", oldValue, _internal_cnf_first_name));
        }
    }

    public function set cnf_last_name(value:String) : void
    {
        var oldValue:String = _internal_cnf_last_name;
        if (oldValue !== value)
        {
            _internal_cnf_last_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_last_name", oldValue, _internal_cnf_last_name));
        }
    }

    public function set cnf_second_name(value:String) : void
    {
        var oldValue:String = _internal_cnf_second_name;
        if (oldValue !== value)
        {
            _internal_cnf_second_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_second_name", oldValue, _internal_cnf_second_name));
        }
    }

    public function set cnf_birthday(value:String) : void
    {
        var oldValue:String = _internal_cnf_birthday;
        if (oldValue !== value)
        {
            _internal_cnf_birthday = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_birthday", oldValue, _internal_cnf_birthday));
        }
    }

    public function set cnf_birth_country(value:String) : void
    {
        var oldValue:String = _internal_cnf_birth_country;
        if (oldValue !== value)
        {
            _internal_cnf_birth_country = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_birth_country", oldValue, _internal_cnf_birth_country));
        }
    }

    public function set cnf_birth_settlement(value:String) : void
    {
        var oldValue:String = _internal_cnf_birth_settlement;
        if (oldValue !== value)
        {
            _internal_cnf_birth_settlement = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_birth_settlement", oldValue, _internal_cnf_birth_settlement));
        }
    }

    public function set cnf_sex(value:int) : void
    {
        var oldValue:int = _internal_cnf_sex;
        if (oldValue !== value)
        {
            _internal_cnf_sex = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_sex", oldValue, _internal_cnf_sex));
        }
    }

    public function set cnf_taxid(value:String) : void
    {
        var oldValue:String = _internal_cnf_taxid;
        if (oldValue !== value)
        {
            _internal_cnf_taxid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_taxid", oldValue, _internal_cnf_taxid));
        }
    }

    public function set cnf_secret(value:String) : void
    {
        var oldValue:String = _internal_cnf_secret;
        if (oldValue !== value)
        {
            _internal_cnf_secret = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_secret", oldValue, _internal_cnf_secret));
        }
    }

    public function set cnf_doc_type(value:String) : void
    {
        var oldValue:String = _internal_cnf_doc_type;
        if (oldValue !== value)
        {
            _internal_cnf_doc_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_doc_type", oldValue, _internal_cnf_doc_type));
        }
    }

    public function set cnf_doc_num(value:String) : void
    {
        var oldValue:String = _internal_cnf_doc_num;
        if (oldValue !== value)
        {
            _internal_cnf_doc_num = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_doc_num", oldValue, _internal_cnf_doc_num));
        }
    }

    public function set cnf_doc_rel_type(value:String) : void
    {
        var oldValue:String = _internal_cnf_doc_rel_type;
        if (oldValue !== value)
        {
            _internal_cnf_doc_rel_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_doc_rel_type", oldValue, _internal_cnf_doc_rel_type));
        }
    }

    public function set cnf_doc_rel_num(value:String) : void
    {
        var oldValue:String = _internal_cnf_doc_rel_num;
        if (oldValue !== value)
        {
            _internal_cnf_doc_rel_num = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cnf_doc_rel_num", oldValue, _internal_cnf_doc_rel_num));
        }
    }

    public function set PATIENT_SIGNED(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_PATIENT_SIGNED;
        if (oldValue !== value)
        {
            _internal_PATIENT_SIGNED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_SIGNED", oldValue, _internal_PATIENT_SIGNED));
        }
    }

    public function set PATIENT_CONSENT(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_PATIENT_CONSENT;
        if (oldValue !== value)
        {
            _internal_PATIENT_CONSENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_CONSENT", oldValue, _internal_PATIENT_CONSENT));
        }
    }

    public function set EH_DEC_ID(value:String) : void
    {
        var oldValue:String = _internal_EH_DEC_ID;
        if (oldValue !== value)
        {
            _internal_EH_DEC_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_DEC_ID", oldValue, _internal_EH_DEC_ID));
        }
    }

    public function set AUTH_TYPE(value:String) : void
    {
        var oldValue:String = _internal_AUTH_TYPE;
        if (oldValue !== value)
        {
            _internal_AUTH_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AUTH_TYPE", oldValue, _internal_AUTH_TYPE));
        }
    }

    public function set AUTH_PHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_AUTH_PHONENUMBER;
        if (oldValue !== value)
        {
            _internal_AUTH_PHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AUTH_PHONENUMBER", oldValue, _internal_AUTH_PHONENUMBER));
        }
    }

    public function set STATUS(value:String) : void
    {
        var oldValue:String = _internal_STATUS;
        if (oldValue !== value)
        {
            _internal_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS", oldValue, _internal_STATUS));
        }
    }

    public function set account_eh_id(value:String) : void
    {
        var oldValue:String = _internal_account_eh_id;
        if (oldValue !== value)
        {
            _internal_account_eh_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "account_eh_id", oldValue, _internal_account_eh_id));
        }
    }

    public function set subdivision_eh_id(value:String) : void
    {
        var oldValue:String = _internal_subdivision_eh_id;
        if (oldValue !== value)
        {
            _internal_subdivision_eh_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_eh_id", oldValue, _internal_subdivision_eh_id));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _EHealthModule_DeclarationEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _EHealthModule_DeclarationEntityMetadata) : void
    {
        var oldValue : _EHealthModule_DeclarationEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
