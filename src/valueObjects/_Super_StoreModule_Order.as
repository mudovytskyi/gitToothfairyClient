/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - StoreModule_Order.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_StoreModule_Order extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("StoreModule_Order") == null)
            {
                flash.net.registerClassAlias("StoreModule_Order", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("StoreModule_Order", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _StoreModule_OrderEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_NUMBER : String;
    private var _internal_label : String;
    private var _internal_TIMESTAMP : String;
    private var _internal_STATUS : String;
    private var _internal_STATUS_TIMESTAMP : String;
    private var _internal_CLIENTID : String;
    private var _internal_children : Object;
    private var _internal_isOpen : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_StoreModule_Order()
    {
        _model = new _StoreModule_OrderEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get NUMBER() : String
    {
        return _internal_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get label() : String
    {
        return _internal_label;
    }

    [Bindable(event="propertyChange")]
    public function get TIMESTAMP() : String
    {
        return _internal_TIMESTAMP;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS() : String
    {
        return _internal_STATUS;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS_TIMESTAMP() : String
    {
        return _internal_STATUS_TIMESTAMP;
    }

    [Bindable(event="propertyChange")]
    public function get CLIENTID() : String
    {
        return _internal_CLIENTID;
    }

    [Bindable(event="propertyChange")]
    public function get children() : Object
    {
        return _internal_children;
    }

    [Bindable(event="propertyChange")]
    public function get isOpen() : Boolean
    {
        return _internal_isOpen;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set NUMBER(value:String) : void
    {
        var oldValue:String = _internal_NUMBER;
        if (oldValue !== value)
        {
            _internal_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NUMBER", oldValue, _internal_NUMBER));
        }
    }

    public function set label(value:String) : void
    {
        var oldValue:String = _internal_label;
        if (oldValue !== value)
        {
            _internal_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "label", oldValue, _internal_label));
        }
    }

    public function set TIMESTAMP(value:String) : void
    {
        var oldValue:String = _internal_TIMESTAMP;
        if (oldValue !== value)
        {
            _internal_TIMESTAMP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TIMESTAMP", oldValue, _internal_TIMESTAMP));
        }
    }

    public function set STATUS(value:String) : void
    {
        var oldValue:String = _internal_STATUS;
        if (oldValue !== value)
        {
            _internal_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS", oldValue, _internal_STATUS));
        }
    }

    public function set STATUS_TIMESTAMP(value:String) : void
    {
        var oldValue:String = _internal_STATUS_TIMESTAMP;
        if (oldValue !== value)
        {
            _internal_STATUS_TIMESTAMP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS_TIMESTAMP", oldValue, _internal_STATUS_TIMESTAMP));
        }
    }

    public function set CLIENTID(value:String) : void
    {
        var oldValue:String = _internal_CLIENTID;
        if (oldValue !== value)
        {
            _internal_CLIENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLIENTID", oldValue, _internal_CLIENTID));
        }
    }

    public function set children(value:Object) : void
    {
        var oldValue:Object = _internal_children;
        if (oldValue !== value)
        {
            _internal_children = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "children", oldValue, _internal_children));
        }
    }

    public function set isOpen(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOpen;
        if (oldValue !== value)
        {
            _internal_isOpen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOpen", oldValue, _internal_isOpen));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _StoreModule_OrderEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _StoreModule_OrderEntityMetadata) : void
    {
        var oldValue : _StoreModule_OrderEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
