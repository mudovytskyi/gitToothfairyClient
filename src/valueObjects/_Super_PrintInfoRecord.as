/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PrintInfoRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PrintInfoRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PrintInfoRecord") == null)
            {
                flash.net.registerClassAlias("PrintInfoRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PrintInfoRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _PrintInfoRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_FORMNAME : String;
    private var _internal_DOCTORFULLNAME : String;
    private var _internal_BIRTHDATE : String;
    private var _internal_PATIENTFULLNAME : String;
    private var _internal_LEGALNAME : String;
    private var _internal_CLINICNAME : String;
    private var _internal_ADDRESS : String;
    private var _internal_PHONE : String;
    private var _internal_FAX : String;
    private var _internal_PATIENTSHORTNAME : String;
    private var _internal_DOCTORSHORTNAME : String;
    private var _internal_DATECREATE : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PrintInfoRecord()
    {
        _model = new _PrintInfoRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get FORMNAME() : String
    {
        return _internal_FORMNAME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTORFULLNAME() : String
    {
        return _internal_DOCTORFULLNAME;
    }

    [Bindable(event="propertyChange")]
    public function get BIRTHDATE() : String
    {
        return _internal_BIRTHDATE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTFULLNAME() : String
    {
        return _internal_PATIENTFULLNAME;
    }

    [Bindable(event="propertyChange")]
    public function get LEGALNAME() : String
    {
        return _internal_LEGALNAME;
    }

    [Bindable(event="propertyChange")]
    public function get CLINICNAME() : String
    {
        return _internal_CLINICNAME;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS() : String
    {
        return _internal_ADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get PHONE() : String
    {
        return _internal_PHONE;
    }

    [Bindable(event="propertyChange")]
    public function get FAX() : String
    {
        return _internal_FAX;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSHORTNAME() : String
    {
        return _internal_PATIENTSHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTORSHORTNAME() : String
    {
        return _internal_DOCTORSHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get DATECREATE() : String
    {
        return _internal_DATECREATE;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set FORMNAME(value:String) : void
    {
        var oldValue:String = _internal_FORMNAME;
        if (oldValue !== value)
        {
            _internal_FORMNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FORMNAME", oldValue, _internal_FORMNAME));
        }
    }

    public function set DOCTORFULLNAME(value:String) : void
    {
        var oldValue:String = _internal_DOCTORFULLNAME;
        if (oldValue !== value)
        {
            _internal_DOCTORFULLNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTORFULLNAME", oldValue, _internal_DOCTORFULLNAME));
        }
    }

    public function set BIRTHDATE(value:String) : void
    {
        var oldValue:String = _internal_BIRTHDATE;
        if (oldValue !== value)
        {
            _internal_BIRTHDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BIRTHDATE", oldValue, _internal_BIRTHDATE));
        }
    }

    public function set PATIENTFULLNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENTFULLNAME;
        if (oldValue !== value)
        {
            _internal_PATIENTFULLNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTFULLNAME", oldValue, _internal_PATIENTFULLNAME));
        }
    }

    public function set LEGALNAME(value:String) : void
    {
        var oldValue:String = _internal_LEGALNAME;
        if (oldValue !== value)
        {
            _internal_LEGALNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LEGALNAME", oldValue, _internal_LEGALNAME));
        }
    }

    public function set CLINICNAME(value:String) : void
    {
        var oldValue:String = _internal_CLINICNAME;
        if (oldValue !== value)
        {
            _internal_CLINICNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLINICNAME", oldValue, _internal_CLINICNAME));
        }
    }

    public function set ADDRESS(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS;
        if (oldValue !== value)
        {
            _internal_ADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS", oldValue, _internal_ADDRESS));
        }
    }

    public function set PHONE(value:String) : void
    {
        var oldValue:String = _internal_PHONE;
        if (oldValue !== value)
        {
            _internal_PHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PHONE", oldValue, _internal_PHONE));
        }
    }

    public function set FAX(value:String) : void
    {
        var oldValue:String = _internal_FAX;
        if (oldValue !== value)
        {
            _internal_FAX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FAX", oldValue, _internal_FAX));
        }
    }

    public function set PATIENTSHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSHORTNAME;
        if (oldValue !== value)
        {
            _internal_PATIENTSHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSHORTNAME", oldValue, _internal_PATIENTSHORTNAME));
        }
    }

    public function set DOCTORSHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_DOCTORSHORTNAME;
        if (oldValue !== value)
        {
            _internal_DOCTORSHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTORSHORTNAME", oldValue, _internal_DOCTORSHORTNAME));
        }
    }

    public function set DATECREATE(value:String) : void
    {
        var oldValue:String = _internal_DATECREATE;
        if (oldValue !== value)
        {
            _internal_DATECREATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATECREATE", oldValue, _internal_DATECREATE));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PrintInfoRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PrintInfoRecordEntityMetadata) : void
    {
        var oldValue : _PrintInfoRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
