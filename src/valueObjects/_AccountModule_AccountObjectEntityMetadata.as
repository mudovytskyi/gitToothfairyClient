
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _AccountModule_AccountObjectEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("flow_id", "patient_id", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "flow_sum", "flow_operationtype", "flow_operationnote", "flow_createdate", "flow_ordernumber", "flow_oninvoice_fk", "invoice_number", "invoice_createdate", "flow_isCashpayment", "flow_groupName", "flow_award_personId", "flow_award_personFio", "flow_award_foundDate", "exp_group_id", "exp_group_name", "exp_group_color", "exp_proc_id", "exp_proc_dateclose", "exp_proc_name", "exp_proc_shifr", "exp_doctor_fname", "exp_doctor_lname", "exp_doctor_sname", "exp_patient_fname", "exp_patient_lname", "exp_patient_sname", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "certificate_id", "certificate_number", "storage_docId", "storage_docType", "storage_docNumber", "storage_docDate", "storage_patientId", "storage_patientShortname");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("flow_id", "patient_id", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "flow_sum", "flow_operationtype", "flow_operationnote", "flow_createdate", "flow_ordernumber", "flow_oninvoice_fk", "invoice_number", "invoice_createdate", "flow_isCashpayment", "flow_groupName", "flow_award_personId", "flow_award_personFio", "flow_award_foundDate", "exp_group_id", "exp_group_name", "exp_group_color", "exp_proc_id", "exp_proc_dateclose", "exp_proc_name", "exp_proc_shifr", "exp_doctor_fname", "exp_doctor_lname", "exp_doctor_sname", "exp_patient_fname", "exp_patient_lname", "exp_patient_sname", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "certificate_id", "certificate_number", "storage_docId", "storage_docType", "storage_docNumber", "storage_docDate", "storage_patientId", "storage_patientShortname");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("flow_id", "patient_id", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "flow_sum", "flow_operationtype", "flow_operationnote", "flow_createdate", "flow_ordernumber", "flow_oninvoice_fk", "invoice_number", "invoice_createdate", "flow_isCashpayment", "flow_groupName", "flow_award_personId", "flow_award_personFio", "flow_award_foundDate", "exp_group_id", "exp_group_name", "exp_group_color", "exp_proc_id", "exp_proc_dateclose", "exp_proc_name", "exp_proc_shifr", "exp_doctor_fname", "exp_doctor_lname", "exp_doctor_sname", "exp_patient_fname", "exp_patient_lname", "exp_patient_sname", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "certificate_id", "certificate_number", "storage_docId", "storage_docType", "storage_docNumber", "storage_docDate", "storage_patientId", "storage_patientShortname");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("flow_id", "patient_id", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "flow_sum", "flow_operationtype", "flow_operationnote", "flow_createdate", "flow_ordernumber", "flow_oninvoice_fk", "invoice_number", "invoice_createdate", "flow_isCashpayment", "flow_groupName", "flow_award_personId", "flow_award_personFio", "flow_award_foundDate", "exp_group_id", "exp_group_name", "exp_group_color", "exp_proc_id", "exp_proc_dateclose", "exp_proc_name", "exp_proc_shifr", "exp_doctor_fname", "exp_doctor_lname", "exp_doctor_sname", "exp_patient_fname", "exp_patient_lname", "exp_patient_sname", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "certificate_id", "certificate_number", "storage_docId", "storage_docType", "storage_docNumber", "storage_docDate", "storage_patientId", "storage_patientShortname");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "AccountModule_AccountObject";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_AccountModule_AccountObject;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _AccountModule_AccountObjectEntityMetadata(value : _Super_AccountModule_AccountObject)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["flow_id"] = new Array();
            model_internal::dependentsOnMap["patient_id"] = new Array();
            model_internal::dependentsOnMap["patient_fname"] = new Array();
            model_internal::dependentsOnMap["patient_lname"] = new Array();
            model_internal::dependentsOnMap["patient_sname"] = new Array();
            model_internal::dependentsOnMap["patient_cardnumber"] = new Array();
            model_internal::dependentsOnMap["flow_sum"] = new Array();
            model_internal::dependentsOnMap["flow_operationtype"] = new Array();
            model_internal::dependentsOnMap["flow_operationnote"] = new Array();
            model_internal::dependentsOnMap["flow_createdate"] = new Array();
            model_internal::dependentsOnMap["flow_ordernumber"] = new Array();
            model_internal::dependentsOnMap["flow_oninvoice_fk"] = new Array();
            model_internal::dependentsOnMap["invoice_number"] = new Array();
            model_internal::dependentsOnMap["invoice_createdate"] = new Array();
            model_internal::dependentsOnMap["flow_isCashpayment"] = new Array();
            model_internal::dependentsOnMap["flow_groupName"] = new Array();
            model_internal::dependentsOnMap["flow_award_personId"] = new Array();
            model_internal::dependentsOnMap["flow_award_personFio"] = new Array();
            model_internal::dependentsOnMap["flow_award_foundDate"] = new Array();
            model_internal::dependentsOnMap["exp_group_id"] = new Array();
            model_internal::dependentsOnMap["exp_group_name"] = new Array();
            model_internal::dependentsOnMap["exp_group_color"] = new Array();
            model_internal::dependentsOnMap["exp_proc_id"] = new Array();
            model_internal::dependentsOnMap["exp_proc_dateclose"] = new Array();
            model_internal::dependentsOnMap["exp_proc_name"] = new Array();
            model_internal::dependentsOnMap["exp_proc_shifr"] = new Array();
            model_internal::dependentsOnMap["exp_doctor_fname"] = new Array();
            model_internal::dependentsOnMap["exp_doctor_lname"] = new Array();
            model_internal::dependentsOnMap["exp_doctor_sname"] = new Array();
            model_internal::dependentsOnMap["exp_patient_fname"] = new Array();
            model_internal::dependentsOnMap["exp_patient_lname"] = new Array();
            model_internal::dependentsOnMap["exp_patient_sname"] = new Array();
            model_internal::dependentsOnMap["author_id"] = new Array();
            model_internal::dependentsOnMap["author_shortname"] = new Array();
            model_internal::dependentsOnMap["subdivision_id"] = new Array();
            model_internal::dependentsOnMap["subdivision_name"] = new Array();
            model_internal::dependentsOnMap["certificate_id"] = new Array();
            model_internal::dependentsOnMap["certificate_number"] = new Array();
            model_internal::dependentsOnMap["storage_docId"] = new Array();
            model_internal::dependentsOnMap["storage_docType"] = new Array();
            model_internal::dependentsOnMap["storage_docNumber"] = new Array();
            model_internal::dependentsOnMap["storage_docDate"] = new Array();
            model_internal::dependentsOnMap["storage_patientId"] = new Array();
            model_internal::dependentsOnMap["storage_patientShortname"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["flow_id"] = "String";
        model_internal::propertyTypeMap["patient_id"] = "String";
        model_internal::propertyTypeMap["patient_fname"] = "String";
        model_internal::propertyTypeMap["patient_lname"] = "String";
        model_internal::propertyTypeMap["patient_sname"] = "String";
        model_internal::propertyTypeMap["patient_cardnumber"] = "String";
        model_internal::propertyTypeMap["flow_sum"] = "Number";
        model_internal::propertyTypeMap["flow_operationtype"] = "int";
        model_internal::propertyTypeMap["flow_operationnote"] = "String";
        model_internal::propertyTypeMap["flow_createdate"] = "String";
        model_internal::propertyTypeMap["flow_ordernumber"] = "int";
        model_internal::propertyTypeMap["flow_oninvoice_fk"] = "String";
        model_internal::propertyTypeMap["invoice_number"] = "String";
        model_internal::propertyTypeMap["invoice_createdate"] = "String";
        model_internal::propertyTypeMap["flow_isCashpayment"] = "int";
        model_internal::propertyTypeMap["flow_groupName"] = "String";
        model_internal::propertyTypeMap["flow_award_personId"] = "String";
        model_internal::propertyTypeMap["flow_award_personFio"] = "String";
        model_internal::propertyTypeMap["flow_award_foundDate"] = "String";
        model_internal::propertyTypeMap["exp_group_id"] = "String";
        model_internal::propertyTypeMap["exp_group_name"] = "String";
        model_internal::propertyTypeMap["exp_group_color"] = "int";
        model_internal::propertyTypeMap["exp_proc_id"] = "String";
        model_internal::propertyTypeMap["exp_proc_dateclose"] = "String";
        model_internal::propertyTypeMap["exp_proc_name"] = "String";
        model_internal::propertyTypeMap["exp_proc_shifr"] = "String";
        model_internal::propertyTypeMap["exp_doctor_fname"] = "String";
        model_internal::propertyTypeMap["exp_doctor_lname"] = "String";
        model_internal::propertyTypeMap["exp_doctor_sname"] = "String";
        model_internal::propertyTypeMap["exp_patient_fname"] = "String";
        model_internal::propertyTypeMap["exp_patient_lname"] = "String";
        model_internal::propertyTypeMap["exp_patient_sname"] = "String";
        model_internal::propertyTypeMap["author_id"] = "String";
        model_internal::propertyTypeMap["author_shortname"] = "String";
        model_internal::propertyTypeMap["subdivision_id"] = "String";
        model_internal::propertyTypeMap["subdivision_name"] = "String";
        model_internal::propertyTypeMap["certificate_id"] = "String";
        model_internal::propertyTypeMap["certificate_number"] = "String";
        model_internal::propertyTypeMap["storage_docId"] = "String";
        model_internal::propertyTypeMap["storage_docType"] = "int";
        model_internal::propertyTypeMap["storage_docNumber"] = "String";
        model_internal::propertyTypeMap["storage_docDate"] = "String";
        model_internal::propertyTypeMap["storage_patientId"] = "String";
        model_internal::propertyTypeMap["storage_patientShortname"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity AccountModule_AccountObject");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity AccountModule_AccountObject");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of AccountModule_AccountObject");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AccountModule_AccountObject");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity AccountModule_AccountObject");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AccountModule_AccountObject");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_snameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_cardnumberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_sumAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_operationtypeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_operationnoteAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_createdateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_ordernumberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_oninvoice_fkAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_numberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_createdateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_isCashpaymentAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_groupNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_award_personIdAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_award_personFioAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFlow_award_foundDateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_group_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_group_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_group_colorAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_proc_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_proc_datecloseAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_proc_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_proc_shifrAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_doctor_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_doctor_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_doctor_snameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_patient_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_patient_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isExp_patient_snameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAuthor_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAuthor_shortnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCertificate_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCertificate_numberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isStorage_docIdAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isStorage_docTypeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isStorage_docNumberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isStorage_docDateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isStorage_patientIdAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isStorage_patientShortnameAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get flow_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_snameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_cardnumberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_sumStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_operationtypeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_operationnoteStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_createdateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_ordernumberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_oninvoice_fkStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_numberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_createdateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_isCashpaymentStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_groupNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_award_personIdStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_award_personFioStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get flow_award_foundDateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_group_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_group_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_group_colorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_proc_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_proc_datecloseStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_proc_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_proc_shifrStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_doctor_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_doctor_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_doctor_snameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_patient_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_patient_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get exp_patient_snameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get author_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get author_shortnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get certificate_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get certificate_numberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get storage_docIdStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get storage_docTypeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get storage_docNumberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get storage_docDateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get storage_patientIdStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get storage_patientShortnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
