
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.Form039Module_F39_Val;
import valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc;
import valueObjects.HealthPlanMKB10ModuleTreatmentProc;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _HealthPlanMKB10ModuleTreatmentProcEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("procID", "visit", "procShifr", "planName", "label", "courseName", "procPrice", "procTime", "procUOP", "farms", "assistantID", "assistantShortName", "doctorID", "doctorShortName", "dateClose", "toothUse", "procHealthtype", "procExpenses", "proc_count", "isSaved", "isComplete", "isChanged", "invoice_id", "invoice_number", "invoice_createdate", "discount_flag", "salary_settings", "healthproc_fk", "order_fk", "order_fixed", "order_fixed_date", "complete_examdiag", "procHealthtypes", "NODETYPE", "F39", "F39OUT", "isOpen", "children", "diagnos_proceduresCount", "diagnos_factSumm", "diagnos_planSumm", "subscriptionCardID");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("procID", "visit", "procShifr", "planName", "label", "courseName", "procPrice", "procTime", "procUOP", "farms", "assistantID", "assistantShortName", "doctorID", "doctorShortName", "dateClose", "toothUse", "procHealthtype", "procExpenses", "proc_count", "isSaved", "isComplete", "isChanged", "invoice_id", "invoice_number", "invoice_createdate", "discount_flag", "salary_settings", "healthproc_fk", "order_fk", "order_fixed", "order_fixed_date", "complete_examdiag", "procHealthtypes", "NODETYPE", "F39", "F39OUT", "isOpen", "children", "diagnos_proceduresCount", "diagnos_factSumm", "diagnos_planSumm", "subscriptionCardID");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("procID", "visit", "procShifr", "planName", "label", "courseName", "procPrice", "procTime", "procUOP", "farms", "assistantID", "assistantShortName", "doctorID", "doctorShortName", "dateClose", "toothUse", "procHealthtype", "procExpenses", "proc_count", "isSaved", "isComplete", "isChanged", "invoice_id", "invoice_number", "invoice_createdate", "discount_flag", "salary_settings", "healthproc_fk", "order_fk", "order_fixed", "order_fixed_date", "complete_examdiag", "procHealthtypes", "NODETYPE", "F39", "F39OUT", "isOpen", "children", "diagnos_proceduresCount", "diagnos_factSumm", "diagnos_planSumm", "subscriptionCardID");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("procID", "visit", "procShifr", "planName", "label", "courseName", "procPrice", "procTime", "procUOP", "farms", "assistantID", "assistantShortName", "doctorID", "doctorShortName", "dateClose", "toothUse", "procHealthtype", "procExpenses", "proc_count", "isSaved", "isComplete", "isChanged", "invoice_id", "invoice_number", "invoice_createdate", "discount_flag", "salary_settings", "healthproc_fk", "order_fk", "order_fixed", "order_fixed_date", "complete_examdiag", "procHealthtypes", "NODETYPE", "F39", "F39OUT", "isOpen", "children", "diagnos_proceduresCount", "diagnos_factSumm", "diagnos_planSumm", "subscriptionCardID");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("farms", "F39", "F39OUT", "children");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "HealthPlanMKB10ModuleTreatmentProc";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_HealthPlanMKB10ModuleTreatmentProc;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _HealthPlanMKB10ModuleTreatmentProcEntityMetadata(value : _Super_HealthPlanMKB10ModuleTreatmentProc)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["procID"] = new Array();
            model_internal::dependentsOnMap["visit"] = new Array();
            model_internal::dependentsOnMap["procShifr"] = new Array();
            model_internal::dependentsOnMap["planName"] = new Array();
            model_internal::dependentsOnMap["label"] = new Array();
            model_internal::dependentsOnMap["courseName"] = new Array();
            model_internal::dependentsOnMap["procPrice"] = new Array();
            model_internal::dependentsOnMap["procTime"] = new Array();
            model_internal::dependentsOnMap["procUOP"] = new Array();
            model_internal::dependentsOnMap["farms"] = new Array();
            model_internal::dependentsOnMap["assistantID"] = new Array();
            model_internal::dependentsOnMap["assistantShortName"] = new Array();
            model_internal::dependentsOnMap["doctorID"] = new Array();
            model_internal::dependentsOnMap["doctorShortName"] = new Array();
            model_internal::dependentsOnMap["dateClose"] = new Array();
            model_internal::dependentsOnMap["toothUse"] = new Array();
            model_internal::dependentsOnMap["procHealthtype"] = new Array();
            model_internal::dependentsOnMap["procExpenses"] = new Array();
            model_internal::dependentsOnMap["proc_count"] = new Array();
            model_internal::dependentsOnMap["isSaved"] = new Array();
            model_internal::dependentsOnMap["isComplete"] = new Array();
            model_internal::dependentsOnMap["isChanged"] = new Array();
            model_internal::dependentsOnMap["invoice_id"] = new Array();
            model_internal::dependentsOnMap["invoice_number"] = new Array();
            model_internal::dependentsOnMap["invoice_createdate"] = new Array();
            model_internal::dependentsOnMap["discount_flag"] = new Array();
            model_internal::dependentsOnMap["salary_settings"] = new Array();
            model_internal::dependentsOnMap["healthproc_fk"] = new Array();
            model_internal::dependentsOnMap["order_fk"] = new Array();
            model_internal::dependentsOnMap["order_fixed"] = new Array();
            model_internal::dependentsOnMap["order_fixed_date"] = new Array();
            model_internal::dependentsOnMap["complete_examdiag"] = new Array();
            model_internal::dependentsOnMap["procHealthtypes"] = new Array();
            model_internal::dependentsOnMap["NODETYPE"] = new Array();
            model_internal::dependentsOnMap["F39"] = new Array();
            model_internal::dependentsOnMap["F39OUT"] = new Array();
            model_internal::dependentsOnMap["isOpen"] = new Array();
            model_internal::dependentsOnMap["children"] = new Array();
            model_internal::dependentsOnMap["diagnos_proceduresCount"] = new Array();
            model_internal::dependentsOnMap["diagnos_factSumm"] = new Array();
            model_internal::dependentsOnMap["diagnos_planSumm"] = new Array();
            model_internal::dependentsOnMap["subscriptionCardID"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["farms"] = "valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc";
            model_internal::collectionBaseMap["F39"] = "valueObjects.Form039Module_F39_Val";
            model_internal::collectionBaseMap["F39OUT"] = "valueObjects.Form039Module_F39_Val";
            model_internal::collectionBaseMap["children"] = "valueObjects.HealthPlanMKB10ModuleTreatmentProc";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["procID"] = "String";
        model_internal::propertyTypeMap["visit"] = "int";
        model_internal::propertyTypeMap["procShifr"] = "String";
        model_internal::propertyTypeMap["planName"] = "String";
        model_internal::propertyTypeMap["label"] = "String";
        model_internal::propertyTypeMap["courseName"] = "String";
        model_internal::propertyTypeMap["procPrice"] = "Number";
        model_internal::propertyTypeMap["procTime"] = "int";
        model_internal::propertyTypeMap["procUOP"] = "Number";
        model_internal::propertyTypeMap["farms"] = "ArrayCollection";
        model_internal::propertyTypeMap["assistantID"] = "String";
        model_internal::propertyTypeMap["assistantShortName"] = "String";
        model_internal::propertyTypeMap["doctorID"] = "String";
        model_internal::propertyTypeMap["doctorShortName"] = "String";
        model_internal::propertyTypeMap["dateClose"] = "String";
        model_internal::propertyTypeMap["toothUse"] = "int";
        model_internal::propertyTypeMap["procHealthtype"] = "int";
        model_internal::propertyTypeMap["procExpenses"] = "Number";
        model_internal::propertyTypeMap["proc_count"] = "Number";
        model_internal::propertyTypeMap["isSaved"] = "Boolean";
        model_internal::propertyTypeMap["isComplete"] = "Boolean";
        model_internal::propertyTypeMap["isChanged"] = "Boolean";
        model_internal::propertyTypeMap["invoice_id"] = "Number";
        model_internal::propertyTypeMap["invoice_number"] = "String";
        model_internal::propertyTypeMap["invoice_createdate"] = "String";
        model_internal::propertyTypeMap["discount_flag"] = "int";
        model_internal::propertyTypeMap["salary_settings"] = "int";
        model_internal::propertyTypeMap["healthproc_fk"] = "String";
        model_internal::propertyTypeMap["order_fk"] = "String";
        model_internal::propertyTypeMap["order_fixed"] = "Boolean";
        model_internal::propertyTypeMap["order_fixed_date"] = "String";
        model_internal::propertyTypeMap["complete_examdiag"] = "String";
        model_internal::propertyTypeMap["procHealthtypes"] = "String";
        model_internal::propertyTypeMap["NODETYPE"] = "String";
        model_internal::propertyTypeMap["F39"] = "ArrayCollection";
        model_internal::propertyTypeMap["F39OUT"] = "ArrayCollection";
        model_internal::propertyTypeMap["isOpen"] = "Boolean";
        model_internal::propertyTypeMap["children"] = "ArrayCollection";
        model_internal::propertyTypeMap["diagnos_proceduresCount"] = "int";
        model_internal::propertyTypeMap["diagnos_factSumm"] = "Number";
        model_internal::propertyTypeMap["diagnos_planSumm"] = "Number";
        model_internal::propertyTypeMap["subscriptionCardID"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity HealthPlanMKB10ModuleTreatmentProc");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity HealthPlanMKB10ModuleTreatmentProc");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of HealthPlanMKB10ModuleTreatmentProc");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity HealthPlanMKB10ModuleTreatmentProc");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity HealthPlanMKB10ModuleTreatmentProc");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity HealthPlanMKB10ModuleTreatmentProc");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isProcIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVisitAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcShifrAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPlanNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLabelAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCourseNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcPriceAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcTimeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcUOPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFarmsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAssistantIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAssistantShortNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctorIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctorShortNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDateCloseAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isToothUseAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcHealthtypeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcExpensesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProc_countAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsSavedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsCompleteAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsChangedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_numberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_createdateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_flagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSalary_settingsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHealthproc_fkAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOrder_fkAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOrder_fixedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOrder_fixed_dateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isComplete_examdiagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcHealthtypesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNODETYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isF39Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isF39OUTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsOpenAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isChildrenAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiagnos_proceduresCountAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiagnos_factSummAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiagnos_planSummAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubscriptionCardIDAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get procIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get visitStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procShifrStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get planNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get labelStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get courseNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procPriceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procTimeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procUOPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get farmsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get assistantIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get assistantShortNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctorIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctorShortNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get dateCloseStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get toothUseStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procHealthtypeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procExpensesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get proc_countStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isSavedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isCompleteStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isChangedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_numberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_createdateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_flagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get salary_settingsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get healthproc_fkStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get order_fkStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get order_fixedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get order_fixed_dateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get complete_examdiagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procHealthtypesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NODETYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get F39Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get F39OUTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isOpenStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get childrenStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get diagnos_proceduresCountStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get diagnos_factSummStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get diagnos_planSummStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subscriptionCardIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
