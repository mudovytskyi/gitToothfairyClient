
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _WarehouseModuleOLAPRecordEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("FARMFLOW_ID", "FARMFLOW_QUANTITY", "FARMFLOW_INQUANTITY", "FARMFLOW_OUTQUANTITY", "FARMFLOW_BEFOREQUANTITY", "FARMFLOW_RESQUANTITY", "FARMDOCUMENT_ID", "FARMDOCUMENT_STYPE", "FARMDOCUMENT_RTYPE", "SUPPLIER_ID", "SUPPLIER_NAME", "FARMDOCUMENT_DOCTIMESTAMP", "FARMDOCUMENT_DOCNUMBER", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "TREATMENTPROC_ID", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROCFARM_QUANTITY", "FARMPRICE_ID", "FARMPRICE_PRICE", "FARM_ID", "FARM_NAME", "FARM_NOMENCLATUREARTICLENUMBER", "FARM_MINIMUMQUANTITY", "DOCTOR_ID", "DOCTOR_SHORTNAME", "PATIENT_ID", "PATIENT_SHORTNAME", "SUMMOUT", "SUMMBEFORE", "SUMMRES", "SUMMIN", "SENDER", "RECEIVER", "FARM_REQUIRED", "isOpen");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("FARMFLOW_ID", "FARMFLOW_QUANTITY", "FARMFLOW_INQUANTITY", "FARMFLOW_OUTQUANTITY", "FARMFLOW_BEFOREQUANTITY", "FARMFLOW_RESQUANTITY", "FARMDOCUMENT_ID", "FARMDOCUMENT_STYPE", "FARMDOCUMENT_RTYPE", "SUPPLIER_ID", "SUPPLIER_NAME", "FARMDOCUMENT_DOCTIMESTAMP", "FARMDOCUMENT_DOCNUMBER", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "TREATMENTPROC_ID", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROCFARM_QUANTITY", "FARMPRICE_ID", "FARMPRICE_PRICE", "FARM_ID", "FARM_NAME", "FARM_NOMENCLATUREARTICLENUMBER", "FARM_MINIMUMQUANTITY", "DOCTOR_ID", "DOCTOR_SHORTNAME", "PATIENT_ID", "PATIENT_SHORTNAME", "SUMMOUT", "SUMMBEFORE", "SUMMRES", "SUMMIN", "SENDER", "RECEIVER", "FARM_REQUIRED", "isOpen");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("FARMFLOW_ID", "FARMFLOW_QUANTITY", "FARMFLOW_INQUANTITY", "FARMFLOW_OUTQUANTITY", "FARMFLOW_BEFOREQUANTITY", "FARMFLOW_RESQUANTITY", "FARMDOCUMENT_ID", "FARMDOCUMENT_STYPE", "FARMDOCUMENT_RTYPE", "SUPPLIER_ID", "SUPPLIER_NAME", "FARMDOCUMENT_DOCTIMESTAMP", "FARMDOCUMENT_DOCNUMBER", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "TREATMENTPROC_ID", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROCFARM_QUANTITY", "FARMPRICE_ID", "FARMPRICE_PRICE", "FARM_ID", "FARM_NAME", "FARM_NOMENCLATUREARTICLENUMBER", "FARM_MINIMUMQUANTITY", "DOCTOR_ID", "DOCTOR_SHORTNAME", "PATIENT_ID", "PATIENT_SHORTNAME", "SUMMOUT", "SUMMBEFORE", "SUMMRES", "SUMMIN", "SENDER", "RECEIVER", "FARM_REQUIRED", "isOpen");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("FARMFLOW_ID", "FARMFLOW_QUANTITY", "FARMFLOW_INQUANTITY", "FARMFLOW_OUTQUANTITY", "FARMFLOW_BEFOREQUANTITY", "FARMFLOW_RESQUANTITY", "FARMDOCUMENT_ID", "FARMDOCUMENT_STYPE", "FARMDOCUMENT_RTYPE", "SUPPLIER_ID", "SUPPLIER_NAME", "FARMDOCUMENT_DOCTIMESTAMP", "FARMDOCUMENT_DOCNUMBER", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "TREATMENTPROC_ID", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROCFARM_QUANTITY", "FARMPRICE_ID", "FARMPRICE_PRICE", "FARM_ID", "FARM_NAME", "FARM_NOMENCLATUREARTICLENUMBER", "FARM_MINIMUMQUANTITY", "DOCTOR_ID", "DOCTOR_SHORTNAME", "PATIENT_ID", "PATIENT_SHORTNAME", "SUMMOUT", "SUMMBEFORE", "SUMMRES", "SUMMIN", "SENDER", "RECEIVER", "FARM_REQUIRED", "isOpen");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "WarehouseModuleOLAPRecord";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_WarehouseModuleOLAPRecord;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _WarehouseModuleOLAPRecordEntityMetadata(value : _Super_WarehouseModuleOLAPRecord)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["FARMFLOW_ID"] = new Array();
            model_internal::dependentsOnMap["FARMFLOW_QUANTITY"] = new Array();
            model_internal::dependentsOnMap["FARMFLOW_INQUANTITY"] = new Array();
            model_internal::dependentsOnMap["FARMFLOW_OUTQUANTITY"] = new Array();
            model_internal::dependentsOnMap["FARMFLOW_BEFOREQUANTITY"] = new Array();
            model_internal::dependentsOnMap["FARMFLOW_RESQUANTITY"] = new Array();
            model_internal::dependentsOnMap["FARMDOCUMENT_ID"] = new Array();
            model_internal::dependentsOnMap["FARMDOCUMENT_STYPE"] = new Array();
            model_internal::dependentsOnMap["FARMDOCUMENT_RTYPE"] = new Array();
            model_internal::dependentsOnMap["SUPPLIER_ID"] = new Array();
            model_internal::dependentsOnMap["SUPPLIER_NAME"] = new Array();
            model_internal::dependentsOnMap["FARMDOCUMENT_DOCTIMESTAMP"] = new Array();
            model_internal::dependentsOnMap["FARMDOCUMENT_DOCNUMBER"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROCFARM_ID"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROCFARM_QUANTITY"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_ID"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROC_ID"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROC_NAME"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROCFARM_QUANTITY"] = new Array();
            model_internal::dependentsOnMap["FARMPRICE_ID"] = new Array();
            model_internal::dependentsOnMap["FARMPRICE_PRICE"] = new Array();
            model_internal::dependentsOnMap["FARM_ID"] = new Array();
            model_internal::dependentsOnMap["FARM_NAME"] = new Array();
            model_internal::dependentsOnMap["FARM_NOMENCLATUREARTICLENUMBER"] = new Array();
            model_internal::dependentsOnMap["FARM_MINIMUMQUANTITY"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_ID"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_SHORTNAME"] = new Array();
            model_internal::dependentsOnMap["PATIENT_ID"] = new Array();
            model_internal::dependentsOnMap["PATIENT_SHORTNAME"] = new Array();
            model_internal::dependentsOnMap["SUMMOUT"] = new Array();
            model_internal::dependentsOnMap["SUMMBEFORE"] = new Array();
            model_internal::dependentsOnMap["SUMMRES"] = new Array();
            model_internal::dependentsOnMap["SUMMIN"] = new Array();
            model_internal::dependentsOnMap["SENDER"] = new Array();
            model_internal::dependentsOnMap["RECEIVER"] = new Array();
            model_internal::dependentsOnMap["FARM_REQUIRED"] = new Array();
            model_internal::dependentsOnMap["isOpen"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["FARMFLOW_ID"] = "String";
        model_internal::propertyTypeMap["FARMFLOW_QUANTITY"] = "Number";
        model_internal::propertyTypeMap["FARMFLOW_INQUANTITY"] = "Number";
        model_internal::propertyTypeMap["FARMFLOW_OUTQUANTITY"] = "Number";
        model_internal::propertyTypeMap["FARMFLOW_BEFOREQUANTITY"] = "Number";
        model_internal::propertyTypeMap["FARMFLOW_RESQUANTITY"] = "Number";
        model_internal::propertyTypeMap["FARMDOCUMENT_ID"] = "String";
        model_internal::propertyTypeMap["FARMDOCUMENT_STYPE"] = "String";
        model_internal::propertyTypeMap["FARMDOCUMENT_RTYPE"] = "String";
        model_internal::propertyTypeMap["SUPPLIER_ID"] = "String";
        model_internal::propertyTypeMap["SUPPLIER_NAME"] = "String";
        model_internal::propertyTypeMap["FARMDOCUMENT_DOCTIMESTAMP"] = "String";
        model_internal::propertyTypeMap["FARMDOCUMENT_DOCNUMBER"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROCFARM_ID"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROCFARM_QUANTITY"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_ID"] = "String";
        model_internal::propertyTypeMap["HEALTHPROC_ID"] = "String";
        model_internal::propertyTypeMap["HEALTHPROC_NAME"] = "String";
        model_internal::propertyTypeMap["HEALTHPROCFARM_QUANTITY"] = "Number";
        model_internal::propertyTypeMap["FARMPRICE_ID"] = "String";
        model_internal::propertyTypeMap["FARMPRICE_PRICE"] = "String";
        model_internal::propertyTypeMap["FARM_ID"] = "String";
        model_internal::propertyTypeMap["FARM_NAME"] = "String";
        model_internal::propertyTypeMap["FARM_NOMENCLATUREARTICLENUMBER"] = "String";
        model_internal::propertyTypeMap["FARM_MINIMUMQUANTITY"] = "Number";
        model_internal::propertyTypeMap["DOCTOR_ID"] = "String";
        model_internal::propertyTypeMap["DOCTOR_SHORTNAME"] = "String";
        model_internal::propertyTypeMap["PATIENT_ID"] = "String";
        model_internal::propertyTypeMap["PATIENT_SHORTNAME"] = "String";
        model_internal::propertyTypeMap["SUMMOUT"] = "Number";
        model_internal::propertyTypeMap["SUMMBEFORE"] = "Number";
        model_internal::propertyTypeMap["SUMMRES"] = "Number";
        model_internal::propertyTypeMap["SUMMIN"] = "Number";
        model_internal::propertyTypeMap["SENDER"] = "String";
        model_internal::propertyTypeMap["RECEIVER"] = "String";
        model_internal::propertyTypeMap["FARM_REQUIRED"] = "int";
        model_internal::propertyTypeMap["isOpen"] = "Boolean";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity WarehouseModuleOLAPRecord");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity WarehouseModuleOLAPRecord");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of WarehouseModuleOLAPRecord");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity WarehouseModuleOLAPRecord");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity WarehouseModuleOLAPRecord");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity WarehouseModuleOLAPRecord");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isFARMFLOW_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMFLOW_QUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMFLOW_INQUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMFLOW_OUTQUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMFLOW_BEFOREQUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMFLOW_RESQUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMDOCUMENT_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMDOCUMENT_STYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMDOCUMENT_RTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUPPLIER_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUPPLIER_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMDOCUMENT_DOCTIMESTAMPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMDOCUMENT_DOCNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROCFARM_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROCFARM_QUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROC_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROC_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROCFARM_QUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMPRICE_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARMPRICE_PRICEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_NOMENCLATUREARTICLENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_MINIMUMQUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_SHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_SHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUMMOUTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUMMBEFOREAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUMMRESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUMMINAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSENDERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRECEIVERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_REQUIREDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsOpenAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get FARMFLOW_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMFLOW_QUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMFLOW_INQUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMFLOW_OUTQUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMFLOW_BEFOREQUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMFLOW_RESQUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMDOCUMENT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMDOCUMENT_STYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMDOCUMENT_RTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUPPLIER_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUPPLIER_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMDOCUMENT_DOCTIMESTAMPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMDOCUMENT_DOCNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROCFARM_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROCFARM_QUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROC_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROC_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROCFARM_QUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMPRICE_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARMPRICE_PRICEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_NOMENCLATUREARTICLENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_MINIMUMQUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_SHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_SHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUMMOUTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUMMBEFOREStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUMMRESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUMMINStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SENDERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get RECEIVERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_REQUIREDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isOpenStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
