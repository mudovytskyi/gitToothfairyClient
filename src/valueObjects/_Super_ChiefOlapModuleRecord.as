/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefOlapModuleRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefOlapModuleRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefOlapModuleRecord") == null)
            {
                flash.net.registerClassAlias("ChiefOlapModuleRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefOlapModuleRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefOlapModuleRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_FARM_ID : String;
    private var _internal_FARM_NAME : String;
    private var _internal_TREATMENTPROCFARM_ID : String;
    private var _internal_TREATMENTPROCFARM_QUANTITY : Number;
    private var _internal_HEALTHPROCFARM_QUANTITY : Number;
    private var _internal_TREATMENTPROC_ID : String;
    private var _internal_TREATMENTPROC_NAME : String;
    private var _internal_TREATMENTPROC_SHIFR : String;
    private var _internal_TREATMENTPROC_HEALTHTYPE : String;
    private var _internal_TREATMENTPROC_UOP : Number;
    private var _internal_TREATMENTPROC_PRICE : Number;
    private var _internal_TREATMENTPROC_PRICE_DISCOUNT : Number;
    private var _internal_TREATMENTPROC_PRICE_DISCOUNT_SUMM : Number;
    private var _internal_TREATMENTPROC_DATECLOSE : String;
    private var _internal_TREATMENTPROC_DAYCLOSE : String;
    private var _internal_TREATMENTPROC_EXPENSES : Number;
    private var _internal_TREATMENTPROC_PROC_COUNT : Number;
    private var _internal_TREATMENTPROC_EXPENSES_FACT : Number;
    private var _internal_TREATMENTPROC_EXPENSES_DELTA : Number;
    private var _internal_DOCTOR_ID : String;
    private var _internal_DOCTOR_SHORTNAME : String;
    private var _internal_DOCTOR_SPECIALITY : String;
    private var _internal_DOCTOR_PERCENT : Number;
    private var _internal_DOCTOR_BASESALARY : Number;
    private var _internal_DOCTOR_PERCENTSUMM : Number;
    private var _internal_DOCTOR_FULLSALARY : Number;
    private var _internal_ASSISTANT_BASESALARY : Number;
    private var _internal_ASSISTANT_PERCENTSUMM : Number;
    private var _internal_ASSISTANT_FULLSALARY : Number;
    private var _internal_TREATMENTPROC_CALCTIME : Number;
    private var _internal_PATIENT_ID : String;
    private var _internal_PATIENT_FULLNAME : String;
    private var _internal_PATIENT_BIRTHDAY : String;
    private var _internal_PATIENT_AGE : int;
    private var _internal_PATIENT_SEX : String;
    private var _internal_PATIENT_VILLAGEORCITY : String;
    private var _internal_PATIENT_POPULATIONGROUPE : String;
    private var _internal_PATIENT_ISDISPENSARY : String;
    private var _internal_PATIENT_DISCOUNT_AMOUNT : String;
    private var _internal_PATIENT_VISIT_ID : String;
    private var _internal_PATIENT_VISIT : int;
    private var _internal_PATIENT_COUNT : int;
    private var _internal_WHEREFROM_ID : String;
    private var _internal_WHEREFROM_DESCRIPTION : String;
    private var _internal_GROUP_ID : String;
    private var _internal_GROUP_DESCRIPTION : String;
    private var _internal_HEALTHPROC_ID : String;
    private var _internal_HEALTHPROC_NAME : String;
    private var _internal_HEALTHPROC_PROCTYPE : String;
    private var _internal_HEALTHPROC_PRICE : Number;
    private var _internal_INVOICES_ID : String;
    private var _internal_INVOICES_INVOICENUMBER : String;
    private var _internal_INVOICES_CREATEDATE : String;
    private var _internal_INVOICES_PAIDINCASH : Number;
    private var _internal_TREATMENTPROC_DISCOUNT_AMOUNT : Number;
    private var _internal_ACCOUNTFLOW_ID : String;
    private var _internal_ACCOUNTFLOW_ORDERNUMBER : String;
    private var _internal_ACCOUNTFLOW_SUMM : Number;
    private var _internal_ACCOUNTFLOW_OPERATIONTIMESTAMP : String;
    private var _internal_INVOICES_CREATE_DAY : String;
    private var _internal_ACCOUNTFLOW_OPERATIONDAY : String;
    private var _internal_PATIENTSINSURANCE_ID : String;
    private var _internal_PATIENTSINSURANCE_POLICENUMBER : String;
    private var _internal_PATIENTSINSURANCE_POLICETODATE : String;
    private var _internal_PATIENTSINSURANCE_POLICEFROMDATE : String;
    private var _internal_INSURANCECOMPANYS_ID : String;
    private var _internal_INSURANCECOMPANYS_NAME : String;
    private var _internal_COUNT : int;
    private var _internal_isOpen : Boolean;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefOlapModuleRecord()
    {
        _model = new _ChiefOlapModuleRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get FARM_ID() : String
    {
        return _internal_FARM_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_NAME() : String
    {
        return _internal_FARM_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROCFARM_ID() : String
    {
        return _internal_TREATMENTPROCFARM_ID;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROCFARM_QUANTITY() : Number
    {
        return _internal_TREATMENTPROCFARM_QUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROCFARM_QUANTITY() : Number
    {
        return _internal_HEALTHPROCFARM_QUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_ID() : String
    {
        return _internal_TREATMENTPROC_ID;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_NAME() : String
    {
        return _internal_TREATMENTPROC_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_SHIFR() : String
    {
        return _internal_TREATMENTPROC_SHIFR;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_HEALTHTYPE() : String
    {
        return _internal_TREATMENTPROC_HEALTHTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_UOP() : Number
    {
        return _internal_TREATMENTPROC_UOP;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_PRICE() : Number
    {
        return _internal_TREATMENTPROC_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_PRICE_DISCOUNT() : Number
    {
        return _internal_TREATMENTPROC_PRICE_DISCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_PRICE_DISCOUNT_SUMM() : Number
    {
        return _internal_TREATMENTPROC_PRICE_DISCOUNT_SUMM;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_DATECLOSE() : String
    {
        return _internal_TREATMENTPROC_DATECLOSE;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_DAYCLOSE() : String
    {
        return _internal_TREATMENTPROC_DAYCLOSE;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_EXPENSES() : Number
    {
        return _internal_TREATMENTPROC_EXPENSES;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_PROC_COUNT() : Number
    {
        return _internal_TREATMENTPROC_PROC_COUNT;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_EXPENSES_FACT() : Number
    {
        return _internal_TREATMENTPROC_EXPENSES_FACT;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_EXPENSES_DELTA() : Number
    {
        return _internal_TREATMENTPROC_EXPENSES_DELTA;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_ID() : String
    {
        return _internal_DOCTOR_ID;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_SHORTNAME() : String
    {
        return _internal_DOCTOR_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_SPECIALITY() : String
    {
        return _internal_DOCTOR_SPECIALITY;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_PERCENT() : Number
    {
        return _internal_DOCTOR_PERCENT;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_BASESALARY() : Number
    {
        return _internal_DOCTOR_BASESALARY;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_PERCENTSUMM() : Number
    {
        return _internal_DOCTOR_PERCENTSUMM;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_FULLSALARY() : Number
    {
        return _internal_DOCTOR_FULLSALARY;
    }

    [Bindable(event="propertyChange")]
    public function get ASSISTANT_BASESALARY() : Number
    {
        return _internal_ASSISTANT_BASESALARY;
    }

    [Bindable(event="propertyChange")]
    public function get ASSISTANT_PERCENTSUMM() : Number
    {
        return _internal_ASSISTANT_PERCENTSUMM;
    }

    [Bindable(event="propertyChange")]
    public function get ASSISTANT_FULLSALARY() : Number
    {
        return _internal_ASSISTANT_FULLSALARY;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_CALCTIME() : Number
    {
        return _internal_TREATMENTPROC_CALCTIME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_ID() : String
    {
        return _internal_PATIENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_FULLNAME() : String
    {
        return _internal_PATIENT_FULLNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_BIRTHDAY() : String
    {
        return _internal_PATIENT_BIRTHDAY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_AGE() : int
    {
        return _internal_PATIENT_AGE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_SEX() : String
    {
        return _internal_PATIENT_SEX;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_VILLAGEORCITY() : String
    {
        return _internal_PATIENT_VILLAGEORCITY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_POPULATIONGROUPE() : String
    {
        return _internal_PATIENT_POPULATIONGROUPE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_ISDISPENSARY() : String
    {
        return _internal_PATIENT_ISDISPENSARY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_DISCOUNT_AMOUNT() : String
    {
        return _internal_PATIENT_DISCOUNT_AMOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_VISIT_ID() : String
    {
        return _internal_PATIENT_VISIT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_VISIT() : int
    {
        return _internal_PATIENT_VISIT;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_COUNT() : int
    {
        return _internal_PATIENT_COUNT;
    }

    [Bindable(event="propertyChange")]
    public function get WHEREFROM_ID() : String
    {
        return _internal_WHEREFROM_ID;
    }

    [Bindable(event="propertyChange")]
    public function get WHEREFROM_DESCRIPTION() : String
    {
        return _internal_WHEREFROM_DESCRIPTION;
    }

    [Bindable(event="propertyChange")]
    public function get GROUP_ID() : String
    {
        return _internal_GROUP_ID;
    }

    [Bindable(event="propertyChange")]
    public function get GROUP_DESCRIPTION() : String
    {
        return _internal_GROUP_DESCRIPTION;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROC_ID() : String
    {
        return _internal_HEALTHPROC_ID;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROC_NAME() : String
    {
        return _internal_HEALTHPROC_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROC_PROCTYPE() : String
    {
        return _internal_HEALTHPROC_PROCTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROC_PRICE() : Number
    {
        return _internal_HEALTHPROC_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get INVOICES_ID() : String
    {
        return _internal_INVOICES_ID;
    }

    [Bindable(event="propertyChange")]
    public function get INVOICES_INVOICENUMBER() : String
    {
        return _internal_INVOICES_INVOICENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get INVOICES_CREATEDATE() : String
    {
        return _internal_INVOICES_CREATEDATE;
    }

    [Bindable(event="propertyChange")]
    public function get INVOICES_PAIDINCASH() : Number
    {
        return _internal_INVOICES_PAIDINCASH;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_DISCOUNT_AMOUNT() : Number
    {
        return _internal_TREATMENTPROC_DISCOUNT_AMOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNTFLOW_ID() : String
    {
        return _internal_ACCOUNTFLOW_ID;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNTFLOW_ORDERNUMBER() : String
    {
        return _internal_ACCOUNTFLOW_ORDERNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNTFLOW_SUMM() : Number
    {
        return _internal_ACCOUNTFLOW_SUMM;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNTFLOW_OPERATIONTIMESTAMP() : String
    {
        return _internal_ACCOUNTFLOW_OPERATIONTIMESTAMP;
    }

    [Bindable(event="propertyChange")]
    public function get INVOICES_CREATE_DAY() : String
    {
        return _internal_INVOICES_CREATE_DAY;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNTFLOW_OPERATIONDAY() : String
    {
        return _internal_ACCOUNTFLOW_OPERATIONDAY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSINSURANCE_ID() : String
    {
        return _internal_PATIENTSINSURANCE_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSINSURANCE_POLICENUMBER() : String
    {
        return _internal_PATIENTSINSURANCE_POLICENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSINSURANCE_POLICETODATE() : String
    {
        return _internal_PATIENTSINSURANCE_POLICETODATE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSINSURANCE_POLICEFROMDATE() : String
    {
        return _internal_PATIENTSINSURANCE_POLICEFROMDATE;
    }

    [Bindable(event="propertyChange")]
    public function get INSURANCECOMPANYS_ID() : String
    {
        return _internal_INSURANCECOMPANYS_ID;
    }

    [Bindable(event="propertyChange")]
    public function get INSURANCECOMPANYS_NAME() : String
    {
        return _internal_INSURANCECOMPANYS_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get COUNT() : int
    {
        return _internal_COUNT;
    }

    [Bindable(event="propertyChange")]
    public function get isOpen() : Boolean
    {
        return _internal_isOpen;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set FARM_ID(value:String) : void
    {
        var oldValue:String = _internal_FARM_ID;
        if (oldValue !== value)
        {
            _internal_FARM_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_ID", oldValue, _internal_FARM_ID));
        }
    }

    public function set FARM_NAME(value:String) : void
    {
        var oldValue:String = _internal_FARM_NAME;
        if (oldValue !== value)
        {
            _internal_FARM_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_NAME", oldValue, _internal_FARM_NAME));
        }
    }

    public function set TREATMENTPROCFARM_ID(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROCFARM_ID;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROCFARM_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROCFARM_ID", oldValue, _internal_TREATMENTPROCFARM_ID));
        }
    }

    public function set TREATMENTPROCFARM_QUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROCFARM_QUANTITY;
        if (isNaN(_internal_TREATMENTPROCFARM_QUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROCFARM_QUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROCFARM_QUANTITY", oldValue, _internal_TREATMENTPROCFARM_QUANTITY));
        }
    }

    public function set HEALTHPROCFARM_QUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_HEALTHPROCFARM_QUANTITY;
        if (isNaN(_internal_HEALTHPROCFARM_QUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_HEALTHPROCFARM_QUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROCFARM_QUANTITY", oldValue, _internal_HEALTHPROCFARM_QUANTITY));
        }
    }

    public function set TREATMENTPROC_ID(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROC_ID;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROC_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_ID", oldValue, _internal_TREATMENTPROC_ID));
        }
    }

    public function set TREATMENTPROC_NAME(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROC_NAME;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROC_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_NAME", oldValue, _internal_TREATMENTPROC_NAME));
        }
    }

    public function set TREATMENTPROC_SHIFR(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROC_SHIFR;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROC_SHIFR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_SHIFR", oldValue, _internal_TREATMENTPROC_SHIFR));
        }
    }

    public function set TREATMENTPROC_HEALTHTYPE(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROC_HEALTHTYPE;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROC_HEALTHTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_HEALTHTYPE", oldValue, _internal_TREATMENTPROC_HEALTHTYPE));
        }
    }

    public function set TREATMENTPROC_UOP(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_UOP;
        if (isNaN(_internal_TREATMENTPROC_UOP) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_UOP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_UOP", oldValue, _internal_TREATMENTPROC_UOP));
        }
    }

    public function set TREATMENTPROC_PRICE(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_PRICE;
        if (isNaN(_internal_TREATMENTPROC_PRICE) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_PRICE", oldValue, _internal_TREATMENTPROC_PRICE));
        }
    }

    public function set TREATMENTPROC_PRICE_DISCOUNT(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_PRICE_DISCOUNT;
        if (isNaN(_internal_TREATMENTPROC_PRICE_DISCOUNT) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_PRICE_DISCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_PRICE_DISCOUNT", oldValue, _internal_TREATMENTPROC_PRICE_DISCOUNT));
        }
    }

    public function set TREATMENTPROC_PRICE_DISCOUNT_SUMM(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_PRICE_DISCOUNT_SUMM;
        if (isNaN(_internal_TREATMENTPROC_PRICE_DISCOUNT_SUMM) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_PRICE_DISCOUNT_SUMM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_PRICE_DISCOUNT_SUMM", oldValue, _internal_TREATMENTPROC_PRICE_DISCOUNT_SUMM));
        }
    }

    public function set TREATMENTPROC_DATECLOSE(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROC_DATECLOSE;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROC_DATECLOSE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_DATECLOSE", oldValue, _internal_TREATMENTPROC_DATECLOSE));
        }
    }

    public function set TREATMENTPROC_DAYCLOSE(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROC_DAYCLOSE;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROC_DAYCLOSE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_DAYCLOSE", oldValue, _internal_TREATMENTPROC_DAYCLOSE));
        }
    }

    public function set TREATMENTPROC_EXPENSES(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_EXPENSES;
        if (isNaN(_internal_TREATMENTPROC_EXPENSES) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_EXPENSES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_EXPENSES", oldValue, _internal_TREATMENTPROC_EXPENSES));
        }
    }

    public function set TREATMENTPROC_PROC_COUNT(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_PROC_COUNT;
        if (isNaN(_internal_TREATMENTPROC_PROC_COUNT) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_PROC_COUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_PROC_COUNT", oldValue, _internal_TREATMENTPROC_PROC_COUNT));
        }
    }

    public function set TREATMENTPROC_EXPENSES_FACT(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_EXPENSES_FACT;
        if (isNaN(_internal_TREATMENTPROC_EXPENSES_FACT) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_EXPENSES_FACT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_EXPENSES_FACT", oldValue, _internal_TREATMENTPROC_EXPENSES_FACT));
        }
    }

    public function set TREATMENTPROC_EXPENSES_DELTA(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_EXPENSES_DELTA;
        if (isNaN(_internal_TREATMENTPROC_EXPENSES_DELTA) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_EXPENSES_DELTA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_EXPENSES_DELTA", oldValue, _internal_TREATMENTPROC_EXPENSES_DELTA));
        }
    }

    public function set DOCTOR_ID(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_ID;
        if (oldValue !== value)
        {
            _internal_DOCTOR_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_ID", oldValue, _internal_DOCTOR_ID));
        }
    }

    public function set DOCTOR_SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_SHORTNAME", oldValue, _internal_DOCTOR_SHORTNAME));
        }
    }

    public function set DOCTOR_SPECIALITY(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_SPECIALITY;
        if (oldValue !== value)
        {
            _internal_DOCTOR_SPECIALITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_SPECIALITY", oldValue, _internal_DOCTOR_SPECIALITY));
        }
    }

    public function set DOCTOR_PERCENT(value:Number) : void
    {
        var oldValue:Number = _internal_DOCTOR_PERCENT;
        if (isNaN(_internal_DOCTOR_PERCENT) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_DOCTOR_PERCENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_PERCENT", oldValue, _internal_DOCTOR_PERCENT));
        }
    }

    public function set DOCTOR_BASESALARY(value:Number) : void
    {
        var oldValue:Number = _internal_DOCTOR_BASESALARY;
        if (isNaN(_internal_DOCTOR_BASESALARY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_DOCTOR_BASESALARY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_BASESALARY", oldValue, _internal_DOCTOR_BASESALARY));
        }
    }

    public function set DOCTOR_PERCENTSUMM(value:Number) : void
    {
        var oldValue:Number = _internal_DOCTOR_PERCENTSUMM;
        if (isNaN(_internal_DOCTOR_PERCENTSUMM) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_DOCTOR_PERCENTSUMM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_PERCENTSUMM", oldValue, _internal_DOCTOR_PERCENTSUMM));
        }
    }

    public function set DOCTOR_FULLSALARY(value:Number) : void
    {
        var oldValue:Number = _internal_DOCTOR_FULLSALARY;
        if (isNaN(_internal_DOCTOR_FULLSALARY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_DOCTOR_FULLSALARY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_FULLSALARY", oldValue, _internal_DOCTOR_FULLSALARY));
        }
    }

    public function set ASSISTANT_BASESALARY(value:Number) : void
    {
        var oldValue:Number = _internal_ASSISTANT_BASESALARY;
        if (isNaN(_internal_ASSISTANT_BASESALARY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_ASSISTANT_BASESALARY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ASSISTANT_BASESALARY", oldValue, _internal_ASSISTANT_BASESALARY));
        }
    }

    public function set ASSISTANT_PERCENTSUMM(value:Number) : void
    {
        var oldValue:Number = _internal_ASSISTANT_PERCENTSUMM;
        if (isNaN(_internal_ASSISTANT_PERCENTSUMM) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_ASSISTANT_PERCENTSUMM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ASSISTANT_PERCENTSUMM", oldValue, _internal_ASSISTANT_PERCENTSUMM));
        }
    }

    public function set ASSISTANT_FULLSALARY(value:Number) : void
    {
        var oldValue:Number = _internal_ASSISTANT_FULLSALARY;
        if (isNaN(_internal_ASSISTANT_FULLSALARY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_ASSISTANT_FULLSALARY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ASSISTANT_FULLSALARY", oldValue, _internal_ASSISTANT_FULLSALARY));
        }
    }

    public function set TREATMENTPROC_CALCTIME(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_CALCTIME;
        if (isNaN(_internal_TREATMENTPROC_CALCTIME) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_CALCTIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_CALCTIME", oldValue, _internal_TREATMENTPROC_CALCTIME));
        }
    }

    public function set PATIENT_ID(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_ID;
        if (oldValue !== value)
        {
            _internal_PATIENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_ID", oldValue, _internal_PATIENT_ID));
        }
    }

    public function set PATIENT_FULLNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_FULLNAME;
        if (oldValue !== value)
        {
            _internal_PATIENT_FULLNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_FULLNAME", oldValue, _internal_PATIENT_FULLNAME));
        }
    }

    public function set PATIENT_BIRTHDAY(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_BIRTHDAY;
        if (oldValue !== value)
        {
            _internal_PATIENT_BIRTHDAY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_BIRTHDAY", oldValue, _internal_PATIENT_BIRTHDAY));
        }
    }

    public function set PATIENT_AGE(value:int) : void
    {
        var oldValue:int = _internal_PATIENT_AGE;
        if (oldValue !== value)
        {
            _internal_PATIENT_AGE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_AGE", oldValue, _internal_PATIENT_AGE));
        }
    }

    public function set PATIENT_SEX(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_SEX;
        if (oldValue !== value)
        {
            _internal_PATIENT_SEX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_SEX", oldValue, _internal_PATIENT_SEX));
        }
    }

    public function set PATIENT_VILLAGEORCITY(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_VILLAGEORCITY;
        if (oldValue !== value)
        {
            _internal_PATIENT_VILLAGEORCITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_VILLAGEORCITY", oldValue, _internal_PATIENT_VILLAGEORCITY));
        }
    }

    public function set PATIENT_POPULATIONGROUPE(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_POPULATIONGROUPE;
        if (oldValue !== value)
        {
            _internal_PATIENT_POPULATIONGROUPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_POPULATIONGROUPE", oldValue, _internal_PATIENT_POPULATIONGROUPE));
        }
    }

    public function set PATIENT_ISDISPENSARY(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_ISDISPENSARY;
        if (oldValue !== value)
        {
            _internal_PATIENT_ISDISPENSARY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_ISDISPENSARY", oldValue, _internal_PATIENT_ISDISPENSARY));
        }
    }

    public function set PATIENT_DISCOUNT_AMOUNT(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_DISCOUNT_AMOUNT;
        if (oldValue !== value)
        {
            _internal_PATIENT_DISCOUNT_AMOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_DISCOUNT_AMOUNT", oldValue, _internal_PATIENT_DISCOUNT_AMOUNT));
        }
    }

    public function set PATIENT_VISIT_ID(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_VISIT_ID;
        if (oldValue !== value)
        {
            _internal_PATIENT_VISIT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_VISIT_ID", oldValue, _internal_PATIENT_VISIT_ID));
        }
    }

    public function set PATIENT_VISIT(value:int) : void
    {
        var oldValue:int = _internal_PATIENT_VISIT;
        if (oldValue !== value)
        {
            _internal_PATIENT_VISIT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_VISIT", oldValue, _internal_PATIENT_VISIT));
        }
    }

    public function set PATIENT_COUNT(value:int) : void
    {
        var oldValue:int = _internal_PATIENT_COUNT;
        if (oldValue !== value)
        {
            _internal_PATIENT_COUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_COUNT", oldValue, _internal_PATIENT_COUNT));
        }
    }

    public function set WHEREFROM_ID(value:String) : void
    {
        var oldValue:String = _internal_WHEREFROM_ID;
        if (oldValue !== value)
        {
            _internal_WHEREFROM_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WHEREFROM_ID", oldValue, _internal_WHEREFROM_ID));
        }
    }

    public function set WHEREFROM_DESCRIPTION(value:String) : void
    {
        var oldValue:String = _internal_WHEREFROM_DESCRIPTION;
        if (oldValue !== value)
        {
            _internal_WHEREFROM_DESCRIPTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WHEREFROM_DESCRIPTION", oldValue, _internal_WHEREFROM_DESCRIPTION));
        }
    }

    public function set GROUP_ID(value:String) : void
    {
        var oldValue:String = _internal_GROUP_ID;
        if (oldValue !== value)
        {
            _internal_GROUP_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "GROUP_ID", oldValue, _internal_GROUP_ID));
        }
    }

    public function set GROUP_DESCRIPTION(value:String) : void
    {
        var oldValue:String = _internal_GROUP_DESCRIPTION;
        if (oldValue !== value)
        {
            _internal_GROUP_DESCRIPTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "GROUP_DESCRIPTION", oldValue, _internal_GROUP_DESCRIPTION));
        }
    }

    public function set HEALTHPROC_ID(value:String) : void
    {
        var oldValue:String = _internal_HEALTHPROC_ID;
        if (oldValue !== value)
        {
            _internal_HEALTHPROC_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROC_ID", oldValue, _internal_HEALTHPROC_ID));
        }
    }

    public function set HEALTHPROC_NAME(value:String) : void
    {
        var oldValue:String = _internal_HEALTHPROC_NAME;
        if (oldValue !== value)
        {
            _internal_HEALTHPROC_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROC_NAME", oldValue, _internal_HEALTHPROC_NAME));
        }
    }

    public function set HEALTHPROC_PROCTYPE(value:String) : void
    {
        var oldValue:String = _internal_HEALTHPROC_PROCTYPE;
        if (oldValue !== value)
        {
            _internal_HEALTHPROC_PROCTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROC_PROCTYPE", oldValue, _internal_HEALTHPROC_PROCTYPE));
        }
    }

    public function set HEALTHPROC_PRICE(value:Number) : void
    {
        var oldValue:Number = _internal_HEALTHPROC_PRICE;
        if (isNaN(_internal_HEALTHPROC_PRICE) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_HEALTHPROC_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROC_PRICE", oldValue, _internal_HEALTHPROC_PRICE));
        }
    }

    public function set INVOICES_ID(value:String) : void
    {
        var oldValue:String = _internal_INVOICES_ID;
        if (oldValue !== value)
        {
            _internal_INVOICES_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INVOICES_ID", oldValue, _internal_INVOICES_ID));
        }
    }

    public function set INVOICES_INVOICENUMBER(value:String) : void
    {
        var oldValue:String = _internal_INVOICES_INVOICENUMBER;
        if (oldValue !== value)
        {
            _internal_INVOICES_INVOICENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INVOICES_INVOICENUMBER", oldValue, _internal_INVOICES_INVOICENUMBER));
        }
    }

    public function set INVOICES_CREATEDATE(value:String) : void
    {
        var oldValue:String = _internal_INVOICES_CREATEDATE;
        if (oldValue !== value)
        {
            _internal_INVOICES_CREATEDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INVOICES_CREATEDATE", oldValue, _internal_INVOICES_CREATEDATE));
        }
    }

    public function set INVOICES_PAIDINCASH(value:Number) : void
    {
        var oldValue:Number = _internal_INVOICES_PAIDINCASH;
        if (isNaN(_internal_INVOICES_PAIDINCASH) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_INVOICES_PAIDINCASH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INVOICES_PAIDINCASH", oldValue, _internal_INVOICES_PAIDINCASH));
        }
    }

    public function set TREATMENTPROC_DISCOUNT_AMOUNT(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROC_DISCOUNT_AMOUNT;
        if (isNaN(_internal_TREATMENTPROC_DISCOUNT_AMOUNT) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROC_DISCOUNT_AMOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_DISCOUNT_AMOUNT", oldValue, _internal_TREATMENTPROC_DISCOUNT_AMOUNT));
        }
    }

    public function set ACCOUNTFLOW_ID(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNTFLOW_ID;
        if (oldValue !== value)
        {
            _internal_ACCOUNTFLOW_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNTFLOW_ID", oldValue, _internal_ACCOUNTFLOW_ID));
        }
    }

    public function set ACCOUNTFLOW_ORDERNUMBER(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNTFLOW_ORDERNUMBER;
        if (oldValue !== value)
        {
            _internal_ACCOUNTFLOW_ORDERNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNTFLOW_ORDERNUMBER", oldValue, _internal_ACCOUNTFLOW_ORDERNUMBER));
        }
    }

    public function set ACCOUNTFLOW_SUMM(value:Number) : void
    {
        var oldValue:Number = _internal_ACCOUNTFLOW_SUMM;
        if (isNaN(_internal_ACCOUNTFLOW_SUMM) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_ACCOUNTFLOW_SUMM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNTFLOW_SUMM", oldValue, _internal_ACCOUNTFLOW_SUMM));
        }
    }

    public function set ACCOUNTFLOW_OPERATIONTIMESTAMP(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNTFLOW_OPERATIONTIMESTAMP;
        if (oldValue !== value)
        {
            _internal_ACCOUNTFLOW_OPERATIONTIMESTAMP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNTFLOW_OPERATIONTIMESTAMP", oldValue, _internal_ACCOUNTFLOW_OPERATIONTIMESTAMP));
        }
    }

    public function set INVOICES_CREATE_DAY(value:String) : void
    {
        var oldValue:String = _internal_INVOICES_CREATE_DAY;
        if (oldValue !== value)
        {
            _internal_INVOICES_CREATE_DAY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INVOICES_CREATE_DAY", oldValue, _internal_INVOICES_CREATE_DAY));
        }
    }

    public function set ACCOUNTFLOW_OPERATIONDAY(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNTFLOW_OPERATIONDAY;
        if (oldValue !== value)
        {
            _internal_ACCOUNTFLOW_OPERATIONDAY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNTFLOW_OPERATIONDAY", oldValue, _internal_ACCOUNTFLOW_OPERATIONDAY));
        }
    }

    public function set PATIENTSINSURANCE_ID(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSINSURANCE_ID;
        if (oldValue !== value)
        {
            _internal_PATIENTSINSURANCE_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSINSURANCE_ID", oldValue, _internal_PATIENTSINSURANCE_ID));
        }
    }

    public function set PATIENTSINSURANCE_POLICENUMBER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSINSURANCE_POLICENUMBER;
        if (oldValue !== value)
        {
            _internal_PATIENTSINSURANCE_POLICENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSINSURANCE_POLICENUMBER", oldValue, _internal_PATIENTSINSURANCE_POLICENUMBER));
        }
    }

    public function set PATIENTSINSURANCE_POLICETODATE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSINSURANCE_POLICETODATE;
        if (oldValue !== value)
        {
            _internal_PATIENTSINSURANCE_POLICETODATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSINSURANCE_POLICETODATE", oldValue, _internal_PATIENTSINSURANCE_POLICETODATE));
        }
    }

    public function set PATIENTSINSURANCE_POLICEFROMDATE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSINSURANCE_POLICEFROMDATE;
        if (oldValue !== value)
        {
            _internal_PATIENTSINSURANCE_POLICEFROMDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSINSURANCE_POLICEFROMDATE", oldValue, _internal_PATIENTSINSURANCE_POLICEFROMDATE));
        }
    }

    public function set INSURANCECOMPANYS_ID(value:String) : void
    {
        var oldValue:String = _internal_INSURANCECOMPANYS_ID;
        if (oldValue !== value)
        {
            _internal_INSURANCECOMPANYS_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INSURANCECOMPANYS_ID", oldValue, _internal_INSURANCECOMPANYS_ID));
        }
    }

    public function set INSURANCECOMPANYS_NAME(value:String) : void
    {
        var oldValue:String = _internal_INSURANCECOMPANYS_NAME;
        if (oldValue !== value)
        {
            _internal_INSURANCECOMPANYS_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INSURANCECOMPANYS_NAME", oldValue, _internal_INSURANCECOMPANYS_NAME));
        }
    }

    public function set COUNT(value:int) : void
    {
        var oldValue:int = _internal_COUNT;
        if (oldValue !== value)
        {
            _internal_COUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNT", oldValue, _internal_COUNT));
        }
    }

    public function set isOpen(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOpen;
        if (oldValue !== value)
        {
            _internal_isOpen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOpen", oldValue, _internal_isOpen));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefOlapModuleRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefOlapModuleRecordEntityMetadata) : void
    {
        var oldValue : _ChiefOlapModuleRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
