/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HealthPlanMKB10MKBProtocol.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.HealthPlanMKB10F43;
import valueObjects.HealthPlanMKB10MKB;
import valueObjects.HealthPlanMKB10Template;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HealthPlanMKB10MKBProtocol extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HealthPlanMKB10MKBProtocol") == null)
            {
                flash.net.registerClassAlias("HealthPlanMKB10MKBProtocol", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HealthPlanMKB10MKBProtocol", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HealthPlanMKB10MKB.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10F43.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Template.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10MKBProtocol.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _HealthPlanMKB10MKBProtocolEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : int;
    private var _internal_NAME : String;
    private var _internal_healthtype : Number;
    private var _internal_MKB10 : valueObjects.HealthPlanMKB10MKB;
    private var _internal_F43 : int;
    private var _internal_F43list : ArrayCollection;
    model_internal var _internal_F43list_leaf:valueObjects.HealthPlanMKB10F43;
    private var _internal_PROTOCOLTXT : String;
    private var _internal_Templates : ArrayCollection;
    model_internal var _internal_Templates_leaf:valueObjects.HealthPlanMKB10Template;
    private var _internal_ANAMNESTXT : String;
    private var _internal_STATUSTXT : String;
    private var _internal_RECOMENDTXT : String;
    private var _internal_RENTGENTXT : String;
    private var _internal_EPICRISISTXT : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HealthPlanMKB10MKBProtocol()
    {
        _model = new _HealthPlanMKB10MKBProtocolEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : int
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get healthtype() : Number
    {
        return _internal_healthtype;
    }

    [Bindable(event="propertyChange")]
    public function get MKB10() : valueObjects.HealthPlanMKB10MKB
    {
        return _internal_MKB10;
    }

    [Bindable(event="propertyChange")]
    public function get F43() : int
    {
        return _internal_F43;
    }

    [Bindable(event="propertyChange")]
    public function get F43list() : ArrayCollection
    {
        return _internal_F43list;
    }

    [Bindable(event="propertyChange")]
    public function get PROTOCOLTXT() : String
    {
        return _internal_PROTOCOLTXT;
    }

    [Bindable(event="propertyChange")]
    public function get Templates() : ArrayCollection
    {
        return _internal_Templates;
    }

    [Bindable(event="propertyChange")]
    public function get ANAMNESTXT() : String
    {
        return _internal_ANAMNESTXT;
    }

    [Bindable(event="propertyChange")]
    public function get STATUSTXT() : String
    {
        return _internal_STATUSTXT;
    }

    [Bindable(event="propertyChange")]
    public function get RECOMENDTXT() : String
    {
        return _internal_RECOMENDTXT;
    }

    [Bindable(event="propertyChange")]
    public function get RENTGENTXT() : String
    {
        return _internal_RENTGENTXT;
    }

    [Bindable(event="propertyChange")]
    public function get EPICRISISTXT() : String
    {
        return _internal_EPICRISISTXT;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:int) : void
    {
        var oldValue:int = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set healthtype(value:Number) : void
    {
        var oldValue:Number = _internal_healthtype;
        if (isNaN(_internal_healthtype) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_healthtype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "healthtype", oldValue, _internal_healthtype));
        }
    }

    public function set MKB10(value:valueObjects.HealthPlanMKB10MKB) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10MKB = _internal_MKB10;
        if (oldValue !== value)
        {
            _internal_MKB10 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MKB10", oldValue, _internal_MKB10));
        }
    }

    public function set F43(value:int) : void
    {
        var oldValue:int = _internal_F43;
        if (oldValue !== value)
        {
            _internal_F43 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "F43", oldValue, _internal_F43));
        }
    }

    public function set F43list(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_F43list;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_F43list = value;
            }
            else if (value is Array)
            {
                _internal_F43list = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_F43list = null;
            }
            else
            {
                throw new Error("value of F43list must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "F43list", oldValue, _internal_F43list));
        }
    }

    public function set PROTOCOLTXT(value:String) : void
    {
        var oldValue:String = _internal_PROTOCOLTXT;
        if (oldValue !== value)
        {
            _internal_PROTOCOLTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PROTOCOLTXT", oldValue, _internal_PROTOCOLTXT));
        }
    }

    public function set Templates(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_Templates;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_Templates = value;
            }
            else if (value is Array)
            {
                _internal_Templates = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_Templates = null;
            }
            else
            {
                throw new Error("value of Templates must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Templates", oldValue, _internal_Templates));
        }
    }

    public function set ANAMNESTXT(value:String) : void
    {
        var oldValue:String = _internal_ANAMNESTXT;
        if (oldValue !== value)
        {
            _internal_ANAMNESTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ANAMNESTXT", oldValue, _internal_ANAMNESTXT));
        }
    }

    public function set STATUSTXT(value:String) : void
    {
        var oldValue:String = _internal_STATUSTXT;
        if (oldValue !== value)
        {
            _internal_STATUSTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUSTXT", oldValue, _internal_STATUSTXT));
        }
    }

    public function set RECOMENDTXT(value:String) : void
    {
        var oldValue:String = _internal_RECOMENDTXT;
        if (oldValue !== value)
        {
            _internal_RECOMENDTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RECOMENDTXT", oldValue, _internal_RECOMENDTXT));
        }
    }

    public function set RENTGENTXT(value:String) : void
    {
        var oldValue:String = _internal_RENTGENTXT;
        if (oldValue !== value)
        {
            _internal_RENTGENTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RENTGENTXT", oldValue, _internal_RENTGENTXT));
        }
    }

    public function set EPICRISISTXT(value:String) : void
    {
        var oldValue:String = _internal_EPICRISISTXT;
        if (oldValue !== value)
        {
            _internal_EPICRISISTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EPICRISISTXT", oldValue, _internal_EPICRISISTXT));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HealthPlanMKB10MKBProtocolEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HealthPlanMKB10MKBProtocolEntityMetadata) : void
    {
        var oldValue : _HealthPlanMKB10MKBProtocolEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
