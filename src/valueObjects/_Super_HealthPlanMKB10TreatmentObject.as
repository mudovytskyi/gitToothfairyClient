/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HealthPlanMKB10TreatmentObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.HealthPlanMKB10Diagnos2;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HealthPlanMKB10TreatmentObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HealthPlanMKB10TreatmentObject") == null)
            {
                flash.net.registerClassAlias("HealthPlanMKB10TreatmentObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HealthPlanMKB10TreatmentObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HealthPlanMKB10Diagnos2.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _HealthPlanMKB10TreatmentObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : int;
    private var _internal_Diagnos : valueObjects.HealthPlanMKB10Diagnos2;
    private var _internal_TOOTH : int;
    private var _internal_TOOTHSIDE : String;
    private var _internal_TOOTHROOT : String;
    private var _internal_TOOTHCHANNEL : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HealthPlanMKB10TreatmentObject()
    {
        _model = new _HealthPlanMKB10TreatmentObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : int
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get Diagnos() : valueObjects.HealthPlanMKB10Diagnos2
    {
        return _internal_Diagnos;
    }

    [Bindable(event="propertyChange")]
    public function get TOOTH() : int
    {
        return _internal_TOOTH;
    }

    [Bindable(event="propertyChange")]
    public function get TOOTHSIDE() : String
    {
        return _internal_TOOTHSIDE;
    }

    [Bindable(event="propertyChange")]
    public function get TOOTHROOT() : String
    {
        return _internal_TOOTHROOT;
    }

    [Bindable(event="propertyChange")]
    public function get TOOTHCHANNEL() : String
    {
        return _internal_TOOTHCHANNEL;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:int) : void
    {
        var oldValue:int = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set Diagnos(value:valueObjects.HealthPlanMKB10Diagnos2) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10Diagnos2 = _internal_Diagnos;
        if (oldValue !== value)
        {
            _internal_Diagnos = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Diagnos", oldValue, _internal_Diagnos));
        }
    }

    public function set TOOTH(value:int) : void
    {
        var oldValue:int = _internal_TOOTH;
        if (oldValue !== value)
        {
            _internal_TOOTH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TOOTH", oldValue, _internal_TOOTH));
        }
    }

    public function set TOOTHSIDE(value:String) : void
    {
        var oldValue:String = _internal_TOOTHSIDE;
        if (oldValue !== value)
        {
            _internal_TOOTHSIDE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TOOTHSIDE", oldValue, _internal_TOOTHSIDE));
        }
    }

    public function set TOOTHROOT(value:String) : void
    {
        var oldValue:String = _internal_TOOTHROOT;
        if (oldValue !== value)
        {
            _internal_TOOTHROOT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TOOTHROOT", oldValue, _internal_TOOTHROOT));
        }
    }

    public function set TOOTHCHANNEL(value:String) : void
    {
        var oldValue:String = _internal_TOOTHCHANNEL;
        if (oldValue !== value)
        {
            _internal_TOOTHCHANNEL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TOOTHCHANNEL", oldValue, _internal_TOOTHCHANNEL));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HealthPlanMKB10TreatmentObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HealthPlanMKB10TreatmentObjectEntityMetadata) : void
    {
        var oldValue : _HealthPlanMKB10TreatmentObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
