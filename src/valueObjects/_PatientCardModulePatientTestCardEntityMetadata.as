
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _PatientCardModulePatientTestCardEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ID", "PATIENTID", "TESTDATE", "CARDIACDISEASE", "KIDNEYDISEASE", "LIVERDISEASE", "OTHERDISEASE", "ARTERIALPRESSURE", "RHEUMATISM", "HEPATITIS", "HEPATITISDATE", "HEPATITISTYPE", "EPILEPSIA", "DIABETES", "FIT", "BLEEDING", "ALLERGYFLAG", "ALLERGY", "PREGNANCY", "MEDICATIONS", "BLOODGROUP", "RHESUSFACTOR", "MOUTHULCER", "FUNGUS", "FEVER", "THROATACHE", "LYMPHNODES", "REDSKINAREA", "HYPERHIDROSIS", "DIARRHEA", "AMNESIA", "WEIGHTLOSS", "HEADACHE", "AIDS", "WORKWITHBLOOD", "NARCOMANIA");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ID", "PATIENTID", "TESTDATE", "CARDIACDISEASE", "KIDNEYDISEASE", "LIVERDISEASE", "OTHERDISEASE", "ARTERIALPRESSURE", "RHEUMATISM", "HEPATITIS", "HEPATITISDATE", "HEPATITISTYPE", "EPILEPSIA", "DIABETES", "FIT", "BLEEDING", "ALLERGYFLAG", "ALLERGY", "PREGNANCY", "MEDICATIONS", "BLOODGROUP", "RHESUSFACTOR", "MOUTHULCER", "FUNGUS", "FEVER", "THROATACHE", "LYMPHNODES", "REDSKINAREA", "HYPERHIDROSIS", "DIARRHEA", "AMNESIA", "WEIGHTLOSS", "HEADACHE", "AIDS", "WORKWITHBLOOD", "NARCOMANIA");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ID", "PATIENTID", "TESTDATE", "CARDIACDISEASE", "KIDNEYDISEASE", "LIVERDISEASE", "OTHERDISEASE", "ARTERIALPRESSURE", "RHEUMATISM", "HEPATITIS", "HEPATITISDATE", "HEPATITISTYPE", "EPILEPSIA", "DIABETES", "FIT", "BLEEDING", "ALLERGYFLAG", "ALLERGY", "PREGNANCY", "MEDICATIONS", "BLOODGROUP", "RHESUSFACTOR", "MOUTHULCER", "FUNGUS", "FEVER", "THROATACHE", "LYMPHNODES", "REDSKINAREA", "HYPERHIDROSIS", "DIARRHEA", "AMNESIA", "WEIGHTLOSS", "HEADACHE", "AIDS", "WORKWITHBLOOD", "NARCOMANIA");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ID", "PATIENTID", "TESTDATE", "CARDIACDISEASE", "KIDNEYDISEASE", "LIVERDISEASE", "OTHERDISEASE", "ARTERIALPRESSURE", "RHEUMATISM", "HEPATITIS", "HEPATITISDATE", "HEPATITISTYPE", "EPILEPSIA", "DIABETES", "FIT", "BLEEDING", "ALLERGYFLAG", "ALLERGY", "PREGNANCY", "MEDICATIONS", "BLOODGROUP", "RHESUSFACTOR", "MOUTHULCER", "FUNGUS", "FEVER", "THROATACHE", "LYMPHNODES", "REDSKINAREA", "HYPERHIDROSIS", "DIARRHEA", "AMNESIA", "WEIGHTLOSS", "HEADACHE", "AIDS", "WORKWITHBLOOD", "NARCOMANIA");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "PatientCardModulePatientTestCard";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_PatientCardModulePatientTestCard;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _PatientCardModulePatientTestCardEntityMetadata(value : _Super_PatientCardModulePatientTestCard)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["PATIENTID"] = new Array();
            model_internal::dependentsOnMap["TESTDATE"] = new Array();
            model_internal::dependentsOnMap["CARDIACDISEASE"] = new Array();
            model_internal::dependentsOnMap["KIDNEYDISEASE"] = new Array();
            model_internal::dependentsOnMap["LIVERDISEASE"] = new Array();
            model_internal::dependentsOnMap["OTHERDISEASE"] = new Array();
            model_internal::dependentsOnMap["ARTERIALPRESSURE"] = new Array();
            model_internal::dependentsOnMap["RHEUMATISM"] = new Array();
            model_internal::dependentsOnMap["HEPATITIS"] = new Array();
            model_internal::dependentsOnMap["HEPATITISDATE"] = new Array();
            model_internal::dependentsOnMap["HEPATITISTYPE"] = new Array();
            model_internal::dependentsOnMap["EPILEPSIA"] = new Array();
            model_internal::dependentsOnMap["DIABETES"] = new Array();
            model_internal::dependentsOnMap["FIT"] = new Array();
            model_internal::dependentsOnMap["BLEEDING"] = new Array();
            model_internal::dependentsOnMap["ALLERGYFLAG"] = new Array();
            model_internal::dependentsOnMap["ALLERGY"] = new Array();
            model_internal::dependentsOnMap["PREGNANCY"] = new Array();
            model_internal::dependentsOnMap["MEDICATIONS"] = new Array();
            model_internal::dependentsOnMap["BLOODGROUP"] = new Array();
            model_internal::dependentsOnMap["RHESUSFACTOR"] = new Array();
            model_internal::dependentsOnMap["MOUTHULCER"] = new Array();
            model_internal::dependentsOnMap["FUNGUS"] = new Array();
            model_internal::dependentsOnMap["FEVER"] = new Array();
            model_internal::dependentsOnMap["THROATACHE"] = new Array();
            model_internal::dependentsOnMap["LYMPHNODES"] = new Array();
            model_internal::dependentsOnMap["REDSKINAREA"] = new Array();
            model_internal::dependentsOnMap["HYPERHIDROSIS"] = new Array();
            model_internal::dependentsOnMap["DIARRHEA"] = new Array();
            model_internal::dependentsOnMap["AMNESIA"] = new Array();
            model_internal::dependentsOnMap["WEIGHTLOSS"] = new Array();
            model_internal::dependentsOnMap["HEADACHE"] = new Array();
            model_internal::dependentsOnMap["AIDS"] = new Array();
            model_internal::dependentsOnMap["WORKWITHBLOOD"] = new Array();
            model_internal::dependentsOnMap["NARCOMANIA"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ID"] = "String";
        model_internal::propertyTypeMap["PATIENTID"] = "int";
        model_internal::propertyTypeMap["TESTDATE"] = "Object";
        model_internal::propertyTypeMap["CARDIACDISEASE"] = "int";
        model_internal::propertyTypeMap["KIDNEYDISEASE"] = "int";
        model_internal::propertyTypeMap["LIVERDISEASE"] = "int";
        model_internal::propertyTypeMap["OTHERDISEASE"] = "int";
        model_internal::propertyTypeMap["ARTERIALPRESSURE"] = "int";
        model_internal::propertyTypeMap["RHEUMATISM"] = "int";
        model_internal::propertyTypeMap["HEPATITIS"] = "int";
        model_internal::propertyTypeMap["HEPATITISDATE"] = "String";
        model_internal::propertyTypeMap["HEPATITISTYPE"] = "int";
        model_internal::propertyTypeMap["EPILEPSIA"] = "int";
        model_internal::propertyTypeMap["DIABETES"] = "int";
        model_internal::propertyTypeMap["FIT"] = "int";
        model_internal::propertyTypeMap["BLEEDING"] = "int";
        model_internal::propertyTypeMap["ALLERGYFLAG"] = "int";
        model_internal::propertyTypeMap["ALLERGY"] = "String";
        model_internal::propertyTypeMap["PREGNANCY"] = "int";
        model_internal::propertyTypeMap["MEDICATIONS"] = "String";
        model_internal::propertyTypeMap["BLOODGROUP"] = "int";
        model_internal::propertyTypeMap["RHESUSFACTOR"] = "int";
        model_internal::propertyTypeMap["MOUTHULCER"] = "int";
        model_internal::propertyTypeMap["FUNGUS"] = "int";
        model_internal::propertyTypeMap["FEVER"] = "int";
        model_internal::propertyTypeMap["THROATACHE"] = "int";
        model_internal::propertyTypeMap["LYMPHNODES"] = "int";
        model_internal::propertyTypeMap["REDSKINAREA"] = "int";
        model_internal::propertyTypeMap["HYPERHIDROSIS"] = "int";
        model_internal::propertyTypeMap["DIARRHEA"] = "int";
        model_internal::propertyTypeMap["AMNESIA"] = "int";
        model_internal::propertyTypeMap["WEIGHTLOSS"] = "int";
        model_internal::propertyTypeMap["HEADACHE"] = "int";
        model_internal::propertyTypeMap["AIDS"] = "int";
        model_internal::propertyTypeMap["WORKWITHBLOOD"] = "int";
        model_internal::propertyTypeMap["NARCOMANIA"] = "int";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity PatientCardModulePatientTestCard");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity PatientCardModulePatientTestCard");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of PatientCardModulePatientTestCard");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardModulePatientTestCard");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity PatientCardModulePatientTestCard");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardModulePatientTestCard");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTESTDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDIACDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isKIDNEYDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLIVERDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOTHERDISEASEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isARTERIALPRESSUREAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRHEUMATISMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEPATITISAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEPATITISDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEPATITISTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEPILEPSIAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDIABETESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFITAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBLEEDINGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isALLERGYFLAGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isALLERGYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPREGNANCYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMEDICATIONSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBLOODGROUPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRHESUSFACTORAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOUTHULCERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFUNGUSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFEVERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTHROATACHEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLYMPHNODESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isREDSKINAREAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHYPERHIDROSISAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDIARRHEAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAMNESIAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWEIGHTLOSSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEADACHEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAIDSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWORKWITHBLOODAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNARCOMANIAAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TESTDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDIACDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get KIDNEYDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LIVERDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get OTHERDISEASEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ARTERIALPRESSUREStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get RHEUMATISMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEPATITISStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEPATITISDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEPATITISTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EPILEPSIAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DIABETESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FITStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BLEEDINGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ALLERGYFLAGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ALLERGYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PREGNANCYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MEDICATIONSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BLOODGROUPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get RHESUSFACTORStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOUTHULCERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FUNGUSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FEVERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get THROATACHEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LYMPHNODESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get REDSKINAREAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HYPERHIDROSISStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DIARRHEAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AMNESIAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WEIGHTLOSSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEADACHEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AIDSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WORKWITHBLOODStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NARCOMANIAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
