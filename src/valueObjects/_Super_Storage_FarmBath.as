/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Storage_FarmBath.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Storage_FarmBath extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("Storage_FarmBath") == null)
            {
                flash.net.registerClassAlias("Storage_FarmBath", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("Storage_FarmBath", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _Storage_FarmBathEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_bathtype : String;
    private var _internal_farm_fk : String;
    private var _internal_farm_name : String;
    private var _internal_farm_baseunit : String;
    private var _internal_farmdoc_fk : String;
    private var _internal_quantity : Number;
    private var _internal_price : Number;
    private var _internal_createtimestamp : String;
    private var _internal_treatprcfarm_fk : String;
    private var _internal_subdivision_fk : String;
    private var _internal_author_fk : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Storage_FarmBath()
    {
        _model = new _Storage_FarmBathEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get bathtype() : String
    {
        return _internal_bathtype;
    }

    [Bindable(event="propertyChange")]
    public function get farm_fk() : String
    {
        return _internal_farm_fk;
    }

    [Bindable(event="propertyChange")]
    public function get farm_name() : String
    {
        return _internal_farm_name;
    }

    [Bindable(event="propertyChange")]
    public function get farm_baseunit() : String
    {
        return _internal_farm_baseunit;
    }

    [Bindable(event="propertyChange")]
    public function get farmdoc_fk() : String
    {
        return _internal_farmdoc_fk;
    }

    [Bindable(event="propertyChange")]
    public function get quantity() : Number
    {
        return _internal_quantity;
    }

    [Bindable(event="propertyChange")]
    public function get price() : Number
    {
        return _internal_price;
    }

    [Bindable(event="propertyChange")]
    public function get createtimestamp() : String
    {
        return _internal_createtimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get treatprcfarm_fk() : String
    {
        return _internal_treatprcfarm_fk;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_fk() : String
    {
        return _internal_subdivision_fk;
    }

    [Bindable(event="propertyChange")]
    public function get author_fk() : String
    {
        return _internal_author_fk;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set bathtype(value:String) : void
    {
        var oldValue:String = _internal_bathtype;
        if (oldValue !== value)
        {
            _internal_bathtype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "bathtype", oldValue, _internal_bathtype));
        }
    }

    public function set farm_fk(value:String) : void
    {
        var oldValue:String = _internal_farm_fk;
        if (oldValue !== value)
        {
            _internal_farm_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_fk", oldValue, _internal_farm_fk));
        }
    }

    public function set farm_name(value:String) : void
    {
        var oldValue:String = _internal_farm_name;
        if (oldValue !== value)
        {
            _internal_farm_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_name", oldValue, _internal_farm_name));
        }
    }

    public function set farm_baseunit(value:String) : void
    {
        var oldValue:String = _internal_farm_baseunit;
        if (oldValue !== value)
        {
            _internal_farm_baseunit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_baseunit", oldValue, _internal_farm_baseunit));
        }
    }

    public function set farmdoc_fk(value:String) : void
    {
        var oldValue:String = _internal_farmdoc_fk;
        if (oldValue !== value)
        {
            _internal_farmdoc_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmdoc_fk", oldValue, _internal_farmdoc_fk));
        }
    }

    public function set quantity(value:Number) : void
    {
        var oldValue:Number = _internal_quantity;
        if (isNaN(_internal_quantity) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_quantity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "quantity", oldValue, _internal_quantity));
        }
    }

    public function set price(value:Number) : void
    {
        var oldValue:Number = _internal_price;
        if (isNaN(_internal_price) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_price = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "price", oldValue, _internal_price));
        }
    }

    public function set createtimestamp(value:String) : void
    {
        var oldValue:String = _internal_createtimestamp;
        if (oldValue !== value)
        {
            _internal_createtimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "createtimestamp", oldValue, _internal_createtimestamp));
        }
    }

    public function set treatprcfarm_fk(value:String) : void
    {
        var oldValue:String = _internal_treatprcfarm_fk;
        if (oldValue !== value)
        {
            _internal_treatprcfarm_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatprcfarm_fk", oldValue, _internal_treatprcfarm_fk));
        }
    }

    public function set subdivision_fk(value:String) : void
    {
        var oldValue:String = _internal_subdivision_fk;
        if (oldValue !== value)
        {
            _internal_subdivision_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_fk", oldValue, _internal_subdivision_fk));
        }
    }

    public function set author_fk(value:String) : void
    {
        var oldValue:String = _internal_author_fk;
        if (oldValue !== value)
        {
            _internal_author_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_fk", oldValue, _internal_author_fk));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _Storage_FarmBathEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _Storage_FarmBathEntityMetadata) : void
    {
        var oldValue : _Storage_FarmBathEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
