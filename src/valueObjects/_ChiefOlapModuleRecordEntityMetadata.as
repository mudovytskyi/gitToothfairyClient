
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _ChiefOlapModuleRecordEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("FARM_ID", "FARM_NAME", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "HEALTHPROCFARM_QUANTITY", "TREATMENTPROC_ID", "TREATMENTPROC_NAME", "TREATMENTPROC_SHIFR", "TREATMENTPROC_HEALTHTYPE", "TREATMENTPROC_UOP", "TREATMENTPROC_PRICE", "TREATMENTPROC_PRICE_DISCOUNT", "TREATMENTPROC_PRICE_DISCOUNT_SUMM", "TREATMENTPROC_DATECLOSE", "TREATMENTPROC_DAYCLOSE", "TREATMENTPROC_EXPENSES", "TREATMENTPROC_PROC_COUNT", "TREATMENTPROC_EXPENSES_FACT", "TREATMENTPROC_EXPENSES_DELTA", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "DOCTOR_PERCENT", "DOCTOR_BASESALARY", "DOCTOR_PERCENTSUMM", "DOCTOR_FULLSALARY", "ASSISTANT_BASESALARY", "ASSISTANT_PERCENTSUMM", "ASSISTANT_FULLSALARY", "TREATMENTPROC_CALCTIME", "PATIENT_ID", "PATIENT_FULLNAME", "PATIENT_BIRTHDAY", "PATIENT_AGE", "PATIENT_SEX", "PATIENT_VILLAGEORCITY", "PATIENT_POPULATIONGROUPE", "PATIENT_ISDISPENSARY", "PATIENT_DISCOUNT_AMOUNT", "PATIENT_VISIT_ID", "PATIENT_VISIT", "PATIENT_COUNT", "WHEREFROM_ID", "WHEREFROM_DESCRIPTION", "GROUP_ID", "GROUP_DESCRIPTION", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROC_PROCTYPE", "HEALTHPROC_PRICE", "INVOICES_ID", "INVOICES_INVOICENUMBER", "INVOICES_CREATEDATE", "INVOICES_PAIDINCASH", "TREATMENTPROC_DISCOUNT_AMOUNT", "ACCOUNTFLOW_ID", "ACCOUNTFLOW_ORDERNUMBER", "ACCOUNTFLOW_SUMM", "ACCOUNTFLOW_OPERATIONTIMESTAMP", "INVOICES_CREATE_DAY", "ACCOUNTFLOW_OPERATIONDAY", "PATIENTSINSURANCE_ID", "PATIENTSINSURANCE_POLICENUMBER", "PATIENTSINSURANCE_POLICETODATE", "PATIENTSINSURANCE_POLICEFROMDATE", "INSURANCECOMPANYS_ID", "INSURANCECOMPANYS_NAME", "COUNT", "isOpen");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("FARM_ID", "FARM_NAME", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "HEALTHPROCFARM_QUANTITY", "TREATMENTPROC_ID", "TREATMENTPROC_NAME", "TREATMENTPROC_SHIFR", "TREATMENTPROC_HEALTHTYPE", "TREATMENTPROC_UOP", "TREATMENTPROC_PRICE", "TREATMENTPROC_PRICE_DISCOUNT", "TREATMENTPROC_PRICE_DISCOUNT_SUMM", "TREATMENTPROC_DATECLOSE", "TREATMENTPROC_DAYCLOSE", "TREATMENTPROC_EXPENSES", "TREATMENTPROC_PROC_COUNT", "TREATMENTPROC_EXPENSES_FACT", "TREATMENTPROC_EXPENSES_DELTA", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "DOCTOR_PERCENT", "DOCTOR_BASESALARY", "DOCTOR_PERCENTSUMM", "DOCTOR_FULLSALARY", "ASSISTANT_BASESALARY", "ASSISTANT_PERCENTSUMM", "ASSISTANT_FULLSALARY", "TREATMENTPROC_CALCTIME", "PATIENT_ID", "PATIENT_FULLNAME", "PATIENT_BIRTHDAY", "PATIENT_AGE", "PATIENT_SEX", "PATIENT_VILLAGEORCITY", "PATIENT_POPULATIONGROUPE", "PATIENT_ISDISPENSARY", "PATIENT_DISCOUNT_AMOUNT", "PATIENT_VISIT_ID", "PATIENT_VISIT", "PATIENT_COUNT", "WHEREFROM_ID", "WHEREFROM_DESCRIPTION", "GROUP_ID", "GROUP_DESCRIPTION", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROC_PROCTYPE", "HEALTHPROC_PRICE", "INVOICES_ID", "INVOICES_INVOICENUMBER", "INVOICES_CREATEDATE", "INVOICES_PAIDINCASH", "TREATMENTPROC_DISCOUNT_AMOUNT", "ACCOUNTFLOW_ID", "ACCOUNTFLOW_ORDERNUMBER", "ACCOUNTFLOW_SUMM", "ACCOUNTFLOW_OPERATIONTIMESTAMP", "INVOICES_CREATE_DAY", "ACCOUNTFLOW_OPERATIONDAY", "PATIENTSINSURANCE_ID", "PATIENTSINSURANCE_POLICENUMBER", "PATIENTSINSURANCE_POLICETODATE", "PATIENTSINSURANCE_POLICEFROMDATE", "INSURANCECOMPANYS_ID", "INSURANCECOMPANYS_NAME", "COUNT", "isOpen");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("FARM_ID", "FARM_NAME", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "HEALTHPROCFARM_QUANTITY", "TREATMENTPROC_ID", "TREATMENTPROC_NAME", "TREATMENTPROC_SHIFR", "TREATMENTPROC_HEALTHTYPE", "TREATMENTPROC_UOP", "TREATMENTPROC_PRICE", "TREATMENTPROC_PRICE_DISCOUNT", "TREATMENTPROC_PRICE_DISCOUNT_SUMM", "TREATMENTPROC_DATECLOSE", "TREATMENTPROC_DAYCLOSE", "TREATMENTPROC_EXPENSES", "TREATMENTPROC_PROC_COUNT", "TREATMENTPROC_EXPENSES_FACT", "TREATMENTPROC_EXPENSES_DELTA", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "DOCTOR_PERCENT", "DOCTOR_BASESALARY", "DOCTOR_PERCENTSUMM", "DOCTOR_FULLSALARY", "ASSISTANT_BASESALARY", "ASSISTANT_PERCENTSUMM", "ASSISTANT_FULLSALARY", "TREATMENTPROC_CALCTIME", "PATIENT_ID", "PATIENT_FULLNAME", "PATIENT_BIRTHDAY", "PATIENT_AGE", "PATIENT_SEX", "PATIENT_VILLAGEORCITY", "PATIENT_POPULATIONGROUPE", "PATIENT_ISDISPENSARY", "PATIENT_DISCOUNT_AMOUNT", "PATIENT_VISIT_ID", "PATIENT_VISIT", "PATIENT_COUNT", "WHEREFROM_ID", "WHEREFROM_DESCRIPTION", "GROUP_ID", "GROUP_DESCRIPTION", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROC_PROCTYPE", "HEALTHPROC_PRICE", "INVOICES_ID", "INVOICES_INVOICENUMBER", "INVOICES_CREATEDATE", "INVOICES_PAIDINCASH", "TREATMENTPROC_DISCOUNT_AMOUNT", "ACCOUNTFLOW_ID", "ACCOUNTFLOW_ORDERNUMBER", "ACCOUNTFLOW_SUMM", "ACCOUNTFLOW_OPERATIONTIMESTAMP", "INVOICES_CREATE_DAY", "ACCOUNTFLOW_OPERATIONDAY", "PATIENTSINSURANCE_ID", "PATIENTSINSURANCE_POLICENUMBER", "PATIENTSINSURANCE_POLICETODATE", "PATIENTSINSURANCE_POLICEFROMDATE", "INSURANCECOMPANYS_ID", "INSURANCECOMPANYS_NAME", "COUNT", "isOpen");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("FARM_ID", "FARM_NAME", "TREATMENTPROCFARM_ID", "TREATMENTPROCFARM_QUANTITY", "HEALTHPROCFARM_QUANTITY", "TREATMENTPROC_ID", "TREATMENTPROC_NAME", "TREATMENTPROC_SHIFR", "TREATMENTPROC_HEALTHTYPE", "TREATMENTPROC_UOP", "TREATMENTPROC_PRICE", "TREATMENTPROC_PRICE_DISCOUNT", "TREATMENTPROC_PRICE_DISCOUNT_SUMM", "TREATMENTPROC_DATECLOSE", "TREATMENTPROC_DAYCLOSE", "TREATMENTPROC_EXPENSES", "TREATMENTPROC_PROC_COUNT", "TREATMENTPROC_EXPENSES_FACT", "TREATMENTPROC_EXPENSES_DELTA", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "DOCTOR_PERCENT", "DOCTOR_BASESALARY", "DOCTOR_PERCENTSUMM", "DOCTOR_FULLSALARY", "ASSISTANT_BASESALARY", "ASSISTANT_PERCENTSUMM", "ASSISTANT_FULLSALARY", "TREATMENTPROC_CALCTIME", "PATIENT_ID", "PATIENT_FULLNAME", "PATIENT_BIRTHDAY", "PATIENT_AGE", "PATIENT_SEX", "PATIENT_VILLAGEORCITY", "PATIENT_POPULATIONGROUPE", "PATIENT_ISDISPENSARY", "PATIENT_DISCOUNT_AMOUNT", "PATIENT_VISIT_ID", "PATIENT_VISIT", "PATIENT_COUNT", "WHEREFROM_ID", "WHEREFROM_DESCRIPTION", "GROUP_ID", "GROUP_DESCRIPTION", "HEALTHPROC_ID", "HEALTHPROC_NAME", "HEALTHPROC_PROCTYPE", "HEALTHPROC_PRICE", "INVOICES_ID", "INVOICES_INVOICENUMBER", "INVOICES_CREATEDATE", "INVOICES_PAIDINCASH", "TREATMENTPROC_DISCOUNT_AMOUNT", "ACCOUNTFLOW_ID", "ACCOUNTFLOW_ORDERNUMBER", "ACCOUNTFLOW_SUMM", "ACCOUNTFLOW_OPERATIONTIMESTAMP", "INVOICES_CREATE_DAY", "ACCOUNTFLOW_OPERATIONDAY", "PATIENTSINSURANCE_ID", "PATIENTSINSURANCE_POLICENUMBER", "PATIENTSINSURANCE_POLICETODATE", "PATIENTSINSURANCE_POLICEFROMDATE", "INSURANCECOMPANYS_ID", "INSURANCECOMPANYS_NAME", "COUNT", "isOpen");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "ChiefOlapModuleRecord";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_ChiefOlapModuleRecord;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _ChiefOlapModuleRecordEntityMetadata(value : _Super_ChiefOlapModuleRecord)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["FARM_ID"] = new Array();
            model_internal::dependentsOnMap["FARM_NAME"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROCFARM_ID"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROCFARM_QUANTITY"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROCFARM_QUANTITY"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_ID"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_NAME"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_SHIFR"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_HEALTHTYPE"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_UOP"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_PRICE"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_PRICE_DISCOUNT"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_PRICE_DISCOUNT_SUMM"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_DATECLOSE"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_DAYCLOSE"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_EXPENSES"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_PROC_COUNT"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_EXPENSES_FACT"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_EXPENSES_DELTA"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_ID"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_SHORTNAME"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_SPECIALITY"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_PERCENT"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_BASESALARY"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_PERCENTSUMM"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_FULLSALARY"] = new Array();
            model_internal::dependentsOnMap["ASSISTANT_BASESALARY"] = new Array();
            model_internal::dependentsOnMap["ASSISTANT_PERCENTSUMM"] = new Array();
            model_internal::dependentsOnMap["ASSISTANT_FULLSALARY"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_CALCTIME"] = new Array();
            model_internal::dependentsOnMap["PATIENT_ID"] = new Array();
            model_internal::dependentsOnMap["PATIENT_FULLNAME"] = new Array();
            model_internal::dependentsOnMap["PATIENT_BIRTHDAY"] = new Array();
            model_internal::dependentsOnMap["PATIENT_AGE"] = new Array();
            model_internal::dependentsOnMap["PATIENT_SEX"] = new Array();
            model_internal::dependentsOnMap["PATIENT_VILLAGEORCITY"] = new Array();
            model_internal::dependentsOnMap["PATIENT_POPULATIONGROUPE"] = new Array();
            model_internal::dependentsOnMap["PATIENT_ISDISPENSARY"] = new Array();
            model_internal::dependentsOnMap["PATIENT_DISCOUNT_AMOUNT"] = new Array();
            model_internal::dependentsOnMap["PATIENT_VISIT_ID"] = new Array();
            model_internal::dependentsOnMap["PATIENT_VISIT"] = new Array();
            model_internal::dependentsOnMap["PATIENT_COUNT"] = new Array();
            model_internal::dependentsOnMap["WHEREFROM_ID"] = new Array();
            model_internal::dependentsOnMap["WHEREFROM_DESCRIPTION"] = new Array();
            model_internal::dependentsOnMap["GROUP_ID"] = new Array();
            model_internal::dependentsOnMap["GROUP_DESCRIPTION"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROC_ID"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROC_NAME"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROC_PROCTYPE"] = new Array();
            model_internal::dependentsOnMap["HEALTHPROC_PRICE"] = new Array();
            model_internal::dependentsOnMap["INVOICES_ID"] = new Array();
            model_internal::dependentsOnMap["INVOICES_INVOICENUMBER"] = new Array();
            model_internal::dependentsOnMap["INVOICES_CREATEDATE"] = new Array();
            model_internal::dependentsOnMap["INVOICES_PAIDINCASH"] = new Array();
            model_internal::dependentsOnMap["TREATMENTPROC_DISCOUNT_AMOUNT"] = new Array();
            model_internal::dependentsOnMap["ACCOUNTFLOW_ID"] = new Array();
            model_internal::dependentsOnMap["ACCOUNTFLOW_ORDERNUMBER"] = new Array();
            model_internal::dependentsOnMap["ACCOUNTFLOW_SUMM"] = new Array();
            model_internal::dependentsOnMap["ACCOUNTFLOW_OPERATIONTIMESTAMP"] = new Array();
            model_internal::dependentsOnMap["INVOICES_CREATE_DAY"] = new Array();
            model_internal::dependentsOnMap["ACCOUNTFLOW_OPERATIONDAY"] = new Array();
            model_internal::dependentsOnMap["PATIENTSINSURANCE_ID"] = new Array();
            model_internal::dependentsOnMap["PATIENTSINSURANCE_POLICENUMBER"] = new Array();
            model_internal::dependentsOnMap["PATIENTSINSURANCE_POLICETODATE"] = new Array();
            model_internal::dependentsOnMap["PATIENTSINSURANCE_POLICEFROMDATE"] = new Array();
            model_internal::dependentsOnMap["INSURANCECOMPANYS_ID"] = new Array();
            model_internal::dependentsOnMap["INSURANCECOMPANYS_NAME"] = new Array();
            model_internal::dependentsOnMap["COUNT"] = new Array();
            model_internal::dependentsOnMap["isOpen"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["FARM_ID"] = "String";
        model_internal::propertyTypeMap["FARM_NAME"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROCFARM_ID"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROCFARM_QUANTITY"] = "Number";
        model_internal::propertyTypeMap["HEALTHPROCFARM_QUANTITY"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_ID"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROC_NAME"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROC_SHIFR"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROC_HEALTHTYPE"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROC_UOP"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_PRICE"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_PRICE_DISCOUNT"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_PRICE_DISCOUNT_SUMM"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_DATECLOSE"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROC_DAYCLOSE"] = "String";
        model_internal::propertyTypeMap["TREATMENTPROC_EXPENSES"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_PROC_COUNT"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_EXPENSES_FACT"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_EXPENSES_DELTA"] = "Number";
        model_internal::propertyTypeMap["DOCTOR_ID"] = "String";
        model_internal::propertyTypeMap["DOCTOR_SHORTNAME"] = "String";
        model_internal::propertyTypeMap["DOCTOR_SPECIALITY"] = "String";
        model_internal::propertyTypeMap["DOCTOR_PERCENT"] = "Number";
        model_internal::propertyTypeMap["DOCTOR_BASESALARY"] = "Number";
        model_internal::propertyTypeMap["DOCTOR_PERCENTSUMM"] = "Number";
        model_internal::propertyTypeMap["DOCTOR_FULLSALARY"] = "Number";
        model_internal::propertyTypeMap["ASSISTANT_BASESALARY"] = "Number";
        model_internal::propertyTypeMap["ASSISTANT_PERCENTSUMM"] = "Number";
        model_internal::propertyTypeMap["ASSISTANT_FULLSALARY"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_CALCTIME"] = "Number";
        model_internal::propertyTypeMap["PATIENT_ID"] = "String";
        model_internal::propertyTypeMap["PATIENT_FULLNAME"] = "String";
        model_internal::propertyTypeMap["PATIENT_BIRTHDAY"] = "String";
        model_internal::propertyTypeMap["PATIENT_AGE"] = "int";
        model_internal::propertyTypeMap["PATIENT_SEX"] = "String";
        model_internal::propertyTypeMap["PATIENT_VILLAGEORCITY"] = "String";
        model_internal::propertyTypeMap["PATIENT_POPULATIONGROUPE"] = "String";
        model_internal::propertyTypeMap["PATIENT_ISDISPENSARY"] = "String";
        model_internal::propertyTypeMap["PATIENT_DISCOUNT_AMOUNT"] = "String";
        model_internal::propertyTypeMap["PATIENT_VISIT_ID"] = "String";
        model_internal::propertyTypeMap["PATIENT_VISIT"] = "int";
        model_internal::propertyTypeMap["PATIENT_COUNT"] = "int";
        model_internal::propertyTypeMap["WHEREFROM_ID"] = "String";
        model_internal::propertyTypeMap["WHEREFROM_DESCRIPTION"] = "String";
        model_internal::propertyTypeMap["GROUP_ID"] = "String";
        model_internal::propertyTypeMap["GROUP_DESCRIPTION"] = "String";
        model_internal::propertyTypeMap["HEALTHPROC_ID"] = "String";
        model_internal::propertyTypeMap["HEALTHPROC_NAME"] = "String";
        model_internal::propertyTypeMap["HEALTHPROC_PROCTYPE"] = "String";
        model_internal::propertyTypeMap["HEALTHPROC_PRICE"] = "Number";
        model_internal::propertyTypeMap["INVOICES_ID"] = "String";
        model_internal::propertyTypeMap["INVOICES_INVOICENUMBER"] = "String";
        model_internal::propertyTypeMap["INVOICES_CREATEDATE"] = "String";
        model_internal::propertyTypeMap["INVOICES_PAIDINCASH"] = "Number";
        model_internal::propertyTypeMap["TREATMENTPROC_DISCOUNT_AMOUNT"] = "Number";
        model_internal::propertyTypeMap["ACCOUNTFLOW_ID"] = "String";
        model_internal::propertyTypeMap["ACCOUNTFLOW_ORDERNUMBER"] = "String";
        model_internal::propertyTypeMap["ACCOUNTFLOW_SUMM"] = "Number";
        model_internal::propertyTypeMap["ACCOUNTFLOW_OPERATIONTIMESTAMP"] = "String";
        model_internal::propertyTypeMap["INVOICES_CREATE_DAY"] = "String";
        model_internal::propertyTypeMap["ACCOUNTFLOW_OPERATIONDAY"] = "String";
        model_internal::propertyTypeMap["PATIENTSINSURANCE_ID"] = "String";
        model_internal::propertyTypeMap["PATIENTSINSURANCE_POLICENUMBER"] = "String";
        model_internal::propertyTypeMap["PATIENTSINSURANCE_POLICETODATE"] = "String";
        model_internal::propertyTypeMap["PATIENTSINSURANCE_POLICEFROMDATE"] = "String";
        model_internal::propertyTypeMap["INSURANCECOMPANYS_ID"] = "String";
        model_internal::propertyTypeMap["INSURANCECOMPANYS_NAME"] = "String";
        model_internal::propertyTypeMap["COUNT"] = "int";
        model_internal::propertyTypeMap["isOpen"] = "Boolean";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity ChiefOlapModuleRecord");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity ChiefOlapModuleRecord");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of ChiefOlapModuleRecord");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ChiefOlapModuleRecord");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity ChiefOlapModuleRecord");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity ChiefOlapModuleRecord");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFARM_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROCFARM_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROCFARM_QUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROCFARM_QUANTITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_SHIFRAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_HEALTHTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_UOPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_PRICEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_PRICE_DISCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_PRICE_DISCOUNT_SUMMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_DATECLOSEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_DAYCLOSEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_EXPENSESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_PROC_COUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_EXPENSES_FACTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_EXPENSES_DELTAAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_SHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_SPECIALITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_PERCENTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_BASESALARYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_PERCENTSUMMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_FULLSALARYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isASSISTANT_BASESALARYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isASSISTANT_PERCENTSUMMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isASSISTANT_FULLSALARYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_CALCTIMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_FULLNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_BIRTHDAYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_AGEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_SEXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_VILLAGEORCITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_POPULATIONGROUPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_ISDISPENSARYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_DISCOUNT_AMOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_VISIT_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_VISITAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_COUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWHEREFROM_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWHEREFROM_DESCRIPTIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGROUP_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGROUP_DESCRIPTIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROC_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROC_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROC_PROCTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHEALTHPROC_PRICEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINVOICES_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINVOICES_INVOICENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINVOICES_CREATEDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINVOICES_PAIDINCASHAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTREATMENTPROC_DISCOUNT_AMOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNTFLOW_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNTFLOW_ORDERNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNTFLOW_SUMMAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNTFLOW_OPERATIONTIMESTAMPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINVOICES_CREATE_DAYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNTFLOW_OPERATIONDAYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTSINSURANCE_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTSINSURANCE_POLICENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTSINSURANCE_POLICETODATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTSINSURANCE_POLICEFROMDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINSURANCECOMPANYS_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINSURANCECOMPANYS_NAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsOpenAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FARM_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROCFARM_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROCFARM_QUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROCFARM_QUANTITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_SHIFRStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_HEALTHTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_UOPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_PRICEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_PRICE_DISCOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_PRICE_DISCOUNT_SUMMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_DATECLOSEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_DAYCLOSEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_EXPENSESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_PROC_COUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_EXPENSES_FACTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_EXPENSES_DELTAStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_SHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_SPECIALITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_PERCENTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_BASESALARYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_PERCENTSUMMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_FULLSALARYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ASSISTANT_BASESALARYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ASSISTANT_PERCENTSUMMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ASSISTANT_FULLSALARYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_CALCTIMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_FULLNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_BIRTHDAYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_AGEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_SEXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_VILLAGEORCITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_POPULATIONGROUPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_ISDISPENSARYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_DISCOUNT_AMOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_VISIT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_VISITStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_COUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WHEREFROM_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WHEREFROM_DESCRIPTIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get GROUP_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get GROUP_DESCRIPTIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROC_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROC_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROC_PROCTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HEALTHPROC_PRICEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INVOICES_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INVOICES_INVOICENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INVOICES_CREATEDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INVOICES_PAIDINCASHStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TREATMENTPROC_DISCOUNT_AMOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNTFLOW_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNTFLOW_ORDERNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNTFLOW_SUMMStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNTFLOW_OPERATIONTIMESTAMPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INVOICES_CREATE_DAYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNTFLOW_OPERATIONDAYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTSINSURANCE_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTSINSURANCE_POLICENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTSINSURANCE_POLICETODATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTSINSURANCE_POLICEFROMDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INSURANCECOMPANYS_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INSURANCECOMPANYS_NAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isOpenStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
