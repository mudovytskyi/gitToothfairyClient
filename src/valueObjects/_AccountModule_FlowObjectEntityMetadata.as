
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _AccountModule_FlowObjectEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("is_invoice", "invoice_id", "invoice_timestamp", "invoice_number", "invoice_sum", "invoice_discountsum", "invoice_paidincash", "invoice_discount", "invoice_isCashpayment", "patient_id", "patient_insuranceId", "patient_insuranceNumber", "patient_insuranceCompanyName", "patient_insuranceCompanyId", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "patient_discount", "discount_amount_auto", "INDDISCOUNT", "patient_balance", "patient_paidincash", "patient_balanceNoncash", "patient_paidincashNoncash", "patient_balanceAll", "patient_paidincashAll", "patient_label", "procedures_MinDate", "procedures_MaxDate", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "discount_cash");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("is_invoice", "invoice_id", "invoice_timestamp", "invoice_number", "invoice_sum", "invoice_discountsum", "invoice_paidincash", "invoice_discount", "invoice_isCashpayment", "patient_id", "patient_insuranceId", "patient_insuranceNumber", "patient_insuranceCompanyName", "patient_insuranceCompanyId", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "patient_discount", "discount_amount_auto", "INDDISCOUNT", "patient_balance", "patient_paidincash", "patient_balanceNoncash", "patient_paidincashNoncash", "patient_balanceAll", "patient_paidincashAll", "patient_label", "procedures_MinDate", "procedures_MaxDate", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "discount_cash");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("is_invoice", "invoice_id", "invoice_timestamp", "invoice_number", "invoice_sum", "invoice_discountsum", "invoice_paidincash", "invoice_discount", "invoice_isCashpayment", "patient_id", "patient_insuranceId", "patient_insuranceNumber", "patient_insuranceCompanyName", "patient_insuranceCompanyId", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "patient_discount", "discount_amount_auto", "INDDISCOUNT", "patient_balance", "patient_paidincash", "patient_balanceNoncash", "patient_paidincashNoncash", "patient_balanceAll", "patient_paidincashAll", "patient_label", "procedures_MinDate", "procedures_MaxDate", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "discount_cash");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("is_invoice", "invoice_id", "invoice_timestamp", "invoice_number", "invoice_sum", "invoice_discountsum", "invoice_paidincash", "invoice_discount", "invoice_isCashpayment", "patient_id", "patient_insuranceId", "patient_insuranceNumber", "patient_insuranceCompanyName", "patient_insuranceCompanyId", "patient_fname", "patient_lname", "patient_sname", "patient_cardnumber", "patient_discount", "discount_amount_auto", "INDDISCOUNT", "patient_balance", "patient_paidincash", "patient_balanceNoncash", "patient_paidincashNoncash", "patient_balanceAll", "patient_paidincashAll", "patient_label", "procedures_MinDate", "procedures_MaxDate", "author_id", "author_shortname", "subdivision_id", "subdivision_name", "discount_cash");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "AccountModule_FlowObject";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_AccountModule_FlowObject;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _AccountModule_FlowObjectEntityMetadata(value : _Super_AccountModule_FlowObject)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["is_invoice"] = new Array();
            model_internal::dependentsOnMap["invoice_id"] = new Array();
            model_internal::dependentsOnMap["invoice_timestamp"] = new Array();
            model_internal::dependentsOnMap["invoice_number"] = new Array();
            model_internal::dependentsOnMap["invoice_sum"] = new Array();
            model_internal::dependentsOnMap["invoice_discountsum"] = new Array();
            model_internal::dependentsOnMap["invoice_paidincash"] = new Array();
            model_internal::dependentsOnMap["invoice_discount"] = new Array();
            model_internal::dependentsOnMap["invoice_isCashpayment"] = new Array();
            model_internal::dependentsOnMap["patient_id"] = new Array();
            model_internal::dependentsOnMap["patient_insuranceId"] = new Array();
            model_internal::dependentsOnMap["patient_insuranceNumber"] = new Array();
            model_internal::dependentsOnMap["patient_insuranceCompanyName"] = new Array();
            model_internal::dependentsOnMap["patient_insuranceCompanyId"] = new Array();
            model_internal::dependentsOnMap["patient_fname"] = new Array();
            model_internal::dependentsOnMap["patient_lname"] = new Array();
            model_internal::dependentsOnMap["patient_sname"] = new Array();
            model_internal::dependentsOnMap["patient_cardnumber"] = new Array();
            model_internal::dependentsOnMap["patient_discount"] = new Array();
            model_internal::dependentsOnMap["discount_amount_auto"] = new Array();
            model_internal::dependentsOnMap["INDDISCOUNT"] = new Array();
            model_internal::dependentsOnMap["patient_balance"] = new Array();
            model_internal::dependentsOnMap["patient_paidincash"] = new Array();
            model_internal::dependentsOnMap["patient_balanceNoncash"] = new Array();
            model_internal::dependentsOnMap["patient_paidincashNoncash"] = new Array();
            model_internal::dependentsOnMap["patient_balanceAll"] = new Array();
            model_internal::dependentsOnMap["patient_paidincashAll"] = new Array();
            model_internal::dependentsOnMap["patient_label"] = new Array();
            model_internal::dependentsOnMap["procedures_MinDate"] = new Array();
            model_internal::dependentsOnMap["procedures_MaxDate"] = new Array();
            model_internal::dependentsOnMap["author_id"] = new Array();
            model_internal::dependentsOnMap["author_shortname"] = new Array();
            model_internal::dependentsOnMap["subdivision_id"] = new Array();
            model_internal::dependentsOnMap["subdivision_name"] = new Array();
            model_internal::dependentsOnMap["discount_cash"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["is_invoice"] = "Boolean";
        model_internal::propertyTypeMap["invoice_id"] = "String";
        model_internal::propertyTypeMap["invoice_timestamp"] = "String";
        model_internal::propertyTypeMap["invoice_number"] = "String";
        model_internal::propertyTypeMap["invoice_sum"] = "Number";
        model_internal::propertyTypeMap["invoice_discountsum"] = "Number";
        model_internal::propertyTypeMap["invoice_paidincash"] = "Number";
        model_internal::propertyTypeMap["invoice_discount"] = "Number";
        model_internal::propertyTypeMap["invoice_isCashpayment"] = "int";
        model_internal::propertyTypeMap["patient_id"] = "String";
        model_internal::propertyTypeMap["patient_insuranceId"] = "String";
        model_internal::propertyTypeMap["patient_insuranceNumber"] = "String";
        model_internal::propertyTypeMap["patient_insuranceCompanyName"] = "String";
        model_internal::propertyTypeMap["patient_insuranceCompanyId"] = "String";
        model_internal::propertyTypeMap["patient_fname"] = "String";
        model_internal::propertyTypeMap["patient_lname"] = "String";
        model_internal::propertyTypeMap["patient_sname"] = "String";
        model_internal::propertyTypeMap["patient_cardnumber"] = "String";
        model_internal::propertyTypeMap["patient_discount"] = "Number";
        model_internal::propertyTypeMap["discount_amount_auto"] = "Number";
        model_internal::propertyTypeMap["INDDISCOUNT"] = "Boolean";
        model_internal::propertyTypeMap["patient_balance"] = "Number";
        model_internal::propertyTypeMap["patient_paidincash"] = "Number";
        model_internal::propertyTypeMap["patient_balanceNoncash"] = "Number";
        model_internal::propertyTypeMap["patient_paidincashNoncash"] = "Number";
        model_internal::propertyTypeMap["patient_balanceAll"] = "Number";
        model_internal::propertyTypeMap["patient_paidincashAll"] = "Number";
        model_internal::propertyTypeMap["patient_label"] = "String";
        model_internal::propertyTypeMap["procedures_MinDate"] = "String";
        model_internal::propertyTypeMap["procedures_MaxDate"] = "String";
        model_internal::propertyTypeMap["author_id"] = "String";
        model_internal::propertyTypeMap["author_shortname"] = "String";
        model_internal::propertyTypeMap["subdivision_id"] = "String";
        model_internal::propertyTypeMap["subdivision_name"] = "String";
        model_internal::propertyTypeMap["discount_cash"] = "Number";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity AccountModule_FlowObject");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity AccountModule_FlowObject");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of AccountModule_FlowObject");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AccountModule_FlowObject");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity AccountModule_FlowObject");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AccountModule_FlowObject");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIs_invoiceAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_timestampAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_numberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_sumAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_discountsumAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_paidincashAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_discountAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoice_isCashpaymentAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_insuranceIdAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_insuranceNumberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_insuranceCompanyNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_insuranceCompanyIdAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_snameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_cardnumberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_discountAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_amount_autoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINDDISCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_balanceAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_paidincashAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_balanceNoncashAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_paidincashNoncashAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_balanceAllAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_paidincashAllAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_labelAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcedures_MinDateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isProcedures_MaxDateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAuthor_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAuthor_shortnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_cashAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get is_invoiceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_timestampStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_numberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_sumStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_discountsumStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_paidincashStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_discountStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get invoice_isCashpaymentStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_insuranceIdStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_insuranceNumberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_insuranceCompanyNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_insuranceCompanyIdStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_snameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_cardnumberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_discountStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_amount_autoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INDDISCOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_balanceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_paidincashStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_balanceNoncashStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_paidincashNoncashStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_balanceAllStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_paidincashAllStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_labelStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procedures_MinDateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get procedures_MaxDateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get author_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get author_shortnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_cashStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
