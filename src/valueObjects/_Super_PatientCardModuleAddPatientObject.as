/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PatientCardModuleAddPatientObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.PatientCardModule_mobilephone;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PatientCardModuleAddPatientObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PatientCardModuleAddPatientObject") == null)
            {
                flash.net.registerClassAlias("PatientCardModuleAddPatientObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PatientCardModuleAddPatientObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.PatientCardModule_mobilephone.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _PatientCardModuleAddPatientObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : int;
    private var _internal_CARDNUMBER_FULL : String;
    private var _internal_CARDNUMBER : int;
    private var _internal_CARDDATE : String;
    private var _internal_CARDBEFORNUMBER : String;
    private var _internal_CARDAFTERNUMBER : String;
    private var _internal_FIRSTNAME : String;
    private var _internal_MIDDLENAME : String;
    private var _internal_LASTNAME : String;
    private var _internal_FULLNAME : String;
    private var _internal_SHORTNAME : String;
    private var _internal_BIRTHDAY : String;
    private var _internal_SEX : int;
    private var _internal_ADDRESS : String;
    private var _internal_POSTALCODE : String;
    private var _internal_TELEPHONENUMBER : String;
    private var _internal_MOBILENUMBER : String;
    private var _internal_EMPLOYMENTPLACE : String;
    private var _internal_JOB : String;
    private var _internal_WORKPHONE : String;
    private var _internal_VILLAGEORCITY : int;
    private var _internal_POPULATIONGROUPE : int;
    private var _internal_ISDISPANCER : Object;
    private var _internal_HOBBY : String;
    private var _internal_COUNTRY : String;
    private var _internal_STATE : String;
    private var _internal_RAGION : String;
    private var _internal_EMAIL : String;
    private var _internal_CITY : String;
    private var _internal_PASSPORT_SERIES : String;
    private var _internal_PASSPORT_NUMBER : String;
    private var _internal_PASSPORT_ISSUING : String;
    private var _internal_PASSPORT_DATE : String;
    private var _internal_WHEREFROMID : String;
    private var _internal_FATHERFIRSTNAME : String;
    private var _internal_MOTHERFIRSTNAME : String;
    private var _internal_FATHERMIDDLENAME : String;
    private var _internal_MOTHERMIDDLENAME : String;
    private var _internal_FATHERLASTNAME : String;
    private var _internal_MOTHERLASTNAME : String;
    private var _internal_FATHERBIRTHDATE : String;
    private var _internal_MOTHERBIRTHDATE : String;
    private var _internal_GROUPID : String;
    private var _internal_RELATIVEID : String;
    private var _internal_LEGALGUARDIANID : String;
    private var _internal_INSURANCEID : int;
    private var _internal_INSURANCECOUNT : int;
    private var _internal_TESTPATOLOGYFLAG : int;
    private var _internal_COUNTS_TOOTHEXAMS : int;
    private var _internal_COUNTS_PROCEDURES_CLOSED : int;
    private var _internal_COUNTS_PROCEDURES_NOTCLOSED : int;
    private var _internal_COUNTS_FILES : int;
    private var _internal_full_addres : String;
    private var _internal_full_fio : String;
    private var _internal_discount_amount : Number;
    private var _internal_discount_amount_auto : Number;
    private var _internal_INDDISCOUNT : Boolean;
    private var _internal_DISCOUNTCARDNUMBER : String;
    private var _internal_is_returned : int;
    private var _internal_patient_ages : int;
    private var _internal_patient_notes : String;
    private var _internal_doctors : String;
    private var _internal_doctor_lead : String;
    private var _internal_agreementTimestamp : String;
    private var _internal_mobiles : ArrayCollection;
    model_internal var _internal_mobiles_leaf:valueObjects.PatientCardModule_mobilephone;
    private var _internal_is_archived : Boolean;
    private var _internal_subdivision_id : String;
    private var _internal_subdivision_label : String;
    private var _internal_NH_TREATTYPE : int;
    private var _internal_EH_DEC_ID : String;
    private var _internal_TAXID : String;
    private var _internal_agreementtype : int;
    private var _internal_birthcert_series : String;
    private var _internal_birthcert_number : String;
    private var _internal_birthcert_issuing : String;
    private var _internal_birthcert_date : String;
    private var _internal_IS_MOTHER : int;
    private var _internal_MOTHER_ID : int;
    private var _internal_MOTHER_PASSPORT_SERIES : String;
    private var _internal_MOTHER_PASSPORT_NUMBER : String;
    private var _internal_MOTHER_PASSPORT_ISSUING : String;
    private var _internal_MOTHER_PASSPORT_DATE : String;
    private var _internal_FATHER_ID : int;
    private var _internal_FATHER_PASSPORT_SERIES : String;
    private var _internal_FATHER_PASSPORT_NUMBER : String;
    private var _internal_FATHER_PASSPORT_ISSUING : String;
    private var _internal_FATHER_PASSPORT_DATE : String;
    private var _internal_CONFLICTFLAG : Boolean;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PatientCardModuleAddPatientObject()
    {
        _model = new _PatientCardModuleAddPatientObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : int
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get CARDNUMBER_FULL() : String
    {
        return _internal_CARDNUMBER_FULL;
    }

    [Bindable(event="propertyChange")]
    public function get CARDNUMBER() : int
    {
        return _internal_CARDNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CARDDATE() : String
    {
        return _internal_CARDDATE;
    }

    [Bindable(event="propertyChange")]
    public function get CARDBEFORNUMBER() : String
    {
        return _internal_CARDBEFORNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CARDAFTERNUMBER() : String
    {
        return _internal_CARDAFTERNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get FIRSTNAME() : String
    {
        return _internal_FIRSTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get MIDDLENAME() : String
    {
        return _internal_MIDDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get LASTNAME() : String
    {
        return _internal_LASTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get FULLNAME() : String
    {
        return _internal_FULLNAME;
    }

    [Bindable(event="propertyChange")]
    public function get SHORTNAME() : String
    {
        return _internal_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get BIRTHDAY() : String
    {
        return _internal_BIRTHDAY;
    }

    [Bindable(event="propertyChange")]
    public function get SEX() : int
    {
        return _internal_SEX;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS() : String
    {
        return _internal_ADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get POSTALCODE() : String
    {
        return _internal_POSTALCODE;
    }

    [Bindable(event="propertyChange")]
    public function get TELEPHONENUMBER() : String
    {
        return _internal_TELEPHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get MOBILENUMBER() : String
    {
        return _internal_MOBILENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get EMPLOYMENTPLACE() : String
    {
        return _internal_EMPLOYMENTPLACE;
    }

    [Bindable(event="propertyChange")]
    public function get JOB() : String
    {
        return _internal_JOB;
    }

    [Bindable(event="propertyChange")]
    public function get WORKPHONE() : String
    {
        return _internal_WORKPHONE;
    }

    [Bindable(event="propertyChange")]
    public function get VILLAGEORCITY() : int
    {
        return _internal_VILLAGEORCITY;
    }

    [Bindable(event="propertyChange")]
    public function get POPULATIONGROUPE() : int
    {
        return _internal_POPULATIONGROUPE;
    }

    [Bindable(event="propertyChange")]
    public function get ISDISPANCER() : Object
    {
        return _internal_ISDISPANCER;
    }

    [Bindable(event="propertyChange")]
    public function get HOBBY() : String
    {
        return _internal_HOBBY;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTRY() : String
    {
        return _internal_COUNTRY;
    }

    [Bindable(event="propertyChange")]
    public function get STATE() : String
    {
        return _internal_STATE;
    }

    [Bindable(event="propertyChange")]
    public function get RAGION() : String
    {
        return _internal_RAGION;
    }

    [Bindable(event="propertyChange")]
    public function get EMAIL() : String
    {
        return _internal_EMAIL;
    }

    [Bindable(event="propertyChange")]
    public function get CITY() : String
    {
        return _internal_CITY;
    }

    [Bindable(event="propertyChange")]
    public function get PASSPORT_SERIES() : String
    {
        return _internal_PASSPORT_SERIES;
    }

    [Bindable(event="propertyChange")]
    public function get PASSPORT_NUMBER() : String
    {
        return _internal_PASSPORT_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PASSPORT_ISSUING() : String
    {
        return _internal_PASSPORT_ISSUING;
    }

    [Bindable(event="propertyChange")]
    public function get PASSPORT_DATE() : String
    {
        return _internal_PASSPORT_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get WHEREFROMID() : String
    {
        return _internal_WHEREFROMID;
    }

    [Bindable(event="propertyChange")]
    public function get FATHERFIRSTNAME() : String
    {
        return _internal_FATHERFIRSTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHERFIRSTNAME() : String
    {
        return _internal_MOTHERFIRSTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get FATHERMIDDLENAME() : String
    {
        return _internal_FATHERMIDDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHERMIDDLENAME() : String
    {
        return _internal_MOTHERMIDDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get FATHERLASTNAME() : String
    {
        return _internal_FATHERLASTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHERLASTNAME() : String
    {
        return _internal_MOTHERLASTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get FATHERBIRTHDATE() : String
    {
        return _internal_FATHERBIRTHDATE;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHERBIRTHDATE() : String
    {
        return _internal_MOTHERBIRTHDATE;
    }

    [Bindable(event="propertyChange")]
    public function get GROUPID() : String
    {
        return _internal_GROUPID;
    }

    [Bindable(event="propertyChange")]
    public function get RELATIVEID() : String
    {
        return _internal_RELATIVEID;
    }

    [Bindable(event="propertyChange")]
    public function get LEGALGUARDIANID() : String
    {
        return _internal_LEGALGUARDIANID;
    }

    [Bindable(event="propertyChange")]
    public function get INSURANCEID() : int
    {
        return _internal_INSURANCEID;
    }

    [Bindable(event="propertyChange")]
    public function get INSURANCECOUNT() : int
    {
        return _internal_INSURANCECOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get TESTPATOLOGYFLAG() : int
    {
        return _internal_TESTPATOLOGYFLAG;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_TOOTHEXAMS() : int
    {
        return _internal_COUNTS_TOOTHEXAMS;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_PROCEDURES_CLOSED() : int
    {
        return _internal_COUNTS_PROCEDURES_CLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_PROCEDURES_NOTCLOSED() : int
    {
        return _internal_COUNTS_PROCEDURES_NOTCLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get COUNTS_FILES() : int
    {
        return _internal_COUNTS_FILES;
    }

    [Bindable(event="propertyChange")]
    public function get full_addres() : String
    {
        return _internal_full_addres;
    }

    [Bindable(event="propertyChange")]
    public function get full_fio() : String
    {
        return _internal_full_fio;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount() : Number
    {
        return _internal_discount_amount;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount_auto() : Number
    {
        return _internal_discount_amount_auto;
    }

    [Bindable(event="propertyChange")]
    public function get INDDISCOUNT() : Boolean
    {
        return _internal_INDDISCOUNT;
    }

    [Bindable(event="propertyChange")]
    public function get DISCOUNTCARDNUMBER() : String
    {
        return _internal_DISCOUNTCARDNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get is_returned() : int
    {
        return _internal_is_returned;
    }

    [Bindable(event="propertyChange")]
    public function get patient_ages() : int
    {
        return _internal_patient_ages;
    }

    [Bindable(event="propertyChange")]
    public function get patient_notes() : String
    {
        return _internal_patient_notes;
    }

    [Bindable(event="propertyChange")]
    public function get doctors() : String
    {
        return _internal_doctors;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lead() : String
    {
        return _internal_doctor_lead;
    }

    [Bindable(event="propertyChange")]
    public function get agreementTimestamp() : String
    {
        return _internal_agreementTimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get mobiles() : ArrayCollection
    {
        return _internal_mobiles;
    }

    [Bindable(event="propertyChange")]
    public function get is_archived() : Boolean
    {
        return _internal_is_archived;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_id() : String
    {
        return _internal_subdivision_id;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_label() : String
    {
        return _internal_subdivision_label;
    }

    [Bindable(event="propertyChange")]
    public function get NH_TREATTYPE() : int
    {
        return _internal_NH_TREATTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get EH_DEC_ID() : String
    {
        return _internal_EH_DEC_ID;
    }

    [Bindable(event="propertyChange")]
    public function get TAXID() : String
    {
        return _internal_TAXID;
    }

    [Bindable(event="propertyChange")]
    public function get agreementtype() : int
    {
        return _internal_agreementtype;
    }

    [Bindable(event="propertyChange")]
    public function get birthcert_series() : String
    {
        return _internal_birthcert_series;
    }

    [Bindable(event="propertyChange")]
    public function get birthcert_number() : String
    {
        return _internal_birthcert_number;
    }

    [Bindable(event="propertyChange")]
    public function get birthcert_issuing() : String
    {
        return _internal_birthcert_issuing;
    }

    [Bindable(event="propertyChange")]
    public function get birthcert_date() : String
    {
        return _internal_birthcert_date;
    }

    [Bindable(event="propertyChange")]
    public function get IS_MOTHER() : int
    {
        return _internal_IS_MOTHER;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHER_ID() : int
    {
        return _internal_MOTHER_ID;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHER_PASSPORT_SERIES() : String
    {
        return _internal_MOTHER_PASSPORT_SERIES;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHER_PASSPORT_NUMBER() : String
    {
        return _internal_MOTHER_PASSPORT_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHER_PASSPORT_ISSUING() : String
    {
        return _internal_MOTHER_PASSPORT_ISSUING;
    }

    [Bindable(event="propertyChange")]
    public function get MOTHER_PASSPORT_DATE() : String
    {
        return _internal_MOTHER_PASSPORT_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get FATHER_ID() : int
    {
        return _internal_FATHER_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FATHER_PASSPORT_SERIES() : String
    {
        return _internal_FATHER_PASSPORT_SERIES;
    }

    [Bindable(event="propertyChange")]
    public function get FATHER_PASSPORT_NUMBER() : String
    {
        return _internal_FATHER_PASSPORT_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get FATHER_PASSPORT_ISSUING() : String
    {
        return _internal_FATHER_PASSPORT_ISSUING;
    }

    [Bindable(event="propertyChange")]
    public function get FATHER_PASSPORT_DATE() : String
    {
        return _internal_FATHER_PASSPORT_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get CONFLICTFLAG() : Boolean
    {
        return _internal_CONFLICTFLAG;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:int) : void
    {
        var oldValue:int = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set CARDNUMBER_FULL(value:String) : void
    {
        var oldValue:String = _internal_CARDNUMBER_FULL;
        if (oldValue !== value)
        {
            _internal_CARDNUMBER_FULL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDNUMBER_FULL", oldValue, _internal_CARDNUMBER_FULL));
        }
    }

    public function set CARDNUMBER(value:int) : void
    {
        var oldValue:int = _internal_CARDNUMBER;
        if (oldValue !== value)
        {
            _internal_CARDNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDNUMBER", oldValue, _internal_CARDNUMBER));
        }
    }

    public function set CARDDATE(value:String) : void
    {
        var oldValue:String = _internal_CARDDATE;
        if (oldValue !== value)
        {
            _internal_CARDDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDDATE", oldValue, _internal_CARDDATE));
        }
    }

    public function set CARDBEFORNUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARDBEFORNUMBER;
        if (oldValue !== value)
        {
            _internal_CARDBEFORNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDBEFORNUMBER", oldValue, _internal_CARDBEFORNUMBER));
        }
    }

    public function set CARDAFTERNUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARDAFTERNUMBER;
        if (oldValue !== value)
        {
            _internal_CARDAFTERNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDAFTERNUMBER", oldValue, _internal_CARDAFTERNUMBER));
        }
    }

    public function set FIRSTNAME(value:String) : void
    {
        var oldValue:String = _internal_FIRSTNAME;
        if (oldValue !== value)
        {
            _internal_FIRSTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FIRSTNAME", oldValue, _internal_FIRSTNAME));
        }
    }

    public function set MIDDLENAME(value:String) : void
    {
        var oldValue:String = _internal_MIDDLENAME;
        if (oldValue !== value)
        {
            _internal_MIDDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MIDDLENAME", oldValue, _internal_MIDDLENAME));
        }
    }

    public function set LASTNAME(value:String) : void
    {
        var oldValue:String = _internal_LASTNAME;
        if (oldValue !== value)
        {
            _internal_LASTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LASTNAME", oldValue, _internal_LASTNAME));
        }
    }

    public function set FULLNAME(value:String) : void
    {
        var oldValue:String = _internal_FULLNAME;
        if (oldValue !== value)
        {
            _internal_FULLNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FULLNAME", oldValue, _internal_FULLNAME));
        }
    }

    public function set SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SHORTNAME", oldValue, _internal_SHORTNAME));
        }
    }

    public function set BIRTHDAY(value:String) : void
    {
        var oldValue:String = _internal_BIRTHDAY;
        if (oldValue !== value)
        {
            _internal_BIRTHDAY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BIRTHDAY", oldValue, _internal_BIRTHDAY));
        }
    }

    public function set SEX(value:int) : void
    {
        var oldValue:int = _internal_SEX;
        if (oldValue !== value)
        {
            _internal_SEX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SEX", oldValue, _internal_SEX));
        }
    }

    public function set ADDRESS(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS;
        if (oldValue !== value)
        {
            _internal_ADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS", oldValue, _internal_ADDRESS));
        }
    }

    public function set POSTALCODE(value:String) : void
    {
        var oldValue:String = _internal_POSTALCODE;
        if (oldValue !== value)
        {
            _internal_POSTALCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "POSTALCODE", oldValue, _internal_POSTALCODE));
        }
    }

    public function set TELEPHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_TELEPHONENUMBER;
        if (oldValue !== value)
        {
            _internal_TELEPHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TELEPHONENUMBER", oldValue, _internal_TELEPHONENUMBER));
        }
    }

    public function set MOBILENUMBER(value:String) : void
    {
        var oldValue:String = _internal_MOBILENUMBER;
        if (oldValue !== value)
        {
            _internal_MOBILENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOBILENUMBER", oldValue, _internal_MOBILENUMBER));
        }
    }

    public function set EMPLOYMENTPLACE(value:String) : void
    {
        var oldValue:String = _internal_EMPLOYMENTPLACE;
        if (oldValue !== value)
        {
            _internal_EMPLOYMENTPLACE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMPLOYMENTPLACE", oldValue, _internal_EMPLOYMENTPLACE));
        }
    }

    public function set JOB(value:String) : void
    {
        var oldValue:String = _internal_JOB;
        if (oldValue !== value)
        {
            _internal_JOB = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "JOB", oldValue, _internal_JOB));
        }
    }

    public function set WORKPHONE(value:String) : void
    {
        var oldValue:String = _internal_WORKPHONE;
        if (oldValue !== value)
        {
            _internal_WORKPHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WORKPHONE", oldValue, _internal_WORKPHONE));
        }
    }

    public function set VILLAGEORCITY(value:int) : void
    {
        var oldValue:int = _internal_VILLAGEORCITY;
        if (oldValue !== value)
        {
            _internal_VILLAGEORCITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VILLAGEORCITY", oldValue, _internal_VILLAGEORCITY));
        }
    }

    public function set POPULATIONGROUPE(value:int) : void
    {
        var oldValue:int = _internal_POPULATIONGROUPE;
        if (oldValue !== value)
        {
            _internal_POPULATIONGROUPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "POPULATIONGROUPE", oldValue, _internal_POPULATIONGROUPE));
        }
    }

    public function set ISDISPANCER(value:Object) : void
    {
        var oldValue:Object = _internal_ISDISPANCER;
        if (oldValue !== value)
        {
            _internal_ISDISPANCER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ISDISPANCER", oldValue, _internal_ISDISPANCER));
        }
    }

    public function set HOBBY(value:String) : void
    {
        var oldValue:String = _internal_HOBBY;
        if (oldValue !== value)
        {
            _internal_HOBBY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HOBBY", oldValue, _internal_HOBBY));
        }
    }

    public function set COUNTRY(value:String) : void
    {
        var oldValue:String = _internal_COUNTRY;
        if (oldValue !== value)
        {
            _internal_COUNTRY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTRY", oldValue, _internal_COUNTRY));
        }
    }

    public function set STATE(value:String) : void
    {
        var oldValue:String = _internal_STATE;
        if (oldValue !== value)
        {
            _internal_STATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATE", oldValue, _internal_STATE));
        }
    }

    public function set RAGION(value:String) : void
    {
        var oldValue:String = _internal_RAGION;
        if (oldValue !== value)
        {
            _internal_RAGION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RAGION", oldValue, _internal_RAGION));
        }
    }

    public function set EMAIL(value:String) : void
    {
        var oldValue:String = _internal_EMAIL;
        if (oldValue !== value)
        {
            _internal_EMAIL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMAIL", oldValue, _internal_EMAIL));
        }
    }

    public function set CITY(value:String) : void
    {
        var oldValue:String = _internal_CITY;
        if (oldValue !== value)
        {
            _internal_CITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CITY", oldValue, _internal_CITY));
        }
    }

    public function set PASSPORT_SERIES(value:String) : void
    {
        var oldValue:String = _internal_PASSPORT_SERIES;
        if (oldValue !== value)
        {
            _internal_PASSPORT_SERIES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PASSPORT_SERIES", oldValue, _internal_PASSPORT_SERIES));
        }
    }

    public function set PASSPORT_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_PASSPORT_NUMBER;
        if (oldValue !== value)
        {
            _internal_PASSPORT_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PASSPORT_NUMBER", oldValue, _internal_PASSPORT_NUMBER));
        }
    }

    public function set PASSPORT_ISSUING(value:String) : void
    {
        var oldValue:String = _internal_PASSPORT_ISSUING;
        if (oldValue !== value)
        {
            _internal_PASSPORT_ISSUING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PASSPORT_ISSUING", oldValue, _internal_PASSPORT_ISSUING));
        }
    }

    public function set PASSPORT_DATE(value:String) : void
    {
        var oldValue:String = _internal_PASSPORT_DATE;
        if (oldValue !== value)
        {
            _internal_PASSPORT_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PASSPORT_DATE", oldValue, _internal_PASSPORT_DATE));
        }
    }

    public function set WHEREFROMID(value:String) : void
    {
        var oldValue:String = _internal_WHEREFROMID;
        if (oldValue !== value)
        {
            _internal_WHEREFROMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WHEREFROMID", oldValue, _internal_WHEREFROMID));
        }
    }

    public function set FATHERFIRSTNAME(value:String) : void
    {
        var oldValue:String = _internal_FATHERFIRSTNAME;
        if (oldValue !== value)
        {
            _internal_FATHERFIRSTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHERFIRSTNAME", oldValue, _internal_FATHERFIRSTNAME));
        }
    }

    public function set MOTHERFIRSTNAME(value:String) : void
    {
        var oldValue:String = _internal_MOTHERFIRSTNAME;
        if (oldValue !== value)
        {
            _internal_MOTHERFIRSTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHERFIRSTNAME", oldValue, _internal_MOTHERFIRSTNAME));
        }
    }

    public function set FATHERMIDDLENAME(value:String) : void
    {
        var oldValue:String = _internal_FATHERMIDDLENAME;
        if (oldValue !== value)
        {
            _internal_FATHERMIDDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHERMIDDLENAME", oldValue, _internal_FATHERMIDDLENAME));
        }
    }

    public function set MOTHERMIDDLENAME(value:String) : void
    {
        var oldValue:String = _internal_MOTHERMIDDLENAME;
        if (oldValue !== value)
        {
            _internal_MOTHERMIDDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHERMIDDLENAME", oldValue, _internal_MOTHERMIDDLENAME));
        }
    }

    public function set FATHERLASTNAME(value:String) : void
    {
        var oldValue:String = _internal_FATHERLASTNAME;
        if (oldValue !== value)
        {
            _internal_FATHERLASTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHERLASTNAME", oldValue, _internal_FATHERLASTNAME));
        }
    }

    public function set MOTHERLASTNAME(value:String) : void
    {
        var oldValue:String = _internal_MOTHERLASTNAME;
        if (oldValue !== value)
        {
            _internal_MOTHERLASTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHERLASTNAME", oldValue, _internal_MOTHERLASTNAME));
        }
    }

    public function set FATHERBIRTHDATE(value:String) : void
    {
        var oldValue:String = _internal_FATHERBIRTHDATE;
        if (oldValue !== value)
        {
            _internal_FATHERBIRTHDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHERBIRTHDATE", oldValue, _internal_FATHERBIRTHDATE));
        }
    }

    public function set MOTHERBIRTHDATE(value:String) : void
    {
        var oldValue:String = _internal_MOTHERBIRTHDATE;
        if (oldValue !== value)
        {
            _internal_MOTHERBIRTHDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHERBIRTHDATE", oldValue, _internal_MOTHERBIRTHDATE));
        }
    }

    public function set GROUPID(value:String) : void
    {
        var oldValue:String = _internal_GROUPID;
        if (oldValue !== value)
        {
            _internal_GROUPID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "GROUPID", oldValue, _internal_GROUPID));
        }
    }

    public function set RELATIVEID(value:String) : void
    {
        var oldValue:String = _internal_RELATIVEID;
        if (oldValue !== value)
        {
            _internal_RELATIVEID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RELATIVEID", oldValue, _internal_RELATIVEID));
        }
    }

    public function set LEGALGUARDIANID(value:String) : void
    {
        var oldValue:String = _internal_LEGALGUARDIANID;
        if (oldValue !== value)
        {
            _internal_LEGALGUARDIANID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LEGALGUARDIANID", oldValue, _internal_LEGALGUARDIANID));
        }
    }

    public function set INSURANCEID(value:int) : void
    {
        var oldValue:int = _internal_INSURANCEID;
        if (oldValue !== value)
        {
            _internal_INSURANCEID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INSURANCEID", oldValue, _internal_INSURANCEID));
        }
    }

    public function set INSURANCECOUNT(value:int) : void
    {
        var oldValue:int = _internal_INSURANCECOUNT;
        if (oldValue !== value)
        {
            _internal_INSURANCECOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INSURANCECOUNT", oldValue, _internal_INSURANCECOUNT));
        }
    }

    public function set TESTPATOLOGYFLAG(value:int) : void
    {
        var oldValue:int = _internal_TESTPATOLOGYFLAG;
        if (oldValue !== value)
        {
            _internal_TESTPATOLOGYFLAG = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TESTPATOLOGYFLAG", oldValue, _internal_TESTPATOLOGYFLAG));
        }
    }

    public function set COUNTS_TOOTHEXAMS(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_TOOTHEXAMS;
        if (oldValue !== value)
        {
            _internal_COUNTS_TOOTHEXAMS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_TOOTHEXAMS", oldValue, _internal_COUNTS_TOOTHEXAMS));
        }
    }

    public function set COUNTS_PROCEDURES_CLOSED(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_PROCEDURES_CLOSED;
        if (oldValue !== value)
        {
            _internal_COUNTS_PROCEDURES_CLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_PROCEDURES_CLOSED", oldValue, _internal_COUNTS_PROCEDURES_CLOSED));
        }
    }

    public function set COUNTS_PROCEDURES_NOTCLOSED(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_PROCEDURES_NOTCLOSED;
        if (oldValue !== value)
        {
            _internal_COUNTS_PROCEDURES_NOTCLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_PROCEDURES_NOTCLOSED", oldValue, _internal_COUNTS_PROCEDURES_NOTCLOSED));
        }
    }

    public function set COUNTS_FILES(value:int) : void
    {
        var oldValue:int = _internal_COUNTS_FILES;
        if (oldValue !== value)
        {
            _internal_COUNTS_FILES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COUNTS_FILES", oldValue, _internal_COUNTS_FILES));
        }
    }

    public function set full_addres(value:String) : void
    {
        var oldValue:String = _internal_full_addres;
        if (oldValue !== value)
        {
            _internal_full_addres = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "full_addres", oldValue, _internal_full_addres));
        }
    }

    public function set full_fio(value:String) : void
    {
        var oldValue:String = _internal_full_fio;
        if (oldValue !== value)
        {
            _internal_full_fio = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "full_fio", oldValue, _internal_full_fio));
        }
    }

    public function set discount_amount(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount;
        if (isNaN(_internal_discount_amount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount", oldValue, _internal_discount_amount));
        }
    }

    public function set discount_amount_auto(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount_auto;
        if (isNaN(_internal_discount_amount_auto) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount_auto = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount_auto", oldValue, _internal_discount_amount_auto));
        }
    }

    public function set INDDISCOUNT(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_INDDISCOUNT;
        if (oldValue !== value)
        {
            _internal_INDDISCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INDDISCOUNT", oldValue, _internal_INDDISCOUNT));
        }
    }

    public function set DISCOUNTCARDNUMBER(value:String) : void
    {
        var oldValue:String = _internal_DISCOUNTCARDNUMBER;
        if (oldValue !== value)
        {
            _internal_DISCOUNTCARDNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DISCOUNTCARDNUMBER", oldValue, _internal_DISCOUNTCARDNUMBER));
        }
    }

    public function set is_returned(value:int) : void
    {
        var oldValue:int = _internal_is_returned;
        if (oldValue !== value)
        {
            _internal_is_returned = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_returned", oldValue, _internal_is_returned));
        }
    }

    public function set patient_ages(value:int) : void
    {
        var oldValue:int = _internal_patient_ages;
        if (oldValue !== value)
        {
            _internal_patient_ages = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_ages", oldValue, _internal_patient_ages));
        }
    }

    public function set patient_notes(value:String) : void
    {
        var oldValue:String = _internal_patient_notes;
        if (oldValue !== value)
        {
            _internal_patient_notes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_notes", oldValue, _internal_patient_notes));
        }
    }

    public function set doctors(value:String) : void
    {
        var oldValue:String = _internal_doctors;
        if (oldValue !== value)
        {
            _internal_doctors = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctors", oldValue, _internal_doctors));
        }
    }

    public function set doctor_lead(value:String) : void
    {
        var oldValue:String = _internal_doctor_lead;
        if (oldValue !== value)
        {
            _internal_doctor_lead = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lead", oldValue, _internal_doctor_lead));
        }
    }

    public function set agreementTimestamp(value:String) : void
    {
        var oldValue:String = _internal_agreementTimestamp;
        if (oldValue !== value)
        {
            _internal_agreementTimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "agreementTimestamp", oldValue, _internal_agreementTimestamp));
        }
    }

    public function set mobiles(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_mobiles;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_mobiles = value;
            }
            else if (value is Array)
            {
                _internal_mobiles = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_mobiles = null;
            }
            else
            {
                throw new Error("value of mobiles must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "mobiles", oldValue, _internal_mobiles));
        }
    }

    public function set is_archived(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_is_archived;
        if (oldValue !== value)
        {
            _internal_is_archived = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_archived", oldValue, _internal_is_archived));
        }
    }

    public function set subdivision_id(value:String) : void
    {
        var oldValue:String = _internal_subdivision_id;
        if (oldValue !== value)
        {
            _internal_subdivision_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_id", oldValue, _internal_subdivision_id));
        }
    }

    public function set subdivision_label(value:String) : void
    {
        var oldValue:String = _internal_subdivision_label;
        if (oldValue !== value)
        {
            _internal_subdivision_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_label", oldValue, _internal_subdivision_label));
        }
    }

    public function set NH_TREATTYPE(value:int) : void
    {
        var oldValue:int = _internal_NH_TREATTYPE;
        if (oldValue !== value)
        {
            _internal_NH_TREATTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NH_TREATTYPE", oldValue, _internal_NH_TREATTYPE));
        }
    }

    public function set EH_DEC_ID(value:String) : void
    {
        var oldValue:String = _internal_EH_DEC_ID;
        if (oldValue !== value)
        {
            _internal_EH_DEC_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_DEC_ID", oldValue, _internal_EH_DEC_ID));
        }
    }

    public function set TAXID(value:String) : void
    {
        var oldValue:String = _internal_TAXID;
        if (oldValue !== value)
        {
            _internal_TAXID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TAXID", oldValue, _internal_TAXID));
        }
    }

    public function set agreementtype(value:int) : void
    {
        var oldValue:int = _internal_agreementtype;
        if (oldValue !== value)
        {
            _internal_agreementtype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "agreementtype", oldValue, _internal_agreementtype));
        }
    }

    public function set birthcert_series(value:String) : void
    {
        var oldValue:String = _internal_birthcert_series;
        if (oldValue !== value)
        {
            _internal_birthcert_series = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "birthcert_series", oldValue, _internal_birthcert_series));
        }
    }

    public function set birthcert_number(value:String) : void
    {
        var oldValue:String = _internal_birthcert_number;
        if (oldValue !== value)
        {
            _internal_birthcert_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "birthcert_number", oldValue, _internal_birthcert_number));
        }
    }

    public function set birthcert_issuing(value:String) : void
    {
        var oldValue:String = _internal_birthcert_issuing;
        if (oldValue !== value)
        {
            _internal_birthcert_issuing = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "birthcert_issuing", oldValue, _internal_birthcert_issuing));
        }
    }

    public function set birthcert_date(value:String) : void
    {
        var oldValue:String = _internal_birthcert_date;
        if (oldValue !== value)
        {
            _internal_birthcert_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "birthcert_date", oldValue, _internal_birthcert_date));
        }
    }

    public function set IS_MOTHER(value:int) : void
    {
        var oldValue:int = _internal_IS_MOTHER;
        if (oldValue !== value)
        {
            _internal_IS_MOTHER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "IS_MOTHER", oldValue, _internal_IS_MOTHER));
        }
    }

    public function set MOTHER_ID(value:int) : void
    {
        var oldValue:int = _internal_MOTHER_ID;
        if (oldValue !== value)
        {
            _internal_MOTHER_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHER_ID", oldValue, _internal_MOTHER_ID));
        }
    }

    public function set MOTHER_PASSPORT_SERIES(value:String) : void
    {
        var oldValue:String = _internal_MOTHER_PASSPORT_SERIES;
        if (oldValue !== value)
        {
            _internal_MOTHER_PASSPORT_SERIES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHER_PASSPORT_SERIES", oldValue, _internal_MOTHER_PASSPORT_SERIES));
        }
    }

    public function set MOTHER_PASSPORT_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_MOTHER_PASSPORT_NUMBER;
        if (oldValue !== value)
        {
            _internal_MOTHER_PASSPORT_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHER_PASSPORT_NUMBER", oldValue, _internal_MOTHER_PASSPORT_NUMBER));
        }
    }

    public function set MOTHER_PASSPORT_ISSUING(value:String) : void
    {
        var oldValue:String = _internal_MOTHER_PASSPORT_ISSUING;
        if (oldValue !== value)
        {
            _internal_MOTHER_PASSPORT_ISSUING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHER_PASSPORT_ISSUING", oldValue, _internal_MOTHER_PASSPORT_ISSUING));
        }
    }

    public function set MOTHER_PASSPORT_DATE(value:String) : void
    {
        var oldValue:String = _internal_MOTHER_PASSPORT_DATE;
        if (oldValue !== value)
        {
            _internal_MOTHER_PASSPORT_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOTHER_PASSPORT_DATE", oldValue, _internal_MOTHER_PASSPORT_DATE));
        }
    }

    public function set FATHER_ID(value:int) : void
    {
        var oldValue:int = _internal_FATHER_ID;
        if (oldValue !== value)
        {
            _internal_FATHER_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHER_ID", oldValue, _internal_FATHER_ID));
        }
    }

    public function set FATHER_PASSPORT_SERIES(value:String) : void
    {
        var oldValue:String = _internal_FATHER_PASSPORT_SERIES;
        if (oldValue !== value)
        {
            _internal_FATHER_PASSPORT_SERIES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHER_PASSPORT_SERIES", oldValue, _internal_FATHER_PASSPORT_SERIES));
        }
    }

    public function set FATHER_PASSPORT_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_FATHER_PASSPORT_NUMBER;
        if (oldValue !== value)
        {
            _internal_FATHER_PASSPORT_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHER_PASSPORT_NUMBER", oldValue, _internal_FATHER_PASSPORT_NUMBER));
        }
    }

    public function set FATHER_PASSPORT_ISSUING(value:String) : void
    {
        var oldValue:String = _internal_FATHER_PASSPORT_ISSUING;
        if (oldValue !== value)
        {
            _internal_FATHER_PASSPORT_ISSUING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHER_PASSPORT_ISSUING", oldValue, _internal_FATHER_PASSPORT_ISSUING));
        }
    }

    public function set FATHER_PASSPORT_DATE(value:String) : void
    {
        var oldValue:String = _internal_FATHER_PASSPORT_DATE;
        if (oldValue !== value)
        {
            _internal_FATHER_PASSPORT_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FATHER_PASSPORT_DATE", oldValue, _internal_FATHER_PASSPORT_DATE));
        }
    }

    public function set CONFLICTFLAG(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_CONFLICTFLAG;
        if (oldValue !== value)
        {
            _internal_CONFLICTFLAG = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CONFLICTFLAG", oldValue, _internal_CONFLICTFLAG));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PatientCardModuleAddPatientObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PatientCardModuleAddPatientObjectEntityMetadata) : void
    {
        var oldValue : _PatientCardModuleAddPatientObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
