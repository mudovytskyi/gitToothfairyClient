/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - TreatmentManagerModuleProtocol.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.TreatmentManagerModuleDoctor;
import valueObjects.TreatmentManagerModuleF43;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_TreatmentManagerModuleProtocol extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("TreatmentManagerModuleProtocol") == null)
            {
                flash.net.registerClassAlias("TreatmentManagerModuleProtocol", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("TreatmentManagerModuleProtocol", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.TreatmentManagerModuleF43.initRemoteClassAliasSingleChild();
        valueObjects.TreatmentManagerModuleDoctor.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _TreatmentManagerModuleProtocolEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_protocolID : String;
    private var _internal_protocolName : String;
    private var _internal_protocolF43 : int;
    private var _internal_f43 : ArrayCollection;
    model_internal var _internal_f43_leaf:valueObjects.TreatmentManagerModuleF43;
    private var _internal_protocolType : int;
    private var _internal_protocolTXT : String;
    private var _internal_protocolAnamnesTXT : String;
    private var _internal_protocolStatusTXT : String;
    private var _internal_protocolRecomendTXT : String;
    private var _internal_protocolCreateDate : String;
    private var _internal_doctor : valueObjects.TreatmentManagerModuleDoctor;
    private var _internal_templates_count : int;
    private var _internal_protocolEpicrisisTXT : String;
    private var _internal_protocolRentgenTXT : String;
    private var _internal_isQuick : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_TreatmentManagerModuleProtocol()
    {
        _model = new _TreatmentManagerModuleProtocolEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get protocolID() : String
    {
        return _internal_protocolID;
    }

    [Bindable(event="propertyChange")]
    public function get protocolName() : String
    {
        return _internal_protocolName;
    }

    [Bindable(event="propertyChange")]
    public function get protocolF43() : int
    {
        return _internal_protocolF43;
    }

    [Bindable(event="propertyChange")]
    public function get f43() : ArrayCollection
    {
        return _internal_f43;
    }

    [Bindable(event="propertyChange")]
    public function get protocolType() : int
    {
        return _internal_protocolType;
    }

    [Bindable(event="propertyChange")]
    public function get protocolTXT() : String
    {
        return _internal_protocolTXT;
    }

    [Bindable(event="propertyChange")]
    public function get protocolAnamnesTXT() : String
    {
        return _internal_protocolAnamnesTXT;
    }

    [Bindable(event="propertyChange")]
    public function get protocolStatusTXT() : String
    {
        return _internal_protocolStatusTXT;
    }

    [Bindable(event="propertyChange")]
    public function get protocolRecomendTXT() : String
    {
        return _internal_protocolRecomendTXT;
    }

    [Bindable(event="propertyChange")]
    public function get protocolCreateDate() : String
    {
        return _internal_protocolCreateDate;
    }

    [Bindable(event="propertyChange")]
    public function get doctor() : valueObjects.TreatmentManagerModuleDoctor
    {
        return _internal_doctor;
    }

    [Bindable(event="propertyChange")]
    public function get templates_count() : int
    {
        return _internal_templates_count;
    }

    [Bindable(event="propertyChange")]
    public function get protocolEpicrisisTXT() : String
    {
        return _internal_protocolEpicrisisTXT;
    }

    [Bindable(event="propertyChange")]
    public function get protocolRentgenTXT() : String
    {
        return _internal_protocolRentgenTXT;
    }

    [Bindable(event="propertyChange")]
    public function get isQuick() : Boolean
    {
        return _internal_isQuick;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set protocolID(value:String) : void
    {
        var oldValue:String = _internal_protocolID;
        if (oldValue !== value)
        {
            _internal_protocolID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolID", oldValue, _internal_protocolID));
        }
    }

    public function set protocolName(value:String) : void
    {
        var oldValue:String = _internal_protocolName;
        if (oldValue !== value)
        {
            _internal_protocolName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolName", oldValue, _internal_protocolName));
        }
    }

    public function set protocolF43(value:int) : void
    {
        var oldValue:int = _internal_protocolF43;
        if (oldValue !== value)
        {
            _internal_protocolF43 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolF43", oldValue, _internal_protocolF43));
        }
    }

    public function set f43(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_f43;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_f43 = value;
            }
            else if (value is Array)
            {
                _internal_f43 = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_f43 = null;
            }
            else
            {
                throw new Error("value of f43 must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "f43", oldValue, _internal_f43));
        }
    }

    public function set protocolType(value:int) : void
    {
        var oldValue:int = _internal_protocolType;
        if (oldValue !== value)
        {
            _internal_protocolType = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolType", oldValue, _internal_protocolType));
        }
    }

    public function set protocolTXT(value:String) : void
    {
        var oldValue:String = _internal_protocolTXT;
        if (oldValue !== value)
        {
            _internal_protocolTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolTXT", oldValue, _internal_protocolTXT));
        }
    }

    public function set protocolAnamnesTXT(value:String) : void
    {
        var oldValue:String = _internal_protocolAnamnesTXT;
        if (oldValue !== value)
        {
            _internal_protocolAnamnesTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolAnamnesTXT", oldValue, _internal_protocolAnamnesTXT));
        }
    }

    public function set protocolStatusTXT(value:String) : void
    {
        var oldValue:String = _internal_protocolStatusTXT;
        if (oldValue !== value)
        {
            _internal_protocolStatusTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolStatusTXT", oldValue, _internal_protocolStatusTXT));
        }
    }

    public function set protocolRecomendTXT(value:String) : void
    {
        var oldValue:String = _internal_protocolRecomendTXT;
        if (oldValue !== value)
        {
            _internal_protocolRecomendTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolRecomendTXT", oldValue, _internal_protocolRecomendTXT));
        }
    }

    public function set protocolCreateDate(value:String) : void
    {
        var oldValue:String = _internal_protocolCreateDate;
        if (oldValue !== value)
        {
            _internal_protocolCreateDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolCreateDate", oldValue, _internal_protocolCreateDate));
        }
    }

    public function set doctor(value:valueObjects.TreatmentManagerModuleDoctor) : void
    {
        var oldValue:valueObjects.TreatmentManagerModuleDoctor = _internal_doctor;
        if (oldValue !== value)
        {
            _internal_doctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor", oldValue, _internal_doctor));
        }
    }

    public function set templates_count(value:int) : void
    {
        var oldValue:int = _internal_templates_count;
        if (oldValue !== value)
        {
            _internal_templates_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templates_count", oldValue, _internal_templates_count));
        }
    }

    public function set protocolEpicrisisTXT(value:String) : void
    {
        var oldValue:String = _internal_protocolEpicrisisTXT;
        if (oldValue !== value)
        {
            _internal_protocolEpicrisisTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolEpicrisisTXT", oldValue, _internal_protocolEpicrisisTXT));
        }
    }

    public function set protocolRentgenTXT(value:String) : void
    {
        var oldValue:String = _internal_protocolRentgenTXT;
        if (oldValue !== value)
        {
            _internal_protocolRentgenTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolRentgenTXT", oldValue, _internal_protocolRentgenTXT));
        }
    }

    public function set isQuick(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isQuick;
        if (oldValue !== value)
        {
            _internal_isQuick = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isQuick", oldValue, _internal_isQuick));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _TreatmentManagerModuleProtocolEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _TreatmentManagerModuleProtocolEntityMetadata) : void
    {
        var oldValue : _TreatmentManagerModuleProtocolEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
