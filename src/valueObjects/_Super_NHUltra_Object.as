/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - NHUltra_Object.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_NHUltra_Object extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("NHUltra_Object") == null)
            {
                flash.net.registerClassAlias("NHUltra_Object", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("NHUltra_Object", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _NHUltra_ObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_docDate : String;
    private var _internal_createTimestamp : String;
    private var _internal_conclusion : String;
    private var _internal_author_id : String;
    private var _internal_author_name : String;
    private var _internal_subdivision_id : String;
    private var _internal_subdivision_name : String;
    private var _internal_doctor_id : String;
    private var _internal_doctor_name : String;
    private var _internal_patient_id : String;
    private var _internal_template_id : String;
    private var _internal_template_label : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_NHUltra_Object()
    {
        _model = new _NHUltra_ObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get docDate() : String
    {
        return _internal_docDate;
    }

    [Bindable(event="propertyChange")]
    public function get createTimestamp() : String
    {
        return _internal_createTimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get conclusion() : String
    {
        return _internal_conclusion;
    }

    [Bindable(event="propertyChange")]
    public function get author_id() : String
    {
        return _internal_author_id;
    }

    [Bindable(event="propertyChange")]
    public function get author_name() : String
    {
        return _internal_author_name;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_id() : String
    {
        return _internal_subdivision_id;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_name() : String
    {
        return _internal_subdivision_name;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_name() : String
    {
        return _internal_doctor_name;
    }

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get template_id() : String
    {
        return _internal_template_id;
    }

    [Bindable(event="propertyChange")]
    public function get template_label() : String
    {
        return _internal_template_label;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set docDate(value:String) : void
    {
        var oldValue:String = _internal_docDate;
        if (oldValue !== value)
        {
            _internal_docDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "docDate", oldValue, _internal_docDate));
        }
    }

    public function set createTimestamp(value:String) : void
    {
        var oldValue:String = _internal_createTimestamp;
        if (oldValue !== value)
        {
            _internal_createTimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "createTimestamp", oldValue, _internal_createTimestamp));
        }
    }

    public function set conclusion(value:String) : void
    {
        var oldValue:String = _internal_conclusion;
        if (oldValue !== value)
        {
            _internal_conclusion = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "conclusion", oldValue, _internal_conclusion));
        }
    }

    public function set author_id(value:String) : void
    {
        var oldValue:String = _internal_author_id;
        if (oldValue !== value)
        {
            _internal_author_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_id", oldValue, _internal_author_id));
        }
    }

    public function set author_name(value:String) : void
    {
        var oldValue:String = _internal_author_name;
        if (oldValue !== value)
        {
            _internal_author_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_name", oldValue, _internal_author_name));
        }
    }

    public function set subdivision_id(value:String) : void
    {
        var oldValue:String = _internal_subdivision_id;
        if (oldValue !== value)
        {
            _internal_subdivision_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_id", oldValue, _internal_subdivision_id));
        }
    }

    public function set subdivision_name(value:String) : void
    {
        var oldValue:String = _internal_subdivision_name;
        if (oldValue !== value)
        {
            _internal_subdivision_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_name", oldValue, _internal_subdivision_name));
        }
    }

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_name(value:String) : void
    {
        var oldValue:String = _internal_doctor_name;
        if (oldValue !== value)
        {
            _internal_doctor_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_name", oldValue, _internal_doctor_name));
        }
    }

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set template_id(value:String) : void
    {
        var oldValue:String = _internal_template_id;
        if (oldValue !== value)
        {
            _internal_template_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "template_id", oldValue, _internal_template_id));
        }
    }

    public function set template_label(value:String) : void
    {
        var oldValue:String = _internal_template_label;
        if (oldValue !== value)
        {
            _internal_template_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "template_label", oldValue, _internal_template_label));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _NHUltra_ObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _NHUltra_ObjectEntityMetadata) : void
    {
        var oldValue : _NHUltra_ObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
