
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _AssistantsModuleAssistantEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ID", "TIN", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "DATEWORKSTART", "DATEWORKEND", "MOBILEPHONE", "HOMEPHONE", "EMAIL", "SKYPE", "HOMEADRESS", "POSTALCODE", "STATE", "REGION", "CITY", "BARCODE", "isFired", "fingerid");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ID", "TIN", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "DATEWORKSTART", "DATEWORKEND", "MOBILEPHONE", "HOMEPHONE", "EMAIL", "SKYPE", "HOMEADRESS", "POSTALCODE", "STATE", "REGION", "CITY", "BARCODE", "isFired", "fingerid");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ID", "TIN", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "DATEWORKSTART", "DATEWORKEND", "MOBILEPHONE", "HOMEPHONE", "EMAIL", "SKYPE", "HOMEADRESS", "POSTALCODE", "STATE", "REGION", "CITY", "BARCODE", "isFired", "fingerid");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ID", "TIN", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "DATEWORKSTART", "DATEWORKEND", "MOBILEPHONE", "HOMEPHONE", "EMAIL", "SKYPE", "HOMEADRESS", "POSTALCODE", "STATE", "REGION", "CITY", "BARCODE", "isFired", "fingerid");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "AssistantsModuleAssistant";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_AssistantsModuleAssistant;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _AssistantsModuleAssistantEntityMetadata(value : _Super_AssistantsModuleAssistant)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["TIN"] = new Array();
            model_internal::dependentsOnMap["FIRSTNAME"] = new Array();
            model_internal::dependentsOnMap["MIDDLENAME"] = new Array();
            model_internal::dependentsOnMap["LASTNAME"] = new Array();
            model_internal::dependentsOnMap["FULLNAME"] = new Array();
            model_internal::dependentsOnMap["SHORTNAME"] = new Array();
            model_internal::dependentsOnMap["BIRTHDAY"] = new Array();
            model_internal::dependentsOnMap["SEX"] = new Array();
            model_internal::dependentsOnMap["DATEWORKSTART"] = new Array();
            model_internal::dependentsOnMap["DATEWORKEND"] = new Array();
            model_internal::dependentsOnMap["MOBILEPHONE"] = new Array();
            model_internal::dependentsOnMap["HOMEPHONE"] = new Array();
            model_internal::dependentsOnMap["EMAIL"] = new Array();
            model_internal::dependentsOnMap["SKYPE"] = new Array();
            model_internal::dependentsOnMap["HOMEADRESS"] = new Array();
            model_internal::dependentsOnMap["POSTALCODE"] = new Array();
            model_internal::dependentsOnMap["STATE"] = new Array();
            model_internal::dependentsOnMap["REGION"] = new Array();
            model_internal::dependentsOnMap["CITY"] = new Array();
            model_internal::dependentsOnMap["BARCODE"] = new Array();
            model_internal::dependentsOnMap["isFired"] = new Array();
            model_internal::dependentsOnMap["fingerid"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ID"] = "String";
        model_internal::propertyTypeMap["TIN"] = "String";
        model_internal::propertyTypeMap["FIRSTNAME"] = "String";
        model_internal::propertyTypeMap["MIDDLENAME"] = "String";
        model_internal::propertyTypeMap["LASTNAME"] = "String";
        model_internal::propertyTypeMap["FULLNAME"] = "String";
        model_internal::propertyTypeMap["SHORTNAME"] = "String";
        model_internal::propertyTypeMap["BIRTHDAY"] = "String";
        model_internal::propertyTypeMap["SEX"] = "String";
        model_internal::propertyTypeMap["DATEWORKSTART"] = "String";
        model_internal::propertyTypeMap["DATEWORKEND"] = "String";
        model_internal::propertyTypeMap["MOBILEPHONE"] = "String";
        model_internal::propertyTypeMap["HOMEPHONE"] = "String";
        model_internal::propertyTypeMap["EMAIL"] = "String";
        model_internal::propertyTypeMap["SKYPE"] = "String";
        model_internal::propertyTypeMap["HOMEADRESS"] = "String";
        model_internal::propertyTypeMap["POSTALCODE"] = "String";
        model_internal::propertyTypeMap["STATE"] = "String";
        model_internal::propertyTypeMap["REGION"] = "String";
        model_internal::propertyTypeMap["CITY"] = "String";
        model_internal::propertyTypeMap["BARCODE"] = "String";
        model_internal::propertyTypeMap["isFired"] = "int";
        model_internal::propertyTypeMap["fingerid"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity AssistantsModuleAssistant");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity AssistantsModuleAssistant");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of AssistantsModuleAssistant");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AssistantsModuleAssistant");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity AssistantsModuleAssistant");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AssistantsModuleAssistant");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTINAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFIRSTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMIDDLENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLASTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFULLNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBIRTHDAYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSEXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDATEWORKSTARTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDATEWORKENDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOBILEPHONEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHOMEPHONEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEMAILAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSKYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHOMEADRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPOSTALCODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSTATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isREGIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBARCODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIsFiredAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFingeridAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TINStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FIRSTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MIDDLENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LASTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FULLNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BIRTHDAYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SEXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DATEWORKSTARTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DATEWORKENDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOBILEPHONEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HOMEPHONEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EMAILStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SKYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HOMEADRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get POSTALCODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get STATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get REGIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BARCODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get isFiredStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get fingeridStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
