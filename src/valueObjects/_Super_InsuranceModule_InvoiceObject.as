/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - InsuranceModule_InvoiceObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_InsuranceModule_InvoiceObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("InsuranceModule_InvoiceObject") == null)
            {
                flash.net.registerClassAlias("InsuranceModule_InvoiceObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("InsuranceModule_InvoiceObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _InsuranceModule_InvoiceObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_name : String;
    private var _internal_type : String;
    private var _internal_invoice_number : String;
    private var _internal_invoice_date : String;
    private var _internal_summClosed : Number;
    private var _internal_summ : Number;
    private var _internal_doctor_id : String;
    private var _internal_doctor_lname : String;
    private var _internal_doctor_fname : String;
    private var _internal_doctor_sname : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_InsuranceModule_InvoiceObject()
    {
        _model = new _InsuranceModule_InvoiceObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get name() : String
    {
        return _internal_name;
    }

    [Bindable(event="propertyChange")]
    public function get type() : String
    {
        return _internal_type;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_number() : String
    {
        return _internal_invoice_number;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_date() : String
    {
        return _internal_invoice_date;
    }

    [Bindable(event="propertyChange")]
    public function get summClosed() : Number
    {
        return _internal_summClosed;
    }

    [Bindable(event="propertyChange")]
    public function get summ() : Number
    {
        return _internal_summ;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lname() : String
    {
        return _internal_doctor_lname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_fname() : String
    {
        return _internal_doctor_fname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_sname() : String
    {
        return _internal_doctor_sname;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set name(value:String) : void
    {
        var oldValue:String = _internal_name;
        if (oldValue !== value)
        {
            _internal_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name", oldValue, _internal_name));
        }
    }

    public function set type(value:String) : void
    {
        var oldValue:String = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    public function set invoice_number(value:String) : void
    {
        var oldValue:String = _internal_invoice_number;
        if (oldValue !== value)
        {
            _internal_invoice_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_number", oldValue, _internal_invoice_number));
        }
    }

    public function set invoice_date(value:String) : void
    {
        var oldValue:String = _internal_invoice_date;
        if (oldValue !== value)
        {
            _internal_invoice_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_date", oldValue, _internal_invoice_date));
        }
    }

    public function set summClosed(value:Number) : void
    {
        var oldValue:Number = _internal_summClosed;
        if (isNaN(_internal_summClosed) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_summClosed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "summClosed", oldValue, _internal_summClosed));
        }
    }

    public function set summ(value:Number) : void
    {
        var oldValue:Number = _internal_summ;
        if (isNaN(_internal_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "summ", oldValue, _internal_summ));
        }
    }

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_lname(value:String) : void
    {
        var oldValue:String = _internal_doctor_lname;
        if (oldValue !== value)
        {
            _internal_doctor_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lname", oldValue, _internal_doctor_lname));
        }
    }

    public function set doctor_fname(value:String) : void
    {
        var oldValue:String = _internal_doctor_fname;
        if (oldValue !== value)
        {
            _internal_doctor_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_fname", oldValue, _internal_doctor_fname));
        }
    }

    public function set doctor_sname(value:String) : void
    {
        var oldValue:String = _internal_doctor_sname;
        if (oldValue !== value)
        {
            _internal_doctor_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_sname", oldValue, _internal_doctor_sname));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _InsuranceModule_InvoiceObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _InsuranceModule_InvoiceObjectEntityMetadata) : void
    {
        var oldValue : _InsuranceModule_InvoiceObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
