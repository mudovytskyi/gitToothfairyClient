/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HospitalCardDataBrief.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HospitalCardDataBrief extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HospitalCardDataBrief") == null)
            {
                flash.net.registerClassAlias("HospitalCardDataBrief", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HospitalCardDataBrief", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _HospitalCardDataBriefEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_PATIENT_ID : String;
    private var _internal_CARD_NUMBER : String;
    private var _internal_CREATED : String;
    private var _internal_OPENED : String;
    private var _internal_CLOSED : String;
    private var _internal_DOCTOR_FULL_NAME_DIAGNOS : String;
    private var _internal_DOCTOR_FULL_NAME_OUTCOME : String;
    private var _internal_INCOME_DIAGNOSIS : String;
    private var _internal_INCOME_DIAGNOSIS_MKH : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HospitalCardDataBrief()
    {
        _model = new _HospitalCardDataBriefEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_ID() : String
    {
        return _internal_PATIENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get CARD_NUMBER() : String
    {
        return _internal_CARD_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CREATED() : String
    {
        return _internal_CREATED;
    }

    [Bindable(event="propertyChange")]
    public function get OPENED() : String
    {
        return _internal_OPENED;
    }

    [Bindable(event="propertyChange")]
    public function get CLOSED() : String
    {
        return _internal_CLOSED;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_FULL_NAME_DIAGNOS() : String
    {
        return _internal_DOCTOR_FULL_NAME_DIAGNOS;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_FULL_NAME_OUTCOME() : String
    {
        return _internal_DOCTOR_FULL_NAME_OUTCOME;
    }

    [Bindable(event="propertyChange")]
    public function get INCOME_DIAGNOSIS() : String
    {
        return _internal_INCOME_DIAGNOSIS;
    }

    [Bindable(event="propertyChange")]
    public function get INCOME_DIAGNOSIS_MKH() : String
    {
        return _internal_INCOME_DIAGNOSIS_MKH;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set PATIENT_ID(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_ID;
        if (oldValue !== value)
        {
            _internal_PATIENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_ID", oldValue, _internal_PATIENT_ID));
        }
    }

    public function set CARD_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARD_NUMBER;
        if (oldValue !== value)
        {
            _internal_CARD_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARD_NUMBER", oldValue, _internal_CARD_NUMBER));
        }
    }

    public function set CREATED(value:String) : void
    {
        var oldValue:String = _internal_CREATED;
        if (oldValue !== value)
        {
            _internal_CREATED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CREATED", oldValue, _internal_CREATED));
        }
    }

    public function set OPENED(value:String) : void
    {
        var oldValue:String = _internal_OPENED;
        if (oldValue !== value)
        {
            _internal_OPENED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OPENED", oldValue, _internal_OPENED));
        }
    }

    public function set CLOSED(value:String) : void
    {
        var oldValue:String = _internal_CLOSED;
        if (oldValue !== value)
        {
            _internal_CLOSED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLOSED", oldValue, _internal_CLOSED));
        }
    }

    public function set DOCTOR_FULL_NAME_DIAGNOS(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_FULL_NAME_DIAGNOS;
        if (oldValue !== value)
        {
            _internal_DOCTOR_FULL_NAME_DIAGNOS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_FULL_NAME_DIAGNOS", oldValue, _internal_DOCTOR_FULL_NAME_DIAGNOS));
        }
    }

    public function set DOCTOR_FULL_NAME_OUTCOME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_FULL_NAME_OUTCOME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_FULL_NAME_OUTCOME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_FULL_NAME_OUTCOME", oldValue, _internal_DOCTOR_FULL_NAME_OUTCOME));
        }
    }

    public function set INCOME_DIAGNOSIS(value:String) : void
    {
        var oldValue:String = _internal_INCOME_DIAGNOSIS;
        if (oldValue !== value)
        {
            _internal_INCOME_DIAGNOSIS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INCOME_DIAGNOSIS", oldValue, _internal_INCOME_DIAGNOSIS));
        }
    }

    public function set INCOME_DIAGNOSIS_MKH(value:String) : void
    {
        var oldValue:String = _internal_INCOME_DIAGNOSIS_MKH;
        if (oldValue !== value)
        {
            _internal_INCOME_DIAGNOSIS_MKH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INCOME_DIAGNOSIS_MKH", oldValue, _internal_INCOME_DIAGNOSIS_MKH));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HospitalCardDataBriefEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HospitalCardDataBriefEntityMetadata) : void
    {
        var oldValue : _HospitalCardDataBriefEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
