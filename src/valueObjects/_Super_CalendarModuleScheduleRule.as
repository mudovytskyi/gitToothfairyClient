/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CalendarModuleScheduleRule.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CalendarModuleScheduleRule extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CalendarModuleScheduleRule") == null)
            {
                flash.net.registerClassAlias("CalendarModuleScheduleRule", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CalendarModuleScheduleRule", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _CalendarModuleScheduleRuleEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : int;
    private var _internal_name : Object;
    private var _internal_daysRange : Object;
    private var _internal_isWorking : Boolean;
    private var _internal_repeatType : int;
    private var _internal_skipDaysCount : int;
    private var _internal_cabinetid : int;
    private var _internal_startTime : Object;
    private var _internal_endTime : Object;
    private var _internal_cabinetNumber : Object;
    private var _internal_cabinetName : Object;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CalendarModuleScheduleRule()
    {
        _model = new _CalendarModuleScheduleRuleEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : int
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get name() : Object
    {
        return _internal_name;
    }

    [Bindable(event="propertyChange")]
    public function get daysRange() : Object
    {
        return _internal_daysRange;
    }

    [Bindable(event="propertyChange")]
    public function get isWorking() : Boolean
    {
        return _internal_isWorking;
    }

    [Bindable(event="propertyChange")]
    public function get repeatType() : int
    {
        return _internal_repeatType;
    }

    [Bindable(event="propertyChange")]
    public function get skipDaysCount() : int
    {
        return _internal_skipDaysCount;
    }

    [Bindable(event="propertyChange")]
    public function get cabinetid() : int
    {
        return _internal_cabinetid;
    }

    [Bindable(event="propertyChange")]
    public function get startTime() : Object
    {
        return _internal_startTime;
    }

    [Bindable(event="propertyChange")]
    public function get endTime() : Object
    {
        return _internal_endTime;
    }

    [Bindable(event="propertyChange")]
    public function get cabinetNumber() : Object
    {
        return _internal_cabinetNumber;
    }

    [Bindable(event="propertyChange")]
    public function get cabinetName() : Object
    {
        return _internal_cabinetName;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:int) : void
    {
        var oldValue:int = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set name(value:Object) : void
    {
        var oldValue:Object = _internal_name;
        if (oldValue !== value)
        {
            _internal_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name", oldValue, _internal_name));
        }
    }

    public function set daysRange(value:Object) : void
    {
        var oldValue:Object = _internal_daysRange;
        if (oldValue !== value)
        {
            _internal_daysRange = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "daysRange", oldValue, _internal_daysRange));
        }
    }

    public function set isWorking(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isWorking;
        if (oldValue !== value)
        {
            _internal_isWorking = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isWorking", oldValue, _internal_isWorking));
        }
    }

    public function set repeatType(value:int) : void
    {
        var oldValue:int = _internal_repeatType;
        if (oldValue !== value)
        {
            _internal_repeatType = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "repeatType", oldValue, _internal_repeatType));
        }
    }

    public function set skipDaysCount(value:int) : void
    {
        var oldValue:int = _internal_skipDaysCount;
        if (oldValue !== value)
        {
            _internal_skipDaysCount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "skipDaysCount", oldValue, _internal_skipDaysCount));
        }
    }

    public function set cabinetid(value:int) : void
    {
        var oldValue:int = _internal_cabinetid;
        if (oldValue !== value)
        {
            _internal_cabinetid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cabinetid", oldValue, _internal_cabinetid));
        }
    }

    public function set startTime(value:Object) : void
    {
        var oldValue:Object = _internal_startTime;
        if (oldValue !== value)
        {
            _internal_startTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "startTime", oldValue, _internal_startTime));
        }
    }

    public function set endTime(value:Object) : void
    {
        var oldValue:Object = _internal_endTime;
        if (oldValue !== value)
        {
            _internal_endTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "endTime", oldValue, _internal_endTime));
        }
    }

    public function set cabinetNumber(value:Object) : void
    {
        var oldValue:Object = _internal_cabinetNumber;
        if (oldValue !== value)
        {
            _internal_cabinetNumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cabinetNumber", oldValue, _internal_cabinetNumber));
        }
    }

    public function set cabinetName(value:Object) : void
    {
        var oldValue:Object = _internal_cabinetName;
        if (oldValue !== value)
        {
            _internal_cabinetName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cabinetName", oldValue, _internal_cabinetName));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CalendarModuleScheduleRuleEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CalendarModuleScheduleRuleEntityMetadata) : void
    {
        var oldValue : _CalendarModuleScheduleRuleEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
