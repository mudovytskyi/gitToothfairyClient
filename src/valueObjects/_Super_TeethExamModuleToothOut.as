/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - TeethExamModuleToothOut.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_TeethExamModuleToothOut extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("TeethExamModuleToothOut") == null)
            {
                flash.net.registerClassAlias("TeethExamModuleToothOut", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("TeethExamModuleToothOut", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _TeethExamModuleToothOutEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_examid : String;
    private var _internal_exam : String;
    private var _internal_examdate : String;
    private var _internal_checkup : String;
    private var _internal_complaints : String;
    private var _internal_doctor : String;
    private var _internal_assistant : String;
    private var _internal_channelsexam : String;
    private var _internal_bitetype : int;
    private var _internal_afterflag : int;
    private var _internal_course_fk : String;
    private var _internal_course_date : String;
    private var _internal_diseasehistory : String;
    private var _internal_treatmentplan : String;
    private var _internal_examplan : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_TeethExamModuleToothOut()
    {
        _model = new _TeethExamModuleToothOutEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get examid() : String
    {
        return _internal_examid;
    }

    [Bindable(event="propertyChange")]
    public function get exam() : String
    {
        return _internal_exam;
    }

    [Bindable(event="propertyChange")]
    public function get examdate() : String
    {
        return _internal_examdate;
    }

    [Bindable(event="propertyChange")]
    public function get checkup() : String
    {
        return _internal_checkup;
    }

    [Bindable(event="propertyChange")]
    public function get complaints() : String
    {
        return _internal_complaints;
    }

    [Bindable(event="propertyChange")]
    public function get doctor() : String
    {
        return _internal_doctor;
    }

    [Bindable(event="propertyChange")]
    public function get assistant() : String
    {
        return _internal_assistant;
    }

    [Bindable(event="propertyChange")]
    public function get channelsexam() : String
    {
        return _internal_channelsexam;
    }

    [Bindable(event="propertyChange")]
    public function get bitetype() : int
    {
        return _internal_bitetype;
    }

    [Bindable(event="propertyChange")]
    public function get afterflag() : int
    {
        return _internal_afterflag;
    }

    [Bindable(event="propertyChange")]
    public function get course_fk() : String
    {
        return _internal_course_fk;
    }

    [Bindable(event="propertyChange")]
    public function get course_date() : String
    {
        return _internal_course_date;
    }

    [Bindable(event="propertyChange")]
    public function get diseasehistory() : String
    {
        return _internal_diseasehistory;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentplan() : String
    {
        return _internal_treatmentplan;
    }

    [Bindable(event="propertyChange")]
    public function get examplan() : String
    {
        return _internal_examplan;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set examid(value:String) : void
    {
        var oldValue:String = _internal_examid;
        if (oldValue !== value)
        {
            _internal_examid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examid", oldValue, _internal_examid));
        }
    }

    public function set exam(value:String) : void
    {
        var oldValue:String = _internal_exam;
        if (oldValue !== value)
        {
            _internal_exam = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exam", oldValue, _internal_exam));
        }
    }

    public function set examdate(value:String) : void
    {
        var oldValue:String = _internal_examdate;
        if (oldValue !== value)
        {
            _internal_examdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examdate", oldValue, _internal_examdate));
        }
    }

    public function set checkup(value:String) : void
    {
        var oldValue:String = _internal_checkup;
        if (oldValue !== value)
        {
            _internal_checkup = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "checkup", oldValue, _internal_checkup));
        }
    }

    public function set complaints(value:String) : void
    {
        var oldValue:String = _internal_complaints;
        if (oldValue !== value)
        {
            _internal_complaints = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "complaints", oldValue, _internal_complaints));
        }
    }

    public function set doctor(value:String) : void
    {
        var oldValue:String = _internal_doctor;
        if (oldValue !== value)
        {
            _internal_doctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor", oldValue, _internal_doctor));
        }
    }

    public function set assistant(value:String) : void
    {
        var oldValue:String = _internal_assistant;
        if (oldValue !== value)
        {
            _internal_assistant = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistant", oldValue, _internal_assistant));
        }
    }

    public function set channelsexam(value:String) : void
    {
        var oldValue:String = _internal_channelsexam;
        if (oldValue !== value)
        {
            _internal_channelsexam = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "channelsexam", oldValue, _internal_channelsexam));
        }
    }

    public function set bitetype(value:int) : void
    {
        var oldValue:int = _internal_bitetype;
        if (oldValue !== value)
        {
            _internal_bitetype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "bitetype", oldValue, _internal_bitetype));
        }
    }

    public function set afterflag(value:int) : void
    {
        var oldValue:int = _internal_afterflag;
        if (oldValue !== value)
        {
            _internal_afterflag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "afterflag", oldValue, _internal_afterflag));
        }
    }

    public function set course_fk(value:String) : void
    {
        var oldValue:String = _internal_course_fk;
        if (oldValue !== value)
        {
            _internal_course_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "course_fk", oldValue, _internal_course_fk));
        }
    }

    public function set course_date(value:String) : void
    {
        var oldValue:String = _internal_course_date;
        if (oldValue !== value)
        {
            _internal_course_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "course_date", oldValue, _internal_course_date));
        }
    }

    public function set diseasehistory(value:String) : void
    {
        var oldValue:String = _internal_diseasehistory;
        if (oldValue !== value)
        {
            _internal_diseasehistory = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diseasehistory", oldValue, _internal_diseasehistory));
        }
    }

    public function set treatmentplan(value:String) : void
    {
        var oldValue:String = _internal_treatmentplan;
        if (oldValue !== value)
        {
            _internal_treatmentplan = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentplan", oldValue, _internal_treatmentplan));
        }
    }

    public function set examplan(value:String) : void
    {
        var oldValue:String = _internal_examplan;
        if (oldValue !== value)
        {
            _internal_examplan = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examplan", oldValue, _internal_examplan));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _TeethExamModuleToothOutEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _TeethExamModuleToothOutEntityMetadata) : void
    {
        var oldValue : _TeethExamModuleToothOutEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
