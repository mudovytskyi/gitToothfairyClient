/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AWDDoctorModuleTableTask.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AWDDoctorModuleTableTask extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AWDDoctorModuleTableTask") == null)
            {
                flash.net.registerClassAlias("AWDDoctorModuleTableTask", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AWDDoctorModuleTableTask", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AWDDoctorModuleTableTaskEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_TASKDATE : String;
    private var _internal_TASKBEGINOFTHEINTERVAL : String;
    private var _internal_TASKENDOFTHEINTERVAL : String;
    private var _internal_TASKPATIENTID : String;
    private var _internal_TASKDOCTORID : String;
    private var _internal_TASKROOMID : String;
    private var _internal_TASKWORKPLACEID : String;
    private var _internal_TASKWORKDESCRIPTION : String;
    private var _internal_TASKCOMMENT : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AWDDoctorModuleTableTask()
    {
        _model = new _AWDDoctorModuleTableTaskEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get TASKDATE() : String
    {
        return _internal_TASKDATE;
    }

    [Bindable(event="propertyChange")]
    public function get TASKBEGINOFTHEINTERVAL() : String
    {
        return _internal_TASKBEGINOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get TASKENDOFTHEINTERVAL() : String
    {
        return _internal_TASKENDOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get TASKPATIENTID() : String
    {
        return _internal_TASKPATIENTID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKDOCTORID() : String
    {
        return _internal_TASKDOCTORID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKROOMID() : String
    {
        return _internal_TASKROOMID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKWORKPLACEID() : String
    {
        return _internal_TASKWORKPLACEID;
    }

    [Bindable(event="propertyChange")]
    public function get TASKWORKDESCRIPTION() : String
    {
        return _internal_TASKWORKDESCRIPTION;
    }

    [Bindable(event="propertyChange")]
    public function get TASKCOMMENT() : String
    {
        return _internal_TASKCOMMENT;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set TASKDATE(value:String) : void
    {
        var oldValue:String = _internal_TASKDATE;
        if (oldValue !== value)
        {
            _internal_TASKDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKDATE", oldValue, _internal_TASKDATE));
        }
    }

    public function set TASKBEGINOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TASKBEGINOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TASKBEGINOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKBEGINOFTHEINTERVAL", oldValue, _internal_TASKBEGINOFTHEINTERVAL));
        }
    }

    public function set TASKENDOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TASKENDOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TASKENDOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKENDOFTHEINTERVAL", oldValue, _internal_TASKENDOFTHEINTERVAL));
        }
    }

    public function set TASKPATIENTID(value:String) : void
    {
        var oldValue:String = _internal_TASKPATIENTID;
        if (oldValue !== value)
        {
            _internal_TASKPATIENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKPATIENTID", oldValue, _internal_TASKPATIENTID));
        }
    }

    public function set TASKDOCTORID(value:String) : void
    {
        var oldValue:String = _internal_TASKDOCTORID;
        if (oldValue !== value)
        {
            _internal_TASKDOCTORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKDOCTORID", oldValue, _internal_TASKDOCTORID));
        }
    }

    public function set TASKROOMID(value:String) : void
    {
        var oldValue:String = _internal_TASKROOMID;
        if (oldValue !== value)
        {
            _internal_TASKROOMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKROOMID", oldValue, _internal_TASKROOMID));
        }
    }

    public function set TASKWORKPLACEID(value:String) : void
    {
        var oldValue:String = _internal_TASKWORKPLACEID;
        if (oldValue !== value)
        {
            _internal_TASKWORKPLACEID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKWORKPLACEID", oldValue, _internal_TASKWORKPLACEID));
        }
    }

    public function set TASKWORKDESCRIPTION(value:String) : void
    {
        var oldValue:String = _internal_TASKWORKDESCRIPTION;
        if (oldValue !== value)
        {
            _internal_TASKWORKDESCRIPTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKWORKDESCRIPTION", oldValue, _internal_TASKWORKDESCRIPTION));
        }
    }

    public function set TASKCOMMENT(value:String) : void
    {
        var oldValue:String = _internal_TASKCOMMENT;
        if (oldValue !== value)
        {
            _internal_TASKCOMMENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASKCOMMENT", oldValue, _internal_TASKCOMMENT));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AWDDoctorModuleTableTaskEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AWDDoctorModuleTableTaskEntityMetadata) : void
    {
        var oldValue : _AWDDoctorModuleTableTaskEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
