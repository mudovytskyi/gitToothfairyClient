/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - OnlineRegistrationSettings.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_OnlineRegistrationSettings extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("OnlineRegistrationSettings") == null)
            {
                flash.net.registerClassAlias("OnlineRegistrationSettings", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("OnlineRegistrationSettings", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _OnlineRegistrationSettingsEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_isRequiredSurname : Boolean;
    private var _internal_isRequiredName : Boolean;
    private var _internal_isRequiredMiddlename : Boolean;
    private var _internal_isRequiredPhone : Boolean;
    private var _internal_isRequiredEmail : Boolean;
    private var _internal_borderColor : String;
    private var _internal_backgroundColor : String;
    private var _internal_language : String;
    private var _internal_ReadImagesFromDB : Boolean;
    private var _internal_DaysAndTimeClinicWork : ArrayCollection;
    private var _internal_CountMinutesForOnlineReceptin : int;
    private var _internal_CountDayFromRegistration : Object;
    private var _internal_isConsiderSchedule : Boolean;
    private var _internal_countryPhoneCode : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_OnlineRegistrationSettings()
    {
        _model = new _OnlineRegistrationSettingsEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get isRequiredSurname() : Boolean
    {
        return _internal_isRequiredSurname;
    }

    [Bindable(event="propertyChange")]
    public function get isRequiredName() : Boolean
    {
        return _internal_isRequiredName;
    }

    [Bindable(event="propertyChange")]
    public function get isRequiredMiddlename() : Boolean
    {
        return _internal_isRequiredMiddlename;
    }

    [Bindable(event="propertyChange")]
    public function get isRequiredPhone() : Boolean
    {
        return _internal_isRequiredPhone;
    }

    [Bindable(event="propertyChange")]
    public function get isRequiredEmail() : Boolean
    {
        return _internal_isRequiredEmail;
    }

    [Bindable(event="propertyChange")]
    public function get borderColor() : String
    {
        return _internal_borderColor;
    }

    [Bindable(event="propertyChange")]
    public function get backgroundColor() : String
    {
        return _internal_backgroundColor;
    }

    [Bindable(event="propertyChange")]
    public function get language() : String
    {
        return _internal_language;
    }

    [Bindable(event="propertyChange")]
    public function get ReadImagesFromDB() : Boolean
    {
        return _internal_ReadImagesFromDB;
    }

    [Bindable(event="propertyChange")]
    public function get DaysAndTimeClinicWork() : ArrayCollection
    {
        return _internal_DaysAndTimeClinicWork;
    }

    [Bindable(event="propertyChange")]
    public function get CountMinutesForOnlineReceptin() : int
    {
        return _internal_CountMinutesForOnlineReceptin;
    }

    [Bindable(event="propertyChange")]
    public function get CountDayFromRegistration() : Object
    {
        return _internal_CountDayFromRegistration;
    }

    [Bindable(event="propertyChange")]
    public function get isConsiderSchedule() : Boolean
    {
        return _internal_isConsiderSchedule;
    }

    [Bindable(event="propertyChange")]
    public function get countryPhoneCode() : String
    {
        return _internal_countryPhoneCode;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set isRequiredSurname(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isRequiredSurname;
        if (oldValue !== value)
        {
            _internal_isRequiredSurname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isRequiredSurname", oldValue, _internal_isRequiredSurname));
        }
    }

    public function set isRequiredName(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isRequiredName;
        if (oldValue !== value)
        {
            _internal_isRequiredName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isRequiredName", oldValue, _internal_isRequiredName));
        }
    }

    public function set isRequiredMiddlename(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isRequiredMiddlename;
        if (oldValue !== value)
        {
            _internal_isRequiredMiddlename = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isRequiredMiddlename", oldValue, _internal_isRequiredMiddlename));
        }
    }

    public function set isRequiredPhone(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isRequiredPhone;
        if (oldValue !== value)
        {
            _internal_isRequiredPhone = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isRequiredPhone", oldValue, _internal_isRequiredPhone));
        }
    }

    public function set isRequiredEmail(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isRequiredEmail;
        if (oldValue !== value)
        {
            _internal_isRequiredEmail = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isRequiredEmail", oldValue, _internal_isRequiredEmail));
        }
    }

    public function set borderColor(value:String) : void
    {
        var oldValue:String = _internal_borderColor;
        if (oldValue !== value)
        {
            _internal_borderColor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "borderColor", oldValue, _internal_borderColor));
        }
    }

    public function set backgroundColor(value:String) : void
    {
        var oldValue:String = _internal_backgroundColor;
        if (oldValue !== value)
        {
            _internal_backgroundColor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "backgroundColor", oldValue, _internal_backgroundColor));
        }
    }

    public function set language(value:String) : void
    {
        var oldValue:String = _internal_language;
        if (oldValue !== value)
        {
            _internal_language = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "language", oldValue, _internal_language));
        }
    }

    public function set ReadImagesFromDB(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_ReadImagesFromDB;
        if (oldValue !== value)
        {
            _internal_ReadImagesFromDB = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ReadImagesFromDB", oldValue, _internal_ReadImagesFromDB));
        }
    }

    public function set DaysAndTimeClinicWork(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_DaysAndTimeClinicWork;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_DaysAndTimeClinicWork = value;
            }
            else if (value is Array)
            {
                _internal_DaysAndTimeClinicWork = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_DaysAndTimeClinicWork = null;
            }
            else
            {
                throw new Error("value of DaysAndTimeClinicWork must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DaysAndTimeClinicWork", oldValue, _internal_DaysAndTimeClinicWork));
        }
    }

    public function set CountMinutesForOnlineReceptin(value:int) : void
    {
        var oldValue:int = _internal_CountMinutesForOnlineReceptin;
        if (oldValue !== value)
        {
            _internal_CountMinutesForOnlineReceptin = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CountMinutesForOnlineReceptin", oldValue, _internal_CountMinutesForOnlineReceptin));
        }
    }

    public function set CountDayFromRegistration(value:Object) : void
    {
        var oldValue:Object = _internal_CountDayFromRegistration;
        if (oldValue !== value)
        {
            _internal_CountDayFromRegistration = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CountDayFromRegistration", oldValue, _internal_CountDayFromRegistration));
        }
    }

    public function set isConsiderSchedule(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isConsiderSchedule;
        if (oldValue !== value)
        {
            _internal_isConsiderSchedule = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isConsiderSchedule", oldValue, _internal_isConsiderSchedule));
        }
    }

    public function set countryPhoneCode(value:String) : void
    {
        var oldValue:String = _internal_countryPhoneCode;
        if (oldValue !== value)
        {
            _internal_countryPhoneCode = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "countryPhoneCode", oldValue, _internal_countryPhoneCode));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _OnlineRegistrationSettingsEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _OnlineRegistrationSettingsEntityMetadata) : void
    {
        var oldValue : _OnlineRegistrationSettingsEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
