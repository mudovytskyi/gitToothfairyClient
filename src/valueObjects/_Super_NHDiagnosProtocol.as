/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - NHDiagnosProtocol.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.NHDiagnosTemplate;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_NHDiagnosProtocol extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("NHDiagnosProtocol") == null)
            {
                flash.net.registerClassAlias("NHDiagnosProtocol", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("NHDiagnosProtocol", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.NHDiagnosTemplate.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _NHDiagnosProtocolEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_diagnos2_id : String;
    private var _internal_protocol_id : String;
    private var _internal_protocol_name : String;
    private var _internal_selectedTemplate : valueObjects.NHDiagnosTemplate;
    private var _internal_templates : ArrayCollection;
    model_internal var _internal_templates_leaf:valueObjects.NHDiagnosTemplate;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_NHDiagnosProtocol()
    {
        _model = new _NHDiagnosProtocolEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get diagnos2_id() : String
    {
        return _internal_diagnos2_id;
    }

    [Bindable(event="propertyChange")]
    public function get protocol_id() : String
    {
        return _internal_protocol_id;
    }

    [Bindable(event="propertyChange")]
    public function get protocol_name() : String
    {
        return _internal_protocol_name;
    }

    [Bindable(event="propertyChange")]
    public function get selectedTemplate() : valueObjects.NHDiagnosTemplate
    {
        return _internal_selectedTemplate;
    }

    [Bindable(event="propertyChange")]
    public function get templates() : ArrayCollection
    {
        return _internal_templates;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set diagnos2_id(value:String) : void
    {
        var oldValue:String = _internal_diagnos2_id;
        if (oldValue !== value)
        {
            _internal_diagnos2_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diagnos2_id", oldValue, _internal_diagnos2_id));
        }
    }

    public function set protocol_id(value:String) : void
    {
        var oldValue:String = _internal_protocol_id;
        if (oldValue !== value)
        {
            _internal_protocol_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocol_id", oldValue, _internal_protocol_id));
        }
    }

    public function set protocol_name(value:String) : void
    {
        var oldValue:String = _internal_protocol_name;
        if (oldValue !== value)
        {
            _internal_protocol_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocol_name", oldValue, _internal_protocol_name));
        }
    }

    public function set selectedTemplate(value:valueObjects.NHDiagnosTemplate) : void
    {
        var oldValue:valueObjects.NHDiagnosTemplate = _internal_selectedTemplate;
        if (oldValue !== value)
        {
            _internal_selectedTemplate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "selectedTemplate", oldValue, _internal_selectedTemplate));
        }
    }

    public function set templates(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_templates;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_templates = value;
            }
            else if (value is Array)
            {
                _internal_templates = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_templates = null;
            }
            else
            {
                throw new Error("value of templates must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templates", oldValue, _internal_templates));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _NHDiagnosProtocolEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _NHDiagnosProtocolEntityMetadata) : void
    {
        var oldValue : _NHDiagnosProtocolEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
