/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - WarehouseModuleFarmCard.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.WarehouseModuleDoctor;
import valueObjects.WarehouseModuleFarm;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_WarehouseModuleFarmCard extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("WarehouseModuleFarmCard") == null)
            {
                flash.net.registerClassAlias("WarehouseModuleFarmCard", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("WarehouseModuleFarmCard", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.WarehouseModuleDoctor.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarm.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarmUnit.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarmPrice.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _WarehouseModuleFarmCardEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_QUANTITY : Number;
    private var _internal_DOCTOR : valueObjects.WarehouseModuleDoctor;
    private var _internal_ID : String;
    private var _internal_PRICE : Number;
    private var _internal_FARM : valueObjects.WarehouseModuleFarm;
    private var _internal_STATUS : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_WarehouseModuleFarmCard()
    {
        _model = new _WarehouseModuleFarmCardEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get QUANTITY() : Number
    {
        return _internal_QUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR() : valueObjects.WarehouseModuleDoctor
    {
        return _internal_DOCTOR;
    }

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PRICE() : Number
    {
        return _internal_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get FARM() : valueObjects.WarehouseModuleFarm
    {
        return _internal_FARM;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS() : String
    {
        return _internal_STATUS;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set QUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_QUANTITY;
        if (isNaN(_internal_QUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_QUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "QUANTITY", oldValue, _internal_QUANTITY));
        }
    }

    public function set DOCTOR(value:valueObjects.WarehouseModuleDoctor) : void
    {
        var oldValue:valueObjects.WarehouseModuleDoctor = _internal_DOCTOR;
        if (oldValue !== value)
        {
            _internal_DOCTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR", oldValue, _internal_DOCTOR));
        }
    }

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set PRICE(value:Number) : void
    {
        var oldValue:Number = _internal_PRICE;
        if (isNaN(_internal_PRICE) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRICE", oldValue, _internal_PRICE));
        }
    }

    public function set FARM(value:valueObjects.WarehouseModuleFarm) : void
    {
        var oldValue:valueObjects.WarehouseModuleFarm = _internal_FARM;
        if (oldValue !== value)
        {
            _internal_FARM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM", oldValue, _internal_FARM));
        }
    }

    public function set STATUS(value:String) : void
    {
        var oldValue:String = _internal_STATUS;
        if (oldValue !== value)
        {
            _internal_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS", oldValue, _internal_STATUS));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _WarehouseModuleFarmCardEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _WarehouseModuleFarmCardEntityMetadata) : void
    {
        var oldValue : _WarehouseModuleFarmCardEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
