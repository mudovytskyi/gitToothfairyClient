
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _EHealthModule_DeclarationEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("json", "id", "ptn_id", "ptn_fname", "ptn_lname", "ptn_mname", "ptn_sex", "ptn_birthday", "ptn_email", "ptn_taxid", "dec_id", "FK_SUBDIVISION", "FK_ACCOUNT", "ptn_birth_country", "ptn_birth_settlement", "ptn_secret", "ptn_doc_type", "ptn_doc_num", "emg_first_name", "emg_last_name", "emg_second_name", "cnf_enable", "cnf_rel_type", "cnf_first_name", "cnf_last_name", "cnf_second_name", "cnf_birthday", "cnf_birth_country", "cnf_birth_settlement", "cnf_sex", "cnf_taxid", "cnf_secret", "cnf_doc_type", "cnf_doc_num", "cnf_doc_rel_type", "cnf_doc_rel_num", "PATIENT_SIGNED", "PATIENT_CONSENT", "EH_DEC_ID", "AUTH_TYPE", "AUTH_PHONENUMBER", "STATUS", "account_eh_id", "subdivision_eh_id");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("json", "id", "ptn_id", "ptn_fname", "ptn_lname", "ptn_mname", "ptn_sex", "ptn_birthday", "ptn_email", "ptn_taxid", "dec_id", "FK_SUBDIVISION", "FK_ACCOUNT", "ptn_birth_country", "ptn_birth_settlement", "ptn_secret", "ptn_doc_type", "ptn_doc_num", "emg_first_name", "emg_last_name", "emg_second_name", "cnf_enable", "cnf_rel_type", "cnf_first_name", "cnf_last_name", "cnf_second_name", "cnf_birthday", "cnf_birth_country", "cnf_birth_settlement", "cnf_sex", "cnf_taxid", "cnf_secret", "cnf_doc_type", "cnf_doc_num", "cnf_doc_rel_type", "cnf_doc_rel_num", "PATIENT_SIGNED", "PATIENT_CONSENT", "EH_DEC_ID", "AUTH_TYPE", "AUTH_PHONENUMBER", "STATUS", "account_eh_id", "subdivision_eh_id");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("json", "id", "ptn_id", "ptn_fname", "ptn_lname", "ptn_mname", "ptn_sex", "ptn_birthday", "ptn_email", "ptn_taxid", "dec_id", "FK_SUBDIVISION", "FK_ACCOUNT", "ptn_birth_country", "ptn_birth_settlement", "ptn_secret", "ptn_doc_type", "ptn_doc_num", "emg_first_name", "emg_last_name", "emg_second_name", "cnf_enable", "cnf_rel_type", "cnf_first_name", "cnf_last_name", "cnf_second_name", "cnf_birthday", "cnf_birth_country", "cnf_birth_settlement", "cnf_sex", "cnf_taxid", "cnf_secret", "cnf_doc_type", "cnf_doc_num", "cnf_doc_rel_type", "cnf_doc_rel_num", "PATIENT_SIGNED", "PATIENT_CONSENT", "EH_DEC_ID", "AUTH_TYPE", "AUTH_PHONENUMBER", "STATUS", "account_eh_id", "subdivision_eh_id");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("json", "id", "ptn_id", "ptn_fname", "ptn_lname", "ptn_mname", "ptn_sex", "ptn_birthday", "ptn_email", "ptn_taxid", "dec_id", "FK_SUBDIVISION", "FK_ACCOUNT", "ptn_birth_country", "ptn_birth_settlement", "ptn_secret", "ptn_doc_type", "ptn_doc_num", "emg_first_name", "emg_last_name", "emg_second_name", "cnf_enable", "cnf_rel_type", "cnf_first_name", "cnf_last_name", "cnf_second_name", "cnf_birthday", "cnf_birth_country", "cnf_birth_settlement", "cnf_sex", "cnf_taxid", "cnf_secret", "cnf_doc_type", "cnf_doc_num", "cnf_doc_rel_type", "cnf_doc_rel_num", "PATIENT_SIGNED", "PATIENT_CONSENT", "EH_DEC_ID", "AUTH_TYPE", "AUTH_PHONENUMBER", "STATUS", "account_eh_id", "subdivision_eh_id");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "EHealthModule_Declaration";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_EHealthModule_Declaration;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _EHealthModule_DeclarationEntityMetadata(value : _Super_EHealthModule_Declaration)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["json"] = new Array();
            model_internal::dependentsOnMap["id"] = new Array();
            model_internal::dependentsOnMap["ptn_id"] = new Array();
            model_internal::dependentsOnMap["ptn_fname"] = new Array();
            model_internal::dependentsOnMap["ptn_lname"] = new Array();
            model_internal::dependentsOnMap["ptn_mname"] = new Array();
            model_internal::dependentsOnMap["ptn_sex"] = new Array();
            model_internal::dependentsOnMap["ptn_birthday"] = new Array();
            model_internal::dependentsOnMap["ptn_email"] = new Array();
            model_internal::dependentsOnMap["ptn_taxid"] = new Array();
            model_internal::dependentsOnMap["dec_id"] = new Array();
            model_internal::dependentsOnMap["FK_SUBDIVISION"] = new Array();
            model_internal::dependentsOnMap["FK_ACCOUNT"] = new Array();
            model_internal::dependentsOnMap["ptn_birth_country"] = new Array();
            model_internal::dependentsOnMap["ptn_birth_settlement"] = new Array();
            model_internal::dependentsOnMap["ptn_secret"] = new Array();
            model_internal::dependentsOnMap["ptn_doc_type"] = new Array();
            model_internal::dependentsOnMap["ptn_doc_num"] = new Array();
            model_internal::dependentsOnMap["emg_first_name"] = new Array();
            model_internal::dependentsOnMap["emg_last_name"] = new Array();
            model_internal::dependentsOnMap["emg_second_name"] = new Array();
            model_internal::dependentsOnMap["cnf_enable"] = new Array();
            model_internal::dependentsOnMap["cnf_rel_type"] = new Array();
            model_internal::dependentsOnMap["cnf_first_name"] = new Array();
            model_internal::dependentsOnMap["cnf_last_name"] = new Array();
            model_internal::dependentsOnMap["cnf_second_name"] = new Array();
            model_internal::dependentsOnMap["cnf_birthday"] = new Array();
            model_internal::dependentsOnMap["cnf_birth_country"] = new Array();
            model_internal::dependentsOnMap["cnf_birth_settlement"] = new Array();
            model_internal::dependentsOnMap["cnf_sex"] = new Array();
            model_internal::dependentsOnMap["cnf_taxid"] = new Array();
            model_internal::dependentsOnMap["cnf_secret"] = new Array();
            model_internal::dependentsOnMap["cnf_doc_type"] = new Array();
            model_internal::dependentsOnMap["cnf_doc_num"] = new Array();
            model_internal::dependentsOnMap["cnf_doc_rel_type"] = new Array();
            model_internal::dependentsOnMap["cnf_doc_rel_num"] = new Array();
            model_internal::dependentsOnMap["PATIENT_SIGNED"] = new Array();
            model_internal::dependentsOnMap["PATIENT_CONSENT"] = new Array();
            model_internal::dependentsOnMap["EH_DEC_ID"] = new Array();
            model_internal::dependentsOnMap["AUTH_TYPE"] = new Array();
            model_internal::dependentsOnMap["AUTH_PHONENUMBER"] = new Array();
            model_internal::dependentsOnMap["STATUS"] = new Array();
            model_internal::dependentsOnMap["account_eh_id"] = new Array();
            model_internal::dependentsOnMap["subdivision_eh_id"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["json"] = "String";
        model_internal::propertyTypeMap["id"] = "String";
        model_internal::propertyTypeMap["ptn_id"] = "String";
        model_internal::propertyTypeMap["ptn_fname"] = "String";
        model_internal::propertyTypeMap["ptn_lname"] = "String";
        model_internal::propertyTypeMap["ptn_mname"] = "String";
        model_internal::propertyTypeMap["ptn_sex"] = "int";
        model_internal::propertyTypeMap["ptn_birthday"] = "String";
        model_internal::propertyTypeMap["ptn_email"] = "String";
        model_internal::propertyTypeMap["ptn_taxid"] = "String";
        model_internal::propertyTypeMap["dec_id"] = "String";
        model_internal::propertyTypeMap["FK_SUBDIVISION"] = "String";
        model_internal::propertyTypeMap["FK_ACCOUNT"] = "String";
        model_internal::propertyTypeMap["ptn_birth_country"] = "String";
        model_internal::propertyTypeMap["ptn_birth_settlement"] = "String";
        model_internal::propertyTypeMap["ptn_secret"] = "String";
        model_internal::propertyTypeMap["ptn_doc_type"] = "String";
        model_internal::propertyTypeMap["ptn_doc_num"] = "String";
        model_internal::propertyTypeMap["emg_first_name"] = "String";
        model_internal::propertyTypeMap["emg_last_name"] = "String";
        model_internal::propertyTypeMap["emg_second_name"] = "String";
        model_internal::propertyTypeMap["cnf_enable"] = "Boolean";
        model_internal::propertyTypeMap["cnf_rel_type"] = "String";
        model_internal::propertyTypeMap["cnf_first_name"] = "String";
        model_internal::propertyTypeMap["cnf_last_name"] = "String";
        model_internal::propertyTypeMap["cnf_second_name"] = "String";
        model_internal::propertyTypeMap["cnf_birthday"] = "String";
        model_internal::propertyTypeMap["cnf_birth_country"] = "String";
        model_internal::propertyTypeMap["cnf_birth_settlement"] = "String";
        model_internal::propertyTypeMap["cnf_sex"] = "int";
        model_internal::propertyTypeMap["cnf_taxid"] = "String";
        model_internal::propertyTypeMap["cnf_secret"] = "String";
        model_internal::propertyTypeMap["cnf_doc_type"] = "String";
        model_internal::propertyTypeMap["cnf_doc_num"] = "String";
        model_internal::propertyTypeMap["cnf_doc_rel_type"] = "String";
        model_internal::propertyTypeMap["cnf_doc_rel_num"] = "String";
        model_internal::propertyTypeMap["PATIENT_SIGNED"] = "Boolean";
        model_internal::propertyTypeMap["PATIENT_CONSENT"] = "Boolean";
        model_internal::propertyTypeMap["EH_DEC_ID"] = "String";
        model_internal::propertyTypeMap["AUTH_TYPE"] = "String";
        model_internal::propertyTypeMap["AUTH_PHONENUMBER"] = "String";
        model_internal::propertyTypeMap["STATUS"] = "String";
        model_internal::propertyTypeMap["account_eh_id"] = "String";
        model_internal::propertyTypeMap["subdivision_eh_id"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity EHealthModule_Declaration");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity EHealthModule_Declaration");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of EHealthModule_Declaration");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity EHealthModule_Declaration");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity EHealthModule_Declaration");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity EHealthModule_Declaration");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isJsonAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_fnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_lnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_mnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_sexAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_birthdayAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_emailAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_taxidAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDec_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFK_SUBDIVISIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFK_ACCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_birth_countryAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_birth_settlementAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_secretAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_doc_typeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPtn_doc_numAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEmg_first_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEmg_last_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEmg_second_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_enableAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_rel_typeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_first_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_last_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_second_nameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_birthdayAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_birth_countryAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_birth_settlementAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_sexAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_taxidAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_secretAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_doc_typeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_doc_numAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_doc_rel_typeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCnf_doc_rel_numAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_SIGNEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENT_CONSENTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_DEC_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAUTH_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAUTH_PHONENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSTATUSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAccount_eh_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_eh_idAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get jsonStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_fnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_lnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_mnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_sexStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_birthdayStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_emailStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_taxidStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get dec_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FK_SUBDIVISIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FK_ACCOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_birth_countryStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_birth_settlementStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_secretStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_doc_typeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ptn_doc_numStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get emg_first_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get emg_last_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get emg_second_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_enableStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_rel_typeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_first_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_last_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_second_nameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_birthdayStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_birth_countryStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_birth_settlementStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_sexStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_taxidStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_secretStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_doc_typeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_doc_numStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_doc_rel_typeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get cnf_doc_rel_numStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_SIGNEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENT_CONSENTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_DEC_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AUTH_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AUTH_PHONENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get STATUSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get account_eh_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_eh_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
