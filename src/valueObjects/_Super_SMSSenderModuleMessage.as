/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - SMSSenderModuleMessage.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_SMSSenderModuleMessage extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("SMSSenderModuleMessage") == null)
            {
                flash.net.registerClassAlias("SMSSenderModuleMessage", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("SMSSenderModuleMessage", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _SMSSenderModuleMessageEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_errormsg : String;
    private var _internal_patientid : int;
    private var _internal_createDate : String;
    private var _internal_scheduledTime : String;
    private var _internal_serverSendTime : String;
    private var _internal_smsSendTime : String;
    private var _internal_deliveryTime : String;
    private var _internal_id : int;
    private var _internal_sms_id : String;
    private var _internal_phone : String;
    private var _internal_response : int;
    private var _internal_message : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_SMSSenderModuleMessage()
    {
        _model = new _SMSSenderModuleMessageEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get errormsg() : String
    {
        return _internal_errormsg;
    }

    [Bindable(event="propertyChange")]
    public function get patientid() : int
    {
        return _internal_patientid;
    }

    [Bindable(event="propertyChange")]
    public function get createDate() : String
    {
        return _internal_createDate;
    }

    [Bindable(event="propertyChange")]
    public function get scheduledTime() : String
    {
        return _internal_scheduledTime;
    }

    [Bindable(event="propertyChange")]
    public function get serverSendTime() : String
    {
        return _internal_serverSendTime;
    }

    [Bindable(event="propertyChange")]
    public function get smsSendTime() : String
    {
        return _internal_smsSendTime;
    }

    [Bindable(event="propertyChange")]
    public function get deliveryTime() : String
    {
        return _internal_deliveryTime;
    }

    [Bindable(event="propertyChange")]
    public function get id() : int
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get sms_id() : String
    {
        return _internal_sms_id;
    }

    [Bindable(event="propertyChange")]
    public function get phone() : String
    {
        return _internal_phone;
    }

    [Bindable(event="propertyChange")]
    public function get response() : int
    {
        return _internal_response;
    }

    [Bindable(event="propertyChange")]
    public function get message() : String
    {
        return _internal_message;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set errormsg(value:String) : void
    {
        var oldValue:String = _internal_errormsg;
        if (oldValue !== value)
        {
            _internal_errormsg = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "errormsg", oldValue, _internal_errormsg));
        }
    }

    public function set patientid(value:int) : void
    {
        var oldValue:int = _internal_patientid;
        if (oldValue !== value)
        {
            _internal_patientid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientid", oldValue, _internal_patientid));
        }
    }

    public function set createDate(value:String) : void
    {
        var oldValue:String = _internal_createDate;
        if (oldValue !== value)
        {
            _internal_createDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "createDate", oldValue, _internal_createDate));
        }
    }

    public function set scheduledTime(value:String) : void
    {
        var oldValue:String = _internal_scheduledTime;
        if (oldValue !== value)
        {
            _internal_scheduledTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "scheduledTime", oldValue, _internal_scheduledTime));
        }
    }

    public function set serverSendTime(value:String) : void
    {
        var oldValue:String = _internal_serverSendTime;
        if (oldValue !== value)
        {
            _internal_serverSendTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "serverSendTime", oldValue, _internal_serverSendTime));
        }
    }

    public function set smsSendTime(value:String) : void
    {
        var oldValue:String = _internal_smsSendTime;
        if (oldValue !== value)
        {
            _internal_smsSendTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "smsSendTime", oldValue, _internal_smsSendTime));
        }
    }

    public function set deliveryTime(value:String) : void
    {
        var oldValue:String = _internal_deliveryTime;
        if (oldValue !== value)
        {
            _internal_deliveryTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "deliveryTime", oldValue, _internal_deliveryTime));
        }
    }

    public function set id(value:int) : void
    {
        var oldValue:int = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set sms_id(value:String) : void
    {
        var oldValue:String = _internal_sms_id;
        if (oldValue !== value)
        {
            _internal_sms_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sms_id", oldValue, _internal_sms_id));
        }
    }

    public function set phone(value:String) : void
    {
        var oldValue:String = _internal_phone;
        if (oldValue !== value)
        {
            _internal_phone = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "phone", oldValue, _internal_phone));
        }
    }

    public function set response(value:int) : void
    {
        var oldValue:int = _internal_response;
        if (oldValue !== value)
        {
            _internal_response = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "response", oldValue, _internal_response));
        }
    }

    public function set message(value:String) : void
    {
        var oldValue:String = _internal_message;
        if (oldValue !== value)
        {
            _internal_message = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "message", oldValue, _internal_message));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _SMSSenderModuleMessageEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _SMSSenderModuleMessageEntityMetadata) : void
    {
        var oldValue : _SMSSenderModuleMessageEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
