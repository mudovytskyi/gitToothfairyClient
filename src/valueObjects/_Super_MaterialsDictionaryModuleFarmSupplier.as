/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - MaterialsDictionaryModuleFarmSupplier.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_MaterialsDictionaryModuleFarmSupplier extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("MaterialsDictionaryModuleFarmSupplier") == null)
            {
                flash.net.registerClassAlias("MaterialsDictionaryModuleFarmSupplier", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("MaterialsDictionaryModuleFarmSupplier", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _MaterialsDictionaryModuleFarmSupplierEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_FARMID : String;
    private var _internal_SUPPLIERID : String;
    private var _internal_SUPPLIERNAME : String;
    private var _internal_SUPPLIERADDRESS : String;
    private var _internal_SUPPLIERTELEPHONENUMBER : String;
    private var _internal_PRICE : String;
    private var _internal_MODIFIED : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_MaterialsDictionaryModuleFarmSupplier()
    {
        _model = new _MaterialsDictionaryModuleFarmSupplierEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FARMID() : String
    {
        return _internal_FARMID;
    }

    [Bindable(event="propertyChange")]
    public function get SUPPLIERID() : String
    {
        return _internal_SUPPLIERID;
    }

    [Bindable(event="propertyChange")]
    public function get SUPPLIERNAME() : String
    {
        return _internal_SUPPLIERNAME;
    }

    [Bindable(event="propertyChange")]
    public function get SUPPLIERADDRESS() : String
    {
        return _internal_SUPPLIERADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get SUPPLIERTELEPHONENUMBER() : String
    {
        return _internal_SUPPLIERTELEPHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PRICE() : String
    {
        return _internal_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get MODIFIED() : String
    {
        return _internal_MODIFIED;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set FARMID(value:String) : void
    {
        var oldValue:String = _internal_FARMID;
        if (oldValue !== value)
        {
            _internal_FARMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMID", oldValue, _internal_FARMID));
        }
    }

    public function set SUPPLIERID(value:String) : void
    {
        var oldValue:String = _internal_SUPPLIERID;
        if (oldValue !== value)
        {
            _internal_SUPPLIERID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUPPLIERID", oldValue, _internal_SUPPLIERID));
        }
    }

    public function set SUPPLIERNAME(value:String) : void
    {
        var oldValue:String = _internal_SUPPLIERNAME;
        if (oldValue !== value)
        {
            _internal_SUPPLIERNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUPPLIERNAME", oldValue, _internal_SUPPLIERNAME));
        }
    }

    public function set SUPPLIERADDRESS(value:String) : void
    {
        var oldValue:String = _internal_SUPPLIERADDRESS;
        if (oldValue !== value)
        {
            _internal_SUPPLIERADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUPPLIERADDRESS", oldValue, _internal_SUPPLIERADDRESS));
        }
    }

    public function set SUPPLIERTELEPHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_SUPPLIERTELEPHONENUMBER;
        if (oldValue !== value)
        {
            _internal_SUPPLIERTELEPHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUPPLIERTELEPHONENUMBER", oldValue, _internal_SUPPLIERTELEPHONENUMBER));
        }
    }

    public function set PRICE(value:String) : void
    {
        var oldValue:String = _internal_PRICE;
        if (oldValue !== value)
        {
            _internal_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRICE", oldValue, _internal_PRICE));
        }
    }

    public function set MODIFIED(value:String) : void
    {
        var oldValue:String = _internal_MODIFIED;
        if (oldValue !== value)
        {
            _internal_MODIFIED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MODIFIED", oldValue, _internal_MODIFIED));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _MaterialsDictionaryModuleFarmSupplierEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _MaterialsDictionaryModuleFarmSupplierEntityMetadata) : void
    {
        var oldValue : _MaterialsDictionaryModuleFarmSupplierEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
