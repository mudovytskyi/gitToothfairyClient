/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - TreatmentManagerModuleTemplateProcedure.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.Form039Module_F39_Val;
import valueObjects.TreatmentManagerModuleTemplateProcedurFarm;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_TreatmentManagerModuleTemplateProcedure extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("TreatmentManagerModuleTemplateProcedure") == null)
            {
                flash.net.registerClassAlias("TreatmentManagerModuleTemplateProcedure", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("TreatmentManagerModuleTemplateProcedure", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.TreatmentManagerModuleTemplateProcedurFarm.initRemoteClassAliasSingleChild();
        valueObjects.Form039Module_F39_Val.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _TreatmentManagerModuleTemplateProcedureEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_procedurID : String;
    private var _internal_procedurName : String;
    private var _internal_procedurPlanName : String;
    private var _internal_procedurShifr : String;
    private var _internal_procedurVisit : int;
    private var _internal_procedurStacknumber : int;
    private var _internal_healthprocID : String;
    private var _internal_procedurPrice : Number;
    private var _internal_healthType : int;
    private var _internal_farms : ArrayCollection;
    model_internal var _internal_farms_leaf:valueObjects.TreatmentManagerModuleTemplateProcedurFarm;
    private var _internal_healthTypes : String;
    private var _internal_F39 : ArrayCollection;
    model_internal var _internal_F39_leaf:valueObjects.Form039Module_F39_Val;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_TreatmentManagerModuleTemplateProcedure()
    {
        _model = new _TreatmentManagerModuleTemplateProcedureEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get procedurID() : String
    {
        return _internal_procedurID;
    }

    [Bindable(event="propertyChange")]
    public function get procedurName() : String
    {
        return _internal_procedurName;
    }

    [Bindable(event="propertyChange")]
    public function get procedurPlanName() : String
    {
        return _internal_procedurPlanName;
    }

    [Bindable(event="propertyChange")]
    public function get procedurShifr() : String
    {
        return _internal_procedurShifr;
    }

    [Bindable(event="propertyChange")]
    public function get procedurVisit() : int
    {
        return _internal_procedurVisit;
    }

    [Bindable(event="propertyChange")]
    public function get procedurStacknumber() : int
    {
        return _internal_procedurStacknumber;
    }

    [Bindable(event="propertyChange")]
    public function get healthprocID() : String
    {
        return _internal_healthprocID;
    }

    [Bindable(event="propertyChange")]
    public function get procedurPrice() : Number
    {
        return _internal_procedurPrice;
    }

    [Bindable(event="propertyChange")]
    public function get healthType() : int
    {
        return _internal_healthType;
    }

    [Bindable(event="propertyChange")]
    public function get farms() : ArrayCollection
    {
        return _internal_farms;
    }

    [Bindable(event="propertyChange")]
    public function get healthTypes() : String
    {
        return _internal_healthTypes;
    }

    [Bindable(event="propertyChange")]
    public function get F39() : ArrayCollection
    {
        return _internal_F39;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set procedurID(value:String) : void
    {
        var oldValue:String = _internal_procedurID;
        if (oldValue !== value)
        {
            _internal_procedurID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurID", oldValue, _internal_procedurID));
        }
    }

    public function set procedurName(value:String) : void
    {
        var oldValue:String = _internal_procedurName;
        if (oldValue !== value)
        {
            _internal_procedurName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurName", oldValue, _internal_procedurName));
        }
    }

    public function set procedurPlanName(value:String) : void
    {
        var oldValue:String = _internal_procedurPlanName;
        if (oldValue !== value)
        {
            _internal_procedurPlanName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurPlanName", oldValue, _internal_procedurPlanName));
        }
    }

    public function set procedurShifr(value:String) : void
    {
        var oldValue:String = _internal_procedurShifr;
        if (oldValue !== value)
        {
            _internal_procedurShifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurShifr", oldValue, _internal_procedurShifr));
        }
    }

    public function set procedurVisit(value:int) : void
    {
        var oldValue:int = _internal_procedurVisit;
        if (oldValue !== value)
        {
            _internal_procedurVisit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurVisit", oldValue, _internal_procedurVisit));
        }
    }

    public function set procedurStacknumber(value:int) : void
    {
        var oldValue:int = _internal_procedurStacknumber;
        if (oldValue !== value)
        {
            _internal_procedurStacknumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurStacknumber", oldValue, _internal_procedurStacknumber));
        }
    }

    public function set healthprocID(value:String) : void
    {
        var oldValue:String = _internal_healthprocID;
        if (oldValue !== value)
        {
            _internal_healthprocID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "healthprocID", oldValue, _internal_healthprocID));
        }
    }

    public function set procedurPrice(value:Number) : void
    {
        var oldValue:Number = _internal_procedurPrice;
        if (isNaN(_internal_procedurPrice) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_procedurPrice = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedurPrice", oldValue, _internal_procedurPrice));
        }
    }

    public function set healthType(value:int) : void
    {
        var oldValue:int = _internal_healthType;
        if (oldValue !== value)
        {
            _internal_healthType = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "healthType", oldValue, _internal_healthType));
        }
    }

    public function set farms(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_farms;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_farms = value;
            }
            else if (value is Array)
            {
                _internal_farms = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_farms = null;
            }
            else
            {
                throw new Error("value of farms must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farms", oldValue, _internal_farms));
        }
    }

    public function set healthTypes(value:String) : void
    {
        var oldValue:String = _internal_healthTypes;
        if (oldValue !== value)
        {
            _internal_healthTypes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "healthTypes", oldValue, _internal_healthTypes));
        }
    }

    public function set F39(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_F39;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_F39 = value;
            }
            else if (value is Array)
            {
                _internal_F39 = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_F39 = null;
            }
            else
            {
                throw new Error("value of F39 must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "F39", oldValue, _internal_F39));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _TreatmentManagerModuleTemplateProcedureEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _TreatmentManagerModuleTemplateProcedureEntityMetadata) : void
    {
        var oldValue : _TreatmentManagerModuleTemplateProcedureEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
