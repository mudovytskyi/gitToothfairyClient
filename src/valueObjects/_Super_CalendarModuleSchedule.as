/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CalendarModuleSchedule.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.CalendarModuleDaySchedule;
import valueObjects.CalendarModuleDoctor;
import valueObjects.CalendarModuleScheduleRule;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CalendarModuleSchedule extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CalendarModuleSchedule") == null)
            {
                flash.net.registerClassAlias("CalendarModuleSchedule", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CalendarModuleSchedule", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.CalendarModuleScheduleRule.initRemoteClassAliasSingleChild();
        valueObjects.CalendarModuleDoctor.initRemoteClassAliasSingleChild();
        valueObjects.CalendarModuleDaySchedule.initRemoteClassAliasSingleChild();
        valueObjects.CalendarModuleInterval.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _CalendarModuleScheduleEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : int;
    private var _internal_name : Object;
    private var _internal_Rules : ArrayCollection;
    model_internal var _internal_Rules_leaf:valueObjects.CalendarModuleScheduleRule;
    private var _internal_startDate : Object;
    private var _internal_endDate : Object;
    private var _internal_Doctor : valueObjects.CalendarModuleDoctor;
    private var _internal_isCurrent : Boolean;
    private var _internal_daySchedule : ArrayCollection;
    model_internal var _internal_daySchedule_leaf:valueObjects.CalendarModuleDaySchedule;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CalendarModuleSchedule()
    {
        _model = new _CalendarModuleScheduleEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : int
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get name() : Object
    {
        return _internal_name;
    }

    [Bindable(event="propertyChange")]
    public function get Rules() : ArrayCollection
    {
        return _internal_Rules;
    }

    [Bindable(event="propertyChange")]
    public function get startDate() : Object
    {
        return _internal_startDate;
    }

    [Bindable(event="propertyChange")]
    public function get endDate() : Object
    {
        return _internal_endDate;
    }

    [Bindable(event="propertyChange")]
    public function get Doctor() : valueObjects.CalendarModuleDoctor
    {
        return _internal_Doctor;
    }

    [Bindable(event="propertyChange")]
    public function get isCurrent() : Boolean
    {
        return _internal_isCurrent;
    }

    [Bindable(event="propertyChange")]
    public function get daySchedule() : ArrayCollection
    {
        return _internal_daySchedule;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:int) : void
    {
        var oldValue:int = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set name(value:Object) : void
    {
        var oldValue:Object = _internal_name;
        if (oldValue !== value)
        {
            _internal_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name", oldValue, _internal_name));
        }
    }

    public function set Rules(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_Rules;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_Rules = value;
            }
            else if (value is Array)
            {
                _internal_Rules = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_Rules = null;
            }
            else
            {
                throw new Error("value of Rules must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Rules", oldValue, _internal_Rules));
        }
    }

    public function set startDate(value:Object) : void
    {
        var oldValue:Object = _internal_startDate;
        if (oldValue !== value)
        {
            _internal_startDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "startDate", oldValue, _internal_startDate));
        }
    }

    public function set endDate(value:Object) : void
    {
        var oldValue:Object = _internal_endDate;
        if (oldValue !== value)
        {
            _internal_endDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "endDate", oldValue, _internal_endDate));
        }
    }

    public function set Doctor(value:valueObjects.CalendarModuleDoctor) : void
    {
        var oldValue:valueObjects.CalendarModuleDoctor = _internal_Doctor;
        if (oldValue !== value)
        {
            _internal_Doctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Doctor", oldValue, _internal_Doctor));
        }
    }

    public function set isCurrent(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isCurrent;
        if (oldValue !== value)
        {
            _internal_isCurrent = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isCurrent", oldValue, _internal_isCurrent));
        }
    }

    public function set daySchedule(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_daySchedule;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_daySchedule = value;
            }
            else if (value is Array)
            {
                _internal_daySchedule = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_daySchedule = null;
            }
            else
            {
                throw new Error("value of daySchedule must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "daySchedule", oldValue, _internal_daySchedule));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CalendarModuleScheduleEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CalendarModuleScheduleEntityMetadata) : void
    {
        var oldValue : _CalendarModuleScheduleEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
