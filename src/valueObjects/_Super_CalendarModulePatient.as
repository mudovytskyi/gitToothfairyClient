/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CalendarModulePatient.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CalendarModulePatient extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CalendarModulePatient") == null)
            {
                flash.net.registerClassAlias("CalendarModulePatient", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CalendarModulePatient", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _CalendarModulePatientEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_SHORTNAME : String;
    private var _internal_CARDNUMBER : String;
    private var _internal_doctors : String;
    private var _internal_agreementTimestamp : String;
    private var _internal_dob : String;
    private var _internal_doctor_lead : String;
    private var _internal_GROUP_COLOR : int;
    private var _internal_GROUP_DESCR : String;
    private var _internal_NH_TREATTYPE : int;
    private var _internal_agreementtype : int;
    private var _internal_patient_ages : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CalendarModulePatient()
    {
        _model = new _CalendarModulePatientEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get SHORTNAME() : String
    {
        return _internal_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get CARDNUMBER() : String
    {
        return _internal_CARDNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get doctors() : String
    {
        return _internal_doctors;
    }

    [Bindable(event="propertyChange")]
    public function get agreementTimestamp() : String
    {
        return _internal_agreementTimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get dob() : String
    {
        return _internal_dob;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lead() : String
    {
        return _internal_doctor_lead;
    }

    [Bindable(event="propertyChange")]
    public function get GROUP_COLOR() : int
    {
        return _internal_GROUP_COLOR;
    }

    [Bindable(event="propertyChange")]
    public function get GROUP_DESCR() : String
    {
        return _internal_GROUP_DESCR;
    }

    [Bindable(event="propertyChange")]
    public function get NH_TREATTYPE() : int
    {
        return _internal_NH_TREATTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get agreementtype() : int
    {
        return _internal_agreementtype;
    }

    [Bindable(event="propertyChange")]
    public function get patient_ages() : int
    {
        return _internal_patient_ages;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SHORTNAME", oldValue, _internal_SHORTNAME));
        }
    }

    public function set CARDNUMBER(value:String) : void
    {
        var oldValue:String = _internal_CARDNUMBER;
        if (oldValue !== value)
        {
            _internal_CARDNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDNUMBER", oldValue, _internal_CARDNUMBER));
        }
    }

    public function set doctors(value:String) : void
    {
        var oldValue:String = _internal_doctors;
        if (oldValue !== value)
        {
            _internal_doctors = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctors", oldValue, _internal_doctors));
        }
    }

    public function set agreementTimestamp(value:String) : void
    {
        var oldValue:String = _internal_agreementTimestamp;
        if (oldValue !== value)
        {
            _internal_agreementTimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "agreementTimestamp", oldValue, _internal_agreementTimestamp));
        }
    }

    public function set dob(value:String) : void
    {
        var oldValue:String = _internal_dob;
        if (oldValue !== value)
        {
            _internal_dob = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dob", oldValue, _internal_dob));
        }
    }

    public function set doctor_lead(value:String) : void
    {
        var oldValue:String = _internal_doctor_lead;
        if (oldValue !== value)
        {
            _internal_doctor_lead = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lead", oldValue, _internal_doctor_lead));
        }
    }

    public function set GROUP_COLOR(value:int) : void
    {
        var oldValue:int = _internal_GROUP_COLOR;
        if (oldValue !== value)
        {
            _internal_GROUP_COLOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "GROUP_COLOR", oldValue, _internal_GROUP_COLOR));
        }
    }

    public function set GROUP_DESCR(value:String) : void
    {
        var oldValue:String = _internal_GROUP_DESCR;
        if (oldValue !== value)
        {
            _internal_GROUP_DESCR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "GROUP_DESCR", oldValue, _internal_GROUP_DESCR));
        }
    }

    public function set NH_TREATTYPE(value:int) : void
    {
        var oldValue:int = _internal_NH_TREATTYPE;
        if (oldValue !== value)
        {
            _internal_NH_TREATTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NH_TREATTYPE", oldValue, _internal_NH_TREATTYPE));
        }
    }

    public function set agreementtype(value:int) : void
    {
        var oldValue:int = _internal_agreementtype;
        if (oldValue !== value)
        {
            _internal_agreementtype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "agreementtype", oldValue, _internal_agreementtype));
        }
    }

    public function set patient_ages(value:int) : void
    {
        var oldValue:int = _internal_patient_ages;
        if (oldValue !== value)
        {
            _internal_patient_ages = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_ages", oldValue, _internal_patient_ages));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CalendarModulePatientEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CalendarModulePatientEntityMetadata) : void
    {
        var oldValue : _CalendarModulePatientEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
