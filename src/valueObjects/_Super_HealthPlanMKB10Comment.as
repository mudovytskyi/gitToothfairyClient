/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HealthPlanMKB10Comment.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.HealthPlanMKB10Doctor;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HealthPlanMKB10Comment extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HealthPlanMKB10Comment") == null)
            {
                flash.net.registerClassAlias("HealthPlanMKB10Comment", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HealthPlanMKB10Comment", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HealthPlanMKB10Doctor.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _HealthPlanMKB10CommentEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : int;
    private var _internal_OWNERID : int;
    private var _internal_TYPE : int;
    private var _internal_COMMENTTXT : String;
    private var _internal_DATECOMMENT : String;
    private var _internal_SIGNFLAG : Boolean;
    private var _internal_Doctor : valueObjects.HealthPlanMKB10Doctor;
    private var _internal_deltype : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HealthPlanMKB10Comment()
    {
        _model = new _HealthPlanMKB10CommentEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : int
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get OWNERID() : int
    {
        return _internal_OWNERID;
    }

    [Bindable(event="propertyChange")]
    public function get TYPE() : int
    {
        return _internal_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get COMMENTTXT() : String
    {
        return _internal_COMMENTTXT;
    }

    [Bindable(event="propertyChange")]
    public function get DATECOMMENT() : String
    {
        return _internal_DATECOMMENT;
    }

    [Bindable(event="propertyChange")]
    public function get SIGNFLAG() : Boolean
    {
        return _internal_SIGNFLAG;
    }

    [Bindable(event="propertyChange")]
    public function get Doctor() : valueObjects.HealthPlanMKB10Doctor
    {
        return _internal_Doctor;
    }

    [Bindable(event="propertyChange")]
    public function get deltype() : Boolean
    {
        return _internal_deltype;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:int) : void
    {
        var oldValue:int = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set OWNERID(value:int) : void
    {
        var oldValue:int = _internal_OWNERID;
        if (oldValue !== value)
        {
            _internal_OWNERID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OWNERID", oldValue, _internal_OWNERID));
        }
    }

    public function set TYPE(value:int) : void
    {
        var oldValue:int = _internal_TYPE;
        if (oldValue !== value)
        {
            _internal_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TYPE", oldValue, _internal_TYPE));
        }
    }

    public function set COMMENTTXT(value:String) : void
    {
        var oldValue:String = _internal_COMMENTTXT;
        if (oldValue !== value)
        {
            _internal_COMMENTTXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COMMENTTXT", oldValue, _internal_COMMENTTXT));
        }
    }

    public function set DATECOMMENT(value:String) : void
    {
        var oldValue:String = _internal_DATECOMMENT;
        if (oldValue !== value)
        {
            _internal_DATECOMMENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATECOMMENT", oldValue, _internal_DATECOMMENT));
        }
    }

    public function set SIGNFLAG(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SIGNFLAG;
        if (oldValue !== value)
        {
            _internal_SIGNFLAG = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SIGNFLAG", oldValue, _internal_SIGNFLAG));
        }
    }

    public function set Doctor(value:valueObjects.HealthPlanMKB10Doctor) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10Doctor = _internal_Doctor;
        if (oldValue !== value)
        {
            _internal_Doctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Doctor", oldValue, _internal_Doctor));
        }
    }

    public function set deltype(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_deltype;
        if (oldValue !== value)
        {
            _internal_deltype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "deltype", oldValue, _internal_deltype));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HealthPlanMKB10CommentEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HealthPlanMKB10CommentEntityMetadata) : void
    {
        var oldValue : _HealthPlanMKB10CommentEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
