/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PatientCardModuleDoctorObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PatientCardModuleDoctorObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PatientCardModuleDoctorObject") == null)
            {
                flash.net.registerClassAlias("PatientCardModuleDoctorObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PatientCardModuleDoctorObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _PatientCardModuleDoctorObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_fname : String;
    private var _internal_lname : String;
    private var _internal_pname : String;
    private var _internal_shortname : String;
    private var _internal_docid : int;
    private var _internal_isWork : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PatientCardModuleDoctorObject()
    {
        _model = new _PatientCardModuleDoctorObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get fname() : String
    {
        return _internal_fname;
    }

    [Bindable(event="propertyChange")]
    public function get lname() : String
    {
        return _internal_lname;
    }

    [Bindable(event="propertyChange")]
    public function get pname() : String
    {
        return _internal_pname;
    }

    [Bindable(event="propertyChange")]
    public function get shortname() : String
    {
        return _internal_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get docid() : int
    {
        return _internal_docid;
    }

    [Bindable(event="propertyChange")]
    public function get isWork() : Boolean
    {
        return _internal_isWork;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set fname(value:String) : void
    {
        var oldValue:String = _internal_fname;
        if (oldValue !== value)
        {
            _internal_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fname", oldValue, _internal_fname));
        }
    }

    public function set lname(value:String) : void
    {
        var oldValue:String = _internal_lname;
        if (oldValue !== value)
        {
            _internal_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "lname", oldValue, _internal_lname));
        }
    }

    public function set pname(value:String) : void
    {
        var oldValue:String = _internal_pname;
        if (oldValue !== value)
        {
            _internal_pname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "pname", oldValue, _internal_pname));
        }
    }

    public function set shortname(value:String) : void
    {
        var oldValue:String = _internal_shortname;
        if (oldValue !== value)
        {
            _internal_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "shortname", oldValue, _internal_shortname));
        }
    }

    public function set docid(value:int) : void
    {
        var oldValue:int = _internal_docid;
        if (oldValue !== value)
        {
            _internal_docid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "docid", oldValue, _internal_docid));
        }
    }

    public function set isWork(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isWork;
        if (oldValue !== value)
        {
            _internal_isWork = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isWork", oldValue, _internal_isWork));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PatientCardModuleDoctorObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PatientCardModuleDoctorObjectEntityMetadata) : void
    {
        var oldValue : _PatientCardModuleDoctorObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
