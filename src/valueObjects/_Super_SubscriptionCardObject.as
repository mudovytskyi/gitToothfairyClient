/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - SubscriptionCardObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.SubscriptionDictionaryObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_SubscriptionCardObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("SubscriptionCardObject") == null)
            {
                flash.net.registerClassAlias("SubscriptionCardObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("SubscriptionCardObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.SubscriptionDictionaryObject.initRemoteClassAliasSingleChild();
        valueObjects.SubscriptionTypeObject.initRemoteClassAliasSingleChild();
        valueObjects.SubscriptionProcedureObject.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _SubscriptionCardObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_PATIENT_ID : String;
    private var _internal_DOCTOR_ID : String;
    private var _internal_subscription : valueObjects.SubscriptionDictionaryObject;
    private var _internal_CREATED_DATE : Object;
    private var _internal_CURRENT_MONTH : int;
    private var _internal_DATE_FROM : Object;
    private var _internal_DATE_TO : Object;
    private var _internal_IS_ACTIVATED : Boolean;
    private var _internal_IS_EXPIRED : Boolean;
    private var _internal_STATUS : Boolean;
    private var _internal_VISITS_LEFT : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_SubscriptionCardObject()
    {
        _model = new _SubscriptionCardObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_ID() : String
    {
        return _internal_PATIENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_ID() : String
    {
        return _internal_DOCTOR_ID;
    }

    [Bindable(event="propertyChange")]
    public function get subscription() : valueObjects.SubscriptionDictionaryObject
    {
        return _internal_subscription;
    }

    [Bindable(event="propertyChange")]
    public function get CREATED_DATE() : Object
    {
        return _internal_CREATED_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get CURRENT_MONTH() : int
    {
        return _internal_CURRENT_MONTH;
    }

    [Bindable(event="propertyChange")]
    public function get DATE_FROM() : Object
    {
        return _internal_DATE_FROM;
    }

    [Bindable(event="propertyChange")]
    public function get DATE_TO() : Object
    {
        return _internal_DATE_TO;
    }

    [Bindable(event="propertyChange")]
    public function get IS_ACTIVATED() : Boolean
    {
        return _internal_IS_ACTIVATED;
    }

    [Bindable(event="propertyChange")]
    public function get IS_EXPIRED() : Boolean
    {
        return _internal_IS_EXPIRED;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS() : Boolean
    {
        return _internal_STATUS;
    }

    [Bindable(event="propertyChange")]
    public function get VISITS_LEFT() : int
    {
        return _internal_VISITS_LEFT;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set PATIENT_ID(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_ID;
        if (oldValue !== value)
        {
            _internal_PATIENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_ID", oldValue, _internal_PATIENT_ID));
        }
    }

    public function set DOCTOR_ID(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_ID;
        if (oldValue !== value)
        {
            _internal_DOCTOR_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_ID", oldValue, _internal_DOCTOR_ID));
        }
    }

    public function set subscription(value:valueObjects.SubscriptionDictionaryObject) : void
    {
        var oldValue:valueObjects.SubscriptionDictionaryObject = _internal_subscription;
        if (oldValue !== value)
        {
            _internal_subscription = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subscription", oldValue, _internal_subscription));
        }
    }

    public function set CREATED_DATE(value:Object) : void
    {
        var oldValue:Object = _internal_CREATED_DATE;
        if (oldValue !== value)
        {
            _internal_CREATED_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CREATED_DATE", oldValue, _internal_CREATED_DATE));
        }
    }

    public function set CURRENT_MONTH(value:int) : void
    {
        var oldValue:int = _internal_CURRENT_MONTH;
        if (oldValue !== value)
        {
            _internal_CURRENT_MONTH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CURRENT_MONTH", oldValue, _internal_CURRENT_MONTH));
        }
    }

    public function set DATE_FROM(value:Object) : void
    {
        var oldValue:Object = _internal_DATE_FROM;
        if (oldValue !== value)
        {
            _internal_DATE_FROM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATE_FROM", oldValue, _internal_DATE_FROM));
        }
    }

    public function set DATE_TO(value:Object) : void
    {
        var oldValue:Object = _internal_DATE_TO;
        if (oldValue !== value)
        {
            _internal_DATE_TO = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATE_TO", oldValue, _internal_DATE_TO));
        }
    }

    public function set IS_ACTIVATED(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_IS_ACTIVATED;
        if (oldValue !== value)
        {
            _internal_IS_ACTIVATED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "IS_ACTIVATED", oldValue, _internal_IS_ACTIVATED));
        }
    }

    public function set IS_EXPIRED(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_IS_EXPIRED;
        if (oldValue !== value)
        {
            _internal_IS_EXPIRED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "IS_EXPIRED", oldValue, _internal_IS_EXPIRED));
        }
    }

    public function set STATUS(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_STATUS;
        if (oldValue !== value)
        {
            _internal_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS", oldValue, _internal_STATUS));
        }
    }

    public function set VISITS_LEFT(value:int) : void
    {
        var oldValue:int = _internal_VISITS_LEFT;
        if (oldValue !== value)
        {
            _internal_VISITS_LEFT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VISITS_LEFT", oldValue, _internal_VISITS_LEFT));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _SubscriptionCardObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _SubscriptionCardObjectEntityMetadata) : void
    {
        var oldValue : _SubscriptionCardObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
