/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HealthPlanMKB10TreatmentCourse.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.HealthPlanMKB10Diagnos2;
import valueObjects.HealthPlanMKB10ToothExam;
import valueObjects.HealthPlanMKB10TreatmentAdditions;
import valueObjects.HealthPlanMKB10TreatmentObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HealthPlanMKB10TreatmentCourse extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HealthPlanMKB10TreatmentCourse") == null)
            {
                flash.net.registerClassAlias("HealthPlanMKB10TreatmentCourse", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HealthPlanMKB10TreatmentCourse", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HealthPlanMKB10TreatmentAdditions.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Doctor.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10ToothExam.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Diagnos2.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10MKBProtocol.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10MKB.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10F43.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Template.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Comment.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10TreatmentObject.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _HealthPlanMKB10TreatmentCourseEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : int;
    private var _internal_addiotions_Anamnes : ArrayCollection;
    model_internal var _internal_addiotions_Anamnes_leaf:valueObjects.HealthPlanMKB10TreatmentAdditions;
    private var _internal_addiotions_Xray : ArrayCollection;
    model_internal var _internal_addiotions_Xray_leaf:valueObjects.HealthPlanMKB10TreatmentAdditions;
    private var _internal_addiotions_Status : ArrayCollection;
    model_internal var _internal_addiotions_Status_leaf:valueObjects.HealthPlanMKB10TreatmentAdditions;
    private var _internal_addiotions_Recomend : ArrayCollection;
    model_internal var _internal_addiotions_Recomend_leaf:valueObjects.HealthPlanMKB10TreatmentAdditions;
    private var _internal_addiotions_Epicrisis : ArrayCollection;
    model_internal var _internal_addiotions_Epicrisis_leaf:valueObjects.HealthPlanMKB10TreatmentAdditions;
    private var _internal_VISIT : int;
    private var _internal_Befor : valueObjects.HealthPlanMKB10ToothExam;
    private var _internal_After : valueObjects.HealthPlanMKB10ToothExam;
    private var _internal_Diagnos : ArrayCollection;
    model_internal var _internal_Diagnos_leaf:valueObjects.HealthPlanMKB10Diagnos2;
    private var _internal_PatientName : String;
    private var _internal_TreatmentObjects : ArrayCollection;
    model_internal var _internal_TreatmentObjects_leaf:valueObjects.HealthPlanMKB10TreatmentObject;
    private var _internal_ProtocolAnamnes : String;
    private var _internal_ProtocolStatus : String;
    private var _internal_ProtocolRecomend : String;
    private var _internal_ProtocolRentgen : String;
    private var _internal_ProtocolEpicrisis : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HealthPlanMKB10TreatmentCourse()
    {
        _model = new _HealthPlanMKB10TreatmentCourseEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : int
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get addiotions_Anamnes() : ArrayCollection
    {
        return _internal_addiotions_Anamnes;
    }

    [Bindable(event="propertyChange")]
    public function get addiotions_Xray() : ArrayCollection
    {
        return _internal_addiotions_Xray;
    }

    [Bindable(event="propertyChange")]
    public function get addiotions_Status() : ArrayCollection
    {
        return _internal_addiotions_Status;
    }

    [Bindable(event="propertyChange")]
    public function get addiotions_Recomend() : ArrayCollection
    {
        return _internal_addiotions_Recomend;
    }

    [Bindable(event="propertyChange")]
    public function get addiotions_Epicrisis() : ArrayCollection
    {
        return _internal_addiotions_Epicrisis;
    }

    [Bindable(event="propertyChange")]
    public function get VISIT() : int
    {
        return _internal_VISIT;
    }

    [Bindable(event="propertyChange")]
    public function get Befor() : valueObjects.HealthPlanMKB10ToothExam
    {
        return _internal_Befor;
    }

    [Bindable(event="propertyChange")]
    public function get After() : valueObjects.HealthPlanMKB10ToothExam
    {
        return _internal_After;
    }

    [Bindable(event="propertyChange")]
    public function get Diagnos() : ArrayCollection
    {
        return _internal_Diagnos;
    }

    [Bindable(event="propertyChange")]
    public function get PatientName() : String
    {
        return _internal_PatientName;
    }

    [Bindable(event="propertyChange")]
    public function get TreatmentObjects() : ArrayCollection
    {
        return _internal_TreatmentObjects;
    }

    [Bindable(event="propertyChange")]
    public function get ProtocolAnamnes() : String
    {
        return _internal_ProtocolAnamnes;
    }

    [Bindable(event="propertyChange")]
    public function get ProtocolStatus() : String
    {
        return _internal_ProtocolStatus;
    }

    [Bindable(event="propertyChange")]
    public function get ProtocolRecomend() : String
    {
        return _internal_ProtocolRecomend;
    }

    [Bindable(event="propertyChange")]
    public function get ProtocolRentgen() : String
    {
        return _internal_ProtocolRentgen;
    }

    [Bindable(event="propertyChange")]
    public function get ProtocolEpicrisis() : String
    {
        return _internal_ProtocolEpicrisis;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:int) : void
    {
        var oldValue:int = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set addiotions_Anamnes(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_addiotions_Anamnes;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_addiotions_Anamnes = value;
            }
            else if (value is Array)
            {
                _internal_addiotions_Anamnes = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_addiotions_Anamnes = null;
            }
            else
            {
                throw new Error("value of addiotions_Anamnes must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "addiotions_Anamnes", oldValue, _internal_addiotions_Anamnes));
        }
    }

    public function set addiotions_Xray(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_addiotions_Xray;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_addiotions_Xray = value;
            }
            else if (value is Array)
            {
                _internal_addiotions_Xray = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_addiotions_Xray = null;
            }
            else
            {
                throw new Error("value of addiotions_Xray must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "addiotions_Xray", oldValue, _internal_addiotions_Xray));
        }
    }

    public function set addiotions_Status(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_addiotions_Status;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_addiotions_Status = value;
            }
            else if (value is Array)
            {
                _internal_addiotions_Status = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_addiotions_Status = null;
            }
            else
            {
                throw new Error("value of addiotions_Status must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "addiotions_Status", oldValue, _internal_addiotions_Status));
        }
    }

    public function set addiotions_Recomend(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_addiotions_Recomend;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_addiotions_Recomend = value;
            }
            else if (value is Array)
            {
                _internal_addiotions_Recomend = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_addiotions_Recomend = null;
            }
            else
            {
                throw new Error("value of addiotions_Recomend must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "addiotions_Recomend", oldValue, _internal_addiotions_Recomend));
        }
    }

    public function set addiotions_Epicrisis(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_addiotions_Epicrisis;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_addiotions_Epicrisis = value;
            }
            else if (value is Array)
            {
                _internal_addiotions_Epicrisis = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_addiotions_Epicrisis = null;
            }
            else
            {
                throw new Error("value of addiotions_Epicrisis must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "addiotions_Epicrisis", oldValue, _internal_addiotions_Epicrisis));
        }
    }

    public function set VISIT(value:int) : void
    {
        var oldValue:int = _internal_VISIT;
        if (oldValue !== value)
        {
            _internal_VISIT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VISIT", oldValue, _internal_VISIT));
        }
    }

    public function set Befor(value:valueObjects.HealthPlanMKB10ToothExam) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10ToothExam = _internal_Befor;
        if (oldValue !== value)
        {
            _internal_Befor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Befor", oldValue, _internal_Befor));
        }
    }

    public function set After(value:valueObjects.HealthPlanMKB10ToothExam) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10ToothExam = _internal_After;
        if (oldValue !== value)
        {
            _internal_After = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "After", oldValue, _internal_After));
        }
    }

    public function set Diagnos(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_Diagnos;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_Diagnos = value;
            }
            else if (value is Array)
            {
                _internal_Diagnos = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_Diagnos = null;
            }
            else
            {
                throw new Error("value of Diagnos must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Diagnos", oldValue, _internal_Diagnos));
        }
    }

    public function set PatientName(value:String) : void
    {
        var oldValue:String = _internal_PatientName;
        if (oldValue !== value)
        {
            _internal_PatientName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PatientName", oldValue, _internal_PatientName));
        }
    }

    public function set TreatmentObjects(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_TreatmentObjects;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_TreatmentObjects = value;
            }
            else if (value is Array)
            {
                _internal_TreatmentObjects = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_TreatmentObjects = null;
            }
            else
            {
                throw new Error("value of TreatmentObjects must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TreatmentObjects", oldValue, _internal_TreatmentObjects));
        }
    }

    public function set ProtocolAnamnes(value:String) : void
    {
        var oldValue:String = _internal_ProtocolAnamnes;
        if (oldValue !== value)
        {
            _internal_ProtocolAnamnes = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ProtocolAnamnes", oldValue, _internal_ProtocolAnamnes));
        }
    }

    public function set ProtocolStatus(value:String) : void
    {
        var oldValue:String = _internal_ProtocolStatus;
        if (oldValue !== value)
        {
            _internal_ProtocolStatus = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ProtocolStatus", oldValue, _internal_ProtocolStatus));
        }
    }

    public function set ProtocolRecomend(value:String) : void
    {
        var oldValue:String = _internal_ProtocolRecomend;
        if (oldValue !== value)
        {
            _internal_ProtocolRecomend = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ProtocolRecomend", oldValue, _internal_ProtocolRecomend));
        }
    }

    public function set ProtocolRentgen(value:String) : void
    {
        var oldValue:String = _internal_ProtocolRentgen;
        if (oldValue !== value)
        {
            _internal_ProtocolRentgen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ProtocolRentgen", oldValue, _internal_ProtocolRentgen));
        }
    }

    public function set ProtocolEpicrisis(value:String) : void
    {
        var oldValue:String = _internal_ProtocolEpicrisis;
        if (oldValue !== value)
        {
            _internal_ProtocolEpicrisis = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ProtocolEpicrisis", oldValue, _internal_ProtocolEpicrisis));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HealthPlanMKB10TreatmentCourseEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HealthPlanMKB10TreatmentCourseEntityMetadata) : void
    {
        var oldValue : _HealthPlanMKB10TreatmentCourseEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
