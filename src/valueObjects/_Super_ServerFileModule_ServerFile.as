/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ServerFileModule_ServerFile.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.ServerFileModule_TagValue;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ServerFileModule_ServerFile extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ServerFileModule_ServerFile") == null)
            {
                flash.net.registerClassAlias("ServerFileModule_ServerFile", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ServerFileModule_ServerFile", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ServerFileModule_TagValue.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _ServerFileModule_ServerFileEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_comment : String;
    private var _internal_extention : String;
    private var _internal_name : String;
    private var _internal_serverName : String;
    private var _internal_patientId : String;
    private var _internal_isThumbnailable : Boolean;
    private var _internal_thumbData : Object;
    private var _internal_tagValues : ArrayCollection;
    model_internal var _internal_tagValues_leaf:valueObjects.ServerFileModule_TagValue;
    private var _internal_tooth : String;
    private var _internal_protocolid : String;
    private var _internal_protocolName : String;
    private var _internal_type : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ServerFileModule_ServerFile()
    {
        _model = new _ServerFileModule_ServerFileEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get comment() : String
    {
        return _internal_comment;
    }

    [Bindable(event="propertyChange")]
    public function get extention() : String
    {
        return _internal_extention;
    }

    [Bindable(event="propertyChange")]
    public function get name() : String
    {
        return _internal_name;
    }

    [Bindable(event="propertyChange")]
    public function get serverName() : String
    {
        return _internal_serverName;
    }

    [Bindable(event="propertyChange")]
    public function get patientId() : String
    {
        return _internal_patientId;
    }

    [Bindable(event="propertyChange")]
    public function get isThumbnailable() : Boolean
    {
        return _internal_isThumbnailable;
    }

    [Bindable(event="propertyChange")]
    public function get thumbData() : Object
    {
        return _internal_thumbData;
    }

    [Bindable(event="propertyChange")]
    public function get tagValues() : ArrayCollection
    {
        return _internal_tagValues;
    }

    [Bindable(event="propertyChange")]
    public function get tooth() : String
    {
        return _internal_tooth;
    }

    [Bindable(event="propertyChange")]
    public function get protocolid() : String
    {
        return _internal_protocolid;
    }

    [Bindable(event="propertyChange")]
    public function get protocolName() : String
    {
        return _internal_protocolName;
    }

    [Bindable(event="propertyChange")]
    public function get type() : String
    {
        return _internal_type;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set comment(value:String) : void
    {
        var oldValue:String = _internal_comment;
        if (oldValue !== value)
        {
            _internal_comment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "comment", oldValue, _internal_comment));
        }
    }

    public function set extention(value:String) : void
    {
        var oldValue:String = _internal_extention;
        if (oldValue !== value)
        {
            _internal_extention = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "extention", oldValue, _internal_extention));
        }
    }

    public function set name(value:String) : void
    {
        var oldValue:String = _internal_name;
        if (oldValue !== value)
        {
            _internal_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name", oldValue, _internal_name));
        }
    }

    public function set serverName(value:String) : void
    {
        var oldValue:String = _internal_serverName;
        if (oldValue !== value)
        {
            _internal_serverName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "serverName", oldValue, _internal_serverName));
        }
    }

    public function set patientId(value:String) : void
    {
        var oldValue:String = _internal_patientId;
        if (oldValue !== value)
        {
            _internal_patientId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientId", oldValue, _internal_patientId));
        }
    }

    public function set isThumbnailable(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isThumbnailable;
        if (oldValue !== value)
        {
            _internal_isThumbnailable = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isThumbnailable", oldValue, _internal_isThumbnailable));
        }
    }

    public function set thumbData(value:Object) : void
    {
        var oldValue:Object = _internal_thumbData;
        if (oldValue !== value)
        {
            _internal_thumbData = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "thumbData", oldValue, _internal_thumbData));
        }
    }

    public function set tagValues(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_tagValues;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_tagValues = value;
            }
            else if (value is Array)
            {
                _internal_tagValues = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_tagValues = null;
            }
            else
            {
                throw new Error("value of tagValues must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tagValues", oldValue, _internal_tagValues));
        }
    }

    public function set tooth(value:String) : void
    {
        var oldValue:String = _internal_tooth;
        if (oldValue !== value)
        {
            _internal_tooth = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tooth", oldValue, _internal_tooth));
        }
    }

    public function set protocolid(value:String) : void
    {
        var oldValue:String = _internal_protocolid;
        if (oldValue !== value)
        {
            _internal_protocolid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolid", oldValue, _internal_protocolid));
        }
    }

    public function set protocolName(value:String) : void
    {
        var oldValue:String = _internal_protocolName;
        if (oldValue !== value)
        {
            _internal_protocolName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocolName", oldValue, _internal_protocolName));
        }
    }

    public function set type(value:String) : void
    {
        var oldValue:String = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ServerFileModule_ServerFileEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ServerFileModule_ServerFileEntityMetadata) : void
    {
        var oldValue : _ServerFileModule_ServerFileEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
