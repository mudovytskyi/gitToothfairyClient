/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - NHDiagnosInspectionExamOutAddon.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_NHDiagnosInspectionExamOutAddon extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("NHDiagnosInspectionExamOutAddon") == null)
            {
                flash.net.registerClassAlias("NHDiagnosInspectionExamOutAddon", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("NHDiagnosInspectionExamOutAddon", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _NHDiagnosInspectionExamOutAddonEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_examid : int;
    private var _internal_anamnesis : String;
    private var _internal_status : String;
    private var _internal_recommendations : String;
    private var _internal_temperature : Number;
    private var _internal_height : Number;
    private var _internal_weight : Number;
    private var _internal_systolic_pressure : int;
    private var _internal_diastolic_pressure : int;
    private var _internal_checkupID : String;
    private var _internal_checkupData : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_NHDiagnosInspectionExamOutAddon()
    {
        _model = new _NHDiagnosInspectionExamOutAddonEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get examid() : int
    {
        return _internal_examid;
    }

    [Bindable(event="propertyChange")]
    public function get anamnesis() : String
    {
        return _internal_anamnesis;
    }

    [Bindable(event="propertyChange")]
    public function get status() : String
    {
        return _internal_status;
    }

    [Bindable(event="propertyChange")]
    public function get recommendations() : String
    {
        return _internal_recommendations;
    }

    [Bindable(event="propertyChange")]
    public function get temperature() : Number
    {
        return _internal_temperature;
    }

    [Bindable(event="propertyChange")]
    public function get height() : Number
    {
        return _internal_height;
    }

    [Bindable(event="propertyChange")]
    public function get weight() : Number
    {
        return _internal_weight;
    }

    [Bindable(event="propertyChange")]
    public function get systolic_pressure() : int
    {
        return _internal_systolic_pressure;
    }

    [Bindable(event="propertyChange")]
    public function get diastolic_pressure() : int
    {
        return _internal_diastolic_pressure;
    }

    [Bindable(event="propertyChange")]
    public function get checkupID() : String
    {
        return _internal_checkupID;
    }

    [Bindable(event="propertyChange")]
    public function get checkupData() : String
    {
        return _internal_checkupData;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set examid(value:int) : void
    {
        var oldValue:int = _internal_examid;
        if (oldValue !== value)
        {
            _internal_examid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examid", oldValue, _internal_examid));
        }
    }

    public function set anamnesis(value:String) : void
    {
        var oldValue:String = _internal_anamnesis;
        if (oldValue !== value)
        {
            _internal_anamnesis = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "anamnesis", oldValue, _internal_anamnesis));
        }
    }

    public function set status(value:String) : void
    {
        var oldValue:String = _internal_status;
        if (oldValue !== value)
        {
            _internal_status = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "status", oldValue, _internal_status));
        }
    }

    public function set recommendations(value:String) : void
    {
        var oldValue:String = _internal_recommendations;
        if (oldValue !== value)
        {
            _internal_recommendations = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "recommendations", oldValue, _internal_recommendations));
        }
    }

    public function set temperature(value:Number) : void
    {
        var oldValue:Number = _internal_temperature;
        if (isNaN(_internal_temperature) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_temperature = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "temperature", oldValue, _internal_temperature));
        }
    }

    public function set height(value:Number) : void
    {
        var oldValue:Number = _internal_height;
        if (isNaN(_internal_height) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_height = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "height", oldValue, _internal_height));
        }
    }

    public function set weight(value:Number) : void
    {
        var oldValue:Number = _internal_weight;
        if (isNaN(_internal_weight) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_weight = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "weight", oldValue, _internal_weight));
        }
    }

    public function set systolic_pressure(value:int) : void
    {
        var oldValue:int = _internal_systolic_pressure;
        if (oldValue !== value)
        {
            _internal_systolic_pressure = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "systolic_pressure", oldValue, _internal_systolic_pressure));
        }
    }

    public function set diastolic_pressure(value:int) : void
    {
        var oldValue:int = _internal_diastolic_pressure;
        if (oldValue !== value)
        {
            _internal_diastolic_pressure = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diastolic_pressure", oldValue, _internal_diastolic_pressure));
        }
    }

    public function set checkupID(value:String) : void
    {
        var oldValue:String = _internal_checkupID;
        if (oldValue !== value)
        {
            _internal_checkupID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "checkupID", oldValue, _internal_checkupID));
        }
    }

    public function set checkupData(value:String) : void
    {
        var oldValue:String = _internal_checkupData;
        if (oldValue !== value)
        {
            _internal_checkupData = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "checkupData", oldValue, _internal_checkupData));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _NHDiagnosInspectionExamOutAddonEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _NHDiagnosInspectionExamOutAddonEntityMetadata) : void
    {
        var oldValue : _NHDiagnosInspectionExamOutAddonEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
