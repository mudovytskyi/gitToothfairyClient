
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.AWDDoctorModule_mobilephone;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _AWDDoctorModuleTaskPatientEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("TaskID", "TaskBeginTime", "TaskEndTime", "TaskFactVisit", "TaskFactOutVisit", "TaskNoticed", "TaskWorkDescription", "TaskComment", "PatientID", "PatientLastname", "PatientFirstname", "PatientMiddlename", "PatientFullCardNumber", "PatientPrimaryFlag", "PatientTelephone", "PatientMobile", "PatientBirthday", "PatientSex", "PatientTestFlag", "InvoiceToSaveEnableFlag", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "task_range", "full_addres", "full_fio", "discount_amount", "is_returned", "patient_ages", "patient_notes", "patient_leaddoctor", "mobiles");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("TaskID", "TaskBeginTime", "TaskEndTime", "TaskFactVisit", "TaskFactOutVisit", "TaskNoticed", "TaskWorkDescription", "TaskComment", "PatientID", "PatientLastname", "PatientFirstname", "PatientMiddlename", "PatientFullCardNumber", "PatientPrimaryFlag", "PatientTelephone", "PatientMobile", "PatientBirthday", "PatientSex", "PatientTestFlag", "InvoiceToSaveEnableFlag", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "task_range", "full_addres", "full_fio", "discount_amount", "is_returned", "patient_ages", "patient_notes", "patient_leaddoctor", "mobiles");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("TaskID", "TaskBeginTime", "TaskEndTime", "TaskFactVisit", "TaskFactOutVisit", "TaskNoticed", "TaskWorkDescription", "TaskComment", "PatientID", "PatientLastname", "PatientFirstname", "PatientMiddlename", "PatientFullCardNumber", "PatientPrimaryFlag", "PatientTelephone", "PatientMobile", "PatientBirthday", "PatientSex", "PatientTestFlag", "InvoiceToSaveEnableFlag", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "task_range", "full_addres", "full_fio", "discount_amount", "is_returned", "patient_ages", "patient_notes", "patient_leaddoctor", "mobiles");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("TaskID", "TaskBeginTime", "TaskEndTime", "TaskFactVisit", "TaskFactOutVisit", "TaskNoticed", "TaskWorkDescription", "TaskComment", "PatientID", "PatientLastname", "PatientFirstname", "PatientMiddlename", "PatientFullCardNumber", "PatientPrimaryFlag", "PatientTelephone", "PatientMobile", "PatientBirthday", "PatientSex", "PatientTestFlag", "InvoiceToSaveEnableFlag", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "task_range", "full_addres", "full_fio", "discount_amount", "is_returned", "patient_ages", "patient_notes", "patient_leaddoctor", "mobiles");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("mobiles");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "AWDDoctorModuleTaskPatient";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_AWDDoctorModuleTaskPatient;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _AWDDoctorModuleTaskPatientEntityMetadata(value : _Super_AWDDoctorModuleTaskPatient)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["TaskID"] = new Array();
            model_internal::dependentsOnMap["TaskBeginTime"] = new Array();
            model_internal::dependentsOnMap["TaskEndTime"] = new Array();
            model_internal::dependentsOnMap["TaskFactVisit"] = new Array();
            model_internal::dependentsOnMap["TaskFactOutVisit"] = new Array();
            model_internal::dependentsOnMap["TaskNoticed"] = new Array();
            model_internal::dependentsOnMap["TaskWorkDescription"] = new Array();
            model_internal::dependentsOnMap["TaskComment"] = new Array();
            model_internal::dependentsOnMap["PatientID"] = new Array();
            model_internal::dependentsOnMap["PatientLastname"] = new Array();
            model_internal::dependentsOnMap["PatientFirstname"] = new Array();
            model_internal::dependentsOnMap["PatientMiddlename"] = new Array();
            model_internal::dependentsOnMap["PatientFullCardNumber"] = new Array();
            model_internal::dependentsOnMap["PatientPrimaryFlag"] = new Array();
            model_internal::dependentsOnMap["PatientTelephone"] = new Array();
            model_internal::dependentsOnMap["PatientMobile"] = new Array();
            model_internal::dependentsOnMap["PatientBirthday"] = new Array();
            model_internal::dependentsOnMap["PatientSex"] = new Array();
            model_internal::dependentsOnMap["PatientTestFlag"] = new Array();
            model_internal::dependentsOnMap["InvoiceToSaveEnableFlag"] = new Array();
            model_internal::dependentsOnMap["COUNTS_TOOTHEXAMS"] = new Array();
            model_internal::dependentsOnMap["COUNTS_PROCEDURES_CLOSED"] = new Array();
            model_internal::dependentsOnMap["COUNTS_PROCEDURES_NOTCLOSED"] = new Array();
            model_internal::dependentsOnMap["COUNTS_FILES"] = new Array();
            model_internal::dependentsOnMap["task_range"] = new Array();
            model_internal::dependentsOnMap["full_addres"] = new Array();
            model_internal::dependentsOnMap["full_fio"] = new Array();
            model_internal::dependentsOnMap["discount_amount"] = new Array();
            model_internal::dependentsOnMap["is_returned"] = new Array();
            model_internal::dependentsOnMap["patient_ages"] = new Array();
            model_internal::dependentsOnMap["patient_notes"] = new Array();
            model_internal::dependentsOnMap["patient_leaddoctor"] = new Array();
            model_internal::dependentsOnMap["mobiles"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["mobiles"] = "valueObjects.AWDDoctorModule_mobilephone";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["TaskID"] = "String";
        model_internal::propertyTypeMap["TaskBeginTime"] = "String";
        model_internal::propertyTypeMap["TaskEndTime"] = "String";
        model_internal::propertyTypeMap["TaskFactVisit"] = "String";
        model_internal::propertyTypeMap["TaskFactOutVisit"] = "String";
        model_internal::propertyTypeMap["TaskNoticed"] = "String";
        model_internal::propertyTypeMap["TaskWorkDescription"] = "String";
        model_internal::propertyTypeMap["TaskComment"] = "String";
        model_internal::propertyTypeMap["PatientID"] = "String";
        model_internal::propertyTypeMap["PatientLastname"] = "String";
        model_internal::propertyTypeMap["PatientFirstname"] = "String";
        model_internal::propertyTypeMap["PatientMiddlename"] = "String";
        model_internal::propertyTypeMap["PatientFullCardNumber"] = "String";
        model_internal::propertyTypeMap["PatientPrimaryFlag"] = "String";
        model_internal::propertyTypeMap["PatientTelephone"] = "String";
        model_internal::propertyTypeMap["PatientMobile"] = "String";
        model_internal::propertyTypeMap["PatientBirthday"] = "String";
        model_internal::propertyTypeMap["PatientSex"] = "String";
        model_internal::propertyTypeMap["PatientTestFlag"] = "String";
        model_internal::propertyTypeMap["InvoiceToSaveEnableFlag"] = "int";
        model_internal::propertyTypeMap["COUNTS_TOOTHEXAMS"] = "int";
        model_internal::propertyTypeMap["COUNTS_PROCEDURES_CLOSED"] = "int";
        model_internal::propertyTypeMap["COUNTS_PROCEDURES_NOTCLOSED"] = "int";
        model_internal::propertyTypeMap["COUNTS_FILES"] = "int";
        model_internal::propertyTypeMap["task_range"] = "String";
        model_internal::propertyTypeMap["full_addres"] = "String";
        model_internal::propertyTypeMap["full_fio"] = "String";
        model_internal::propertyTypeMap["discount_amount"] = "Number";
        model_internal::propertyTypeMap["is_returned"] = "int";
        model_internal::propertyTypeMap["patient_ages"] = "int";
        model_internal::propertyTypeMap["patient_notes"] = "String";
        model_internal::propertyTypeMap["patient_leaddoctor"] = "String";
        model_internal::propertyTypeMap["mobiles"] = "ArrayCollection";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity AWDDoctorModuleTaskPatient");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity AWDDoctorModuleTaskPatient");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of AWDDoctorModuleTaskPatient");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AWDDoctorModuleTaskPatient");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity AWDDoctorModuleTaskPatient");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AWDDoctorModuleTaskPatient");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isTaskIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTaskBeginTimeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTaskEndTimeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTaskFactVisitAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTaskFactOutVisitAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTaskNoticedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTaskWorkDescriptionAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTaskCommentAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientLastnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientFirstnameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientMiddlenameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientFullCardNumberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientPrimaryFlagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientTelephoneAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientMobileAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientBirthdayAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientSexAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatientTestFlagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInvoiceToSaveEnableFlagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_TOOTHEXAMSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_PROCEDURES_CLOSEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_PROCEDURES_NOTCLOSEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_FILESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTask_rangeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFull_addresAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFull_fioAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_amountAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIs_returnedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_agesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_notesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_leaddoctorAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMobilesAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get TaskIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TaskBeginTimeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TaskEndTimeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TaskFactVisitStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TaskFactOutVisitStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TaskNoticedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TaskWorkDescriptionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TaskCommentStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientLastnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientFirstnameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientMiddlenameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientFullCardNumberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientPrimaryFlagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientTelephoneStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientMobileStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientBirthdayStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientSexStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PatientTestFlagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get InvoiceToSaveEnableFlagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_TOOTHEXAMSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_PROCEDURES_CLOSEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_PROCEDURES_NOTCLOSEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_FILESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get task_rangeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get full_addresStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get full_fioStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_amountStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get is_returnedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_agesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_notesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_leaddoctorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get mobilesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
