/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefExpensesModuleTreatmentProcOLAPData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefExpensesModuleTreatmentProcOLAPData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefExpensesModuleTreatmentProcOLAPData") == null)
            {
                flash.net.registerClassAlias("ChiefExpensesModuleTreatmentProcOLAPData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefExpensesModuleTreatmentProcOLAPData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefExpensesModuleTreatmentProcOLAPDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_treatmentproc_id : String;
    private var _internal_treatmentproc_shifr : String;
    private var _internal_treatmentproc_name : String;
    private var _internal_treatmentproc_nameforplan : String;
    private var _internal_treatmentproc_price : Number;
    private var _internal_doctor_id : String;
    private var _internal_doctor_lastname : String;
    private var _internal_doctor_firstname : String;
    private var _internal_doctor_middlename : String;
    private var _internal_doctor_shortname : String;
    private var _internal_treatmentproc_dateclose : String;
    private var _internal_treatmentproc_invoiceid : String;
    private var _internal_treatmentproc_expenses : Number;
    private var _internal_assistant_id : String;
    private var _internal_assistant_lastname : String;
    private var _internal_assistant_firstname : String;
    private var _internal_assistant_middlename : String;
    private var _internal_assistant_shortname : String;
    private var _internal_treatmentproc_proc_count : int;
    private var _internal_treatmentproc_healthproc_fk : String;
    private var _internal_treatmentproc_expenses_fact : Number;
    private var _internal_accountflow_id : String;
    private var _internal_patient_id : String;
    private var _internal_patient_lastname : String;
    private var _internal_patient_firstname : String;
    private var _internal_patient_middlename : String;
    private var _internal_patient_shortname : String;
    private var _internal_accountflow_summ : Number;
    private var _internal_accountflow_operationnote : String;
    private var _internal_accountflow_createdate : String;
    private var _internal_accountflow_operationtimestamp : String;
    private var _internal_accountflow_is_cashpayment : String;
    private var _internal_texp_group_id : String;
    private var _internal_texp_group_name : String;
    private var _internal_texp_group_color : int;
    private var _internal_texp_group_gridsequence : int;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefExpensesModuleTreatmentProcOLAPData()
    {
        _model = new _ChiefExpensesModuleTreatmentProcOLAPDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get treatmentproc_id() : String
    {
        return _internal_treatmentproc_id;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_shifr() : String
    {
        return _internal_treatmentproc_shifr;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_name() : String
    {
        return _internal_treatmentproc_name;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_nameforplan() : String
    {
        return _internal_treatmentproc_nameforplan;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_price() : Number
    {
        return _internal_treatmentproc_price;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lastname() : String
    {
        return _internal_doctor_lastname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_firstname() : String
    {
        return _internal_doctor_firstname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_middlename() : String
    {
        return _internal_doctor_middlename;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_shortname() : String
    {
        return _internal_doctor_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_dateclose() : String
    {
        return _internal_treatmentproc_dateclose;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_invoiceid() : String
    {
        return _internal_treatmentproc_invoiceid;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_expenses() : Number
    {
        return _internal_treatmentproc_expenses;
    }

    [Bindable(event="propertyChange")]
    public function get assistant_id() : String
    {
        return _internal_assistant_id;
    }

    [Bindable(event="propertyChange")]
    public function get assistant_lastname() : String
    {
        return _internal_assistant_lastname;
    }

    [Bindable(event="propertyChange")]
    public function get assistant_firstname() : String
    {
        return _internal_assistant_firstname;
    }

    [Bindable(event="propertyChange")]
    public function get assistant_middlename() : String
    {
        return _internal_assistant_middlename;
    }

    [Bindable(event="propertyChange")]
    public function get assistant_shortname() : String
    {
        return _internal_assistant_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_proc_count() : int
    {
        return _internal_treatmentproc_proc_count;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_healthproc_fk() : String
    {
        return _internal_treatmentproc_healthproc_fk;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentproc_expenses_fact() : Number
    {
        return _internal_treatmentproc_expenses_fact;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_id() : String
    {
        return _internal_accountflow_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_lastname() : String
    {
        return _internal_patient_lastname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_firstname() : String
    {
        return _internal_patient_firstname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_middlename() : String
    {
        return _internal_patient_middlename;
    }

    [Bindable(event="propertyChange")]
    public function get patient_shortname() : String
    {
        return _internal_patient_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_summ() : Number
    {
        return _internal_accountflow_summ;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_operationnote() : String
    {
        return _internal_accountflow_operationnote;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_createdate() : String
    {
        return _internal_accountflow_createdate;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_operationtimestamp() : String
    {
        return _internal_accountflow_operationtimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_is_cashpayment() : String
    {
        return _internal_accountflow_is_cashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get texp_group_id() : String
    {
        return _internal_texp_group_id;
    }

    [Bindable(event="propertyChange")]
    public function get texp_group_name() : String
    {
        return _internal_texp_group_name;
    }

    [Bindable(event="propertyChange")]
    public function get texp_group_color() : int
    {
        return _internal_texp_group_color;
    }

    [Bindable(event="propertyChange")]
    public function get texp_group_gridsequence() : int
    {
        return _internal_texp_group_gridsequence;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set treatmentproc_id(value:String) : void
    {
        var oldValue:String = _internal_treatmentproc_id;
        if (oldValue !== value)
        {
            _internal_treatmentproc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_id", oldValue, _internal_treatmentproc_id));
        }
    }

    public function set treatmentproc_shifr(value:String) : void
    {
        var oldValue:String = _internal_treatmentproc_shifr;
        if (oldValue !== value)
        {
            _internal_treatmentproc_shifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_shifr", oldValue, _internal_treatmentproc_shifr));
        }
    }

    public function set treatmentproc_name(value:String) : void
    {
        var oldValue:String = _internal_treatmentproc_name;
        if (oldValue !== value)
        {
            _internal_treatmentproc_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_name", oldValue, _internal_treatmentproc_name));
        }
    }

    public function set treatmentproc_nameforplan(value:String) : void
    {
        var oldValue:String = _internal_treatmentproc_nameforplan;
        if (oldValue !== value)
        {
            _internal_treatmentproc_nameforplan = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_nameforplan", oldValue, _internal_treatmentproc_nameforplan));
        }
    }

    public function set treatmentproc_price(value:Number) : void
    {
        var oldValue:Number = _internal_treatmentproc_price;
        if (isNaN(_internal_treatmentproc_price) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_treatmentproc_price = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_price", oldValue, _internal_treatmentproc_price));
        }
    }

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_lastname(value:String) : void
    {
        var oldValue:String = _internal_doctor_lastname;
        if (oldValue !== value)
        {
            _internal_doctor_lastname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lastname", oldValue, _internal_doctor_lastname));
        }
    }

    public function set doctor_firstname(value:String) : void
    {
        var oldValue:String = _internal_doctor_firstname;
        if (oldValue !== value)
        {
            _internal_doctor_firstname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_firstname", oldValue, _internal_doctor_firstname));
        }
    }

    public function set doctor_middlename(value:String) : void
    {
        var oldValue:String = _internal_doctor_middlename;
        if (oldValue !== value)
        {
            _internal_doctor_middlename = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_middlename", oldValue, _internal_doctor_middlename));
        }
    }

    public function set doctor_shortname(value:String) : void
    {
        var oldValue:String = _internal_doctor_shortname;
        if (oldValue !== value)
        {
            _internal_doctor_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_shortname", oldValue, _internal_doctor_shortname));
        }
    }

    public function set treatmentproc_dateclose(value:String) : void
    {
        var oldValue:String = _internal_treatmentproc_dateclose;
        if (oldValue !== value)
        {
            _internal_treatmentproc_dateclose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_dateclose", oldValue, _internal_treatmentproc_dateclose));
        }
    }

    public function set treatmentproc_invoiceid(value:String) : void
    {
        var oldValue:String = _internal_treatmentproc_invoiceid;
        if (oldValue !== value)
        {
            _internal_treatmentproc_invoiceid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_invoiceid", oldValue, _internal_treatmentproc_invoiceid));
        }
    }

    public function set treatmentproc_expenses(value:Number) : void
    {
        var oldValue:Number = _internal_treatmentproc_expenses;
        if (isNaN(_internal_treatmentproc_expenses) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_treatmentproc_expenses = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_expenses", oldValue, _internal_treatmentproc_expenses));
        }
    }

    public function set assistant_id(value:String) : void
    {
        var oldValue:String = _internal_assistant_id;
        if (oldValue !== value)
        {
            _internal_assistant_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistant_id", oldValue, _internal_assistant_id));
        }
    }

    public function set assistant_lastname(value:String) : void
    {
        var oldValue:String = _internal_assistant_lastname;
        if (oldValue !== value)
        {
            _internal_assistant_lastname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistant_lastname", oldValue, _internal_assistant_lastname));
        }
    }

    public function set assistant_firstname(value:String) : void
    {
        var oldValue:String = _internal_assistant_firstname;
        if (oldValue !== value)
        {
            _internal_assistant_firstname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistant_firstname", oldValue, _internal_assistant_firstname));
        }
    }

    public function set assistant_middlename(value:String) : void
    {
        var oldValue:String = _internal_assistant_middlename;
        if (oldValue !== value)
        {
            _internal_assistant_middlename = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistant_middlename", oldValue, _internal_assistant_middlename));
        }
    }

    public function set assistant_shortname(value:String) : void
    {
        var oldValue:String = _internal_assistant_shortname;
        if (oldValue !== value)
        {
            _internal_assistant_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistant_shortname", oldValue, _internal_assistant_shortname));
        }
    }

    public function set treatmentproc_proc_count(value:int) : void
    {
        var oldValue:int = _internal_treatmentproc_proc_count;
        if (oldValue !== value)
        {
            _internal_treatmentproc_proc_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_proc_count", oldValue, _internal_treatmentproc_proc_count));
        }
    }

    public function set treatmentproc_healthproc_fk(value:String) : void
    {
        var oldValue:String = _internal_treatmentproc_healthproc_fk;
        if (oldValue !== value)
        {
            _internal_treatmentproc_healthproc_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_healthproc_fk", oldValue, _internal_treatmentproc_healthproc_fk));
        }
    }

    public function set treatmentproc_expenses_fact(value:Number) : void
    {
        var oldValue:Number = _internal_treatmentproc_expenses_fact;
        if (isNaN(_internal_treatmentproc_expenses_fact) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_treatmentproc_expenses_fact = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentproc_expenses_fact", oldValue, _internal_treatmentproc_expenses_fact));
        }
    }

    public function set accountflow_id(value:String) : void
    {
        var oldValue:String = _internal_accountflow_id;
        if (oldValue !== value)
        {
            _internal_accountflow_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_id", oldValue, _internal_accountflow_id));
        }
    }

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set patient_lastname(value:String) : void
    {
        var oldValue:String = _internal_patient_lastname;
        if (oldValue !== value)
        {
            _internal_patient_lastname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_lastname", oldValue, _internal_patient_lastname));
        }
    }

    public function set patient_firstname(value:String) : void
    {
        var oldValue:String = _internal_patient_firstname;
        if (oldValue !== value)
        {
            _internal_patient_firstname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_firstname", oldValue, _internal_patient_firstname));
        }
    }

    public function set patient_middlename(value:String) : void
    {
        var oldValue:String = _internal_patient_middlename;
        if (oldValue !== value)
        {
            _internal_patient_middlename = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_middlename", oldValue, _internal_patient_middlename));
        }
    }

    public function set patient_shortname(value:String) : void
    {
        var oldValue:String = _internal_patient_shortname;
        if (oldValue !== value)
        {
            _internal_patient_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_shortname", oldValue, _internal_patient_shortname));
        }
    }

    public function set accountflow_summ(value:Number) : void
    {
        var oldValue:Number = _internal_accountflow_summ;
        if (isNaN(_internal_accountflow_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_accountflow_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_summ", oldValue, _internal_accountflow_summ));
        }
    }

    public function set accountflow_operationnote(value:String) : void
    {
        var oldValue:String = _internal_accountflow_operationnote;
        if (oldValue !== value)
        {
            _internal_accountflow_operationnote = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_operationnote", oldValue, _internal_accountflow_operationnote));
        }
    }

    public function set accountflow_createdate(value:String) : void
    {
        var oldValue:String = _internal_accountflow_createdate;
        if (oldValue !== value)
        {
            _internal_accountflow_createdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_createdate", oldValue, _internal_accountflow_createdate));
        }
    }

    public function set accountflow_operationtimestamp(value:String) : void
    {
        var oldValue:String = _internal_accountflow_operationtimestamp;
        if (oldValue !== value)
        {
            _internal_accountflow_operationtimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_operationtimestamp", oldValue, _internal_accountflow_operationtimestamp));
        }
    }

    public function set accountflow_is_cashpayment(value:String) : void
    {
        var oldValue:String = _internal_accountflow_is_cashpayment;
        if (oldValue !== value)
        {
            _internal_accountflow_is_cashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_is_cashpayment", oldValue, _internal_accountflow_is_cashpayment));
        }
    }

    public function set texp_group_id(value:String) : void
    {
        var oldValue:String = _internal_texp_group_id;
        if (oldValue !== value)
        {
            _internal_texp_group_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "texp_group_id", oldValue, _internal_texp_group_id));
        }
    }

    public function set texp_group_name(value:String) : void
    {
        var oldValue:String = _internal_texp_group_name;
        if (oldValue !== value)
        {
            _internal_texp_group_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "texp_group_name", oldValue, _internal_texp_group_name));
        }
    }

    public function set texp_group_color(value:int) : void
    {
        var oldValue:int = _internal_texp_group_color;
        if (oldValue !== value)
        {
            _internal_texp_group_color = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "texp_group_color", oldValue, _internal_texp_group_color));
        }
    }

    public function set texp_group_gridsequence(value:int) : void
    {
        var oldValue:int = _internal_texp_group_gridsequence;
        if (oldValue !== value)
        {
            _internal_texp_group_gridsequence = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "texp_group_gridsequence", oldValue, _internal_texp_group_gridsequence));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefExpensesModuleTreatmentProcOLAPDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefExpensesModuleTreatmentProcOLAPDataEntityMetadata) : void
    {
        var oldValue : _ChiefExpensesModuleTreatmentProcOLAPDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
