/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefBudgetModule_BudgetStatistics.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.ChiefBudgetModule_BudgetData;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefBudgetModule_BudgetStatistics extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefBudgetModule_BudgetStatistics") == null)
            {
                flash.net.registerClassAlias("ChiefBudgetModule_BudgetStatistics", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefBudgetModule_BudgetStatistics", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ChiefBudgetModule_BudgetData.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _ChiefBudgetModule_BudgetStatisticsEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_period : String;
    private var _internal_period_type : String;
    private var _internal_period_startDate : String;
    private var _internal_period_endDate : String;
    private var _internal_cash_incomeSumm : Number;
    private var _internal_cash_expenseSumm : Number;
    private var _internal_noncash_incomeSumm : Number;
    private var _internal_noncash_expenseSumm : Number;
    private var _internal_budget : ArrayCollection;
    model_internal var _internal_budget_leaf:valueObjects.ChiefBudgetModule_BudgetData;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefBudgetModule_BudgetStatistics()
    {
        _model = new _ChiefBudgetModule_BudgetStatisticsEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get period() : String
    {
        return _internal_period;
    }

    [Bindable(event="propertyChange")]
    public function get period_type() : String
    {
        return _internal_period_type;
    }

    [Bindable(event="propertyChange")]
    public function get period_startDate() : String
    {
        return _internal_period_startDate;
    }

    [Bindable(event="propertyChange")]
    public function get period_endDate() : String
    {
        return _internal_period_endDate;
    }

    [Bindable(event="propertyChange")]
    public function get cash_incomeSumm() : Number
    {
        return _internal_cash_incomeSumm;
    }

    [Bindable(event="propertyChange")]
    public function get cash_expenseSumm() : Number
    {
        return _internal_cash_expenseSumm;
    }

    [Bindable(event="propertyChange")]
    public function get noncash_incomeSumm() : Number
    {
        return _internal_noncash_incomeSumm;
    }

    [Bindable(event="propertyChange")]
    public function get noncash_expenseSumm() : Number
    {
        return _internal_noncash_expenseSumm;
    }

    [Bindable(event="propertyChange")]
    public function get budget() : ArrayCollection
    {
        return _internal_budget;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set period(value:String) : void
    {
        var oldValue:String = _internal_period;
        if (oldValue !== value)
        {
            _internal_period = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period", oldValue, _internal_period));
        }
    }

    public function set period_type(value:String) : void
    {
        var oldValue:String = _internal_period_type;
        if (oldValue !== value)
        {
            _internal_period_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period_type", oldValue, _internal_period_type));
        }
    }

    public function set period_startDate(value:String) : void
    {
        var oldValue:String = _internal_period_startDate;
        if (oldValue !== value)
        {
            _internal_period_startDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period_startDate", oldValue, _internal_period_startDate));
        }
    }

    public function set period_endDate(value:String) : void
    {
        var oldValue:String = _internal_period_endDate;
        if (oldValue !== value)
        {
            _internal_period_endDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "period_endDate", oldValue, _internal_period_endDate));
        }
    }

    public function set cash_incomeSumm(value:Number) : void
    {
        var oldValue:Number = _internal_cash_incomeSumm;
        if (isNaN(_internal_cash_incomeSumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_cash_incomeSumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cash_incomeSumm", oldValue, _internal_cash_incomeSumm));
        }
    }

    public function set cash_expenseSumm(value:Number) : void
    {
        var oldValue:Number = _internal_cash_expenseSumm;
        if (isNaN(_internal_cash_expenseSumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_cash_expenseSumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cash_expenseSumm", oldValue, _internal_cash_expenseSumm));
        }
    }

    public function set noncash_incomeSumm(value:Number) : void
    {
        var oldValue:Number = _internal_noncash_incomeSumm;
        if (isNaN(_internal_noncash_incomeSumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_noncash_incomeSumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "noncash_incomeSumm", oldValue, _internal_noncash_incomeSumm));
        }
    }

    public function set noncash_expenseSumm(value:Number) : void
    {
        var oldValue:Number = _internal_noncash_expenseSumm;
        if (isNaN(_internal_noncash_expenseSumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_noncash_expenseSumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "noncash_expenseSumm", oldValue, _internal_noncash_expenseSumm));
        }
    }

    public function set budget(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_budget;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_budget = value;
            }
            else if (value is Array)
            {
                _internal_budget = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_budget = null;
            }
            else
            {
                throw new Error("value of budget must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "budget", oldValue, _internal_budget));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefBudgetModule_BudgetStatisticsEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefBudgetModule_BudgetStatisticsEntityMetadata) : void
    {
        var oldValue : _ChiefBudgetModule_BudgetStatisticsEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
