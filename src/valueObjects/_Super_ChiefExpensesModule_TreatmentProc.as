/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefExpensesModule_TreatmentProc.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefExpensesModule_TreatmentProc extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefExpensesModule_TreatmentProc") == null)
            {
                flash.net.registerClassAlias("ChiefExpensesModule_TreatmentProc", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefExpensesModule_TreatmentProc", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefExpensesModule_TreatmentProcEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_num : int;
    private var _internal_type : String;
    private var _internal_proc_id : String;
    private var _internal_proc_name : String;
    private var _internal_proc_shifr : String;
    private var _internal_proc_closedate : String;
    private var _internal_proc_expenses_plane : Number;
    private var _internal_proc_expenses_fact : Number;
    private var _internal_doc_id : String;
    private var _internal_doc_lname : String;
    private var _internal_doc_fname : String;
    private var _internal_doc_sname : String;
    private var _internal_pat_id : String;
    private var _internal_pat_lname : String;
    private var _internal_pat_fname : String;
    private var _internal_pat_sname : String;
    private var _internal_ass_id : String;
    private var _internal_ass_lname : String;
    private var _internal_ass_fname : String;
    private var _internal_ass_sname : String;
    private var _internal_accflow_id : String;
    private var _internal_accflow_summ : Number;
    private var _internal_accflow_note : String;
    private var _internal_accflow_is_cashpayment : int;
    private var _internal_accflow_operationtimestamp : String;
    private var _internal_accflow_createdate : String;
    private var _internal_accflow_group_id : String;
    private var _internal_accflow_group_name : String;
    private var _internal_accflow_group_color : int;
    private var _internal_accflow_group_gridsequence : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefExpensesModule_TreatmentProc()
    {
        _model = new _ChiefExpensesModule_TreatmentProcEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get num() : int
    {
        return _internal_num;
    }

    [Bindable(event="propertyChange")]
    public function get type() : String
    {
        return _internal_type;
    }

    [Bindable(event="propertyChange")]
    public function get proc_id() : String
    {
        return _internal_proc_id;
    }

    [Bindable(event="propertyChange")]
    public function get proc_name() : String
    {
        return _internal_proc_name;
    }

    [Bindable(event="propertyChange")]
    public function get proc_shifr() : String
    {
        return _internal_proc_shifr;
    }

    [Bindable(event="propertyChange")]
    public function get proc_closedate() : String
    {
        return _internal_proc_closedate;
    }

    [Bindable(event="propertyChange")]
    public function get proc_expenses_plane() : Number
    {
        return _internal_proc_expenses_plane;
    }

    [Bindable(event="propertyChange")]
    public function get proc_expenses_fact() : Number
    {
        return _internal_proc_expenses_fact;
    }

    [Bindable(event="propertyChange")]
    public function get doc_id() : String
    {
        return _internal_doc_id;
    }

    [Bindable(event="propertyChange")]
    public function get doc_lname() : String
    {
        return _internal_doc_lname;
    }

    [Bindable(event="propertyChange")]
    public function get doc_fname() : String
    {
        return _internal_doc_fname;
    }

    [Bindable(event="propertyChange")]
    public function get doc_sname() : String
    {
        return _internal_doc_sname;
    }

    [Bindable(event="propertyChange")]
    public function get pat_id() : String
    {
        return _internal_pat_id;
    }

    [Bindable(event="propertyChange")]
    public function get pat_lname() : String
    {
        return _internal_pat_lname;
    }

    [Bindable(event="propertyChange")]
    public function get pat_fname() : String
    {
        return _internal_pat_fname;
    }

    [Bindable(event="propertyChange")]
    public function get pat_sname() : String
    {
        return _internal_pat_sname;
    }

    [Bindable(event="propertyChange")]
    public function get ass_id() : String
    {
        return _internal_ass_id;
    }

    [Bindable(event="propertyChange")]
    public function get ass_lname() : String
    {
        return _internal_ass_lname;
    }

    [Bindable(event="propertyChange")]
    public function get ass_fname() : String
    {
        return _internal_ass_fname;
    }

    [Bindable(event="propertyChange")]
    public function get ass_sname() : String
    {
        return _internal_ass_sname;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_id() : String
    {
        return _internal_accflow_id;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_summ() : Number
    {
        return _internal_accflow_summ;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_note() : String
    {
        return _internal_accflow_note;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_is_cashpayment() : int
    {
        return _internal_accflow_is_cashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_operationtimestamp() : String
    {
        return _internal_accflow_operationtimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_createdate() : String
    {
        return _internal_accflow_createdate;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_group_id() : String
    {
        return _internal_accflow_group_id;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_group_name() : String
    {
        return _internal_accflow_group_name;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_group_color() : int
    {
        return _internal_accflow_group_color;
    }

    [Bindable(event="propertyChange")]
    public function get accflow_group_gridsequence() : String
    {
        return _internal_accflow_group_gridsequence;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set num(value:int) : void
    {
        var oldValue:int = _internal_num;
        if (oldValue !== value)
        {
            _internal_num = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "num", oldValue, _internal_num));
        }
    }

    public function set type(value:String) : void
    {
        var oldValue:String = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    public function set proc_id(value:String) : void
    {
        var oldValue:String = _internal_proc_id;
        if (oldValue !== value)
        {
            _internal_proc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_id", oldValue, _internal_proc_id));
        }
    }

    public function set proc_name(value:String) : void
    {
        var oldValue:String = _internal_proc_name;
        if (oldValue !== value)
        {
            _internal_proc_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_name", oldValue, _internal_proc_name));
        }
    }

    public function set proc_shifr(value:String) : void
    {
        var oldValue:String = _internal_proc_shifr;
        if (oldValue !== value)
        {
            _internal_proc_shifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_shifr", oldValue, _internal_proc_shifr));
        }
    }

    public function set proc_closedate(value:String) : void
    {
        var oldValue:String = _internal_proc_closedate;
        if (oldValue !== value)
        {
            _internal_proc_closedate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_closedate", oldValue, _internal_proc_closedate));
        }
    }

    public function set proc_expenses_plane(value:Number) : void
    {
        var oldValue:Number = _internal_proc_expenses_plane;
        if (isNaN(_internal_proc_expenses_plane) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_proc_expenses_plane = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_expenses_plane", oldValue, _internal_proc_expenses_plane));
        }
    }

    public function set proc_expenses_fact(value:Number) : void
    {
        var oldValue:Number = _internal_proc_expenses_fact;
        if (isNaN(_internal_proc_expenses_fact) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_proc_expenses_fact = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_expenses_fact", oldValue, _internal_proc_expenses_fact));
        }
    }

    public function set doc_id(value:String) : void
    {
        var oldValue:String = _internal_doc_id;
        if (oldValue !== value)
        {
            _internal_doc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doc_id", oldValue, _internal_doc_id));
        }
    }

    public function set doc_lname(value:String) : void
    {
        var oldValue:String = _internal_doc_lname;
        if (oldValue !== value)
        {
            _internal_doc_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doc_lname", oldValue, _internal_doc_lname));
        }
    }

    public function set doc_fname(value:String) : void
    {
        var oldValue:String = _internal_doc_fname;
        if (oldValue !== value)
        {
            _internal_doc_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doc_fname", oldValue, _internal_doc_fname));
        }
    }

    public function set doc_sname(value:String) : void
    {
        var oldValue:String = _internal_doc_sname;
        if (oldValue !== value)
        {
            _internal_doc_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doc_sname", oldValue, _internal_doc_sname));
        }
    }

    public function set pat_id(value:String) : void
    {
        var oldValue:String = _internal_pat_id;
        if (oldValue !== value)
        {
            _internal_pat_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "pat_id", oldValue, _internal_pat_id));
        }
    }

    public function set pat_lname(value:String) : void
    {
        var oldValue:String = _internal_pat_lname;
        if (oldValue !== value)
        {
            _internal_pat_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "pat_lname", oldValue, _internal_pat_lname));
        }
    }

    public function set pat_fname(value:String) : void
    {
        var oldValue:String = _internal_pat_fname;
        if (oldValue !== value)
        {
            _internal_pat_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "pat_fname", oldValue, _internal_pat_fname));
        }
    }

    public function set pat_sname(value:String) : void
    {
        var oldValue:String = _internal_pat_sname;
        if (oldValue !== value)
        {
            _internal_pat_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "pat_sname", oldValue, _internal_pat_sname));
        }
    }

    public function set ass_id(value:String) : void
    {
        var oldValue:String = _internal_ass_id;
        if (oldValue !== value)
        {
            _internal_ass_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ass_id", oldValue, _internal_ass_id));
        }
    }

    public function set ass_lname(value:String) : void
    {
        var oldValue:String = _internal_ass_lname;
        if (oldValue !== value)
        {
            _internal_ass_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ass_lname", oldValue, _internal_ass_lname));
        }
    }

    public function set ass_fname(value:String) : void
    {
        var oldValue:String = _internal_ass_fname;
        if (oldValue !== value)
        {
            _internal_ass_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ass_fname", oldValue, _internal_ass_fname));
        }
    }

    public function set ass_sname(value:String) : void
    {
        var oldValue:String = _internal_ass_sname;
        if (oldValue !== value)
        {
            _internal_ass_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ass_sname", oldValue, _internal_ass_sname));
        }
    }

    public function set accflow_id(value:String) : void
    {
        var oldValue:String = _internal_accflow_id;
        if (oldValue !== value)
        {
            _internal_accflow_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_id", oldValue, _internal_accflow_id));
        }
    }

    public function set accflow_summ(value:Number) : void
    {
        var oldValue:Number = _internal_accflow_summ;
        if (isNaN(_internal_accflow_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_accflow_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_summ", oldValue, _internal_accflow_summ));
        }
    }

    public function set accflow_note(value:String) : void
    {
        var oldValue:String = _internal_accflow_note;
        if (oldValue !== value)
        {
            _internal_accflow_note = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_note", oldValue, _internal_accflow_note));
        }
    }

    public function set accflow_is_cashpayment(value:int) : void
    {
        var oldValue:int = _internal_accflow_is_cashpayment;
        if (oldValue !== value)
        {
            _internal_accflow_is_cashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_is_cashpayment", oldValue, _internal_accflow_is_cashpayment));
        }
    }

    public function set accflow_operationtimestamp(value:String) : void
    {
        var oldValue:String = _internal_accflow_operationtimestamp;
        if (oldValue !== value)
        {
            _internal_accflow_operationtimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_operationtimestamp", oldValue, _internal_accflow_operationtimestamp));
        }
    }

    public function set accflow_createdate(value:String) : void
    {
        var oldValue:String = _internal_accflow_createdate;
        if (oldValue !== value)
        {
            _internal_accflow_createdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_createdate", oldValue, _internal_accflow_createdate));
        }
    }

    public function set accflow_group_id(value:String) : void
    {
        var oldValue:String = _internal_accflow_group_id;
        if (oldValue !== value)
        {
            _internal_accflow_group_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_group_id", oldValue, _internal_accflow_group_id));
        }
    }

    public function set accflow_group_name(value:String) : void
    {
        var oldValue:String = _internal_accflow_group_name;
        if (oldValue !== value)
        {
            _internal_accflow_group_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_group_name", oldValue, _internal_accflow_group_name));
        }
    }

    public function set accflow_group_color(value:int) : void
    {
        var oldValue:int = _internal_accflow_group_color;
        if (oldValue !== value)
        {
            _internal_accflow_group_color = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_group_color", oldValue, _internal_accflow_group_color));
        }
    }

    public function set accflow_group_gridsequence(value:String) : void
    {
        var oldValue:String = _internal_accflow_group_gridsequence;
        if (oldValue !== value)
        {
            _internal_accflow_group_gridsequence = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accflow_group_gridsequence", oldValue, _internal_accflow_group_gridsequence));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefExpensesModule_TreatmentProcEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefExpensesModule_TreatmentProcEntityMetadata) : void
    {
        var oldValue : _ChiefExpensesModule_TreatmentProcEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
