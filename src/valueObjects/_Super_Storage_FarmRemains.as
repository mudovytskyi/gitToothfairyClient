/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Storage_FarmRemains.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Storage_FarmRemains extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("Storage_FarmRemains") == null)
            {
                flash.net.registerClassAlias("Storage_FarmRemains", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("Storage_FarmRemains", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _Storage_FarmRemainsEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_optimaluantity : Number;
    private var _internal_farm_id : String;
    private var _internal_farm_name : String;
    private var _internal_farm_unitmeasure : String;
    private var _internal_quantity : Number;
    private var _internal_price : Number;
    private var _internal_subdivision_fk : String;
    private var _internal_subdivision_name : String;
    private var _internal_minimumquantity : Number;
    private var _internal_optimalquantity : Number;
    private var _internal_storageLabel : String;
    private var _internal_storageColor : int;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Storage_FarmRemains()
    {
        _model = new _Storage_FarmRemainsEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get optimaluantity() : Number
    {
        return _internal_optimaluantity;
    }

    [Bindable(event="propertyChange")]
    public function get farm_id() : String
    {
        return _internal_farm_id;
    }

    [Bindable(event="propertyChange")]
    public function get farm_name() : String
    {
        return _internal_farm_name;
    }

    [Bindable(event="propertyChange")]
    public function get farm_unitmeasure() : String
    {
        return _internal_farm_unitmeasure;
    }

    [Bindable(event="propertyChange")]
    public function get quantity() : Number
    {
        return _internal_quantity;
    }

    [Bindable(event="propertyChange")]
    public function get price() : Number
    {
        return _internal_price;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_fk() : String
    {
        return _internal_subdivision_fk;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_name() : String
    {
        return _internal_subdivision_name;
    }

    [Bindable(event="propertyChange")]
    public function get minimumquantity() : Number
    {
        return _internal_minimumquantity;
    }

    [Bindable(event="propertyChange")]
    public function get optimalquantity() : Number
    {
        return _internal_optimalquantity;
    }

    [Bindable(event="propertyChange")]
    public function get storageLabel() : String
    {
        return _internal_storageLabel;
    }

    [Bindable(event="propertyChange")]
    public function get storageColor() : int
    {
        return _internal_storageColor;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set optimaluantity(value:Number) : void
    {
        var oldValue:Number = _internal_optimaluantity;
        if (isNaN(_internal_optimaluantity) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_optimaluantity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "optimaluantity", oldValue, _internal_optimaluantity));
        }
    }

    public function set farm_id(value:String) : void
    {
        var oldValue:String = _internal_farm_id;
        if (oldValue !== value)
        {
            _internal_farm_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_id", oldValue, _internal_farm_id));
        }
    }

    public function set farm_name(value:String) : void
    {
        var oldValue:String = _internal_farm_name;
        if (oldValue !== value)
        {
            _internal_farm_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_name", oldValue, _internal_farm_name));
        }
    }

    public function set farm_unitmeasure(value:String) : void
    {
        var oldValue:String = _internal_farm_unitmeasure;
        if (oldValue !== value)
        {
            _internal_farm_unitmeasure = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_unitmeasure", oldValue, _internal_farm_unitmeasure));
        }
    }

    public function set quantity(value:Number) : void
    {
        var oldValue:Number = _internal_quantity;
        if (isNaN(_internal_quantity) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_quantity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "quantity", oldValue, _internal_quantity));
        }
    }

    public function set price(value:Number) : void
    {
        var oldValue:Number = _internal_price;
        if (isNaN(_internal_price) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_price = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "price", oldValue, _internal_price));
        }
    }

    public function set subdivision_fk(value:String) : void
    {
        var oldValue:String = _internal_subdivision_fk;
        if (oldValue !== value)
        {
            _internal_subdivision_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_fk", oldValue, _internal_subdivision_fk));
        }
    }

    public function set subdivision_name(value:String) : void
    {
        var oldValue:String = _internal_subdivision_name;
        if (oldValue !== value)
        {
            _internal_subdivision_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_name", oldValue, _internal_subdivision_name));
        }
    }

    public function set minimumquantity(value:Number) : void
    {
        var oldValue:Number = _internal_minimumquantity;
        if (isNaN(_internal_minimumquantity) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_minimumquantity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "minimumquantity", oldValue, _internal_minimumquantity));
        }
    }

    public function set optimalquantity(value:Number) : void
    {
        var oldValue:Number = _internal_optimalquantity;
        if (isNaN(_internal_optimalquantity) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_optimalquantity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "optimalquantity", oldValue, _internal_optimalquantity));
        }
    }

    public function set storageLabel(value:String) : void
    {
        var oldValue:String = _internal_storageLabel;
        if (oldValue !== value)
        {
            _internal_storageLabel = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storageLabel", oldValue, _internal_storageLabel));
        }
    }

    public function set storageColor(value:int) : void
    {
        var oldValue:int = _internal_storageColor;
        if (oldValue !== value)
        {
            _internal_storageColor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storageColor", oldValue, _internal_storageColor));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _Storage_FarmRemainsEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _Storage_FarmRemainsEntityMetadata) : void
    {
        var oldValue : _Storage_FarmRemainsEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
