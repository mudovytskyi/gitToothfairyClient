/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HistoryModuleIllHistory.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HistoryModuleIllHistory extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HistoryModuleIllHistory") == null)
            {
                flash.net.registerClassAlias("HistoryModuleIllHistory", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HistoryModuleIllHistory", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _HistoryModuleIllHistoryEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_PATIENTID : String;
    private var _internal_DATEOFRECORD : String;
    private var _internal_EXAMDATE : String;
    private var _internal_RECORDTYPE : String;
    private var _internal_HISTORYTEXT : String;
    private var _internal_DOCTORID : String;
    private var _internal_NN : int;
    private var _internal_EXAMID : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HistoryModuleIllHistory()
    {
        _model = new _HistoryModuleIllHistoryEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get PATIENTID() : String
    {
        return _internal_PATIENTID;
    }

    [Bindable(event="propertyChange")]
    public function get DATEOFRECORD() : String
    {
        return _internal_DATEOFRECORD;
    }

    [Bindable(event="propertyChange")]
    public function get EXAMDATE() : String
    {
        return _internal_EXAMDATE;
    }

    [Bindable(event="propertyChange")]
    public function get RECORDTYPE() : String
    {
        return _internal_RECORDTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get HISTORYTEXT() : String
    {
        return _internal_HISTORYTEXT;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTORID() : String
    {
        return _internal_DOCTORID;
    }

    [Bindable(event="propertyChange")]
    public function get NN() : int
    {
        return _internal_NN;
    }

    [Bindable(event="propertyChange")]
    public function get EXAMID() : String
    {
        return _internal_EXAMID;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set PATIENTID(value:String) : void
    {
        var oldValue:String = _internal_PATIENTID;
        if (oldValue !== value)
        {
            _internal_PATIENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTID", oldValue, _internal_PATIENTID));
        }
    }

    public function set DATEOFRECORD(value:String) : void
    {
        var oldValue:String = _internal_DATEOFRECORD;
        if (oldValue !== value)
        {
            _internal_DATEOFRECORD = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATEOFRECORD", oldValue, _internal_DATEOFRECORD));
        }
    }

    public function set EXAMDATE(value:String) : void
    {
        var oldValue:String = _internal_EXAMDATE;
        if (oldValue !== value)
        {
            _internal_EXAMDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXAMDATE", oldValue, _internal_EXAMDATE));
        }
    }

    public function set RECORDTYPE(value:String) : void
    {
        var oldValue:String = _internal_RECORDTYPE;
        if (oldValue !== value)
        {
            _internal_RECORDTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RECORDTYPE", oldValue, _internal_RECORDTYPE));
        }
    }

    public function set HISTORYTEXT(value:String) : void
    {
        var oldValue:String = _internal_HISTORYTEXT;
        if (oldValue !== value)
        {
            _internal_HISTORYTEXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HISTORYTEXT", oldValue, _internal_HISTORYTEXT));
        }
    }

    public function set DOCTORID(value:String) : void
    {
        var oldValue:String = _internal_DOCTORID;
        if (oldValue !== value)
        {
            _internal_DOCTORID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTORID", oldValue, _internal_DOCTORID));
        }
    }

    public function set NN(value:int) : void
    {
        var oldValue:int = _internal_NN;
        if (oldValue !== value)
        {
            _internal_NN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NN", oldValue, _internal_NN));
        }
    }

    public function set EXAMID(value:String) : void
    {
        var oldValue:String = _internal_EXAMID;
        if (oldValue !== value)
        {
            _internal_EXAMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXAMID", oldValue, _internal_EXAMID));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HistoryModuleIllHistoryEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HistoryModuleIllHistoryEntityMetadata) : void
    {
        var oldValue : _HistoryModuleIllHistoryEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
