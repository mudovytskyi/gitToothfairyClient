/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AccountModuleProcedureObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AccountModuleProcedureObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AccountModuleProcedureObject") == null)
            {
                flash.net.registerClassAlias("AccountModuleProcedureObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AccountModuleProcedureObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AccountModuleProcedureObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_proc_id : String;
    private var _internal_proc_nameforplan : String;
    private var _internal_proc_price : String;
    private var _internal_proc_count : Number;
    private var _internal_dateclose : String;
    private var _internal_doctor_fname : String;
    private var _internal_doctor_lname : String;
    private var _internal_doctor_pname : String;
    private var _internal_discount_amount : Number;
    private var _internal_discount_flag : int;
    private var _internal_proc_shifr : String;
    private var _internal_healthproc_fk : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AccountModuleProcedureObject()
    {
        _model = new _AccountModuleProcedureObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get proc_id() : String
    {
        return _internal_proc_id;
    }

    [Bindable(event="propertyChange")]
    public function get proc_nameforplan() : String
    {
        return _internal_proc_nameforplan;
    }

    [Bindable(event="propertyChange")]
    public function get proc_price() : String
    {
        return _internal_proc_price;
    }

    [Bindable(event="propertyChange")]
    public function get proc_count() : Number
    {
        return _internal_proc_count;
    }

    [Bindable(event="propertyChange")]
    public function get dateclose() : String
    {
        return _internal_dateclose;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_fname() : String
    {
        return _internal_doctor_fname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lname() : String
    {
        return _internal_doctor_lname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_pname() : String
    {
        return _internal_doctor_pname;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount() : Number
    {
        return _internal_discount_amount;
    }

    [Bindable(event="propertyChange")]
    public function get discount_flag() : int
    {
        return _internal_discount_flag;
    }

    [Bindable(event="propertyChange")]
    public function get proc_shifr() : String
    {
        return _internal_proc_shifr;
    }

    [Bindable(event="propertyChange")]
    public function get healthproc_fk() : String
    {
        return _internal_healthproc_fk;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set proc_id(value:String) : void
    {
        var oldValue:String = _internal_proc_id;
        if (oldValue !== value)
        {
            _internal_proc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_id", oldValue, _internal_proc_id));
        }
    }

    public function set proc_nameforplan(value:String) : void
    {
        var oldValue:String = _internal_proc_nameforplan;
        if (oldValue !== value)
        {
            _internal_proc_nameforplan = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_nameforplan", oldValue, _internal_proc_nameforplan));
        }
    }

    public function set proc_price(value:String) : void
    {
        var oldValue:String = _internal_proc_price;
        if (oldValue !== value)
        {
            _internal_proc_price = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_price", oldValue, _internal_proc_price));
        }
    }

    public function set proc_count(value:Number) : void
    {
        var oldValue:Number = _internal_proc_count;
        if (isNaN(_internal_proc_count) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_proc_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_count", oldValue, _internal_proc_count));
        }
    }

    public function set dateclose(value:String) : void
    {
        var oldValue:String = _internal_dateclose;
        if (oldValue !== value)
        {
            _internal_dateclose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dateclose", oldValue, _internal_dateclose));
        }
    }

    public function set doctor_fname(value:String) : void
    {
        var oldValue:String = _internal_doctor_fname;
        if (oldValue !== value)
        {
            _internal_doctor_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_fname", oldValue, _internal_doctor_fname));
        }
    }

    public function set doctor_lname(value:String) : void
    {
        var oldValue:String = _internal_doctor_lname;
        if (oldValue !== value)
        {
            _internal_doctor_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lname", oldValue, _internal_doctor_lname));
        }
    }

    public function set doctor_pname(value:String) : void
    {
        var oldValue:String = _internal_doctor_pname;
        if (oldValue !== value)
        {
            _internal_doctor_pname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_pname", oldValue, _internal_doctor_pname));
        }
    }

    public function set discount_amount(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount;
        if (isNaN(_internal_discount_amount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount", oldValue, _internal_discount_amount));
        }
    }

    public function set discount_flag(value:int) : void
    {
        var oldValue:int = _internal_discount_flag;
        if (oldValue !== value)
        {
            _internal_discount_flag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_flag", oldValue, _internal_discount_flag));
        }
    }

    public function set proc_shifr(value:String) : void
    {
        var oldValue:String = _internal_proc_shifr;
        if (oldValue !== value)
        {
            _internal_proc_shifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "proc_shifr", oldValue, _internal_proc_shifr));
        }
    }

    public function set healthproc_fk(value:String) : void
    {
        var oldValue:String = _internal_healthproc_fk;
        if (oldValue !== value)
        {
            _internal_healthproc_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "healthproc_fk", oldValue, _internal_healthproc_fk));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AccountModuleProcedureObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AccountModuleProcedureObjectEntityMetadata) : void
    {
        var oldValue : _AccountModuleProcedureObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
