
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _SMSSenderSettingEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("SMSModuleMode", "SMSModuleGateIP", "SMSModuleGatePort", "SMSModuleGateAdditional", "SMSLogin", "SMSPassword", "SMSEnable", "SMSEnableOnTask", "SMSEnableOnDOB", "SMSSendDays", "SMSStartTime", "SMSEndTime", "SMSSendBeforeInDays", "SMSSendBeforeInHours", "SMSTaskTemplate", "SMSDispanserTaskTemplate", "SMSDOBTemplate", "SMSSenderName", "SMSQueueCapacity", "SMSTranslit", "SMSModuleUseGinMobile", "SMSConflictIgnore");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("SMSModuleMode", "SMSModuleGateIP", "SMSModuleGatePort", "SMSModuleGateAdditional", "SMSLogin", "SMSPassword", "SMSEnable", "SMSEnableOnTask", "SMSEnableOnDOB", "SMSSendDays", "SMSStartTime", "SMSEndTime", "SMSSendBeforeInDays", "SMSSendBeforeInHours", "SMSTaskTemplate", "SMSDispanserTaskTemplate", "SMSDOBTemplate", "SMSSenderName", "SMSQueueCapacity", "SMSTranslit", "SMSModuleUseGinMobile", "SMSConflictIgnore");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("SMSModuleMode", "SMSModuleGateIP", "SMSModuleGatePort", "SMSModuleGateAdditional", "SMSLogin", "SMSPassword", "SMSEnable", "SMSEnableOnTask", "SMSEnableOnDOB", "SMSSendDays", "SMSStartTime", "SMSEndTime", "SMSSendBeforeInDays", "SMSSendBeforeInHours", "SMSTaskTemplate", "SMSDispanserTaskTemplate", "SMSDOBTemplate", "SMSSenderName", "SMSQueueCapacity", "SMSTranslit", "SMSModuleUseGinMobile", "SMSConflictIgnore");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("SMSModuleMode", "SMSModuleGateIP", "SMSModuleGatePort", "SMSModuleGateAdditional", "SMSLogin", "SMSPassword", "SMSEnable", "SMSEnableOnTask", "SMSEnableOnDOB", "SMSSendDays", "SMSStartTime", "SMSEndTime", "SMSSendBeforeInDays", "SMSSendBeforeInHours", "SMSTaskTemplate", "SMSDispanserTaskTemplate", "SMSDOBTemplate", "SMSSenderName", "SMSQueueCapacity", "SMSTranslit", "SMSModuleUseGinMobile", "SMSConflictIgnore");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "SMSSenderSetting";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_SMSSenderSetting;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _SMSSenderSettingEntityMetadata(value : _Super_SMSSenderSetting)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["SMSModuleMode"] = new Array();
            model_internal::dependentsOnMap["SMSModuleGateIP"] = new Array();
            model_internal::dependentsOnMap["SMSModuleGatePort"] = new Array();
            model_internal::dependentsOnMap["SMSModuleGateAdditional"] = new Array();
            model_internal::dependentsOnMap["SMSLogin"] = new Array();
            model_internal::dependentsOnMap["SMSPassword"] = new Array();
            model_internal::dependentsOnMap["SMSEnable"] = new Array();
            model_internal::dependentsOnMap["SMSEnableOnTask"] = new Array();
            model_internal::dependentsOnMap["SMSEnableOnDOB"] = new Array();
            model_internal::dependentsOnMap["SMSSendDays"] = new Array();
            model_internal::dependentsOnMap["SMSStartTime"] = new Array();
            model_internal::dependentsOnMap["SMSEndTime"] = new Array();
            model_internal::dependentsOnMap["SMSSendBeforeInDays"] = new Array();
            model_internal::dependentsOnMap["SMSSendBeforeInHours"] = new Array();
            model_internal::dependentsOnMap["SMSTaskTemplate"] = new Array();
            model_internal::dependentsOnMap["SMSDispanserTaskTemplate"] = new Array();
            model_internal::dependentsOnMap["SMSDOBTemplate"] = new Array();
            model_internal::dependentsOnMap["SMSSenderName"] = new Array();
            model_internal::dependentsOnMap["SMSQueueCapacity"] = new Array();
            model_internal::dependentsOnMap["SMSTranslit"] = new Array();
            model_internal::dependentsOnMap["SMSModuleUseGinMobile"] = new Array();
            model_internal::dependentsOnMap["SMSConflictIgnore"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["SMSModuleMode"] = "int";
        model_internal::propertyTypeMap["SMSModuleGateIP"] = "String";
        model_internal::propertyTypeMap["SMSModuleGatePort"] = "String";
        model_internal::propertyTypeMap["SMSModuleGateAdditional"] = "String";
        model_internal::propertyTypeMap["SMSLogin"] = "String";
        model_internal::propertyTypeMap["SMSPassword"] = "String";
        model_internal::propertyTypeMap["SMSEnable"] = "Boolean";
        model_internal::propertyTypeMap["SMSEnableOnTask"] = "Boolean";
        model_internal::propertyTypeMap["SMSEnableOnDOB"] = "Boolean";
        model_internal::propertyTypeMap["SMSSendDays"] = "String";
        model_internal::propertyTypeMap["SMSStartTime"] = "String";
        model_internal::propertyTypeMap["SMSEndTime"] = "String";
        model_internal::propertyTypeMap["SMSSendBeforeInDays"] = "int";
        model_internal::propertyTypeMap["SMSSendBeforeInHours"] = "int";
        model_internal::propertyTypeMap["SMSTaskTemplate"] = "String";
        model_internal::propertyTypeMap["SMSDispanserTaskTemplate"] = "String";
        model_internal::propertyTypeMap["SMSDOBTemplate"] = "String";
        model_internal::propertyTypeMap["SMSSenderName"] = "String";
        model_internal::propertyTypeMap["SMSQueueCapacity"] = "int";
        model_internal::propertyTypeMap["SMSTranslit"] = "Boolean";
        model_internal::propertyTypeMap["SMSModuleUseGinMobile"] = "Boolean";
        model_internal::propertyTypeMap["SMSConflictIgnore"] = "Boolean";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity SMSSenderSetting");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity SMSSenderSetting");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of SMSSenderSetting");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity SMSSenderSetting");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity SMSSenderSetting");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity SMSSenderSetting");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isSMSModuleModeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSModuleGateIPAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSModuleGatePortAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSModuleGateAdditionalAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSLoginAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSPasswordAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSEnableAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSEnableOnTaskAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSEnableOnDOBAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSSendDaysAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSStartTimeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSEndTimeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSSendBeforeInDaysAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSSendBeforeInHoursAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSTaskTemplateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSDispanserTaskTemplateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSDOBTemplateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSSenderNameAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSQueueCapacityAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSTranslitAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSModuleUseGinMobileAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSConflictIgnoreAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get SMSModuleModeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSModuleGateIPStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSModuleGatePortStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSModuleGateAdditionalStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSLoginStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSPasswordStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSEnableStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSEnableOnTaskStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSEnableOnDOBStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSSendDaysStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSStartTimeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSEndTimeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSSendBeforeInDaysStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSSendBeforeInHoursStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSTaskTemplateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSDispanserTaskTemplateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSDOBTemplateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSSenderNameStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSQueueCapacityStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSTranslitStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSModuleUseGinMobileStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSConflictIgnoreStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
