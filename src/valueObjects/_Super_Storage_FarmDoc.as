/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Storage_FarmDoc.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.Storage_FarmBath;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Storage_FarmDoc extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("Storage_FarmDoc") == null)
            {
                flash.net.registerClassAlias("Storage_FarmDoc", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("Storage_FarmDoc", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.Storage_FarmBath.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _Storage_FarmDocEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_doctype : int;
    private var _internal_supplier_fk : String;
    private var _internal_supplier_name : String;
    private var _internal_createtimestamp : String;
    private var _internal_docdate : String;
    private var _internal_summ : Number;
    private var _internal_docnumber : String;
    private var _internal_accountflow_fk : String;
    private var _internal_subdivision_fk : String;
    private var _internal_subdivision_name : String;
    private var _internal_author_fk : String;
    private var _internal_author_name : String;
    private var _internal_note : String;
    private var _internal_farmstorage_fk : String;
    private var _internal_farmstorage_name : String;
    private var _internal_doctor_fk : String;
    private var _internal_patient_fk : String;
    private var _internal_patient_name : String;
    private var _internal_room_fk : String;
    private var _internal_farmbaths : ArrayCollection;
    model_internal var _internal_farmbaths_leaf:valueObjects.Storage_FarmBath;
    private var _internal_orderNumber : String;
    private var _internal_orderIsCashpayment : int;
    private var _internal_orderOperationtimestamp : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Storage_FarmDoc()
    {
        _model = new _Storage_FarmDocEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctype() : int
    {
        return _internal_doctype;
    }

    [Bindable(event="propertyChange")]
    public function get supplier_fk() : String
    {
        return _internal_supplier_fk;
    }

    [Bindable(event="propertyChange")]
    public function get supplier_name() : String
    {
        return _internal_supplier_name;
    }

    [Bindable(event="propertyChange")]
    public function get createtimestamp() : String
    {
        return _internal_createtimestamp;
    }

    [Bindable(event="propertyChange")]
    public function get docdate() : String
    {
        return _internal_docdate;
    }

    [Bindable(event="propertyChange")]
    public function get summ() : Number
    {
        return _internal_summ;
    }

    [Bindable(event="propertyChange")]
    public function get docnumber() : String
    {
        return _internal_docnumber;
    }

    [Bindable(event="propertyChange")]
    public function get accountflow_fk() : String
    {
        return _internal_accountflow_fk;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_fk() : String
    {
        return _internal_subdivision_fk;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_name() : String
    {
        return _internal_subdivision_name;
    }

    [Bindable(event="propertyChange")]
    public function get author_fk() : String
    {
        return _internal_author_fk;
    }

    [Bindable(event="propertyChange")]
    public function get author_name() : String
    {
        return _internal_author_name;
    }

    [Bindable(event="propertyChange")]
    public function get note() : String
    {
        return _internal_note;
    }

    [Bindable(event="propertyChange")]
    public function get farmstorage_fk() : String
    {
        return _internal_farmstorage_fk;
    }

    [Bindable(event="propertyChange")]
    public function get farmstorage_name() : String
    {
        return _internal_farmstorage_name;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_fk() : String
    {
        return _internal_doctor_fk;
    }

    [Bindable(event="propertyChange")]
    public function get patient_fk() : String
    {
        return _internal_patient_fk;
    }

    [Bindable(event="propertyChange")]
    public function get patient_name() : String
    {
        return _internal_patient_name;
    }

    [Bindable(event="propertyChange")]
    public function get room_fk() : String
    {
        return _internal_room_fk;
    }

    [Bindable(event="propertyChange")]
    public function get farmbaths() : ArrayCollection
    {
        return _internal_farmbaths;
    }

    [Bindable(event="propertyChange")]
    public function get orderNumber() : String
    {
        return _internal_orderNumber;
    }

    [Bindable(event="propertyChange")]
    public function get orderIsCashpayment() : int
    {
        return _internal_orderIsCashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get orderOperationtimestamp() : String
    {
        return _internal_orderOperationtimestamp;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set doctype(value:int) : void
    {
        var oldValue:int = _internal_doctype;
        if (oldValue !== value)
        {
            _internal_doctype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctype", oldValue, _internal_doctype));
        }
    }

    public function set supplier_fk(value:String) : void
    {
        var oldValue:String = _internal_supplier_fk;
        if (oldValue !== value)
        {
            _internal_supplier_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "supplier_fk", oldValue, _internal_supplier_fk));
        }
    }

    public function set supplier_name(value:String) : void
    {
        var oldValue:String = _internal_supplier_name;
        if (oldValue !== value)
        {
            _internal_supplier_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "supplier_name", oldValue, _internal_supplier_name));
        }
    }

    public function set createtimestamp(value:String) : void
    {
        var oldValue:String = _internal_createtimestamp;
        if (oldValue !== value)
        {
            _internal_createtimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "createtimestamp", oldValue, _internal_createtimestamp));
        }
    }

    public function set docdate(value:String) : void
    {
        var oldValue:String = _internal_docdate;
        if (oldValue !== value)
        {
            _internal_docdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "docdate", oldValue, _internal_docdate));
        }
    }

    public function set summ(value:Number) : void
    {
        var oldValue:Number = _internal_summ;
        if (isNaN(_internal_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "summ", oldValue, _internal_summ));
        }
    }

    public function set docnumber(value:String) : void
    {
        var oldValue:String = _internal_docnumber;
        if (oldValue !== value)
        {
            _internal_docnumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "docnumber", oldValue, _internal_docnumber));
        }
    }

    public function set accountflow_fk(value:String) : void
    {
        var oldValue:String = _internal_accountflow_fk;
        if (oldValue !== value)
        {
            _internal_accountflow_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "accountflow_fk", oldValue, _internal_accountflow_fk));
        }
    }

    public function set subdivision_fk(value:String) : void
    {
        var oldValue:String = _internal_subdivision_fk;
        if (oldValue !== value)
        {
            _internal_subdivision_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_fk", oldValue, _internal_subdivision_fk));
        }
    }

    public function set subdivision_name(value:String) : void
    {
        var oldValue:String = _internal_subdivision_name;
        if (oldValue !== value)
        {
            _internal_subdivision_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_name", oldValue, _internal_subdivision_name));
        }
    }

    public function set author_fk(value:String) : void
    {
        var oldValue:String = _internal_author_fk;
        if (oldValue !== value)
        {
            _internal_author_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_fk", oldValue, _internal_author_fk));
        }
    }

    public function set author_name(value:String) : void
    {
        var oldValue:String = _internal_author_name;
        if (oldValue !== value)
        {
            _internal_author_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_name", oldValue, _internal_author_name));
        }
    }

    public function set note(value:String) : void
    {
        var oldValue:String = _internal_note;
        if (oldValue !== value)
        {
            _internal_note = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "note", oldValue, _internal_note));
        }
    }

    public function set farmstorage_fk(value:String) : void
    {
        var oldValue:String = _internal_farmstorage_fk;
        if (oldValue !== value)
        {
            _internal_farmstorage_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmstorage_fk", oldValue, _internal_farmstorage_fk));
        }
    }

    public function set farmstorage_name(value:String) : void
    {
        var oldValue:String = _internal_farmstorage_name;
        if (oldValue !== value)
        {
            _internal_farmstorage_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmstorage_name", oldValue, _internal_farmstorage_name));
        }
    }

    public function set doctor_fk(value:String) : void
    {
        var oldValue:String = _internal_doctor_fk;
        if (oldValue !== value)
        {
            _internal_doctor_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_fk", oldValue, _internal_doctor_fk));
        }
    }

    public function set patient_fk(value:String) : void
    {
        var oldValue:String = _internal_patient_fk;
        if (oldValue !== value)
        {
            _internal_patient_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_fk", oldValue, _internal_patient_fk));
        }
    }

    public function set patient_name(value:String) : void
    {
        var oldValue:String = _internal_patient_name;
        if (oldValue !== value)
        {
            _internal_patient_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_name", oldValue, _internal_patient_name));
        }
    }

    public function set room_fk(value:String) : void
    {
        var oldValue:String = _internal_room_fk;
        if (oldValue !== value)
        {
            _internal_room_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "room_fk", oldValue, _internal_room_fk));
        }
    }

    public function set farmbaths(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_farmbaths;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_farmbaths = value;
            }
            else if (value is Array)
            {
                _internal_farmbaths = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_farmbaths = null;
            }
            else
            {
                throw new Error("value of farmbaths must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmbaths", oldValue, _internal_farmbaths));
        }
    }

    public function set orderNumber(value:String) : void
    {
        var oldValue:String = _internal_orderNumber;
        if (oldValue !== value)
        {
            _internal_orderNumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "orderNumber", oldValue, _internal_orderNumber));
        }
    }

    public function set orderIsCashpayment(value:int) : void
    {
        var oldValue:int = _internal_orderIsCashpayment;
        if (oldValue !== value)
        {
            _internal_orderIsCashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "orderIsCashpayment", oldValue, _internal_orderIsCashpayment));
        }
    }

    public function set orderOperationtimestamp(value:String) : void
    {
        var oldValue:String = _internal_orderOperationtimestamp;
        if (oldValue !== value)
        {
            _internal_orderOperationtimestamp = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "orderOperationtimestamp", oldValue, _internal_orderOperationtimestamp));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _Storage_FarmDocEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _Storage_FarmDocEntityMetadata) : void
    {
        var oldValue : _Storage_FarmDocEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
