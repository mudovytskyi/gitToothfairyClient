/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CalendarModuleIntervalItem.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CalendarModuleIntervalItem extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CalendarModuleIntervalItem") == null)
            {
                flash.net.registerClassAlias("CalendarModuleIntervalItem", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CalendarModuleIntervalItem", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _CalendarModuleIntervalItemEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ISENABLED : Object;
    private var _internal_DOCTORID : ArrayCollection;
    private var _internal_STARTDATE : Object;
    private var _internal_ENDDATE : Object;
    private var _internal_MINDATE : ArrayCollection;
    private var _internal_MAXDATE : ArrayCollection;
    private var _internal_ROOMID : String;
    private var _internal_CURDOC : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CalendarModuleIntervalItem()
    {
        _model = new _CalendarModuleIntervalItemEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ISENABLED() : Object
    {
        return _internal_ISENABLED;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTORID() : ArrayCollection
    {
        return _internal_DOCTORID;
    }

    [Bindable(event="propertyChange")]
    public function get STARTDATE() : Object
    {
        return _internal_STARTDATE;
    }

    [Bindable(event="propertyChange")]
    public function get ENDDATE() : Object
    {
        return _internal_ENDDATE;
    }

    [Bindable(event="propertyChange")]
    public function get MINDATE() : ArrayCollection
    {
        return _internal_MINDATE;
    }

    [Bindable(event="propertyChange")]
    public function get MAXDATE() : ArrayCollection
    {
        return _internal_MAXDATE;
    }

    [Bindable(event="propertyChange")]
    public function get ROOMID() : String
    {
        return _internal_ROOMID;
    }

    [Bindable(event="propertyChange")]
    public function get CURDOC() : int
    {
        return _internal_CURDOC;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ISENABLED(value:Object) : void
    {
        var oldValue:Object = _internal_ISENABLED;
        if (oldValue !== value)
        {
            _internal_ISENABLED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ISENABLED", oldValue, _internal_ISENABLED));
        }
    }

    public function set DOCTORID(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_DOCTORID;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_DOCTORID = value;
            }
            else if (value is Array)
            {
                _internal_DOCTORID = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_DOCTORID = null;
            }
            else
            {
                throw new Error("value of DOCTORID must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTORID", oldValue, _internal_DOCTORID));
        }
    }

    public function set STARTDATE(value:Object) : void
    {
        var oldValue:Object = _internal_STARTDATE;
        if (oldValue !== value)
        {
            _internal_STARTDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STARTDATE", oldValue, _internal_STARTDATE));
        }
    }

    public function set ENDDATE(value:Object) : void
    {
        var oldValue:Object = _internal_ENDDATE;
        if (oldValue !== value)
        {
            _internal_ENDDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ENDDATE", oldValue, _internal_ENDDATE));
        }
    }

    public function set MINDATE(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_MINDATE;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_MINDATE = value;
            }
            else if (value is Array)
            {
                _internal_MINDATE = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_MINDATE = null;
            }
            else
            {
                throw new Error("value of MINDATE must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MINDATE", oldValue, _internal_MINDATE));
        }
    }

    public function set MAXDATE(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_MAXDATE;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_MAXDATE = value;
            }
            else if (value is Array)
            {
                _internal_MAXDATE = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_MAXDATE = null;
            }
            else
            {
                throw new Error("value of MAXDATE must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MAXDATE", oldValue, _internal_MAXDATE));
        }
    }

    public function set ROOMID(value:String) : void
    {
        var oldValue:String = _internal_ROOMID;
        if (oldValue !== value)
        {
            _internal_ROOMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ROOMID", oldValue, _internal_ROOMID));
        }
    }

    public function set CURDOC(value:int) : void
    {
        var oldValue:int = _internal_CURDOC;
        if (oldValue !== value)
        {
            _internal_CURDOC = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CURDOC", oldValue, _internal_CURDOC));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CalendarModuleIntervalItemEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CalendarModuleIntervalItemEntityMetadata) : void
    {
        var oldValue : _CalendarModuleIntervalItemEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
