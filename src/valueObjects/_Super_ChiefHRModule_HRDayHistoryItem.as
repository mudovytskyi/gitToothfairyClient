/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefHRModule_HRDayHistoryItem.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefHRModule_HRDayHistoryItem extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefHRModule_HRDayHistoryItem") == null)
            {
                flash.net.registerClassAlias("ChiefHRModule_HRDayHistoryItem", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefHRModule_HRDayHistoryItem", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefHRModule_HRDayHistoryItemEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_TIME : String;
    private var _internal_TEXT : String;
    private var _internal_IS_CORRECT : Boolean;
    private var _internal_ASSDOC_ID : int;
    private var _internal_ASSDOC_SNAME : String;
    private var _internal_PATIENT_ID : int;
    private var _internal_PATIENT_SNAME : String;
    private var _internal_IS_DOCTOR : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefHRModule_HRDayHistoryItem()
    {
        _model = new _ChiefHRModule_HRDayHistoryItemEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get TIME() : String
    {
        return _internal_TIME;
    }

    [Bindable(event="propertyChange")]
    public function get TEXT() : String
    {
        return _internal_TEXT;
    }

    [Bindable(event="propertyChange")]
    public function get IS_CORRECT() : Boolean
    {
        return _internal_IS_CORRECT;
    }

    [Bindable(event="propertyChange")]
    public function get ASSDOC_ID() : int
    {
        return _internal_ASSDOC_ID;
    }

    [Bindable(event="propertyChange")]
    public function get ASSDOC_SNAME() : String
    {
        return _internal_ASSDOC_SNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_ID() : int
    {
        return _internal_PATIENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_SNAME() : String
    {
        return _internal_PATIENT_SNAME;
    }

    [Bindable(event="propertyChange")]
    public function get IS_DOCTOR() : Boolean
    {
        return _internal_IS_DOCTOR;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set TIME(value:String) : void
    {
        var oldValue:String = _internal_TIME;
        if (oldValue !== value)
        {
            _internal_TIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TIME", oldValue, _internal_TIME));
        }
    }

    public function set TEXT(value:String) : void
    {
        var oldValue:String = _internal_TEXT;
        if (oldValue !== value)
        {
            _internal_TEXT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TEXT", oldValue, _internal_TEXT));
        }
    }

    public function set IS_CORRECT(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_IS_CORRECT;
        if (oldValue !== value)
        {
            _internal_IS_CORRECT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "IS_CORRECT", oldValue, _internal_IS_CORRECT));
        }
    }

    public function set ASSDOC_ID(value:int) : void
    {
        var oldValue:int = _internal_ASSDOC_ID;
        if (oldValue !== value)
        {
            _internal_ASSDOC_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ASSDOC_ID", oldValue, _internal_ASSDOC_ID));
        }
    }

    public function set ASSDOC_SNAME(value:String) : void
    {
        var oldValue:String = _internal_ASSDOC_SNAME;
        if (oldValue !== value)
        {
            _internal_ASSDOC_SNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ASSDOC_SNAME", oldValue, _internal_ASSDOC_SNAME));
        }
    }

    public function set PATIENT_ID(value:int) : void
    {
        var oldValue:int = _internal_PATIENT_ID;
        if (oldValue !== value)
        {
            _internal_PATIENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_ID", oldValue, _internal_PATIENT_ID));
        }
    }

    public function set PATIENT_SNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_SNAME;
        if (oldValue !== value)
        {
            _internal_PATIENT_SNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_SNAME", oldValue, _internal_PATIENT_SNAME));
        }
    }

    public function set IS_DOCTOR(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_IS_DOCTOR;
        if (oldValue !== value)
        {
            _internal_IS_DOCTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "IS_DOCTOR", oldValue, _internal_IS_DOCTOR));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefHRModule_HRDayHistoryItemEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefHRModule_HRDayHistoryItemEntityMetadata) : void
    {
        var oldValue : _ChiefHRModule_HRDayHistoryItemEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
