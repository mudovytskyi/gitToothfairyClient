/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AdminModuleAutorizeObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AdminModuleAutorizeObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AdminModuleAutorizeObject") == null)
            {
                flash.net.registerClassAlias("AdminModuleAutorizeObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AdminModuleAutorizeObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AdminModuleAutorizeObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_EH_IS_ACCESS_TOKEN_EXPIRY : Boolean;
    private var _internal_ACCOUNT_ID : String;
    private var _internal_ACCOUNT_ACCESS : Boolean;
    private var _internal_ACCOUNT_USER : String;
    private var _internal_ACCOUNT_PASSWORD : String;
    private var _internal_ACCOUNT_KEY : String;
    private var _internal_ACCOUNT_CHECK : int;
    private var _internal_ACCOUNT_LASTNAME : String;
    private var _internal_ACCOUNT_FIRSTNAME : String;
    private var _internal_ACCOUNT_MIDDLENAME : String;
    private var _internal_ACCOUNT_SHORTNAME : String;
    private var _internal_DOCTOR_ID : String;
    private var _internal_DOCTOR_SHORTNAME : String;
    private var _internal_DOCTOR_SPECIALITY : String;
    private var _internal_SPEC : int;
    private var _internal_SUBDIVISIONID : String;
    private var _internal_SUBDIVISIONNAME : String;
    private var _internal_SUBDIVISIONCITY : String;
    private var _internal_LOGO : String;
    private var _internal_ACCOUNT_CASHIERCODE : String;
    private var _internal_INFO2 : String;
    private var _internal_ACCOUNTTYPE : String;
    private var _internal_TAX_ID : String;
    private var _internal_BIRTH_DATE : String;
    private var _internal_BIRTH_PLACE : String;
    private var _internal_GENDER : int;
    private var _internal_EMAIL : String;
    private var _internal_USER_POSITION : String;
    private var _internal_PHONE_TYPE : String;
    private var _internal_PHONE : String;
    private var _internal_DOC_TYPE : String;
    private var _internal_DOC_NUMBER : String;
    private var _internal_IS_OWNER : Boolean;
    private var _internal_EH_ACCESS_TOKEN : String;
    private var _internal_EH_ACCESS_TOKEN_EXPIRY : String;
    private var _internal_EH_SECRET_KEY : String;
    private var _internal_OA_DIR : String;
    private var _internal_EH_LEGALENTITY_ID : String;
    private var _internal_SCOPE : String;
    private var _internal_EH_STARTDATE : String;
    private var _internal_EH_ENDDATE : String;
    private var _internal_EH_USER_ID : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AdminModuleAutorizeObject()
    {
        _model = new _AdminModuleAutorizeObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get EH_IS_ACCESS_TOKEN_EXPIRY() : Boolean
    {
        return _internal_EH_IS_ACCESS_TOKEN_EXPIRY;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_ID() : String
    {
        return _internal_ACCOUNT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_ACCESS() : Boolean
    {
        return _internal_ACCOUNT_ACCESS;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_USER() : String
    {
        return _internal_ACCOUNT_USER;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_PASSWORD() : String
    {
        return _internal_ACCOUNT_PASSWORD;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_KEY() : String
    {
        return _internal_ACCOUNT_KEY;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_CHECK() : int
    {
        return _internal_ACCOUNT_CHECK;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_LASTNAME() : String
    {
        return _internal_ACCOUNT_LASTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_FIRSTNAME() : String
    {
        return _internal_ACCOUNT_FIRSTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_MIDDLENAME() : String
    {
        return _internal_ACCOUNT_MIDDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_SHORTNAME() : String
    {
        return _internal_ACCOUNT_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_ID() : String
    {
        return _internal_DOCTOR_ID;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_SHORTNAME() : String
    {
        return _internal_DOCTOR_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_SPECIALITY() : String
    {
        return _internal_DOCTOR_SPECIALITY;
    }

    [Bindable(event="propertyChange")]
    public function get SPEC() : int
    {
        return _internal_SPEC;
    }

    [Bindable(event="propertyChange")]
    public function get SUBDIVISIONID() : String
    {
        return _internal_SUBDIVISIONID;
    }

    [Bindable(event="propertyChange")]
    public function get SUBDIVISIONNAME() : String
    {
        return _internal_SUBDIVISIONNAME;
    }

    [Bindable(event="propertyChange")]
    public function get SUBDIVISIONCITY() : String
    {
        return _internal_SUBDIVISIONCITY;
    }

    [Bindable(event="propertyChange")]
    public function get LOGO() : String
    {
        return _internal_LOGO;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNT_CASHIERCODE() : String
    {
        return _internal_ACCOUNT_CASHIERCODE;
    }

    [Bindable(event="propertyChange")]
    public function get INFO2() : String
    {
        return _internal_INFO2;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNTTYPE() : String
    {
        return _internal_ACCOUNTTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get TAX_ID() : String
    {
        return _internal_TAX_ID;
    }

    [Bindable(event="propertyChange")]
    public function get BIRTH_DATE() : String
    {
        return _internal_BIRTH_DATE;
    }

    [Bindable(event="propertyChange")]
    public function get BIRTH_PLACE() : String
    {
        return _internal_BIRTH_PLACE;
    }

    [Bindable(event="propertyChange")]
    public function get GENDER() : int
    {
        return _internal_GENDER;
    }

    [Bindable(event="propertyChange")]
    public function get EMAIL() : String
    {
        return _internal_EMAIL;
    }

    [Bindable(event="propertyChange")]
    public function get USER_POSITION() : String
    {
        return _internal_USER_POSITION;
    }

    [Bindable(event="propertyChange")]
    public function get PHONE_TYPE() : String
    {
        return _internal_PHONE_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get PHONE() : String
    {
        return _internal_PHONE;
    }

    [Bindable(event="propertyChange")]
    public function get DOC_TYPE() : String
    {
        return _internal_DOC_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get DOC_NUMBER() : String
    {
        return _internal_DOC_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get IS_OWNER() : Boolean
    {
        return _internal_IS_OWNER;
    }

    [Bindable(event="propertyChange")]
    public function get EH_ACCESS_TOKEN() : String
    {
        return _internal_EH_ACCESS_TOKEN;
    }

    [Bindable(event="propertyChange")]
    public function get EH_ACCESS_TOKEN_EXPIRY() : String
    {
        return _internal_EH_ACCESS_TOKEN_EXPIRY;
    }

    [Bindable(event="propertyChange")]
    public function get EH_SECRET_KEY() : String
    {
        return _internal_EH_SECRET_KEY;
    }

    [Bindable(event="propertyChange")]
    public function get OA_DIR() : String
    {
        return _internal_OA_DIR;
    }

    [Bindable(event="propertyChange")]
    public function get EH_LEGALENTITY_ID() : String
    {
        return _internal_EH_LEGALENTITY_ID;
    }

    [Bindable(event="propertyChange")]
    public function get SCOPE() : String
    {
        return _internal_SCOPE;
    }

    [Bindable(event="propertyChange")]
    public function get EH_STARTDATE() : String
    {
        return _internal_EH_STARTDATE;
    }

    [Bindable(event="propertyChange")]
    public function get EH_ENDDATE() : String
    {
        return _internal_EH_ENDDATE;
    }

    [Bindable(event="propertyChange")]
    public function get EH_USER_ID() : String
    {
        return _internal_EH_USER_ID;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set EH_IS_ACCESS_TOKEN_EXPIRY(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_EH_IS_ACCESS_TOKEN_EXPIRY;
        if (oldValue !== value)
        {
            _internal_EH_IS_ACCESS_TOKEN_EXPIRY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_IS_ACCESS_TOKEN_EXPIRY", oldValue, _internal_EH_IS_ACCESS_TOKEN_EXPIRY));
        }
    }

    public function set ACCOUNT_ID(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_ID;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_ID", oldValue, _internal_ACCOUNT_ID));
        }
    }

    public function set ACCOUNT_ACCESS(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_ACCOUNT_ACCESS;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_ACCESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_ACCESS", oldValue, _internal_ACCOUNT_ACCESS));
        }
    }

    public function set ACCOUNT_USER(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_USER;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_USER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_USER", oldValue, _internal_ACCOUNT_USER));
        }
    }

    public function set ACCOUNT_PASSWORD(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_PASSWORD;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_PASSWORD = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_PASSWORD", oldValue, _internal_ACCOUNT_PASSWORD));
        }
    }

    public function set ACCOUNT_KEY(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_KEY;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_KEY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_KEY", oldValue, _internal_ACCOUNT_KEY));
        }
    }

    public function set ACCOUNT_CHECK(value:int) : void
    {
        var oldValue:int = _internal_ACCOUNT_CHECK;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_CHECK = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_CHECK", oldValue, _internal_ACCOUNT_CHECK));
        }
    }

    public function set ACCOUNT_LASTNAME(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_LASTNAME;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_LASTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_LASTNAME", oldValue, _internal_ACCOUNT_LASTNAME));
        }
    }

    public function set ACCOUNT_FIRSTNAME(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_FIRSTNAME;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_FIRSTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_FIRSTNAME", oldValue, _internal_ACCOUNT_FIRSTNAME));
        }
    }

    public function set ACCOUNT_MIDDLENAME(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_MIDDLENAME;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_MIDDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_MIDDLENAME", oldValue, _internal_ACCOUNT_MIDDLENAME));
        }
    }

    public function set ACCOUNT_SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_SHORTNAME", oldValue, _internal_ACCOUNT_SHORTNAME));
        }
    }

    public function set DOCTOR_ID(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_ID;
        if (oldValue !== value)
        {
            _internal_DOCTOR_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_ID", oldValue, _internal_DOCTOR_ID));
        }
    }

    public function set DOCTOR_SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_SHORTNAME", oldValue, _internal_DOCTOR_SHORTNAME));
        }
    }

    public function set DOCTOR_SPECIALITY(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_SPECIALITY;
        if (oldValue !== value)
        {
            _internal_DOCTOR_SPECIALITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_SPECIALITY", oldValue, _internal_DOCTOR_SPECIALITY));
        }
    }

    public function set SPEC(value:int) : void
    {
        var oldValue:int = _internal_SPEC;
        if (oldValue !== value)
        {
            _internal_SPEC = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SPEC", oldValue, _internal_SPEC));
        }
    }

    public function set SUBDIVISIONID(value:String) : void
    {
        var oldValue:String = _internal_SUBDIVISIONID;
        if (oldValue !== value)
        {
            _internal_SUBDIVISIONID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUBDIVISIONID", oldValue, _internal_SUBDIVISIONID));
        }
    }

    public function set SUBDIVISIONNAME(value:String) : void
    {
        var oldValue:String = _internal_SUBDIVISIONNAME;
        if (oldValue !== value)
        {
            _internal_SUBDIVISIONNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUBDIVISIONNAME", oldValue, _internal_SUBDIVISIONNAME));
        }
    }

    public function set SUBDIVISIONCITY(value:String) : void
    {
        var oldValue:String = _internal_SUBDIVISIONCITY;
        if (oldValue !== value)
        {
            _internal_SUBDIVISIONCITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUBDIVISIONCITY", oldValue, _internal_SUBDIVISIONCITY));
        }
    }

    public function set LOGO(value:String) : void
    {
        var oldValue:String = _internal_LOGO;
        if (oldValue !== value)
        {
            _internal_LOGO = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LOGO", oldValue, _internal_LOGO));
        }
    }

    public function set ACCOUNT_CASHIERCODE(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNT_CASHIERCODE;
        if (oldValue !== value)
        {
            _internal_ACCOUNT_CASHIERCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNT_CASHIERCODE", oldValue, _internal_ACCOUNT_CASHIERCODE));
        }
    }

    public function set INFO2(value:String) : void
    {
        var oldValue:String = _internal_INFO2;
        if (oldValue !== value)
        {
            _internal_INFO2 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "INFO2", oldValue, _internal_INFO2));
        }
    }

    public function set ACCOUNTTYPE(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNTTYPE;
        if (oldValue !== value)
        {
            _internal_ACCOUNTTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNTTYPE", oldValue, _internal_ACCOUNTTYPE));
        }
    }

    public function set TAX_ID(value:String) : void
    {
        var oldValue:String = _internal_TAX_ID;
        if (oldValue !== value)
        {
            _internal_TAX_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TAX_ID", oldValue, _internal_TAX_ID));
        }
    }

    public function set BIRTH_DATE(value:String) : void
    {
        var oldValue:String = _internal_BIRTH_DATE;
        if (oldValue !== value)
        {
            _internal_BIRTH_DATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BIRTH_DATE", oldValue, _internal_BIRTH_DATE));
        }
    }

    public function set BIRTH_PLACE(value:String) : void
    {
        var oldValue:String = _internal_BIRTH_PLACE;
        if (oldValue !== value)
        {
            _internal_BIRTH_PLACE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BIRTH_PLACE", oldValue, _internal_BIRTH_PLACE));
        }
    }

    public function set GENDER(value:int) : void
    {
        var oldValue:int = _internal_GENDER;
        if (oldValue !== value)
        {
            _internal_GENDER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "GENDER", oldValue, _internal_GENDER));
        }
    }

    public function set EMAIL(value:String) : void
    {
        var oldValue:String = _internal_EMAIL;
        if (oldValue !== value)
        {
            _internal_EMAIL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMAIL", oldValue, _internal_EMAIL));
        }
    }

    public function set USER_POSITION(value:String) : void
    {
        var oldValue:String = _internal_USER_POSITION;
        if (oldValue !== value)
        {
            _internal_USER_POSITION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "USER_POSITION", oldValue, _internal_USER_POSITION));
        }
    }

    public function set PHONE_TYPE(value:String) : void
    {
        var oldValue:String = _internal_PHONE_TYPE;
        if (oldValue !== value)
        {
            _internal_PHONE_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PHONE_TYPE", oldValue, _internal_PHONE_TYPE));
        }
    }

    public function set PHONE(value:String) : void
    {
        var oldValue:String = _internal_PHONE;
        if (oldValue !== value)
        {
            _internal_PHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PHONE", oldValue, _internal_PHONE));
        }
    }

    public function set DOC_TYPE(value:String) : void
    {
        var oldValue:String = _internal_DOC_TYPE;
        if (oldValue !== value)
        {
            _internal_DOC_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOC_TYPE", oldValue, _internal_DOC_TYPE));
        }
    }

    public function set DOC_NUMBER(value:String) : void
    {
        var oldValue:String = _internal_DOC_NUMBER;
        if (oldValue !== value)
        {
            _internal_DOC_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOC_NUMBER", oldValue, _internal_DOC_NUMBER));
        }
    }

    public function set IS_OWNER(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_IS_OWNER;
        if (oldValue !== value)
        {
            _internal_IS_OWNER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "IS_OWNER", oldValue, _internal_IS_OWNER));
        }
    }

    public function set EH_ACCESS_TOKEN(value:String) : void
    {
        var oldValue:String = _internal_EH_ACCESS_TOKEN;
        if (oldValue !== value)
        {
            _internal_EH_ACCESS_TOKEN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_ACCESS_TOKEN", oldValue, _internal_EH_ACCESS_TOKEN));
        }
    }

    public function set EH_ACCESS_TOKEN_EXPIRY(value:String) : void
    {
        var oldValue:String = _internal_EH_ACCESS_TOKEN_EXPIRY;
        if (oldValue !== value)
        {
            _internal_EH_ACCESS_TOKEN_EXPIRY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_ACCESS_TOKEN_EXPIRY", oldValue, _internal_EH_ACCESS_TOKEN_EXPIRY));
        }
    }

    public function set EH_SECRET_KEY(value:String) : void
    {
        var oldValue:String = _internal_EH_SECRET_KEY;
        if (oldValue !== value)
        {
            _internal_EH_SECRET_KEY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_SECRET_KEY", oldValue, _internal_EH_SECRET_KEY));
        }
    }

    public function set OA_DIR(value:String) : void
    {
        var oldValue:String = _internal_OA_DIR;
        if (oldValue !== value)
        {
            _internal_OA_DIR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OA_DIR", oldValue, _internal_OA_DIR));
        }
    }

    public function set EH_LEGALENTITY_ID(value:String) : void
    {
        var oldValue:String = _internal_EH_LEGALENTITY_ID;
        if (oldValue !== value)
        {
            _internal_EH_LEGALENTITY_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_LEGALENTITY_ID", oldValue, _internal_EH_LEGALENTITY_ID));
        }
    }

    public function set SCOPE(value:String) : void
    {
        var oldValue:String = _internal_SCOPE;
        if (oldValue !== value)
        {
            _internal_SCOPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SCOPE", oldValue, _internal_SCOPE));
        }
    }

    public function set EH_STARTDATE(value:String) : void
    {
        var oldValue:String = _internal_EH_STARTDATE;
        if (oldValue !== value)
        {
            _internal_EH_STARTDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_STARTDATE", oldValue, _internal_EH_STARTDATE));
        }
    }

    public function set EH_ENDDATE(value:String) : void
    {
        var oldValue:String = _internal_EH_ENDDATE;
        if (oldValue !== value)
        {
            _internal_EH_ENDDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_ENDDATE", oldValue, _internal_EH_ENDDATE));
        }
    }

    public function set EH_USER_ID(value:String) : void
    {
        var oldValue:String = _internal_EH_USER_ID;
        if (oldValue !== value)
        {
            _internal_EH_USER_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EH_USER_ID", oldValue, _internal_EH_USER_ID));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AdminModuleAutorizeObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AdminModuleAutorizeObjectEntityMetadata) : void
    {
        var oldValue : _AdminModuleAutorizeObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
