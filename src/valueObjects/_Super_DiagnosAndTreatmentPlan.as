/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - DiagnosAndTreatmentPlan.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_DiagnosAndTreatmentPlan extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("DiagnosAndTreatmentPlan") == null)
            {
                flash.net.registerClassAlias("DiagnosAndTreatmentPlan", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("DiagnosAndTreatmentPlan", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _DiagnosAndTreatmentPlanEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_DIAGNOSES : String;
    private var _internal_TREATMENTPLAN : String;
    private var _internal_PRICE : Number;
    private var _internal_EXAMID : int;
    private var _internal_CREATEDATEANDTIME : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_DiagnosAndTreatmentPlan()
    {
        _model = new _DiagnosAndTreatmentPlanEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get DIAGNOSES() : String
    {
        return _internal_DIAGNOSES;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPLAN() : String
    {
        return _internal_TREATMENTPLAN;
    }

    [Bindable(event="propertyChange")]
    public function get PRICE() : Number
    {
        return _internal_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get EXAMID() : int
    {
        return _internal_EXAMID;
    }

    [Bindable(event="propertyChange")]
    public function get CREATEDATEANDTIME() : String
    {
        return _internal_CREATEDATEANDTIME;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set DIAGNOSES(value:String) : void
    {
        var oldValue:String = _internal_DIAGNOSES;
        if (oldValue !== value)
        {
            _internal_DIAGNOSES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DIAGNOSES", oldValue, _internal_DIAGNOSES));
        }
    }

    public function set TREATMENTPLAN(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPLAN;
        if (oldValue !== value)
        {
            _internal_TREATMENTPLAN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPLAN", oldValue, _internal_TREATMENTPLAN));
        }
    }

    public function set PRICE(value:Number) : void
    {
        var oldValue:Number = _internal_PRICE;
        if (isNaN(_internal_PRICE) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PRICE", oldValue, _internal_PRICE));
        }
    }

    public function set EXAMID(value:int) : void
    {
        var oldValue:int = _internal_EXAMID;
        if (oldValue !== value)
        {
            _internal_EXAMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXAMID", oldValue, _internal_EXAMID));
        }
    }

    public function set CREATEDATEANDTIME(value:String) : void
    {
        var oldValue:String = _internal_CREATEDATEANDTIME;
        if (oldValue !== value)
        {
            _internal_CREATEDATEANDTIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CREATEDATEANDTIME", oldValue, _internal_CREATEDATEANDTIME));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _DiagnosAndTreatmentPlanEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _DiagnosAndTreatmentPlanEntityMetadata) : void
    {
        var oldValue : _DiagnosAndTreatmentPlanEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
