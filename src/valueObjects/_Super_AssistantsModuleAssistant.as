/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AssistantsModuleAssistant.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AssistantsModuleAssistant extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AssistantsModuleAssistant") == null)
            {
                flash.net.registerClassAlias("AssistantsModuleAssistant", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AssistantsModuleAssistant", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AssistantsModuleAssistantEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_TIN : String;
    private var _internal_FIRSTNAME : String;
    private var _internal_MIDDLENAME : String;
    private var _internal_LASTNAME : String;
    private var _internal_FULLNAME : String;
    private var _internal_SHORTNAME : String;
    private var _internal_BIRTHDAY : String;
    private var _internal_SEX : String;
    private var _internal_DATEWORKSTART : String;
    private var _internal_DATEWORKEND : String;
    private var _internal_MOBILEPHONE : String;
    private var _internal_HOMEPHONE : String;
    private var _internal_EMAIL : String;
    private var _internal_SKYPE : String;
    private var _internal_HOMEADRESS : String;
    private var _internal_POSTALCODE : String;
    private var _internal_STATE : String;
    private var _internal_REGION : String;
    private var _internal_CITY : String;
    private var _internal_BARCODE : String;
    private var _internal_isFired : int;
    private var _internal_fingerid : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AssistantsModuleAssistant()
    {
        _model = new _AssistantsModuleAssistantEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get TIN() : String
    {
        return _internal_TIN;
    }

    [Bindable(event="propertyChange")]
    public function get FIRSTNAME() : String
    {
        return _internal_FIRSTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get MIDDLENAME() : String
    {
        return _internal_MIDDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get LASTNAME() : String
    {
        return _internal_LASTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get FULLNAME() : String
    {
        return _internal_FULLNAME;
    }

    [Bindable(event="propertyChange")]
    public function get SHORTNAME() : String
    {
        return _internal_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get BIRTHDAY() : String
    {
        return _internal_BIRTHDAY;
    }

    [Bindable(event="propertyChange")]
    public function get SEX() : String
    {
        return _internal_SEX;
    }

    [Bindable(event="propertyChange")]
    public function get DATEWORKSTART() : String
    {
        return _internal_DATEWORKSTART;
    }

    [Bindable(event="propertyChange")]
    public function get DATEWORKEND() : String
    {
        return _internal_DATEWORKEND;
    }

    [Bindable(event="propertyChange")]
    public function get MOBILEPHONE() : String
    {
        return _internal_MOBILEPHONE;
    }

    [Bindable(event="propertyChange")]
    public function get HOMEPHONE() : String
    {
        return _internal_HOMEPHONE;
    }

    [Bindable(event="propertyChange")]
    public function get EMAIL() : String
    {
        return _internal_EMAIL;
    }

    [Bindable(event="propertyChange")]
    public function get SKYPE() : String
    {
        return _internal_SKYPE;
    }

    [Bindable(event="propertyChange")]
    public function get HOMEADRESS() : String
    {
        return _internal_HOMEADRESS;
    }

    [Bindable(event="propertyChange")]
    public function get POSTALCODE() : String
    {
        return _internal_POSTALCODE;
    }

    [Bindable(event="propertyChange")]
    public function get STATE() : String
    {
        return _internal_STATE;
    }

    [Bindable(event="propertyChange")]
    public function get REGION() : String
    {
        return _internal_REGION;
    }

    [Bindable(event="propertyChange")]
    public function get CITY() : String
    {
        return _internal_CITY;
    }

    [Bindable(event="propertyChange")]
    public function get BARCODE() : String
    {
        return _internal_BARCODE;
    }

    [Bindable(event="propertyChange")]
    public function get isFired() : int
    {
        return _internal_isFired;
    }

    [Bindable(event="propertyChange")]
    public function get fingerid() : String
    {
        return _internal_fingerid;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set TIN(value:String) : void
    {
        var oldValue:String = _internal_TIN;
        if (oldValue !== value)
        {
            _internal_TIN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TIN", oldValue, _internal_TIN));
        }
    }

    public function set FIRSTNAME(value:String) : void
    {
        var oldValue:String = _internal_FIRSTNAME;
        if (oldValue !== value)
        {
            _internal_FIRSTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FIRSTNAME", oldValue, _internal_FIRSTNAME));
        }
    }

    public function set MIDDLENAME(value:String) : void
    {
        var oldValue:String = _internal_MIDDLENAME;
        if (oldValue !== value)
        {
            _internal_MIDDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MIDDLENAME", oldValue, _internal_MIDDLENAME));
        }
    }

    public function set LASTNAME(value:String) : void
    {
        var oldValue:String = _internal_LASTNAME;
        if (oldValue !== value)
        {
            _internal_LASTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LASTNAME", oldValue, _internal_LASTNAME));
        }
    }

    public function set FULLNAME(value:String) : void
    {
        var oldValue:String = _internal_FULLNAME;
        if (oldValue !== value)
        {
            _internal_FULLNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FULLNAME", oldValue, _internal_FULLNAME));
        }
    }

    public function set SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SHORTNAME", oldValue, _internal_SHORTNAME));
        }
    }

    public function set BIRTHDAY(value:String) : void
    {
        var oldValue:String = _internal_BIRTHDAY;
        if (oldValue !== value)
        {
            _internal_BIRTHDAY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BIRTHDAY", oldValue, _internal_BIRTHDAY));
        }
    }

    public function set SEX(value:String) : void
    {
        var oldValue:String = _internal_SEX;
        if (oldValue !== value)
        {
            _internal_SEX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SEX", oldValue, _internal_SEX));
        }
    }

    public function set DATEWORKSTART(value:String) : void
    {
        var oldValue:String = _internal_DATEWORKSTART;
        if (oldValue !== value)
        {
            _internal_DATEWORKSTART = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATEWORKSTART", oldValue, _internal_DATEWORKSTART));
        }
    }

    public function set DATEWORKEND(value:String) : void
    {
        var oldValue:String = _internal_DATEWORKEND;
        if (oldValue !== value)
        {
            _internal_DATEWORKEND = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATEWORKEND", oldValue, _internal_DATEWORKEND));
        }
    }

    public function set MOBILEPHONE(value:String) : void
    {
        var oldValue:String = _internal_MOBILEPHONE;
        if (oldValue !== value)
        {
            _internal_MOBILEPHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "MOBILEPHONE", oldValue, _internal_MOBILEPHONE));
        }
    }

    public function set HOMEPHONE(value:String) : void
    {
        var oldValue:String = _internal_HOMEPHONE;
        if (oldValue !== value)
        {
            _internal_HOMEPHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HOMEPHONE", oldValue, _internal_HOMEPHONE));
        }
    }

    public function set EMAIL(value:String) : void
    {
        var oldValue:String = _internal_EMAIL;
        if (oldValue !== value)
        {
            _internal_EMAIL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMAIL", oldValue, _internal_EMAIL));
        }
    }

    public function set SKYPE(value:String) : void
    {
        var oldValue:String = _internal_SKYPE;
        if (oldValue !== value)
        {
            _internal_SKYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SKYPE", oldValue, _internal_SKYPE));
        }
    }

    public function set HOMEADRESS(value:String) : void
    {
        var oldValue:String = _internal_HOMEADRESS;
        if (oldValue !== value)
        {
            _internal_HOMEADRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HOMEADRESS", oldValue, _internal_HOMEADRESS));
        }
    }

    public function set POSTALCODE(value:String) : void
    {
        var oldValue:String = _internal_POSTALCODE;
        if (oldValue !== value)
        {
            _internal_POSTALCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "POSTALCODE", oldValue, _internal_POSTALCODE));
        }
    }

    public function set STATE(value:String) : void
    {
        var oldValue:String = _internal_STATE;
        if (oldValue !== value)
        {
            _internal_STATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATE", oldValue, _internal_STATE));
        }
    }

    public function set REGION(value:String) : void
    {
        var oldValue:String = _internal_REGION;
        if (oldValue !== value)
        {
            _internal_REGION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "REGION", oldValue, _internal_REGION));
        }
    }

    public function set CITY(value:String) : void
    {
        var oldValue:String = _internal_CITY;
        if (oldValue !== value)
        {
            _internal_CITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CITY", oldValue, _internal_CITY));
        }
    }

    public function set BARCODE(value:String) : void
    {
        var oldValue:String = _internal_BARCODE;
        if (oldValue !== value)
        {
            _internal_BARCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BARCODE", oldValue, _internal_BARCODE));
        }
    }

    public function set isFired(value:int) : void
    {
        var oldValue:int = _internal_isFired;
        if (oldValue !== value)
        {
            _internal_isFired = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isFired", oldValue, _internal_isFired));
        }
    }

    public function set fingerid(value:String) : void
    {
        var oldValue:String = _internal_fingerid;
        if (oldValue !== value)
        {
            _internal_fingerid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fingerid", oldValue, _internal_fingerid));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AssistantsModuleAssistantEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AssistantsModuleAssistantEntityMetadata) : void
    {
        var oldValue : _AssistantsModuleAssistantEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
