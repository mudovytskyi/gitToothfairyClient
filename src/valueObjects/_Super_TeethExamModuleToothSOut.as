/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - TeethExamModuleToothSOut.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.TeethExamModuleAssistantForTooth;
import valueObjects.TeethExamModuleDocForTooth;
import valueObjects.TeethExamModuleToothOut;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_TeethExamModuleToothSOut extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("TeethExamModuleToothSOut") == null)
            {
                flash.net.registerClassAlias("TeethExamModuleToothSOut", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("TeethExamModuleToothSOut", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.TeethExamModuleDocForTooth.initRemoteClassAliasSingleChild();
        valueObjects.TeethExamModuleAssistantForTooth.initRemoteClassAliasSingleChild();
        valueObjects.TeethExamModuleToothOut.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _TeethExamModuleToothSOutEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_birthyears : Object;
    private var _internal_fname : String;
    private var _internal_lname : String;
    private var _internal_kartnum : String;
    private var _internal_docList : ArrayCollection;
    model_internal var _internal_docList_leaf:valueObjects.TeethExamModuleDocForTooth;
    private var _internal_assistantsList : ArrayCollection;
    model_internal var _internal_assistantsList_leaf:valueObjects.TeethExamModuleAssistantForTooth;
    private var _internal_ToothOut : ArrayCollection;
    model_internal var _internal_ToothOut_leaf:valueObjects.TeethExamModuleToothOut;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_TeethExamModuleToothSOut()
    {
        _model = new _TeethExamModuleToothSOutEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get birthyears() : Object
    {
        return _internal_birthyears;
    }

    [Bindable(event="propertyChange")]
    public function get fname() : String
    {
        return _internal_fname;
    }

    [Bindable(event="propertyChange")]
    public function get lname() : String
    {
        return _internal_lname;
    }

    [Bindable(event="propertyChange")]
    public function get kartnum() : String
    {
        return _internal_kartnum;
    }

    [Bindable(event="propertyChange")]
    public function get docList() : ArrayCollection
    {
        return _internal_docList;
    }

    [Bindable(event="propertyChange")]
    public function get assistantsList() : ArrayCollection
    {
        return _internal_assistantsList;
    }

    [Bindable(event="propertyChange")]
    public function get ToothOut() : ArrayCollection
    {
        return _internal_ToothOut;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set birthyears(value:Object) : void
    {
        var oldValue:Object = _internal_birthyears;
        if (oldValue !== value)
        {
            _internal_birthyears = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "birthyears", oldValue, _internal_birthyears));
        }
    }

    public function set fname(value:String) : void
    {
        var oldValue:String = _internal_fname;
        if (oldValue !== value)
        {
            _internal_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fname", oldValue, _internal_fname));
        }
    }

    public function set lname(value:String) : void
    {
        var oldValue:String = _internal_lname;
        if (oldValue !== value)
        {
            _internal_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "lname", oldValue, _internal_lname));
        }
    }

    public function set kartnum(value:String) : void
    {
        var oldValue:String = _internal_kartnum;
        if (oldValue !== value)
        {
            _internal_kartnum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "kartnum", oldValue, _internal_kartnum));
        }
    }

    public function set docList(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_docList;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_docList = value;
            }
            else if (value is Array)
            {
                _internal_docList = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_docList = null;
            }
            else
            {
                throw new Error("value of docList must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "docList", oldValue, _internal_docList));
        }
    }

    public function set assistantsList(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_assistantsList;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_assistantsList = value;
            }
            else if (value is Array)
            {
                _internal_assistantsList = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_assistantsList = null;
            }
            else
            {
                throw new Error("value of assistantsList must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "assistantsList", oldValue, _internal_assistantsList));
        }
    }

    public function set ToothOut(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_ToothOut;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_ToothOut = value;
            }
            else if (value is Array)
            {
                _internal_ToothOut = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_ToothOut = null;
            }
            else
            {
                throw new Error("value of ToothOut must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ToothOut", oldValue, _internal_ToothOut));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _TeethExamModuleToothSOutEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _TeethExamModuleToothSOutEntityMetadata) : void
    {
        var oldValue : _TeethExamModuleToothSOutEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
