/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefStatisticsModule_PatientDoctorData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefStatisticsModule_PatientDoctorData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefStatisticsModule_PatientDoctorData") == null)
            {
                flash.net.registerClassAlias("ChiefStatisticsModule_PatientDoctorData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefStatisticsModule_PatientDoctorData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefStatisticsModule_PatientDoctorDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_doctor_id : String;
    private var _internal_doctor_lname : String;
    private var _internal_doctor_fname : String;
    private var _internal_doctor_sname : String;
    private var _internal_doctor_short : String;
    private var _internal_task_count : int;
    private var _internal_task_regular_count : int;
    private var _internal_patients_primary_count : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefStatisticsModule_PatientDoctorData()
    {
        _model = new _ChiefStatisticsModule_PatientDoctorDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lname() : String
    {
        return _internal_doctor_lname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_fname() : String
    {
        return _internal_doctor_fname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_sname() : String
    {
        return _internal_doctor_sname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_short() : String
    {
        return _internal_doctor_short;
    }

    [Bindable(event="propertyChange")]
    public function get task_count() : int
    {
        return _internal_task_count;
    }

    [Bindable(event="propertyChange")]
    public function get task_regular_count() : int
    {
        return _internal_task_regular_count;
    }

    [Bindable(event="propertyChange")]
    public function get patients_primary_count() : int
    {
        return _internal_patients_primary_count;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_lname(value:String) : void
    {
        var oldValue:String = _internal_doctor_lname;
        if (oldValue !== value)
        {
            _internal_doctor_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lname", oldValue, _internal_doctor_lname));
        }
    }

    public function set doctor_fname(value:String) : void
    {
        var oldValue:String = _internal_doctor_fname;
        if (oldValue !== value)
        {
            _internal_doctor_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_fname", oldValue, _internal_doctor_fname));
        }
    }

    public function set doctor_sname(value:String) : void
    {
        var oldValue:String = _internal_doctor_sname;
        if (oldValue !== value)
        {
            _internal_doctor_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_sname", oldValue, _internal_doctor_sname));
        }
    }

    public function set doctor_short(value:String) : void
    {
        var oldValue:String = _internal_doctor_short;
        if (oldValue !== value)
        {
            _internal_doctor_short = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_short", oldValue, _internal_doctor_short));
        }
    }

    public function set task_count(value:int) : void
    {
        var oldValue:int = _internal_task_count;
        if (oldValue !== value)
        {
            _internal_task_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_count", oldValue, _internal_task_count));
        }
    }

    public function set task_regular_count(value:int) : void
    {
        var oldValue:int = _internal_task_regular_count;
        if (oldValue !== value)
        {
            _internal_task_regular_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_regular_count", oldValue, _internal_task_regular_count));
        }
    }

    public function set patients_primary_count(value:int) : void
    {
        var oldValue:int = _internal_patients_primary_count;
        if (oldValue !== value)
        {
            _internal_patients_primary_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patients_primary_count", oldValue, _internal_patients_primary_count));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefStatisticsModule_PatientDoctorDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefStatisticsModule_PatientDoctorDataEntityMetadata) : void
    {
        var oldValue : _ChiefStatisticsModule_PatientDoctorDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
