/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AccountModuleReceiptData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.AccountModuleReceiptProcedurData;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AccountModuleReceiptData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AccountModuleReceiptData") == null)
            {
                flash.net.registerClassAlias("AccountModuleReceiptData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AccountModuleReceiptData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.AccountModuleReceiptProcedurData.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _AccountModuleReceiptDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_company : String;
    private var _internal_companyaddress : String;
    private var _internal_companyname : String;
    private var _internal_companyin : String;
    private var _internal_companylogo : Object;
    private var _internal_companyphone : String;
    private var _internal_companysite : String;
    private var _internal_director : String;
    private var _internal_fax : String;
    private var _internal_email : String;
    private var _internal_chiefaccountant : String;
    private var _internal_legaladdress : String;
    private var _internal_bank_name : String;
    private var _internal_bank_mfo : String;
    private var _internal_bank_currentaccount : String;
    private var _internal_license_number : String;
    private var _internal_license_series : String;
    private var _internal_license_startdate : String;
    private var _internal_license_enddate : String;
    private var _internal_license_issued : String;
    private var _internal_receiptnumber : String;
    private var _internal_discount_amount : Number;
    private var _internal_paid_sum : Number;
    private var _internal_doctorSname : String;
    private var _internal_patientSname : String;
    private var _internal_creatdate : String;
    private var _internal_ReceiptProcedurData : ArrayCollection;
    model_internal var _internal_ReceiptProcedurData_leaf:valueObjects.AccountModuleReceiptProcedurData;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AccountModuleReceiptData()
    {
        _model = new _AccountModuleReceiptDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get company() : String
    {
        return _internal_company;
    }

    [Bindable(event="propertyChange")]
    public function get companyaddress() : String
    {
        return _internal_companyaddress;
    }

    [Bindable(event="propertyChange")]
    public function get companyname() : String
    {
        return _internal_companyname;
    }

    [Bindable(event="propertyChange")]
    public function get companyin() : String
    {
        return _internal_companyin;
    }

    [Bindable(event="propertyChange")]
    public function get companylogo() : Object
    {
        return _internal_companylogo;
    }

    [Bindable(event="propertyChange")]
    public function get companyphone() : String
    {
        return _internal_companyphone;
    }

    [Bindable(event="propertyChange")]
    public function get companysite() : String
    {
        return _internal_companysite;
    }

    [Bindable(event="propertyChange")]
    public function get director() : String
    {
        return _internal_director;
    }

    [Bindable(event="propertyChange")]
    public function get fax() : String
    {
        return _internal_fax;
    }

    [Bindable(event="propertyChange")]
    public function get email() : String
    {
        return _internal_email;
    }

    [Bindable(event="propertyChange")]
    public function get chiefaccountant() : String
    {
        return _internal_chiefaccountant;
    }

    [Bindable(event="propertyChange")]
    public function get legaladdress() : String
    {
        return _internal_legaladdress;
    }

    [Bindable(event="propertyChange")]
    public function get bank_name() : String
    {
        return _internal_bank_name;
    }

    [Bindable(event="propertyChange")]
    public function get bank_mfo() : String
    {
        return _internal_bank_mfo;
    }

    [Bindable(event="propertyChange")]
    public function get bank_currentaccount() : String
    {
        return _internal_bank_currentaccount;
    }

    [Bindable(event="propertyChange")]
    public function get license_number() : String
    {
        return _internal_license_number;
    }

    [Bindable(event="propertyChange")]
    public function get license_series() : String
    {
        return _internal_license_series;
    }

    [Bindable(event="propertyChange")]
    public function get license_startdate() : String
    {
        return _internal_license_startdate;
    }

    [Bindable(event="propertyChange")]
    public function get license_enddate() : String
    {
        return _internal_license_enddate;
    }

    [Bindable(event="propertyChange")]
    public function get license_issued() : String
    {
        return _internal_license_issued;
    }

    [Bindable(event="propertyChange")]
    public function get receiptnumber() : String
    {
        return _internal_receiptnumber;
    }

    [Bindable(event="propertyChange")]
    public function get discount_amount() : Number
    {
        return _internal_discount_amount;
    }

    [Bindable(event="propertyChange")]
    public function get paid_sum() : Number
    {
        return _internal_paid_sum;
    }

    [Bindable(event="propertyChange")]
    public function get doctorSname() : String
    {
        return _internal_doctorSname;
    }

    [Bindable(event="propertyChange")]
    public function get patientSname() : String
    {
        return _internal_patientSname;
    }

    [Bindable(event="propertyChange")]
    public function get creatdate() : String
    {
        return _internal_creatdate;
    }

    [Bindable(event="propertyChange")]
    public function get ReceiptProcedurData() : ArrayCollection
    {
        return _internal_ReceiptProcedurData;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set company(value:String) : void
    {
        var oldValue:String = _internal_company;
        if (oldValue !== value)
        {
            _internal_company = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "company", oldValue, _internal_company));
        }
    }

    public function set companyaddress(value:String) : void
    {
        var oldValue:String = _internal_companyaddress;
        if (oldValue !== value)
        {
            _internal_companyaddress = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "companyaddress", oldValue, _internal_companyaddress));
        }
    }

    public function set companyname(value:String) : void
    {
        var oldValue:String = _internal_companyname;
        if (oldValue !== value)
        {
            _internal_companyname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "companyname", oldValue, _internal_companyname));
        }
    }

    public function set companyin(value:String) : void
    {
        var oldValue:String = _internal_companyin;
        if (oldValue !== value)
        {
            _internal_companyin = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "companyin", oldValue, _internal_companyin));
        }
    }

    public function set companylogo(value:Object) : void
    {
        var oldValue:Object = _internal_companylogo;
        if (oldValue !== value)
        {
            _internal_companylogo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "companylogo", oldValue, _internal_companylogo));
        }
    }

    public function set companyphone(value:String) : void
    {
        var oldValue:String = _internal_companyphone;
        if (oldValue !== value)
        {
            _internal_companyphone = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "companyphone", oldValue, _internal_companyphone));
        }
    }

    public function set companysite(value:String) : void
    {
        var oldValue:String = _internal_companysite;
        if (oldValue !== value)
        {
            _internal_companysite = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "companysite", oldValue, _internal_companysite));
        }
    }

    public function set director(value:String) : void
    {
        var oldValue:String = _internal_director;
        if (oldValue !== value)
        {
            _internal_director = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "director", oldValue, _internal_director));
        }
    }

    public function set fax(value:String) : void
    {
        var oldValue:String = _internal_fax;
        if (oldValue !== value)
        {
            _internal_fax = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fax", oldValue, _internal_fax));
        }
    }

    public function set email(value:String) : void
    {
        var oldValue:String = _internal_email;
        if (oldValue !== value)
        {
            _internal_email = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "email", oldValue, _internal_email));
        }
    }

    public function set chiefaccountant(value:String) : void
    {
        var oldValue:String = _internal_chiefaccountant;
        if (oldValue !== value)
        {
            _internal_chiefaccountant = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "chiefaccountant", oldValue, _internal_chiefaccountant));
        }
    }

    public function set legaladdress(value:String) : void
    {
        var oldValue:String = _internal_legaladdress;
        if (oldValue !== value)
        {
            _internal_legaladdress = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "legaladdress", oldValue, _internal_legaladdress));
        }
    }

    public function set bank_name(value:String) : void
    {
        var oldValue:String = _internal_bank_name;
        if (oldValue !== value)
        {
            _internal_bank_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "bank_name", oldValue, _internal_bank_name));
        }
    }

    public function set bank_mfo(value:String) : void
    {
        var oldValue:String = _internal_bank_mfo;
        if (oldValue !== value)
        {
            _internal_bank_mfo = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "bank_mfo", oldValue, _internal_bank_mfo));
        }
    }

    public function set bank_currentaccount(value:String) : void
    {
        var oldValue:String = _internal_bank_currentaccount;
        if (oldValue !== value)
        {
            _internal_bank_currentaccount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "bank_currentaccount", oldValue, _internal_bank_currentaccount));
        }
    }

    public function set license_number(value:String) : void
    {
        var oldValue:String = _internal_license_number;
        if (oldValue !== value)
        {
            _internal_license_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "license_number", oldValue, _internal_license_number));
        }
    }

    public function set license_series(value:String) : void
    {
        var oldValue:String = _internal_license_series;
        if (oldValue !== value)
        {
            _internal_license_series = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "license_series", oldValue, _internal_license_series));
        }
    }

    public function set license_startdate(value:String) : void
    {
        var oldValue:String = _internal_license_startdate;
        if (oldValue !== value)
        {
            _internal_license_startdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "license_startdate", oldValue, _internal_license_startdate));
        }
    }

    public function set license_enddate(value:String) : void
    {
        var oldValue:String = _internal_license_enddate;
        if (oldValue !== value)
        {
            _internal_license_enddate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "license_enddate", oldValue, _internal_license_enddate));
        }
    }

    public function set license_issued(value:String) : void
    {
        var oldValue:String = _internal_license_issued;
        if (oldValue !== value)
        {
            _internal_license_issued = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "license_issued", oldValue, _internal_license_issued));
        }
    }

    public function set receiptnumber(value:String) : void
    {
        var oldValue:String = _internal_receiptnumber;
        if (oldValue !== value)
        {
            _internal_receiptnumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "receiptnumber", oldValue, _internal_receiptnumber));
        }
    }

    public function set discount_amount(value:Number) : void
    {
        var oldValue:Number = _internal_discount_amount;
        if (isNaN(_internal_discount_amount) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_discount_amount = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "discount_amount", oldValue, _internal_discount_amount));
        }
    }

    public function set paid_sum(value:Number) : void
    {
        var oldValue:Number = _internal_paid_sum;
        if (isNaN(_internal_paid_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_paid_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "paid_sum", oldValue, _internal_paid_sum));
        }
    }

    public function set doctorSname(value:String) : void
    {
        var oldValue:String = _internal_doctorSname;
        if (oldValue !== value)
        {
            _internal_doctorSname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctorSname", oldValue, _internal_doctorSname));
        }
    }

    public function set patientSname(value:String) : void
    {
        var oldValue:String = _internal_patientSname;
        if (oldValue !== value)
        {
            _internal_patientSname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientSname", oldValue, _internal_patientSname));
        }
    }

    public function set creatdate(value:String) : void
    {
        var oldValue:String = _internal_creatdate;
        if (oldValue !== value)
        {
            _internal_creatdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "creatdate", oldValue, _internal_creatdate));
        }
    }

    public function set ReceiptProcedurData(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_ReceiptProcedurData;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_ReceiptProcedurData = value;
            }
            else if (value is Array)
            {
                _internal_ReceiptProcedurData = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_ReceiptProcedurData = null;
            }
            else
            {
                throw new Error("value of ReceiptProcedurData must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ReceiptProcedurData", oldValue, _internal_ReceiptProcedurData));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AccountModuleReceiptDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AccountModuleReceiptDataEntityMetadata) : void
    {
        var oldValue : _AccountModuleReceiptDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
