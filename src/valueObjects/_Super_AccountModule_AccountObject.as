/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AccountModule_AccountObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AccountModule_AccountObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AccountModule_AccountObject") == null)
            {
                flash.net.registerClassAlias("AccountModule_AccountObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AccountModule_AccountObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AccountModule_AccountObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_flow_id : String;
    private var _internal_patient_id : String;
    private var _internal_patient_fname : String;
    private var _internal_patient_lname : String;
    private var _internal_patient_sname : String;
    private var _internal_patient_cardnumber : String;
    private var _internal_flow_sum : Number;
    private var _internal_flow_operationtype : int;
    private var _internal_flow_operationnote : String;
    private var _internal_flow_createdate : String;
    private var _internal_flow_ordernumber : int;
    private var _internal_flow_oninvoice_fk : String;
    private var _internal_invoice_number : String;
    private var _internal_invoice_createdate : String;
    private var _internal_flow_isCashpayment : int;
    private var _internal_flow_groupName : String;
    private var _internal_flow_award_personId : String;
    private var _internal_flow_award_personFio : String;
    private var _internal_flow_award_foundDate : String;
    private var _internal_exp_group_id : String;
    private var _internal_exp_group_name : String;
    private var _internal_exp_group_color : int;
    private var _internal_exp_proc_id : String;
    private var _internal_exp_proc_dateclose : String;
    private var _internal_exp_proc_name : String;
    private var _internal_exp_proc_shifr : String;
    private var _internal_exp_doctor_fname : String;
    private var _internal_exp_doctor_lname : String;
    private var _internal_exp_doctor_sname : String;
    private var _internal_exp_patient_fname : String;
    private var _internal_exp_patient_lname : String;
    private var _internal_exp_patient_sname : String;
    private var _internal_author_id : String;
    private var _internal_author_shortname : String;
    private var _internal_subdivision_id : String;
    private var _internal_subdivision_name : String;
    private var _internal_certificate_id : String;
    private var _internal_certificate_number : String;
    private var _internal_storage_docId : String;
    private var _internal_storage_docType : int;
    private var _internal_storage_docNumber : String;
    private var _internal_storage_docDate : String;
    private var _internal_storage_patientId : String;
    private var _internal_storage_patientShortname : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AccountModule_AccountObject()
    {
        _model = new _AccountModule_AccountObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get flow_id() : String
    {
        return _internal_flow_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_fname() : String
    {
        return _internal_patient_fname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_lname() : String
    {
        return _internal_patient_lname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_sname() : String
    {
        return _internal_patient_sname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_cardnumber() : String
    {
        return _internal_patient_cardnumber;
    }

    [Bindable(event="propertyChange")]
    public function get flow_sum() : Number
    {
        return _internal_flow_sum;
    }

    [Bindable(event="propertyChange")]
    public function get flow_operationtype() : int
    {
        return _internal_flow_operationtype;
    }

    [Bindable(event="propertyChange")]
    public function get flow_operationnote() : String
    {
        return _internal_flow_operationnote;
    }

    [Bindable(event="propertyChange")]
    public function get flow_createdate() : String
    {
        return _internal_flow_createdate;
    }

    [Bindable(event="propertyChange")]
    public function get flow_ordernumber() : int
    {
        return _internal_flow_ordernumber;
    }

    [Bindable(event="propertyChange")]
    public function get flow_oninvoice_fk() : String
    {
        return _internal_flow_oninvoice_fk;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_number() : String
    {
        return _internal_invoice_number;
    }

    [Bindable(event="propertyChange")]
    public function get invoice_createdate() : String
    {
        return _internal_invoice_createdate;
    }

    [Bindable(event="propertyChange")]
    public function get flow_isCashpayment() : int
    {
        return _internal_flow_isCashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get flow_groupName() : String
    {
        return _internal_flow_groupName;
    }

    [Bindable(event="propertyChange")]
    public function get flow_award_personId() : String
    {
        return _internal_flow_award_personId;
    }

    [Bindable(event="propertyChange")]
    public function get flow_award_personFio() : String
    {
        return _internal_flow_award_personFio;
    }

    [Bindable(event="propertyChange")]
    public function get flow_award_foundDate() : String
    {
        return _internal_flow_award_foundDate;
    }

    [Bindable(event="propertyChange")]
    public function get exp_group_id() : String
    {
        return _internal_exp_group_id;
    }

    [Bindable(event="propertyChange")]
    public function get exp_group_name() : String
    {
        return _internal_exp_group_name;
    }

    [Bindable(event="propertyChange")]
    public function get exp_group_color() : int
    {
        return _internal_exp_group_color;
    }

    [Bindable(event="propertyChange")]
    public function get exp_proc_id() : String
    {
        return _internal_exp_proc_id;
    }

    [Bindable(event="propertyChange")]
    public function get exp_proc_dateclose() : String
    {
        return _internal_exp_proc_dateclose;
    }

    [Bindable(event="propertyChange")]
    public function get exp_proc_name() : String
    {
        return _internal_exp_proc_name;
    }

    [Bindable(event="propertyChange")]
    public function get exp_proc_shifr() : String
    {
        return _internal_exp_proc_shifr;
    }

    [Bindable(event="propertyChange")]
    public function get exp_doctor_fname() : String
    {
        return _internal_exp_doctor_fname;
    }

    [Bindable(event="propertyChange")]
    public function get exp_doctor_lname() : String
    {
        return _internal_exp_doctor_lname;
    }

    [Bindable(event="propertyChange")]
    public function get exp_doctor_sname() : String
    {
        return _internal_exp_doctor_sname;
    }

    [Bindable(event="propertyChange")]
    public function get exp_patient_fname() : String
    {
        return _internal_exp_patient_fname;
    }

    [Bindable(event="propertyChange")]
    public function get exp_patient_lname() : String
    {
        return _internal_exp_patient_lname;
    }

    [Bindable(event="propertyChange")]
    public function get exp_patient_sname() : String
    {
        return _internal_exp_patient_sname;
    }

    [Bindable(event="propertyChange")]
    public function get author_id() : String
    {
        return _internal_author_id;
    }

    [Bindable(event="propertyChange")]
    public function get author_shortname() : String
    {
        return _internal_author_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_id() : String
    {
        return _internal_subdivision_id;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_name() : String
    {
        return _internal_subdivision_name;
    }

    [Bindable(event="propertyChange")]
    public function get certificate_id() : String
    {
        return _internal_certificate_id;
    }

    [Bindable(event="propertyChange")]
    public function get certificate_number() : String
    {
        return _internal_certificate_number;
    }

    [Bindable(event="propertyChange")]
    public function get storage_docId() : String
    {
        return _internal_storage_docId;
    }

    [Bindable(event="propertyChange")]
    public function get storage_docType() : int
    {
        return _internal_storage_docType;
    }

    [Bindable(event="propertyChange")]
    public function get storage_docNumber() : String
    {
        return _internal_storage_docNumber;
    }

    [Bindable(event="propertyChange")]
    public function get storage_docDate() : String
    {
        return _internal_storage_docDate;
    }

    [Bindable(event="propertyChange")]
    public function get storage_patientId() : String
    {
        return _internal_storage_patientId;
    }

    [Bindable(event="propertyChange")]
    public function get storage_patientShortname() : String
    {
        return _internal_storage_patientShortname;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set flow_id(value:String) : void
    {
        var oldValue:String = _internal_flow_id;
        if (oldValue !== value)
        {
            _internal_flow_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_id", oldValue, _internal_flow_id));
        }
    }

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set patient_fname(value:String) : void
    {
        var oldValue:String = _internal_patient_fname;
        if (oldValue !== value)
        {
            _internal_patient_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_fname", oldValue, _internal_patient_fname));
        }
    }

    public function set patient_lname(value:String) : void
    {
        var oldValue:String = _internal_patient_lname;
        if (oldValue !== value)
        {
            _internal_patient_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_lname", oldValue, _internal_patient_lname));
        }
    }

    public function set patient_sname(value:String) : void
    {
        var oldValue:String = _internal_patient_sname;
        if (oldValue !== value)
        {
            _internal_patient_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_sname", oldValue, _internal_patient_sname));
        }
    }

    public function set patient_cardnumber(value:String) : void
    {
        var oldValue:String = _internal_patient_cardnumber;
        if (oldValue !== value)
        {
            _internal_patient_cardnumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_cardnumber", oldValue, _internal_patient_cardnumber));
        }
    }

    public function set flow_sum(value:Number) : void
    {
        var oldValue:Number = _internal_flow_sum;
        if (isNaN(_internal_flow_sum) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_flow_sum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_sum", oldValue, _internal_flow_sum));
        }
    }

    public function set flow_operationtype(value:int) : void
    {
        var oldValue:int = _internal_flow_operationtype;
        if (oldValue !== value)
        {
            _internal_flow_operationtype = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_operationtype", oldValue, _internal_flow_operationtype));
        }
    }

    public function set flow_operationnote(value:String) : void
    {
        var oldValue:String = _internal_flow_operationnote;
        if (oldValue !== value)
        {
            _internal_flow_operationnote = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_operationnote", oldValue, _internal_flow_operationnote));
        }
    }

    public function set flow_createdate(value:String) : void
    {
        var oldValue:String = _internal_flow_createdate;
        if (oldValue !== value)
        {
            _internal_flow_createdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_createdate", oldValue, _internal_flow_createdate));
        }
    }

    public function set flow_ordernumber(value:int) : void
    {
        var oldValue:int = _internal_flow_ordernumber;
        if (oldValue !== value)
        {
            _internal_flow_ordernumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_ordernumber", oldValue, _internal_flow_ordernumber));
        }
    }

    public function set flow_oninvoice_fk(value:String) : void
    {
        var oldValue:String = _internal_flow_oninvoice_fk;
        if (oldValue !== value)
        {
            _internal_flow_oninvoice_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_oninvoice_fk", oldValue, _internal_flow_oninvoice_fk));
        }
    }

    public function set invoice_number(value:String) : void
    {
        var oldValue:String = _internal_invoice_number;
        if (oldValue !== value)
        {
            _internal_invoice_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_number", oldValue, _internal_invoice_number));
        }
    }

    public function set invoice_createdate(value:String) : void
    {
        var oldValue:String = _internal_invoice_createdate;
        if (oldValue !== value)
        {
            _internal_invoice_createdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "invoice_createdate", oldValue, _internal_invoice_createdate));
        }
    }

    public function set flow_isCashpayment(value:int) : void
    {
        var oldValue:int = _internal_flow_isCashpayment;
        if (oldValue !== value)
        {
            _internal_flow_isCashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_isCashpayment", oldValue, _internal_flow_isCashpayment));
        }
    }

    public function set flow_groupName(value:String) : void
    {
        var oldValue:String = _internal_flow_groupName;
        if (oldValue !== value)
        {
            _internal_flow_groupName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_groupName", oldValue, _internal_flow_groupName));
        }
    }

    public function set flow_award_personId(value:String) : void
    {
        var oldValue:String = _internal_flow_award_personId;
        if (oldValue !== value)
        {
            _internal_flow_award_personId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_award_personId", oldValue, _internal_flow_award_personId));
        }
    }

    public function set flow_award_personFio(value:String) : void
    {
        var oldValue:String = _internal_flow_award_personFio;
        if (oldValue !== value)
        {
            _internal_flow_award_personFio = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_award_personFio", oldValue, _internal_flow_award_personFio));
        }
    }

    public function set flow_award_foundDate(value:String) : void
    {
        var oldValue:String = _internal_flow_award_foundDate;
        if (oldValue !== value)
        {
            _internal_flow_award_foundDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "flow_award_foundDate", oldValue, _internal_flow_award_foundDate));
        }
    }

    public function set exp_group_id(value:String) : void
    {
        var oldValue:String = _internal_exp_group_id;
        if (oldValue !== value)
        {
            _internal_exp_group_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_group_id", oldValue, _internal_exp_group_id));
        }
    }

    public function set exp_group_name(value:String) : void
    {
        var oldValue:String = _internal_exp_group_name;
        if (oldValue !== value)
        {
            _internal_exp_group_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_group_name", oldValue, _internal_exp_group_name));
        }
    }

    public function set exp_group_color(value:int) : void
    {
        var oldValue:int = _internal_exp_group_color;
        if (oldValue !== value)
        {
            _internal_exp_group_color = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_group_color", oldValue, _internal_exp_group_color));
        }
    }

    public function set exp_proc_id(value:String) : void
    {
        var oldValue:String = _internal_exp_proc_id;
        if (oldValue !== value)
        {
            _internal_exp_proc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_proc_id", oldValue, _internal_exp_proc_id));
        }
    }

    public function set exp_proc_dateclose(value:String) : void
    {
        var oldValue:String = _internal_exp_proc_dateclose;
        if (oldValue !== value)
        {
            _internal_exp_proc_dateclose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_proc_dateclose", oldValue, _internal_exp_proc_dateclose));
        }
    }

    public function set exp_proc_name(value:String) : void
    {
        var oldValue:String = _internal_exp_proc_name;
        if (oldValue !== value)
        {
            _internal_exp_proc_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_proc_name", oldValue, _internal_exp_proc_name));
        }
    }

    public function set exp_proc_shifr(value:String) : void
    {
        var oldValue:String = _internal_exp_proc_shifr;
        if (oldValue !== value)
        {
            _internal_exp_proc_shifr = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_proc_shifr", oldValue, _internal_exp_proc_shifr));
        }
    }

    public function set exp_doctor_fname(value:String) : void
    {
        var oldValue:String = _internal_exp_doctor_fname;
        if (oldValue !== value)
        {
            _internal_exp_doctor_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_doctor_fname", oldValue, _internal_exp_doctor_fname));
        }
    }

    public function set exp_doctor_lname(value:String) : void
    {
        var oldValue:String = _internal_exp_doctor_lname;
        if (oldValue !== value)
        {
            _internal_exp_doctor_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_doctor_lname", oldValue, _internal_exp_doctor_lname));
        }
    }

    public function set exp_doctor_sname(value:String) : void
    {
        var oldValue:String = _internal_exp_doctor_sname;
        if (oldValue !== value)
        {
            _internal_exp_doctor_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_doctor_sname", oldValue, _internal_exp_doctor_sname));
        }
    }

    public function set exp_patient_fname(value:String) : void
    {
        var oldValue:String = _internal_exp_patient_fname;
        if (oldValue !== value)
        {
            _internal_exp_patient_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_patient_fname", oldValue, _internal_exp_patient_fname));
        }
    }

    public function set exp_patient_lname(value:String) : void
    {
        var oldValue:String = _internal_exp_patient_lname;
        if (oldValue !== value)
        {
            _internal_exp_patient_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_patient_lname", oldValue, _internal_exp_patient_lname));
        }
    }

    public function set exp_patient_sname(value:String) : void
    {
        var oldValue:String = _internal_exp_patient_sname;
        if (oldValue !== value)
        {
            _internal_exp_patient_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "exp_patient_sname", oldValue, _internal_exp_patient_sname));
        }
    }

    public function set author_id(value:String) : void
    {
        var oldValue:String = _internal_author_id;
        if (oldValue !== value)
        {
            _internal_author_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_id", oldValue, _internal_author_id));
        }
    }

    public function set author_shortname(value:String) : void
    {
        var oldValue:String = _internal_author_shortname;
        if (oldValue !== value)
        {
            _internal_author_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "author_shortname", oldValue, _internal_author_shortname));
        }
    }

    public function set subdivision_id(value:String) : void
    {
        var oldValue:String = _internal_subdivision_id;
        if (oldValue !== value)
        {
            _internal_subdivision_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_id", oldValue, _internal_subdivision_id));
        }
    }

    public function set subdivision_name(value:String) : void
    {
        var oldValue:String = _internal_subdivision_name;
        if (oldValue !== value)
        {
            _internal_subdivision_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_name", oldValue, _internal_subdivision_name));
        }
    }

    public function set certificate_id(value:String) : void
    {
        var oldValue:String = _internal_certificate_id;
        if (oldValue !== value)
        {
            _internal_certificate_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "certificate_id", oldValue, _internal_certificate_id));
        }
    }

    public function set certificate_number(value:String) : void
    {
        var oldValue:String = _internal_certificate_number;
        if (oldValue !== value)
        {
            _internal_certificate_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "certificate_number", oldValue, _internal_certificate_number));
        }
    }

    public function set storage_docId(value:String) : void
    {
        var oldValue:String = _internal_storage_docId;
        if (oldValue !== value)
        {
            _internal_storage_docId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storage_docId", oldValue, _internal_storage_docId));
        }
    }

    public function set storage_docType(value:int) : void
    {
        var oldValue:int = _internal_storage_docType;
        if (oldValue !== value)
        {
            _internal_storage_docType = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storage_docType", oldValue, _internal_storage_docType));
        }
    }

    public function set storage_docNumber(value:String) : void
    {
        var oldValue:String = _internal_storage_docNumber;
        if (oldValue !== value)
        {
            _internal_storage_docNumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storage_docNumber", oldValue, _internal_storage_docNumber));
        }
    }

    public function set storage_docDate(value:String) : void
    {
        var oldValue:String = _internal_storage_docDate;
        if (oldValue !== value)
        {
            _internal_storage_docDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storage_docDate", oldValue, _internal_storage_docDate));
        }
    }

    public function set storage_patientId(value:String) : void
    {
        var oldValue:String = _internal_storage_patientId;
        if (oldValue !== value)
        {
            _internal_storage_patientId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storage_patientId", oldValue, _internal_storage_patientId));
        }
    }

    public function set storage_patientShortname(value:String) : void
    {
        var oldValue:String = _internal_storage_patientShortname;
        if (oldValue !== value)
        {
            _internal_storage_patientShortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "storage_patientShortname", oldValue, _internal_storage_patientShortname));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AccountModule_AccountObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AccountModule_AccountObjectEntityMetadata) : void
    {
        var oldValue : _AccountModule_AccountObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
