/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - DispanserModule_Task.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.DispanserModule_mobilephone;
import valueObjects.HealthPlanMKB10TreatmentAdditions;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_DispanserModule_Task extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("DispanserModule_Task") == null)
            {
                flash.net.registerClassAlias("DispanserModule_Task", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("DispanserModule_Task", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HealthPlanMKB10TreatmentAdditions.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Doctor.initRemoteClassAliasSingleChild();
        valueObjects.DispanserModule_mobilephone.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _DispanserModule_TaskEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_addiotions_Anamnes : ArrayCollection;
    model_internal var _internal_addiotions_Anamnes_leaf:valueObjects.HealthPlanMKB10TreatmentAdditions;
    private var _internal_addiotions_Xray : ArrayCollection;
    model_internal var _internal_addiotions_Xray_leaf:valueObjects.HealthPlanMKB10TreatmentAdditions;
    private var _internal_distask_id : String;
    private var _internal_patient_id : String;
    private var _internal_patient_fname : String;
    private var _internal_patient_lname : String;
    private var _internal_patient_sname : String;
    private var _internal_patient_cardnum : String;
    private var _internal_patient_mobiles : ArrayCollection;
    model_internal var _internal_patient_mobiles_leaf:valueObjects.DispanserModule_mobilephone;
    private var _internal_distask_date : String;
    private var _internal_task_id : String;
    private var _internal_task_date : String;
    private var _internal_task_begin_time : String;
    private var _internal_task_end_time : String;
    private var _internal_task_isVisited : int;
    private var _internal_distask_comment : String;
    private var _internal_doctor_id : String;
    private var _internal_doctor_fname : String;
    private var _internal_doctor_lname : String;
    private var _internal_disptype_id : String;
    private var _internal_disptype_label : String;
    private var _internal_disptype_color : int;
    private var _internal_smsstatus : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_DispanserModule_Task()
    {
        _model = new _DispanserModule_TaskEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get addiotions_Anamnes() : ArrayCollection
    {
        return _internal_addiotions_Anamnes;
    }

    [Bindable(event="propertyChange")]
    public function get addiotions_Xray() : ArrayCollection
    {
        return _internal_addiotions_Xray;
    }

    [Bindable(event="propertyChange")]
    public function get distask_id() : String
    {
        return _internal_distask_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_fname() : String
    {
        return _internal_patient_fname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_lname() : String
    {
        return _internal_patient_lname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_sname() : String
    {
        return _internal_patient_sname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_cardnum() : String
    {
        return _internal_patient_cardnum;
    }

    [Bindable(event="propertyChange")]
    public function get patient_mobiles() : ArrayCollection
    {
        return _internal_patient_mobiles;
    }

    [Bindable(event="propertyChange")]
    public function get distask_date() : String
    {
        return _internal_distask_date;
    }

    [Bindable(event="propertyChange")]
    public function get task_id() : String
    {
        return _internal_task_id;
    }

    [Bindable(event="propertyChange")]
    public function get task_date() : String
    {
        return _internal_task_date;
    }

    [Bindable(event="propertyChange")]
    public function get task_begin_time() : String
    {
        return _internal_task_begin_time;
    }

    [Bindable(event="propertyChange")]
    public function get task_end_time() : String
    {
        return _internal_task_end_time;
    }

    [Bindable(event="propertyChange")]
    public function get task_isVisited() : int
    {
        return _internal_task_isVisited;
    }

    [Bindable(event="propertyChange")]
    public function get distask_comment() : String
    {
        return _internal_distask_comment;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_fname() : String
    {
        return _internal_doctor_fname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lname() : String
    {
        return _internal_doctor_lname;
    }

    [Bindable(event="propertyChange")]
    public function get disptype_id() : String
    {
        return _internal_disptype_id;
    }

    [Bindable(event="propertyChange")]
    public function get disptype_label() : String
    {
        return _internal_disptype_label;
    }

    [Bindable(event="propertyChange")]
    public function get disptype_color() : int
    {
        return _internal_disptype_color;
    }

    [Bindable(event="propertyChange")]
    public function get smsstatus() : Boolean
    {
        return _internal_smsstatus;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set addiotions_Anamnes(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_addiotions_Anamnes;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_addiotions_Anamnes = value;
            }
            else if (value is Array)
            {
                _internal_addiotions_Anamnes = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_addiotions_Anamnes = null;
            }
            else
            {
                throw new Error("value of addiotions_Anamnes must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "addiotions_Anamnes", oldValue, _internal_addiotions_Anamnes));
        }
    }

    public function set addiotions_Xray(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_addiotions_Xray;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_addiotions_Xray = value;
            }
            else if (value is Array)
            {
                _internal_addiotions_Xray = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_addiotions_Xray = null;
            }
            else
            {
                throw new Error("value of addiotions_Xray must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "addiotions_Xray", oldValue, _internal_addiotions_Xray));
        }
    }

    public function set distask_id(value:String) : void
    {
        var oldValue:String = _internal_distask_id;
        if (oldValue !== value)
        {
            _internal_distask_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "distask_id", oldValue, _internal_distask_id));
        }
    }

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set patient_fname(value:String) : void
    {
        var oldValue:String = _internal_patient_fname;
        if (oldValue !== value)
        {
            _internal_patient_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_fname", oldValue, _internal_patient_fname));
        }
    }

    public function set patient_lname(value:String) : void
    {
        var oldValue:String = _internal_patient_lname;
        if (oldValue !== value)
        {
            _internal_patient_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_lname", oldValue, _internal_patient_lname));
        }
    }

    public function set patient_sname(value:String) : void
    {
        var oldValue:String = _internal_patient_sname;
        if (oldValue !== value)
        {
            _internal_patient_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_sname", oldValue, _internal_patient_sname));
        }
    }

    public function set patient_cardnum(value:String) : void
    {
        var oldValue:String = _internal_patient_cardnum;
        if (oldValue !== value)
        {
            _internal_patient_cardnum = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_cardnum", oldValue, _internal_patient_cardnum));
        }
    }

    public function set patient_mobiles(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_patient_mobiles;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_patient_mobiles = value;
            }
            else if (value is Array)
            {
                _internal_patient_mobiles = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_patient_mobiles = null;
            }
            else
            {
                throw new Error("value of patient_mobiles must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_mobiles", oldValue, _internal_patient_mobiles));
        }
    }

    public function set distask_date(value:String) : void
    {
        var oldValue:String = _internal_distask_date;
        if (oldValue !== value)
        {
            _internal_distask_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "distask_date", oldValue, _internal_distask_date));
        }
    }

    public function set task_id(value:String) : void
    {
        var oldValue:String = _internal_task_id;
        if (oldValue !== value)
        {
            _internal_task_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_id", oldValue, _internal_task_id));
        }
    }

    public function set task_date(value:String) : void
    {
        var oldValue:String = _internal_task_date;
        if (oldValue !== value)
        {
            _internal_task_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_date", oldValue, _internal_task_date));
        }
    }

    public function set task_begin_time(value:String) : void
    {
        var oldValue:String = _internal_task_begin_time;
        if (oldValue !== value)
        {
            _internal_task_begin_time = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_begin_time", oldValue, _internal_task_begin_time));
        }
    }

    public function set task_end_time(value:String) : void
    {
        var oldValue:String = _internal_task_end_time;
        if (oldValue !== value)
        {
            _internal_task_end_time = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_end_time", oldValue, _internal_task_end_time));
        }
    }

    public function set task_isVisited(value:int) : void
    {
        var oldValue:int = _internal_task_isVisited;
        if (oldValue !== value)
        {
            _internal_task_isVisited = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_isVisited", oldValue, _internal_task_isVisited));
        }
    }

    public function set distask_comment(value:String) : void
    {
        var oldValue:String = _internal_distask_comment;
        if (oldValue !== value)
        {
            _internal_distask_comment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "distask_comment", oldValue, _internal_distask_comment));
        }
    }

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_fname(value:String) : void
    {
        var oldValue:String = _internal_doctor_fname;
        if (oldValue !== value)
        {
            _internal_doctor_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_fname", oldValue, _internal_doctor_fname));
        }
    }

    public function set doctor_lname(value:String) : void
    {
        var oldValue:String = _internal_doctor_lname;
        if (oldValue !== value)
        {
            _internal_doctor_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lname", oldValue, _internal_doctor_lname));
        }
    }

    public function set disptype_id(value:String) : void
    {
        var oldValue:String = _internal_disptype_id;
        if (oldValue !== value)
        {
            _internal_disptype_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "disptype_id", oldValue, _internal_disptype_id));
        }
    }

    public function set disptype_label(value:String) : void
    {
        var oldValue:String = _internal_disptype_label;
        if (oldValue !== value)
        {
            _internal_disptype_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "disptype_label", oldValue, _internal_disptype_label));
        }
    }

    public function set disptype_color(value:int) : void
    {
        var oldValue:int = _internal_disptype_color;
        if (oldValue !== value)
        {
            _internal_disptype_color = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "disptype_color", oldValue, _internal_disptype_color));
        }
    }

    public function set smsstatus(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_smsstatus;
        if (oldValue !== value)
        {
            _internal_smsstatus = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "smsstatus", oldValue, _internal_smsstatus));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _DispanserModule_TaskEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _DispanserModule_TaskEntityMetadata) : void
    {
        var oldValue : _DispanserModule_TaskEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
