
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _AdminModuleAutorizeObjectEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("EH_IS_ACCESS_TOKEN_EXPIRY", "ACCOUNT_ID", "ACCOUNT_ACCESS", "ACCOUNT_USER", "ACCOUNT_PASSWORD", "ACCOUNT_KEY", "ACCOUNT_CHECK", "ACCOUNT_LASTNAME", "ACCOUNT_FIRSTNAME", "ACCOUNT_MIDDLENAME", "ACCOUNT_SHORTNAME", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "SPEC", "SUBDIVISIONID", "SUBDIVISIONNAME", "SUBDIVISIONCITY", "LOGO", "ACCOUNT_CASHIERCODE", "INFO2", "ACCOUNTTYPE", "TAX_ID", "BIRTH_DATE", "BIRTH_PLACE", "GENDER", "EMAIL", "USER_POSITION", "PHONE_TYPE", "PHONE", "DOC_TYPE", "DOC_NUMBER", "IS_OWNER", "EH_ACCESS_TOKEN", "EH_ACCESS_TOKEN_EXPIRY", "EH_SECRET_KEY", "OA_DIR", "EH_LEGALENTITY_ID", "SCOPE", "EH_STARTDATE", "EH_ENDDATE", "EH_USER_ID");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("EH_IS_ACCESS_TOKEN_EXPIRY", "ACCOUNT_ID", "ACCOUNT_ACCESS", "ACCOUNT_USER", "ACCOUNT_PASSWORD", "ACCOUNT_KEY", "ACCOUNT_CHECK", "ACCOUNT_LASTNAME", "ACCOUNT_FIRSTNAME", "ACCOUNT_MIDDLENAME", "ACCOUNT_SHORTNAME", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "SPEC", "SUBDIVISIONID", "SUBDIVISIONNAME", "SUBDIVISIONCITY", "LOGO", "ACCOUNT_CASHIERCODE", "INFO2", "ACCOUNTTYPE", "TAX_ID", "BIRTH_DATE", "BIRTH_PLACE", "GENDER", "EMAIL", "USER_POSITION", "PHONE_TYPE", "PHONE", "DOC_TYPE", "DOC_NUMBER", "IS_OWNER", "EH_ACCESS_TOKEN", "EH_ACCESS_TOKEN_EXPIRY", "EH_SECRET_KEY", "OA_DIR", "EH_LEGALENTITY_ID", "SCOPE", "EH_STARTDATE", "EH_ENDDATE", "EH_USER_ID");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("EH_IS_ACCESS_TOKEN_EXPIRY", "ACCOUNT_ID", "ACCOUNT_ACCESS", "ACCOUNT_USER", "ACCOUNT_PASSWORD", "ACCOUNT_KEY", "ACCOUNT_CHECK", "ACCOUNT_LASTNAME", "ACCOUNT_FIRSTNAME", "ACCOUNT_MIDDLENAME", "ACCOUNT_SHORTNAME", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "SPEC", "SUBDIVISIONID", "SUBDIVISIONNAME", "SUBDIVISIONCITY", "LOGO", "ACCOUNT_CASHIERCODE", "INFO2", "ACCOUNTTYPE", "TAX_ID", "BIRTH_DATE", "BIRTH_PLACE", "GENDER", "EMAIL", "USER_POSITION", "PHONE_TYPE", "PHONE", "DOC_TYPE", "DOC_NUMBER", "IS_OWNER", "EH_ACCESS_TOKEN", "EH_ACCESS_TOKEN_EXPIRY", "EH_SECRET_KEY", "OA_DIR", "EH_LEGALENTITY_ID", "SCOPE", "EH_STARTDATE", "EH_ENDDATE", "EH_USER_ID");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("EH_IS_ACCESS_TOKEN_EXPIRY", "ACCOUNT_ID", "ACCOUNT_ACCESS", "ACCOUNT_USER", "ACCOUNT_PASSWORD", "ACCOUNT_KEY", "ACCOUNT_CHECK", "ACCOUNT_LASTNAME", "ACCOUNT_FIRSTNAME", "ACCOUNT_MIDDLENAME", "ACCOUNT_SHORTNAME", "DOCTOR_ID", "DOCTOR_SHORTNAME", "DOCTOR_SPECIALITY", "SPEC", "SUBDIVISIONID", "SUBDIVISIONNAME", "SUBDIVISIONCITY", "LOGO", "ACCOUNT_CASHIERCODE", "INFO2", "ACCOUNTTYPE", "TAX_ID", "BIRTH_DATE", "BIRTH_PLACE", "GENDER", "EMAIL", "USER_POSITION", "PHONE_TYPE", "PHONE", "DOC_TYPE", "DOC_NUMBER", "IS_OWNER", "EH_ACCESS_TOKEN", "EH_ACCESS_TOKEN_EXPIRY", "EH_SECRET_KEY", "OA_DIR", "EH_LEGALENTITY_ID", "SCOPE", "EH_STARTDATE", "EH_ENDDATE", "EH_USER_ID");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "AdminModuleAutorizeObject";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_AdminModuleAutorizeObject;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _AdminModuleAutorizeObjectEntityMetadata(value : _Super_AdminModuleAutorizeObject)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["EH_IS_ACCESS_TOKEN_EXPIRY"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_ID"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_ACCESS"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_USER"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_PASSWORD"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_KEY"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_CHECK"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_LASTNAME"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_FIRSTNAME"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_MIDDLENAME"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_SHORTNAME"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_ID"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_SHORTNAME"] = new Array();
            model_internal::dependentsOnMap["DOCTOR_SPECIALITY"] = new Array();
            model_internal::dependentsOnMap["SPEC"] = new Array();
            model_internal::dependentsOnMap["SUBDIVISIONID"] = new Array();
            model_internal::dependentsOnMap["SUBDIVISIONNAME"] = new Array();
            model_internal::dependentsOnMap["SUBDIVISIONCITY"] = new Array();
            model_internal::dependentsOnMap["LOGO"] = new Array();
            model_internal::dependentsOnMap["ACCOUNT_CASHIERCODE"] = new Array();
            model_internal::dependentsOnMap["INFO2"] = new Array();
            model_internal::dependentsOnMap["ACCOUNTTYPE"] = new Array();
            model_internal::dependentsOnMap["TAX_ID"] = new Array();
            model_internal::dependentsOnMap["BIRTH_DATE"] = new Array();
            model_internal::dependentsOnMap["BIRTH_PLACE"] = new Array();
            model_internal::dependentsOnMap["GENDER"] = new Array();
            model_internal::dependentsOnMap["EMAIL"] = new Array();
            model_internal::dependentsOnMap["USER_POSITION"] = new Array();
            model_internal::dependentsOnMap["PHONE_TYPE"] = new Array();
            model_internal::dependentsOnMap["PHONE"] = new Array();
            model_internal::dependentsOnMap["DOC_TYPE"] = new Array();
            model_internal::dependentsOnMap["DOC_NUMBER"] = new Array();
            model_internal::dependentsOnMap["IS_OWNER"] = new Array();
            model_internal::dependentsOnMap["EH_ACCESS_TOKEN"] = new Array();
            model_internal::dependentsOnMap["EH_ACCESS_TOKEN_EXPIRY"] = new Array();
            model_internal::dependentsOnMap["EH_SECRET_KEY"] = new Array();
            model_internal::dependentsOnMap["OA_DIR"] = new Array();
            model_internal::dependentsOnMap["EH_LEGALENTITY_ID"] = new Array();
            model_internal::dependentsOnMap["SCOPE"] = new Array();
            model_internal::dependentsOnMap["EH_STARTDATE"] = new Array();
            model_internal::dependentsOnMap["EH_ENDDATE"] = new Array();
            model_internal::dependentsOnMap["EH_USER_ID"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["EH_IS_ACCESS_TOKEN_EXPIRY"] = "Boolean";
        model_internal::propertyTypeMap["ACCOUNT_ID"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_ACCESS"] = "Boolean";
        model_internal::propertyTypeMap["ACCOUNT_USER"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_PASSWORD"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_KEY"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_CHECK"] = "int";
        model_internal::propertyTypeMap["ACCOUNT_LASTNAME"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_FIRSTNAME"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_MIDDLENAME"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_SHORTNAME"] = "String";
        model_internal::propertyTypeMap["DOCTOR_ID"] = "String";
        model_internal::propertyTypeMap["DOCTOR_SHORTNAME"] = "String";
        model_internal::propertyTypeMap["DOCTOR_SPECIALITY"] = "String";
        model_internal::propertyTypeMap["SPEC"] = "int";
        model_internal::propertyTypeMap["SUBDIVISIONID"] = "String";
        model_internal::propertyTypeMap["SUBDIVISIONNAME"] = "String";
        model_internal::propertyTypeMap["SUBDIVISIONCITY"] = "String";
        model_internal::propertyTypeMap["LOGO"] = "String";
        model_internal::propertyTypeMap["ACCOUNT_CASHIERCODE"] = "String";
        model_internal::propertyTypeMap["INFO2"] = "String";
        model_internal::propertyTypeMap["ACCOUNTTYPE"] = "String";
        model_internal::propertyTypeMap["TAX_ID"] = "String";
        model_internal::propertyTypeMap["BIRTH_DATE"] = "String";
        model_internal::propertyTypeMap["BIRTH_PLACE"] = "String";
        model_internal::propertyTypeMap["GENDER"] = "int";
        model_internal::propertyTypeMap["EMAIL"] = "String";
        model_internal::propertyTypeMap["USER_POSITION"] = "String";
        model_internal::propertyTypeMap["PHONE_TYPE"] = "String";
        model_internal::propertyTypeMap["PHONE"] = "String";
        model_internal::propertyTypeMap["DOC_TYPE"] = "String";
        model_internal::propertyTypeMap["DOC_NUMBER"] = "String";
        model_internal::propertyTypeMap["IS_OWNER"] = "Boolean";
        model_internal::propertyTypeMap["EH_ACCESS_TOKEN"] = "String";
        model_internal::propertyTypeMap["EH_ACCESS_TOKEN_EXPIRY"] = "String";
        model_internal::propertyTypeMap["EH_SECRET_KEY"] = "String";
        model_internal::propertyTypeMap["OA_DIR"] = "String";
        model_internal::propertyTypeMap["EH_LEGALENTITY_ID"] = "String";
        model_internal::propertyTypeMap["SCOPE"] = "String";
        model_internal::propertyTypeMap["EH_STARTDATE"] = "String";
        model_internal::propertyTypeMap["EH_ENDDATE"] = "String";
        model_internal::propertyTypeMap["EH_USER_ID"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity AdminModuleAutorizeObject");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity AdminModuleAutorizeObject");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of AdminModuleAutorizeObject");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AdminModuleAutorizeObject");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity AdminModuleAutorizeObject");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity AdminModuleAutorizeObject");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isEH_IS_ACCESS_TOKEN_EXPIRYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_ACCESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_USERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_PASSWORDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_KEYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_CHECKAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_LASTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_FIRSTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_MIDDLENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_SHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_SHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTOR_SPECIALITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSPECAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUBDIVISIONIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUBDIVISIONNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSUBDIVISIONCITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLOGOAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNT_CASHIERCODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINFO2Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isACCOUNTTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTAX_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBIRTH_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBIRTH_PLACEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGENDERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEMAILAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isUSER_POSITIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPHONE_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPHONEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOC_TYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOC_NUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIS_OWNERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_ACCESS_TOKENAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_ACCESS_TOKEN_EXPIRYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_SECRET_KEYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOA_DIRAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_LEGALENTITY_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSCOPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_STARTDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_ENDDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_USER_IDAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get EH_IS_ACCESS_TOKEN_EXPIRYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_ACCESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_USERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_PASSWORDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_KEYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_CHECKStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_LASTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_FIRSTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_MIDDLENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_SHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_SHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTOR_SPECIALITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SPECStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUBDIVISIONIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUBDIVISIONNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SUBDIVISIONCITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LOGOStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNT_CASHIERCODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INFO2Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ACCOUNTTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TAX_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BIRTH_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BIRTH_PLACEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get GENDERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EMAILStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get USER_POSITIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PHONE_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PHONEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOC_TYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOC_NUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get IS_OWNERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_ACCESS_TOKENStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_ACCESS_TOKEN_EXPIRYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_SECRET_KEYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get OA_DIRStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_LEGALENTITY_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SCOPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_STARTDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_ENDDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_USER_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
