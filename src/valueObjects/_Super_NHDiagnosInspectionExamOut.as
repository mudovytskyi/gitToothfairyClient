/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - NHDiagnosInspectionExamOut.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.NHDiagnosProtocol;
import valueObjects.NHDiagnos_FarmReceptionMethod;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_NHDiagnosInspectionExamOut extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("NHDiagnosInspectionExamOut") == null)
            {
                flash.net.registerClassAlias("NHDiagnosInspectionExamOut", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("NHDiagnosInspectionExamOut", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.NHDiagnosProtocol.initRemoteClassAliasSingleChild();
        valueObjects.NHDiagnosTemplate.initRemoteClassAliasSingleChild();
        valueObjects.NHDiagnos_FarmReceptionMethod.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _NHDiagnosInspectionExamOutEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_nh_diagnos : String;
    private var _internal_examid : int;
    private var _internal_examdate : String;
    private var _internal_doctor : String;
    private var _internal_docid : int;
    private var _internal_afterflag : int;
    private var _internal_course_fk : String;
    private var _internal_course_date : String;
    private var _internal_diseasehistory : String;
    private var _internal_treatmentplan : String;
    private var _internal_examplan : String;
    private var _internal_selectedProtocols : ArrayCollection;
    model_internal var _internal_selectedProtocols_leaf:valueObjects.NHDiagnosProtocol;
    private var _internal_complaints : String;
    private var _internal_anamnesis : String;
    private var _internal_status : String;
    private var _internal_recommendations : String;
    private var _internal_checkup : String;
    private var _internal_temperature : Number;
    private var _internal_height : Number;
    private var _internal_weight : Number;
    private var _internal_systolic_pressure : int;
    private var _internal_diastolic_pressure : int;
    private var _internal_farmReceptionMethods : ArrayCollection;
    model_internal var _internal_farmReceptionMethods_leaf:valueObjects.NHDiagnos_FarmReceptionMethod;
    private var _internal_checkupID : String;
    private var _internal_checkupData : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_NHDiagnosInspectionExamOut()
    {
        _model = new _NHDiagnosInspectionExamOutEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get nh_diagnos() : String
    {
        return _internal_nh_diagnos;
    }

    [Bindable(event="propertyChange")]
    public function get examid() : int
    {
        return _internal_examid;
    }

    [Bindable(event="propertyChange")]
    public function get examdate() : String
    {
        return _internal_examdate;
    }

    [Bindable(event="propertyChange")]
    public function get doctor() : String
    {
        return _internal_doctor;
    }

    [Bindable(event="propertyChange")]
    public function get docid() : int
    {
        return _internal_docid;
    }

    [Bindable(event="propertyChange")]
    public function get afterflag() : int
    {
        return _internal_afterflag;
    }

    [Bindable(event="propertyChange")]
    public function get course_fk() : String
    {
        return _internal_course_fk;
    }

    [Bindable(event="propertyChange")]
    public function get course_date() : String
    {
        return _internal_course_date;
    }

    [Bindable(event="propertyChange")]
    public function get diseasehistory() : String
    {
        return _internal_diseasehistory;
    }

    [Bindable(event="propertyChange")]
    public function get treatmentplan() : String
    {
        return _internal_treatmentplan;
    }

    [Bindable(event="propertyChange")]
    public function get examplan() : String
    {
        return _internal_examplan;
    }

    [Bindable(event="propertyChange")]
    public function get selectedProtocols() : ArrayCollection
    {
        return _internal_selectedProtocols;
    }

    [Bindable(event="propertyChange")]
    public function get complaints() : String
    {
        return _internal_complaints;
    }

    [Bindable(event="propertyChange")]
    public function get anamnesis() : String
    {
        return _internal_anamnesis;
    }

    [Bindable(event="propertyChange")]
    public function get status() : String
    {
        return _internal_status;
    }

    [Bindable(event="propertyChange")]
    public function get recommendations() : String
    {
        return _internal_recommendations;
    }

    [Bindable(event="propertyChange")]
    public function get checkup() : String
    {
        return _internal_checkup;
    }

    [Bindable(event="propertyChange")]
    public function get temperature() : Number
    {
        return _internal_temperature;
    }

    [Bindable(event="propertyChange")]
    public function get height() : Number
    {
        return _internal_height;
    }

    [Bindable(event="propertyChange")]
    public function get weight() : Number
    {
        return _internal_weight;
    }

    [Bindable(event="propertyChange")]
    public function get systolic_pressure() : int
    {
        return _internal_systolic_pressure;
    }

    [Bindable(event="propertyChange")]
    public function get diastolic_pressure() : int
    {
        return _internal_diastolic_pressure;
    }

    [Bindable(event="propertyChange")]
    public function get farmReceptionMethods() : ArrayCollection
    {
        return _internal_farmReceptionMethods;
    }

    [Bindable(event="propertyChange")]
    public function get checkupID() : String
    {
        return _internal_checkupID;
    }

    [Bindable(event="propertyChange")]
    public function get checkupData() : String
    {
        return _internal_checkupData;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set nh_diagnos(value:String) : void
    {
        var oldValue:String = _internal_nh_diagnos;
        if (oldValue !== value)
        {
            _internal_nh_diagnos = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "nh_diagnos", oldValue, _internal_nh_diagnos));
        }
    }

    public function set examid(value:int) : void
    {
        var oldValue:int = _internal_examid;
        if (oldValue !== value)
        {
            _internal_examid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examid", oldValue, _internal_examid));
        }
    }

    public function set examdate(value:String) : void
    {
        var oldValue:String = _internal_examdate;
        if (oldValue !== value)
        {
            _internal_examdate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examdate", oldValue, _internal_examdate));
        }
    }

    public function set doctor(value:String) : void
    {
        var oldValue:String = _internal_doctor;
        if (oldValue !== value)
        {
            _internal_doctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor", oldValue, _internal_doctor));
        }
    }

    public function set docid(value:int) : void
    {
        var oldValue:int = _internal_docid;
        if (oldValue !== value)
        {
            _internal_docid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "docid", oldValue, _internal_docid));
        }
    }

    public function set afterflag(value:int) : void
    {
        var oldValue:int = _internal_afterflag;
        if (oldValue !== value)
        {
            _internal_afterflag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "afterflag", oldValue, _internal_afterflag));
        }
    }

    public function set course_fk(value:String) : void
    {
        var oldValue:String = _internal_course_fk;
        if (oldValue !== value)
        {
            _internal_course_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "course_fk", oldValue, _internal_course_fk));
        }
    }

    public function set course_date(value:String) : void
    {
        var oldValue:String = _internal_course_date;
        if (oldValue !== value)
        {
            _internal_course_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "course_date", oldValue, _internal_course_date));
        }
    }

    public function set diseasehistory(value:String) : void
    {
        var oldValue:String = _internal_diseasehistory;
        if (oldValue !== value)
        {
            _internal_diseasehistory = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diseasehistory", oldValue, _internal_diseasehistory));
        }
    }

    public function set treatmentplan(value:String) : void
    {
        var oldValue:String = _internal_treatmentplan;
        if (oldValue !== value)
        {
            _internal_treatmentplan = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatmentplan", oldValue, _internal_treatmentplan));
        }
    }

    public function set examplan(value:String) : void
    {
        var oldValue:String = _internal_examplan;
        if (oldValue !== value)
        {
            _internal_examplan = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "examplan", oldValue, _internal_examplan));
        }
    }

    public function set selectedProtocols(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_selectedProtocols;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_selectedProtocols = value;
            }
            else if (value is Array)
            {
                _internal_selectedProtocols = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_selectedProtocols = null;
            }
            else
            {
                throw new Error("value of selectedProtocols must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "selectedProtocols", oldValue, _internal_selectedProtocols));
        }
    }

    public function set complaints(value:String) : void
    {
        var oldValue:String = _internal_complaints;
        if (oldValue !== value)
        {
            _internal_complaints = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "complaints", oldValue, _internal_complaints));
        }
    }

    public function set anamnesis(value:String) : void
    {
        var oldValue:String = _internal_anamnesis;
        if (oldValue !== value)
        {
            _internal_anamnesis = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "anamnesis", oldValue, _internal_anamnesis));
        }
    }

    public function set status(value:String) : void
    {
        var oldValue:String = _internal_status;
        if (oldValue !== value)
        {
            _internal_status = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "status", oldValue, _internal_status));
        }
    }

    public function set recommendations(value:String) : void
    {
        var oldValue:String = _internal_recommendations;
        if (oldValue !== value)
        {
            _internal_recommendations = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "recommendations", oldValue, _internal_recommendations));
        }
    }

    public function set checkup(value:String) : void
    {
        var oldValue:String = _internal_checkup;
        if (oldValue !== value)
        {
            _internal_checkup = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "checkup", oldValue, _internal_checkup));
        }
    }

    public function set temperature(value:Number) : void
    {
        var oldValue:Number = _internal_temperature;
        if (isNaN(_internal_temperature) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_temperature = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "temperature", oldValue, _internal_temperature));
        }
    }

    public function set height(value:Number) : void
    {
        var oldValue:Number = _internal_height;
        if (isNaN(_internal_height) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_height = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "height", oldValue, _internal_height));
        }
    }

    public function set weight(value:Number) : void
    {
        var oldValue:Number = _internal_weight;
        if (isNaN(_internal_weight) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_weight = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "weight", oldValue, _internal_weight));
        }
    }

    public function set systolic_pressure(value:int) : void
    {
        var oldValue:int = _internal_systolic_pressure;
        if (oldValue !== value)
        {
            _internal_systolic_pressure = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "systolic_pressure", oldValue, _internal_systolic_pressure));
        }
    }

    public function set diastolic_pressure(value:int) : void
    {
        var oldValue:int = _internal_diastolic_pressure;
        if (oldValue !== value)
        {
            _internal_diastolic_pressure = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diastolic_pressure", oldValue, _internal_diastolic_pressure));
        }
    }

    public function set farmReceptionMethods(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_farmReceptionMethods;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_farmReceptionMethods = value;
            }
            else if (value is Array)
            {
                _internal_farmReceptionMethods = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_farmReceptionMethods = null;
            }
            else
            {
                throw new Error("value of farmReceptionMethods must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmReceptionMethods", oldValue, _internal_farmReceptionMethods));
        }
    }

    public function set checkupID(value:String) : void
    {
        var oldValue:String = _internal_checkupID;
        if (oldValue !== value)
        {
            _internal_checkupID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "checkupID", oldValue, _internal_checkupID));
        }
    }

    public function set checkupData(value:String) : void
    {
        var oldValue:String = _internal_checkupData;
        if (oldValue !== value)
        {
            _internal_checkupData = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "checkupData", oldValue, _internal_checkupData));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _NHDiagnosInspectionExamOutEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _NHDiagnosInspectionExamOutEntityMetadata) : void
    {
        var oldValue : _NHDiagnosInspectionExamOutEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
