/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PatientCardModulePatientTasksHistoryObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PatientCardModulePatientTasksHistoryObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PatientCardModulePatientTasksHistoryObject") == null)
            {
                flash.net.registerClassAlias("PatientCardModulePatientTasksHistoryObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PatientCardModulePatientTasksHistoryObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _PatientCardModulePatientTasksHistoryObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_TaskID : String;
    private var _internal_TaskDATE : String;
    private var _internal_TaskBEGINOFTHEINTERVAL : String;
    private var _internal_TaskENDOFTHEINTERVAL : String;
    private var _internal_TaskNOTICED : String;
    private var _internal_TaskFACTOFVISIT : String;
    private var _internal_DoctorSHORTNAME : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PatientCardModulePatientTasksHistoryObject()
    {
        _model = new _PatientCardModulePatientTasksHistoryObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get TaskID() : String
    {
        return _internal_TaskID;
    }

    [Bindable(event="propertyChange")]
    public function get TaskDATE() : String
    {
        return _internal_TaskDATE;
    }

    [Bindable(event="propertyChange")]
    public function get TaskBEGINOFTHEINTERVAL() : String
    {
        return _internal_TaskBEGINOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get TaskENDOFTHEINTERVAL() : String
    {
        return _internal_TaskENDOFTHEINTERVAL;
    }

    [Bindable(event="propertyChange")]
    public function get TaskNOTICED() : String
    {
        return _internal_TaskNOTICED;
    }

    [Bindable(event="propertyChange")]
    public function get TaskFACTOFVISIT() : String
    {
        return _internal_TaskFACTOFVISIT;
    }

    [Bindable(event="propertyChange")]
    public function get DoctorSHORTNAME() : String
    {
        return _internal_DoctorSHORTNAME;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set TaskID(value:String) : void
    {
        var oldValue:String = _internal_TaskID;
        if (oldValue !== value)
        {
            _internal_TaskID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskID", oldValue, _internal_TaskID));
        }
    }

    public function set TaskDATE(value:String) : void
    {
        var oldValue:String = _internal_TaskDATE;
        if (oldValue !== value)
        {
            _internal_TaskDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskDATE", oldValue, _internal_TaskDATE));
        }
    }

    public function set TaskBEGINOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TaskBEGINOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TaskBEGINOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskBEGINOFTHEINTERVAL", oldValue, _internal_TaskBEGINOFTHEINTERVAL));
        }
    }

    public function set TaskENDOFTHEINTERVAL(value:String) : void
    {
        var oldValue:String = _internal_TaskENDOFTHEINTERVAL;
        if (oldValue !== value)
        {
            _internal_TaskENDOFTHEINTERVAL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskENDOFTHEINTERVAL", oldValue, _internal_TaskENDOFTHEINTERVAL));
        }
    }

    public function set TaskNOTICED(value:String) : void
    {
        var oldValue:String = _internal_TaskNOTICED;
        if (oldValue !== value)
        {
            _internal_TaskNOTICED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskNOTICED", oldValue, _internal_TaskNOTICED));
        }
    }

    public function set TaskFACTOFVISIT(value:String) : void
    {
        var oldValue:String = _internal_TaskFACTOFVISIT;
        if (oldValue !== value)
        {
            _internal_TaskFACTOFVISIT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TaskFACTOFVISIT", oldValue, _internal_TaskFACTOFVISIT));
        }
    }

    public function set DoctorSHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_DoctorSHORTNAME;
        if (oldValue !== value)
        {
            _internal_DoctorSHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DoctorSHORTNAME", oldValue, _internal_DoctorSHORTNAME));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PatientCardModulePatientTasksHistoryObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PatientCardModulePatientTasksHistoryObjectEntityMetadata) : void
    {
        var oldValue : _PatientCardModulePatientTasksHistoryObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
