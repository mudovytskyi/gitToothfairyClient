/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AdminModuleJournalRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AdminModuleJournalRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AdminModuleJournalRecord") == null)
            {
                flash.net.registerClassAlias("AdminModuleJournalRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AdminModuleJournalRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AdminModuleJournalRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_ACCOUNTID : String;
    private var _internal_TABLENAME : String;
    private var _internal_FIELDNAME : String;
    private var _internal_RECORDID : String;
    private var _internal_OLDVALUE : String;
    private var _internal_NEWVALUE : String;
    private var _internal_DATETIME : String;
    private var _internal_USER : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AdminModuleJournalRecord()
    {
        _model = new _AdminModuleJournalRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get ACCOUNTID() : String
    {
        return _internal_ACCOUNTID;
    }

    [Bindable(event="propertyChange")]
    public function get TABLENAME() : String
    {
        return _internal_TABLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get FIELDNAME() : String
    {
        return _internal_FIELDNAME;
    }

    [Bindable(event="propertyChange")]
    public function get RECORDID() : String
    {
        return _internal_RECORDID;
    }

    [Bindable(event="propertyChange")]
    public function get OLDVALUE() : String
    {
        return _internal_OLDVALUE;
    }

    [Bindable(event="propertyChange")]
    public function get NEWVALUE() : String
    {
        return _internal_NEWVALUE;
    }

    [Bindable(event="propertyChange")]
    public function get DATETIME() : String
    {
        return _internal_DATETIME;
    }

    [Bindable(event="propertyChange")]
    public function get USER() : String
    {
        return _internal_USER;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set ACCOUNTID(value:String) : void
    {
        var oldValue:String = _internal_ACCOUNTID;
        if (oldValue !== value)
        {
            _internal_ACCOUNTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ACCOUNTID", oldValue, _internal_ACCOUNTID));
        }
    }

    public function set TABLENAME(value:String) : void
    {
        var oldValue:String = _internal_TABLENAME;
        if (oldValue !== value)
        {
            _internal_TABLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TABLENAME", oldValue, _internal_TABLENAME));
        }
    }

    public function set FIELDNAME(value:String) : void
    {
        var oldValue:String = _internal_FIELDNAME;
        if (oldValue !== value)
        {
            _internal_FIELDNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FIELDNAME", oldValue, _internal_FIELDNAME));
        }
    }

    public function set RECORDID(value:String) : void
    {
        var oldValue:String = _internal_RECORDID;
        if (oldValue !== value)
        {
            _internal_RECORDID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RECORDID", oldValue, _internal_RECORDID));
        }
    }

    public function set OLDVALUE(value:String) : void
    {
        var oldValue:String = _internal_OLDVALUE;
        if (oldValue !== value)
        {
            _internal_OLDVALUE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OLDVALUE", oldValue, _internal_OLDVALUE));
        }
    }

    public function set NEWVALUE(value:String) : void
    {
        var oldValue:String = _internal_NEWVALUE;
        if (oldValue !== value)
        {
            _internal_NEWVALUE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NEWVALUE", oldValue, _internal_NEWVALUE));
        }
    }

    public function set DATETIME(value:String) : void
    {
        var oldValue:String = _internal_DATETIME;
        if (oldValue !== value)
        {
            _internal_DATETIME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATETIME", oldValue, _internal_DATETIME));
        }
    }

    public function set USER(value:String) : void
    {
        var oldValue:String = _internal_USER;
        if (oldValue !== value)
        {
            _internal_USER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "USER", oldValue, _internal_USER));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AdminModuleJournalRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AdminModuleJournalRecordEntityMetadata) : void
    {
        var oldValue : _AdminModuleJournalRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
