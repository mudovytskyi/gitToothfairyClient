/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefBudgetModule_BudgetData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.ChiefBudgetModule_BudgetData;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefBudgetModule_BudgetData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefBudgetModule_BudgetData") == null)
            {
                flash.net.registerClassAlias("ChiefBudgetModule_BudgetData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefBudgetModule_BudgetData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.ChiefBudgetModule_BudgetData.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _ChiefBudgetModule_BudgetDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_group_id : String;
    private var _internal_group_name : String;
    private var _internal_group_is_cashpayment : int;
    private var _internal_group_monthly_is : int;
    private var _internal_group_monthly_defaultsumm : Number;
    private var _internal_group_color : int;
    private var _internal_cash_expense : Number;
    private var _internal_cash_income : Number;
    private var _internal_noncash_income : Number;
    private var _internal_noncash_expense : Number;
    private var _internal_cash_summ : Number;
    private var _internal_noncash_summ : Number;
    private var _internal_summ : Number;
    private var _internal_income_summ : Number;
    private var _internal_expense_summ : Number;
    private var _internal_included : valueObjects.ChiefBudgetModule_BudgetData;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefBudgetModule_BudgetData()
    {
        _model = new _ChiefBudgetModule_BudgetDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get group_id() : String
    {
        return _internal_group_id;
    }

    [Bindable(event="propertyChange")]
    public function get group_name() : String
    {
        return _internal_group_name;
    }

    [Bindable(event="propertyChange")]
    public function get group_is_cashpayment() : int
    {
        return _internal_group_is_cashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get group_monthly_is() : int
    {
        return _internal_group_monthly_is;
    }

    [Bindable(event="propertyChange")]
    public function get group_monthly_defaultsumm() : Number
    {
        return _internal_group_monthly_defaultsumm;
    }

    [Bindable(event="propertyChange")]
    public function get group_color() : int
    {
        return _internal_group_color;
    }

    [Bindable(event="propertyChange")]
    public function get cash_expense() : Number
    {
        return _internal_cash_expense;
    }

    [Bindable(event="propertyChange")]
    public function get cash_income() : Number
    {
        return _internal_cash_income;
    }

    [Bindable(event="propertyChange")]
    public function get noncash_income() : Number
    {
        return _internal_noncash_income;
    }

    [Bindable(event="propertyChange")]
    public function get noncash_expense() : Number
    {
        return _internal_noncash_expense;
    }

    [Bindable(event="propertyChange")]
    public function get cash_summ() : Number
    {
        return _internal_cash_summ;
    }

    [Bindable(event="propertyChange")]
    public function get noncash_summ() : Number
    {
        return _internal_noncash_summ;
    }

    [Bindable(event="propertyChange")]
    public function get summ() : Number
    {
        return _internal_summ;
    }

    [Bindable(event="propertyChange")]
    public function get income_summ() : Number
    {
        return _internal_income_summ;
    }

    [Bindable(event="propertyChange")]
    public function get expense_summ() : Number
    {
        return _internal_expense_summ;
    }

    [Bindable(event="propertyChange")]
    public function get included() : valueObjects.ChiefBudgetModule_BudgetData
    {
        return _internal_included;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set group_id(value:String) : void
    {
        var oldValue:String = _internal_group_id;
        if (oldValue !== value)
        {
            _internal_group_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_id", oldValue, _internal_group_id));
        }
    }

    public function set group_name(value:String) : void
    {
        var oldValue:String = _internal_group_name;
        if (oldValue !== value)
        {
            _internal_group_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_name", oldValue, _internal_group_name));
        }
    }

    public function set group_is_cashpayment(value:int) : void
    {
        var oldValue:int = _internal_group_is_cashpayment;
        if (oldValue !== value)
        {
            _internal_group_is_cashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_is_cashpayment", oldValue, _internal_group_is_cashpayment));
        }
    }

    public function set group_monthly_is(value:int) : void
    {
        var oldValue:int = _internal_group_monthly_is;
        if (oldValue !== value)
        {
            _internal_group_monthly_is = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_monthly_is", oldValue, _internal_group_monthly_is));
        }
    }

    public function set group_monthly_defaultsumm(value:Number) : void
    {
        var oldValue:Number = _internal_group_monthly_defaultsumm;
        if (isNaN(_internal_group_monthly_defaultsumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_group_monthly_defaultsumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_monthly_defaultsumm", oldValue, _internal_group_monthly_defaultsumm));
        }
    }

    public function set group_color(value:int) : void
    {
        var oldValue:int = _internal_group_color;
        if (oldValue !== value)
        {
            _internal_group_color = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_color", oldValue, _internal_group_color));
        }
    }

    public function set cash_expense(value:Number) : void
    {
        var oldValue:Number = _internal_cash_expense;
        if (isNaN(_internal_cash_expense) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_cash_expense = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cash_expense", oldValue, _internal_cash_expense));
        }
    }

    public function set cash_income(value:Number) : void
    {
        var oldValue:Number = _internal_cash_income;
        if (isNaN(_internal_cash_income) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_cash_income = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cash_income", oldValue, _internal_cash_income));
        }
    }

    public function set noncash_income(value:Number) : void
    {
        var oldValue:Number = _internal_noncash_income;
        if (isNaN(_internal_noncash_income) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_noncash_income = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "noncash_income", oldValue, _internal_noncash_income));
        }
    }

    public function set noncash_expense(value:Number) : void
    {
        var oldValue:Number = _internal_noncash_expense;
        if (isNaN(_internal_noncash_expense) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_noncash_expense = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "noncash_expense", oldValue, _internal_noncash_expense));
        }
    }

    public function set cash_summ(value:Number) : void
    {
        var oldValue:Number = _internal_cash_summ;
        if (isNaN(_internal_cash_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_cash_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cash_summ", oldValue, _internal_cash_summ));
        }
    }

    public function set noncash_summ(value:Number) : void
    {
        var oldValue:Number = _internal_noncash_summ;
        if (isNaN(_internal_noncash_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_noncash_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "noncash_summ", oldValue, _internal_noncash_summ));
        }
    }

    public function set summ(value:Number) : void
    {
        var oldValue:Number = _internal_summ;
        if (isNaN(_internal_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "summ", oldValue, _internal_summ));
        }
    }

    public function set income_summ(value:Number) : void
    {
        var oldValue:Number = _internal_income_summ;
        if (isNaN(_internal_income_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_income_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "income_summ", oldValue, _internal_income_summ));
        }
    }

    public function set expense_summ(value:Number) : void
    {
        var oldValue:Number = _internal_expense_summ;
        if (isNaN(_internal_expense_summ) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_expense_summ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "expense_summ", oldValue, _internal_expense_summ));
        }
    }

    public function set included(value:valueObjects.ChiefBudgetModule_BudgetData) : void
    {
        var oldValue:valueObjects.ChiefBudgetModule_BudgetData = _internal_included;
        if (oldValue !== value)
        {
            _internal_included = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "included", oldValue, _internal_included));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefBudgetModule_BudgetDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefBudgetModule_BudgetDataEntityMetadata) : void
    {
        var oldValue : _ChiefBudgetModule_BudgetDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
