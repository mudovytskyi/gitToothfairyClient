/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - HealthPlanMKB10TreatmentDiagnos.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.HealthPlanMKB10Comment;
import valueObjects.HealthPlanMKB10Doctor;
import valueObjects.HealthPlanMKB10MKBProtocol;
import valueObjects.HealthPlanMKB10Template;
import valueObjects.HealthPlanMKB10TreatmentObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_HealthPlanMKB10TreatmentDiagnos extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("HealthPlanMKB10TreatmentDiagnos") == null)
            {
                flash.net.registerClassAlias("HealthPlanMKB10TreatmentDiagnos", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("HealthPlanMKB10TreatmentDiagnos", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.HealthPlanMKB10Template.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10MKBProtocol.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10MKB.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10F43.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Doctor.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Comment.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10TreatmentObject.initRemoteClassAliasSingleChild();
        valueObjects.HealthPlanMKB10Diagnos2.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _HealthPlanMKB10TreatmentDiagnosEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_Status : int;
    private var _internal_PlanSumma : Number;
    private var _internal_FactSumma : Number;
    private var _internal_BalanceSumma : Number;
    private var _internal_procedures_count : int;
    private var _internal_templates : ArrayCollection;
    model_internal var _internal_templates_leaf:valueObjects.HealthPlanMKB10Template;
    private var _internal_TOOTH_MIN : int;
    private var _internal_ID : int;
    private var _internal_f43 : int;
    private var _internal_EXAMID : int;
    private var _internal_protocol : valueObjects.HealthPlanMKB10MKBProtocol;
    private var _internal_Doctor : valueObjects.HealthPlanMKB10Doctor;
    private var _internal_DIAGNOSDATE : String;
    private var _internal_EXAMDATE : String;
    private var _internal_Template : valueObjects.HealthPlanMKB10Template;
    private var _internal_GARANTDATE : String;
    private var _internal_Protocols : ArrayCollection;
    model_internal var _internal_Protocols_leaf:valueObjects.HealthPlanMKB10MKBProtocol;
    private var _internal_DiagnosComments : ArrayCollection;
    model_internal var _internal_DiagnosComments_leaf:valueObjects.HealthPlanMKB10Comment;
    private var _internal_STACKNUMBER : int;
    private var _internal_selected : Boolean;
    private var _internal_TreatmentObjects : ArrayCollection;
    model_internal var _internal_TreatmentObjects_leaf:valueObjects.HealthPlanMKB10TreatmentObject;
    private var _internal_TREATMENTCOURSE : int;
    private var _internal_TEETH : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_HealthPlanMKB10TreatmentDiagnos()
    {
        _model = new _HealthPlanMKB10TreatmentDiagnosEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get Status() : int
    {
        return _internal_Status;
    }

    [Bindable(event="propertyChange")]
    public function get PlanSumma() : Number
    {
        return _internal_PlanSumma;
    }

    [Bindable(event="propertyChange")]
    public function get FactSumma() : Number
    {
        return _internal_FactSumma;
    }

    [Bindable(event="propertyChange")]
    public function get BalanceSumma() : Number
    {
        return _internal_BalanceSumma;
    }

    [Bindable(event="propertyChange")]
    public function get procedures_count() : int
    {
        return _internal_procedures_count;
    }

    [Bindable(event="propertyChange")]
    public function get templates() : ArrayCollection
    {
        return _internal_templates;
    }

    [Bindable(event="propertyChange")]
    public function get TOOTH_MIN() : int
    {
        return _internal_TOOTH_MIN;
    }

    [Bindable(event="propertyChange")]
    public function get ID() : int
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get f43() : int
    {
        return _internal_f43;
    }

    [Bindable(event="propertyChange")]
    public function get EXAMID() : int
    {
        return _internal_EXAMID;
    }

    [Bindable(event="propertyChange")]
    public function get protocol() : valueObjects.HealthPlanMKB10MKBProtocol
    {
        return _internal_protocol;
    }

    [Bindable(event="propertyChange")]
    public function get Doctor() : valueObjects.HealthPlanMKB10Doctor
    {
        return _internal_Doctor;
    }

    [Bindable(event="propertyChange")]
    public function get DIAGNOSDATE() : String
    {
        return _internal_DIAGNOSDATE;
    }

    [Bindable(event="propertyChange")]
    public function get EXAMDATE() : String
    {
        return _internal_EXAMDATE;
    }

    [Bindable(event="propertyChange")]
    public function get Template() : valueObjects.HealthPlanMKB10Template
    {
        return _internal_Template;
    }

    [Bindable(event="propertyChange")]
    public function get GARANTDATE() : String
    {
        return _internal_GARANTDATE;
    }

    [Bindable(event="propertyChange")]
    public function get Protocols() : ArrayCollection
    {
        return _internal_Protocols;
    }

    [Bindable(event="propertyChange")]
    public function get DiagnosComments() : ArrayCollection
    {
        return _internal_DiagnosComments;
    }

    [Bindable(event="propertyChange")]
    public function get STACKNUMBER() : int
    {
        return _internal_STACKNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get selected() : Boolean
    {
        return _internal_selected;
    }

    [Bindable(event="propertyChange")]
    public function get TreatmentObjects() : ArrayCollection
    {
        return _internal_TreatmentObjects;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTCOURSE() : int
    {
        return _internal_TREATMENTCOURSE;
    }

    [Bindable(event="propertyChange")]
    public function get TEETH() : String
    {
        return _internal_TEETH;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set Status(value:int) : void
    {
        var oldValue:int = _internal_Status;
        if (oldValue !== value)
        {
            _internal_Status = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Status", oldValue, _internal_Status));
        }
    }

    public function set PlanSumma(value:Number) : void
    {
        var oldValue:Number = _internal_PlanSumma;
        if (isNaN(_internal_PlanSumma) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_PlanSumma = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PlanSumma", oldValue, _internal_PlanSumma));
        }
    }

    public function set FactSumma(value:Number) : void
    {
        var oldValue:Number = _internal_FactSumma;
        if (isNaN(_internal_FactSumma) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_FactSumma = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FactSumma", oldValue, _internal_FactSumma));
        }
    }

    public function set BalanceSumma(value:Number) : void
    {
        var oldValue:Number = _internal_BalanceSumma;
        if (isNaN(_internal_BalanceSumma) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_BalanceSumma = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BalanceSumma", oldValue, _internal_BalanceSumma));
        }
    }

    public function set procedures_count(value:int) : void
    {
        var oldValue:int = _internal_procedures_count;
        if (oldValue !== value)
        {
            _internal_procedures_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "procedures_count", oldValue, _internal_procedures_count));
        }
    }

    public function set templates(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_templates;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_templates = value;
            }
            else if (value is Array)
            {
                _internal_templates = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_templates = null;
            }
            else
            {
                throw new Error("value of templates must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "templates", oldValue, _internal_templates));
        }
    }

    public function set TOOTH_MIN(value:int) : void
    {
        var oldValue:int = _internal_TOOTH_MIN;
        if (oldValue !== value)
        {
            _internal_TOOTH_MIN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TOOTH_MIN", oldValue, _internal_TOOTH_MIN));
        }
    }

    public function set ID(value:int) : void
    {
        var oldValue:int = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set f43(value:int) : void
    {
        var oldValue:int = _internal_f43;
        if (oldValue !== value)
        {
            _internal_f43 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "f43", oldValue, _internal_f43));
        }
    }

    public function set EXAMID(value:int) : void
    {
        var oldValue:int = _internal_EXAMID;
        if (oldValue !== value)
        {
            _internal_EXAMID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXAMID", oldValue, _internal_EXAMID));
        }
    }

    public function set protocol(value:valueObjects.HealthPlanMKB10MKBProtocol) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10MKBProtocol = _internal_protocol;
        if (oldValue !== value)
        {
            _internal_protocol = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "protocol", oldValue, _internal_protocol));
        }
    }

    public function set Doctor(value:valueObjects.HealthPlanMKB10Doctor) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10Doctor = _internal_Doctor;
        if (oldValue !== value)
        {
            _internal_Doctor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Doctor", oldValue, _internal_Doctor));
        }
    }

    public function set DIAGNOSDATE(value:String) : void
    {
        var oldValue:String = _internal_DIAGNOSDATE;
        if (oldValue !== value)
        {
            _internal_DIAGNOSDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DIAGNOSDATE", oldValue, _internal_DIAGNOSDATE));
        }
    }

    public function set EXAMDATE(value:String) : void
    {
        var oldValue:String = _internal_EXAMDATE;
        if (oldValue !== value)
        {
            _internal_EXAMDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXAMDATE", oldValue, _internal_EXAMDATE));
        }
    }

    public function set Template(value:valueObjects.HealthPlanMKB10Template) : void
    {
        var oldValue:valueObjects.HealthPlanMKB10Template = _internal_Template;
        if (oldValue !== value)
        {
            _internal_Template = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Template", oldValue, _internal_Template));
        }
    }

    public function set GARANTDATE(value:String) : void
    {
        var oldValue:String = _internal_GARANTDATE;
        if (oldValue !== value)
        {
            _internal_GARANTDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "GARANTDATE", oldValue, _internal_GARANTDATE));
        }
    }

    public function set Protocols(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_Protocols;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_Protocols = value;
            }
            else if (value is Array)
            {
                _internal_Protocols = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_Protocols = null;
            }
            else
            {
                throw new Error("value of Protocols must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Protocols", oldValue, _internal_Protocols));
        }
    }

    public function set DiagnosComments(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_DiagnosComments;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_DiagnosComments = value;
            }
            else if (value is Array)
            {
                _internal_DiagnosComments = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_DiagnosComments = null;
            }
            else
            {
                throw new Error("value of DiagnosComments must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DiagnosComments", oldValue, _internal_DiagnosComments));
        }
    }

    public function set STACKNUMBER(value:int) : void
    {
        var oldValue:int = _internal_STACKNUMBER;
        if (oldValue !== value)
        {
            _internal_STACKNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STACKNUMBER", oldValue, _internal_STACKNUMBER));
        }
    }

    public function set selected(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_selected;
        if (oldValue !== value)
        {
            _internal_selected = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "selected", oldValue, _internal_selected));
        }
    }

    public function set TreatmentObjects(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_TreatmentObjects;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_TreatmentObjects = value;
            }
            else if (value is Array)
            {
                _internal_TreatmentObjects = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_TreatmentObjects = null;
            }
            else
            {
                throw new Error("value of TreatmentObjects must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TreatmentObjects", oldValue, _internal_TreatmentObjects));
        }
    }

    public function set TREATMENTCOURSE(value:int) : void
    {
        var oldValue:int = _internal_TREATMENTCOURSE;
        if (oldValue !== value)
        {
            _internal_TREATMENTCOURSE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTCOURSE", oldValue, _internal_TREATMENTCOURSE));
        }
    }

    public function set TEETH(value:String) : void
    {
        var oldValue:String = _internal_TEETH;
        if (oldValue !== value)
        {
            _internal_TEETH = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TEETH", oldValue, _internal_TEETH));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _HealthPlanMKB10TreatmentDiagnosEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _HealthPlanMKB10TreatmentDiagnosEntityMetadata) : void
    {
        var oldValue : _HealthPlanMKB10TreatmentDiagnosEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
