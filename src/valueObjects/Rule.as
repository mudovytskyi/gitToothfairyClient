package valueObjects
{
	import com.adobe.fiber.runtime.lib.StringFunc;
	
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	
	import mx.formatters.DateFormatter;
	import mx.rpc.xml.SimpleXMLDecoder;
	import mx.rpc.xml.SimpleXMLEncoder;
	
	public class Rule
	{
		public var ID: String;
		public var operatingsScheduleID: String;
		
		public var name: String;
		public var isWorking: Boolean = true;
		public var daysRange: Array = new Array();	
		
		public var repeatType: int;				// -1 - не повторювати
		// 0 - кожен день
		// 1- з перервою в N днів
		// 2 - кожен тиждень
		// 3 - кожен місяць	
		
		public var skipDaysCount: int;
		
		public var startTime: Date;	
		public var endTime: Date;
		
		public var cabinetID: String;
		
		public function Rule()
		{		
		}
		
		public static function createFromForSaveFormat(personnelModuleRule: PersonnelModuleRule): Rule
		{
			var rule: Rule = new Rule();
			
			rule.ID = personnelModuleRule.ID;
			rule.operatingsScheduleID = personnelModuleRule.OPERATINGSCHEDULEID;
			rule.name = personnelModuleRule.NAME;
			rule.isWorking = Boolean(parseInt(personnelModuleRule.ISWORKING));
			
			if ((personnelModuleRule.DAYSRANGE) && (personnelModuleRule.DAYSRANGE != ""))
			{
				var xmlDoc: XMLDocument = new XMLDocument(personnelModuleRule.DAYSRANGE);
				var decoder: SimpleXMLDecoder = new SimpleXMLDecoder(false);
				var resultObj: Object = decoder.decodeXML(xmlDoc);
				
				var array: Array = new Array();
				
				if (resultObj.Rule.item is Array) array = resultObj.Rule.item
				else array.push(resultObj.Rule.item);
				
				var newArray: Array = new Array();
				
				for (var i: int = 0; i < array.length; i++)
				{
					var obj: Object = new Object();
					
					obj.rangeStart = new Date(StringFunc.right(array[i].rangeStart, 4), parseInt(StringFunc.substring(array[i].rangeStart, 3, 2)) - 1, StringFunc.left(array[i].rangeStart, 2));
					obj.rangeEnd = new Date(StringFunc.right(array[i].rangeEnd, 4), parseInt(StringFunc.substring(array[i].rangeEnd, 3, 2)) - 1, StringFunc.left(array[i].rangeEnd, 2));
					obj.isWorking = Boolean(array[i].isWorking);
					
					newArray.push(obj);
				}
				
				rule.daysRange = newArray;
			}
			
			rule.repeatType = parseInt(personnelModuleRule.REPEATTYPE);			
			rule.skipDaysCount = parseInt(personnelModuleRule.SKIPDAYSCOUNT);				
			rule.startTime = new Date(null, null, null, StringFunc.left(personnelModuleRule.STARTTIME, 2), StringFunc.substring(personnelModuleRule.STARTTIME, 3, 2));
			rule.endTime = new Date(null, null, null, StringFunc.left(personnelModuleRule.ENDTIME, 2), StringFunc.substring(personnelModuleRule.ENDTIME, 3, 2));			
			rule.cabinetID = personnelModuleRule.CABINETID;
			
			return rule;
		}
		
		public function convertToSaveFormat(): PersonnelModuleRule
		{
			var personnelModuleRule: PersonnelModuleRule = new PersonnelModuleRule();
			
			personnelModuleRule.ID = this.ID;
			personnelModuleRule.OPERATINGSCHEDULEID = this.operatingsScheduleID;
			personnelModuleRule.NAME = this.name;			
			personnelModuleRule.ISWORKING = (int(this.isWorking)).toString();
			
			/*selectedRanges="{[ {rangeStart: new Date(2006,0,11), rangeEnd: new Date(2006,0,11)},
			{rangeStart:new Date(2006,0,23), rangeEnd: new Date(2006,1,10)} ]}"*/	
			
			var array: Array = new Array();	
			
			var dateFormatter: DateFormatter = new DateFormatter();
			dateFormatter.formatString = "DD.MM.YYYY";
			
			for (var i: int = 0; i < this.daysRange.length; i++)
			{
				var obj: Object = new Object();
				
				obj.rangeStart = dateFormatter.format(this.daysRange[i].rangeStart);
				obj.rangeEnd = dateFormatter.format(this.daysRange[i].rangeEnd);
				obj.isWorking = int(this.isWorking);
				
				array.push(obj);
			}
			
			var qName: QName = new QName("Rule");
			var xmlDocument: XMLDocument = new XMLDocument();					
			var simpleXMLEncoder: SimpleXMLEncoder = new SimpleXMLEncoder(xmlDocument);				
			var xmlNode: XMLNode = simpleXMLEncoder.encodeValue(array, qName, xmlDocument);	
			
			var xml: XML = new XML(xmlDocument.toString());
			
			personnelModuleRule.DAYSRANGE = xml.toXMLString();			
			
			personnelModuleRule.REPEATTYPE = this.repeatType.toString();			
			personnelModuleRule.SKIPDAYSCOUNT = this.skipDaysCount.toString();
			
			dateFormatter.formatString = "JJ:NN";
			
			personnelModuleRule.STARTTIME = dateFormatter.format(this.startTime);
			personnelModuleRule.ENDTIME = dateFormatter.format(this.endTime);
			
			personnelModuleRule.CABINETID = this.cabinetID;
			
			return personnelModuleRule;
		}		
	}
}