/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - DiscountModuleDiscountCard.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.DiscountModulePatient;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_DiscountModuleDiscountCard extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("DiscountModuleDiscountCard") == null)
            {
                flash.net.registerClassAlias("DiscountModuleDiscountCard", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("DiscountModuleDiscountCard", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.DiscountModulePatient.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _DiscountModuleDiscountCardEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_SUMM : Number;
    private var _internal_ENABLED : Boolean;
    private var _internal_NUMBER : String;
    private var _internal_BARCODE : String;
    private var _internal_PATIENT : valueObjects.DiscountModulePatient;
    private var _internal_DISCOUNT : Number;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_DiscountModuleDiscountCard()
    {
        _model = new _DiscountModuleDiscountCardEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get SUMM() : Number
    {
        return _internal_SUMM;
    }

    [Bindable(event="propertyChange")]
    public function get ENABLED() : Boolean
    {
        return _internal_ENABLED;
    }

    [Bindable(event="propertyChange")]
    public function get NUMBER() : String
    {
        return _internal_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get BARCODE() : String
    {
        return _internal_BARCODE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT() : valueObjects.DiscountModulePatient
    {
        return _internal_PATIENT;
    }

    [Bindable(event="propertyChange")]
    public function get DISCOUNT() : Number
    {
        return _internal_DISCOUNT;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set SUMM(value:Number) : void
    {
        var oldValue:Number = _internal_SUMM;
        if (isNaN(_internal_SUMM) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_SUMM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMM", oldValue, _internal_SUMM));
        }
    }

    public function set ENABLED(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_ENABLED;
        if (oldValue !== value)
        {
            _internal_ENABLED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ENABLED", oldValue, _internal_ENABLED));
        }
    }

    public function set NUMBER(value:String) : void
    {
        var oldValue:String = _internal_NUMBER;
        if (oldValue !== value)
        {
            _internal_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NUMBER", oldValue, _internal_NUMBER));
        }
    }

    public function set BARCODE(value:String) : void
    {
        var oldValue:String = _internal_BARCODE;
        if (oldValue !== value)
        {
            _internal_BARCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "BARCODE", oldValue, _internal_BARCODE));
        }
    }

    public function set PATIENT(value:valueObjects.DiscountModulePatient) : void
    {
        var oldValue:valueObjects.DiscountModulePatient = _internal_PATIENT;
        if (oldValue !== value)
        {
            _internal_PATIENT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT", oldValue, _internal_PATIENT));
        }
    }

    public function set DISCOUNT(value:Number) : void
    {
        var oldValue:Number = _internal_DISCOUNT;
        if (isNaN(_internal_DISCOUNT) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_DISCOUNT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DISCOUNT", oldValue, _internal_DISCOUNT));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _DiscountModuleDiscountCardEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _DiscountModuleDiscountCardEntityMetadata) : void
    {
        var oldValue : _DiscountModuleDiscountCardEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
