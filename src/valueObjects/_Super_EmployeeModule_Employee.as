/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - EmployeeModule_Employee.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_EmployeeModule_Employee extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("EmployeeModule_Employee") == null)
            {
                flash.net.registerClassAlias("EmployeeModule_Employee", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("EmployeeModule_Employee", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _EmployeeModule_EmployeeEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_fname : String;
    private var _internal_sname : String;
    private var _internal_lname : String;
    private var _internal_fullname : String;
    private var _internal_shortname : String;
    private var _internal_birthday : String;
    private var _internal_sex : int;
    private var _internal_phone1 : String;
    private var _internal_phone2 : String;
    private var _internal_specialty : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_EmployeeModule_Employee()
    {
        _model = new _EmployeeModule_EmployeeEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get fname() : String
    {
        return _internal_fname;
    }

    [Bindable(event="propertyChange")]
    public function get sname() : String
    {
        return _internal_sname;
    }

    [Bindable(event="propertyChange")]
    public function get lname() : String
    {
        return _internal_lname;
    }

    [Bindable(event="propertyChange")]
    public function get fullname() : String
    {
        return _internal_fullname;
    }

    [Bindable(event="propertyChange")]
    public function get shortname() : String
    {
        return _internal_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get birthday() : String
    {
        return _internal_birthday;
    }

    [Bindable(event="propertyChange")]
    public function get sex() : int
    {
        return _internal_sex;
    }

    [Bindable(event="propertyChange")]
    public function get phone1() : String
    {
        return _internal_phone1;
    }

    [Bindable(event="propertyChange")]
    public function get phone2() : String
    {
        return _internal_phone2;
    }

    [Bindable(event="propertyChange")]
    public function get specialty() : String
    {
        return _internal_specialty;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set fname(value:String) : void
    {
        var oldValue:String = _internal_fname;
        if (oldValue !== value)
        {
            _internal_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fname", oldValue, _internal_fname));
        }
    }

    public function set sname(value:String) : void
    {
        var oldValue:String = _internal_sname;
        if (oldValue !== value)
        {
            _internal_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sname", oldValue, _internal_sname));
        }
    }

    public function set lname(value:String) : void
    {
        var oldValue:String = _internal_lname;
        if (oldValue !== value)
        {
            _internal_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "lname", oldValue, _internal_lname));
        }
    }

    public function set fullname(value:String) : void
    {
        var oldValue:String = _internal_fullname;
        if (oldValue !== value)
        {
            _internal_fullname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fullname", oldValue, _internal_fullname));
        }
    }

    public function set shortname(value:String) : void
    {
        var oldValue:String = _internal_shortname;
        if (oldValue !== value)
        {
            _internal_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "shortname", oldValue, _internal_shortname));
        }
    }

    public function set birthday(value:String) : void
    {
        var oldValue:String = _internal_birthday;
        if (oldValue !== value)
        {
            _internal_birthday = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "birthday", oldValue, _internal_birthday));
        }
    }

    public function set sex(value:int) : void
    {
        var oldValue:int = _internal_sex;
        if (oldValue !== value)
        {
            _internal_sex = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sex", oldValue, _internal_sex));
        }
    }

    public function set phone1(value:String) : void
    {
        var oldValue:String = _internal_phone1;
        if (oldValue !== value)
        {
            _internal_phone1 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "phone1", oldValue, _internal_phone1));
        }
    }

    public function set phone2(value:String) : void
    {
        var oldValue:String = _internal_phone2;
        if (oldValue !== value)
        {
            _internal_phone2 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "phone2", oldValue, _internal_phone2));
        }
    }

    public function set specialty(value:String) : void
    {
        var oldValue:String = _internal_specialty;
        if (oldValue !== value)
        {
            _internal_specialty = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "specialty", oldValue, _internal_specialty));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _EmployeeModule_EmployeeEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _EmployeeModule_EmployeeEntityMetadata) : void
    {
        var oldValue : _EmployeeModule_EmployeeEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
