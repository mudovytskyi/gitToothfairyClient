/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - CalendarModuleInsurancePolisObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.CalendarModuleInsuranceCompanyObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_CalendarModuleInsurancePolisObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("CalendarModuleInsurancePolisObject") == null)
            {
                flash.net.registerClassAlias("CalendarModuleInsurancePolisObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("CalendarModuleInsurancePolisObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.CalendarModuleInsuranceCompanyObject.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _CalendarModuleInsurancePolisObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_Id : String;
    private var _internal_policyNumber : String;
    private var _internal_toDate : String;
    private var _internal_fromDate : String;
    private var _internal_Comments : String;
    private var _internal_company : valueObjects.CalendarModuleInsuranceCompanyObject;
    private var _internal_patientId : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_CalendarModuleInsurancePolisObject()
    {
        _model = new _CalendarModuleInsurancePolisObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get Id() : String
    {
        return _internal_Id;
    }

    [Bindable(event="propertyChange")]
    public function get policyNumber() : String
    {
        return _internal_policyNumber;
    }

    [Bindable(event="propertyChange")]
    public function get toDate() : String
    {
        return _internal_toDate;
    }

    [Bindable(event="propertyChange")]
    public function get fromDate() : String
    {
        return _internal_fromDate;
    }

    [Bindable(event="propertyChange")]
    public function get Comments() : String
    {
        return _internal_Comments;
    }

    [Bindable(event="propertyChange")]
    public function get company() : valueObjects.CalendarModuleInsuranceCompanyObject
    {
        return _internal_company;
    }

    [Bindable(event="propertyChange")]
    public function get patientId() : String
    {
        return _internal_patientId;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set Id(value:String) : void
    {
        var oldValue:String = _internal_Id;
        if (oldValue !== value)
        {
            _internal_Id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Id", oldValue, _internal_Id));
        }
    }

    public function set policyNumber(value:String) : void
    {
        var oldValue:String = _internal_policyNumber;
        if (oldValue !== value)
        {
            _internal_policyNumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "policyNumber", oldValue, _internal_policyNumber));
        }
    }

    public function set toDate(value:String) : void
    {
        var oldValue:String = _internal_toDate;
        if (oldValue !== value)
        {
            _internal_toDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "toDate", oldValue, _internal_toDate));
        }
    }

    public function set fromDate(value:String) : void
    {
        var oldValue:String = _internal_fromDate;
        if (oldValue !== value)
        {
            _internal_fromDate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "fromDate", oldValue, _internal_fromDate));
        }
    }

    public function set Comments(value:String) : void
    {
        var oldValue:String = _internal_Comments;
        if (oldValue !== value)
        {
            _internal_Comments = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "Comments", oldValue, _internal_Comments));
        }
    }

    public function set company(value:valueObjects.CalendarModuleInsuranceCompanyObject) : void
    {
        var oldValue:valueObjects.CalendarModuleInsuranceCompanyObject = _internal_company;
        if (oldValue !== value)
        {
            _internal_company = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "company", oldValue, _internal_company));
        }
    }

    public function set patientId(value:String) : void
    {
        var oldValue:String = _internal_patientId;
        if (oldValue !== value)
        {
            _internal_patientId = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patientId", oldValue, _internal_patientId));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _CalendarModuleInsurancePolisObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _CalendarModuleInsurancePolisObjectEntityMetadata) : void
    {
        var oldValue : _CalendarModuleInsurancePolisObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
