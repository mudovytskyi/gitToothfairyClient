/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - SMSSenderSetting.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_SMSSenderSetting extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("SMSSenderSetting") == null)
            {
                flash.net.registerClassAlias("SMSSenderSetting", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("SMSSenderSetting", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _SMSSenderSettingEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_SMSModuleMode : int;
    private var _internal_SMSModuleGateIP : String;
    private var _internal_SMSModuleGatePort : String;
    private var _internal_SMSModuleGateAdditional : String;
    private var _internal_SMSLogin : String;
    private var _internal_SMSPassword : String;
    private var _internal_SMSEnable : Boolean;
    private var _internal_SMSEnableOnTask : Boolean;
    private var _internal_SMSEnableOnDOB : Boolean;
    private var _internal_SMSSendDays : String;
    private var _internal_SMSStartTime : String;
    private var _internal_SMSEndTime : String;
    private var _internal_SMSSendBeforeInDays : int;
    private var _internal_SMSSendBeforeInHours : int;
    private var _internal_SMSTaskTemplate : String;
    private var _internal_SMSDispanserTaskTemplate : String;
    private var _internal_SMSDOBTemplate : String;
    private var _internal_SMSSenderName : String;
    private var _internal_SMSQueueCapacity : int;
    private var _internal_SMSTranslit : Boolean;
    private var _internal_SMSModuleUseGinMobile : Boolean;
    private var _internal_SMSConflictIgnore : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_SMSSenderSetting()
    {
        _model = new _SMSSenderSettingEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get SMSModuleMode() : int
    {
        return _internal_SMSModuleMode;
    }

    [Bindable(event="propertyChange")]
    public function get SMSModuleGateIP() : String
    {
        return _internal_SMSModuleGateIP;
    }

    [Bindable(event="propertyChange")]
    public function get SMSModuleGatePort() : String
    {
        return _internal_SMSModuleGatePort;
    }

    [Bindable(event="propertyChange")]
    public function get SMSModuleGateAdditional() : String
    {
        return _internal_SMSModuleGateAdditional;
    }

    [Bindable(event="propertyChange")]
    public function get SMSLogin() : String
    {
        return _internal_SMSLogin;
    }

    [Bindable(event="propertyChange")]
    public function get SMSPassword() : String
    {
        return _internal_SMSPassword;
    }

    [Bindable(event="propertyChange")]
    public function get SMSEnable() : Boolean
    {
        return _internal_SMSEnable;
    }

    [Bindable(event="propertyChange")]
    public function get SMSEnableOnTask() : Boolean
    {
        return _internal_SMSEnableOnTask;
    }

    [Bindable(event="propertyChange")]
    public function get SMSEnableOnDOB() : Boolean
    {
        return _internal_SMSEnableOnDOB;
    }

    [Bindable(event="propertyChange")]
    public function get SMSSendDays() : String
    {
        return _internal_SMSSendDays;
    }

    [Bindable(event="propertyChange")]
    public function get SMSStartTime() : String
    {
        return _internal_SMSStartTime;
    }

    [Bindable(event="propertyChange")]
    public function get SMSEndTime() : String
    {
        return _internal_SMSEndTime;
    }

    [Bindable(event="propertyChange")]
    public function get SMSSendBeforeInDays() : int
    {
        return _internal_SMSSendBeforeInDays;
    }

    [Bindable(event="propertyChange")]
    public function get SMSSendBeforeInHours() : int
    {
        return _internal_SMSSendBeforeInHours;
    }

    [Bindable(event="propertyChange")]
    public function get SMSTaskTemplate() : String
    {
        return _internal_SMSTaskTemplate;
    }

    [Bindable(event="propertyChange")]
    public function get SMSDispanserTaskTemplate() : String
    {
        return _internal_SMSDispanserTaskTemplate;
    }

    [Bindable(event="propertyChange")]
    public function get SMSDOBTemplate() : String
    {
        return _internal_SMSDOBTemplate;
    }

    [Bindable(event="propertyChange")]
    public function get SMSSenderName() : String
    {
        return _internal_SMSSenderName;
    }

    [Bindable(event="propertyChange")]
    public function get SMSQueueCapacity() : int
    {
        return _internal_SMSQueueCapacity;
    }

    [Bindable(event="propertyChange")]
    public function get SMSTranslit() : Boolean
    {
        return _internal_SMSTranslit;
    }

    [Bindable(event="propertyChange")]
    public function get SMSModuleUseGinMobile() : Boolean
    {
        return _internal_SMSModuleUseGinMobile;
    }

    [Bindable(event="propertyChange")]
    public function get SMSConflictIgnore() : Boolean
    {
        return _internal_SMSConflictIgnore;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set SMSModuleMode(value:int) : void
    {
        var oldValue:int = _internal_SMSModuleMode;
        if (oldValue !== value)
        {
            _internal_SMSModuleMode = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSModuleMode", oldValue, _internal_SMSModuleMode));
        }
    }

    public function set SMSModuleGateIP(value:String) : void
    {
        var oldValue:String = _internal_SMSModuleGateIP;
        if (oldValue !== value)
        {
            _internal_SMSModuleGateIP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSModuleGateIP", oldValue, _internal_SMSModuleGateIP));
        }
    }

    public function set SMSModuleGatePort(value:String) : void
    {
        var oldValue:String = _internal_SMSModuleGatePort;
        if (oldValue !== value)
        {
            _internal_SMSModuleGatePort = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSModuleGatePort", oldValue, _internal_SMSModuleGatePort));
        }
    }

    public function set SMSModuleGateAdditional(value:String) : void
    {
        var oldValue:String = _internal_SMSModuleGateAdditional;
        if (oldValue !== value)
        {
            _internal_SMSModuleGateAdditional = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSModuleGateAdditional", oldValue, _internal_SMSModuleGateAdditional));
        }
    }

    public function set SMSLogin(value:String) : void
    {
        var oldValue:String = _internal_SMSLogin;
        if (oldValue !== value)
        {
            _internal_SMSLogin = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSLogin", oldValue, _internal_SMSLogin));
        }
    }

    public function set SMSPassword(value:String) : void
    {
        var oldValue:String = _internal_SMSPassword;
        if (oldValue !== value)
        {
            _internal_SMSPassword = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSPassword", oldValue, _internal_SMSPassword));
        }
    }

    public function set SMSEnable(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SMSEnable;
        if (oldValue !== value)
        {
            _internal_SMSEnable = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSEnable", oldValue, _internal_SMSEnable));
        }
    }

    public function set SMSEnableOnTask(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SMSEnableOnTask;
        if (oldValue !== value)
        {
            _internal_SMSEnableOnTask = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSEnableOnTask", oldValue, _internal_SMSEnableOnTask));
        }
    }

    public function set SMSEnableOnDOB(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SMSEnableOnDOB;
        if (oldValue !== value)
        {
            _internal_SMSEnableOnDOB = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSEnableOnDOB", oldValue, _internal_SMSEnableOnDOB));
        }
    }

    public function set SMSSendDays(value:String) : void
    {
        var oldValue:String = _internal_SMSSendDays;
        if (oldValue !== value)
        {
            _internal_SMSSendDays = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSSendDays", oldValue, _internal_SMSSendDays));
        }
    }

    public function set SMSStartTime(value:String) : void
    {
        var oldValue:String = _internal_SMSStartTime;
        if (oldValue !== value)
        {
            _internal_SMSStartTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSStartTime", oldValue, _internal_SMSStartTime));
        }
    }

    public function set SMSEndTime(value:String) : void
    {
        var oldValue:String = _internal_SMSEndTime;
        if (oldValue !== value)
        {
            _internal_SMSEndTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSEndTime", oldValue, _internal_SMSEndTime));
        }
    }

    public function set SMSSendBeforeInDays(value:int) : void
    {
        var oldValue:int = _internal_SMSSendBeforeInDays;
        if (oldValue !== value)
        {
            _internal_SMSSendBeforeInDays = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSSendBeforeInDays", oldValue, _internal_SMSSendBeforeInDays));
        }
    }

    public function set SMSSendBeforeInHours(value:int) : void
    {
        var oldValue:int = _internal_SMSSendBeforeInHours;
        if (oldValue !== value)
        {
            _internal_SMSSendBeforeInHours = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSSendBeforeInHours", oldValue, _internal_SMSSendBeforeInHours));
        }
    }

    public function set SMSTaskTemplate(value:String) : void
    {
        var oldValue:String = _internal_SMSTaskTemplate;
        if (oldValue !== value)
        {
            _internal_SMSTaskTemplate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSTaskTemplate", oldValue, _internal_SMSTaskTemplate));
        }
    }

    public function set SMSDispanserTaskTemplate(value:String) : void
    {
        var oldValue:String = _internal_SMSDispanserTaskTemplate;
        if (oldValue !== value)
        {
            _internal_SMSDispanserTaskTemplate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSDispanserTaskTemplate", oldValue, _internal_SMSDispanserTaskTemplate));
        }
    }

    public function set SMSDOBTemplate(value:String) : void
    {
        var oldValue:String = _internal_SMSDOBTemplate;
        if (oldValue !== value)
        {
            _internal_SMSDOBTemplate = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSDOBTemplate", oldValue, _internal_SMSDOBTemplate));
        }
    }

    public function set SMSSenderName(value:String) : void
    {
        var oldValue:String = _internal_SMSSenderName;
        if (oldValue !== value)
        {
            _internal_SMSSenderName = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSSenderName", oldValue, _internal_SMSSenderName));
        }
    }

    public function set SMSQueueCapacity(value:int) : void
    {
        var oldValue:int = _internal_SMSQueueCapacity;
        if (oldValue !== value)
        {
            _internal_SMSQueueCapacity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSQueueCapacity", oldValue, _internal_SMSQueueCapacity));
        }
    }

    public function set SMSTranslit(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SMSTranslit;
        if (oldValue !== value)
        {
            _internal_SMSTranslit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSTranslit", oldValue, _internal_SMSTranslit));
        }
    }

    public function set SMSModuleUseGinMobile(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SMSModuleUseGinMobile;
        if (oldValue !== value)
        {
            _internal_SMSModuleUseGinMobile = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSModuleUseGinMobile", oldValue, _internal_SMSModuleUseGinMobile));
        }
    }

    public function set SMSConflictIgnore(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SMSConflictIgnore;
        if (oldValue !== value)
        {
            _internal_SMSConflictIgnore = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SMSConflictIgnore", oldValue, _internal_SMSConflictIgnore));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _SMSSenderSettingEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _SMSSenderSettingEntityMetadata) : void
    {
        var oldValue : _SMSSenderSettingEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
