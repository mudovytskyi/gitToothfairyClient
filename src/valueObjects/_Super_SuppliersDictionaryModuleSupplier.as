/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - SuppliersDictionaryModuleSupplier.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_SuppliersDictionaryModuleSupplier extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("SuppliersDictionaryModuleSupplier") == null)
            {
                flash.net.registerClassAlias("SuppliersDictionaryModuleSupplier", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("SuppliersDictionaryModuleSupplier", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _SuppliersDictionaryModuleSupplierEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_NAME : String;
    private var _internal_LEGALNAME : String;
    private var _internal_ADDRESS : String;
    private var _internal_ORGANIZATIONTYPE : String;
    private var _internal_CODE : String;
    private var _internal_DIRECTOR : String;
    private var _internal_TELEPHONENUMBER : String;
    private var _internal_FAX : String;
    private var _internal_SITENAME : String;
    private var _internal_EMAIL : String;
    private var _internal_ICQ : String;
    private var _internal_SKYPE : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_SuppliersDictionaryModuleSupplier()
    {
        _model = new _SuppliersDictionaryModuleSupplierEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get NAME() : String
    {
        return _internal_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get LEGALNAME() : String
    {
        return _internal_LEGALNAME;
    }

    [Bindable(event="propertyChange")]
    public function get ADDRESS() : String
    {
        return _internal_ADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get ORGANIZATIONTYPE() : String
    {
        return _internal_ORGANIZATIONTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get CODE() : String
    {
        return _internal_CODE;
    }

    [Bindable(event="propertyChange")]
    public function get DIRECTOR() : String
    {
        return _internal_DIRECTOR;
    }

    [Bindable(event="propertyChange")]
    public function get TELEPHONENUMBER() : String
    {
        return _internal_TELEPHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get FAX() : String
    {
        return _internal_FAX;
    }

    [Bindable(event="propertyChange")]
    public function get SITENAME() : String
    {
        return _internal_SITENAME;
    }

    [Bindable(event="propertyChange")]
    public function get EMAIL() : String
    {
        return _internal_EMAIL;
    }

    [Bindable(event="propertyChange")]
    public function get ICQ() : String
    {
        return _internal_ICQ;
    }

    [Bindable(event="propertyChange")]
    public function get SKYPE() : String
    {
        return _internal_SKYPE;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set NAME(value:String) : void
    {
        var oldValue:String = _internal_NAME;
        if (oldValue !== value)
        {
            _internal_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NAME", oldValue, _internal_NAME));
        }
    }

    public function set LEGALNAME(value:String) : void
    {
        var oldValue:String = _internal_LEGALNAME;
        if (oldValue !== value)
        {
            _internal_LEGALNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LEGALNAME", oldValue, _internal_LEGALNAME));
        }
    }

    public function set ADDRESS(value:String) : void
    {
        var oldValue:String = _internal_ADDRESS;
        if (oldValue !== value)
        {
            _internal_ADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ADDRESS", oldValue, _internal_ADDRESS));
        }
    }

    public function set ORGANIZATIONTYPE(value:String) : void
    {
        var oldValue:String = _internal_ORGANIZATIONTYPE;
        if (oldValue !== value)
        {
            _internal_ORGANIZATIONTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ORGANIZATIONTYPE", oldValue, _internal_ORGANIZATIONTYPE));
        }
    }

    public function set CODE(value:String) : void
    {
        var oldValue:String = _internal_CODE;
        if (oldValue !== value)
        {
            _internal_CODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CODE", oldValue, _internal_CODE));
        }
    }

    public function set DIRECTOR(value:String) : void
    {
        var oldValue:String = _internal_DIRECTOR;
        if (oldValue !== value)
        {
            _internal_DIRECTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DIRECTOR", oldValue, _internal_DIRECTOR));
        }
    }

    public function set TELEPHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_TELEPHONENUMBER;
        if (oldValue !== value)
        {
            _internal_TELEPHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TELEPHONENUMBER", oldValue, _internal_TELEPHONENUMBER));
        }
    }

    public function set FAX(value:String) : void
    {
        var oldValue:String = _internal_FAX;
        if (oldValue !== value)
        {
            _internal_FAX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FAX", oldValue, _internal_FAX));
        }
    }

    public function set SITENAME(value:String) : void
    {
        var oldValue:String = _internal_SITENAME;
        if (oldValue !== value)
        {
            _internal_SITENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SITENAME", oldValue, _internal_SITENAME));
        }
    }

    public function set EMAIL(value:String) : void
    {
        var oldValue:String = _internal_EMAIL;
        if (oldValue !== value)
        {
            _internal_EMAIL = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EMAIL", oldValue, _internal_EMAIL));
        }
    }

    public function set ICQ(value:String) : void
    {
        var oldValue:String = _internal_ICQ;
        if (oldValue !== value)
        {
            _internal_ICQ = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ICQ", oldValue, _internal_ICQ));
        }
    }

    public function set SKYPE(value:String) : void
    {
        var oldValue:String = _internal_SKYPE;
        if (oldValue !== value)
        {
            _internal_SKYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SKYPE", oldValue, _internal_SKYPE));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _SuppliersDictionaryModuleSupplierEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _SuppliersDictionaryModuleSupplierEntityMetadata) : void
    {
        var oldValue : _SuppliersDictionaryModuleSupplierEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
