
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.collections.ArrayCollection;
import valueObjects.PatientCardModule_mobilephone;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _PatientCardModuleAddPatientObjectEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDDATE", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "ADDRESS", "POSTALCODE", "TELEPHONENUMBER", "MOBILENUMBER", "EMPLOYMENTPLACE", "JOB", "WORKPHONE", "VILLAGEORCITY", "POPULATIONGROUPE", "ISDISPANCER", "HOBBY", "COUNTRY", "STATE", "RAGION", "EMAIL", "CITY", "PASSPORT_SERIES", "PASSPORT_NUMBER", "PASSPORT_ISSUING", "PASSPORT_DATE", "WHEREFROMID", "FATHERFIRSTNAME", "MOTHERFIRSTNAME", "FATHERMIDDLENAME", "MOTHERMIDDLENAME", "FATHERLASTNAME", "MOTHERLASTNAME", "FATHERBIRTHDATE", "MOTHERBIRTHDATE", "GROUPID", "RELATIVEID", "LEGALGUARDIANID", "INSURANCEID", "INSURANCECOUNT", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "full_addres", "full_fio", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "mobiles", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "EH_DEC_ID", "TAXID", "agreementtype", "birthcert_series", "birthcert_number", "birthcert_issuing", "birthcert_date", "IS_MOTHER", "MOTHER_ID", "MOTHER_PASSPORT_SERIES", "MOTHER_PASSPORT_NUMBER", "MOTHER_PASSPORT_ISSUING", "MOTHER_PASSPORT_DATE", "FATHER_ID", "FATHER_PASSPORT_SERIES", "FATHER_PASSPORT_NUMBER", "FATHER_PASSPORT_ISSUING", "FATHER_PASSPORT_DATE", "CONFLICTFLAG");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDDATE", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "ADDRESS", "POSTALCODE", "TELEPHONENUMBER", "MOBILENUMBER", "EMPLOYMENTPLACE", "JOB", "WORKPHONE", "VILLAGEORCITY", "POPULATIONGROUPE", "ISDISPANCER", "HOBBY", "COUNTRY", "STATE", "RAGION", "EMAIL", "CITY", "PASSPORT_SERIES", "PASSPORT_NUMBER", "PASSPORT_ISSUING", "PASSPORT_DATE", "WHEREFROMID", "FATHERFIRSTNAME", "MOTHERFIRSTNAME", "FATHERMIDDLENAME", "MOTHERMIDDLENAME", "FATHERLASTNAME", "MOTHERLASTNAME", "FATHERBIRTHDATE", "MOTHERBIRTHDATE", "GROUPID", "RELATIVEID", "LEGALGUARDIANID", "INSURANCEID", "INSURANCECOUNT", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "full_addres", "full_fio", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "mobiles", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "EH_DEC_ID", "TAXID", "agreementtype", "birthcert_series", "birthcert_number", "birthcert_issuing", "birthcert_date", "IS_MOTHER", "MOTHER_ID", "MOTHER_PASSPORT_SERIES", "MOTHER_PASSPORT_NUMBER", "MOTHER_PASSPORT_ISSUING", "MOTHER_PASSPORT_DATE", "FATHER_ID", "FATHER_PASSPORT_SERIES", "FATHER_PASSPORT_NUMBER", "FATHER_PASSPORT_ISSUING", "FATHER_PASSPORT_DATE", "CONFLICTFLAG");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDDATE", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "ADDRESS", "POSTALCODE", "TELEPHONENUMBER", "MOBILENUMBER", "EMPLOYMENTPLACE", "JOB", "WORKPHONE", "VILLAGEORCITY", "POPULATIONGROUPE", "ISDISPANCER", "HOBBY", "COUNTRY", "STATE", "RAGION", "EMAIL", "CITY", "PASSPORT_SERIES", "PASSPORT_NUMBER", "PASSPORT_ISSUING", "PASSPORT_DATE", "WHEREFROMID", "FATHERFIRSTNAME", "MOTHERFIRSTNAME", "FATHERMIDDLENAME", "MOTHERMIDDLENAME", "FATHERLASTNAME", "MOTHERLASTNAME", "FATHERBIRTHDATE", "MOTHERBIRTHDATE", "GROUPID", "RELATIVEID", "LEGALGUARDIANID", "INSURANCEID", "INSURANCECOUNT", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "full_addres", "full_fio", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "mobiles", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "EH_DEC_ID", "TAXID", "agreementtype", "birthcert_series", "birthcert_number", "birthcert_issuing", "birthcert_date", "IS_MOTHER", "MOTHER_ID", "MOTHER_PASSPORT_SERIES", "MOTHER_PASSPORT_NUMBER", "MOTHER_PASSPORT_ISSUING", "MOTHER_PASSPORT_DATE", "FATHER_ID", "FATHER_PASSPORT_SERIES", "FATHER_PASSPORT_NUMBER", "FATHER_PASSPORT_ISSUING", "FATHER_PASSPORT_DATE", "CONFLICTFLAG");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("ID", "CARDNUMBER_FULL", "CARDNUMBER", "CARDDATE", "CARDBEFORNUMBER", "CARDAFTERNUMBER", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "FULLNAME", "SHORTNAME", "BIRTHDAY", "SEX", "ADDRESS", "POSTALCODE", "TELEPHONENUMBER", "MOBILENUMBER", "EMPLOYMENTPLACE", "JOB", "WORKPHONE", "VILLAGEORCITY", "POPULATIONGROUPE", "ISDISPANCER", "HOBBY", "COUNTRY", "STATE", "RAGION", "EMAIL", "CITY", "PASSPORT_SERIES", "PASSPORT_NUMBER", "PASSPORT_ISSUING", "PASSPORT_DATE", "WHEREFROMID", "FATHERFIRSTNAME", "MOTHERFIRSTNAME", "FATHERMIDDLENAME", "MOTHERMIDDLENAME", "FATHERLASTNAME", "MOTHERLASTNAME", "FATHERBIRTHDATE", "MOTHERBIRTHDATE", "GROUPID", "RELATIVEID", "LEGALGUARDIANID", "INSURANCEID", "INSURANCECOUNT", "TESTPATOLOGYFLAG", "COUNTS_TOOTHEXAMS", "COUNTS_PROCEDURES_CLOSED", "COUNTS_PROCEDURES_NOTCLOSED", "COUNTS_FILES", "full_addres", "full_fio", "discount_amount", "discount_amount_auto", "INDDISCOUNT", "DISCOUNTCARDNUMBER", "is_returned", "patient_ages", "patient_notes", "doctors", "doctor_lead", "agreementTimestamp", "mobiles", "is_archived", "subdivision_id", "subdivision_label", "NH_TREATTYPE", "EH_DEC_ID", "TAXID", "agreementtype", "birthcert_series", "birthcert_number", "birthcert_issuing", "birthcert_date", "IS_MOTHER", "MOTHER_ID", "MOTHER_PASSPORT_SERIES", "MOTHER_PASSPORT_NUMBER", "MOTHER_PASSPORT_ISSUING", "MOTHER_PASSPORT_DATE", "FATHER_ID", "FATHER_PASSPORT_SERIES", "FATHER_PASSPORT_NUMBER", "FATHER_PASSPORT_ISSUING", "FATHER_PASSPORT_DATE", "CONFLICTFLAG");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array("mobiles");
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "PatientCardModuleAddPatientObject";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_PatientCardModuleAddPatientObject;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _PatientCardModuleAddPatientObjectEntityMetadata(value : _Super_PatientCardModuleAddPatientObject)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["ID"] = new Array();
            model_internal::dependentsOnMap["CARDNUMBER_FULL"] = new Array();
            model_internal::dependentsOnMap["CARDNUMBER"] = new Array();
            model_internal::dependentsOnMap["CARDDATE"] = new Array();
            model_internal::dependentsOnMap["CARDBEFORNUMBER"] = new Array();
            model_internal::dependentsOnMap["CARDAFTERNUMBER"] = new Array();
            model_internal::dependentsOnMap["FIRSTNAME"] = new Array();
            model_internal::dependentsOnMap["MIDDLENAME"] = new Array();
            model_internal::dependentsOnMap["LASTNAME"] = new Array();
            model_internal::dependentsOnMap["FULLNAME"] = new Array();
            model_internal::dependentsOnMap["SHORTNAME"] = new Array();
            model_internal::dependentsOnMap["BIRTHDAY"] = new Array();
            model_internal::dependentsOnMap["SEX"] = new Array();
            model_internal::dependentsOnMap["ADDRESS"] = new Array();
            model_internal::dependentsOnMap["POSTALCODE"] = new Array();
            model_internal::dependentsOnMap["TELEPHONENUMBER"] = new Array();
            model_internal::dependentsOnMap["MOBILENUMBER"] = new Array();
            model_internal::dependentsOnMap["EMPLOYMENTPLACE"] = new Array();
            model_internal::dependentsOnMap["JOB"] = new Array();
            model_internal::dependentsOnMap["WORKPHONE"] = new Array();
            model_internal::dependentsOnMap["VILLAGEORCITY"] = new Array();
            model_internal::dependentsOnMap["POPULATIONGROUPE"] = new Array();
            model_internal::dependentsOnMap["ISDISPANCER"] = new Array();
            model_internal::dependentsOnMap["HOBBY"] = new Array();
            model_internal::dependentsOnMap["COUNTRY"] = new Array();
            model_internal::dependentsOnMap["STATE"] = new Array();
            model_internal::dependentsOnMap["RAGION"] = new Array();
            model_internal::dependentsOnMap["EMAIL"] = new Array();
            model_internal::dependentsOnMap["CITY"] = new Array();
            model_internal::dependentsOnMap["PASSPORT_SERIES"] = new Array();
            model_internal::dependentsOnMap["PASSPORT_NUMBER"] = new Array();
            model_internal::dependentsOnMap["PASSPORT_ISSUING"] = new Array();
            model_internal::dependentsOnMap["PASSPORT_DATE"] = new Array();
            model_internal::dependentsOnMap["WHEREFROMID"] = new Array();
            model_internal::dependentsOnMap["FATHERFIRSTNAME"] = new Array();
            model_internal::dependentsOnMap["MOTHERFIRSTNAME"] = new Array();
            model_internal::dependentsOnMap["FATHERMIDDLENAME"] = new Array();
            model_internal::dependentsOnMap["MOTHERMIDDLENAME"] = new Array();
            model_internal::dependentsOnMap["FATHERLASTNAME"] = new Array();
            model_internal::dependentsOnMap["MOTHERLASTNAME"] = new Array();
            model_internal::dependentsOnMap["FATHERBIRTHDATE"] = new Array();
            model_internal::dependentsOnMap["MOTHERBIRTHDATE"] = new Array();
            model_internal::dependentsOnMap["GROUPID"] = new Array();
            model_internal::dependentsOnMap["RELATIVEID"] = new Array();
            model_internal::dependentsOnMap["LEGALGUARDIANID"] = new Array();
            model_internal::dependentsOnMap["INSURANCEID"] = new Array();
            model_internal::dependentsOnMap["INSURANCECOUNT"] = new Array();
            model_internal::dependentsOnMap["TESTPATOLOGYFLAG"] = new Array();
            model_internal::dependentsOnMap["COUNTS_TOOTHEXAMS"] = new Array();
            model_internal::dependentsOnMap["COUNTS_PROCEDURES_CLOSED"] = new Array();
            model_internal::dependentsOnMap["COUNTS_PROCEDURES_NOTCLOSED"] = new Array();
            model_internal::dependentsOnMap["COUNTS_FILES"] = new Array();
            model_internal::dependentsOnMap["full_addres"] = new Array();
            model_internal::dependentsOnMap["full_fio"] = new Array();
            model_internal::dependentsOnMap["discount_amount"] = new Array();
            model_internal::dependentsOnMap["discount_amount_auto"] = new Array();
            model_internal::dependentsOnMap["INDDISCOUNT"] = new Array();
            model_internal::dependentsOnMap["DISCOUNTCARDNUMBER"] = new Array();
            model_internal::dependentsOnMap["is_returned"] = new Array();
            model_internal::dependentsOnMap["patient_ages"] = new Array();
            model_internal::dependentsOnMap["patient_notes"] = new Array();
            model_internal::dependentsOnMap["doctors"] = new Array();
            model_internal::dependentsOnMap["doctor_lead"] = new Array();
            model_internal::dependentsOnMap["agreementTimestamp"] = new Array();
            model_internal::dependentsOnMap["mobiles"] = new Array();
            model_internal::dependentsOnMap["is_archived"] = new Array();
            model_internal::dependentsOnMap["subdivision_id"] = new Array();
            model_internal::dependentsOnMap["subdivision_label"] = new Array();
            model_internal::dependentsOnMap["NH_TREATTYPE"] = new Array();
            model_internal::dependentsOnMap["EH_DEC_ID"] = new Array();
            model_internal::dependentsOnMap["TAXID"] = new Array();
            model_internal::dependentsOnMap["agreementtype"] = new Array();
            model_internal::dependentsOnMap["birthcert_series"] = new Array();
            model_internal::dependentsOnMap["birthcert_number"] = new Array();
            model_internal::dependentsOnMap["birthcert_issuing"] = new Array();
            model_internal::dependentsOnMap["birthcert_date"] = new Array();
            model_internal::dependentsOnMap["IS_MOTHER"] = new Array();
            model_internal::dependentsOnMap["MOTHER_ID"] = new Array();
            model_internal::dependentsOnMap["MOTHER_PASSPORT_SERIES"] = new Array();
            model_internal::dependentsOnMap["MOTHER_PASSPORT_NUMBER"] = new Array();
            model_internal::dependentsOnMap["MOTHER_PASSPORT_ISSUING"] = new Array();
            model_internal::dependentsOnMap["MOTHER_PASSPORT_DATE"] = new Array();
            model_internal::dependentsOnMap["FATHER_ID"] = new Array();
            model_internal::dependentsOnMap["FATHER_PASSPORT_SERIES"] = new Array();
            model_internal::dependentsOnMap["FATHER_PASSPORT_NUMBER"] = new Array();
            model_internal::dependentsOnMap["FATHER_PASSPORT_ISSUING"] = new Array();
            model_internal::dependentsOnMap["FATHER_PASSPORT_DATE"] = new Array();
            model_internal::dependentsOnMap["CONFLICTFLAG"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
            model_internal::collectionBaseMap["mobiles"] = "valueObjects.PatientCardModule_mobilephone";
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["ID"] = "int";
        model_internal::propertyTypeMap["CARDNUMBER_FULL"] = "String";
        model_internal::propertyTypeMap["CARDNUMBER"] = "int";
        model_internal::propertyTypeMap["CARDDATE"] = "String";
        model_internal::propertyTypeMap["CARDBEFORNUMBER"] = "String";
        model_internal::propertyTypeMap["CARDAFTERNUMBER"] = "String";
        model_internal::propertyTypeMap["FIRSTNAME"] = "String";
        model_internal::propertyTypeMap["MIDDLENAME"] = "String";
        model_internal::propertyTypeMap["LASTNAME"] = "String";
        model_internal::propertyTypeMap["FULLNAME"] = "String";
        model_internal::propertyTypeMap["SHORTNAME"] = "String";
        model_internal::propertyTypeMap["BIRTHDAY"] = "String";
        model_internal::propertyTypeMap["SEX"] = "int";
        model_internal::propertyTypeMap["ADDRESS"] = "String";
        model_internal::propertyTypeMap["POSTALCODE"] = "String";
        model_internal::propertyTypeMap["TELEPHONENUMBER"] = "String";
        model_internal::propertyTypeMap["MOBILENUMBER"] = "String";
        model_internal::propertyTypeMap["EMPLOYMENTPLACE"] = "String";
        model_internal::propertyTypeMap["JOB"] = "String";
        model_internal::propertyTypeMap["WORKPHONE"] = "String";
        model_internal::propertyTypeMap["VILLAGEORCITY"] = "int";
        model_internal::propertyTypeMap["POPULATIONGROUPE"] = "int";
        model_internal::propertyTypeMap["ISDISPANCER"] = "Object";
        model_internal::propertyTypeMap["HOBBY"] = "String";
        model_internal::propertyTypeMap["COUNTRY"] = "String";
        model_internal::propertyTypeMap["STATE"] = "String";
        model_internal::propertyTypeMap["RAGION"] = "String";
        model_internal::propertyTypeMap["EMAIL"] = "String";
        model_internal::propertyTypeMap["CITY"] = "String";
        model_internal::propertyTypeMap["PASSPORT_SERIES"] = "String";
        model_internal::propertyTypeMap["PASSPORT_NUMBER"] = "String";
        model_internal::propertyTypeMap["PASSPORT_ISSUING"] = "String";
        model_internal::propertyTypeMap["PASSPORT_DATE"] = "String";
        model_internal::propertyTypeMap["WHEREFROMID"] = "String";
        model_internal::propertyTypeMap["FATHERFIRSTNAME"] = "String";
        model_internal::propertyTypeMap["MOTHERFIRSTNAME"] = "String";
        model_internal::propertyTypeMap["FATHERMIDDLENAME"] = "String";
        model_internal::propertyTypeMap["MOTHERMIDDLENAME"] = "String";
        model_internal::propertyTypeMap["FATHERLASTNAME"] = "String";
        model_internal::propertyTypeMap["MOTHERLASTNAME"] = "String";
        model_internal::propertyTypeMap["FATHERBIRTHDATE"] = "String";
        model_internal::propertyTypeMap["MOTHERBIRTHDATE"] = "String";
        model_internal::propertyTypeMap["GROUPID"] = "String";
        model_internal::propertyTypeMap["RELATIVEID"] = "String";
        model_internal::propertyTypeMap["LEGALGUARDIANID"] = "String";
        model_internal::propertyTypeMap["INSURANCEID"] = "int";
        model_internal::propertyTypeMap["INSURANCECOUNT"] = "int";
        model_internal::propertyTypeMap["TESTPATOLOGYFLAG"] = "int";
        model_internal::propertyTypeMap["COUNTS_TOOTHEXAMS"] = "int";
        model_internal::propertyTypeMap["COUNTS_PROCEDURES_CLOSED"] = "int";
        model_internal::propertyTypeMap["COUNTS_PROCEDURES_NOTCLOSED"] = "int";
        model_internal::propertyTypeMap["COUNTS_FILES"] = "int";
        model_internal::propertyTypeMap["full_addres"] = "String";
        model_internal::propertyTypeMap["full_fio"] = "String";
        model_internal::propertyTypeMap["discount_amount"] = "Number";
        model_internal::propertyTypeMap["discount_amount_auto"] = "Number";
        model_internal::propertyTypeMap["INDDISCOUNT"] = "Boolean";
        model_internal::propertyTypeMap["DISCOUNTCARDNUMBER"] = "String";
        model_internal::propertyTypeMap["is_returned"] = "int";
        model_internal::propertyTypeMap["patient_ages"] = "int";
        model_internal::propertyTypeMap["patient_notes"] = "String";
        model_internal::propertyTypeMap["doctors"] = "String";
        model_internal::propertyTypeMap["doctor_lead"] = "String";
        model_internal::propertyTypeMap["agreementTimestamp"] = "String";
        model_internal::propertyTypeMap["mobiles"] = "ArrayCollection";
        model_internal::propertyTypeMap["is_archived"] = "Boolean";
        model_internal::propertyTypeMap["subdivision_id"] = "String";
        model_internal::propertyTypeMap["subdivision_label"] = "String";
        model_internal::propertyTypeMap["NH_TREATTYPE"] = "int";
        model_internal::propertyTypeMap["EH_DEC_ID"] = "String";
        model_internal::propertyTypeMap["TAXID"] = "String";
        model_internal::propertyTypeMap["agreementtype"] = "int";
        model_internal::propertyTypeMap["birthcert_series"] = "String";
        model_internal::propertyTypeMap["birthcert_number"] = "String";
        model_internal::propertyTypeMap["birthcert_issuing"] = "String";
        model_internal::propertyTypeMap["birthcert_date"] = "String";
        model_internal::propertyTypeMap["IS_MOTHER"] = "int";
        model_internal::propertyTypeMap["MOTHER_ID"] = "int";
        model_internal::propertyTypeMap["MOTHER_PASSPORT_SERIES"] = "String";
        model_internal::propertyTypeMap["MOTHER_PASSPORT_NUMBER"] = "String";
        model_internal::propertyTypeMap["MOTHER_PASSPORT_ISSUING"] = "String";
        model_internal::propertyTypeMap["MOTHER_PASSPORT_DATE"] = "String";
        model_internal::propertyTypeMap["FATHER_ID"] = "int";
        model_internal::propertyTypeMap["FATHER_PASSPORT_SERIES"] = "String";
        model_internal::propertyTypeMap["FATHER_PASSPORT_NUMBER"] = "String";
        model_internal::propertyTypeMap["FATHER_PASSPORT_ISSUING"] = "String";
        model_internal::propertyTypeMap["FATHER_PASSPORT_DATE"] = "String";
        model_internal::propertyTypeMap["CONFLICTFLAG"] = "Boolean";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity PatientCardModuleAddPatientObject");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity PatientCardModuleAddPatientObject");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of PatientCardModuleAddPatientObject");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardModuleAddPatientObject");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity PatientCardModuleAddPatientObject");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity PatientCardModuleAddPatientObject");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDNUMBER_FULLAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDBEFORNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDAFTERNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFIRSTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMIDDLENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLASTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFULLNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBIRTHDAYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSEXAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isADDRESSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPOSTALCODEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTELEPHONENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOBILENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEMPLOYMENTPLACEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isJOBAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWORKPHONEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVILLAGEORCITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPOPULATIONGROUPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isISDISPANCERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isHOBBYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTRYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSTATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRAGIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEMAILAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCITYAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPASSPORT_SERIESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPASSPORT_NUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPASSPORT_ISSUINGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPASSPORT_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWHEREFROMIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHERFIRSTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHERFIRSTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHERMIDDLENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHERMIDDLENAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHERLASTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHERLASTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHERBIRTHDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHERBIRTHDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGROUPIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRELATIVEIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLEGALGUARDIANIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINSURANCEIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINSURANCECOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTESTPATOLOGYFLAGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_TOOTHEXAMSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_PROCEDURES_CLOSEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_PROCEDURES_NOTCLOSEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCOUNTS_FILESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFull_addresAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFull_fioAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_amountAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDiscount_amount_autoAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isINDDISCOUNTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDISCOUNTCARDNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIs_returnedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_agesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPatient_notesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctorsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDoctor_leadAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAgreementTimestampAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMobilesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIs_archivedAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_idAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSubdivision_labelAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNH_TREATTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEH_DEC_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTAXIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAgreementtypeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBirthcert_seriesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBirthcert_numberAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBirthcert_issuingAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isBirthcert_dateAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIS_MOTHERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHER_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHER_PASSPORT_SERIESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHER_PASSPORT_NUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHER_PASSPORT_ISSUINGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMOTHER_PASSPORT_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHER_IDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHER_PASSPORT_SERIESAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHER_PASSPORT_NUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHER_PASSPORT_ISSUINGAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFATHER_PASSPORT_DATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCONFLICTFLAGAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDNUMBER_FULLStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDBEFORNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDAFTERNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FIRSTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MIDDLENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LASTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FULLNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get BIRTHDAYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SEXStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ADDRESSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get POSTALCODEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TELEPHONENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOBILENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EMPLOYMENTPLACEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get JOBStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WORKPHONEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get VILLAGEORCITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get POPULATIONGROUPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ISDISPANCERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get HOBBYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTRYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get STATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get RAGIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EMAILStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CITYStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PASSPORT_SERIESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PASSPORT_NUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PASSPORT_ISSUINGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PASSPORT_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WHEREFROMIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHERFIRSTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHERFIRSTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHERMIDDLENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHERMIDDLENAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHERLASTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHERLASTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHERBIRTHDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHERBIRTHDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get GROUPIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get RELATIVEIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LEGALGUARDIANIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INSURANCEIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INSURANCECOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TESTPATOLOGYFLAGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_TOOTHEXAMSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_PROCEDURES_CLOSEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_PROCEDURES_NOTCLOSEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get COUNTS_FILESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get full_addresStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get full_fioStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_amountStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get discount_amount_autoStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get INDDISCOUNTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DISCOUNTCARDNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get is_returnedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_agesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get patient_notesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctorsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get doctor_leadStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get agreementTimestampStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get mobilesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get is_archivedStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_idStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get subdivision_labelStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get NH_TREATTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get EH_DEC_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TAXIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get agreementtypeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get birthcert_seriesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get birthcert_numberStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get birthcert_issuingStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get birthcert_dateStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get IS_MOTHERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHER_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHER_PASSPORT_SERIESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHER_PASSPORT_NUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHER_PASSPORT_ISSUINGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MOTHER_PASSPORT_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHER_IDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHER_PASSPORT_SERIESStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHER_PASSPORT_NUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHER_PASSPORT_ISSUINGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FATHER_PASSPORT_DATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CONFLICTFLAGStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
