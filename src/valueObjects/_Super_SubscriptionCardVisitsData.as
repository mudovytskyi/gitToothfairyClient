/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - SubscriptionCardVisitsData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_SubscriptionCardVisitsData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("SubscriptionCardVisitsData") == null)
            {
                flash.net.registerClassAlias("SubscriptionCardVisitsData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("SubscriptionCardVisitsData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _SubscriptionCardVisitsDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_SUBSCRIPTION_CARD_ID : String;
    private var _internal_SUBSCRIPTION_NAME : String;
    private var _internal_DOCTOR_NAME : String;
    private var _internal_DATE_OF_VISIT : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_SubscriptionCardVisitsData()
    {
        _model = new _SubscriptionCardVisitsDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get SUBSCRIPTION_CARD_ID() : String
    {
        return _internal_SUBSCRIPTION_CARD_ID;
    }

    [Bindable(event="propertyChange")]
    public function get SUBSCRIPTION_NAME() : String
    {
        return _internal_SUBSCRIPTION_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_NAME() : String
    {
        return _internal_DOCTOR_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get DATE_OF_VISIT() : String
    {
        return _internal_DATE_OF_VISIT;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set SUBSCRIPTION_CARD_ID(value:String) : void
    {
        var oldValue:String = _internal_SUBSCRIPTION_CARD_ID;
        if (oldValue !== value)
        {
            _internal_SUBSCRIPTION_CARD_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUBSCRIPTION_CARD_ID", oldValue, _internal_SUBSCRIPTION_CARD_ID));
        }
    }

    public function set SUBSCRIPTION_NAME(value:String) : void
    {
        var oldValue:String = _internal_SUBSCRIPTION_NAME;
        if (oldValue !== value)
        {
            _internal_SUBSCRIPTION_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUBSCRIPTION_NAME", oldValue, _internal_SUBSCRIPTION_NAME));
        }
    }

    public function set DOCTOR_NAME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_NAME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_NAME", oldValue, _internal_DOCTOR_NAME));
        }
    }

    public function set DATE_OF_VISIT(value:String) : void
    {
        var oldValue:String = _internal_DATE_OF_VISIT;
        if (oldValue !== value)
        {
            _internal_DATE_OF_VISIT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DATE_OF_VISIT", oldValue, _internal_DATE_OF_VISIT));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _SubscriptionCardVisitsDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _SubscriptionCardVisitsDataEntityMetadata) : void
    {
        var oldValue : _SubscriptionCardVisitsDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
