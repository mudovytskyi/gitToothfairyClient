/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - AdminModuleAccountScienceDegree.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_AdminModuleAccountScienceDegree extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("AdminModuleAccountScienceDegree") == null)
            {
                flash.net.registerClassAlias("AdminModuleAccountScienceDegree", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("AdminModuleAccountScienceDegree", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _AdminModuleAccountScienceDegreeEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_diploma_name : String;
    private var _internal_id : String;
    private var _internal_account_fk : String;
    private var _internal_country : String;
    private var _internal_city : String;
    private var _internal_degree : String;
    private var _internal_institution_name : String;
    private var _internal_diploma_number : String;
    private var _internal_speciality : String;
    private var _internal_issued_date : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_AdminModuleAccountScienceDegree()
    {
        _model = new _AdminModuleAccountScienceDegreeEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get diploma_name() : String
    {
        return _internal_diploma_name;
    }

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get account_fk() : String
    {
        return _internal_account_fk;
    }

    [Bindable(event="propertyChange")]
    public function get country() : String
    {
        return _internal_country;
    }

    [Bindable(event="propertyChange")]
    public function get city() : String
    {
        return _internal_city;
    }

    [Bindable(event="propertyChange")]
    public function get degree() : String
    {
        return _internal_degree;
    }

    [Bindable(event="propertyChange")]
    public function get institution_name() : String
    {
        return _internal_institution_name;
    }

    [Bindable(event="propertyChange")]
    public function get diploma_number() : String
    {
        return _internal_diploma_number;
    }

    [Bindable(event="propertyChange")]
    public function get speciality() : String
    {
        return _internal_speciality;
    }

    [Bindable(event="propertyChange")]
    public function get issued_date() : String
    {
        return _internal_issued_date;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set diploma_name(value:String) : void
    {
        var oldValue:String = _internal_diploma_name;
        if (oldValue !== value)
        {
            _internal_diploma_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diploma_name", oldValue, _internal_diploma_name));
        }
    }

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set account_fk(value:String) : void
    {
        var oldValue:String = _internal_account_fk;
        if (oldValue !== value)
        {
            _internal_account_fk = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "account_fk", oldValue, _internal_account_fk));
        }
    }

    public function set country(value:String) : void
    {
        var oldValue:String = _internal_country;
        if (oldValue !== value)
        {
            _internal_country = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "country", oldValue, _internal_country));
        }
    }

    public function set city(value:String) : void
    {
        var oldValue:String = _internal_city;
        if (oldValue !== value)
        {
            _internal_city = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "city", oldValue, _internal_city));
        }
    }

    public function set degree(value:String) : void
    {
        var oldValue:String = _internal_degree;
        if (oldValue !== value)
        {
            _internal_degree = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "degree", oldValue, _internal_degree));
        }
    }

    public function set institution_name(value:String) : void
    {
        var oldValue:String = _internal_institution_name;
        if (oldValue !== value)
        {
            _internal_institution_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "institution_name", oldValue, _internal_institution_name));
        }
    }

    public function set diploma_number(value:String) : void
    {
        var oldValue:String = _internal_diploma_number;
        if (oldValue !== value)
        {
            _internal_diploma_number = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "diploma_number", oldValue, _internal_diploma_number));
        }
    }

    public function set speciality(value:String) : void
    {
        var oldValue:String = _internal_speciality;
        if (oldValue !== value)
        {
            _internal_speciality = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "speciality", oldValue, _internal_speciality));
        }
    }

    public function set issued_date(value:String) : void
    {
        var oldValue:String = _internal_issued_date;
        if (oldValue !== value)
        {
            _internal_issued_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "issued_date", oldValue, _internal_issued_date));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _AdminModuleAccountScienceDegreeEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _AdminModuleAccountScienceDegreeEntityMetadata) : void
    {
        var oldValue : _AdminModuleAccountScienceDegreeEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
