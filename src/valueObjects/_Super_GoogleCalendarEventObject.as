/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - GoogleCalendarEventObject.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.GoogleCalendarEventDateTimeObject;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_GoogleCalendarEventObject extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("GoogleCalendarEventObject") == null)
            {
                flash.net.registerClassAlias("GoogleCalendarEventObject", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("GoogleCalendarEventObject", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.GoogleCalendarEventDateTimeObject.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _GoogleCalendarEventObjectEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_TASK_ID : String;
    private var _internal_startEvent : valueObjects.GoogleCalendarEventDateTimeObject;
    private var _internal_endEvent : valueObjects.GoogleCalendarEventDateTimeObject;
    private var _internal_SUMMARY : String;
    private var _internal_LOCATION : String;
    private var _internal_DESCRIPTION : String;
    private var _internal_STATUS : String;
    private var _internal_COLOR_ID : int;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_GoogleCalendarEventObject()
    {
        _model = new _GoogleCalendarEventObjectEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get TASK_ID() : String
    {
        return _internal_TASK_ID;
    }

    [Bindable(event="propertyChange")]
    public function get startEvent() : valueObjects.GoogleCalendarEventDateTimeObject
    {
        return _internal_startEvent;
    }

    [Bindable(event="propertyChange")]
    public function get endEvent() : valueObjects.GoogleCalendarEventDateTimeObject
    {
        return _internal_endEvent;
    }

    [Bindable(event="propertyChange")]
    public function get SUMMARY() : String
    {
        return _internal_SUMMARY;
    }

    [Bindable(event="propertyChange")]
    public function get LOCATION() : String
    {
        return _internal_LOCATION;
    }

    [Bindable(event="propertyChange")]
    public function get DESCRIPTION() : String
    {
        return _internal_DESCRIPTION;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS() : String
    {
        return _internal_STATUS;
    }

    [Bindable(event="propertyChange")]
    public function get COLOR_ID() : int
    {
        return _internal_COLOR_ID;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set TASK_ID(value:String) : void
    {
        var oldValue:String = _internal_TASK_ID;
        if (oldValue !== value)
        {
            _internal_TASK_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TASK_ID", oldValue, _internal_TASK_ID));
        }
    }

    public function set startEvent(value:valueObjects.GoogleCalendarEventDateTimeObject) : void
    {
        var oldValue:valueObjects.GoogleCalendarEventDateTimeObject = _internal_startEvent;
        if (oldValue !== value)
        {
            _internal_startEvent = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "startEvent", oldValue, _internal_startEvent));
        }
    }

    public function set endEvent(value:valueObjects.GoogleCalendarEventDateTimeObject) : void
    {
        var oldValue:valueObjects.GoogleCalendarEventDateTimeObject = _internal_endEvent;
        if (oldValue !== value)
        {
            _internal_endEvent = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "endEvent", oldValue, _internal_endEvent));
        }
    }

    public function set SUMMARY(value:String) : void
    {
        var oldValue:String = _internal_SUMMARY;
        if (oldValue !== value)
        {
            _internal_SUMMARY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMMARY", oldValue, _internal_SUMMARY));
        }
    }

    public function set LOCATION(value:String) : void
    {
        var oldValue:String = _internal_LOCATION;
        if (oldValue !== value)
        {
            _internal_LOCATION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "LOCATION", oldValue, _internal_LOCATION));
        }
    }

    public function set DESCRIPTION(value:String) : void
    {
        var oldValue:String = _internal_DESCRIPTION;
        if (oldValue !== value)
        {
            _internal_DESCRIPTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DESCRIPTION", oldValue, _internal_DESCRIPTION));
        }
    }

    public function set STATUS(value:String) : void
    {
        var oldValue:String = _internal_STATUS;
        if (oldValue !== value)
        {
            _internal_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS", oldValue, _internal_STATUS));
        }
    }

    public function set COLOR_ID(value:int) : void
    {
        var oldValue:int = _internal_COLOR_ID;
        if (oldValue !== value)
        {
            _internal_COLOR_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "COLOR_ID", oldValue, _internal_COLOR_ID));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _GoogleCalendarEventObjectEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _GoogleCalendarEventObjectEntityMetadata) : void
    {
        var oldValue : _GoogleCalendarEventObjectEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
