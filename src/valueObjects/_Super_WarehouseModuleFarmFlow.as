/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - WarehouseModuleFarmFlow.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.WarehouseModuleFarmPrice;
import valueObjects.WarehouseModuleTPFarm;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_WarehouseModuleFarmFlow extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("WarehouseModuleFarmFlow") == null)
            {
                flash.net.registerClassAlias("WarehouseModuleFarmFlow", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("WarehouseModuleFarmFlow", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.WarehouseModuleFarmPrice.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarm.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleTPFarm.initRemoteClassAliasSingleChild();
        valueObjects.WarehouseModuleFarmUnit.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _WarehouseModuleFarmFlowEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_FARMDOCUMENTID : String;
    private var _internal_FARMPRICE : valueObjects.WarehouseModuleFarmPrice;
    private var _internal_QUANTITY : Number;
    private var _internal_CARDQUANTITY : Number;
    private var _internal_TPFARM : valueObjects.WarehouseModuleTPFarm;
    private var _internal_STATUS : String;
    private var _internal_SUMM : Number;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_WarehouseModuleFarmFlow()
    {
        _model = new _WarehouseModuleFarmFlowEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FARMDOCUMENTID() : String
    {
        return _internal_FARMDOCUMENTID;
    }

    [Bindable(event="propertyChange")]
    public function get FARMPRICE() : valueObjects.WarehouseModuleFarmPrice
    {
        return _internal_FARMPRICE;
    }

    [Bindable(event="propertyChange")]
    public function get QUANTITY() : Number
    {
        return _internal_QUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get CARDQUANTITY() : Number
    {
        return _internal_CARDQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get TPFARM() : valueObjects.WarehouseModuleTPFarm
    {
        return _internal_TPFARM;
    }

    [Bindable(event="propertyChange")]
    public function get STATUS() : String
    {
        return _internal_STATUS;
    }

    [Bindable(event="propertyChange")]
    public function get SUMM() : Number
    {
        return _internal_SUMM;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set FARMDOCUMENTID(value:String) : void
    {
        var oldValue:String = _internal_FARMDOCUMENTID;
        if (oldValue !== value)
        {
            _internal_FARMDOCUMENTID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMDOCUMENTID", oldValue, _internal_FARMDOCUMENTID));
        }
    }

    public function set FARMPRICE(value:valueObjects.WarehouseModuleFarmPrice) : void
    {
        var oldValue:valueObjects.WarehouseModuleFarmPrice = _internal_FARMPRICE;
        if (oldValue !== value)
        {
            _internal_FARMPRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMPRICE", oldValue, _internal_FARMPRICE));
        }
    }

    public function set QUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_QUANTITY;
        if (isNaN(_internal_QUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_QUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "QUANTITY", oldValue, _internal_QUANTITY));
        }
    }

    public function set CARDQUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_CARDQUANTITY;
        if (isNaN(_internal_CARDQUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_CARDQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CARDQUANTITY", oldValue, _internal_CARDQUANTITY));
        }
    }

    public function set TPFARM(value:valueObjects.WarehouseModuleTPFarm) : void
    {
        var oldValue:valueObjects.WarehouseModuleTPFarm = _internal_TPFARM;
        if (oldValue !== value)
        {
            _internal_TPFARM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TPFARM", oldValue, _internal_TPFARM));
        }
    }

    public function set STATUS(value:String) : void
    {
        var oldValue:String = _internal_STATUS;
        if (oldValue !== value)
        {
            _internal_STATUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "STATUS", oldValue, _internal_STATUS));
        }
    }

    public function set SUMM(value:Number) : void
    {
        var oldValue:Number = _internal_SUMM;
        if (isNaN(_internal_SUMM) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_SUMM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMM", oldValue, _internal_SUMM));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _WarehouseModuleFarmFlowEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _WarehouseModuleFarmFlowEntityMetadata) : void
    {
        var oldValue : _WarehouseModuleFarmFlowEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
