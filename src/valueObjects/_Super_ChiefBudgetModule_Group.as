/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - ChiefBudgetModule_Group.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_ChiefBudgetModule_Group extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("ChiefBudgetModule_Group") == null)
            {
                flash.net.registerClassAlias("ChiefBudgetModule_Group", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("ChiefBudgetModule_Group", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _ChiefBudgetModule_GroupEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_id : String;
    private var _internal_name : String;
    private var _internal_group_type : int;
    private var _internal_rules : int;
    private var _internal_is_cashpayment : int;
    private var _internal_monthly_is : int;
    private var _internal_monthly_defaultsumm : Number;
    private var _internal_group_color : int;
    private var _internal_group_sequince : int;
    private var _internal_group_residue : String;
    private var _internal_group_residue_name : String;
    private var _internal_isEmployee : int;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_ChiefBudgetModule_Group()
    {
        _model = new _ChiefBudgetModule_GroupEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get name() : String
    {
        return _internal_name;
    }

    [Bindable(event="propertyChange")]
    public function get group_type() : int
    {
        return _internal_group_type;
    }

    [Bindable(event="propertyChange")]
    public function get rules() : int
    {
        return _internal_rules;
    }

    [Bindable(event="propertyChange")]
    public function get is_cashpayment() : int
    {
        return _internal_is_cashpayment;
    }

    [Bindable(event="propertyChange")]
    public function get monthly_is() : int
    {
        return _internal_monthly_is;
    }

    [Bindable(event="propertyChange")]
    public function get monthly_defaultsumm() : Number
    {
        return _internal_monthly_defaultsumm;
    }

    [Bindable(event="propertyChange")]
    public function get group_color() : int
    {
        return _internal_group_color;
    }

    [Bindable(event="propertyChange")]
    public function get group_sequince() : int
    {
        return _internal_group_sequince;
    }

    [Bindable(event="propertyChange")]
    public function get group_residue() : String
    {
        return _internal_group_residue;
    }

    [Bindable(event="propertyChange")]
    public function get group_residue_name() : String
    {
        return _internal_group_residue_name;
    }

    [Bindable(event="propertyChange")]
    public function get isEmployee() : int
    {
        return _internal_isEmployee;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set name(value:String) : void
    {
        var oldValue:String = _internal_name;
        if (oldValue !== value)
        {
            _internal_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "name", oldValue, _internal_name));
        }
    }

    public function set group_type(value:int) : void
    {
        var oldValue:int = _internal_group_type;
        if (oldValue !== value)
        {
            _internal_group_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_type", oldValue, _internal_group_type));
        }
    }

    public function set rules(value:int) : void
    {
        var oldValue:int = _internal_rules;
        if (oldValue !== value)
        {
            _internal_rules = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rules", oldValue, _internal_rules));
        }
    }

    public function set is_cashpayment(value:int) : void
    {
        var oldValue:int = _internal_is_cashpayment;
        if (oldValue !== value)
        {
            _internal_is_cashpayment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "is_cashpayment", oldValue, _internal_is_cashpayment));
        }
    }

    public function set monthly_is(value:int) : void
    {
        var oldValue:int = _internal_monthly_is;
        if (oldValue !== value)
        {
            _internal_monthly_is = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "monthly_is", oldValue, _internal_monthly_is));
        }
    }

    public function set monthly_defaultsumm(value:Number) : void
    {
        var oldValue:Number = _internal_monthly_defaultsumm;
        if (isNaN(_internal_monthly_defaultsumm) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_monthly_defaultsumm = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "monthly_defaultsumm", oldValue, _internal_monthly_defaultsumm));
        }
    }

    public function set group_color(value:int) : void
    {
        var oldValue:int = _internal_group_color;
        if (oldValue !== value)
        {
            _internal_group_color = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_color", oldValue, _internal_group_color));
        }
    }

    public function set group_sequince(value:int) : void
    {
        var oldValue:int = _internal_group_sequince;
        if (oldValue !== value)
        {
            _internal_group_sequince = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_sequince", oldValue, _internal_group_sequince));
        }
    }

    public function set group_residue(value:String) : void
    {
        var oldValue:String = _internal_group_residue;
        if (oldValue !== value)
        {
            _internal_group_residue = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_residue", oldValue, _internal_group_residue));
        }
    }

    public function set group_residue_name(value:String) : void
    {
        var oldValue:String = _internal_group_residue_name;
        if (oldValue !== value)
        {
            _internal_group_residue_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group_residue_name", oldValue, _internal_group_residue_name));
        }
    }

    public function set isEmployee(value:int) : void
    {
        var oldValue:int = _internal_isEmployee;
        if (oldValue !== value)
        {
            _internal_isEmployee = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isEmployee", oldValue, _internal_isEmployee));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ChiefBudgetModule_GroupEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ChiefBudgetModule_GroupEntityMetadata) : void
    {
        var oldValue : _ChiefBudgetModule_GroupEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
