/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - StorageWriteOff_OLAPData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_StorageWriteOff_OLAPData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("StorageWriteOff_OLAPData") == null)
            {
                flash.net.registerClassAlias("StorageWriteOff_OLAPData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("StorageWriteOff_OLAPData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _StorageWriteOff_OLAPDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_treatprocfarm_id : String;
    private var _internal_cabinet_id : String;
    private var _internal_cabinet_label : String;
    private var _internal_doctor_id : String;
    private var _internal_doctor_shortname : String;
    private var _internal_patient_id : String;
    private var _internal_patient_shortname : String;
    private var _internal_patient_cardnumber : String;
    private var _internal_treatproc_id : String;
    private var _internal_treatproc_label : String;
    private var _internal_treatproc_count : int;
    private var _internal_treatproc_dateclose : String;
    private var _internal_farm_id : String;
    private var _internal_farm_label : String;
    private var _internal_farm_count : Number;
    private var _internal_farm_countFull : Number;
    private var _internal_farm_dateclose : String;
    private var _internal_farmdoc_id : String;
    private var _internal_farm_unitLabel : String;
    private var _internal_farm_remains : Number;
    private var _internal_farm_remainsIndex : Number;
    private var _internal_subdivision_id : String;
    private var _internal_subdivision_name : String;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_StorageWriteOff_OLAPData()
    {
        _model = new _StorageWriteOff_OLAPDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get treatprocfarm_id() : String
    {
        return _internal_treatprocfarm_id;
    }

    [Bindable(event="propertyChange")]
    public function get cabinet_id() : String
    {
        return _internal_cabinet_id;
    }

    [Bindable(event="propertyChange")]
    public function get cabinet_label() : String
    {
        return _internal_cabinet_label;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_shortname() : String
    {
        return _internal_doctor_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_id() : String
    {
        return _internal_patient_id;
    }

    [Bindable(event="propertyChange")]
    public function get patient_shortname() : String
    {
        return _internal_patient_shortname;
    }

    [Bindable(event="propertyChange")]
    public function get patient_cardnumber() : String
    {
        return _internal_patient_cardnumber;
    }

    [Bindable(event="propertyChange")]
    public function get treatproc_id() : String
    {
        return _internal_treatproc_id;
    }

    [Bindable(event="propertyChange")]
    public function get treatproc_label() : String
    {
        return _internal_treatproc_label;
    }

    [Bindable(event="propertyChange")]
    public function get treatproc_count() : int
    {
        return _internal_treatproc_count;
    }

    [Bindable(event="propertyChange")]
    public function get treatproc_dateclose() : String
    {
        return _internal_treatproc_dateclose;
    }

    [Bindable(event="propertyChange")]
    public function get farm_id() : String
    {
        return _internal_farm_id;
    }

    [Bindable(event="propertyChange")]
    public function get farm_label() : String
    {
        return _internal_farm_label;
    }

    [Bindable(event="propertyChange")]
    public function get farm_count() : Number
    {
        return _internal_farm_count;
    }

    [Bindable(event="propertyChange")]
    public function get farm_countFull() : Number
    {
        return _internal_farm_countFull;
    }

    [Bindable(event="propertyChange")]
    public function get farm_dateclose() : String
    {
        return _internal_farm_dateclose;
    }

    [Bindable(event="propertyChange")]
    public function get farmdoc_id() : String
    {
        return _internal_farmdoc_id;
    }

    [Bindable(event="propertyChange")]
    public function get farm_unitLabel() : String
    {
        return _internal_farm_unitLabel;
    }

    [Bindable(event="propertyChange")]
    public function get farm_remains() : Number
    {
        return _internal_farm_remains;
    }

    [Bindable(event="propertyChange")]
    public function get farm_remainsIndex() : Number
    {
        return _internal_farm_remainsIndex;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_id() : String
    {
        return _internal_subdivision_id;
    }

    [Bindable(event="propertyChange")]
    public function get subdivision_name() : String
    {
        return _internal_subdivision_name;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set treatprocfarm_id(value:String) : void
    {
        var oldValue:String = _internal_treatprocfarm_id;
        if (oldValue !== value)
        {
            _internal_treatprocfarm_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatprocfarm_id", oldValue, _internal_treatprocfarm_id));
        }
    }

    public function set cabinet_id(value:String) : void
    {
        var oldValue:String = _internal_cabinet_id;
        if (oldValue !== value)
        {
            _internal_cabinet_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cabinet_id", oldValue, _internal_cabinet_id));
        }
    }

    public function set cabinet_label(value:String) : void
    {
        var oldValue:String = _internal_cabinet_label;
        if (oldValue !== value)
        {
            _internal_cabinet_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "cabinet_label", oldValue, _internal_cabinet_label));
        }
    }

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_shortname(value:String) : void
    {
        var oldValue:String = _internal_doctor_shortname;
        if (oldValue !== value)
        {
            _internal_doctor_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_shortname", oldValue, _internal_doctor_shortname));
        }
    }

    public function set patient_id(value:String) : void
    {
        var oldValue:String = _internal_patient_id;
        if (oldValue !== value)
        {
            _internal_patient_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_id", oldValue, _internal_patient_id));
        }
    }

    public function set patient_shortname(value:String) : void
    {
        var oldValue:String = _internal_patient_shortname;
        if (oldValue !== value)
        {
            _internal_patient_shortname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_shortname", oldValue, _internal_patient_shortname));
        }
    }

    public function set patient_cardnumber(value:String) : void
    {
        var oldValue:String = _internal_patient_cardnumber;
        if (oldValue !== value)
        {
            _internal_patient_cardnumber = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "patient_cardnumber", oldValue, _internal_patient_cardnumber));
        }
    }

    public function set treatproc_id(value:String) : void
    {
        var oldValue:String = _internal_treatproc_id;
        if (oldValue !== value)
        {
            _internal_treatproc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatproc_id", oldValue, _internal_treatproc_id));
        }
    }

    public function set treatproc_label(value:String) : void
    {
        var oldValue:String = _internal_treatproc_label;
        if (oldValue !== value)
        {
            _internal_treatproc_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatproc_label", oldValue, _internal_treatproc_label));
        }
    }

    public function set treatproc_count(value:int) : void
    {
        var oldValue:int = _internal_treatproc_count;
        if (oldValue !== value)
        {
            _internal_treatproc_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatproc_count", oldValue, _internal_treatproc_count));
        }
    }

    public function set treatproc_dateclose(value:String) : void
    {
        var oldValue:String = _internal_treatproc_dateclose;
        if (oldValue !== value)
        {
            _internal_treatproc_dateclose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "treatproc_dateclose", oldValue, _internal_treatproc_dateclose));
        }
    }

    public function set farm_id(value:String) : void
    {
        var oldValue:String = _internal_farm_id;
        if (oldValue !== value)
        {
            _internal_farm_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_id", oldValue, _internal_farm_id));
        }
    }

    public function set farm_label(value:String) : void
    {
        var oldValue:String = _internal_farm_label;
        if (oldValue !== value)
        {
            _internal_farm_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_label", oldValue, _internal_farm_label));
        }
    }

    public function set farm_count(value:Number) : void
    {
        var oldValue:Number = _internal_farm_count;
        if (isNaN(_internal_farm_count) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_farm_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_count", oldValue, _internal_farm_count));
        }
    }

    public function set farm_countFull(value:Number) : void
    {
        var oldValue:Number = _internal_farm_countFull;
        if (isNaN(_internal_farm_countFull) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_farm_countFull = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_countFull", oldValue, _internal_farm_countFull));
        }
    }

    public function set farm_dateclose(value:String) : void
    {
        var oldValue:String = _internal_farm_dateclose;
        if (oldValue !== value)
        {
            _internal_farm_dateclose = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_dateclose", oldValue, _internal_farm_dateclose));
        }
    }

    public function set farmdoc_id(value:String) : void
    {
        var oldValue:String = _internal_farmdoc_id;
        if (oldValue !== value)
        {
            _internal_farmdoc_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farmdoc_id", oldValue, _internal_farmdoc_id));
        }
    }

    public function set farm_unitLabel(value:String) : void
    {
        var oldValue:String = _internal_farm_unitLabel;
        if (oldValue !== value)
        {
            _internal_farm_unitLabel = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_unitLabel", oldValue, _internal_farm_unitLabel));
        }
    }

    public function set farm_remains(value:Number) : void
    {
        var oldValue:Number = _internal_farm_remains;
        if (isNaN(_internal_farm_remains) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_farm_remains = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_remains", oldValue, _internal_farm_remains));
        }
    }

    public function set farm_remainsIndex(value:Number) : void
    {
        var oldValue:Number = _internal_farm_remainsIndex;
        if (isNaN(_internal_farm_remainsIndex) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_farm_remainsIndex = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "farm_remainsIndex", oldValue, _internal_farm_remainsIndex));
        }
    }

    public function set subdivision_id(value:String) : void
    {
        var oldValue:String = _internal_subdivision_id;
        if (oldValue !== value)
        {
            _internal_subdivision_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_id", oldValue, _internal_subdivision_id));
        }
    }

    public function set subdivision_name(value:String) : void
    {
        var oldValue:String = _internal_subdivision_name;
        if (oldValue !== value)
        {
            _internal_subdivision_name = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "subdivision_name", oldValue, _internal_subdivision_name));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _StorageWriteOff_OLAPDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _StorageWriteOff_OLAPDataEntityMetadata) : void
    {
        var oldValue : _StorageWriteOff_OLAPDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
