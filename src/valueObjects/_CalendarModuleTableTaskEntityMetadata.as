
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import valueObjects.CalendarModuleInsurancePolisObject;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _CalendarModuleTableTaskEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("TASKID", "TASKTASKDATE", "TASKBEGINOFTHEINTERVAL", "TASKENDOFTHEINTERVAL", "TASKPATIENTID", "TASKDOCTORID", "TASKROOMID", "TASKWORKPLACEID", "TASKNOTICED", "TASKFACTOFVISIT", "TASKWORKDESCRIPTION", "TASKCOMMENT", "PATIENTSHORTNAME", "PATIENTFULLNAME", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "DOCTORSHORTNAME", "ROOMNUMBER", "ROOMNAME", "ROOMCOLORID", "ROOMGOOGLECOLORID", "WORKPLACENUMBER", "PRIMARYPATIENT", "FNAME", "LNAME", "MNAME", "CARDNUMBER", "PSHORTNAME", "insurance", "AUTHORID", "AUTHOR_SHORTANME", "TASKTYPE", "SMSSTATUS", "SMS_SEND_STATUS", "xml_item");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array();
    model_internal static var allAlwaysAvailableProperties:Array = new Array("TASKID", "TASKTASKDATE", "TASKBEGINOFTHEINTERVAL", "TASKENDOFTHEINTERVAL", "TASKPATIENTID", "TASKDOCTORID", "TASKROOMID", "TASKWORKPLACEID", "TASKNOTICED", "TASKFACTOFVISIT", "TASKWORKDESCRIPTION", "TASKCOMMENT", "PATIENTSHORTNAME", "PATIENTFULLNAME", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "DOCTORSHORTNAME", "ROOMNUMBER", "ROOMNAME", "ROOMCOLORID", "ROOMGOOGLECOLORID", "WORKPLACENUMBER", "PRIMARYPATIENT", "FNAME", "LNAME", "MNAME", "CARDNUMBER", "PSHORTNAME", "insurance", "AUTHORID", "AUTHOR_SHORTANME", "TASKTYPE", "SMSSTATUS", "SMS_SEND_STATUS", "xml_item");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("TASKID", "TASKTASKDATE", "TASKBEGINOFTHEINTERVAL", "TASKENDOFTHEINTERVAL", "TASKPATIENTID", "TASKDOCTORID", "TASKROOMID", "TASKWORKPLACEID", "TASKNOTICED", "TASKFACTOFVISIT", "TASKWORKDESCRIPTION", "TASKCOMMENT", "PATIENTSHORTNAME", "PATIENTFULLNAME", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "DOCTORSHORTNAME", "ROOMNUMBER", "ROOMNAME", "ROOMCOLORID", "ROOMGOOGLECOLORID", "WORKPLACENUMBER", "PRIMARYPATIENT", "FNAME", "LNAME", "MNAME", "CARDNUMBER", "PSHORTNAME", "insurance", "AUTHORID", "AUTHOR_SHORTANME", "TASKTYPE", "SMSSTATUS", "SMS_SEND_STATUS", "xml_item");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("TASKID", "TASKTASKDATE", "TASKBEGINOFTHEINTERVAL", "TASKENDOFTHEINTERVAL", "TASKPATIENTID", "TASKDOCTORID", "TASKROOMID", "TASKWORKPLACEID", "TASKNOTICED", "TASKFACTOFVISIT", "TASKWORKDESCRIPTION", "TASKCOMMENT", "PATIENTSHORTNAME", "PATIENTFULLNAME", "PATIENTTELEPHONENUMBER", "PATIENTMOBILENUMBER", "DOCTORSHORTNAME", "ROOMNUMBER", "ROOMNAME", "ROOMCOLORID", "ROOMGOOGLECOLORID", "WORKPLACENUMBER", "PRIMARYPATIENT", "FNAME", "LNAME", "MNAME", "CARDNUMBER", "PSHORTNAME", "insurance", "AUTHORID", "AUTHOR_SHORTANME", "TASKTYPE", "SMSSTATUS", "SMS_SEND_STATUS", "xml_item");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "CalendarModuleTableTask";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;


    model_internal var _instance:_Super_CalendarModuleTableTask;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _CalendarModuleTableTaskEntityMetadata(value : _Super_CalendarModuleTableTask)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["TASKID"] = new Array();
            model_internal::dependentsOnMap["TASKTASKDATE"] = new Array();
            model_internal::dependentsOnMap["TASKBEGINOFTHEINTERVAL"] = new Array();
            model_internal::dependentsOnMap["TASKENDOFTHEINTERVAL"] = new Array();
            model_internal::dependentsOnMap["TASKPATIENTID"] = new Array();
            model_internal::dependentsOnMap["TASKDOCTORID"] = new Array();
            model_internal::dependentsOnMap["TASKROOMID"] = new Array();
            model_internal::dependentsOnMap["TASKWORKPLACEID"] = new Array();
            model_internal::dependentsOnMap["TASKNOTICED"] = new Array();
            model_internal::dependentsOnMap["TASKFACTOFVISIT"] = new Array();
            model_internal::dependentsOnMap["TASKWORKDESCRIPTION"] = new Array();
            model_internal::dependentsOnMap["TASKCOMMENT"] = new Array();
            model_internal::dependentsOnMap["PATIENTSHORTNAME"] = new Array();
            model_internal::dependentsOnMap["PATIENTFULLNAME"] = new Array();
            model_internal::dependentsOnMap["PATIENTTELEPHONENUMBER"] = new Array();
            model_internal::dependentsOnMap["PATIENTMOBILENUMBER"] = new Array();
            model_internal::dependentsOnMap["DOCTORSHORTNAME"] = new Array();
            model_internal::dependentsOnMap["ROOMNUMBER"] = new Array();
            model_internal::dependentsOnMap["ROOMNAME"] = new Array();
            model_internal::dependentsOnMap["ROOMCOLORID"] = new Array();
            model_internal::dependentsOnMap["ROOMGOOGLECOLORID"] = new Array();
            model_internal::dependentsOnMap["WORKPLACENUMBER"] = new Array();
            model_internal::dependentsOnMap["PRIMARYPATIENT"] = new Array();
            model_internal::dependentsOnMap["FNAME"] = new Array();
            model_internal::dependentsOnMap["LNAME"] = new Array();
            model_internal::dependentsOnMap["MNAME"] = new Array();
            model_internal::dependentsOnMap["CARDNUMBER"] = new Array();
            model_internal::dependentsOnMap["PSHORTNAME"] = new Array();
            model_internal::dependentsOnMap["insurance"] = new Array();
            model_internal::dependentsOnMap["AUTHORID"] = new Array();
            model_internal::dependentsOnMap["AUTHOR_SHORTANME"] = new Array();
            model_internal::dependentsOnMap["TASKTYPE"] = new Array();
            model_internal::dependentsOnMap["SMSSTATUS"] = new Array();
            model_internal::dependentsOnMap["SMS_SEND_STATUS"] = new Array();
            model_internal::dependentsOnMap["xml_item"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["TASKID"] = "String";
        model_internal::propertyTypeMap["TASKTASKDATE"] = "String";
        model_internal::propertyTypeMap["TASKBEGINOFTHEINTERVAL"] = "String";
        model_internal::propertyTypeMap["TASKENDOFTHEINTERVAL"] = "String";
        model_internal::propertyTypeMap["TASKPATIENTID"] = "String";
        model_internal::propertyTypeMap["TASKDOCTORID"] = "String";
        model_internal::propertyTypeMap["TASKROOMID"] = "String";
        model_internal::propertyTypeMap["TASKWORKPLACEID"] = "String";
        model_internal::propertyTypeMap["TASKNOTICED"] = "String";
        model_internal::propertyTypeMap["TASKFACTOFVISIT"] = "String";
        model_internal::propertyTypeMap["TASKWORKDESCRIPTION"] = "String";
        model_internal::propertyTypeMap["TASKCOMMENT"] = "String";
        model_internal::propertyTypeMap["PATIENTSHORTNAME"] = "String";
        model_internal::propertyTypeMap["PATIENTFULLNAME"] = "String";
        model_internal::propertyTypeMap["PATIENTTELEPHONENUMBER"] = "String";
        model_internal::propertyTypeMap["PATIENTMOBILENUMBER"] = "String";
        model_internal::propertyTypeMap["DOCTORSHORTNAME"] = "String";
        model_internal::propertyTypeMap["ROOMNUMBER"] = "String";
        model_internal::propertyTypeMap["ROOMNAME"] = "String";
        model_internal::propertyTypeMap["ROOMCOLORID"] = "int";
        model_internal::propertyTypeMap["ROOMGOOGLECOLORID"] = "int";
        model_internal::propertyTypeMap["WORKPLACENUMBER"] = "String";
        model_internal::propertyTypeMap["PRIMARYPATIENT"] = "int";
        model_internal::propertyTypeMap["FNAME"] = "String";
        model_internal::propertyTypeMap["LNAME"] = "String";
        model_internal::propertyTypeMap["MNAME"] = "String";
        model_internal::propertyTypeMap["CARDNUMBER"] = "String";
        model_internal::propertyTypeMap["PSHORTNAME"] = "String";
        model_internal::propertyTypeMap["insurance"] = "valueObjects.CalendarModuleInsurancePolisObject";
        model_internal::propertyTypeMap["AUTHORID"] = "String";
        model_internal::propertyTypeMap["AUTHOR_SHORTANME"] = "String";
        model_internal::propertyTypeMap["TASKTYPE"] = "int";
        model_internal::propertyTypeMap["SMSSTATUS"] = "int";
        model_internal::propertyTypeMap["SMS_SEND_STATUS"] = "int";
        model_internal::propertyTypeMap["xml_item"] = "String";

        model_internal::_instance = value;
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity CalendarModuleTableTask");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity CalendarModuleTableTask");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of CalendarModuleTableTask");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity CalendarModuleTableTask");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity CalendarModuleTableTask");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity CalendarModuleTableTask");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isTASKIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKTASKDATEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKBEGINOFTHEINTERVALAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKENDOFTHEINTERVALAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKPATIENTIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKDOCTORIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKROOMIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKWORKPLACEIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKNOTICEDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKFACTOFVISITAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKWORKDESCRIPTIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKCOMMENTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTSHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTFULLNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTTELEPHONENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPATIENTMOBILENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDOCTORSHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isROOMNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isROOMNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isROOMCOLORIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isROOMGOOGLECOLORIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWORKPLACENUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPRIMARYPATIENTAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCARDNUMBERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isPSHORTNAMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInsuranceAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAUTHORIDAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isAUTHOR_SHORTANMEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTASKTYPEAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMSSTATUSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSMS_SEND_STATUSAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isXml_itemAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get TASKIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKTASKDATEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKBEGINOFTHEINTERVALStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKENDOFTHEINTERVALStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKPATIENTIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKDOCTORIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKROOMIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKWORKPLACEIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKNOTICEDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKFACTOFVISITStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKWORKDESCRIPTIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKCOMMENTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTSHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTFULLNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTTELEPHONENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PATIENTMOBILENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get DOCTORSHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ROOMNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ROOMNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ROOMCOLORIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get ROOMGOOGLECOLORIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get WORKPLACENUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PRIMARYPATIENTStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get FNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get LNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get MNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get CARDNUMBERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get PSHORTNAMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get insuranceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AUTHORIDStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get AUTHOR_SHORTANMEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get TASKTYPEStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMSSTATUSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get SMS_SEND_STATUSStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get xml_itemStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
