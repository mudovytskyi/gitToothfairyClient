/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - DispanserModule_PatientDispancerRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.DispanserModule_Disptype;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_DispanserModule_PatientDispancerRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("DispanserModule_PatientDispancerRecord") == null)
            {
                flash.net.registerClassAlias("DispanserModule_PatientDispancerRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("DispanserModule_PatientDispancerRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.DispanserModule_Disptype.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _DispanserModule_PatientDispancerRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_doctor_id : String;
    private var _internal_doctor_sname : String;
    private var _internal_doctor_lname : String;
    private var _internal_doctor_fname : String;
    private var _internal_task_id : String;
    private var _internal_task_factofvisit : int;
    private var _internal_task_noticed : int;
    private var _internal_dispanceType : valueObjects.DispanserModule_Disptype;
    private var _internal_dispance_id : String;
    private var _internal_dispance_date : String;
    private var _internal_dispance_comment : String;
    private var _internal_task_date : String;
    private var _internal_smsstatus : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_DispanserModule_PatientDispancerRecord()
    {
        _model = new _DispanserModule_PatientDispancerRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get doctor_id() : String
    {
        return _internal_doctor_id;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_sname() : String
    {
        return _internal_doctor_sname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_lname() : String
    {
        return _internal_doctor_lname;
    }

    [Bindable(event="propertyChange")]
    public function get doctor_fname() : String
    {
        return _internal_doctor_fname;
    }

    [Bindable(event="propertyChange")]
    public function get task_id() : String
    {
        return _internal_task_id;
    }

    [Bindable(event="propertyChange")]
    public function get task_factofvisit() : int
    {
        return _internal_task_factofvisit;
    }

    [Bindable(event="propertyChange")]
    public function get task_noticed() : int
    {
        return _internal_task_noticed;
    }

    [Bindable(event="propertyChange")]
    public function get dispanceType() : valueObjects.DispanserModule_Disptype
    {
        return _internal_dispanceType;
    }

    [Bindable(event="propertyChange")]
    public function get dispance_id() : String
    {
        return _internal_dispance_id;
    }

    [Bindable(event="propertyChange")]
    public function get dispance_date() : String
    {
        return _internal_dispance_date;
    }

    [Bindable(event="propertyChange")]
    public function get dispance_comment() : String
    {
        return _internal_dispance_comment;
    }

    [Bindable(event="propertyChange")]
    public function get task_date() : String
    {
        return _internal_task_date;
    }

    [Bindable(event="propertyChange")]
    public function get smsstatus() : Boolean
    {
        return _internal_smsstatus;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set doctor_id(value:String) : void
    {
        var oldValue:String = _internal_doctor_id;
        if (oldValue !== value)
        {
            _internal_doctor_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_id", oldValue, _internal_doctor_id));
        }
    }

    public function set doctor_sname(value:String) : void
    {
        var oldValue:String = _internal_doctor_sname;
        if (oldValue !== value)
        {
            _internal_doctor_sname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_sname", oldValue, _internal_doctor_sname));
        }
    }

    public function set doctor_lname(value:String) : void
    {
        var oldValue:String = _internal_doctor_lname;
        if (oldValue !== value)
        {
            _internal_doctor_lname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_lname", oldValue, _internal_doctor_lname));
        }
    }

    public function set doctor_fname(value:String) : void
    {
        var oldValue:String = _internal_doctor_fname;
        if (oldValue !== value)
        {
            _internal_doctor_fname = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "doctor_fname", oldValue, _internal_doctor_fname));
        }
    }

    public function set task_id(value:String) : void
    {
        var oldValue:String = _internal_task_id;
        if (oldValue !== value)
        {
            _internal_task_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_id", oldValue, _internal_task_id));
        }
    }

    public function set task_factofvisit(value:int) : void
    {
        var oldValue:int = _internal_task_factofvisit;
        if (oldValue !== value)
        {
            _internal_task_factofvisit = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_factofvisit", oldValue, _internal_task_factofvisit));
        }
    }

    public function set task_noticed(value:int) : void
    {
        var oldValue:int = _internal_task_noticed;
        if (oldValue !== value)
        {
            _internal_task_noticed = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_noticed", oldValue, _internal_task_noticed));
        }
    }

    public function set dispanceType(value:valueObjects.DispanserModule_Disptype) : void
    {
        var oldValue:valueObjects.DispanserModule_Disptype = _internal_dispanceType;
        if (oldValue !== value)
        {
            _internal_dispanceType = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dispanceType", oldValue, _internal_dispanceType));
        }
    }

    public function set dispance_id(value:String) : void
    {
        var oldValue:String = _internal_dispance_id;
        if (oldValue !== value)
        {
            _internal_dispance_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dispance_id", oldValue, _internal_dispance_id));
        }
    }

    public function set dispance_date(value:String) : void
    {
        var oldValue:String = _internal_dispance_date;
        if (oldValue !== value)
        {
            _internal_dispance_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dispance_date", oldValue, _internal_dispance_date));
        }
    }

    public function set dispance_comment(value:String) : void
    {
        var oldValue:String = _internal_dispance_comment;
        if (oldValue !== value)
        {
            _internal_dispance_comment = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dispance_comment", oldValue, _internal_dispance_comment));
        }
    }

    public function set task_date(value:String) : void
    {
        var oldValue:String = _internal_task_date;
        if (oldValue !== value)
        {
            _internal_task_date = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "task_date", oldValue, _internal_task_date));
        }
    }

    public function set smsstatus(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_smsstatus;
        if (oldValue !== value)
        {
            _internal_smsstatus = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "smsstatus", oldValue, _internal_smsstatus));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _DispanserModule_PatientDispancerRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _DispanserModule_PatientDispancerRecordEntityMetadata) : void
    {
        var oldValue : _DispanserModule_PatientDispancerRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
