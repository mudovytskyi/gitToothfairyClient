/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Form039Module_F39_Record.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.Form039Module_F39_Record;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Form039Module_F39_Record extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("Form039Module_F39_Record") == null)
            {
                flash.net.registerClassAlias("Form039Module_F39_Record", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("Form039Module_F39_Record", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.Form039Module_F39_Record.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _Form039Module_F39_RecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_ID : String;
    private var _internal_DESCRIPTION : String;
    private var _internal_NUMBER : int;
    private var _internal_CHILDREN : ArrayCollection;
    model_internal var _internal_CHILDREN_leaf:valueObjects.Form039Module_F39_Record;
    private var _internal_TYPE : int;
    private var _internal_OLDSELECTED : Boolean;
    private var _internal_SELECTED : Boolean;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Form039Module_F39_Record()
    {
        _model = new _Form039Module_F39_RecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get ID() : String
    {
        return _internal_ID;
    }

    [Bindable(event="propertyChange")]
    public function get DESCRIPTION() : String
    {
        return _internal_DESCRIPTION;
    }

    [Bindable(event="propertyChange")]
    public function get NUMBER() : int
    {
        return _internal_NUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get CHILDREN() : ArrayCollection
    {
        return _internal_CHILDREN;
    }

    [Bindable(event="propertyChange")]
    public function get TYPE() : int
    {
        return _internal_TYPE;
    }

    [Bindable(event="propertyChange")]
    public function get OLDSELECTED() : Boolean
    {
        return _internal_OLDSELECTED;
    }

    [Bindable(event="propertyChange")]
    public function get SELECTED() : Boolean
    {
        return _internal_SELECTED;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set ID(value:String) : void
    {
        var oldValue:String = _internal_ID;
        if (oldValue !== value)
        {
            _internal_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ID", oldValue, _internal_ID));
        }
    }

    public function set DESCRIPTION(value:String) : void
    {
        var oldValue:String = _internal_DESCRIPTION;
        if (oldValue !== value)
        {
            _internal_DESCRIPTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DESCRIPTION", oldValue, _internal_DESCRIPTION));
        }
    }

    public function set NUMBER(value:int) : void
    {
        var oldValue:int = _internal_NUMBER;
        if (oldValue !== value)
        {
            _internal_NUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "NUMBER", oldValue, _internal_NUMBER));
        }
    }

    public function set CHILDREN(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_CHILDREN;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_CHILDREN = value;
            }
            else if (value is Array)
            {
                _internal_CHILDREN = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_CHILDREN = null;
            }
            else
            {
                throw new Error("value of CHILDREN must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CHILDREN", oldValue, _internal_CHILDREN));
        }
    }

    public function set TYPE(value:int) : void
    {
        var oldValue:int = _internal_TYPE;
        if (oldValue !== value)
        {
            _internal_TYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TYPE", oldValue, _internal_TYPE));
        }
    }

    public function set OLDSELECTED(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_OLDSELECTED;
        if (oldValue !== value)
        {
            _internal_OLDSELECTED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "OLDSELECTED", oldValue, _internal_OLDSELECTED));
        }
    }

    public function set SELECTED(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_SELECTED;
        if (oldValue !== value)
        {
            _internal_SELECTED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SELECTED", oldValue, _internal_SELECTED));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _Form039Module_F39_RecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _Form039Module_F39_RecordEntityMetadata) : void
    {
        var oldValue : _Form039Module_F39_RecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
