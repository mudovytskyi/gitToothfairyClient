/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - WarehouseModuleOLAPRecord.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_WarehouseModuleOLAPRecord extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("WarehouseModuleOLAPRecord") == null)
            {
                flash.net.registerClassAlias("WarehouseModuleOLAPRecord", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("WarehouseModuleOLAPRecord", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _WarehouseModuleOLAPRecordEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_FARMFLOW_ID : String;
    private var _internal_FARMFLOW_QUANTITY : Number;
    private var _internal_FARMFLOW_INQUANTITY : Number;
    private var _internal_FARMFLOW_OUTQUANTITY : Number;
    private var _internal_FARMFLOW_BEFOREQUANTITY : Number;
    private var _internal_FARMFLOW_RESQUANTITY : Number;
    private var _internal_FARMDOCUMENT_ID : String;
    private var _internal_FARMDOCUMENT_STYPE : String;
    private var _internal_FARMDOCUMENT_RTYPE : String;
    private var _internal_SUPPLIER_ID : String;
    private var _internal_SUPPLIER_NAME : String;
    private var _internal_FARMDOCUMENT_DOCTIMESTAMP : String;
    private var _internal_FARMDOCUMENT_DOCNUMBER : String;
    private var _internal_TREATMENTPROCFARM_ID : String;
    private var _internal_TREATMENTPROCFARM_QUANTITY : Number;
    private var _internal_TREATMENTPROC_ID : String;
    private var _internal_HEALTHPROC_ID : String;
    private var _internal_HEALTHPROC_NAME : String;
    private var _internal_HEALTHPROCFARM_QUANTITY : Number;
    private var _internal_FARMPRICE_ID : String;
    private var _internal_FARMPRICE_PRICE : String;
    private var _internal_FARM_ID : String;
    private var _internal_FARM_NAME : String;
    private var _internal_FARM_NOMENCLATUREARTICLENUMBER : String;
    private var _internal_FARM_MINIMUMQUANTITY : Number;
    private var _internal_DOCTOR_ID : String;
    private var _internal_DOCTOR_SHORTNAME : String;
    private var _internal_PATIENT_ID : String;
    private var _internal_PATIENT_SHORTNAME : String;
    private var _internal_SUMMOUT : Number;
    private var _internal_SUMMBEFORE : Number;
    private var _internal_SUMMRES : Number;
    private var _internal_SUMMIN : Number;
    private var _internal_SENDER : String;
    private var _internal_RECEIVER : String;
    private var _internal_FARM_REQUIRED : int;
    private var _internal_isOpen : Boolean;

    private static var emptyArray:Array = new Array();

    // Change this value according to your application's floating-point precision
    private static var epsilon:Number = 0.0001;

    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_WarehouseModuleOLAPRecord()
    {
        _model = new _WarehouseModuleOLAPRecordEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get FARMFLOW_ID() : String
    {
        return _internal_FARMFLOW_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FARMFLOW_QUANTITY() : Number
    {
        return _internal_FARMFLOW_QUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get FARMFLOW_INQUANTITY() : Number
    {
        return _internal_FARMFLOW_INQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get FARMFLOW_OUTQUANTITY() : Number
    {
        return _internal_FARMFLOW_OUTQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get FARMFLOW_BEFOREQUANTITY() : Number
    {
        return _internal_FARMFLOW_BEFOREQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get FARMFLOW_RESQUANTITY() : Number
    {
        return _internal_FARMFLOW_RESQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get FARMDOCUMENT_ID() : String
    {
        return _internal_FARMDOCUMENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FARMDOCUMENT_STYPE() : String
    {
        return _internal_FARMDOCUMENT_STYPE;
    }

    [Bindable(event="propertyChange")]
    public function get FARMDOCUMENT_RTYPE() : String
    {
        return _internal_FARMDOCUMENT_RTYPE;
    }

    [Bindable(event="propertyChange")]
    public function get SUPPLIER_ID() : String
    {
        return _internal_SUPPLIER_ID;
    }

    [Bindable(event="propertyChange")]
    public function get SUPPLIER_NAME() : String
    {
        return _internal_SUPPLIER_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get FARMDOCUMENT_DOCTIMESTAMP() : String
    {
        return _internal_FARMDOCUMENT_DOCTIMESTAMP;
    }

    [Bindable(event="propertyChange")]
    public function get FARMDOCUMENT_DOCNUMBER() : String
    {
        return _internal_FARMDOCUMENT_DOCNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROCFARM_ID() : String
    {
        return _internal_TREATMENTPROCFARM_ID;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROCFARM_QUANTITY() : Number
    {
        return _internal_TREATMENTPROCFARM_QUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get TREATMENTPROC_ID() : String
    {
        return _internal_TREATMENTPROC_ID;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROC_ID() : String
    {
        return _internal_HEALTHPROC_ID;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROC_NAME() : String
    {
        return _internal_HEALTHPROC_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get HEALTHPROCFARM_QUANTITY() : Number
    {
        return _internal_HEALTHPROCFARM_QUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get FARMPRICE_ID() : String
    {
        return _internal_FARMPRICE_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FARMPRICE_PRICE() : String
    {
        return _internal_FARMPRICE_PRICE;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_ID() : String
    {
        return _internal_FARM_ID;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_NAME() : String
    {
        return _internal_FARM_NAME;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_NOMENCLATUREARTICLENUMBER() : String
    {
        return _internal_FARM_NOMENCLATUREARTICLENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_MINIMUMQUANTITY() : Number
    {
        return _internal_FARM_MINIMUMQUANTITY;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_ID() : String
    {
        return _internal_DOCTOR_ID;
    }

    [Bindable(event="propertyChange")]
    public function get DOCTOR_SHORTNAME() : String
    {
        return _internal_DOCTOR_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_ID() : String
    {
        return _internal_PATIENT_ID;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENT_SHORTNAME() : String
    {
        return _internal_PATIENT_SHORTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get SUMMOUT() : Number
    {
        return _internal_SUMMOUT;
    }

    [Bindable(event="propertyChange")]
    public function get SUMMBEFORE() : Number
    {
        return _internal_SUMMBEFORE;
    }

    [Bindable(event="propertyChange")]
    public function get SUMMRES() : Number
    {
        return _internal_SUMMRES;
    }

    [Bindable(event="propertyChange")]
    public function get SUMMIN() : Number
    {
        return _internal_SUMMIN;
    }

    [Bindable(event="propertyChange")]
    public function get SENDER() : String
    {
        return _internal_SENDER;
    }

    [Bindable(event="propertyChange")]
    public function get RECEIVER() : String
    {
        return _internal_RECEIVER;
    }

    [Bindable(event="propertyChange")]
    public function get FARM_REQUIRED() : int
    {
        return _internal_FARM_REQUIRED;
    }

    [Bindable(event="propertyChange")]
    public function get isOpen() : Boolean
    {
        return _internal_isOpen;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set FARMFLOW_ID(value:String) : void
    {
        var oldValue:String = _internal_FARMFLOW_ID;
        if (oldValue !== value)
        {
            _internal_FARMFLOW_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMFLOW_ID", oldValue, _internal_FARMFLOW_ID));
        }
    }

    public function set FARMFLOW_QUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_FARMFLOW_QUANTITY;
        if (isNaN(_internal_FARMFLOW_QUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_FARMFLOW_QUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMFLOW_QUANTITY", oldValue, _internal_FARMFLOW_QUANTITY));
        }
    }

    public function set FARMFLOW_INQUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_FARMFLOW_INQUANTITY;
        if (isNaN(_internal_FARMFLOW_INQUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_FARMFLOW_INQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMFLOW_INQUANTITY", oldValue, _internal_FARMFLOW_INQUANTITY));
        }
    }

    public function set FARMFLOW_OUTQUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_FARMFLOW_OUTQUANTITY;
        if (isNaN(_internal_FARMFLOW_OUTQUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_FARMFLOW_OUTQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMFLOW_OUTQUANTITY", oldValue, _internal_FARMFLOW_OUTQUANTITY));
        }
    }

    public function set FARMFLOW_BEFOREQUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_FARMFLOW_BEFOREQUANTITY;
        if (isNaN(_internal_FARMFLOW_BEFOREQUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_FARMFLOW_BEFOREQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMFLOW_BEFOREQUANTITY", oldValue, _internal_FARMFLOW_BEFOREQUANTITY));
        }
    }

    public function set FARMFLOW_RESQUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_FARMFLOW_RESQUANTITY;
        if (isNaN(_internal_FARMFLOW_RESQUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_FARMFLOW_RESQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMFLOW_RESQUANTITY", oldValue, _internal_FARMFLOW_RESQUANTITY));
        }
    }

    public function set FARMDOCUMENT_ID(value:String) : void
    {
        var oldValue:String = _internal_FARMDOCUMENT_ID;
        if (oldValue !== value)
        {
            _internal_FARMDOCUMENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMDOCUMENT_ID", oldValue, _internal_FARMDOCUMENT_ID));
        }
    }

    public function set FARMDOCUMENT_STYPE(value:String) : void
    {
        var oldValue:String = _internal_FARMDOCUMENT_STYPE;
        if (oldValue !== value)
        {
            _internal_FARMDOCUMENT_STYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMDOCUMENT_STYPE", oldValue, _internal_FARMDOCUMENT_STYPE));
        }
    }

    public function set FARMDOCUMENT_RTYPE(value:String) : void
    {
        var oldValue:String = _internal_FARMDOCUMENT_RTYPE;
        if (oldValue !== value)
        {
            _internal_FARMDOCUMENT_RTYPE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMDOCUMENT_RTYPE", oldValue, _internal_FARMDOCUMENT_RTYPE));
        }
    }

    public function set SUPPLIER_ID(value:String) : void
    {
        var oldValue:String = _internal_SUPPLIER_ID;
        if (oldValue !== value)
        {
            _internal_SUPPLIER_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUPPLIER_ID", oldValue, _internal_SUPPLIER_ID));
        }
    }

    public function set SUPPLIER_NAME(value:String) : void
    {
        var oldValue:String = _internal_SUPPLIER_NAME;
        if (oldValue !== value)
        {
            _internal_SUPPLIER_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUPPLIER_NAME", oldValue, _internal_SUPPLIER_NAME));
        }
    }

    public function set FARMDOCUMENT_DOCTIMESTAMP(value:String) : void
    {
        var oldValue:String = _internal_FARMDOCUMENT_DOCTIMESTAMP;
        if (oldValue !== value)
        {
            _internal_FARMDOCUMENT_DOCTIMESTAMP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMDOCUMENT_DOCTIMESTAMP", oldValue, _internal_FARMDOCUMENT_DOCTIMESTAMP));
        }
    }

    public function set FARMDOCUMENT_DOCNUMBER(value:String) : void
    {
        var oldValue:String = _internal_FARMDOCUMENT_DOCNUMBER;
        if (oldValue !== value)
        {
            _internal_FARMDOCUMENT_DOCNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMDOCUMENT_DOCNUMBER", oldValue, _internal_FARMDOCUMENT_DOCNUMBER));
        }
    }

    public function set TREATMENTPROCFARM_ID(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROCFARM_ID;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROCFARM_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROCFARM_ID", oldValue, _internal_TREATMENTPROCFARM_ID));
        }
    }

    public function set TREATMENTPROCFARM_QUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_TREATMENTPROCFARM_QUANTITY;
        if (isNaN(_internal_TREATMENTPROCFARM_QUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_TREATMENTPROCFARM_QUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROCFARM_QUANTITY", oldValue, _internal_TREATMENTPROCFARM_QUANTITY));
        }
    }

    public function set TREATMENTPROC_ID(value:String) : void
    {
        var oldValue:String = _internal_TREATMENTPROC_ID;
        if (oldValue !== value)
        {
            _internal_TREATMENTPROC_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TREATMENTPROC_ID", oldValue, _internal_TREATMENTPROC_ID));
        }
    }

    public function set HEALTHPROC_ID(value:String) : void
    {
        var oldValue:String = _internal_HEALTHPROC_ID;
        if (oldValue !== value)
        {
            _internal_HEALTHPROC_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROC_ID", oldValue, _internal_HEALTHPROC_ID));
        }
    }

    public function set HEALTHPROC_NAME(value:String) : void
    {
        var oldValue:String = _internal_HEALTHPROC_NAME;
        if (oldValue !== value)
        {
            _internal_HEALTHPROC_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROC_NAME", oldValue, _internal_HEALTHPROC_NAME));
        }
    }

    public function set HEALTHPROCFARM_QUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_HEALTHPROCFARM_QUANTITY;
        if (isNaN(_internal_HEALTHPROCFARM_QUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_HEALTHPROCFARM_QUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HEALTHPROCFARM_QUANTITY", oldValue, _internal_HEALTHPROCFARM_QUANTITY));
        }
    }

    public function set FARMPRICE_ID(value:String) : void
    {
        var oldValue:String = _internal_FARMPRICE_ID;
        if (oldValue !== value)
        {
            _internal_FARMPRICE_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMPRICE_ID", oldValue, _internal_FARMPRICE_ID));
        }
    }

    public function set FARMPRICE_PRICE(value:String) : void
    {
        var oldValue:String = _internal_FARMPRICE_PRICE;
        if (oldValue !== value)
        {
            _internal_FARMPRICE_PRICE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARMPRICE_PRICE", oldValue, _internal_FARMPRICE_PRICE));
        }
    }

    public function set FARM_ID(value:String) : void
    {
        var oldValue:String = _internal_FARM_ID;
        if (oldValue !== value)
        {
            _internal_FARM_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_ID", oldValue, _internal_FARM_ID));
        }
    }

    public function set FARM_NAME(value:String) : void
    {
        var oldValue:String = _internal_FARM_NAME;
        if (oldValue !== value)
        {
            _internal_FARM_NAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_NAME", oldValue, _internal_FARM_NAME));
        }
    }

    public function set FARM_NOMENCLATUREARTICLENUMBER(value:String) : void
    {
        var oldValue:String = _internal_FARM_NOMENCLATUREARTICLENUMBER;
        if (oldValue !== value)
        {
            _internal_FARM_NOMENCLATUREARTICLENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_NOMENCLATUREARTICLENUMBER", oldValue, _internal_FARM_NOMENCLATUREARTICLENUMBER));
        }
    }

    public function set FARM_MINIMUMQUANTITY(value:Number) : void
    {
        var oldValue:Number = _internal_FARM_MINIMUMQUANTITY;
        if (isNaN(_internal_FARM_MINIMUMQUANTITY) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_FARM_MINIMUMQUANTITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_MINIMUMQUANTITY", oldValue, _internal_FARM_MINIMUMQUANTITY));
        }
    }

    public function set DOCTOR_ID(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_ID;
        if (oldValue !== value)
        {
            _internal_DOCTOR_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_ID", oldValue, _internal_DOCTOR_ID));
        }
    }

    public function set DOCTOR_SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_DOCTOR_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_DOCTOR_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "DOCTOR_SHORTNAME", oldValue, _internal_DOCTOR_SHORTNAME));
        }
    }

    public function set PATIENT_ID(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_ID;
        if (oldValue !== value)
        {
            _internal_PATIENT_ID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_ID", oldValue, _internal_PATIENT_ID));
        }
    }

    public function set PATIENT_SHORTNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENT_SHORTNAME;
        if (oldValue !== value)
        {
            _internal_PATIENT_SHORTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENT_SHORTNAME", oldValue, _internal_PATIENT_SHORTNAME));
        }
    }

    public function set SUMMOUT(value:Number) : void
    {
        var oldValue:Number = _internal_SUMMOUT;
        if (isNaN(_internal_SUMMOUT) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_SUMMOUT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMMOUT", oldValue, _internal_SUMMOUT));
        }
    }

    public function set SUMMBEFORE(value:Number) : void
    {
        var oldValue:Number = _internal_SUMMBEFORE;
        if (isNaN(_internal_SUMMBEFORE) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_SUMMBEFORE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMMBEFORE", oldValue, _internal_SUMMBEFORE));
        }
    }

    public function set SUMMRES(value:Number) : void
    {
        var oldValue:Number = _internal_SUMMRES;
        if (isNaN(_internal_SUMMRES) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_SUMMRES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMMRES", oldValue, _internal_SUMMRES));
        }
    }

    public function set SUMMIN(value:Number) : void
    {
        var oldValue:Number = _internal_SUMMIN;
        if (isNaN(_internal_SUMMIN) == true || Math.abs(oldValue - value) > epsilon)
        {
            _internal_SUMMIN = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SUMMIN", oldValue, _internal_SUMMIN));
        }
    }

    public function set SENDER(value:String) : void
    {
        var oldValue:String = _internal_SENDER;
        if (oldValue !== value)
        {
            _internal_SENDER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SENDER", oldValue, _internal_SENDER));
        }
    }

    public function set RECEIVER(value:String) : void
    {
        var oldValue:String = _internal_RECEIVER;
        if (oldValue !== value)
        {
            _internal_RECEIVER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RECEIVER", oldValue, _internal_RECEIVER));
        }
    }

    public function set FARM_REQUIRED(value:int) : void
    {
        var oldValue:int = _internal_FARM_REQUIRED;
        if (oldValue !== value)
        {
            _internal_FARM_REQUIRED = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "FARM_REQUIRED", oldValue, _internal_FARM_REQUIRED));
        }
    }

    public function set isOpen(value:Boolean) : void
    {
        var oldValue:Boolean = _internal_isOpen;
        if (oldValue !== value)
        {
            _internal_isOpen = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "isOpen", oldValue, _internal_isOpen));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _WarehouseModuleOLAPRecordEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _WarehouseModuleOLAPRecordEntityMetadata) : void
    {
        var oldValue : _WarehouseModuleOLAPRecordEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
