/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - PatientCardPDFModulePatientCardData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.PatientCardPDFModuleAmbcard;
import valueObjects.PatientCardPDFModuleExam;
import valueObjects.PatientCardPDFModuleHygcontrol;
import valueObjects.PatientCardPDFModuleIllHistoryRecord;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_PatientCardPDFModulePatientCardData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
        try
        {
            if (flash.net.getClassByAlias("PatientCardPDFModulePatientCardData") == null)
            {
                flash.net.registerClassAlias("PatientCardPDFModulePatientCardData", cz);
            }
        }
        catch (e:Error)
        {
            flash.net.registerClassAlias("PatientCardPDFModulePatientCardData", cz);
        }
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.PatientCardPDFModuleExam.initRemoteClassAliasSingleChild();
        valueObjects.PatientCardPDFModuleAmbcard.initRemoteClassAliasSingleChild();
        valueObjects.PatientCardPDFModuleHygcontrol.initRemoteClassAliasSingleChild();
        valueObjects.PatientCardPDFModuleIllHistoryRecord.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _PatientCardPDFModulePatientCardDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_CLINICNAME : String;
    private var _internal_CLINICLEGALNAME : String;
    private var _internal_CLINICADDRESS : String;
    private var _internal_PATIENTCARDNUMBER : String;
    private var _internal_PATIENTCARDDATE : String;
    private var _internal_PATIENTFIRSTNAME : String;
    private var _internal_PATIENTMIDDLENAME : String;
    private var _internal_PATIENTLASTNAME : String;
    private var _internal_PATIENTBIRTHDAY : String;
    private var _internal_PATIENTSEX : String;
    private var _internal_PATIENTPOSTALCODE : String;
    private var _internal_PATIENTCOUNTRY : String;
    private var _internal_PATIENTSTATE : String;
    private var _internal_PATIENTCITY : String;
    private var _internal_PATIENTRAGION : String;
    private var _internal_PATIENTADDRESS : String;
    private var _internal_PATIENTTELEPHONENUMBER : String;
    private var _internal_PATIENTMOBILENUMBER : String;
    private var _internal_PATIENTEMPLOYMENTPLACE : String;
    private var _internal_PATIENTJOB : String;
    private var _internal_PATIENTWORKPHONE : String;
    private var _internal_PATIENTTESTTESTDATE : String;
    private var _internal_PATIENTTESTCARDIACDISEASE : String;
    private var _internal_PATIENTTESTKIDNEYDISEASE : String;
    private var _internal_PATIENTTESTLIVERDISEASE : String;
    private var _internal_PATIENTTESTOTHERDISEASE : String;
    private var _internal_PATIENTTESTARTERIALPRESSURE : String;
    private var _internal_PATIENTTESTRHEUMATISM : String;
    private var _internal_PATIENTTESTHEPATITIS : String;
    private var _internal_PATIENTTESTDIABETES : String;
    private var _internal_PATIENTTESTFIT : String;
    private var _internal_PATIENTTESTBLEEDING : String;
    private var _internal_PATIENTTESTALLERGY : String;
    private var _internal_PATIENTTESTPREGNANCY : String;
    private var _internal_PATIENTTESTMEDICATIONS : String;
    private var _internal_PATIENTTESTBLOODGROUP : String;
    private var _internal_PATIENTTESTRHESUSFACTOR : String;
    private var _internal_PATIENTTESTMOUTHULCER : String;
    private var _internal_PATIENTTESTFUNGUS : String;
    private var _internal_PATIENTTESTFEVER : String;
    private var _internal_PATIENTTESTTHROATACHE : String;
    private var _internal_PATIENTTESTLYMPHNODES : String;
    private var _internal_PATIENTTESTREDSKINAREA : String;
    private var _internal_PATIENTTESTHYPERHIDROSIS : String;
    private var _internal_PATIENTTESTDIARRHEA : String;
    private var _internal_PATIENTTESTAMNESIA : String;
    private var _internal_PATIENTTESTWEIGHTLOSS : String;
    private var _internal_PATIENTTESTHEADACHE : String;
    private var _internal_PATIENTTESTAIDS : String;
    private var _internal_PATIENTTESTWORKWITHBLOOD : String;
    private var _internal_PATIENTTESTNARCOMANIA : String;
    private var _internal_TOOTHEXAM : ArrayCollection;
    model_internal var _internal_TOOTHEXAM_leaf:valueObjects.PatientCardPDFModuleExam;
    private var _internal_AMBCARD : ArrayCollection;
    model_internal var _internal_AMBCARD_leaf:valueObjects.PatientCardPDFModuleAmbcard;
    private var _internal_HYGCONTROL : ArrayCollection;
    model_internal var _internal_HYGCONTROL_leaf:valueObjects.PatientCardPDFModuleHygcontrol;
    private var _internal_ILLHISTORY : ArrayCollection;
    model_internal var _internal_ILLHISTORY_leaf:valueObjects.PatientCardPDFModuleIllHistoryRecord;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_PatientCardPDFModulePatientCardData()
    {
        _model = new _PatientCardPDFModulePatientCardDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get CLINICNAME() : String
    {
        return _internal_CLINICNAME;
    }

    [Bindable(event="propertyChange")]
    public function get CLINICLEGALNAME() : String
    {
        return _internal_CLINICLEGALNAME;
    }

    [Bindable(event="propertyChange")]
    public function get CLINICADDRESS() : String
    {
        return _internal_CLINICADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTCARDNUMBER() : String
    {
        return _internal_PATIENTCARDNUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTCARDDATE() : String
    {
        return _internal_PATIENTCARDDATE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTFIRSTNAME() : String
    {
        return _internal_PATIENTFIRSTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTMIDDLENAME() : String
    {
        return _internal_PATIENTMIDDLENAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTLASTNAME() : String
    {
        return _internal_PATIENTLASTNAME;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTBIRTHDAY() : String
    {
        return _internal_PATIENTBIRTHDAY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSEX() : String
    {
        return _internal_PATIENTSEX;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTPOSTALCODE() : String
    {
        return _internal_PATIENTPOSTALCODE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTCOUNTRY() : String
    {
        return _internal_PATIENTCOUNTRY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTSTATE() : String
    {
        return _internal_PATIENTSTATE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTCITY() : String
    {
        return _internal_PATIENTCITY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTRAGION() : String
    {
        return _internal_PATIENTRAGION;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTADDRESS() : String
    {
        return _internal_PATIENTADDRESS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTELEPHONENUMBER() : String
    {
        return _internal_PATIENTTELEPHONENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTMOBILENUMBER() : String
    {
        return _internal_PATIENTMOBILENUMBER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTEMPLOYMENTPLACE() : String
    {
        return _internal_PATIENTEMPLOYMENTPLACE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTJOB() : String
    {
        return _internal_PATIENTJOB;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTWORKPHONE() : String
    {
        return _internal_PATIENTWORKPHONE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTTESTDATE() : String
    {
        return _internal_PATIENTTESTTESTDATE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTCARDIACDISEASE() : String
    {
        return _internal_PATIENTTESTCARDIACDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTKIDNEYDISEASE() : String
    {
        return _internal_PATIENTTESTKIDNEYDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTLIVERDISEASE() : String
    {
        return _internal_PATIENTTESTLIVERDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTOTHERDISEASE() : String
    {
        return _internal_PATIENTTESTOTHERDISEASE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTARTERIALPRESSURE() : String
    {
        return _internal_PATIENTTESTARTERIALPRESSURE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTRHEUMATISM() : String
    {
        return _internal_PATIENTTESTRHEUMATISM;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTHEPATITIS() : String
    {
        return _internal_PATIENTTESTHEPATITIS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTDIABETES() : String
    {
        return _internal_PATIENTTESTDIABETES;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTFIT() : String
    {
        return _internal_PATIENTTESTFIT;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTBLEEDING() : String
    {
        return _internal_PATIENTTESTBLEEDING;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTALLERGY() : String
    {
        return _internal_PATIENTTESTALLERGY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTPREGNANCY() : String
    {
        return _internal_PATIENTTESTPREGNANCY;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTMEDICATIONS() : String
    {
        return _internal_PATIENTTESTMEDICATIONS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTBLOODGROUP() : String
    {
        return _internal_PATIENTTESTBLOODGROUP;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTRHESUSFACTOR() : String
    {
        return _internal_PATIENTTESTRHESUSFACTOR;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTMOUTHULCER() : String
    {
        return _internal_PATIENTTESTMOUTHULCER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTFUNGUS() : String
    {
        return _internal_PATIENTTESTFUNGUS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTFEVER() : String
    {
        return _internal_PATIENTTESTFEVER;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTTHROATACHE() : String
    {
        return _internal_PATIENTTESTTHROATACHE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTLYMPHNODES() : String
    {
        return _internal_PATIENTTESTLYMPHNODES;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTREDSKINAREA() : String
    {
        return _internal_PATIENTTESTREDSKINAREA;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTHYPERHIDROSIS() : String
    {
        return _internal_PATIENTTESTHYPERHIDROSIS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTDIARRHEA() : String
    {
        return _internal_PATIENTTESTDIARRHEA;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTAMNESIA() : String
    {
        return _internal_PATIENTTESTAMNESIA;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTWEIGHTLOSS() : String
    {
        return _internal_PATIENTTESTWEIGHTLOSS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTHEADACHE() : String
    {
        return _internal_PATIENTTESTHEADACHE;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTAIDS() : String
    {
        return _internal_PATIENTTESTAIDS;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTWORKWITHBLOOD() : String
    {
        return _internal_PATIENTTESTWORKWITHBLOOD;
    }

    [Bindable(event="propertyChange")]
    public function get PATIENTTESTNARCOMANIA() : String
    {
        return _internal_PATIENTTESTNARCOMANIA;
    }

    [Bindable(event="propertyChange")]
    public function get TOOTHEXAM() : ArrayCollection
    {
        return _internal_TOOTHEXAM;
    }

    [Bindable(event="propertyChange")]
    public function get AMBCARD() : ArrayCollection
    {
        return _internal_AMBCARD;
    }

    [Bindable(event="propertyChange")]
    public function get HYGCONTROL() : ArrayCollection
    {
        return _internal_HYGCONTROL;
    }

    [Bindable(event="propertyChange")]
    public function get ILLHISTORY() : ArrayCollection
    {
        return _internal_ILLHISTORY;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set CLINICNAME(value:String) : void
    {
        var oldValue:String = _internal_CLINICNAME;
        if (oldValue !== value)
        {
            _internal_CLINICNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLINICNAME", oldValue, _internal_CLINICNAME));
        }
    }

    public function set CLINICLEGALNAME(value:String) : void
    {
        var oldValue:String = _internal_CLINICLEGALNAME;
        if (oldValue !== value)
        {
            _internal_CLINICLEGALNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLINICLEGALNAME", oldValue, _internal_CLINICLEGALNAME));
        }
    }

    public function set CLINICADDRESS(value:String) : void
    {
        var oldValue:String = _internal_CLINICADDRESS;
        if (oldValue !== value)
        {
            _internal_CLINICADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "CLINICADDRESS", oldValue, _internal_CLINICADDRESS));
        }
    }

    public function set PATIENTCARDNUMBER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTCARDNUMBER;
        if (oldValue !== value)
        {
            _internal_PATIENTCARDNUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTCARDNUMBER", oldValue, _internal_PATIENTCARDNUMBER));
        }
    }

    public function set PATIENTCARDDATE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTCARDDATE;
        if (oldValue !== value)
        {
            _internal_PATIENTCARDDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTCARDDATE", oldValue, _internal_PATIENTCARDDATE));
        }
    }

    public function set PATIENTFIRSTNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENTFIRSTNAME;
        if (oldValue !== value)
        {
            _internal_PATIENTFIRSTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTFIRSTNAME", oldValue, _internal_PATIENTFIRSTNAME));
        }
    }

    public function set PATIENTMIDDLENAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENTMIDDLENAME;
        if (oldValue !== value)
        {
            _internal_PATIENTMIDDLENAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTMIDDLENAME", oldValue, _internal_PATIENTMIDDLENAME));
        }
    }

    public function set PATIENTLASTNAME(value:String) : void
    {
        var oldValue:String = _internal_PATIENTLASTNAME;
        if (oldValue !== value)
        {
            _internal_PATIENTLASTNAME = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTLASTNAME", oldValue, _internal_PATIENTLASTNAME));
        }
    }

    public function set PATIENTBIRTHDAY(value:String) : void
    {
        var oldValue:String = _internal_PATIENTBIRTHDAY;
        if (oldValue !== value)
        {
            _internal_PATIENTBIRTHDAY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTBIRTHDAY", oldValue, _internal_PATIENTBIRTHDAY));
        }
    }

    public function set PATIENTSEX(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSEX;
        if (oldValue !== value)
        {
            _internal_PATIENTSEX = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSEX", oldValue, _internal_PATIENTSEX));
        }
    }

    public function set PATIENTPOSTALCODE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTPOSTALCODE;
        if (oldValue !== value)
        {
            _internal_PATIENTPOSTALCODE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTPOSTALCODE", oldValue, _internal_PATIENTPOSTALCODE));
        }
    }

    public function set PATIENTCOUNTRY(value:String) : void
    {
        var oldValue:String = _internal_PATIENTCOUNTRY;
        if (oldValue !== value)
        {
            _internal_PATIENTCOUNTRY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTCOUNTRY", oldValue, _internal_PATIENTCOUNTRY));
        }
    }

    public function set PATIENTSTATE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTSTATE;
        if (oldValue !== value)
        {
            _internal_PATIENTSTATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTSTATE", oldValue, _internal_PATIENTSTATE));
        }
    }

    public function set PATIENTCITY(value:String) : void
    {
        var oldValue:String = _internal_PATIENTCITY;
        if (oldValue !== value)
        {
            _internal_PATIENTCITY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTCITY", oldValue, _internal_PATIENTCITY));
        }
    }

    public function set PATIENTRAGION(value:String) : void
    {
        var oldValue:String = _internal_PATIENTRAGION;
        if (oldValue !== value)
        {
            _internal_PATIENTRAGION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTRAGION", oldValue, _internal_PATIENTRAGION));
        }
    }

    public function set PATIENTADDRESS(value:String) : void
    {
        var oldValue:String = _internal_PATIENTADDRESS;
        if (oldValue !== value)
        {
            _internal_PATIENTADDRESS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTADDRESS", oldValue, _internal_PATIENTADDRESS));
        }
    }

    public function set PATIENTTELEPHONENUMBER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTELEPHONENUMBER;
        if (oldValue !== value)
        {
            _internal_PATIENTTELEPHONENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTELEPHONENUMBER", oldValue, _internal_PATIENTTELEPHONENUMBER));
        }
    }

    public function set PATIENTMOBILENUMBER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTMOBILENUMBER;
        if (oldValue !== value)
        {
            _internal_PATIENTMOBILENUMBER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTMOBILENUMBER", oldValue, _internal_PATIENTMOBILENUMBER));
        }
    }

    public function set PATIENTEMPLOYMENTPLACE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTEMPLOYMENTPLACE;
        if (oldValue !== value)
        {
            _internal_PATIENTEMPLOYMENTPLACE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTEMPLOYMENTPLACE", oldValue, _internal_PATIENTEMPLOYMENTPLACE));
        }
    }

    public function set PATIENTJOB(value:String) : void
    {
        var oldValue:String = _internal_PATIENTJOB;
        if (oldValue !== value)
        {
            _internal_PATIENTJOB = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTJOB", oldValue, _internal_PATIENTJOB));
        }
    }

    public function set PATIENTWORKPHONE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTWORKPHONE;
        if (oldValue !== value)
        {
            _internal_PATIENTWORKPHONE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTWORKPHONE", oldValue, _internal_PATIENTWORKPHONE));
        }
    }

    public function set PATIENTTESTTESTDATE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTTESTDATE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTTESTDATE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTTESTDATE", oldValue, _internal_PATIENTTESTTESTDATE));
        }
    }

    public function set PATIENTTESTCARDIACDISEASE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTCARDIACDISEASE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTCARDIACDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTCARDIACDISEASE", oldValue, _internal_PATIENTTESTCARDIACDISEASE));
        }
    }

    public function set PATIENTTESTKIDNEYDISEASE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTKIDNEYDISEASE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTKIDNEYDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTKIDNEYDISEASE", oldValue, _internal_PATIENTTESTKIDNEYDISEASE));
        }
    }

    public function set PATIENTTESTLIVERDISEASE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTLIVERDISEASE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTLIVERDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTLIVERDISEASE", oldValue, _internal_PATIENTTESTLIVERDISEASE));
        }
    }

    public function set PATIENTTESTOTHERDISEASE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTOTHERDISEASE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTOTHERDISEASE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTOTHERDISEASE", oldValue, _internal_PATIENTTESTOTHERDISEASE));
        }
    }

    public function set PATIENTTESTARTERIALPRESSURE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTARTERIALPRESSURE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTARTERIALPRESSURE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTARTERIALPRESSURE", oldValue, _internal_PATIENTTESTARTERIALPRESSURE));
        }
    }

    public function set PATIENTTESTRHEUMATISM(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTRHEUMATISM;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTRHEUMATISM = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTRHEUMATISM", oldValue, _internal_PATIENTTESTRHEUMATISM));
        }
    }

    public function set PATIENTTESTHEPATITIS(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTHEPATITIS;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTHEPATITIS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTHEPATITIS", oldValue, _internal_PATIENTTESTHEPATITIS));
        }
    }

    public function set PATIENTTESTDIABETES(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTDIABETES;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTDIABETES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTDIABETES", oldValue, _internal_PATIENTTESTDIABETES));
        }
    }

    public function set PATIENTTESTFIT(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTFIT;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTFIT = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTFIT", oldValue, _internal_PATIENTTESTFIT));
        }
    }

    public function set PATIENTTESTBLEEDING(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTBLEEDING;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTBLEEDING = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTBLEEDING", oldValue, _internal_PATIENTTESTBLEEDING));
        }
    }

    public function set PATIENTTESTALLERGY(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTALLERGY;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTALLERGY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTALLERGY", oldValue, _internal_PATIENTTESTALLERGY));
        }
    }

    public function set PATIENTTESTPREGNANCY(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTPREGNANCY;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTPREGNANCY = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTPREGNANCY", oldValue, _internal_PATIENTTESTPREGNANCY));
        }
    }

    public function set PATIENTTESTMEDICATIONS(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTMEDICATIONS;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTMEDICATIONS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTMEDICATIONS", oldValue, _internal_PATIENTTESTMEDICATIONS));
        }
    }

    public function set PATIENTTESTBLOODGROUP(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTBLOODGROUP;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTBLOODGROUP = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTBLOODGROUP", oldValue, _internal_PATIENTTESTBLOODGROUP));
        }
    }

    public function set PATIENTTESTRHESUSFACTOR(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTRHESUSFACTOR;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTRHESUSFACTOR = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTRHESUSFACTOR", oldValue, _internal_PATIENTTESTRHESUSFACTOR));
        }
    }

    public function set PATIENTTESTMOUTHULCER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTMOUTHULCER;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTMOUTHULCER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTMOUTHULCER", oldValue, _internal_PATIENTTESTMOUTHULCER));
        }
    }

    public function set PATIENTTESTFUNGUS(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTFUNGUS;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTFUNGUS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTFUNGUS", oldValue, _internal_PATIENTTESTFUNGUS));
        }
    }

    public function set PATIENTTESTFEVER(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTFEVER;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTFEVER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTFEVER", oldValue, _internal_PATIENTTESTFEVER));
        }
    }

    public function set PATIENTTESTTHROATACHE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTTHROATACHE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTTHROATACHE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTTHROATACHE", oldValue, _internal_PATIENTTESTTHROATACHE));
        }
    }

    public function set PATIENTTESTLYMPHNODES(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTLYMPHNODES;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTLYMPHNODES = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTLYMPHNODES", oldValue, _internal_PATIENTTESTLYMPHNODES));
        }
    }

    public function set PATIENTTESTREDSKINAREA(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTREDSKINAREA;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTREDSKINAREA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTREDSKINAREA", oldValue, _internal_PATIENTTESTREDSKINAREA));
        }
    }

    public function set PATIENTTESTHYPERHIDROSIS(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTHYPERHIDROSIS;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTHYPERHIDROSIS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTHYPERHIDROSIS", oldValue, _internal_PATIENTTESTHYPERHIDROSIS));
        }
    }

    public function set PATIENTTESTDIARRHEA(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTDIARRHEA;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTDIARRHEA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTDIARRHEA", oldValue, _internal_PATIENTTESTDIARRHEA));
        }
    }

    public function set PATIENTTESTAMNESIA(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTAMNESIA;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTAMNESIA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTAMNESIA", oldValue, _internal_PATIENTTESTAMNESIA));
        }
    }

    public function set PATIENTTESTWEIGHTLOSS(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTWEIGHTLOSS;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTWEIGHTLOSS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTWEIGHTLOSS", oldValue, _internal_PATIENTTESTWEIGHTLOSS));
        }
    }

    public function set PATIENTTESTHEADACHE(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTHEADACHE;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTHEADACHE = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTHEADACHE", oldValue, _internal_PATIENTTESTHEADACHE));
        }
    }

    public function set PATIENTTESTAIDS(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTAIDS;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTAIDS = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTAIDS", oldValue, _internal_PATIENTTESTAIDS));
        }
    }

    public function set PATIENTTESTWORKWITHBLOOD(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTWORKWITHBLOOD;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTWORKWITHBLOOD = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTWORKWITHBLOOD", oldValue, _internal_PATIENTTESTWORKWITHBLOOD));
        }
    }

    public function set PATIENTTESTNARCOMANIA(value:String) : void
    {
        var oldValue:String = _internal_PATIENTTESTNARCOMANIA;
        if (oldValue !== value)
        {
            _internal_PATIENTTESTNARCOMANIA = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "PATIENTTESTNARCOMANIA", oldValue, _internal_PATIENTTESTNARCOMANIA));
        }
    }

    public function set TOOTHEXAM(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_TOOTHEXAM;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_TOOTHEXAM = value;
            }
            else if (value is Array)
            {
                _internal_TOOTHEXAM = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_TOOTHEXAM = null;
            }
            else
            {
                throw new Error("value of TOOTHEXAM must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TOOTHEXAM", oldValue, _internal_TOOTHEXAM));
        }
    }

    public function set AMBCARD(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_AMBCARD;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_AMBCARD = value;
            }
            else if (value is Array)
            {
                _internal_AMBCARD = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_AMBCARD = null;
            }
            else
            {
                throw new Error("value of AMBCARD must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "AMBCARD", oldValue, _internal_AMBCARD));
        }
    }

    public function set HYGCONTROL(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_HYGCONTROL;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_HYGCONTROL = value;
            }
            else if (value is Array)
            {
                _internal_HYGCONTROL = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_HYGCONTROL = null;
            }
            else
            {
                throw new Error("value of HYGCONTROL must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "HYGCONTROL", oldValue, _internal_HYGCONTROL));
        }
    }

    public function set ILLHISTORY(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_ILLHISTORY;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_ILLHISTORY = value;
            }
            else if (value is Array)
            {
                _internal_ILLHISTORY = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_ILLHISTORY = null;
            }
            else
            {
                throw new Error("value of ILLHISTORY must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ILLHISTORY", oldValue, _internal_ILLHISTORY));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _PatientCardPDFModulePatientCardDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _PatientCardPDFModulePatientCardDataEntityMetadata) : void
    {
        var oldValue : _PatientCardPDFModulePatientCardDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
