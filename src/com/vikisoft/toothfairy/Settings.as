package com.vikisoft.toothfairy
{
	import flash.filesystem.File;
	import flash.net.SharedObject;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayList;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class Settings
	{
		
		
		
		public static const COLOR_ORANGE_DARK:uint=0xf68e1e; 
		public static const COLOR_ORANGE:uint=0xfbc182; 
		public static const COLOR_YELLOW:uint=0xf7b81f; 
		
		public static const COLOR_BLUE:uint=0x19779b;
		public static const COLOR_BLUE_LIGHT:uint=0x4ba9cd;
		public static const COLOR_BLUE2:uint=0x2b5f74;
		public static const COLOR_BLUE2_DARK:uint=0x094b65;
		
		public static const COLOR_GREEN_LIGHT:uint=0x4ad97f;
		public static const COLOR_GREEN:uint=0x16b24f;
		public static const COLOR_RED:uint=0xf6441e;
		
		
		
		
		
		
		public static var LOGO:ByteArray;
		
		/**
		 * ToothFairy: DD.MM.YYYY
		 * */
		public static var TOOTHFAIRY_DATA_FORMAT:String = "DD.MM.YYYY";
		
		/**
		 * ToothFairy character for separating Date: .
		 * */
		public static var TOOTHFAIRY_DATA_SEPARATOR:String = ".";
		
		/**
		 * ToothFairy type of date format: 
		 * 			1 - xx.xx.YYYY 
		 * 			2 - xx.YYYY.xx 
		 * 			3 - YYYY.xx.xx
		 * */
		public static var TOOTHFAIRY_DATA_TYPE:int = 1;
		
		/**
		 * ToothFairy: DD.MM.YYYY JJ:NN:SS
		 * */
		public static var TOOTHFAIRY_TIMESTAMP_FORMAT:String = "DD.MM.YYYY JJ:NN:SS";
		
		/**
		 * Firebird: YYYY-MM-DD
		 * */
		public static const FIREBIRD_DATA_FORMAT:String = "YYYY-MM-DD";
		
		/**
		 * Firebird: YYYY-MM-DD JJ:NN:SS
		 * */
		public static const FIREBIRD_TIMESTAMP_FORMAT:String = "YYYY-MM-DD JJ:NN:SS";
		
		
		/**
		 * ToothFairy currency label
		 * */
		public static var TOOTHFAIRY_CURRENCY_LABEL:String = "грн";
		
		/**
		 * ToothFairy currency decimal label
		 * */
		public static var TOOTHFAIRY_CURRENCYDECIMAL_LABEL:String = "коп";
		
		/**
		 * Minutes in one UOP
		 * */
		public static const MINUTES_UOP_RATE:Number = 16;
		
		/**
		 * Delay of timer in lazy loading inputs
		 * */
		public static const TOOTHFAIRY_LAZY_LOADING_TIMER_DELAY:Number = 500;
		
		/**
		 * Number decimal separator
		 * */
		public static const NUMBER_DECIMAL_SEPARATOR:String = ".";
		/**
		 * Number grouping separator
		 * */
		public static const NUMBER_GROUPING_SEPARATOR:String = "";
		
		public static const providerForPatientsLazyCombobox:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'FIOCARDNUMBERLabel'), searchdata:'FIOCARDNUMBER', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'patientTable.FIODataGridColumn'), searchdata:'FIO', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'LastNameDataGridColumn'), searchdata:'LASTNAME', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'FirstNameDataGridColumn'), searchdata:'FIRSTNAME', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'MiddleNameDataGridColumn'), searchdata:'MIDDLENAME', datagridsearch:1}, 
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'CardNumberDataGridColumn'), searchdata:'CARDNUMBER_FULL', datagridsearch:1},
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'CityLabel'), searchdata:'CITY', datagridsearch:0},
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'RegionLabel'), searchdata:'RAGION', datagridsearch:0},
			{label:ResourceManager.getInstance().getString('PatientCardModule', 'StateLabel'), searchdata:'STATE', datagridsearch:0}
		]);
		
		public static const providerForCashControl:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('AccountModule', 'cashbank'), searchdata:-1}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'cash'), searchdata:0}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'bank'), searchdata:1}
			
		]);
		
		
		public static const providerCBForCashControl:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('AccountModule', 'cash'), searchdata:0}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'bank'), searchdata:1},
			{label:ResourceManager.getInstance().getString('AccountModule', 'cashplusbank'), searchdata:-1}
			
		]);
		public static const providerCBForCashControl2:ArrayList = new ArrayList([
			
			{label:ResourceManager.getInstance().getString('AccountModule', 'cashplusbank'), searchdata:-1},
			{label:ResourceManager.getInstance().getString('AccountModule', 'cash'), searchdata:0}, 
			{label:ResourceManager.getInstance().getString('AccountModule', 'bank'), searchdata:1}
			
		]);
		
		/**
		 * global temporary folder
		 * */
		public static var FOLDER_TEMP:File;
		
		
		/**
		 * global toothfairydocument folder
		 * */
		public static var FOLDER_DOCUMENTS:File;
		
		
		/**
		 * alpha of first buttons
		 * */
		public static var ALPHA_PRIMARY:Number = 0.5;
		
		/**
		 * alpha of secondary buttons
		 * */
		public static var ALPHA_SECONDARY:Number = 0.3;
		
		
		
		
		public static function INIT_DEFAULT():Boolean
		{
			// server ip adresses
			if (SharedObject.getLocal("toothfairy").data["main_serverList"] == null)
			{
				var currentIP:String = SharedObject.getLocal("toothfairy").data["main_activeServer"];
				if(currentIP == "localhost:81")
				{
					SharedObject.getLocal("toothfairy").data["main_activeServer"] = "localhost:81";
					SharedObject.getLocal("toothfairy").data["main_activeServer_key"] = "19502fd451e789abfa2d5c1357358146";
					
					SharedObject.getLocal("toothfairy").data["main_serverList"] = 
						[{id:0, isActive:true, serverIp:"localhost:81", key:"19502fd451e789abfa2d5c1357358146"},
						 {id:1, isActive:false, serverIp:"localhost", key:"6d4f477f0dc48bf4d85a979b3b572334"}];
				}
				else if(currentIP == "localhost")
				{
					SharedObject.getLocal("toothfairy").data["main_activeServer"] = "localhost";
					SharedObject.getLocal("toothfairy").data["main_activeServer_key"] = "6d4f477f0dc48bf4d85a979b3b572334";
					
					SharedObject.getLocal("toothfairy").data["main_serverList"] = 
						[{id:0, isActive:false, serverIp:"localhost:81", key:"19502fd451e789abfa2d5c1357358146"},
						 {id:1, isActive:true, serverIp:"localhost", key:"6d4f477f0dc48bf4d85a979b3b572334"}];
				}
				else
				{
					SharedObject.getLocal("toothfairy").data["main_activeServer"] = "localhost:81";
					SharedObject.getLocal("toothfairy").data["main_activeServer_key"] = "19502fd451e789abfa2d5c1357358146";
					
					SharedObject.getLocal("toothfairy").data["main_serverList"] = 
						[{id:0, isActive:true, serverIp:"localhost:81", key:"19502fd451e789abfa2d5c1357358146"},
						 {id:1, isActive:false, serverIp:"localhost", key:"6d4f477f0dc48bf4d85a979b3b572334"}];
				}
				
				
			}
			return true;
		}
	}
}