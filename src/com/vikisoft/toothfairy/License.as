package com.vikisoft.toothfairy
{
	[Bindable]
	public class License
	{
		public static var _HARDWARE_KEY:String = "-";
		public static var _KEY:String = "-";
		public static var _HARDWARE_IP:String = "-";
		public static var _CLIENT_ID:String = "-";
		public static var _LOCATION:String = "uk_UA";
		public static var _SMS_COST:Number = 0.6;
		
		public static var VERSION_EXTENDED:Boolean = false;
		public static var VERSION_APP:String = "-";
		public static var VERSION_DATABASE:String = "-";
		public static var VERSION_PHP:String = "-";
		public static var VERSION_WORK_PHP:String = "1139";
	}
}