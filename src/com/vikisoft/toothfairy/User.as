package com.vikisoft.toothfairy
{
	

	[Bindable]
	public class User
	{
		public static var _CLIENT_ID:String = "-";
		
		public static var isARM:Boolean = false;
		
		public static var ACCOUNT_ID:String = '';
		public static var ACCOUNT_LASTNAME:String = '';
		public static var ACCOUNT_FIRSTNAME:String = '';
		public static var ACCOUNT_MIDDLENAME:String = '';
		public static var ACCOUNT_INFO2:String = "-";
		
		
		//eHealth *** START ***
		private static var _EH_ACCESS_TOKEN:String = '';
		public static function get EH_ACCESS_TOKEN():String
		{
			return _EH_ACCESS_TOKEN;
		}
		public static function set EH_ACCESS_TOKEN(value:String):void
		{
			_EH_ACCESS_TOKEN = value;
			
			_EH_IS_ACCESS_TOKEN = false;
			if(_EH_ACCESS_TOKEN != null && _EH_ACCESS_TOKEN != '')
			{
				_EH_IS_ACCESS_TOKEN = true;
			}
		}
		
		private static var _EH_IS_ACCESS_TOKEN:Boolean = false;
		public static function get EH_IS_ACCESS_TOKEN():Boolean
		{
			return _EH_IS_ACCESS_TOKEN;
		}
		
		public static var EH_SECRET_KEY:String = "";
		public static var OA_DIR:String = "";
		public static var EH_SCOPES:String = "";
		public static var EMAIL:String = "";
		private static var _EH_LEGALENTITY_ID:String = "";
		public static function get EH_LEGALENTITY_ID():String
		{
			return _EH_LEGALENTITY_ID;
		}
		
		public static function set EH_LEGALENTITY_ID(value:String):void
		{
			_EH_LEGALENTITY_ID = value;
			
			_EH_IS_LEGALENTITY_ID = false;
			if(_EH_LEGALENTITY_ID != null && _EH_LEGALENTITY_ID != '')
			{
				_EH_IS_LEGALENTITY_ID = true;
			}
		}
		
		private static var _EH_IS_LEGALENTITY_ID:Boolean = false;
		public static function get EH_IS_LEGALENTITY_ID():Boolean
		{
			return _EH_IS_LEGALENTITY_ID;
		}
		//eHealth *** END ***
		
		
		public static var DOCTOR_ID:String = '';
		public static var DOCTOR_SHORTNAME:String = '';
		public static var DOCTOR_SPECIALITY:String = null;
		public static var ACCOUNT_CAHSIERCODE:String = '';
		public static var ACCOUNT_CAHSIER_REGISTERED:Boolean = false;
		public static var ACCOUNT_EMPTYRECEIPT_PRINTED:Boolean = false;
		
		
		public static var ASSISTANT_ID:String = '';
		public static var ASSISTANT_SHORTNAME:String = '';
		
		public static var SUBDIVISIONID:String = '';
		public static var SUBDIVISIONNAME:String = '';
		public static var SUBDIVISIONCITY:String = '';
		
		
		public static var configString:String = '';
		
		public static var ACTIVE_PRICE_LABEL:String = '';
		

	}
}