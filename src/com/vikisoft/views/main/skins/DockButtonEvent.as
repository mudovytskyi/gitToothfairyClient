package com.vikisoft.views.main.skins
{
	import flash.events.Event;
	
	public class DockButtonEvent extends Event
	{
		public static var SELECTED_CHANGE:String = "iconisSelectedUpdated";
		
		public var isSelected:Boolean;
		
		public function DockButtonEvent(type:String, isSelected:Boolean=false, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.isSelected = isSelected;
		}
		
		public override function clone():Event
		{
			return new DockButtonEvent(type, isSelected, bubbles, cancelable);
		}
		
		
	}
}