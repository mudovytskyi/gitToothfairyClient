package modules.ServerFileManagerModule.serverfile
{
	import flash.events.Event;
	
	import modules.ServerFileManagerModule.serverfile.ServerFile;
	
	public class ServerFileEvent extends Event
	{
		public var errorString:String;
		
		[Bindable]
		public var file:ServerFile;
		

		public static var EVENT_OPEN:String = 'sfOpen';
		public static var EVENT_UPLOADCOMPLETE:String = 'sfUploadComplete';
		public static var EVENT_EDIT:String = 'sfEdit';
		public static var EVENT_DELETE:String = 'sfDelete';
		public static var EVENT_TAGS:String = 'sfTags';
		public static var EVENT_RENAME:String = 'sfRename';
		public static var EVENT_ADD_TO_PRINT_QUEUE:String = 'sfAddToPrintQueue';
		public static var EVENT_REMOVE_FROM_PRINT_QUEUE:String = 'sfRemoveFromPrintQueue';
		
		public function ServerFileEvent(type:String, file:ServerFile=null, errorString:String = '',  bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.file = file;
			this.errorString = errorString;
		}
		
		public override function clone():Event 
		{
			return new ServerFileEvent(type, file, errorString);
		}
	}
}