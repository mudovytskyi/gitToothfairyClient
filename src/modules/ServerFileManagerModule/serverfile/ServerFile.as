package modules.ServerFileManagerModule.serverfile {
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	
	import org.alivepdf.encoding.Base64;
	
	import valueObjects.ServerFileModule_ServerFile;

    public class ServerFile extends ServerFileModule_ServerFile {
		[Bindable]
		public var file:EventDispatcher=null;
		
		[Bindable]
		public var thumb:ByteArray;
		
		[Bindable]
		public var isEditMode:Boolean = true;
		
        public function ServerFile(sf:ServerFileModule_ServerFile=null) {
            super();
			if(sf!=null)
			{
				this.id=sf.id;
				this.extention=sf.extention;
				this.comment=sf.comment;
				this.isThumbnailable=sf.isThumbnailable;
				this.name=sf.name;
				this.comment=sf.comment;
				this.patientId=sf.patientId;
				this.serverName=sf.serverName;
				this.protocolid=sf.protocolid;
				this.protocolName=sf.protocolName;
				if(this.isThumbnailable&&(sf.thumbData as String)!=null)
				{
					this.thumb=Base64.decode64(sf.thumbData as String);
				}
				this.tooth=sf.tooth;
				if(sf.tagValues==null)
				{
					this.tagValues=new ArrayCollection;
				}
				else
				{
					this.tagValues=sf.tagValues;
				}

			}
			else
			{
				this.tooth='';
				this.comment='';
				this.tagValues=new ArrayCollection;
			}
			
        } 
    }
}