package modules.ServerFileManagerModule.serverfile
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.utils.Base64Encoder;
	
	import modules.ServerFileManagerModule.serverfile.ServerFile;
	
	import services.ipClass;
	
	import toothfairy.configurations.ConstantSettings;
	
	public class ServerFileLoader extends EventDispatcher
	{	
		private static var SERVICE_URL:String;
		private var location:String;
		private var distination:File;
		private var urlRequest:URLRequest;
		private static var loader:ServerFileLoader;
		private var uploadArray:ArrayCollection=new ArrayCollection;
		private var downloadArray:ArrayCollection=new ArrayCollection;
		private var isNeedToOpen:Boolean;
		public static function Default():ServerFileLoader
		{
			if(loader==null) 
			{
				ServerFileLoader.SERVICE_URL = "http://"+ipClass.serverIp+"/toothfairy/services/ServerFileLoadModule.php";
				loader=new ServerFileLoader(null, ConstantSettings.FOLDER_DOCUMENTS);
			}
			return loader;
		}
		
		public function ServerFileLoader(target:IEventDispatcher=null,  distination:File=null, location:String="storage", isNeedToOpen:Boolean=true)
		{
			super(target);
			this.location=location;
			this.distination=distination;
			this.isNeedToOpen=isNeedToOpen;
			uploadArray.addEventListener(CollectionEvent.COLLECTION_CHANGE,onUploadArrayChange,false,0,true);
			downloadArray.addEventListener(CollectionEvent.COLLECTION_CHANGE,onDownloadArrayChange,false,0,true);
		}
		
		private function onUploadArrayChange(event:CollectionEvent):void
		{
			if((event.kind==CollectionEventKind.ADD&&(event.target as ArrayCollection).length==1)||
				(event.kind==CollectionEventKind.REMOVE&&(event.target as ArrayCollection).length>0))
			{
				upload(uploadArray.getItemAt(0) as ServerFile);					
			}
		}
		
		private function onDownloadArrayChange(event:CollectionEvent):void
		{
			if((event.kind==CollectionEventKind.ADD&&(event.target as ArrayCollection).length==1)||
				(event.kind==CollectionEventKind.REMOVE&&(event.target as ArrayCollection).length>0))
			{
				var sf:ServerFile=downloadArray.getItemAt(0) as ServerFile;
				download(sf);					
			}
		}
		
		public function load(sf:ServerFile, isDownload:Boolean=false):void
		{
			if(!isDownload)
			{
				uploadArray.addItem(sf);
			}
			else
			{
			/*	if(sf.type==ServerFile$Type.FOLDER)
				{
					return;
				}
				*/
				downloadArray.addItem(sf);
			}
		}
		
		
		///--------------------------------------------------------UPLOAD-----------------------------------------////
		private function upload(sf:ServerFile):void
		{
			/*if(sf.type==ServerFile$Type.FILE)
			{*/
				urlRequest=new URLRequest(SERVICE_URL);
				
				var encoder:Base64Encoder = new Base64Encoder();        
				encoder.encode("admin:111777");
				var credsHeader:URLRequestHeader = new URLRequestHeader("Authorization", "Basic " + encoder.toString());
				urlRequest.requestHeaders.push(credsHeader);
				
				var parameters:URLVariables = new URLVariables();
				parameters.servername = sf.serverName;
				parameters.name = sf.name;
				parameters.extention = sf.extention;
				parameters.patientId = sf.patientId;
				parameters.location = location;
				if(sf.type != null && sf.type != '')
				{
					parameters.type = sf.type;
				}
				urlRequest.data = parameters;
				//*listners
				//trace('upload '+sf.name);
				sf.file.addEventListener(IOErrorEvent.IO_ERROR, onUploadError, false, 0, true);
				sf.file.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onUploadError, false, 0, true);
				sf.file.addEventListener(Event.COMPLETE, onCompleteUpload, false, 0, true);
				//listners*
				
				(sf.file as FileReference).upload(urlRequest);
			/*}*/
		}
		
		private function onCompleteUpload(event:Event):void
		{
			var sf:ServerFile=uploadArray.getItemAt(0) as ServerFile;
			sf.file=null;
			uploadArray.removeItemAt(0);
			//trace('onCompleteUpload '+sf.name);
			dispatchEvent(new ServerFileEvent(ServerFileEvent.EVENT_UPLOADCOMPLETE,sf));
		}
		///--------------------------------------------------------UPLOAD-----------------------------------------////
		
		
		///--------------------------------------------------------DOWNLOAD-----------------------------------------////
		private function download(sf:ServerFile):void
		{
			
			
			urlRequest=new URLRequest(SERVICE_URL);
			urlRequest.method = URLRequestMethod.GET;
			
			var encoder:Base64Encoder = new Base64Encoder();        
			encoder.encode("admin:111777");
			var credsHeader:URLRequestHeader = new URLRequestHeader("Authorization", "Basic " + encoder.toString());
			urlRequest.requestHeaders.push(credsHeader);
			
			var parameters:URLVariables = new URLVariables();
			parameters.servername = sf.serverName;
			parameters.location = location;
			urlRequest.data = parameters;
			
			var urlLoader:URLLoader=new URLLoader();
			urlLoader.dataFormat=URLLoaderDataFormat.BINARY;
			
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onDownloadError, false, 0, true);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onDownloadError, false, 0, true);
			urlLoader.addEventListener(Event.COMPLETE, onSavingStart, false, 0, true);
			
			sf.file=urlLoader;
			(sf.file as URLLoader).load(urlRequest);
			
		}
		
		
		private function onSavingStart(event:Event):void
		{
			var sf:ServerFile=downloadArray.getItemAt(0) as ServerFile;
			sf.file=distination.resolvePath(sf.name); 
			var fileStream:FileStream = new FileStream();  
			fileStream.addEventListener(Event.CLOSE, onCompleteDownload, false, 0, true);  
			fileStream.openAsync(sf.file as File, FileMode.WRITE);  
			fileStream.writeBytes(event.target.data, 0, event.target.data.length);  
			fileStream.close();  
		}
		
		
		private function onCompleteDownload(event:Event):void
		{
			var sf:ServerFile=downloadArray.getItemAt(0) as ServerFile;
			if(isNeedToOpen)
			{
				openFile(sf);
			}
			downloadArray.removeItemAt(0);
		}
		
		private  function openFile(sf:ServerFile):void
		{
/*			if(sf.type==ServerFile$Type.FILE)
			{*/
				try
				{
					(sf.file as File).openWithDefaultApplication();
					
				}
				catch(error:Error) {
					(sf.file as File).save(sf.name);
				}
				sf.file=null;
			/*}*/
		}
		///--------------------------------------------------------DOWNLOAD-----------------------------------------////
		
		
		///--------------------------------------------------------ERRORS-----------------------------------------////
		public function onUploadError(event:Event):void
		{
			var sf:ServerFile=uploadArray.getItemAt(0) as ServerFile;
			var errorString:String;	
			switch(event.type)
			{
				case SecurityErrorEvent.SECURITY_ERROR:
				{
					errorString+=ERROR_SECURITY+': '+ (event as SecurityErrorEvent).errorID;
					break;
				}
				case IOErrorEvent.IO_ERROR:
				{
					errorString+=ERROR_IO+': '+ (event as IOErrorEvent).errorID;
					break;
				}
				default:
				{
					errorString+=ERROR_UNKNOWN+': '+event.toString();
					break;
				}
			}
			
			sf.file=null;
			uploadArray.removeItemAt(0);
			Alert.show('Файл: '+sf.name+'\nUID: '+sf.serverName+'\n'+errorString);
		}
		
		public static var ERROR_SECURITY:String='';
		public static var ERROR_IO:String='';
		public static var ERROR_UNKNOWN:String='';
		
		public function onDownloadError(event:Event):void
		{
			var sf:ServerFile=downloadArray.getItemAt(0) as ServerFile;
			var errorString:String;	
			switch(event.type)
			{
				case SecurityErrorEvent.SECURITY_ERROR:
				{
					errorString+=ERROR_SECURITY+': '+ (event as SecurityErrorEvent).errorID;
					break;
				}
				case IOErrorEvent.IO_ERROR:
				{
					errorString+=ERROR_IO+': '+ (event as IOErrorEvent).errorID;
					break;
				}
				default:
				{
					errorString+=ERROR_UNKNOWN+': '+event.toString();
					break;
				}
			}
			sf.file=null;
			downloadArray.removeItemAt(0);
			Alert.show(sf.name+'\nUID: '+sf.serverName+'\n'+errorString);
		}
		///--------------------------------------------------------ERRORS-----------------------------------------////
	}
}