package modules.ServerFileManagerModule.assets
{
	[Bindable]
	public class AssetsServerFile
	{
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/Camera.png")]
		public static const BUTTON_CAMERA_ICON:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/cameraIcon.png")]
		public static const cameraButtonIcon:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/cameraVideoStartIcon.png")]
		public static const cameraVideoStartIcon:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/cameraVideoStopIcon.png")]
		public static const cameraVideoStopIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/image.png")]
		public static const imageIMAGEIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/doc.png")]
		public static const imageDOCIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/docx.png")]
		public static const imageDOCXIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/pdf.png")]
		public static const imagePDFIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/rar.png")]
		public static const imageRARIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/doc.png")]
		public static const imageRTFIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/txt.png")]
		public static const imageTXTIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/xls.png")]
		public static const imageXLSIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/xlsx.png")]
		public static const imageXLSXIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/zip.png")]
		public static const imageZIPIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/flv.png")]
		public static const imageFLVIcon:Class;
		
		[Embed(source = "modules/ServerFileManagerModule/assets/filetypeicons/undefined.png")]
		public static const imageUNDEFINEDIcon:Class;
		
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/okIcon8.png")]
		public static var ICON_OK_8:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/cancelIcon8.png")]
		public static var ICON_CANCEL_8:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/okIcon15.png")]
		public static var ICON_OK_15:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/cancelIcon15.png")]
		public static var ICON_CANCEL_15:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/okIcon20.png")]
		public static var ICON_OK_20:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/cancelIcon20.png")]
		public static var ICON_CANCEL_20:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/editIcon16.png")]
		public static var ICON_EDIT_16:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/tagsIcon16.png")]
		public static var ICON_TAGS_16:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/renameIcon16.png")]
		public static var ICON_RENAME_16:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/addIcon32.png")]
		public static var ADD_ICON_32:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/removeIcon32.png")]
		public static var REMOVE_ICON_32:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/updateIcon32.png")]
		public static var UPDATE_ICON_32:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/addIcon16.png")]
		public static var ADD_ICON_16:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/removeIcon16.png")]
		public static var REMOVE_ICON_16:Class;
		
		[Embed(source="modules/ServerFileManagerModule/assets/buttons/updateIcon16.png")]
		public static var UPDATE_ICON_16:Class;
		
		private static var _fileTypeIcons:Array;
			
			public static function get fileTypeIcons():Array
			{
				if(_fileTypeIcons==null)
				{
					_fileTypeIcons = new Array();
					_fileTypeIcons["bmp"] = AssetsServerFile.imageIMAGEIcon;
					_fileTypeIcons["doc"] = AssetsServerFile.imageDOCIcon;
					_fileTypeIcons["docx"] = AssetsServerFile.imageDOCXIcon;
					_fileTypeIcons["gif"] = AssetsServerFile.imageIMAGEIcon;
					_fileTypeIcons["jpeg"] = AssetsServerFile.imageIMAGEIcon;
					_fileTypeIcons["jpg"] = AssetsServerFile.imageIMAGEIcon;
					_fileTypeIcons["pdf"] = AssetsServerFile.imagePDFIcon;
					_fileTypeIcons["png"] = AssetsServerFile.imageIMAGEIcon;
					_fileTypeIcons["rar"] = AssetsServerFile.imageRARIcon;
					_fileTypeIcons["rtf"] = AssetsServerFile.imageRTFIcon;
					_fileTypeIcons["txt"] = AssetsServerFile.imageTXTIcon;
					_fileTypeIcons["xls"] = AssetsServerFile.imageXLSIcon;
					_fileTypeIcons["xlsx"] = AssetsServerFile.imageXLSXIcon;
					_fileTypeIcons["zip"] = AssetsServerFile.imageZIPIcon;
					_fileTypeIcons["flv"] = AssetsServerFile.imageFLVIcon;
					_fileTypeIcons["avi"] = AssetsServerFile.imageFLVIcon;
					_fileTypeIcons["mp4"] = AssetsServerFile.imageFLVIcon;
					_fileTypeIcons["mov"] = AssetsServerFile.imageFLVIcon;
					_fileTypeIcons["undefined"] = AssetsServerFile.imageUNDEFINEDIcon;
				}
				return _fileTypeIcons;
			}
		
	}
}