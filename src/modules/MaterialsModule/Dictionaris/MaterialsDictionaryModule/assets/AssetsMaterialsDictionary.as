package modules.MaterialsModule.Dictionaris.MaterialsDictionaryModule.assets
{
	

	public class AssetsMaterialsDictionary
	{
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/farmIcon.png")]
		public static var farmIcon:Class;
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/folderClosedIcon.png")]
		public static var folderClosedIcon:Class;
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/folderOpenIcon.png")]
		public static var folderOpenIcon:Class;
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/subFolderIcon.png")]
		public static var folderSubIcon:Class;
		
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/subFolderIconAdd25.png")]
		public static var folderSubIconAdd25:Class;
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/farmIconAdd25.png")]
		public static var farmIconAdd25:Class;
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/farmIconEdit25.png")]
		public static var farmIconEdit25:Class;
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/folderClosedIconAdd25.png")]
		public static var folderClosedIconAdd25:Class;
		[Bindable]
		[Embed(source="modules/MaterialsModule/Dictionaris/MaterialsDictionaryModule/assets/folderClosedIconEdit25.png")]
		public static var folderClosedIconEdit25:Class;
	}
}