package modules.MaterialsModule.Dictionaris.MaterialsDictionaryModule.components
{
	import flash.utils.ByteArray;

	public class ObjectFunction
	{
		public function ObjectFunction()
		{
		}	
		
		public static function clone(obj: *) : Object
		{
			var clone: ByteArray = new ByteArray();
			clone.writeObject(obj);
			clone.position = 0;
			
			return clone.readObject();
		}	
	}
}