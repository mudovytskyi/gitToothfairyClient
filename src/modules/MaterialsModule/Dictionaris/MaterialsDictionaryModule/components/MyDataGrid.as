package modules.MaterialsModule.Dictionaris.MaterialsDictionaryModule.components
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.DataGrid;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.mx_internal;
	
	public class MyDataGrid extends DataGrid
	{
		public var rowColorFunction: Function;
		
		public function MyDataGrid()
		{
			super();
		}
		
		public function getMousePosition(event:MouseEvent):Point
		{		
			var r: IListItemRenderer;			
			
			r = mx_internal:: mouseEventToItemRendererOrEditor(event);	
			
			if (itemEditorInstance && (r == itemEditorInstance))
			{
				return new Point(editedItemPosition.columnIndex, editedItemPosition.rowIndex);
			}
			else
			{
				return itemRendererToIndices(r);
			}						
		}	
		
		override protected function drawRowBackground(s:Sprite,	rowIndex:int, y:Number, height:Number, color:uint, dataIndex:int):void
		{
			if(rowColorFunction != null && dataProvider != null) 
			{
				var item:Object;
				if(dataIndex < dataProvider.length)
				{
					item = dataProvider[dataIndex];
				}
				
				if(item)
				{
					color = rowColorFunction(item, rowIndex, dataIndex, color);
				}
			}
			
			super.drawRowBackground(s, rowIndex, y, height, color, dataIndex);
		}
	}
}