package modules.MaterialsModule.Dictionaris.MaterialsDictionaryModule.components
{
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.AdvancedDataGrid;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.mx_internal;
	
	public class MyAdvancedDataGrid extends AdvancedDataGrid
	{
		public function MyAdvancedDataGrid()
		{
			super();
		}
		
		public function getMousePosition(event:MouseEvent):Point
		{		
			var r: IListItemRenderer;			
			
			r = mx_internal:: mouseEventToItemRendererOrEditor(event);	
			
			if (itemEditorInstance && (r == itemEditorInstance))
			{
				return new Point(editedItemPosition.columnIndex, editedItemPosition.rowIndex);
			}
			else
			{
				var point: Point = new Point();
				
				try
				{
					point = itemRendererToIndices(r);
				}
				catch(errObject: Error) 
				{
					//trace(errObject.message);
				}
				
				return point;
			}						
		}
		
		public function getColumnIndex(column: AdvancedDataGridColumn): int
		{
			var columnIndex: int = -1;
			
			for (var i: int = 0; i < this.columns.length; i++)
			{
				if (this.columns[i] == column)
				{
					columnIndex = i;
					break;
				}
			} 
			
			return columnIndex;
		}
	}
}