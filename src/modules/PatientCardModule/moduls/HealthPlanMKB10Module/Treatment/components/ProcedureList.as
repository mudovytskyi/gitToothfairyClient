package modules.PatientCardModule.moduls.HealthPlanMKB10Module.Treatment.components
{
	import spark.components.List;
	
	public class ProcedureList extends List
	{
		[bindable]
		public var IS_GENERAL:Boolean = false;
		
		public function ProcedureList()
		{
			super();
		}
	}
}