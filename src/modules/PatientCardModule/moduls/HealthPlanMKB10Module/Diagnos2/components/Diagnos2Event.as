package modules.PatientCardModule.moduls.HealthPlanMKB10Module.Diagnos2.components
{
	import flash.events.Event;
	
	import valueObjects.HealthPlanMKB10Diagnos2;
	
	public class Diagnos2Event extends Event
	{
		public var diagnos2:HealthPlanMKB10Diagnos2;
		
		public function Diagnos2Event(type:String, diagnos2:HealthPlanMKB10Diagnos2, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.diagnos2 = diagnos2;
		}
	}
	
}