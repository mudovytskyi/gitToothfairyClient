package modules.PatientCardModule.moduls.HealthPlanMKB10Module.components.additionswindow
{
	import flash.events.Event;
	
	import valueObjects.HealthPlanMKB10TreatmentAdditions;
	
	public class AdditionsEvent extends Event
	{
		public static const UPDATE_DATE:String = 'updateAdditionDate';
		public static const DELETE:String = 'deleteAddition';
		public static const ADD:String = 'addAddition';
		
		public var addition:HealthPlanMKB10TreatmentAdditions;
		
		public function AdditionsEvent(type:String, addition:HealthPlanMKB10TreatmentAdditions, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.addition = addition;
		}
	}
}