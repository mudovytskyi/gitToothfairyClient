package modules.PatientCardModule.moduls.HealthPlanMKB10Module.components
{
	[Bindable]
	public final class AdditionType
	{
		public static const ANAMNES:int = 0;
		public static const STATUS:int = 1;
		public static const RECOMMENDATION:int = 2;
		public static const EPICRISIS:int = 3;
		public static const XRAY:int = 4;
	}
}