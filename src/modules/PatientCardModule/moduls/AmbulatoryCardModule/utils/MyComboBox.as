package modules.PatientCardModule.moduls.AmbulatoryCardModule.utils
{
	import spark.components.ComboBox;
	public class MyComboBox extends ComboBox
	{
		public function MyComboBox()
		{
			super();
		}
		
		/**
		 * like ValueMember in .NET System.Windows.Forms.ComboBox
		 */
		[Bindable]
		public var idField:String;
		
		public function get selectedId():Object {
			if(idField == null || selectedItem == null) return null;
			
			if(selectedItem.hasOwnProperty(idField)) {
				return selectedItem[idField];
			}
			return null;
		}
		
//		public function set selectedId(value:Object):void {
//			if(idField == null || idField == null) return;
//			for each(var item:Object in collection) {
//				if(item.hasOwnProperty(idField) && item[idField] == value) {
//					selectedItem = item;
//					break;
//				}
//			}
//		}
		
	}
}