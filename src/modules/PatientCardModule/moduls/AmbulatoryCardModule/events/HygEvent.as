package modules.PatientCardModule.moduls.AmbulatoryCardModule.events
{
	import flash.events.Event;
	
	import valueObjects.AmbulatoryCardModuleHygcontrol;
	
	public class HygEvent extends Event
	{
		public var hyg:AmbulatoryCardModuleHygcontrol;
		public var text:String;
		public function HygEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			var ev:HygEvent = new HygEvent(type);
			ev.hyg = this.hyg;
			return ev;
		}
	}
}