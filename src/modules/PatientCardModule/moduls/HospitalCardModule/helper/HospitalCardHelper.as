/********************************************
 title   : HospitalCardHelper.as
 package : modules.PatientCardModule.moduls.HospitalCardModule.helper
 version : 1.0
 author  : Mykola Udovytskyi
 website : https://toothfairy.dev.com
 date    : Sep 17, 2019  time : 5:50:35 PM
 ********************************************/
package modules.PatientCardModule.moduls.HospitalCardModule.helper
{
	//---------------------------------
	//
	// Libraries
	//
	//---------------------------------
	import flash.errors.IllegalOperationError;
	import flash.net.registerClassAlias;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import modules.PatientCardModule.moduls.HospitalCardModule.utils.HospitalCardFormatter;
	
	import toothfairy.statics.Exchenger;
	
	import valueObjects.HospitalCardDataFull;
	import valueObjects.HospitalCardDiagnosis;
	import valueObjects.HospitalCardDisabilityCertificate;
	import valueObjects.HospitalCardProcedure;

	/**
	 *   @extends: 
	 *   @implements:
	 *   @use: 
	 */
	//---------------------------------
	//
	// Class : HospitalCardHelper.as
	//
	//---------------------------------
	public final class HospitalCardHelper
	{
		private static function get MainDiagnosis():HospitalCardDiagnosis
		{
			var mainDiagnosis:HospitalCardDiagnosis = new HospitalCardDiagnosis();
			mainDiagnosis.IS_MAIN = true;
			mainDiagnosis.IS_CHECKED = true;
			return mainDiagnosis;
		}
		
		private static const diagnosisDP:ArrayCollection = new ArrayCollection(
			[
				MainDiagnosis,
				new HospitalCardDiagnosis(),
				new HospitalCardDiagnosis(),
				new HospitalCardDiagnosis(),
				new HospitalCardDiagnosis(),
				new HospitalCardDiagnosis(),
				new HospitalCardDiagnosis(),
				new HospitalCardDiagnosis(),
		]);
		
		private static const proceduresDP:ArrayCollection = new ArrayCollection(
			[
				new HospitalCardProcedure(),
				new HospitalCardProcedure()
		]);
		
		private static const certificatesDP:ArrayCollection = new ArrayCollection(
			[
				new HospitalCardDisabilityCertificate(),
				new HospitalCardDisabilityCertificate()
		]);
		
		
		//---------------------------------
		//
		// Constructor
		//
		//---------------------------------
		public function HospitalCardHelper()
		{
			throw new IllegalOperationError(StringUtil.substitute("Can't instantiate this class {0}",[getQualifiedClassName(this)]));
		}
		
		//---------------------------------
		//
		// Static public methods
		//
		//---------------------------------
		public static function getNewHospitalCardPagesData():HospitalCardDataFull
		{
			var data:HospitalCardDataFull = new HospitalCardDataFull();

			data.hospitalDiagnosis = clone(diagnosisDP) as ArrayCollection;
			data.hospitalProcedueres = clone(proceduresDP) as ArrayCollection;
			data.hospitalDisabilityCerts = clone(certificatesDP) as ArrayCollection;
			
			data.ID = "";
			data.PATIENT_ID = Exchenger.selectedPatientId;
			data.CARD_NUMBER = "";
			data.CREATED = HospitalCardFormatter.getClientDateTimeString();
			data.OPENED = "";
			data.CLOSED = "";
			data.CLINIC_NAME = "";
			data.CLINIC_ADDRESS = "";
			data.CLINIC_REGISTRATION_CODE = "";
			data.PATIENT_SEX = "";
			data.PATIENT_FULL_NAME = Exchenger.selectedPatientFIO;
			data.PATIENT_BIRTH = "";
			data.PATIENT_AGE = "";
			data.PATIENT_DOCUMENT = "";
			data.PATIENT_DOCUMENT_NUMBER = "";
			data.PATIENT_CITIZENSHIP = "";
			data.HABITATION_TYPE = "";
			data.ADDRESS_PART_1 = "";
			data.ADDRESS_PART_2 = "";
			data.POSTAL_CODE = "";
			data.WORKPLACE_PART_1
			data.WORKPLACE_PART_2 = "";
			data.REFFERENT_CLINIC = "";
			data.REFFERENT_REGISTRATION_CODE = "";
			data.INCOME_DIAGNOSIS = "";
			data.INCOME_DIAGNOSIS_MKH = "";
			data.DEPARTMENT_CODE_INCOME = "";
			data.DEPARTMENT_CODE_OUTCOME = "";
			data.HOSPITALIZATION_TYPE = "";
			data.LAST_HIV_PROBE_DATE = "";
			data.BLOOD_TYPE = "";
			data.BLOOD_RH = "";
			data.LAST_VASSERMAN_REACTION_DATE = "";
			data.FARM_ALLERGY_REACTIONS_PART_1 = "";
			data.FARM_ALLERGY_REACTIONS_PART_2 = "";
			data.HOSPITALIZATION_REPEATED_IN_YEAR = "";
			data.HOSPITALIZATION_REPEATED_IN_MONTH = "";
			data.TREATED_BED_DAYS = "";
			data.INJURY_TYPE = "";
			data.ADDITIONAL_OUTCOME_DIAGNOSIS = "";
			data.RESISTANTION_CATEGORY = "";
			data.DOCTOR_FULL_NAME_DIAGNOS = "";
			data.DOCTOR_SIGN_DIAGNOS = "";
			data.DOCTOR_REGISTRATION_DIAGNOS = "";
			data.DOCTOR_DATE_DIAGNOS = "";
			data.OTHER_TREATMENTS = "";
			data.CANCER_TREATMENT_TYPE = "";
			data.EMPLOYABILITY = "";
			data.EXPERTISE_CONCLUSION = "";
			data.TREATMENT_RESULT = "";
			data.ONCOLOGY_CHECKUP_DATE = "";
			data.CHEST_ORGANS_CHECKUP_DATE = "";
			data.INSURANCY = "";
			data.DOCTOR_FULL_NAME_OUTCOME = "";
			data.DOCTOR_SIGN_OUTCOME = "";
			data.DOCTOR_REGISTRATION_OUTCOME = "";
			data.HEADDEPARTMENT_FULL_NAME = "";
			data.HEADDEPARTMENT_SIGN = "";
			data.HEADDEPARTMENT_REGISTRATION = "";
			
			return data;
		}
		
		public static function preparePrefilledHospitalCardPagesData(data:HospitalCardDataFull):HospitalCardDataFull
		{
			data.hospitalDiagnosis = clone(diagnosisDP) as ArrayCollection;
			data.hospitalProcedueres = clone(proceduresDP) as ArrayCollection;
			data.hospitalDisabilityCerts = clone(certificatesDP) as ArrayCollection;
			
			data.CLOSED = "";
			data.REFFERENT_CLINIC = "";
			data.REFFERENT_REGISTRATION_CODE = "";
			data.INCOME_DIAGNOSIS = "";
			data.INCOME_DIAGNOSIS_MKH = "";
			data.DEPARTMENT_CODE_INCOME = "";
			data.DEPARTMENT_CODE_OUTCOME = "";
			data.HOSPITALIZATION_TYPE = "";
			data.LAST_HIV_PROBE_DATE = "";
			data.BLOOD_TYPE = "";
			data.BLOOD_RH = "";
			data.LAST_VASSERMAN_REACTION_DATE = "";
			data.FARM_ALLERGY_REACTIONS_PART_1 = "";
			data.FARM_ALLERGY_REACTIONS_PART_2 = "";
			data.HOSPITALIZATION_REPEATED_IN_YEAR = "";
			data.HOSPITALIZATION_REPEATED_IN_MONTH = "";
			data.TREATED_BED_DAYS = "";
			data.INJURY_TYPE = "";
			data.ADDITIONAL_OUTCOME_DIAGNOSIS = "";
			data.RESISTANTION_CATEGORY = "";
			data.DOCTOR_FULL_NAME_DIAGNOS = "";
			data.DOCTOR_SIGN_DIAGNOS = "";
			data.DOCTOR_REGISTRATION_DIAGNOS = "";
			data.DOCTOR_DATE_DIAGNOS = "";
			data.OTHER_TREATMENTS = "";
			data.CANCER_TREATMENT_TYPE = "";
			data.EMPLOYABILITY = "";
			data.EXPERTISE_CONCLUSION = "";
			data.TREATMENT_RESULT = "";
			data.ONCOLOGY_CHECKUP_DATE = "";
			data.CHEST_ORGANS_CHECKUP_DATE = "";
			data.INSURANCY = "";
			data.DOCTOR_FULL_NAME_OUTCOME = "";
			data.DOCTOR_SIGN_OUTCOME = "";
			data.DOCTOR_REGISTRATION_OUTCOME = "";
			data.HEADDEPARTMENT_FULL_NAME = "";
			data.HEADDEPARTMENT_SIGN = "";
			data.HEADDEPARTMENT_REGISTRATION = "";
			
			return data;
		}
		
		public static function clone(source:Object):* 
		{ 
			var myBA:ByteArray = new ByteArray(); 
			myBA.writeObject(source); 
			myBA.position = 0; 
			
			return(myBA.readObject()); 
		}
		
		public static function copyTypeSafe(ac:ArrayCollection):ArrayCollection
		{
			var cloneAc:ArrayCollection = new ArrayCollection();
			
			if (ac.length == 0) {
				return cloneAc;
			}
			
			var className:String = getQualifiedClassName(ac.getItemAt(0));
			registerClassAlias(className, (getDefinitionByName(className) as Class));
			
			for each (var obj:Object in ac)  {
				cloneAc.addItem(ObjectUtil.copy(obj));
			}
			
			return cloneAc;
			
		}
	}
}