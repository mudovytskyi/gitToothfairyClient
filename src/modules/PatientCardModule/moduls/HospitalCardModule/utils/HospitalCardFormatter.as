package modules.PatientCardModule.moduls.HospitalCardModule.utils
{
	import com.vikisoft.toothfairy.Settings;
	
	import spark.formatters.DateTimeFormatter;

	public class HospitalCardFormatter
	{
		private static const DATE_TIME_FORMATTER:DateTimeFormatter = new DateTimeFormatter();
		
		public static function convertToTimestamp(str:String):String
		{
			if (!str) 
				return str;
			return [str.substr(0, 2), str.substr(2, 2), str.substr(4, 2)].join('.') + '  ' +
				[str.substr(6,2), str.substr(8,2), str.substr(10,2)].join(':');
		}
		
		public static function getClientDateTimeString(date:Date = null):String
		{
			date ||= new Date();
			DATE_TIME_FORMATTER.dateTimePattern = "ddMMyyHHmmss";
			return DATE_TIME_FORMATTER.format(date);
		}
	}
}