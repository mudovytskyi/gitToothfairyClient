

import actionscriptdatetimelibrary.DateTimeField;

import com.adobe.fiber.runtime.lib.DateTimeFunc;

import components.date.ToolsForFirebird;
import components.string.TextLib;

import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;
import flash.desktop.NativeProcess;
import flash.desktop.NativeProcessStartupInfo;
import flash.events.Event;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.net.FileReference;
import flash.net.SharedObject;
import flash.utils.ByteArray;

import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.formatters.DateFormatter;
import mx.managers.PopUpManager;

import toothfairy.settings.ProgramSettings;

private function StartViziograph(ID:String, FIRSTNAME:String, LASTNAME:String, DOB:String, CARDNUMBER:String, SEX:int):void
{
	var visiographApplication:String = SharedObject.getLocal("toothfairy").data["visiographApplication"];
	if((DOB == null) || (DOB.length == 0) || (DOB == ''))
	{
		var _defDate:Date = new Date();
		_defDate.fullYear = 1970;
		_defDate.month = 0;
		_defDate.date = 1;
		_defDate.hours = 0;
		_defDate.minutes = 0;
		_defDate.seconds = 0;
		_defDate.milliseconds = 0;
		DOB = ToolsForFirebird.dateToFirebirdDateString(_defDate);
	}			
	
	
	if ((visiographApplication == null) || (visiographApplication == "None"))
	{
		Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotSelected'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);
		return;
	}
	else
	{
		
		var fname:String = new String;
		var lname:String = new String;
		
		var np:NativeProcess;
		var info:NativeProcessStartupInfo;
		var args:Vector.<String>;
		var df:DateFormatter = new DateFormatter();
		switch (visiographApplication)
		{						
			case "VixWin":
				var pathToExecutableVixWin: String = SharedObject.getLocal("toothfairy").data["pathToExecutableVixWin"];		
				var pathToDataBaseVixWin: String = SharedObject.getLocal("toothfairy").data["pathToDataBaseVixWin"];			
				
				if ((pathToExecutableVixWin == null) || (pathToExecutableVixWin == "") ||
					(pathToDataBaseVixWin == null) || (pathToDataBaseVixWin == "")) 
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);								
				}	
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToExecutableVixWin + '\\vixwin.exe');
						
						args = new Vector.<String>();
						args.push("-I");
						args.push(ID);
						args.push("-N");
						args.push(FIRSTNAME);
						args.push("^");
						args.push(LASTNAME);
						args.push("-P");
						args.push(pathToDataBaseVixWin+"\\"+ID);
						info.arguments = args;
						
						np.start(info);
						//np.exit();
						//ViziographStartModuleExecute("open", pathToExecutableVixWin+'\\vixwin.exe', "-I "+ ID + " -N "+ FIRSTNAME + "^"+ LASTNAME + " -P " + pathToDataBaseVixWin+"\\"+ID);
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
			
			case "ProgenyImaging":							
				var pathToExecutableProgenyImaging: String = SharedObject.getLocal("toothfairy").data["pathToExecutableProgenyImaging"];
				
				if ((pathToExecutableProgenyImaging == null) || (pathToExecutableProgenyImaging == ""))
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);							
				}
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToExecutableProgenyImaging + '\\PIBridge.exe');
						
						args = new Vector.<String>();
						args.push("cmd=open");
						args.push("id=" + ID);
						args.push("first=" + FIRSTNAME);
						args.push("last=" + LASTNAME);
						args.push("dob=" + DOB);
						info.arguments = args;
						
						np.start(info);
						//np.exit();
						//ViziographStartModuleExecute("open", pathToExecutableProgenyImaging+'\\PIBridge.exe', " cmd=open, id="+ ID + ", first="+ FIRSTNAME + ", last="+ LASTNAME + ", dob=" + DOB);
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
			
			case "CSNImage":
				var pathToExecutableCSNImage: String = SharedObject.getLocal("toothfairy").data["pathToExecutableCSNImage"];
				var pathToDataBaseCSNImage: String = SharedObject.getLocal("toothfairy").data["pathToDataBaseCSNImage"];
				var clinicNameCSNImage: String = SharedObject.getLocal("toothfairy").data["clinicNameCSNImage"];
				var clinicAddressCSNImage: String = SharedObject.getLocal("toothfairy").data["clinicAddressCSNImage"];
				
				df.formatString = "DD/MM/YYYY";
				if ((pathToExecutableCSNImage == null) || (pathToExecutableCSNImage == "") ||
					(pathToDataBaseCSNImage == null) || (pathToDataBaseCSNImage == "") ||
					(clinicNameCSNImage == null) || (clinicNameCSNImage == "") ||
					(clinicAddressCSNImage == null) || (clinicAddressCSNImage == ""))
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);								
				}	
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToExecutableCSNImage+'\\TW.exe');
						
						var args_string:String = '"-P' + pathToDataBaseCSNImage + '\\' + CARDNUMBER +
												'-N' + LASTNAME + ' ' + FIRSTNAME + '" "' + '-C1' + clinicNameCSNImage +
												'-C2' + clinicAddressCSNImage + '" "' + '-BD' + df.format(DOB) + '"';
						
						args = new Vector.<String>();
						args.push(args_string);
						/*args.push('"');
						args.push("-P");
						args.push(pathToDataBaseCSNImage+ "\\" + CARDNUMBER);
						args.push("-N");
						args.push(LASTNAME+" "+FIRSTNAME);
						args.push('" "');
						args.push("-C1");
						args.push(clinicNameCSNImage);
						args.push("-C2");
						args.push(clinicAddressCSNImage);
						args.push('" "');
						args.push("-BD");
						args.push(df.format(DOB));
						args.push('"');*/
						info.arguments = args;
						
						np.start(info);
						//np.exit();
						//ViziographStartModuleExecute("open", pathToExecutableCSNImage+'\\TW.exe', ' "-P' + pathToDataBaseCSNImage + '\\' + CARDNUMBER + '-N' + LASTNAME + ' ' + FIRSTNAME + '" "-C1' + clinicNameCSNImage + '-C2' + clinicAddressCSNImage + '" "' + '-BD' + 	df.format(DOB)+'"');
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
			
			case "Planmeca Romexis":
				var pathToDxStartRomexis: String = SharedObject.getLocal("toothfairy").data["pathToDxStartRomexis"];			
				df.formatString = "DDMMYYYY";
				if ((pathToDxStartRomexis == null) || (pathToDxStartRomexis == "")) 
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);								
				}	
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						//info.executable = new File(pathToDxStartRomexis + '\\PIBridge.exe');
						info.executable = new File(pathToDxStartRomexis + '\\DxStart.exe');
						
						args = new Vector.<String>();
						//args.push(pathToDxStartRomexis+"\\DxStart.exe");
						args.push('"' + ID + '"');
						args.push('"' + TextLib.translit2LatinWA(LASTNAME) + '"');
						args.push('"' + TextLib.translit2LatinWA(FIRSTNAME) + '"');
						args.push('"'+df.format(DOB)+'"');
						info.arguments = args;
						
						np.start(info);
						//np.exit();
						//ViziographStartModuleExecute("open", pathToDxStartRomexis+'\\DxStart.exe', ' "'+ ID + '" ' + '"' + LASTNAME + '" ' + '"' + FIRSTNAME + '" ' + '"'+df.format(DOB)+'"');//Planmeca\\Romexis\\pmbridge\\Program\\
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;			
			
			case "DBSWIN":
				var pathToDBSWINexecutable: String = SharedObject.getLocal("toothfairy").data["pathToDBSWINexecutable"];		
				var pathToImportFileFolder: String = SharedObject.getLocal("toothfairy").data["pathToImportFileFolder"];	
				var nameImportFile: String = SharedObject.getLocal("toothfairy").data["nameImportFile"];	
				var importFileEncoding:String = SharedObject.getLocal("toothfairy").data["importFileEncoding"];
				var runDBSWINCheckBox:Boolean = SharedObject.getLocal("toothfairy").data["runDBSWINCheckBox"];
				
				df.formatString = "DD.MM.YYYY";
				if(runDBSWINCheckBox == true)
				{
					var file:File;
					var fileStream:FileStream;
					var FMsex:String;
					if((pathToImportFileFolder == null) || (pathToImportFileFolder == "")||
						(nameImportFile == null) || (nameImportFile == ""))
					{
						Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);
					}
					else
					{
						fileStream = new FileStream();
						file = File.documentsDirectory.resolvePath(pathToImportFileFolder + '\\' + nameImportFile);
						fileStream.open(file, FileMode.WRITE);
						//"Smith;Brian;15.01.1996;23;;;;;M;;"
						if(SEX == 1) FMsex = "F";
						else FMsex = "M";
						fileStream.writeMultiByte(LASTNAME + ';' + FIRSTNAME + ';' + df.format(DOB) + ';' + CARDNUMBER + ';;;;;' + FMsex + ';;', importFileEncoding);
						fileStream.close();
					}
				}
				else
				{
					if ((pathToDBSWINexecutable == null) || (pathToDBSWINexecutable == "")||
						(pathToImportFileFolder == null) || (pathToImportFileFolder == "")||
						(nameImportFile == null) || (nameImportFile == "")) 
					{
						Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);								
					}	
					else
					{
						try
						{
							fileStream = new FileStream();
							file = File.documentsDirectory.resolvePath(pathToImportFileFolder + '\\' + nameImportFile);
							fileStream.open(file, FileMode.WRITE);
							//"Smith;Brian;15.01.1996;23;;;;;M;;"
							if(SEX == 1) FMsex = "F";
							else FMsex = "M";
							fileStream.writeMultiByte(LASTNAME + ';' + FIRSTNAME + ';' + df.format(DOB) + ';' + CARDNUMBER + ';;;;;' + FMsex + ';;', importFileEncoding);
							fileStream.close();
							
							np = new NativeProcess();
							info = new NativeProcessStartupInfo();
							info.executable = new File(pathToDBSWINexecutable + '\\DBSWIN.exe');
							
							np.start(info);
							//np.exit();
							//ViziographStartModuleExecute("open", pathToDBSWINexecutable+'\\DBSWIN.exe', '');//DBSWIN
						}
						catch (e:Error)
						{
							Alert.show(e.message, "Ошибка");
						}
					}
				}
				break;
			
			case "SOPRO":
				var pathToSOPROexecutable: String = SharedObject.getLocal("toothfairy").data["pathToSOPROexecutable"];
				if ((pathToSOPROexecutable == null) || (pathToSOPROexecutable == "")) 
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);								
				}	
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToSOPROexecutable + '\\SOPRO Imaging.exe');
						
						args = new Vector.<String>();
						//pathToSOPROexecutable + '\\SOPRO Imaging.exe'
						args.push(ID);
						args.push('"' + FIRSTNAME  + '"');
						args.push('"' + LASTNAME + '"');
						info.arguments = args;
						
						np.start(info);
						//np.exit();
						//ViziographStartModuleExecute("open", pathToDxStartRomexis+'\\DxStart.exe', ' "'+ ID + '" ' + '"' + LASTNAME + '" ' + '"' + FIRSTNAME + '" ' + '"'+df.format(DOB)+'"');//Planmeca\\Romexis\\pmbridge\\Program\\
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;	
			
			case "DIGORA":
				var clip:Clipboard = Clipboard.generalClipboard;
				clip.clear();
				var digoraString:String = '$$DFWIN$$ OPEN -n"'+ LASTNAME +', ' + FIRSTNAME +'" -c"' + ID + '" -r -a';
				clip.setData(ClipboardFormats.TEXT_FORMAT, digoraString, true);
				
				//start Digora
				if(SharedObject.getLocal("toothfairy").data["runningDIGORABoolean"] == false)
				{
					var pathToDIGORAexecutable: String = SharedObject.getLocal("toothfairy").data["pathToDIGORAexecutable"];
					if ((pathToDIGORAexecutable == null) || (pathToDIGORAexecutable == "")) 
					{
						Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);								
					}	
					else
					{
						try
						{
							np = new NativeProcess();
							info = new NativeProcessStartupInfo();
							info.executable = new File(pathToDIGORAexecutable + '\\Digora.exe');
							np.start(info);
						}
						catch (e:Error)
						{
							Alert.show(e.message, "Ошибка");
						}
					}
				}
				
				break;
			
			case "EASYDENT":
				//EASYDENT
				/*
				<LinkageParameter> 
				<Patient LastName="Smith" FirstName="John" ChartNumber="123">
				<Birthday>18/12/1972</Birthday>
				<Address>123 Main St, Salem, OR</Address>
				<ZipCode>97302</ZipCode>
				<Phone>(503)363-5432</Phone>
				<Mobile>(503)215-3215</Mobile>
				<SocialID>123456789</SocialID>
				<Gender>Male</Gender> 
				</Patient>
				</LinkageParameter>
				
				The text file is called linkage.xml, and is created in the same folder on the local computer as the EasyDent program is located.
				*/
				var vatechSex:String;
				df.formatString = "DD/MM/YYYY";
				if(SEX == 1) vatechSex = "Female";
				else vatechSex = "Male";
				var isEASYTranslitBoolean:Boolean = true;
				if(SharedObject.getLocal("toothfairy").data["isEASYTranslitCheckBox"] != null)
					isEASYTranslitBoolean = SharedObject.getLocal("toothfairy").data["isEASYTranslitBoolean"];
				
				var linkXML:XML = <LinkageParameter></LinkageParameter>;
				var _xmlString:String = '';
				if(isEASYTranslitBoolean)
				{
					_xmlString = '<Patient LastName="'+TextLib.translit2LatinWA(LASTNAME)+
										'" FirstName="'+TextLib.translit2LatinWA(FIRSTNAME)+
										'" ChartNumber="'+TextLib.translit2LatinWA(CARDNUMBER)+'">';
				}
				else
				{
					_xmlString = '<Patient LastName="'+LASTNAME+
						'" FirstName="'+FIRSTNAME+
						'" ChartNumber="'+CARDNUMBER+'">';
				}
				_xmlString += '<Birthday>' + df.format(DOB) + '</Birthday>' +
							'<Gender>' + vatechSex +'</Gender> ' +
							'</Patient>';
				
				
				var xmlList:XMLList = XMLList(_xmlString);
				linkXML.appendChild(xmlList);
				//save XML
				var fileLinkXML:File = File.documentsDirectory.resolvePath(SharedObject.getLocal("toothfairy").data["pathToEASYDENTexecutable"]+'\\linkage.xml');
				if (fileLinkXML.exists) fileLinkXML.deleteFile();
				var fileStreamXML:FileStream = new FileStream();
				fileStreamXML.open(fileLinkXML, FileMode.WRITE);
				fileStreamXML.writeMultiByte(linkXML, SharedObject.getLocal("toothfairy").data["importSoproFileEncoding"]);
				fileStreamXML.close();
				//start EasyDent
				if(SharedObject.getLocal("toothfairy").data["runningEASYDENTBoolean"] == false)
				{
					var pathToEASYDENTexecutable: String = SharedObject.getLocal("toothfairy").data["pathToEASYDENTexecutable"];
					if ((pathToEASYDENTexecutable == null) || (pathToEASYDENTexecutable == "")) 
					{
						Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);								
					}	
					else
					{
						try
						{
							np = new NativeProcess();
							info = new NativeProcessStartupInfo();
							info.executable = new File(pathToEASYDENTexecutable + '\\EasyDent4.exe');
							np.start(info);
						}
						catch (e:Error)
						{
							Alert.show(e.message, "Ошибка");
						}
					}
				}
				break;
			
			case "CYGNUS":							
				var pathToCYGNUSexecutable: String = SharedObject.getLocal("toothfairy").data["pathToCYGNUSexecutable"];
				
				if ((pathToCYGNUSexecutable == null) || (pathToCYGNUSexecutable == ""))
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);							
				}
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToCYGNUSexecutable + '\\istart.exe');
						df.formatString = SharedObject.getLocal("toothfairy").data["CYGNUSdateFormat"];
						args = new Vector.<String>();
						args.push('"' + ID + '"');
						if(SharedObject.getLocal("toothfairy").data["useCYGNUSTranslitFlag"] == true)
						{
							fname = TextLib.translit2LatinWA(FIRSTNAME);
							lname = TextLib.translit2LatinWA(LASTNAME);
						}
						else
						{
							fname = FIRSTNAME;
							lname = LASTNAME;
						}
						args.push('"' + fname + '"');
						args.push('"' + lname + '"');
						args.push('"' + df.format(DOB) + '"' );
						info.arguments = args;
						
						np.start(info);
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
			
			case "PROIMAGE":							
				var pathToPROIMAGEexecutable: String = SharedObject.getLocal("toothfairy").data["pathToPROIMAGEexecutable"];
				
				if ((pathToPROIMAGEexecutable == null) || (pathToPROIMAGEexecutable == ""))
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);							
				}
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToPROIMAGEexecutable + '\\ProImageAddPatient.exe');
						df.formatString = 'DD/MM/YYYY';
						args = new Vector.<String>();
						//args.push('000; ' + FIRSTNAME + '; '+ LASTNAME + '; '+ df.format(DOB) + ';');
						args.push('000;');
						args.push(ID + ';');
						fname = FIRSTNAME;
						lname = LASTNAME;
						args.push(fname + ';');
						args.push(lname + ';');
						args.push(df.format(DOB) + ';');
						info.arguments = args;
						//var a:int = 0;
						np.start(info);
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
			
			case "SIDEXIS":							
				var pathToSIDEXISEexecutable: String = SharedObject.getLocal("toothfairy").data["pathToSIDEXISEexecutable"];
				
				if ((pathToSIDEXISEexecutable == null) || (pathToSIDEXISEexecutable == ""))
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);							
				}
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToSIDEXISEexecutable + '\\DSNBRIDGE.exe');
						//info.executable = new File(pathToSIDEXISEexecutable + '\\sidexis.exe');
						
						df.formatString = 'DD/MM/YYYY';
						args = new Vector.<String>();
						//args.push('000; ' + FIRSTNAME + '; '+ LASTNAME + '; '+ df.format(DOB) + ';');
						//TextLib.translit2LatinWA(CARDNUMBER)+
						//DSNBridge.exe “100001AA”,“John”,“Doe”,“01/01/1980”
						args.push('"'+ CARDNUMBER +'", "'+ FIRSTNAME +'", "'+ LASTNAME +'", "'+ df.format(DOB) +'"');
						info.arguments = args;
						//var a:int = 0;
						np.start(info);
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
			
			
			case "BYZZ":
				var pathToBYZZEexecutable: String = SharedObject.getLocal("toothfairy").data["pathToBYZZEexecutable"];
				
				if ((pathToBYZZEexecutable == null) || (pathToBYZZEexecutable == ""))
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);							
				}
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathToBYZZEexecutable + '\\byzz.exe');
						args = new Vector.<String>();
						//byzz.exe /IDNumber"1" /LastName"Фамилия" /FirstName"Имя"
						args.push('/IDNumber'+ CARDNUMBER);
						args.push('/LastName'+ LASTNAME);
						args.push('/FirstName'+ FIRSTNAME);
						info.arguments = args;
						//var a:int = 0;
						np.start(info);
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
			
			
			case "MYRAY":							
				var pathNNTBridgeMyRayExecutable: String = SharedObject.getLocal("toothfairy").data["pathNNTBridgeMyRayExecutable"];
				
				if ((pathNNTBridgeMyRayExecutable == null) || (pathNNTBridgeMyRayExecutable == ""))
				{
					Alert.show(resourceManager.getString('PatientCardModule', 'VisiographBridgeNotTunedUp'), resourceManager.getString('PatientCardModule', 'Attention'), Alert.YES | Alert.CANCEL, this, ViziographStartModuleAlertCloseHandler, null, Alert.OK);							
				}
				else
				{
					try
					{
						np = new NativeProcess();
						info = new NativeProcessStartupInfo();
						info.executable = new File(pathNNTBridgeMyRayExecutable + '\\NNTBridge.exe');
						
						args = new Vector.<String>();
						
						df.formatString = 'DD,MM,YYYY';
						//args.push('"'+ CARDNUMBER +'", "'+ FIRSTNAME +'", "'+ LASTNAME +'", "'+ df.format(DOB) +'"');
						args.push("/patid");
						args.push(ID);
						args.push("/name");
						args.push(FIRSTNAME);
						args.push("/surname");
						args.push(LASTNAME);
						args.push("/dateb");
						args.push(df.format(DOB));
						info.arguments = args;
						trace(info.arguments);
						np.start(info);
						//np.exit();
						//ViziographStartModuleExecute("open", pathToExecutableProgenyImaging+'\\PIBridge.exe', " cmd=open, id="+ ID + ", first="+ FIRSTNAME + ", last="+ LASTNAME + ", dob=" + DOB);
					}
					catch (e:Error)
					{
						Alert.show(e.message, "Ошибка");
					}
				}
				break;
		}
	}
}

/*private function ViziographStartModuleExecute(operation:String, execInfo:String, parameters:String):void
{
var lc:LocalObject = new LocalObject;
//lc.showBusyCursor = true;
lc.source = "apsystem:26338E77-36A6-46FF-91CA-79E91079A81C";
lc.methods.name="Execute";
lc.addEventListener(FaultEvent.FAULT, ViziographStartModuleonFault);
lc.Execute(operation, execInfo, parameters);
}

private function ViziographStartModuleonFault(event:FaultEvent):void
{
// Handle faults here
Alert.show( "Fault: " + event.fault);
}	*/

private var _programSettingsViziofraph:ProgramSettings;
private function ViziographStartModuleAlertCloseHandler(event: CloseEvent): void 
{
	if (event.detail == Alert.YES)
	{
		if(_programSettingsViziofraph == null)
		{
			_programSettingsViziofraph = new ProgramSettings();
			_programSettingsViziofraph.addEventListener(ProgramSettings.CLOSE_PROGRAMSETTINGS_EVENT, programSettingsViziofraphCloseHandler);
			PopUpManager.addPopUp(_programSettingsViziofraph, this, true);
			PopUpManager.centerPopUp(_programSettingsViziofraph);
			_programSettingsViziofraph.settingsTabNavigator.selectedIndex = 2;	
		}
	}
}


protected function programSettingsViziofraphCloseHandler(event:Event):void
{
	PopUpManager.removePopUp(_programSettingsViziofraph);
	_programSettingsViziofraph.removeEventListener(ProgramSettings.CLOSE_PROGRAMSETTINGS_EVENT, programSettingsViziofraphCloseHandler);
	_programSettingsViziofraph = null;
}