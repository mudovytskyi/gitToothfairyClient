package modules.PatientCardModule.moduls.HistoryModule.events
{
	import flash.events.Event;
	
	import valueObjects.HistoryModuleDictionaryObject;
	
	public class DiagnosEvent extends Event
	{
		public var diag:HistoryModuleDictionaryObject;
		public var text:String;
		public function DiagnosEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			var ev:DiagnosEvent = new DiagnosEvent(type);
			ev.diag = this.diag;
			return ev;
		}
	}
}