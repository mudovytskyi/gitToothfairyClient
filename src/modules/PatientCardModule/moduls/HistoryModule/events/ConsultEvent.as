package modules.PatientCardModule.moduls.HistoryModule.events
{
	import flash.events.Event;
	
	import valueObjects.HistoryModuleDictionaryObject;
	
	public class ConsultEvent extends Event
	{
		public var cons:HistoryModuleDictionaryObject;
		public var text:String;
		public function ConsultEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			var ev:ConsultEvent = new ConsultEvent(type);
			ev.cons = this.cons;
			return ev;
		}
	}
}