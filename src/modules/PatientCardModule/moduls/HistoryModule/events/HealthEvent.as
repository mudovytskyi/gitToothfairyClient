package modules.PatientCardModule.moduls.HistoryModule.events
{
	import flash.events.Event;
	
	import valueObjects.HistoryModuleDictionaryObject;
	
	public class HealthEvent extends Event
	{
		public var hl:HistoryModuleDictionaryObject;
		public var text:String;
		public function HealthEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			var ev:HealthEvent = new HealthEvent(type);
			ev.hl = this.hl;
			return ev;
		}
	}
}