package modules.PatientCardModule.moduls.HistoryModule.events
{
	import flash.events.Event;
	
	import valueObjects.HistoryModuleDictionaryObject;
	
	public class PlanEvent extends Event
	{
		public var pl:HistoryModuleDictionaryObject;
		public var text:String;
		public function PlanEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			var ev:PlanEvent = new PlanEvent(type);
			ev.pl = this.pl;
			return ev;
		}
	}
}