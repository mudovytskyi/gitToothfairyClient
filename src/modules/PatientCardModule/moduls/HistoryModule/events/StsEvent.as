package modules.PatientCardModule.moduls.HistoryModule.events
{
	import flash.events.Event;
	
	import valueObjects.HistoryModuleDictionaryObject;
	
	public class StsEvent extends Event
	{
		public var anm:HistoryModuleDictionaryObject;
		public var text:String;
		public function StsEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			var ev:StsEvent = new StsEvent(type);
			ev.anm = this.anm;
			return ev;
		}
	}
}