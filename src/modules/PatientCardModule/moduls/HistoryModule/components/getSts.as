package modules.PatientCardModule.moduls.HistoryModule.components
{
	public class getSts
	{
		import mx.resources.IResourceManager;
		import mx.resources.ResourceManager;
		
		private var _resourceManager:IResourceManager = ResourceManager.getInstance();
		
		protected function get resourceManager():IResourceManager
		{
			return _resourceManager;
		}
		
		public function getStan(numD:int):String
		{
			if (numD == 1) return resourceManager.getString('DetailTooth','zub2b_CariesData');
			else if (numD == 2) return resourceManager.getString('DetailTooth','zub2b_CrownData');
			else if (numD == 3) return resourceManager.getString('DetailTooth','zub2b_StoppingData');
			else if (numD == 4) return resourceManager.getString('DetailTooth','zub2b_FacetData');
			else if (numD == 5) return resourceManager.getString('DetailTooth','zub2b_RestorationData');
			else if (numD == 6) return resourceManager.getString('DetailTooth','zub2b_HemisectionData');
			else if (numD == 7) return resourceManager.getString('DetailTooth','zub2b_PulpitisData');
			else if (numD == 8) return resourceManager.getString('DetailTooth','zub2b_PeriodontitisData');
			else if (numD == 9) return resourceManager.getString('DetailTooth','zub2b_Periodontitis2Data');//парадонтит?
			else if (numD == 11) return resourceManager.getString('DetailTooth','zub2b_RootData');
			else if (numD == 12) return resourceManager.getString('DetailTooth','zub2b_AbsentData');
			else if (numD == 13) return resourceManager.getString('DetailTooth','zub2b_ArtificialData');
			else if (numD == 14) return resourceManager.getString('DetailTooth','zub2b_AmputationData');
			else if (numD == 15) return resourceManager.getString('DetailTooth','zub2b_ResectionData');
			else if (numD == 16) return resourceManager.getString('DetailTooth','zub2b_PinData');
			else if (numD == 17) return resourceManager.getString('DetailTooth','zub2b_ImplantWithCrownData');
			else if (numD == 18) return resourceManager.getString('DetailTooth','zub2b_ReplantationData');
			else if (numD == 19) return resourceManager.getString('DetailTooth','zub2b_DentalCalculusData');
			else if (numD == 20) return resourceManager.getString('DetailTooth','general_paradont');
			else if (numD == 21) return resourceManager.getString('DetailTooth','paradont');//парадонтит
			else if (numD == 22) return resourceManager.getString('DetailTooth','general_gingivit');//"генералізований гігнгівіт";
			else if (numD == 23) return resourceManager.getString('DetailTooth','gingivit');//"локалізований гінгівіт";
			else if (numD == 24) return resourceManager.getString('DetailTooth','gingivit');//"локалізований гінгівіт";
			else if (numD == 25) return resourceManager.getString('DetailTooth','zub2b_ImplantWithoutCrownData');
			else if (numD == 26) return resourceManager.getString('DetailTooth','zub2b_ShortTimeData');//временная пломба
			else if (numD == 27) return resourceManager.getString('DetailTooth','zub2b_ParadontosData');//парадонтоз
			else if (numD == 28) return resourceManager.getString('DetailTooth','zub2b_HardtdefData');//дефект твердых тканей
			else return resourceManager.getString('DetailTooth','zub2b_NormData');//"проверить";//25
		}
	}
}