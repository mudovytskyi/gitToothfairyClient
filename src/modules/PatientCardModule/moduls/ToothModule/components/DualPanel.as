package modules.PatientCardModule.moduls.ToothModule.components
{
    import flash.events.MouseEvent;
    
    import modules.PatientCardModule.moduls.ToothModule.components.events.ChangeStateDualPanel;
    
    import mx.core.UIComponent;
    
    import spark.components.Button;
    import spark.components.Group;
    import spark.components.supportClasses.SkinnableComponent;

    [SkinState("front")]
    [SkinState("back")]
		[Event(name="changeStateDualPanel", type="modules.PatientCardModule.moduls.ToothModule.components.events.ChangeStateDualPanel")]
		
    public class DualPanel extends SkinnableComponent
    {

        private static const FRONT_STATE:String = "front";

        private static const BACK_STATE:String = "back";

		[Bindable]
		public var flipButtonVisible:Boolean = true;
		
        [SkinPart(required="true")]
        public var prefsButton:Button;

        [SkinPart(required="true")]
				public var backContainerHolder:Group;

        [SkinPart(required="true")]
				public var frontContainerHolder:Group;

        private var skinState:String = FRONT_STATE;

        private var _frontView:UIComponent;

        private var frontViewChange:Boolean;

        private var _backView:UIComponent;

        private var backViewChange:Boolean;

        [Bindable]
        public var transitionDuration:int = 200;


        public function get frontView():UIComponent
        {
            return _frontView;
        }

        public function set frontView(value:UIComponent):void
        {
            _frontView = value;
						_frontView.initialize();
            frontViewChange = true;
            invalidateProperties();
        }

        public function get backView():UIComponent
        {
            return _backView;
        }

        public function set backView(value:UIComponent):void
        {
            _backView = value;
						_backView.initialize();
            backViewChange = true;
            invalidateProperties();
        }

        override protected function partAdded(partName:String, instance:Object):void
        {
            super.partAdded(partName, instance);

            if (instance == prefsButton)
            {
                prefsButton.addEventListener(MouseEvent.CLICK, toggleStateHandler);
            }
            else if (instance == frontContainerHolder)
            {
                frontViewChange = true;
                invalidateProperties();
            }
            else if (instance == backContainerHolder)
            {
                backViewChange = true;
                invalidateProperties();
            }
        }

        override protected function partRemoved(partName:String, instance:Object):void
        {
            super.partRemoved(partName, instance);

            if (instance == prefsButton)
            {
                prefsButton.removeEventListener(MouseEvent.CLICK, toggleStateHandler);
            }
        }


        private function toggleStateHandler(event:MouseEvent):void
        {
						var eventObj:ChangeStateDualPanel;
            if (skinState == FRONT_STATE)
            {
								eventObj = new ChangeStateDualPanel("changeStateDualPanel",BACK_STATE,FRONT_STATE);
								dispatchEvent(eventObj);
                skinState = BACK_STATE;
            }
            else
            {
								eventObj = new ChangeStateDualPanel("changeStateDualPanel",FRONT_STATE,BACK_STATE);
								dispatchEvent(eventObj);
                skinState = FRONT_STATE;
            }

            invalidateSkinState();
        }

        override protected function getCurrentSkinState():String
        {
            return skinState;
        }

        override protected function commitProperties():void
        {
            super.commitProperties();

            if (backViewChange && backContainerHolder)
            {
                backContainerHolder.addElement(_backView);
                backViewChange = false;
            }

            if (frontViewChange && frontContainerHolder)
            {
                frontContainerHolder.addElement(_frontView);
                frontViewChange = false;
            }
        }
    }
}