package modules.PatientCardModule.moduls.ToothModule.components.events
{
	import flash.events.Event;
	
	
	public class ChangeStateDualPanel extends Event
	{
		public var newStage:String;
		public var oldStage:String;
		
		public function ChangeStateDualPanel(type:String, newStage:String, oldStage:String)
		{
			super(type);
			this.newStage = newStage;
			this.oldStage = oldStage;
		}
		
		override public function clone() : Event
		{
			return new ChangeStateDualPanel(type, newStage, oldStage);
		}
	}
}