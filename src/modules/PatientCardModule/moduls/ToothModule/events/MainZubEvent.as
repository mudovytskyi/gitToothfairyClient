package modules.PatientCardModule.moduls.ToothModule.events
{
	import flash.events.Event;
	

	
	public class MainZubEvent extends Event
	{
		public var toothtp:String;
		public var Surface:String;
		public var DiagNum:int;
		public var DiagName:String;
		
		public function MainZubEvent(type:String, toothtp:String, Surface:String, DiagNum:int, DiagName:String)
		{
			super(type);
			this.toothtp = toothtp;
			this.Surface = Surface;
			this.DiagNum = DiagNum;
			this.DiagName = DiagName;
		}
		
		override public function clone():Event
		{
			return new MainZubEvent(type, toothtp, Surface, DiagNum, DiagName)
		}
	}
}