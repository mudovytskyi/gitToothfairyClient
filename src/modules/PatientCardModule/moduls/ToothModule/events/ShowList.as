package modules.PatientCardModule.moduls.ToothModule.events
{
	import flash.events.Event;
	
	import mx.collections.XMLListCollection;
	
	public class ShowList extends Event
	{
		
		public var toothtp:String;
		public var listData:XMLListCollection;
		public var xShow:int;
		public var yShow:int;
		public var surface:String;
		
		public function ShowList(type:String, toothtp:String, listData:XMLListCollection, xShow:int, yShow:int, surface:String)
		{
			super(type);
			this.toothtp = toothtp;
			this.listData = listData;
			this.xShow = xShow;
			this.yShow = yShow;
			this.surface = surface;
		}
		
		override public function clone():Event
		{
			return new ShowList(type, toothtp, listData, xShow, yShow, surface)
		}
	}
}