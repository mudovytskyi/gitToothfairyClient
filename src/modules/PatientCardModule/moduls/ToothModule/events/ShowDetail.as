package modules.PatientCardModule.moduls.ToothModule.events
{
	import flash.events.Event;
	
	public class ShowDetail extends Event
	{
		public var toothType:String; //тип зуба
		
		public var toothNum:int; //номер зуба
		
		public var rbDiag:String; //дистанальная
		public var lbDiag:String; //медиальная
		public var bbDiag:String; //вестибулярная
		public var fbDiag:String; //оральная
		public var ccDiag:String; //окклюзионная
		//зуб 2-4
		public var rLDiag:String; //левый корень
		public var rRDiag:String; //правый корень
		public var rCDiag:String; //центральный корень
		public var rDiag:String; //корень
		
		public var desnaDiag:String; //десна
		
		public var zubDiag:String; //диагноз на весь зуб
		
		public var xShow:int;
		public var yShow:int;
		
		public function ShowDetail(type:String, toothNum:int, toothType:String,
									 rbDiag:String, lbDiag:String, bbDiag:String, fbDiag:String, ccDiag:String, 
									 rLDiag:String, rRDiag:String, rCDiag:String, rDiag:String,
									 desnaDiag:String, zubDiag:String, xShow:int, yShow:int)
		{
			super(type);
			this.toothNum = toothNum;
			this.toothType = toothType;
			this.rbDiag = rbDiag;
			this.lbDiag = lbDiag;
			this.bbDiag = bbDiag;
			this.fbDiag = fbDiag;
			this.ccDiag = ccDiag;
			this.rLDiag = rLDiag;
			this.rRDiag = rRDiag;
			this.rCDiag = rCDiag;
			this.rDiag = rDiag;
			this.zubDiag = zubDiag;
			this.desnaDiag = desnaDiag;
			this.xShow = xShow;
			this.yShow = yShow;
		}
		
		override public function clone():Event
		{
			return new ShowDetail(type, toothNum, toothType,
				rbDiag, lbDiag, bbDiag, fbDiag, ccDiag, 
				rLDiag, rRDiag, rCDiag, rDiag,
				desnaDiag, zubDiag, xShow, yShow)
		}
		
	}
}