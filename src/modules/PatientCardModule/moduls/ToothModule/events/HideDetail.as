package modules.PatientCardModule.moduls.ToothModule.events
{
	import flash.events.Event;
	
	public class HideDetail extends Event
	{
		public function HideDetail(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new HideDetail(type, bubbles, cancelable);
		}
	}
}