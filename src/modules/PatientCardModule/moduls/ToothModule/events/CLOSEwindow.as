package modules.PatientCardModule.moduls.ToothModule.events
{
	import flash.events.Event;
	
	public class CLOSEwindow extends Event
	{
		public function CLOSEwindow(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new CLOSEwindow(type, bubbles, cancelable);
		}
	}
}