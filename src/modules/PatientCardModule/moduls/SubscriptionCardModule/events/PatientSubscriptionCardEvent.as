package modules.PatientCardModule.moduls.SubscriptionCardModule.events
{
	import flash.events.Event;
	
	import mx.utils.StringUtil;
	
	
	public class PatientSubscriptionCardEvent extends Event
	{
		public static const DELETE_SUBSCRIPTION_CARD:String = "DELETE_SUBSCRIPTION_CARD";
		public static const DELETE_SUBSCRIPTION_PROCEDURE:String = "DELETE_SUBSCRIPTION_PROCEDURE";
		
		private var _invokeItem:*;
		public function get invokeItem():* { return _invokeItem; }
		
		public function PatientSubscriptionCardEvent(type:String, invItem:*, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_invokeItem = invItem;
		}
		
		public override function clone():Event
		{
			return new PatientSubscriptionCardEvent(type, invokeItem, bubbles, cancelable);
		}
		
		public override function toString():String
		{
			return StringUtil.substitute("modules.PatientCardModule.moduls.SubscriptionCardModule.events.PatientSubscriptionCardEvent::{0}", this.type);
		}
	}
}