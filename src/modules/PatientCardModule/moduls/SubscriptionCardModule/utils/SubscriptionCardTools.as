package modules.PatientCardModule.moduls.SubscriptionCardModule.utils
{
	import components.date.ToolsForFirebird;
	
	import valueObjects.SubscriptionCardObject;
	import valueObjects.SubscriptionDictionaryObject;

	public class SubscriptionCardTools
	{
		public static function getNewSubscriptionCard(subs:SubscriptionDictionaryObject):SubscriptionCardObject
		{
			var card:SubscriptionCardObject = new SubscriptionCardObject();
			card.subscription = subs;
			card.VISITS_LEFT = subs.MAX_VISITS_PER_MONTH;
			var dtCreated:Date = new Date();
			var sCreated:String = ToolsForFirebird.dateToFirebirdDateString(dtCreated);
			card.CREATED_DATE = sCreated;
			updateCardsTermOfUse(card, dtCreated);
			return card;
		}
		
		public static function updateCardsTermOfUse(card:SubscriptionCardObject, dtActivated:Date):void
		{
			if (!card) return;
			var dtTo:Date = new Date(dtActivated.time);
			dtTo.month += parseInt(card.subscription.type.N_MONTHS);
			var sFrom:String = ToolsForFirebird.dateToFirebirdDateString(dtActivated);
			var sTo:String = ToolsForFirebird.dateToFirebirdDateString(dtTo);
			card.DATE_FROM = sFrom;
			card.DATE_TO = sTo;
		}
	}
}