package modules.PatientCardModule.components.events
{
	import flash.events.Event;
	
	import valueObjects.PatientCardModuleAddPatientObject;
	
	public class AddPatientCloseEvent extends Event
	{
		public static var CLOSE_EVENT:String = 'AddPatient_closeEvent';
		
		public var patient:PatientCardModuleAddPatientObject;
		public function AddPatientCloseEvent(type:String, patient:PatientCardModuleAddPatientObject=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.patient = patient;
		}
		
		public override function clone():Event
		{
			return new AddPatientCloseEvent(type, patient, bubbles, cancelable);
		}
	}
}