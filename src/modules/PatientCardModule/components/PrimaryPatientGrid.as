﻿package modules.PatientCardModule.components {
	
import flash.display.Sprite;
import flash.net.SharedObject;

import mx.controls.dataGridClasses.DataGridColumn;
import mx.logging.ILogger;
import mx.resources.ResourceManager;

import components.date.ToothFairyDate;

import de.sbistram.controls.MatchDataGrid;
import de.sbistram.utils.LogUtils;

import toothfairy.settings.ClientSettings;

[ResourceBundle("PatientCardModule")]
	
public class PrimaryPatientGrid extends MatchDataGrid {
		
	private var _log:ILogger = LogUtils.getLogger(this);
	
	//
	// private constants
	//
	
	private static const DF_FNAME:String = "PFname";
	private static const DF_LNAME:String = "PLname";
	private static const DF_MNAME:String = "PMname";
	private static const DF_PHONE:String	= "PPphone";
	private static const DF_DOCTOR:String	= "DoctorName";
	private static const DF_CLAENDARDATE:String	= "PCDate";
	
	private static const DF_ALL:Array = [DF_LNAME, DF_FNAME, DF_MNAME, DF_PHONE, DF_CLAENDARDATE, DF_DOCTOR];
	
	/**
	 * Constructor
	 */
	public function PrimaryPatientGrid() {
		super();
		ResourceManager.getInstance().localeChain = [ClientSettings.locale];
		columns = createColumns();
		decoratorFunction = decorator;
	}
	
	public var rowColorFunction:Function;
	override protected function drawRowBackground(s:Sprite, rowIndex:int, y:Number, height:Number, color:uint, dataIndex:int):void
	{
		if(rowColorFunction != null && dataProvider != null) 
		{
			var item:Object;
			if(dataIndex < dataProvider.length)
			{
				item = dataProvider[dataIndex];
			}
			
			if(item)
			{
				color = rowColorFunction(item, rowIndex, dataIndex, color);
			}
		}
		super.drawRowBackground(s, rowIndex, y, height, color, dataIndex);
	}
	
	/**
	 * Create data for a combobox selection of one or all columns
	 * Add the 'All' entry for selecting all columns
	 * 
	 * @return Array of all columns headerText and dataField 
	 */	
	public function columnSelectProvider():Array {
		var provider:Array = [{ label:ResourceManager.getInstance().getString('PatientCardModule', 'PatientCardModule_PDG_All'), dataField:null }];
		for (var i:int = 0; i < columns.length; i++) {
			provider.push({ label:columns[i].headerText, dataField:columns[i].dataField });
		}
		return provider;
	}
	
	/** 
	 * @see DataGridColumn#itemToLabel
	 * @return Array of DataGridColumn
	 */
	private function createColumns():Array {
		var columns:Array = [];
		var column:DataGridColumn;
		var df:String;
		
		// create all columns
		for (var i:int = 0; i < DF_ALL.length; i++) {
			column = new DataGridColumn();
			df = DF_ALL[i];
			
			// default values
			column.dataField = df;
			column.itemRenderer = _matchFactory;
			columns.push(column);
			column.setStyle("textAlign", "center");
			
			// column specific values
			if (df == DF_CLAENDARDATE) {
				column.labelFunction=dateFieldLabelFunction;
				column.headerText = ResourceManager.getInstance().getString('PatientCardModule', 'CalendarDateDataGridColumn');
			} else if (df == DF_FNAME) {
				column.setStyle("textAlign", "center");
				column.headerText = ResourceManager.getInstance().getString('PatientCardModule', 'FirstNameDataGridColumn');
			} else if (df == DF_LNAME) {
				column.setStyle("textAlign", "center");
				column.headerText = ResourceManager.getInstance().getString('PatientCardModule', 'LastNameDataGridColumn');
			} else if (df == DF_MNAME) {
				column.setStyle("textAlign", "center");
				column.headerText = ResourceManager.getInstance().getString('PatientCardModule', 'MiddleNameDataGridColumn');
			} else if (df == DF_DOCTOR){
				column.labelFunction=doctorFieldLabelFunction;
				column.setStyle("textAlign", "center");
				column.headerText = ResourceManager.getInstance().getString('PatientCardModule', 'DoctorPDGColumn')
			} else if (df == DF_PHONE) {
				column.setStyle("textAlign", "center");
				column.headerText = ResourceManager.getInstance().getString('PatientCardModule', 'MobileNumberDataGridColumn');
			}
		}
		return columns;
	}
	private function dateFieldLabelFunction(item:Object, column:DataGridColumn):String
	{
		if ((item.PCDate==null)||(item.PCDate==""))
			return "-";
		else return ToothFairyDate.stringFirebirdDateToToothFairyStringDate(item.PCDate);
	}
	private function doctorFieldLabelFunction(item:Object, column:DataGridColumn):String
	{
		if ((item.DoctorName==null)||(item.DoctorName==""))
			return "-";
		else return item.DoctorName;
	}
		
	/**
	 * Decorator method to do some DataGrid decorator stuff:
	 * 
	 *  	- font: color, size, bold, italic, underline.
	 * 
	 * Note: Because the renderer is reused we have to set/reset the values!
	 * 		 And don't use the column index if dragging columns is enabled! Instead 
	 *       use the column like it's done here.
	 * 
	 * @param	r renderer for the current cell
	 * @param	w width of the current cell
	 * @param	h heigth of the current cell
	 */
	private function decorator(r:Object, w:Number, h:Number):void {
		// Note: r.data is the original value object for the hole row
		var column:DataGridColumn = columns[r.listData.columnIndex];
		var df:String = column.dataField;
		r.setStyle("fontWeight", ((df == DF_FNAME)||(df == DF_LNAME)||(df == DF_DOCTOR)) ? "bold" : "normal");
		if (df == DF_DOCTOR) r.setStyle("fontSize", "14");
	}
	
}
}