package modules.PatientCardModule.components
{
	import mx.core.mx_internal;
	
	import spark.components.FormItem;
	import spark.components.Group;
	
	use namespace mx_internal;
	
	public class ParentsFormItem extends FormItem
	{
		/**
		 *  A reference to the Group that contains the FormItem's sequenceContentGroup.
		 */
		[SkinPart(required="false")]
		public var sequenceContentGroup:Group;
		
		//----------------------------------
		//  helpContent
		//----------------------------------
		
		private var _sequenceContent:Array;
		private var sequenceContentChanged:Boolean = false;
		
		[ArrayElementType("mx.core.IVisualElement")]
		[Bindable("sequenceContentChanged")]
		[Inspectable(category="General", arrayType="mx.core.IVisualElement", defaultValue="")]
		
		/** 
		 *  The set of components to include in the sequence content 
		 *  area of the FormItem.
		 * 
		 *  @default null
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 10
		 *  @playerversion AIR 2.5
		 *  @productversion Flex 4.5
		 */
		public function get sequenceContent():Array
		{
			if (sequenceContentGroup)
				return sequenceContentGroup.getMXMLContent();
			else
				return _sequenceContent;
		}
		
		/**
		 *  @private
		 */
		public function set sequenceContent(value:Array):void
		{
			_sequenceContent = value;
			sequenceContentChanged = true;
			invalidateProperties();
		}
		
		
		public function ParentsFormItem()
		{
			super();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Overridden Methods
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  @private
		 */
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if (sequenceContentChanged)
			{
				createSequenceContent();
				sequenceContentChanged = false;
			}
		}
		
		/**
		 *  @private
		 */
		override protected function partAdded(partName:String, instance:Object) : void
		{
			super.partAdded(partName, instance);
			
			if (instance == sequenceContentGroup)
			{
				sequenceContentChanged= true;
				createSequenceContent();
			}
		}
		
		/**
		 *  @private
		 */
		override protected function partRemoved(partName:String, instance:Object) : void
		{
			super.partRemoved(partName, instance);
			
			// Remove the sequenceContent from the sequenceContentGroup so that it can be parented
			// by a different skin's sequenceContentGroup
			if (instance == sequenceContentGroup)
				sequenceContentGroup.removeAllElements();
		}
		
		
		//--------------------------------------------------------------------------
		//
		//  Methods
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  @private
		 */
		private function createSequenceContent():void
		{
			if (sequenceContentGroup)
				sequenceContentGroup.mxmlContent = _sequenceContent; 
		}
	}
}