package modules.PatientCardModule.myPackage
{
	import com.adobe.fiber.runtime.lib.MathFunc;
	import com.adobe.fiber.runtime.lib.StringFunc;
	
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;
	
	import org.alivepdf.colors.IColor;
	import org.alivepdf.colors.RGBColor;
	import org.alivepdf.data.Grid;
	import org.alivepdf.data.GridColumn;
	import org.alivepdf.drawing.Blend;
	import org.alivepdf.fonts.CodePage;
	import org.alivepdf.images.ColorSpace;
	import org.alivepdf.layout.Align;
	import org.alivepdf.layout.Orientation;
	import org.alivepdf.layout.Size;
	import org.alivepdf.layout.Unit;
	
	import valueObjects.PatientCardPDFModuleAmbcard;
	import valueObjects.PatientCardPDFModuleExam;
	import valueObjects.PatientCardPDFModuleHygcontrol;
	import valueObjects.PatientCardPDFModuleIllHistoryRecord;
	
	public class PrintPatientCardToPdf
	{
		[Embed(source="fonts/timcyr.ttf", mimeType="application/octet-stream")]   
		private var fontStreamTimesNewRomanCyrilic: Class;
		
		[Embed(source="fonts/timcyr.afm", mimeType="application/octet-stream")]   
		private var afmStreamTimesNewRomanCyrilic: Class;
		
		[Embed(source="fonts/timesbd.ttf", mimeType="application/octet-stream")]   
		private var fontStreamTimesNewRomanCyrilicBold: Class;
		
		[Embed(source="fonts/timesbd.afm", mimeType="application/octet-stream")]   
		private var afmStreamTimesNewRomanCyrilicBold: Class;
		
		[Embed(source="fonts/timesi.ttf", mimeType="application/octet-stream")]   
		private var fontStreamTimesNewRomanCyrilicItalic: Class;
		
		[Embed(source="fonts/timesi.afm", mimeType="application/octet-stream")]   
		private var afmStreamTimesNewRomanCyrilicItalic: Class;
		
		[Embed(source="modules/PatientCardModule/icons/ListItem.jpg", mimeType="application/octet-stream")]   
		private var ListItem: Class;		
		
		[Embed(source="modules/PatientCardModule/icons/1.png", mimeType="application/octet-stream")]
		private var tooth1Icon:Class;
		
		[Embed(source="modules/PatientCardModule/icons/1b.png", mimeType="application/octet-stream")]
		private var tooth1bIcon:Class;
		
		[Embed(source="modules/PatientCardModule/icons/2.png", mimeType="application/octet-stream")]
		private var tooth2Icon:Class;
		
		[Embed(source="modules/PatientCardModule/icons/2b.png", mimeType="application/octet-stream")]
		private var tooth2bIcon:Class;
		
		[Embed(source="modules/PatientCardModule/icons/3.png", mimeType="application/octet-stream")]
		private var tooth3Icon:Class;
		
		[Embed(source="modules/PatientCardModule/icons/3b.png", mimeType="application/octet-stream")]
		private var tooth3bIcon:Class;
		
		[Embed(source="modules/PatientCardModule/icons/4.png", mimeType="application/octet-stream")]
		private var tooth4Icon:Class;
		
		[Embed(source="modules/PatientCardModule/icons/4b.png", mimeType="application/octet-stream")]
		private var tooth4bIcon:Class;
				
		private var ambulatoryCardInfo:PatientCardPDFModuleAmbcard = new PatientCardPDFModuleAmbcard();
		private var hygieneInfo:PatientCardPDFModuleHygcontrol = new PatientCardPDFModuleHygcontrol;	
		private var illHistory:PatientCardPDFModuleIllHistoryRecord = new PatientCardPDFModuleIllHistoryRecord();	
		
		public function getExamForTooth(toothNumber: int, examString: String): String
		{	
			var strData: String = "<view>"+examString+"</view>";			
			var xmlData:XML = new XML(strData);
			
			var resultArray: ArrayCollection = new ArrayCollection();
			var resultString: String = "";		
			
			for each (var item: XML in xmlData.zub) 
			{				
				if (item.@id != toothNumber.toString()) continue;											
				
				for each (var subItem: XML in item.children())
				{
					var isInArray: Boolean = false;
					
					for (var i: int = 0; i < resultArray.length; i++)
					{
						if (resultArray[i] == subItem[0].toString())
						{
							isInArray = true;
							break;
						}
					}
					
					if (!isInArray) resultArray.addItem(subItem[0].toString());										
				}
				
				break;						
			}	
			
			if (xmlData.hasOwnProperty("GP1"))
			{
				var value: String = xmlData.child('GP1').valueOf();		
				
				if (((["20", "22"].indexOf(value) != -1) && ([11, 12, 13, 14, 15, 16, 17, 18, 21, 22, 23, 24, 25, 26, 27, 28].indexOf(toothNumber) != -1)) ||
					((["21", "23"].indexOf(value) != -1) && ([11, 12, 13, 14, 21, 22, 23, 24].indexOf(toothNumber) != -1)))
				{
					isInArray = false;
					
					for (i = 0; i < resultArray.length; i++)
					{
						if (resultArray[i] == value)
						{
							isInArray = true;
							break;
						}
					}
					
					if (!isInArray) resultArray.addItem(value);	
				}								
			}			
			
			if (xmlData.hasOwnProperty("GP2"))
			{
				value = xmlData.child('GP2').valueOf();		
				
				if (((["20", "22"].indexOf(value) != -1) && ([41, 42, 43, 44, 45, 46, 47, 48, 31, 32, 33, 34, 35, 36, 37, 38].indexOf(toothNumber) != -1)) ||
					((["21", "23"].indexOf(value) != -1) && ([41, 42, 43, 44, 31, 32, 33, 34].indexOf(toothNumber) != -1)))
				{
					isInArray = false;
					
					for (i = 0; i < resultArray.length; i++)
					{
						if (resultArray[i] == value)
						{
							isInArray = true;
							break;
						}
					}
					
					if (!isInArray) resultArray.addItem(value);	
				}								
			}	
			
			for (i = 0; i < resultArray.length; i++)
			{
				resultString += getDiagnos(resultArray[i]) + ", ";
			}
			
			resultString = StringFunc.removeChars(resultString, resultString.length-2, 2);
			
			return resultString;
		}
		
		public function getDiagnos(numD:int):String
		{
			switch (numD)
			{
				case 1: return "C"; break;
				case 2: return "Cd"; break;
				case 3: return "Pl"; break;
				case 4: return "F"; break;
				case 5: return "r"; break;
				case 6: return "H"; break;
				case 7: return "P"; break;
				case 8: return "Pt"; break;
				case 9: return "Lp"; break;
				case 11: return "R"; break;
				case 12: return "A"; break;
				case 13: return "ar"; break;
				case 14: return "Am"; break;
				case 15: return "rez"; break;
				case 16: return "pin"; break;
				case 17: return "I"; break;				
				case 18: return "Rp"; break;
				case 19: return "Cd"; break;
				case 20: return "Gp"; break;
				case 21: return "Fp"; break;
				case 22: return "Gg"; break;
				case 23: return "Fg"; break;	
				case 24: return "Gv"; break;
				
				case 25: return "I"; break;//имплант без коронки
				case 26: return "tPl"; break;//временная пломба
				case 27: return "Lps"; break;//пардонтоз
				case 28: return "htd"; break;//дефект твердых тканей
				
				
				
				default: return ""; break;	
			}				
		}
		
		public function centerLabel(label: String, len: int): String
		{
			var halfOfStringLength: Number = len / 2;
			var halfOfLabelLength: Number = label.length / 2;
			var odds: Number = halfOfStringLength - halfOfLabelLength;
			var frontSpacesCount: int = MathFunc.floor(odds);
			label = StringUtil.repeat(' ', frontSpacesCount) + label;
			return label;
			
		}
		
		
		public function printToPdf(event:ResultEvent):PDF
		{
			var pdfFile: PDF = new PDF(Orientation.LANDSCAPE, Unit.MM, Size.A5);	
			
			var fontStreamTimesNewRomanCyrilic: ByteArray = new fontStreamTimesNewRomanCyrilic();
			var afmStreamTimesNewRomanCyrilic: ByteArray = new afmStreamTimesNewRomanCyrilic();
			
			var fontStreamTimesNewRomanCyrilicBold: ByteArray = new fontStreamTimesNewRomanCyrilicBold();
			var afmStreamTimesNewRomanCyrilicBold: ByteArray = new afmStreamTimesNewRomanCyrilicBold();
			
			var fontStreamTimesNewRomanCyrilicItalic: ByteArray = new fontStreamTimesNewRomanCyrilicItalic();
			var afmStreamTimesNewRomanCyrilicItalic: ByteArray = new afmStreamTimesNewRomanCyrilicItalic();
			
			var timesNewRomanCyrilicFont: EmbeddedFont = new EmbeddedFont(fontStreamTimesNewRomanCyrilic, afmStreamTimesNewRomanCyrilic, CodePage.CP1251);				
			var timesNewRomanCyrilicBoldFont: EmbeddedFont = new EmbeddedFont(fontStreamTimesNewRomanCyrilicBold, afmStreamTimesNewRomanCyrilicBold, CodePage.CP1251);
			var timesNewRomanCyrilicItalicFont: EmbeddedFont = new EmbeddedFont(fontStreamTimesNewRomanCyrilicItalic, afmStreamTimesNewRomanCyrilicItalic, CodePage.CP1251);
			
			//var myCoreFont: IFont = new CoreFont();	
			
			pdfFile.setMargins(10, 10, 10, 10);			
			
			// -----------------------------------------------------------------------------------------------
			// титульна сторінка
			pdfFile.addPage();		
			
			var firstColumnText: String = "Міністерство охорони здоров'я України\nСтоматологічна практика\n";
			if (event.result.CLINICADDRESS != null) firstColumnText += event.result.CLINICADDRESS;
			//ВСТАВИТЬ НАЗВАНИЕ КЛИНИКИ ЮРЕДИЧЕКОЕ И ОБЫЧНОЕ
			var thirdColumnText: String = "Медична документація\nФорма №043/0\nЗатверджено наказом\nМОЗ України\n27.12.1999 р. №302";
			
			var borderColor: IColor = new RGBColor(0x000000);
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 10);
			pdfFile.textStyle(new RGBColor(0x000000));		
			
			pdfFile.lineStyle(borderColor, 0.1);							
			
			pdfFile.setXY(10, 10);
			pdfFile.addMultiCell(90, 6.65, firstColumnText, 1, "C", 0);
			
			pdfFile.setXY(100, 10);
			pdfFile.addMultiCell(50, 20, "", 1, "C", 0);
			
			pdfFile.setXY(150, 10);
			pdfFile.addMultiCell(45, 4, thirdColumnText, 1, "C", 0);	
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 14);
			
			pdfFile.addText("МЕДИЧНА КАРТА СТОМАТОЛОГІЧНОГО ХВОРОГО № "+event.result.PATIENTCARDNUMBER, 25, 40);
			
			pdfFile.setFontSize(12);
			pdfFile.addText("П.І.Б. ", 15, 50);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 12);
			pdfFile.addText(event.result.PATIENTLASTNAME+" "+event.result.PATIENTFIRSTNAME+" "+event.result.PATIENTMIDDLENAME, 35, 50);	
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Стать: ", 15, 58);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 12);
			
			if (event.result.PATIENTSEX=="0") pdfFile.addText("ЧОЛ.", 35, 58)
			else pdfFile.addText("ЖІН.", 35, 58);
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Дата народження:", 50, 58);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 12);
			pdfFile.addText("число:  "+StringFunc.right(event.result.PATIENTBIRTHDAY, 2), 90, 58);
			pdfFile.addText("місяць:  "+StringFunc.substring(event.result.PATIENTBIRTHDAY, 5, 2), 115, 58);
			pdfFile.addText("рік:  "+StringFunc.left(event.result.PATIENTBIRTHDAY, 4), 140, 58);
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Індекс:", 15, 66);
			
			if (event.result.PATIENTPOSTALCODE!=null)
			{
				pdfFile.setFont(timesNewRomanCyrilicFont, 12);
				pdfFile.addText(event.result.PATIENTPOSTALCODE, 35, 66);					
			}					
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Адреса:", 50, 66);
			
			var address:String = "";
			var addressFlag:Boolean = false;
			if (event.result.PATIENTCOUNTRY!=null)
			{address += event.result.PATIENTCOUNTRY+" ";addressFlag=true;}
			if (event.result.PATIENTSTATE!=null)
			{address += event.result.PATIENTSTATE+" ";addressFlag=true;}
			if (event.result.PATIENTCITY!=null)
			{address += event.result.PATIENTCITY+" ";addressFlag=true;}
			if (event.result.PATIENTRAGION!=null)
			{address += event.result.PATIENTRAGION+" ";addressFlag=true;}
			if (event.result.PATIENTADDRESS!=null)
			{address += event.result.PATIENTADDRESS+" ";addressFlag=true;}
			if(addressFlag)
			{				
				pdfFile.setFont(timesNewRomanCyrilicFont, 12);
				pdfFile.addText(address, 70, 66);
				address = null;
			}
			
			
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Телефон:", 15, 82);
			
			if (event.result.PATIENTTELEPHONENUMBER!=null)
			{
				pdfFile.setFont(timesNewRomanCyrilicFont, 12);
				pdfFile.addText(event.result.PATIENTTELEPHONENUMBER, 40, 82);
			}
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Моб.:", 65, 82);
			
			if (event.result.PATIENTMOBILENUMBER!=null)
			{
				pdfFile.setFont(timesNewRomanCyrilicFont, 12);
				pdfFile.addText(event.result.PATIENTMOBILENUMBER, 80, 82);
			}
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Місце роботи:", 15, 90);
			
			if (event.result.PATIENTEMPLOYMENTPLACE!=null)
			{				
				pdfFile.setFont(timesNewRomanCyrilicFont, 12);
				pdfFile.addText(event.result.PATIENTEMPLOYMENTPLACE, 45, 90);
			}
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Посада:", 115, 90);
			
			if (event.result.PATIENTJOB!=null)
			{				
				pdfFile.setFont(timesNewRomanCyrilicFont, 12);
				pdfFile.addText(event.result.PATIENTJOB, 135, 90);
			}
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Телефон:", 15, 98);
			
			if (event.result.PATIENTWORKPHONE!=null)
			{
				pdfFile.setFont(timesNewRomanCyrilicFont, 12);
				pdfFile.addText(event.result.PATIENTWORKPHONE, 40, 98);
			}
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Скарги:", 15, 115);				
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("Перенесені супутні захворювання:", 15, 140);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 10);
			pdfFile.addText("ДИВИСЬ ДАЛІ", 160, 140);		
			
			// -----------------------------------------------------------------------------------------------
			// тест (1 сторінка) 									 			
			pdfFile.addPage();
			
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
			pdfFile.addText("ШАНОВНИЙ ПАЦІЄНТ!", 25, 10);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 10);
			pdfFile.addText("Ми піклуємося про Вас, попіклуйтесь про себе і Ви!", 15, 14);
			
			pdfFile.setFont(timesNewRomanCyrilicItalicFont, 10);
			pdfFile.addText("Підкресліть потрібний варіант відповіді", 20, 20);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 10);
			pdfFile.addText("1.", 10, 28);
			pdfFile.addText("Чи страждаєте Ви захворюваннями:", 15, 28);
			
			pdfFile.addImageStream(new ListItem() as ByteArray, ColorSpace.DEVICE_RGB,  null, 10, 20, 2, 2);
			pdfFile.addText("серця", 25, 32);
			
			switch (event.result.PATIENTTESTCARDIACDISEASE)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 32);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 32);
					break;
			}					
			
			pdfFile.addImageStream(new ListItem() as ByteArray, ColorSpace.DEVICE_RGB,  null, 10, 24, 2, 2);
			pdfFile.addText("нирок", 25, 36);
			
			switch (event.result.PATIENTTESTKIDNEYDISEASE)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 36);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 36);
					break;
			}					
			
			pdfFile.addImageStream(new ListItem() as ByteArray, ColorSpace.DEVICE_RGB,  null, 10, 28, 2, 2);
			pdfFile.addText("печені", 25, 40);
			
			switch (event.result.PATIENTTESTLIVERDISEASE)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 40);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 40);
					break;
			}	
			
			pdfFile.addImageStream(new ListItem() as ByteArray, ColorSpace.DEVICE_RGB,  null, 10, 32, 2, 2);
			pdfFile.addText("інше", 25, 44);
			
			switch (event.result.PATIENTTESTOTHERDISEASE)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 44);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 44);
					break;
			}						
			
			pdfFile.addText("2.", 10, 49);
			pdfFile.addText("Чи страждаєте Ви підвищеним (пониженим)", 15, 49);
			pdfFile.addText("артеріальним тиском?", 15, 53);
			
			switch (event.result.PATIENTTESTARTERIALPRESSURE)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 53);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 53);
					break;
			}					
			
			pdfFile.addText("3.", 10, 58);
			pdfFile.addText("Чи страждаєте Ви ревматичними", 15, 58);
			pdfFile.addText("захворюваннями?", 15, 62);
			
			switch (event.result.PATIENTTESTRHEUMATISM)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 62);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 62);
					break;
			}	
			
			pdfFile.addText("4.", 10, 67);
			pdfFile.addText("Перенесений гепатит", 15, 67);		
			
			switch (event.result.PATIENTTESTHEPATITIS)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 67);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 67);
					break;
			}	
			
			pdfFile.addText("5.", 10, 72);
			pdfFile.addText("Діабет", 15, 72);					
			
			switch (event.result.PATIENTTESTDIABETES)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 72);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 72);
					break;
			}	
			
			pdfFile.addText("6.", 10, 77);
			pdfFile.addText("Припадки, непритомності, запаморочення", 15, 77);		
			
			switch (event.result.PATIENTTESTFIT)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 77);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 77);
					break;
			}	
			
			pdfFile.addText("7.", 10, 82);
			pdfFile.addText("Тривалі кровотечі після порізів", 15, 82);				
			
			switch (event.result.PATIENTTESTBLEEDING)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 82);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 82);
					break;
			}	
			
			pdfFile.addText("8.", 10, 87);
			pdfFile.addText("Алергія (лікарська, харчова), чим", 15, 87);
			pdfFile.addText("усувається приступ?", 15, 91);
			
			if (event.result.PATIENTTESTALLERGY!=null)		
			{
				pdfFile.setXY(14, 93);
				
				var cellText: String = event.result.PATIENTTESTALLERGY.toString();						
				
				var pos: int = StringFunc.find("\r\n", cellText);
				
				if (pos>-1)
				{
					var resultText: String = StringFunc.left(cellText, pos+2); 
					
					if (resultText.length>45) 
					{
						resultText = StringFunc.left(StringFunc.left(resultText, resultText.length-2), 45)+"\r\n";
					}
					
					cellText = StringFunc.right(cellText, cellText.length-pos-2);							
					pos = StringFunc.find('\r\n', cellText);
					
					if (pos>-1) resultText+=StringFunc.left(cellText, Math.min(45, pos+2))
					else resultText+=StringFunc.left(cellText, 45);	
				}
				else resultText = StringFunc.left(cellText, 90);						
				
				pdfFile.addMultiCell(85, 4, resultText, 0, "J");							
			}
			
			pdfFile.addText("9.", 10, 105);
			pdfFile.addText("Вагітність", 15, 105);
			
			switch (event.result.PATIENTTESTPREGNANCY)
			{
				case "0":					
					pdfFile.addText("НІ", 90, 105);
					break;
				case "1":
					pdfFile.addText("ТАК", 90, 105);
					break;
			}	
			
			pdfFile.addText("10.", 10, 110);
			pdfFile.addText("Ліки, які приймаються (вказати)", 15, 110);
			
			if (event.result.PATIENTTESTMEDICATIONS!=null)		
			{
				pdfFile.setXY(14, 112);
				
				cellText = event.result.PATIENTTESTMEDICATIONS.toString();						
				
				pos = StringFunc.find("\r\n", cellText);
				
				if (pos>-1)
				{
					resultText = StringFunc.left(cellText, Math.min(45, pos))+"\r\n";							
					cellText = StringFunc.right(cellText, cellText.length-pos-2);							
					pos = StringFunc.find('\r\n', cellText);
					
					if (pos>-1)
					{
						resultText += StringFunc.left(cellText, Math.min(45, pos))+"\r\n"; 
						cellText = StringFunc.right(cellText, cellText.length-pos-2);
						pos = StringFunc.find('\r\n', cellText);
						
						if (pos>-1)
						{
							resultText += StringFunc.left(cellText, Math.min(45, pos))+"\r\n"; 
							cellText = StringFunc.right(cellText, cellText.length-pos-2);
							pos = StringFunc.find('\r\n', cellText);
							
							if (pos>-1)
							{
								resultText += StringFunc.left(cellText, Math.min(45, pos))+"\r\n"; 
								cellText = StringFunc.right(cellText, cellText.length-pos-2);
								pos = StringFunc.find('\r\n', cellText);
								
								if (pos>-1) resultText+=StringFunc.left(cellText, Math.min(45, pos+2))
								else resultText+=StringFunc.left(cellText, 45);	
							}
							else resultText += StringFunc.left(cellText, 45);									
						}
						else resultText += StringFunc.left(cellText, 90);								
					}		
					else resultText += StringFunc.left(cellText, 135);	
				}
				else resultText = StringFunc.left(cellText, 180);						
				
				pdfFile.addMultiCell(85, 4, resultText, 0, "J");							
			}
			
			pdfFile.addText("11.", 10, 135);
			pdfFile.addText("Група крові", 15, 135);
			
			if (event.result.PATIENTTESTBLOODGROUP != null) pdfFile.addText(event.result.PATIENTTESTBLOODGROUP, 45, 135);
			
			pdfFile.addText("Резус фактор", 15, 139);
			
			switch (event.result.PATIENTTESTRHESUSFACTOR)
			{
				case "1":
					pdfFile.addText("+", 45, 139);
					break;
				
				case "2":
					pdfFile.addText("-", 45, 139);
					break;
			}
			
			pdfFile.setFont(timesNewRomanCyrilicItalicFont, 10);
			pdfFile.addText("Конфіденційність відповідей на наступні питання!", 115, 10);			
			pdfFile.addText("гарантуємо", 115, 14);			
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 10);
			pdfFile.addText("12.", 115, 28);
			pdfFile.addText("Чи спостерігаються у Вас чи були раніше", 123, 28);
			pdfFile.addText("які-нубудь із наступних захворювань:", 123, 32);
			
			pdfFile.addText("Рецедивуючі язви порожнини рота", 115, 37);
			
			switch (event.result.PATIENTTESTMOUTHULCER)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 37);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 37);
					break;
			}					
			
			pdfFile.addText("Рецедивуючі грибкові інфекції", 115, 42);
			
			switch (event.result.PATIENTTESTFUNGUS)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 42);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 42);
					break;
			}
			
			pdfFile.addText("Тривала непояснена гарячка", 115, 47);
			
			switch (event.result.PATIENTTESTFEVER)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 47);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 47);
					break;
			}
			
			pdfFile.addText("Тривалі болі у горлі або", 115, 52);
			pdfFile.addText("ускладнене дихання", 115, 56);
			
			switch (event.result.PATIENTTESTTHROATACHE)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 56);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 56);
					break;
			}
			
			pdfFile.addText("Постійно збільшені лімфовузли", 115, 61);				
			
			switch (event.result.PATIENTTESTLYMPHNODES)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 61);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 61);
					break;
			}
			
			pdfFile.addText("Багрові або червоні ділянки на кожі", 115, 66);
			pdfFile.addText("або в порожнині рота", 115, 70);
			
			switch (event.result.PATIENTTESTREDSKINAREA)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 70);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 70);
					break;
			}
			
			pdfFile.addText("Профузне нічне потовиділення", 115, 75);				
			
			switch (event.result.PATIENTTESTHYPERHIDROSIS)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 75);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 75);
					break;
			}
			
			pdfFile.addText("Діарея", 115, 80);		
			
			switch (event.result.PATIENTTESTDIARRHEA)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 80);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 80);
					break;
			}
			
			pdfFile.addText("Втрата пам'яті", 115, 85);				
			
			switch (event.result.PATIENTTESTAMNESIA)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 85);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 85);
					break;
			}
			
			pdfFile.addText("Нещодавна втрата ваги не в результаті", 115, 90);
			pdfFile.addText("дієти", 115, 94);
			
			switch (event.result.PATIENTTESTWEIGHTLOSS)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 94);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 94);
					break;
			}
			
			pdfFile.addText("Сильні головні болі", 115, 99);	
			
			switch (event.result.PATIENTTESTHEADACHE)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 99);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 99);
					break;
			}
			
			pdfFile.addText("13.", 115, 104);
			pdfFile.addText("Чи можливо, що Ви зазнали впливу", 123, 104);
			pdfFile.addText("вірусу СНІДу?", 123, 108);
			
			switch (event.result.PATIENTTESTAIDS)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 108);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 108);
					break;
			}
			
			pdfFile.addText("14.", 115, 113);
			pdfFile.addText("Чи пов'язана Ваша робота з контактом з", 123, 113);
			pdfFile.addText("цільною кров'ю, препаратами крові або", 123, 117);
			pdfFile.addText("забрудненими голками і інструментом?", 123, 121);
			
			switch (event.result.PATIENTTESTWORKWITHBLOOD)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 121);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 121);
					break;
			}
			
			pdfFile.addText("15.", 115, 126);
			pdfFile.addText("Чи лікувались Ви коли-небудь від", 123, 126);
			pdfFile.addText("наркоманії?", 123, 130);
			
			switch (event.result.PATIENTTESTNARCOMANIA)
			{
				case "0":					
					pdfFile.addText("НІ", 190, 130);
					break;
				case "1":
					pdfFile.addText("ТАК", 190, 130);
					break;
			}
			
			if (event.result.PATIENTTESTTESTDATE!=null)
				pdfFile.addText(StringFunc.substring(event.result.PATIENTTESTTESTDATE, 8, 2)+"."+StringFunc.substring(event.result.PATIENTTESTTESTDATE, 5, 2)+"."+StringFunc.substring(event.result.PATIENTTESTTESTDATE, 0, 4), 115, 140);
			
			// -----------------------------------------------------------------------------------------------
			// огляд зубів (2 сторінка)			
			
			var i: int = 0;
			
			do
			{
				pdfFile.addPage();
				
				pdfFile.setFont(timesNewRomanCyrilicBoldFont, 12);
				pdfFile.addText("Дані об'єктивного дослідження, зовнішній огляд, стан зубів:", 10, 10);
				
				pdfFile.lineStyle(new RGBColor(0x000000), 0.2);
				
				pdfFile.moveTo(128, 11);
				pdfFile.lineTo(203, 11);
				
				pdfFile.moveTo(10, 17);
				pdfFile.lineTo(203, 17);
				
				pdfFile.end();
				
				var contentArray: Array = new Array();
				contentArray.push({c1:"   \r\n    \r\n   ", c2:"", c3:"", c3:"", c5:"", c6:"", c7:"", c8:"", c9:"", c10:"", c11:"", c12:"", c13:"", c14:"", c15:"", c16:"", c17:""});
				contentArray.push({c1:"Дата оглядів", c2:centerLabel("8", 9), c3:centerLabel("7", 9), c4:centerLabel("6", 9), c5:centerLabel("5 (V)", 9), c6:centerLabel("4 (IV)", 9), c7:centerLabel("3 (III)", 9), c8:centerLabel("2 (II)", 9), c9:centerLabel("1 (I)", 9), c10:centerLabel("1 (I)", 9), c11:centerLabel("2 (II)", 9), c12:centerLabel("3 (III)", 9), c13:centerLabel("4 (IV)", 9), c14:centerLabel("5 (V)", 9), c15:centerLabel("6", 9), c16:centerLabel("7", 9), c17:centerLabel("8", 9)});
				contentArray.push({c1:"   \r\n    \r\n   ", c2:"", c3:"", c3:"", c5:"", c6:"", c7:"", c8:"", c9:"", c10:"", c11:"", c12:"", c13:"", c14:"", c15:"", c16:"", c17:""});
				contentArray.push({c1:"", c2:"", c3:"", c3:"", c5:"", c6:"", c7:"", c8:"", c9:"", c10:"", c11:"", c12:"", c13:"", c14:"", c15:"", c16:"", c17:""});
				
				var column1Caption: String = "   \r\n    \r\n                       \r\n    \r\n";
				contentArray[3].c1 = column1Caption;
				
				var column2Caption: String = "";
				var column3Caption: String = "";
				var column4Caption: String = "";
				var column5Caption: String = "";
				var column6Caption: String = "";
				var column7Caption: String = "";
				var column8Caption: String = "";
				var column9Caption: String = "";
				var column10Caption: String = "";
				var column11Caption: String = "";
				var column12Caption: String = "";
				var column13Caption: String = "";
				var column14Caption: String = "";
				var column15Caption: String = "";
				var column16Caption: String = "";
				var column17Caption: String = "";
				
				if((event.result.TOOTHEXAM != null)&&(event.result.TOOTHEXAM.length > 0))
				{
						var patientToothExam:PatientCardPDFModuleExam = event.result.TOOTHEXAM[i] as PatientCardPDFModuleExam;
						
						var examDate:String = StringFunc.right(patientToothExam.EXAMDATE, 2)+"."+StringFunc.substring(patientToothExam.EXAMDATE, 5, 2)+"."+StringFunc.left(patientToothExam.EXAMDATE, 4);
						
						column1Caption = "\r\n\r\n"+examDate+"\r\n\r\n\r\n";	
						contentArray[3].c1 = column1Caption;
						
						column2Caption = centerLabel(getExamForTooth(18, patientToothExam.EXAM), 57);
						contentArray[3].c2 = centerLabel(getExamForTooth(48, patientToothExam.EXAM), 57);
						
						column3Caption = centerLabel(getExamForTooth(17, patientToothExam.EXAM), 57);
						contentArray[3].c3 = centerLabel(getExamForTooth(47, patientToothExam.EXAM), 57);
						
						column4Caption = centerLabel(getExamForTooth(16, patientToothExam.EXAM), 57);
						contentArray[3].c4 = centerLabel(getExamForTooth(46, patientToothExam.EXAM), 57);
						
						column5Caption = centerLabel(getExamForTooth(15, patientToothExam.EXAM), 57);
						contentArray[3].c5 = centerLabel(getExamForTooth(45, patientToothExam.EXAM), 57);
						
						column6Caption = centerLabel(getExamForTooth(14, patientToothExam.EXAM), 57);
						contentArray[3].c6 = centerLabel(getExamForTooth(44, patientToothExam.EXAM), 57);
						
						column7Caption = centerLabel(getExamForTooth(13, patientToothExam.EXAM), 57);
						contentArray[3].c7 = centerLabel(getExamForTooth(43, patientToothExam.EXAM), 57);
						
						column8Caption = centerLabel(getExamForTooth(12, patientToothExam.EXAM), 57);
						contentArray[3].c8 = centerLabel(getExamForTooth(42, patientToothExam.EXAM), 57);
						
						column9Caption = centerLabel(getExamForTooth(11, patientToothExam.EXAM), 57);
						contentArray[3].c9 = centerLabel(getExamForTooth(41, patientToothExam.EXAM), 57);			
						
						
						
						column10Caption = centerLabel(getExamForTooth(21, patientToothExam.EXAM), 57);
						contentArray[3].c10 = centerLabel(getExamForTooth(31, patientToothExam.EXAM), 57);
						
						column11Caption = centerLabel(getExamForTooth(22, patientToothExam.EXAM), 57);
						contentArray[3].c11 = centerLabel(getExamForTooth(32, patientToothExam.EXAM), 57);
						
						column12Caption = centerLabel(getExamForTooth(23, patientToothExam.EXAM), 57);
						contentArray[3].c12 = centerLabel(getExamForTooth(33, patientToothExam.EXAM), 57);
						
						column13Caption = centerLabel(getExamForTooth(24, patientToothExam.EXAM), 57);
						contentArray[3].c13 = centerLabel(getExamForTooth(34, patientToothExam.EXAM), 57);
						
						column14Caption = centerLabel(getExamForTooth(25, patientToothExam.EXAM), 57);
						contentArray[3].c14 = centerLabel(getExamForTooth(35, patientToothExam.EXAM), 57);
						
						column15Caption = centerLabel(getExamForTooth(26, patientToothExam.EXAM), 57);
						contentArray[3].c15 = centerLabel(getExamForTooth(36, patientToothExam.EXAM), 57);
						
						column16Caption = centerLabel(getExamForTooth(27, patientToothExam.EXAM), 57);
						contentArray[3].c16 = centerLabel(getExamForTooth(37, patientToothExam.EXAM), 57);
						
						column17Caption = centerLabel(getExamForTooth(28, patientToothExam.EXAM), 57);
						contentArray[3].c17 = centerLabel(getExamForTooth(38, patientToothExam.EXAM), 57);
				}
				
				var gridColumn1: GridColumn = new GridColumn(column1Caption, "c1", 17, Align.CENTER, Align.CENTER);
				var gridColumn2: GridColumn = new GridColumn(column2Caption, "c2", 11);
				var gridColumn3: GridColumn = new GridColumn(column3Caption, "c3", 11);
				var gridColumn4: GridColumn = new GridColumn(column4Caption, "c4", 11);
				var gridColumn5: GridColumn = new GridColumn(column5Caption, "c5", 11);
				var gridColumn6: GridColumn = new GridColumn(column6Caption, "c6", 11);
				var gridColumn7: GridColumn = new GridColumn(column7Caption, "c7", 11);
				var gridColumn8: GridColumn = new GridColumn(column8Caption, "c8", 11);
				var gridColumn9: GridColumn = new GridColumn(column9Caption, "c9", 11);
				var gridColumn10: GridColumn = new GridColumn(column10Caption, "c10", 11);
				var gridColumn11: GridColumn = new GridColumn(column11Caption, "c11", 11);
				var gridColumn12: GridColumn = new GridColumn(column12Caption, "c12", 11);
				var gridColumn13: GridColumn = new GridColumn(column13Caption, "c13", 11);
				var gridColumn14: GridColumn = new GridColumn(column14Caption, "c14", 11);
				var gridColumn15: GridColumn = new GridColumn(column15Caption, "c15", 11);
				var gridColumn16: GridColumn = new GridColumn(column16Caption, "c16", 11);
				var gridColumn17: GridColumn = new GridColumn(column17Caption, "c17", 11);
				
				var columns:Array = new Array (gridColumn1, gridColumn2, gridColumn3, gridColumn4, gridColumn5, gridColumn6,
					gridColumn7, gridColumn8, gridColumn9, gridColumn10, gridColumn11, gridColumn12,
					gridColumn13, gridColumn14, gridColumn15, gridColumn16, gridColumn17);
				

				
				var headerColor: IColor = new RGBColor(0xFFFFFF);
				var useAlternateRowColor: Boolean = false;
				var borderAlpha: Number = 1;				
				var cellColor: IColor = new RGBColor(0xF0F0F0);											
				
				var grid: Grid = new Grid(contentArray, 0, 2, headerColor, cellColor, useAlternateRowColor, borderColor, borderAlpha, "0 j", columns);							
				
				pdfFile.setFont(timesNewRomanCyrilicBoldFont, 9);
				pdfFile.textStyle(new RGBColor(0x000000));					
				
				pdfFile.addGrid(grid, 0, 13, false);	
				
				pdfFile.addImageStream(new tooth4bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 18, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth4bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 29, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth4bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 40, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth3bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 51, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth2bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 62, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 73, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 84, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 95, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 106, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 117, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 128, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth2bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 139, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth3bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 150, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth4bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 161, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth4bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 172, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth4bIcon() as ByteArray, ColorSpace.DEVICE_RGB, null, 183, 40, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				
				pdfFile.addImageStream(new tooth2Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 18, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth2Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 29, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth2Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 40, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth3Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 51, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth3Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 62, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 73, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 84, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 95, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 106, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 117, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth1Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 128, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth3Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 139, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth3Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 150, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth2Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 161, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth2Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 172, 65, 9, 11.25, 0, 1, Blend.NORMAL, null); 
				pdfFile.addImageStream(new tooth2Icon() as ByteArray, ColorSpace.DEVICE_RGB, null, 183, 65, 9, 11.25, 0, 1, Blend.NORMAL, null);			
				
				pdfFile.setFont(timesNewRomanCyrilicItalicFont, 10);
				pdfFile.addText("Умовні позначення:", 87, 117);
				
				pdfFile.setXY(10, 119);
				
				pdfFile.setFont(timesNewRomanCyrilicFont, 10);
				pdfFile.writeText(4, "C - карієс, P - пульпіт, Pt - періодонтит, Lp - локалізований пародонтит, Gp - генералізований пародонтит,\n" +
					"R - корінь, A - відсутній, Cd - коронка, Pl - пломба, F - фасетка, ar - штучний зуб, r - рестраврація, H - гемісекція,\n" +
					"Am - ампутація, rez - резекція, pin - штифт, I - імплантація, Rp - реплантація, Dc - зубний камінь,\n" + 
					"tPl - тимчасова пломба, Lps - пардонтоз, htd - дефект твердих тканин");
				/*
				tPl - временная пломба
				Lps - пардонтоз
				htd - дефект твердых тканей
				*/
				i++;
				if ((event.result.TOOTHEXAM != null)&&(event.result.TOOTHEXAM.length > i)) patientToothExam = event.result.TOOTHEXAM[i] as PatientCardPDFModuleExam;
				else patientToothExam = null;
			}
			while (patientToothExam != null) 			
			
			// -----------------------------------------------------------------------------------------------
			// 3 сторінка	(прикус, гігієна порожнини рота)			
			
			pdfFile.addPage();
			
			if ((event.result.AMBCARD != null)&&(event.result.AMBCARD.length > 0)) ambulatoryCardInfo = event.result.AMBCARD[0] as PatientCardPDFModuleAmbcard;
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 10);			
			
			var str: String;
			
			if (ambulatoryCardInfo.OCCLUSION != null)	str = ambulatoryCardInfo.OCCLUSION
			else str = "";
			
			pdfFile.setXY(9, 6);
			pdfFile.writeText(6, "Прикус: "+StringFunc.left(str, 200));
			
			pdfFile.lineStyle(new RGBColor(0x000000), 0.2);
			
			pdfFile.moveTo(24, 11);
			pdfFile.lineTo(203, 11);
			
			pdfFile.moveTo(10, 17);
			pdfFile.lineTo(203, 17);
			
			pdfFile.end();
			
			pdfFile.addText("Стан слизової оболонки порожнини рота, ясен, альвеолярних відростків та піднебіння. Індекси ГІ та РМА.", 10, 22);
			
			if (ambulatoryCardInfo.HYGIENE != null)	str = ambulatoryCardInfo.HYGIENE
			else str = "";
			
			pdfFile.setXY(9, 24);
			pdfFile.writeText(6, "Гігієна порожнини рота: "+StringFunc.left(str, 700));
			
			pdfFile.moveTo(51, 29);
			pdfFile.lineTo(203, 29);			
			
			pdfFile.moveTo(10, 35);
			pdfFile.lineTo(203, 35);
			
			pdfFile.moveTo(10, 41);
			pdfFile.lineTo(203, 41);
			
			pdfFile.moveTo(10, 47);
			pdfFile.lineTo(203, 47);
			
			pdfFile.moveTo(10, 53);
			pdfFile.lineTo(203, 53);
			
			pdfFile.moveTo(10, 59);
			pdfFile.lineTo(203, 59);
			
			pdfFile.end();
			
			if (ambulatoryCardInfo.ANALYSIS != null) str = ambulatoryCardInfo.ANALYSIS
			else str = "";
			
			pdfFile.setXY(9, 60);
			pdfFile.writeText(6, "Дані рентгенівських обстежень, лабораторних досліджень: "+StringFunc.left(str, 900));
			
			pdfFile.moveTo(105, 65);
			pdfFile.lineTo(203, 65);
			
			pdfFile.moveTo(10, 71);
			pdfFile.lineTo(203, 71);
			
			pdfFile.moveTo(10, 77);
			pdfFile.lineTo(203, 77);
			
			pdfFile.moveTo(10, 83);
			pdfFile.lineTo(203, 83);
			
			pdfFile.moveTo(10, 89);
			pdfFile.lineTo(203, 89);
			
			pdfFile.moveTo(10, 95);
			pdfFile.lineTo(203, 95);
			
			pdfFile.moveTo(10, 101);
			pdfFile.lineTo(203, 101);
			
			pdfFile.moveTo(10, 107);
			pdfFile.lineTo(203, 107);			
			
			pdfFile.end();
			
			if (ambulatoryCardInfo.VITASCALE != null)	str = ambulatoryCardInfo.VITASCALE
			else str = "";
			
			pdfFile.setXY(9, 108);
			pdfFile.writeText(6, 'Колір за шкалою "Віта": '+StringFunc.left(str, 200));
			
			pdfFile.moveTo(50, 113);
			pdfFile.lineTo(203, 113);
			
			pdfFile.moveTo(10, 119);
			pdfFile.lineTo(203, 119);
			
			pdfFile.end();
			
			str = "";
			var str2: String = "";
			var controlDate: String = "";
			
			if(event.result.HYGCONTROL!=null)
			{
				for (i = 0; i < event.result.HYGCONTROL.length; i++)
				{
					hygieneInfo = event.result.HYGCONTROL[i] as PatientCardPDFModuleHygcontrol;				
					
					if ((hygieneInfo.CONTROLDATE != null) && (hygieneInfo.ISCONTROL = "0"))
					{
						controlDate = StringFunc.right(hygieneInfo.CONTROLDATE, 2)+"."+StringFunc.substring(hygieneInfo.CONTROLDATE, 5, 2)+"."+StringFunc.left(hygieneInfo.CONTROLDATE, 4);
						
						str = controlDate + " " + hygieneInfo.SHORTNAME;
					}
					
					if ((hygieneInfo.CONTROLDATE != null) && (hygieneInfo.ISCONTROL = "1"))
					{
						controlDate = StringFunc.right(hygieneInfo.CONTROLDATE, 2)+"."+StringFunc.substring(hygieneInfo.CONTROLDATE, 5, 2)+"."+StringFunc.left(hygieneInfo.CONTROLDATE, 4);
						
						str2 += controlDate + " " + hygieneInfo.SHORTNAME + "         " + ", ";	
					}			
				}	
			}
			
			if (str2.length > 2) str2 = StringFunc.removeChars(str2, str2.length - 2, 2);
			
			//if (hygieneInfoArray.length>0) commonClinicInformation = commonClinicInformationArray[0] as ClinicRegistrationDataRecord;
			
			pdfFile.addText('Дата навчання навичкам гігієни порожнини рота: '+StringFunc.left(str, 50), 10, 124);
			
			pdfFile.moveTo(91, 125);
			pdfFile.lineTo(203, 125);
			
			pdfFile.end();			
			
			pdfFile.setXY(9, 126);
			pdfFile.writeText(6, 'Дата контролю гігієни порожнини рота: '+StringFunc.left(str2, 200));
			
			pdfFile.moveTo(76, 131);
			pdfFile.lineTo(203, 131);
			
			pdfFile.moveTo(10, 137);
			pdfFile.lineTo(203, 137);
			
			pdfFile.moveTo(10, 143);
			pdfFile.lineTo(203, 143);			
			
			pdfFile.end();
			
			// -----------------------------------------------------------------------------------------------
			// план лікування (4 сторінка)	
			
			pdfFile.addPage();
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 14);
			pdfFile.addText("ПЛАН ЛІКУВАННЯ", 80, 10);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 10);
			
			var text: String = "";
			var recordDate: String = "";		
			
			if ((event.result.ILLHISTORY!=null)&&(event.result.ILLHISTORY.length > 0)) 
			{
				for (i = 0; i< event.result.ILLHISTORY.length; i++)
				{
					illHistory = event.result.ILLHISTORY[i] as PatientCardPDFModuleIllHistoryRecord;				
					
					if (illHistory.RECORDTYPE != "P") continue;
					
					recordDate = StringFunc.right(illHistory.DATEOFRECORD, 2)+"."+StringFunc.substring(illHistory.DATEOFRECORD, 5, 2)+"."+StringFunc.left(illHistory.DATEOFRECORD, 4);
					
					text += recordDate + "   ("+illHistory.DOCTORSHORTNAME+") - "+illHistory.HISTORYTEXT+".\n\n";
				}				
			}
			else illHistory = null;			
			
			
			if (illHistory != null)
			{
				pdfFile.setXY(9, 16);
				pdfFile.writeText(6, text);
			}			
			
			// -----------------------------------------------------------------------------------------------
			// щоденник лікаря (5 сторінка)			
			
			pdfFile.addPage();					
			
			pdfFile.setFont(timesNewRomanCyrilicBoldFont, 14);
			pdfFile.addText("ЩОДЕННИК ЛІКАРЯ", 80, 10);
			
			pdfFile.setFontSize(10);			
			pdfFile.addText("Анамнез, статус, діагноз, лікування та рекомендації", 65, 15);
			
			pdfFile.setFont(timesNewRomanCyrilicFont, 10);
			
			text = "";
			
			var recordType: String = "";
			
			if ((event.result.ILLHISTORY!=null)&&(event.result.ILLHISTORY.length > 0)) 
			{
				for (i = 0; i< event.result.ILLHISTORY.length; i++)
				{
					illHistory = event.result.ILLHISTORY[i] as PatientCardPDFModuleIllHistoryRecord;
					
					if (illHistory.RECORDTYPE == "P") continue;
					
					switch (illHistory.RECORDTYPE)
					{
						case 'A':
							recordType = "Анамнез";
							break;
						
						case 'S':
							recordType = "Статус";
							break;
						
						case 'D':
							recordType = "Діагноз";
							break;
						
						case 'H':
							recordType = "Лікування";
							break;
						
						case 'C':
							recordType = "Рекомендації";
							break;
					}
					
					recordDate = StringFunc.right(illHistory.DATEOFRECORD, 2)+"."+StringFunc.substring(illHistory.DATEOFRECORD, 5, 2)+"."+StringFunc.left(illHistory.DATEOFRECORD, 4);
					
					text += recordDate + "    " + recordType + " ("+illHistory.DOCTORSHORTNAME+") - "+illHistory.HISTORYTEXT+".\n\n";
				}				
			}
			else illHistory = null;	
			
			
			if (illHistory != null)
			{
				pdfFile.setXY(9, 16);
				pdfFile.writeText(6, text);
			}
			
			return pdfFile;
		}	
	}
}