package modules.PatientCardModule.dictionaries.certificate.events
{
	import flash.events.Event;
	
	import valueObjects.DiscountModuleCertificate;
	
	public class CertificateEvent extends Event
	{
		public static const PRINT_AGREEMENT:String="printCertificateAgreement";
		public var certificate:DiscountModuleCertificate;
		
		public function CertificateEvent(type:String, certificate:DiscountModuleCertificate=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.certificate=certificate;
		}
	}
}