package modules
{
	public interface IToothFairyModule
	{
		function getData(patientId:String = null):void;  
		function clearData():void;  
	}
}