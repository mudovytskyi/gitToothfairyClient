package modules.AdminModule.components
{
	import mx.collections.ArrayCollection;
	
	public class AdminClientSettings
	{
		public static var changes:ArrayCollection=new ArrayCollection;
		
		public static function setDefault():void
		{
			AdminClientSettings.isVocabulariesTab = true;
			AdminClientSettings.isTestConstructorEnabled = true;
			AdminClientSettings.isAdministratorTab = true;
			AdminClientSettings.isEnableHRModules = true;
			AdminClientSettings.isHRModuleEnabled = true;
			AdminClientSettings.isOnlyOwnerTasksEdit = false;
			AdminClientSettings.isEnableDiscountCardModule = true;
			AdminClientSettings.isEnableCertificateModule = true;
			AdminClientSettings.isEnableDiscountCardSumChange = false;
			AdminClientSettings.isEnableCertificateSumChange = false;
			
			AdminClientSettings.isEnableAllPatientsARM = false;
			AdminClientSettings.isCalendarTab = false;
			AdminClientSettings.isConstructorTab = true;
			AdminClientSettings.isShowContactData = true;
			AdminClientSettings.isShowFinanceData = true;
			AdminClientSettings.isWizardChangeDoctorEnabled = false;
			
			AdminClientSettings.isARMVocabularies = true;
			AdminClientSettings.isARMTreatbalanceHistorySettings = true;
			AdminClientSettings.isARMTasksHistorySettings = true;
			AdminClientSettings.isARMDoctorsHistorySettings = true;
			AdminClientSettings.isTreatmentCourseModule = true;
			AdminClientSettings.isToothMapModule = true;
			AdminClientSettings.isTestModule = true;
			AdminClientSettings.isExport043Module = true;
			AdminClientSettings.isAddPatientEnable = true;
			AdminClientSettings.isAmbulatoryModule = true;
			AdminClientSettings.isEnableCardNumberChange = true;
			AdminClientSettings.isEnableTestChange = true;
			AdminClientSettings.isLeadDoctorEnabled = true;
			AdminClientSettings.isSubdivisonEditorEnabled = false;
			AdminClientSettings.isQuickTreatmentCourse = false;
			
			AdminClientSettings.isReportsInTreatment = true;
			AdminClientSettings.isEnablePriceChangeInNewTreatment = false;
			AdminClientSettings.isEnableTimeDateChangeInNewTreatment = false;
			AdminClientSettings.isConfirmTreatmentProceduresEnabled = false;
			AdminClientSettings.isEnableMoneyInTreatmentCourse = true;
			AdminClientSettings.isAfterExamsEnabled = true;
			
			AdminClientSettings.isAccountFlowDataChangeEnabled = false;
			AdminClientSettings.isAccountInvoiceDateNumberChangabel = false;
			AdminClientSettings.isPriceInInvoiceCreationEditable = false;
			AdminClientSettings.isEnableDiscountChangeInAccountModule = true;
			AdminClientSettings.isSalaryInAccountEnabled = false;
			AdminClientSettings.isOnlyOwnerInvoiceAndFlowEdit = false;
			AdminClientSettings.isDiscountChangeEnabled=true;
			AdminClientSettings.isShowOnlyCurrentSubdivision=false;	
			AdminClientSettings.isShowOnlyCurrentSubdivisionAccount=false;

			AdminClientSettings.isHRDataEnabled = true;
			AdminClientSettings.isEnableSalaryManager = false;
			AdminClientSettings.isPriceListSalarySettings = false;
			AdminClientSettings.isControlExpensesInPriceList = false;
			AdminClientSettings.isWizardPrimaryEnabled = true;
			AdminClientSettings.isEnableMassMailingInAdministratorPanel = false;
			AdminClientSettings.isSQLiteEnable = true;
			
			AdminClientSettings.isPriceEditEnable = true;
			AdminClientSettings.isPricePrintEnable = true;
			
			AdminClientSettings.isARMDispancer = false;
			AdminClientSettings.isAccountShowOnlyUserOperation=false;
			AdminClientSettings.isNegativeDiscount=false;
			AdminClientSettings.isAccoundLendDebPrevDate=true;
		}
		
		public static function settingFromString(name:String, value:String):void
		{
			AdminClientSettings[name]=(value=="1");
		}
		
		public static function settingToString(name:String):String
		{
			return (AdminClientSettings[name] as Boolean)?"1":"0";
		}
		
		public static var isVocabulariesTab:Boolean = true;
		public static var isTestConstructorEnabled:Boolean = true;
		public static var isAdministratorTab:Boolean = true;
		public static var isEnableHRModules:Boolean = true;
		public static var isHRModuleEnabled:Boolean = true;
		public static var isEnableDiscountCardModule:Boolean = true;
		public static var isEnableCertificateModule:Boolean = true;
		public static var isEnableDiscountCardSumChange:Boolean = false;
		public static var isEnableCertificateSumChange:Boolean = false;
		public static var isOnlyOwnerTasksEdit:Boolean = false;
		
		public static var isEnableAllPatientsARM:Boolean = false;
		public static var isCalendarTab:Boolean = false;
		public static var isConstructorTab:Boolean = true;
		public static var isShowContactData:Boolean = true;
		public static var isShowFinanceData:Boolean = true;
		public static var isWizardChangeDoctorEnabled:Boolean = false;
		
		public static var isARMVocabularies:Boolean = true;
		public static var isARMTreatbalanceHistorySettings:Boolean = true;
		public static var isARMTasksHistorySettings:Boolean = true;
		public static var isARMDoctorsHistorySettings:Boolean = true;
		public static var isTreatmentCourseModule:Boolean = true;
		public static var isToothMapModule:Boolean = true;
		public static var isTestModule:Boolean = true;
		public static var isExport043Module:Boolean = true;
		public static var isAddPatientEnable:Boolean = true;
		public static var isAmbulatoryModule:Boolean = true;
		public static var isEnableCardNumberChange:Boolean = true;
		public static var isEnableTestChange:Boolean = true;
		public static var isLeadDoctorEnabled:Boolean = true;
		public static var isSubdivisonEditorEnabled:Boolean = false;
		public static var isQuickTreatmentCourse:Boolean = false;
		
		public static var isReportsInTreatment:Boolean = true;
		public static var isEnablePriceChangeInNewTreatment:Boolean = false;
		public static var isEnableTimeDateChangeInNewTreatment:Boolean = false;
		public static var isConfirmTreatmentProceduresEnabled:Boolean = false;
		public static var isEnableMoneyInTreatmentCourse:Boolean = true;
		public static var isAfterExamsEnabled:Boolean = true;
		
		public static var isAccountFlowDataChangeEnabled:Boolean = false;
		public static var isAccountInvoiceDateNumberChangabel:Boolean = false;
		public static var isPriceInInvoiceCreationEditable:Boolean = false;
		public static var isEnableDiscountChangeInAccountModule:Boolean = true;
		public static var isSalaryInAccountEnabled:Boolean = false;
		public static var isOnlyOwnerInvoiceAndFlowEdit:Boolean = false;
		public static var isDiscountChangeEnabled:Boolean = true;
		public static var isShowOnlyCurrentSubdivision:Boolean = false;
		public static var isShowOnlyCurrentSubdivisionAccount:Boolean = false;
		
		public static var isHRDataEnabled:Boolean = true;
		public static var isEnableSalaryManager:Boolean = false;
		public static var isPriceListSalarySettings:Boolean = false;
		public static var isControlExpensesInPriceList:Boolean = false;
		public static var isWizardPrimaryEnabled:Boolean = true;
		public static var isEnableMassMailingInAdministratorPanel:Boolean = false;
		public static var isSQLiteEnable:Boolean = true;
		
		public static var isPriceEditEnable:Boolean = true;
		public static var isPricePrintEnable:Boolean = true;
		public static var isAccountShowOnlyUserOperation:Boolean=false;
		public static var isARMDispancer:Boolean = false;
		
		public static var isAccoundLendDebPrevDate:Boolean = true;
		public static var isNegativeDiscount:Boolean = false;
	}
}