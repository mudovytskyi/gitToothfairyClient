package modules.AdminModule.moduls.googlesync.events
{
	import flash.events.Event;
	
	import mx.utils.StringUtil;
	
	public final class GoogleSyncEvent extends Event
	{
		//---- ЗАПИТИ ----
		// календар
		public static const GOOGLE_SYNC_UPDATE_CALENDAR:String = "gsycnModule.GOOGLE_SYNC_UPDATE_CALENDAR";
		
		// події
		public static const GOOGLE_SYNC_CALENDAR_EVENT_ADD:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_ADD";
		public static const GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD";
		public static const GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT:String = "gsycnModule.GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT";
		public static const GOOGLE_SYNC_CALENDAR_EVENT_DELETE:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_DELETE";
		public static const GOOGLE_SYNC_CALENDAR_EVENT_UPDATE:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_UPDATE";
		public static const GOOGLE_SYNC_CALENDAR_EVENT_NOT_FOUND:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_NOT_FOUND";

		// инше
		public static const GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS:String = "gsycnModule.GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS";
		public static const GOOGLE_SYNC_GET_CALENDAR_COLORS:String = "gsycnModule.GOOGLE_SYNC_GET_CALENDAR_COLORS";
		public static const GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES:String = "gsycnModule.GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES";

		
		//---- РЕЗУЛЬТАТИ ----
		public static const GOOGLE_SYNC_CALENDAR_EVENT_ADD_RESULT:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_ADD_RESULT";
		public static const GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD_RESULT:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD_RESULT";
		public static const GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT_RESULT:String = "gsycnModule.GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT_RESULT";
		public static const GOOGLE_SYNC_CALENDAR_EVENT_UPDATE_RESULT:String = "gsycnModule.GOOGLE_SYNC_CALENDAR_EVENT_UPDATE_RESULT";
		
		public static const GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS_RESULT:String = "gsycnModule.GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS_RESULT";
		public static const GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES_RESULT:String = "gsycnModule.GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES_RESULT";
//		public static const GOOGLE_SYNC_ACCOUNT_AUTHENTICATED:String = "gsycnModule.GOOGLE_SYNC_ACCOUNT_AUTHENTICATED";
//		public static const GOOGLE_SYNC_ACCOUNT_RESET_SETTINGS:String = "gsycnModule.GOOGLE_SYNC_ACCOUNT_RESET_SETTINGS";

		
		//---- ПОМИЛКИ  ----
		public static const ERROR_GOOGLE_SYNC_UPDATE_CALENDAR:String = "gsycnModule.ERROR_GOOGLE_SYNC_UPDATE_CALENDAR";
		
		public static const ERROR_GOOGLE_SYNC_CALENDAR_EVENT_ADD:String = "gsycnModule.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_ADD";
		public static const ERROR_GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD:String = "gsycnModule.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD";
		public static const ERROR_GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT:String = "gsycnModule.ERROR_GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT";
		public static const ERROR_GOOGLE_SYNC_CALENDAR_EVENT_DELETE:String = "gsycnModule.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_DELETE";
		public static const ERROR_GOOGLE_SYNC_CALENDAR_EVENT_UPDATE:String = "gsycnModule.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_UPDATE";

		public static const ERROR_GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS:String = "gsycnModule.ERROR_GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS";
		public static const ERROR_GOOGLE_SYNC_GET_CALENDAR_COLORS:String = "gsycnModule.ERROR_GOOGLE_SYNC_GET_CALENDAR_COLORS";
		public static const ERROR_GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES:String = "gsycnModule.ERROR_GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES";

		
		
		private var _vo:*;
		
		public function GoogleSyncEvent(type:String, vo:*, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._vo = vo;
		}
		
		override public function clone():Event
		{
			return new GoogleSyncEvent(this.type, this.vo, bubbles, cancelable);
		}
		
		override public function toString():String
		{
			return StringUtil.substitute("modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent::{0}", this.type);
		}

		public function get vo():* {
			return _vo;
		}
	}
}