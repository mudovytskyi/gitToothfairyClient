package modules.AdminModule.moduls.googlesync.controllers
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.errors.IllegalOperationError;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import mx.utils.StringUtil;
	
	import modules.AdminModule.moduls.googlesync.controllers.commands.AddAllDayGoogleCalendarEventCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.AddGoogleCalendarEventCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.AddWizGoogleCalendarEventCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.CommonErrorGoogleCalendarEventCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.DeleteGoogleCalendarEventCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.GetGoogleCalendarColorsCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.GetGoogleCalendarEventColorsCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.GetGoogleCalendarTimeZonesCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.ICommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.UpdateGoogleCalendarCommand;
	import modules.AdminModule.moduls.googlesync.controllers.commands.UpdateGoogleCalendarEventCommand;
	import modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent;
	
	//----------------------------------
	//
	// Class : GoogleCalendarController.as
	//
	//----------------------------------
	/** @extends: EventDispatcher
	 *   @implements:
	 *   @use:
	 *
	 *   @author: mykolaU
	 *   @date: Jul 5, 2019 @time: 4:54:40 PM */
	//----------------------------------
	public class GoogleCalendarController extends EventDispatcher
	{
		//----------------------------------
		// Variables
		//----------------------------------
		private static var _instance:GoogleCalendarController;
		private var _dictionaryCommands:Dictionary;
		private var _dictionaryHandlers:Dictionary;
		
		//----------------------------------
		//
		// Constructor
		//
		//----------------------------------
		public function GoogleCalendarController(enhancer:GoogleCalendarControllerEnhancer)
		{
			if (!enhancer) 	{
				throw new IllegalOperationError(
					StringUtil.substitute("Please, get instance of {0} class by using static method 'getInstance'", 
						getQualifiedClassName(GoogleCalendarController)));
			}
			_dictionaryCommands = new Dictionary(true);
			_initCommands();
		}
		
		
		//----------------------------------
		//
		// Public methods
		//
		//----------------------------------
		/**
		 * Прив'язати виконання події класу GoogleSyncEvent до призначення 
		 * @param eventType - Тип події
		 * @param commandClass - Клас призначення виконання
		 */		 
		public function mapClass(eventType:String, commandClass:Class):void
		{
			_dictionaryCommands[eventType] = commandClass;
			this.addEventListener(eventType, executerHandler);
		}
		
		public function getCommand(type:String):ICommand
		{
			return new _dictionaryCommands[type]();
		}
		
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function executerHandler(event:GoogleSyncEvent):void
		{
			var cmd:ICommand = this.getCommand(event.type);
			cmd.command(event.vo);
		}
		
		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function _initCommands():void
		{
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_CALENDAR_EVENT_ADD, AddGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD, AddWizGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT, AddAllDayGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_CALENDAR_EVENT_DELETE, DeleteGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_CALENDAR_EVENT_UPDATE, UpdateGoogleCalendarEventCommand);

			mapClass(GoogleSyncEvent.GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS, GetGoogleCalendarEventColorsCommand);
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_GET_CALENDAR_COLORS, GetGoogleCalendarColorsCommand);
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES, GetGoogleCalendarTimeZonesCommand);
			
			mapClass(GoogleSyncEvent.GOOGLE_SYNC_UPDATE_CALENDAR, UpdateGoogleCalendarCommand);

			
			// ПОМИЛКИ ЗАПИТІВ
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_UPDATE_CALENDAR, CommonErrorGoogleCalendarEventCommand);
			
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_ADD, CommonErrorGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD, CommonErrorGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_ADD_ALL_DAY_CALENDAR_EVENT, CommonErrorGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_UPDATE, CommonErrorGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_DELETE, CommonErrorGoogleCalendarEventCommand);

			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS, CommonErrorGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_GET_CALENDAR_COLORS, CommonErrorGoogleCalendarEventCommand);
			mapClass(GoogleSyncEvent.ERROR_GOOGLE_SYNC_GET_CALENDAR_TIME_ZONES, CommonErrorGoogleCalendarEventCommand);
			
			
		}
		
		//----------------------------------
		//
		// Static public methods
		//
		//----------------------------------
		public static function dispatchEvent(eventType:String, data:* = null):void
		{
			var event:GoogleSyncEvent = new GoogleSyncEvent(eventType, data);
			getInstance.dispatchEvent(event);
		}

		public static function addEventHandler(eventType:String, handler:Function, once:Boolean = true):void
		{
			getInstance.addEventListener(eventType, handler);
			
		}
		

		//----------------------------------
		//
		// Getters & Setters
		//	
		//----------------------------------
		public static function get getInstance():GoogleCalendarController
		{
			if (!_instance) {
				_instance = new GoogleCalendarController(new GoogleCalendarControllerEnhancer());
			}
			return _instance;
		}
	}
}

class GoogleCalendarControllerEnhancer {}