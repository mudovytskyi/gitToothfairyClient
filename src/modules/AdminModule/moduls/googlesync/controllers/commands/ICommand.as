package modules.AdminModule.moduls.googlesync.controllers.commands
{

	//----------------------------------
	//
	// Interface
	//
	//----------------------------------
	public interface ICommand
	{
		function command(vo:* = null):void;
	}
}