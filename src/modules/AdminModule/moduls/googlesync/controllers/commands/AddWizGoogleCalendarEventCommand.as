package modules.AdminModule.moduls.googlesync.controllers.commands
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.events.IEventDispatcher;
	
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import modules.AdminModule.moduls.googlesync.controllers.GoogleCalendarController;
	import modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent;
	import modules.AdminModule.moduls.googlesync.utils.GoogleCalendarEventUtils;
	
	import services.ipClass;
	import services.googlecalendarmodule.GoogleCalendarModule;
	
	import toothfairy.configurations.ConfigurationSettings;
	import toothfairy.settings.ClientSettings;
	
	import valueObjects.GoogleCalendarEventObject;
	
	
	//----------------------------------
	//
	// Class : AddWizGoogleCalendarEventCommand.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements: ICommand
	 *   @use:
	 *
	 *   @author: mykol
	 *   @date: Jul 5, 2019 @time: 11:22:18 PM */
	//----------------------------------
	public class AddWizGoogleCalendarEventCommand implements ICommand
	{
	
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		public function command(vo:* = null):void
		{
			var dto:GoogleCalendarEventObject = GoogleCalendarEventUtils.createGoogleCalendarEventDTO(vo);
			var service:GoogleCalendarModule = new GoogleCalendarModule();
			service.endpoint = ipClass.endpointhost;
			
			var responder:CallResponder = new CallResponder();
			responder.addEventListener(ResultEvent.RESULT, onResultEvent);
			responder.addEventListener(FaultEvent.FAULT, onFaultEvent);
			
//			responder.token = service.addWizardClientCalendarEvent(ConfigurationSettings.NHEALTH_MODE == false,
			responder.token = service.addWizardClientCalendarEvent(true,
				ClientSettings.GOOGLE_CALENDAR_ID_SYNCED,
				dto);
		}
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function onResultEvent(event:ResultEvent):void
		{
			trace(event.result);
			GoogleCalendarController.dispatchEvent(GoogleSyncEvent.GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD_RESULT);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		protected function onFaultEvent(event:FaultEvent):void
		{
			GoogleCalendarController.dispatchEvent(
				GoogleSyncEvent.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_ADD_WIZARD, event
			);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function clean(listener:IEventDispatcher):void
		{
			listener.removeEventListener(ResultEvent.RESULT, onResultEvent);		
			listener.removeEventListener(FaultEvent.FAULT, onFaultEvent);
			listener = null;
		}
	}
}