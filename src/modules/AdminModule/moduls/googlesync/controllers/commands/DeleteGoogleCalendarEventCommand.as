package modules.AdminModule.moduls.googlesync.controllers.commands
{
	import flash.events.IEventDispatcher;
	
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import modules.AdminModule.moduls.googlesync.controllers.GoogleCalendarController;
	import modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent;
	
	import services.ipClass;
	import services.googlecalendarmodule.GoogleCalendarModule;
	
	import toothfairy.configurations.ConfigurationSettings;
	import toothfairy.settings.ClientSettings;
	
	public class DeleteGoogleCalendarEventCommand implements ICommand
	{
		public function command(vo:* = null):void
		{
			var service:GoogleCalendarModule = new GoogleCalendarModule();
			service.endpoint = ipClass.endpointhost;
			
			var responder:CallResponder = new CallResponder();
			responder.addEventListener(ResultEvent.RESULT, onResultEvent);
			responder.addEventListener(FaultEvent.FAULT, onFaultEvent);

//			responder.token = service.deleteClientCalendarEvent(ConfigurationSettings.NHEALTH_MODE == false,
			responder.token = service.deleteClientCalendarEvent(true,
				ClientSettings.GOOGLE_CALENDAR_ID_SYNCED,
				vo.id);
		}
		
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function onFaultEvent(event:FaultEvent):void
		{
			GoogleCalendarController.dispatchEvent(
				GoogleSyncEvent.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_DELETE, event
			);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		protected function onResultEvent(event:ResultEvent):void
		{
			trace(event.result);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function clean(listener:IEventDispatcher):void
		{
			listener.removeEventListener(ResultEvent.RESULT, onResultEvent);		
			listener.removeEventListener(FaultEvent.FAULT, onFaultEvent);
			listener = null;
		}
	}
}