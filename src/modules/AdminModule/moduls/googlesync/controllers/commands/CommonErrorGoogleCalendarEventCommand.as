package modules.AdminModule.moduls.googlesync.controllers.commands
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import mx.controls.Alert;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	
	import modules.AdminModule.moduls.googlesync.controllers.GoogleCalendarController;
	import modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent;
	
	import toothfairy.configurations.ConfigurationSettings;
	
	
	//----------------------------------
	//
	// Class : CommonErrorGoogleCalendarEventCommand.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements: ICommand
	 *   @use:
	 *
	 *   @author: mykol
	 *   @date: Jul 5, 2019 @time: 11:22:18 PM */
	//----------------------------------
	public class CommonErrorGoogleCalendarEventCommand implements ICommand
	{
		
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		public function command(vo:* = null):void
		{
			var event:FaultEvent = vo as FaultEvent;
			var faultCode:String = event.fault.faultCode;
			var faultString:String = event.fault.faultString;
			
			var title:String;
			var message:String;
			
			//---------------------------------
			// Nota Benne!!! Треба зареєструвати !!!
			//---------------------------------
//			event.fault.faultString = "{ "error": { "errors": [ { "domain": "usageLimits", "reason": "dailyLimitExceededUnreg", 
//			"message": "Daily Limit for Unauthenticated Use Exceeded. Continued use requires signup.", "extendedHelp": 
//			"https://code.google.com/apis/console" } ], "code": 403, "message": "Daily Limit for Unauthenticated Use 
//		Exceeded. Continued use requires signup.
			
//			faultString = "{ "error": { "errors": [ { "domain": "global", "reason": "required", "message": "Login Required", 
//			"locationType": "header", "location": "Authorization" } ], "code": 401, "message": "Login Required" } } "
			
			var rsManager:IResourceManager = ResourceManager.getInstance();
			if (faultCode == "401" && faultString.indexOf("Authorization") > -1 &&
				faultString.indexOf("Login Required") > -1) {
				//message = rsManager.getString("GoogleCalendarModule", "GoogleCalendarEventAuthError.body");
				//title = rsManager.getString("GoogleCalendarModule", "GoogleCalendarEventAuthError.title");
				message = faultString;
				title = faultCode + '. ' + event.fault.name;
			} else if (faultCode == "401" && faultString.indexOf("authError") > -1 &&
				faultString.indexOf("Invalid Credentials") > -1) {
				//			ЗРАЗОК ВІДГУКУ ІЗ ЗАСТАРІЛИМИ НЕНАЛАШТОВАННЯМИ АВТОРИЗАЦІї
				//			{
				//				"error": {
				//					"errors": [
				//						{
				//							"domain": "global",
				//							"reason": "authError",
				//							"message": "Invalid Credentials",
				//							"locationType": "header",
				//							"location": "Authorization"
				//						}
				//					],
				//					"code": 401,
				//					"message": "Invalid Credentials"
				//				}
				//			}
				
				message = rsManager.getString("GoogleCalendarModule", "GoogleCalendarEventAuthError.body");
				title = rsManager.getString("GoogleCalendarModule", "GoogleCalendarEventAuthError.title");
				
			} else if (faultCode == "404" && faultString.indexOf("notFound") > -1
						&& faultString.indexOf("Not Found") > -1) {
				//			ЗРАЗОК ВІДГУКУ - ПОДІЯ НЕ ЗНАЙДЕНА, НЕВІДОМА у системі гугл
				//				{
				//					"error": {
				//						"errors": [
				//							{
				//								"domain": "global",
				//								"reason": "notFound",
				//								"message": "Not Found"
				//							}
				//						],
				//						"code": 404,
				//						"message": "Not Found"
				//					}
				//				}
				
				message = rsManager.getString("GoogleCalendarModule", "GoogleCalendarEventNotFoundError.body");
				title = rsManager.getString("GoogleCalendarModule", "GoogleCalendarEventNotFoundError.title");
				GoogleCalendarController.dispatchEvent(GoogleSyncEvent.GOOGLE_SYNC_CALENDAR_EVENT_NOT_FOUND);
			} else if (faultCode == "403" && faultString.indexOf("dailyLimitExceededUnreg") > -1 
						&& faultString.indexOf("Daily Limit") > -1) {
//				{
//					"error": {
//						"errors": [
//							{
//								"domain": "usageLimits",
//								"reason": "dailyLimitExceededUnreg",
//								"message": "Daily Limit for Unauthenticated Use Exceeded. Continued use requires signup.",
//								"extendedHelp": "https://code.google.com/apis/console"
//							}
//						],
//						"code": 403,
//						"message": "Daily Limit for Unauthenticated Use Exceeded. Continued use requires signup."
//					}
//				}
				message = rsManager.getString("GoogleCalendarModule", "GoogleCalendarDailyLimitExceeded.body");
				title = rsManager.getString("GoogleCalendarModule", "GoogleCalendarDailyLimitExceeded.title");
			} else {
				message = faultString;
				title = faultCode + '. ' + event.fault.name;
			}
			
//			if (ConfigurationSettings.isGoogleCalendarSynced && !ConfigurationSettings.NHEALTH_MODE)
			if (ConfigurationSettings.isGoogleCalendarSynced)
				Alert.show(message, title);
		}
		
		
		protected function onFaultEvent(event:FaultEvent):void
		{
		}
		
	}
}