package modules.AdminModule.moduls.googlesync.controllers.commands
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.events.IEventDispatcher;
	
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import modules.AdminModule.moduls.googlesync.controllers.GoogleCalendarController;
	import modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent;
	
	import services.ipClass;
	import services.googlecalendarmodule.GoogleCalendarModule;
	
	import toothfairy.configurations.ConfigurationSettings;
	
	import valueObjects.GoogleCalendarObject;
	
	//----------------------------------
	//
	// Class : UpdateGoogleCalendarCommand.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements: ICommand
	 *   @use:
	 *
	 *   @author: mykol
	 *   @date: Jul 9, 2019 @time: 9:57:38 AM */
	//----------------------------------
	public class UpdateGoogleCalendarCommand implements ICommand
	{
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		public function command(vo:* = null):void
		{
			var dto:GoogleCalendarObject = vo;
			
			if (!dto)
				throw new Error("GoogleCalendarObject is null");
			
			var service:GoogleCalendarModule = new GoogleCalendarModule();
			service.endpoint = ipClass.endpointhost;
			
			var responder:CallResponder = new CallResponder();
			responder.addEventListener(ResultEvent.RESULT, onResultEvent);
			responder.addEventListener(FaultEvent.FAULT, onFaultEvent);

//			responder.token = service.updateClientCalendar(ConfigurationSettings.NHEALTH_MODE == false,				dto);
			responder.token = service.updateClientCalendar(true, dto);
		}
		
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function onFaultEvent(event:FaultEvent):void
		{
			GoogleCalendarController.dispatchEvent(
				GoogleSyncEvent.ERROR_GOOGLE_SYNC_UPDATE_CALENDAR, event
			);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		protected function onResultEvent(event:ResultEvent):void
		{
			trace(event.result);	
			clean(event.currentTarget as IEventDispatcher);
		}
		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function clean(listener:IEventDispatcher):void
		{
			listener.removeEventListener(ResultEvent.RESULT, onResultEvent);		
			listener.removeEventListener(FaultEvent.FAULT, onFaultEvent);
			listener = null;
		}
	}
}