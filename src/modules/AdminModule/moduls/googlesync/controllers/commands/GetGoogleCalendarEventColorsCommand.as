package modules.AdminModule.moduls.googlesync.controllers.commands
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import modules.AdminModule.moduls.googlesync.controllers.GoogleCalendarController;
	import modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent;
	
	import services.ipClass;
	import services.googlecalendarmodule.GoogleCalendarModule;
	
	import toothfairy.configurations.ConfigurationSettings;
	import toothfairy.settings.colors.ColorSettings;
	
	//----------------------------------
	//
	// Class : GetGoogleCalendarEventColorsCommand.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements: ICommand
	 *   @use:
	 *
	 *   @author: mykol
	 *   @date: Jul 5, 2019 @time: 11:22:18 PM */
	//----------------------------------
	public class GetGoogleCalendarEventColorsCommand implements ICommand
	{
	
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		public function command(vo:* = null):void
		{
			var service:GoogleCalendarModule = new GoogleCalendarModule();
			service.endpoint = ipClass.endpointhost;
			
			var responder:CallResponder = new CallResponder();
			responder.addEventListener(ResultEvent.RESULT, onResultEvent);
			responder.addEventListener(FaultEvent.FAULT, onFaultEvent);

//			responder.token = service.getClientCalendarEventColors(ConfigurationSettings.NHEALTH_MODE == false);
			responder.token = service.getClientCalendarEventColors(true);
		}
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function onResultEvent(event:ResultEvent):void
		{
			ColorSettings.roomGoogleEventColors = event.result as ArrayCollection;
			GoogleCalendarController.dispatchEvent(GoogleSyncEvent.GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS_RESULT);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		protected function onFaultEvent(event:FaultEvent):void
		{
			GoogleCalendarController.dispatchEvent(
				GoogleSyncEvent.ERROR_GOOGLE_SYNC_GET_CALENDAR_EVENT_COLORS, event
			);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function clean(listener:IEventDispatcher):void
		{
			listener.removeEventListener(ResultEvent.RESULT, onResultEvent);		
			listener.removeEventListener(FaultEvent.FAULT, onFaultEvent);
			listener = null;
		}
	}
}