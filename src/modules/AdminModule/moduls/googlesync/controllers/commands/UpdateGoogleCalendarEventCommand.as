package modules.AdminModule.moduls.googlesync.controllers.commands
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.events.IEventDispatcher;
	
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import modules.AdminModule.moduls.googlesync.controllers.GoogleCalendarController;
	import modules.AdminModule.moduls.googlesync.events.GoogleSyncEvent;
	import modules.AdminModule.moduls.googlesync.utils.GoogleCalendarEventUtils;
	
	import services.ipClass;
	import services.googlecalendarmodule.GoogleCalendarModule;
	
	import toothfairy.configurations.ConfigurationSettings;
	import toothfairy.settings.ClientSettings;
	
	import valueObjects.GoogleCalendarEventObject;
	
	//----------------------------------
	//
	// Class : UpdateGoogleCalendarEventCommand.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements: ICommand
	 *   @use:
	 *
	 *   @author: mykol
	 *   @date: Jul 9, 2019 @time: 9:57:38 AM */
	//----------------------------------
	public class UpdateGoogleCalendarEventCommand implements ICommand
	{
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		public function command(vo:* = null):void
		{
			var dto:GoogleCalendarEventObject = GoogleCalendarEventUtils.createGoogleCalendarEventDTO(vo);
			var service:GoogleCalendarModule = new GoogleCalendarModule();
			service.endpoint = ipClass.endpointhost;
			
			var responder:CallResponder = new CallResponder();
			responder.addEventListener(ResultEvent.RESULT, onResultEvent);
			responder.addEventListener(FaultEvent.FAULT, onFaultEvent);

//			responder.token = service.updateClientCalendarEvent(ConfigurationSettings.NHEALTH_MODE == false,
			responder.token = service.updateClientCalendarEvent(true,
				ClientSettings.GOOGLE_CALENDAR_ID_SYNCED,
				dto);
		}
		
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function onFaultEvent(event:FaultEvent):void
		{
			GoogleCalendarController.dispatchEvent(
				GoogleSyncEvent.ERROR_GOOGLE_SYNC_CALENDAR_EVENT_UPDATE, event
			);
			clean(event.currentTarget as IEventDispatcher);
		}
		
		protected function onResultEvent(event:ResultEvent):void
		{
			GoogleCalendarController.dispatchEvent(GoogleSyncEvent.GOOGLE_SYNC_CALENDAR_EVENT_UPDATE_RESULT);			
			clean(event.currentTarget as IEventDispatcher);
		}
		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function clean(listener:IEventDispatcher):void
		{
			listener.removeEventListener(ResultEvent.RESULT, onResultEvent);		
			listener.removeEventListener(FaultEvent.FAULT, onFaultEvent);
			listener = null;
		}
	}
}