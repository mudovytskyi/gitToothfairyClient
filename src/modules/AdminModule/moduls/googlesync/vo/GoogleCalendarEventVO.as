package modules.AdminModule.moduls.googlesync.vo
{
	public final class GoogleCalendarEventVO
	{
		public var id:String;
		public var taskId:String;
		public var taskType:int;
		public var patientName:String;
		public var doctorName:String;
		public var roomName:String;
		public var roomNumber:String;
		public var startsAt:Date;
		public var endsAt:Date;
		public var description:String;
		public var comment:String;    
		public var colorId:int;
	}
}