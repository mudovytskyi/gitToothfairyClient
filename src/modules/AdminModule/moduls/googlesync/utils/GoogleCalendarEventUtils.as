package modules.AdminModule.moduls.googlesync.utils
{
	
	import flash.globalization.DateTimeFormatter;
	
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	
	import modules.AdminModule.moduls.googlesync.vo.GoogleCalendarEventVO;
	
	import toothfairy.configurations.ConstantSettings;
	import toothfairy.configurations.utils.TimeZoneUtil;
	import toothfairy.settings.ClientSettings;
	
	import valueObjects.GoogleCalendarEventDateTimeObject;
	import valueObjects.GoogleCalendarEventObject;

	public final class GoogleCalendarEventUtils
	{
		private static var df:DateTimeFormatter = new DateTimeFormatter(ClientSettings.locale);
		
		public static function createGoogleCalendarEventDTO(vo:GoogleCalendarEventVO):GoogleCalendarEventObject
		{
			var dto:GoogleCalendarEventObject = new GoogleCalendarEventObject();
			dto.ID = vo.id;
			dto.TASK_ID = vo.taskId;
			var isNotice:Boolean = vo.taskType == 3;
			var sumTempl:String = ResourceManager.getInstance().getString('GoogleCalendarModule', 'GoogleCalendarEventSummaryTemplate' + (isNotice ? 'Notice' : ''));
			dto.SUMMARY = StringUtil.substitute(sumTempl, vo.patientName, vo.doctorName);
			var descTempl:String = ResourceManager.getInstance().getString('GoogleCalendarModule', 'GoogleCalendarEventDescriptionTemplate' + (isNotice ? 'Notice' : ''));
			dto.DESCRIPTION = StringUtil.substitute(descTempl, vo.patientName, vo.doctorName, vo.roomNumber, vo.roomName, vo.description, vo.comment);
			dto.startEvent = getDateTimeOfGoogleCandarEvent(vo.startsAt);
			dto.endEvent = getDateTimeOfGoogleCandarEvent(vo.endsAt);
			dto.COLOR_ID = vo.colorId;
			return dto;
		}
		
		
		public static function createGoogleAllDayCalendarEventDTO(vo:GoogleCalendarEventVO):GoogleCalendarEventObject
		{
			var dto:GoogleCalendarEventObject = new GoogleCalendarEventObject();
			dto.ID = vo.id;
			dto.TASK_ID = vo.taskId;
			var sumTempl:String = ResourceManager.getInstance().getString('GoogleCalendarModule', 'GoogleCalendarEventSummaryTemplate');
			dto.SUMMARY = StringUtil.substitute(sumTempl, vo.patientName, vo.doctorName);
			var descTempl:String = ResourceManager.getInstance().getString('GoogleCalendarModule', 'GoogleCalendarEventDescriptionTemplate');
			dto.DESCRIPTION = StringUtil.substitute(descTempl, vo.patientName, vo.doctorName, vo.roomNumber, vo.roomName, vo.description, vo.comment);
			
			// Цілодобова подія =======
			vo.startsAt.hours = 0;
			vo.startsAt.minutes = 0;
			vo.startsAt.seconds = 0;
			
			vo.endsAt.date += 1;
			vo.endsAt.hours = 0;
			vo.endsAt.minutes = 0;
			vo.endsAt.seconds = 1;
			// -------------------------
			
			dto.startEvent = getDateTimeOfGoogleCandarEvent(vo.startsAt);
			dto.endEvent = getDateTimeOfGoogleCandarEvent(vo.endsAt);
			dto.COLOR_ID = vo.colorId;
			return dto;
		}
		
		public static function getDateTimeOfGoogleCandarEvent(date:Date):GoogleCalendarEventDateTimeObject
		{
			df.setDateTimePattern(ConstantSettings.GOOGLE_CALENDAR_TIMESTAMP_FORMAT);
			
			var gcdtVO:GoogleCalendarEventDateTimeObject = new GoogleCalendarEventDateTimeObject();
			gcdtVO.DATE_TIME = StringUtil.substitute(df.format(date), 
						TimeZoneUtil.getTimeZoneSign(), 
						zeroPad(TimeZoneUtil.getTimeZoneHours(), 2), 
						zeroPad(TimeZoneUtil.getTimeZoneMinutes(), 2));
			gcdtVO.TIME_ZONE = ClientSettings.GOOGLE_CALENDAR_TIME_ZONE; // || "Europe/Kiev";
			return gcdtVO;
		}
		
		private static function zeroPad(number:int, width:int):String {
			var ret:String = "" + number;
			while (ret.length < width)
				ret = "0" + ret;
			return ret;
		}
		
	}
}