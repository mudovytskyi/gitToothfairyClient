package modules.AdminModule.icons
{
	public class IconsAdmin
	{
		[Bindable]
		[Embed(source="modules/AdminModule/icons/logIn.png")]
		public static var LOGIN_15:Class;
		[Bindable]
		[Embed(source="modules/AdminModule/icons/logOut.png")]
		public static var LOGOUT_15:Class;
		
	}
}