package modules.AdminModule.mvc.events
{
	import flash.events.Event;
	
	import mx.utils.StringUtil;
	
	public final class AdminProgramSettingsEvent extends Event
	{
		//---- ЗАПИТИ ----

		// инше
		public static const SET_PROGRAM_SETTINGS:String = "adminModule.SET_PROGRAM_SETTINGS";
		public static const GET_PROGRAM_SETTINGS:String = "adminModule.GET_PROGRAM_SETTINGS";
		public static const CLEAN_PROGRAM_SETTINGS:String = "adminModule.CLEAN_PROGRAM_SETTINGS";

		
		//---- РЕЗУЛЬТАТИ ----
//		public static const PROGRAM_SETTINGS_SET:String = "adminModule.PROGRAM_SETTINGS_SET";
//		public static const PROGRAM_SETTINGS_GOTTEN:String = "adminModule.PROGRAM_SETTINGS_GOTTEN";
//		public static const PROGRAM_SETTINGS_CLEANED:String = "adminModule.PROGRAM_SETTINGS_CLEANED";

		
		//---- ПОМИЛКИ  ----
//		public static const ERROR_SET_PROGRAM_SETTINGS:String = "adminModule.ERROR_SET_PROGRAM_SETTINGS";
//		public static const ERROR_GET_PROGRAM_SETTINGS:String = "adminModule.ERROR_GET_PROGRAM_SETTINGS";
//		public static const ERROR_CLEAN_PROGRAM_SETTINGS:String = "adminModule.ERROR_CLEAN_PROGRAM_SETTINGS";

		
		
		private var _vo:*;
		
		public function AdminProgramSettingsEvent(type:String, vo:*, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._vo = vo;
		}
		
		override public function clone():Event
		{
			return new AdminProgramSettingsEvent(this.type, this.vo, bubbles, cancelable);
		}
		
		override public function toString():String
		{
			return StringUtil.substitute("modules.AdminModule.mvc.events.AdminProgramSettingsEvent::{0}", this.type);
		}

		public function get vo():* {
			return _vo;
		}
	}
}