package modules.AdminModule.mvc.controllers
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.errors.IllegalOperationError;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import mx.utils.StringUtil;
	
	import modules.AdminModule.mvc.controllers.commands.ICommand;
	import modules.AdminModule.mvc.controllers.commands.UpdateProgramSettingsCommand;
	import modules.AdminModule.mvc.events.AdminProgramSettingsEvent;
	
	
	//----------------------------------
	//
	// Class : AdminProgramSettingsController.as
	//
	//----------------------------------
	/** @extends: EventDispatcher
	 *   @implements:
	 *   @use:
	 *
	 *   @author: mykolaU
	 *   @date: Jul 5, 2019 @time: 4:54:40 PM */
	//----------------------------------
	public class AdminProgramSettingsController extends EventDispatcher
	{
		//----------------------------------
		// Variables
		//----------------------------------
		private static var _instance:AdminProgramSettingsController;
		private var _dictionaryCommands:Dictionary;
		private var _dictionaryHandlers:Dictionary;
		
		//----------------------------------
		//
		// Constructor
		//
		//----------------------------------
		public function AdminProgramSettingsController(enhancer:AdminProgramSettingsControllerEnhancer)
		{
			if (!enhancer) 	{
				throw new IllegalOperationError(
					StringUtil.substitute("Please, get instance of {0} class by using static method 'getInstance'", 
						getQualifiedClassName(AdminProgramSettingsController)));
			}
			_dictionaryCommands = new Dictionary(true);
			_initCommands();
		}
		
		
		//----------------------------------
		//
		// Public methods
		//
		//----------------------------------
		/**
		 * Прив'язати виконання події класу GoogleSyncEvent до призначення 
		 * @param eventType - Тип події
		 * @param commandClass - Клас призначення виконання
		 */		 
		public function mapClass(eventType:String, commandClass:Class):void
		{
			_dictionaryCommands[eventType] = commandClass;
			this.addEventListener(eventType, executerHandler);
		}
		
		public function getCommand(type:String):ICommand
		{
			return new _dictionaryCommands[type]();
		}
		
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function executerHandler(event:AdminProgramSettingsEvent):void
		{
			var cmd:ICommand = this.getCommand(event.type);
			cmd.command(event.vo);
		}
		
		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function _initCommands():void
		{
			mapClass(AdminProgramSettingsEvent.GET_PROGRAM_SETTINGS, UpdateProgramSettingsCommand);
//			mapClass(AdminProgramSettingsEvent.SET_PROGRAM_SETTINGS, ?);
//			mapClass(AdminProgramSettingsEvent.CLEAN_PROGRAM_SETTINGS, ?);

			
			// ПОМИЛКИ ЗАПИТІВ
		}
		
		//----------------------------------
		//
		// Static public methods
		//
		//----------------------------------
		public static function dispatchEvent(eventType:String, data:* = null):void
		{
			var event:AdminProgramSettingsEvent = new AdminProgramSettingsEvent(eventType, data);
			getInstance.dispatchEvent(event);
		}

		public static function addEventHandler(eventType:String, handler:Function, once:Boolean = true):void
		{
			getInstance.addEventListener(eventType, handler);
			
		}
		

		//----------------------------------
		//
		// Getters & Setters
		//	
		//----------------------------------
		public static function get getInstance():AdminProgramSettingsController
		{
			if (!_instance) {
				_instance = new AdminProgramSettingsController(new AdminProgramSettingsControllerEnhancer());
			}
			return _instance;
		}
	}
}

class AdminProgramSettingsControllerEnhancer {}