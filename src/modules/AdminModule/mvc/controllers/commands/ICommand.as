package modules.AdminModule.mvc.controllers.commands
{

	//----------------------------------
	//
	// Interface
	//
	//----------------------------------
	public interface ICommand
	{
		function command(vo:* = null):void;
	}
}