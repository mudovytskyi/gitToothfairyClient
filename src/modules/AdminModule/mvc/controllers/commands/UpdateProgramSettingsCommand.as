package modules.AdminModule.mvc.controllers.commands
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import services.ipClass;
	import services.adminservice.AdminService;
	
	import toothfairy.configurations.ConfigurationSettings;
	import toothfairy.settings.ClientSettings;
	import toothfairy.settings.ProgramSettings;
	
	import valueObjects.AdminModuleClientSetting;
	
	//----------------------------------
	//
	// Class : UpdateProgramSettingsCommand.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements: ICommand
	 *   @use:
	 *
	 *   @author: mykol
	 *   @date: Jul 9, 2019 @time: 9:57:38 AM */
	//----------------------------------
	public class UpdateProgramSettingsCommand implements ICommand
	{
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		public function command(vo:* = null):void
		{
			if (vo is ArrayCollection && vo.length > 0) {
				var service:AdminService = new AdminService();
				service.endpoint = ipClass.endpointhost;
				
				var responder:CallResponder = new CallResponder();
				responder.addEventListener(ResultEvent.RESULT, onResultEvent);
				responder.addEventListener(FaultEvent.FAULT, onFaultEvent);
			
				var settings:ArrayCollection = new ArrayCollection();
				var setting:AdminModuleClientSetting = new AdminModuleClientSetting();

				for each (var changeName:String in vo) {
					setting = new AdminModuleClientSetting();
					setting.SETTINGNAME = changeName;
					setting.SETTINGVALUE = ClientSettings.settingToString(changeName);
					settings.addItem(setting);
				}
				
				responder.token = service.setClientSettings(settings);
			}
		}
		
		
		//----------------------------------
		//
		// Event handlers
		//
		//----------------------------------
		protected function onFaultEvent(event:FaultEvent):void
		{
			clean(event.currentTarget as IEventDispatcher);
		}
		
		protected function onResultEvent(event:ResultEvent):void
		{
			clean(event.currentTarget as IEventDispatcher);
		}

		
		//----------------------------------
		//
		// Private methods
		//
		//----------------------------------
		private function clean(listener:IEventDispatcher):void
		{
			listener.removeEventListener(ResultEvent.RESULT, onResultEvent);		
			listener.removeEventListener(FaultEvent.FAULT, onFaultEvent);
			listener = null;
		}
	}
}