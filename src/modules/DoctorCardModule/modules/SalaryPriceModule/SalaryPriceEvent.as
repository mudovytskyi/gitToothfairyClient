package modules.DoctorCardModule.modules.SalaryPriceModule
{
	import flash.events.Event;
	
	import valueObjects.ChiefSalaryModule_SalaryProc;
	
	public class SalaryPriceEvent extends Event
	{
		
		public static const SALARY_CHANGED:String = "salaryPriceChanged";
		
		
		public var salaryProcObject:ChiefSalaryModule_SalaryProc;
		
		public function SalaryPriceEvent(type:String, salaryProcObject:ChiefSalaryModule_SalaryProc=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.salaryProcObject = salaryProcObject;
		}
		
		public override function clone():Event
		{
			return new SalaryPriceEvent(type, salaryProcObject);
		}
	}
}