package modules.dispancer
{
	import flash.events.Event;
	
	public class DispancerEvent extends Event
	{
		
		public static const DELETE_TASK:String = "dispancerDeleteTaskEvent";
		public static const UPD_COMMENT_TASK:String = "dispancerUpdateCommentTaskEvent";
		public static const UPD_DATE_TASK:String = "dispancerUpdateDateaskEvent";
		public static const UPD_TYPE_TASK:String = "dispancerUpdateTypeEvent";
		public static const UPD_SMS_TASK:String = "dispancerUpdateSMSEvent";
		public static const FAULT:String = "faultTaskEvent";
		
		public var patient_id:String;
		public var task_id:String;
		public var task_comment:String;
		public var task_date:Date;
		public var task_type_fk:String;
		public var task_smsstatus:Boolean;
		
		public function DispancerEvent(type:String, patient_id:String=null, task_id:String=null, task_comment:String=null, task_date:Date=null, task_type_fk:String=null, task_smsstatus:Boolean=false, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.patient_id = patient_id;
			this.task_id = task_id;
			this.task_comment = task_comment;
			this.task_date = task_date;
			this.task_type_fk = task_type_fk;
			this.task_smsstatus = task_smsstatus;
		}
		
		public override function clone():Event
		{
			return new DispancerEvent(type, patient_id, task_id, task_comment, task_date, task_type_fk, task_smsstatus);
		}
	}
}