package modules.dispancer.components.renderers
{
	[Bindable]
	public class DispancerIcons
	{
		[Embed(source="modules/dispancer/icons/notvisitedIcon20.png")]
		public static var nodeVisitedIcon:Class;

		[Embed(source="modules/dispancer/icons/visitedIcon20.png")]
		public static var visitedIcon:Class;
		
		[Embed(source="modules/dispancer/icons/task_green.png")]
		public static var task_green:Class;
		[Embed(source="modules/dispancer/icons/task_red.png")]
		public static var task_red:Class;
		
		
		[Embed(source="modules/dispancer/icons/notnoticedIcon20.png")]
		public static var noticed_not:Class;
		[Embed(source="modules/dispancer/icons/noticedIcon20.png")]
		public static var noticed:Class;
		
		[Embed(source="modules/dispancer/icons/updateIcon.png")]
		public static var updateCommentIcon:Class;
		
		[Embed(source="modules/AccountModule/accountflow/renderers/icons/deleteIcon.png")]
		public static var deleteIcon:Class;
		
	}
}