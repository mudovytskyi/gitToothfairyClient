package modules.dispancer.components.events
{
	import flash.events.Event;
	
	import valueObjects.AccountModule_FlowObject;

	
	public class RefreshEvent extends Event
	{
		
		public var taskId:String;
		
		public function RefreshEvent(type:String, taskId:String)
		{
			super(type);
			this.taskId = taskId;
		}
		
		public override function clone():Event
		{
			return new RefreshEvent(type, taskId);
		}
	}
}