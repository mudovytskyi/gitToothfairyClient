package modules.TreatmentManagerModule.components.savegroup.events
{
	import flash.events.Event;
	
	public class SaveEvent extends Event
	{
		public static const SAVE_EVENT:String = "saveEvent";
		
		public var authorObject:*;
		
		public function SaveEvent(type:String, authorObject:*)
		{
			super(type);
			this.authorObject = authorObject;
		}
		
		override public function clone():Event
		{
			return new SaveEvent(type, authorObject)
		}
	}
}