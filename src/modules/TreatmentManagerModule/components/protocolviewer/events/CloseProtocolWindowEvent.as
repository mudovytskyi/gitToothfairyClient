package modules.TreatmentManagerModule.components.protocolviewer.events
{
	import flash.events.Event;
	
	import valueObjects.TreatmentManagerModuleProtocol;
	
	public class CloseProtocolWindowEvent extends Event
	{
		public static const DELETE_EVENT:String = "deleteProtocolCloseEvent";
		public static const RELOAD_EVENT:String = "reloadProtocolCloseEvent";
		
		public var deletedProtocol:TreatmentManagerModuleProtocol;
		
		public function CloseProtocolWindowEvent(type:String, deletedProtocol:TreatmentManagerModuleProtocol, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.deletedProtocol = deletedProtocol;
		}
		
		override public function clone():Event
		{
			return new CloseProtocolWindowEvent(type, deletedProtocol, bubbles, cancelable)
		}
	}
}