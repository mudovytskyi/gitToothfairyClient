package modules.TreatmentManagerModule.components.protocolviewer.events
{
	import flash.events.Event;
	
	public class PreventFlagEvent extends Event
	{
		public static const PREVENTROLLFLAG_EVENT:String = "preventRollFlagEvent";
		
		public var preventFlag:Boolean;
		
		public function PreventFlagEvent(type:String, preventFlag:Boolean=true, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.preventFlag = preventFlag;
		}
		
		override public function clone():Event
		{
			return new PreventFlagEvent(type, preventFlag, bubbles, cancelable)
		}
	}
}