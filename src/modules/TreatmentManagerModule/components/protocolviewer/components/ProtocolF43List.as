package modules.TreatmentManagerModule.components.protocolviewer.components
{
	import spark.components.List;
	
	public class ProtocolF43List extends List
	{
		[Bindable]
		public var protocolId:String;
		
		[Bindable]
		public var verticalScrollPositionCurrent:Number;
		
		public function ProtocolF43List()
		{
			super();
		}
	}
}