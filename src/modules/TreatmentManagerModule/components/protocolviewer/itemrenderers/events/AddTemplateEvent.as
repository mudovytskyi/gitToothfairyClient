package modules.TreatmentManagerModule.components.protocolviewer.itemrenderers.events
{
	import flash.events.Event;
	
	import valueObjects.TreatmentManagerModuleTemplate;
	
	public class AddTemplateEvent extends Event
	{
		public static const ADD_EVENT:String = "addTemplateEvent";
		
		public var addeddTemplate:TreatmentManagerModuleTemplate;
		
		public function AddTemplateEvent(type:String, addeddTemplate:TreatmentManagerModuleTemplate, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.addeddTemplate = addeddTemplate;
		}
		override public function clone():Event
		{
			return new AddTemplateEvent(type, addeddTemplate, bubbles, cancelable)
		}
	}
}