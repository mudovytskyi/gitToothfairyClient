package modules.TreatmentManagerModule.components.protocolviewer.itemrenderers.events
{
	import flash.events.Event;
	
	import valueObjects.TreatmentManagerModuleTemplate;
	
	public class DeleteTemplateEvent extends Event
	{
		public static const DELETE_EVENT:String = "deleteTemplateEvent";
		
		public var deletedTemplate:TreatmentManagerModuleTemplate;
		
		public function DeleteTemplateEvent(type:String, deletedTemplate:TreatmentManagerModuleTemplate, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.deletedTemplate = deletedTemplate;
		}
		override public function clone():Event
		{
			return new DeleteTemplateEvent(type, deletedTemplate, bubbles, cancelable)
		}
	}
}