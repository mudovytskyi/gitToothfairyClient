package modules.TreatmentManagerModule.events
{
	import flash.events.Event;
	
	public class ProtocolTreatmentManagerEvent extends Event
	{
		
		public static const FILTER_EVENT:String = "doFilterEvent";
		
		
		public var protocolTypeFilter:int = -100;
		public var f43TypeFilter:int = -100;
		
		public function ProtocolTreatmentManagerEvent(protocolTypeFilter:int, f43TypeFilter:int, type:String=FILTER_EVENT, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.protocolTypeFilter = protocolTypeFilter;
			this.f43TypeFilter = f43TypeFilter;
		}
		
		public override function clone():Event
		{
			return new ProtocolTreatmentManagerEvent(protocolTypeFilter, f43TypeFilter, type, bubbles, cancelable);
		}
	}
}