package modules.AccountModule.consts
{
	public final class AccountOperationTypes
	{
		/*
			0 - операции по пациентам
			1 - операции по балансу снятие/внесение средств
			2 - операции по зп
			3 - расходы по процедурам
			4 - операции по складу
			5 - списание долга
			6 - оплата передплатної картки
		*/
		public static const OPERATION_PATIENTS:int = 0;
		public static const OPERATION_BALANCE:int = 1;
		public static const OPERATION_SALARY:int = 2;
		public static const OPERATION_PROCEDURE_EXPENSES:int = 3;
		public static const OPERATION_WAREHOUSE:int = 4;
		public static const OPERATION_DEBT_CANCELATION:int = 5;
		public static const OPERATION_SUBSCRIPTION_CARDS:int = 6;
	}
}