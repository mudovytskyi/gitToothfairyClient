package modules.AccountModule.account.statics
{
	import valueObjects.AccountModule_PatientObject;

	[Bindable]
	public class AccountSelectedPatient
	{
		public static var patient:AccountModule_PatientObject;
		
		
		public static function clear():void
		{
			patient = null;
		}
	}
}