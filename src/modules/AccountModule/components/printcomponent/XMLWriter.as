package modules.AccountModule.components.printcomponent
{
	import com.adobe.fiber.runtime.lib.StringFunc;
	
	import components.MoneyToString;
	import components.controls.StimulsoftReportViewer;
	import components.date.DateFormater;
	import components.date.ToolsForFirebird;
	
	import flash.display.DisplayObject;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.SharedObject;
	import flash.xml.XMLDocument;
	
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.rpc.xml.SimpleXMLEncoder;
	
	import valueObjects.AccountModule_PrintAccountInfo;
	
	public class XMLWriter
	{
		private var parent:DisplayObject = null;
		public function XMLWriter(reportData:Object, parent:DisplayObject)
		{
			this.parent = parent;
			if(reportData is AccountModule_PrintAccountInfo){
				createXMLAccount(reportData as AccountModule_PrintAccountInfo);
			}
		}
		
		private function createXMLAccount(data:AccountModule_PrintAccountInfo):void{
			var df:DateFormatter = new DateFormatter();
			
			var xmlDoc:XMLDocument = new XMLDocument();
			var xmlEncoder:SimpleXMLEncoder = new SimpleXMLEncoder(xmlDoc);
			var obj:Object = new Object();
			
			obj.CLINIC_ADDRESS = data.clinic_address == null ? "" : data.clinic_address;
			obj.CLINIC_BANKCURRENTACCOUNT = data.clinic_bankcurrentaccount == null ? "" : data.clinic_bankcurrentaccount;
			obj.CLINIC_BANKMFO = data.clinic_bankmfo == null ? "" : data.clinic_bankmfo;
			obj.CLINIC_BANKNAME = data.clinic_bankname == null ? "" : data.clinic_bankname;
			obj.CLINIC_CHIEFACCOUNT = data.clinic_chiefaccountant == null ? "" : data.clinic_chiefaccountant;
			obj.CLINIC_CODE = data.clinic_code == null ? "" : data.clinic_code;
			obj.CLINIC_DIRECTOR = data.clinic_director == null ? "" : data.clinic_director;
			obj.CLINIC_EMAIL = data.clinic_email == null ? "" : data.clinic_email;
			obj.CLINIC_FAX = data.clinic_fax == null ? "" : data.clinic_fax;
			obj.CLINIC_LEGALADDRESS = data.clinic_legaladdress == null ? "" : data.clinic_legaladdress;
			obj.CLINIC_LEGALNAME = data.clinic_legalname == null ? "" : data.clinic_legalname;
			obj.CLINIC_LICENSEENDDATE = data.clinic_licenseenddate == null ? "" : data.clinic_licenseenddate;
			obj.CLINIC_LICENSEISSUED = data.clinic_licenseissued == null ? "" : data.clinic_licenseissued;
			obj.CLINIC_LICENSENUMBER = data.clinic_licensenumber == null ? "" : data.clinic_licensenumber;
			obj.CLINIC_LICENSESERIES = data.clinic_licenseseries == null ? "" : data.clinic_licenseseries;
			obj.CLINIC_LICENSESTARTDATE = data.clinic_licensestartdate == null ? "" : data.clinic_licensestartdate;
			obj.CLINIC_NAME = data.clinic_name == null ? "" : data.clinic_name;
			obj.CLINIC_PHONE = data.clinic_phone == null ? "" : data.clinic_phone;
			obj.CLINIC_SITENAME = data.clinic_sitename == null ? "" : data.clinic_sitename;
			obj.DISCOUNT_AMOUNT = data.discount_amount.toFixed(2); ///   
			obj.PAID_SUM = data.paid_sum.toFixed(2);
			obj.INVOICE_DISCOUNT_PERCENT = data.invoice_discount_percent == 0 ? "" : data.invoice_discount_percent.toFixed(2)+"%";
			obj.DOCTOR_SHORTNAME = data.doctor_shortname == null ? "" : data.doctor_shortname;
			df.formatString = "DD/MM/YYYY";
			obj.INVOICE_DATE_FORMAT_DDMMYYYY = df.format(ToolsForFirebird.stringFirebirdDateToFlexDate(data.invoice_date));
			obj.INVOICE_DATE_FORMAT_DDMONTHYYYY = DateFormater.dateToGenitiveCase(ToolsForFirebird.stringFirebirdDateToFlexDate(data.invoice_date));
			obj.INVOICE_NUMBER = data.invoice_number == null ? "" : data.invoice_number;
			obj.PATIENT_SHORT_NAME = data.patient_shortname == null ? "" : data.patient_shortname;
			obj.SUM = data.sum.toFixed(2); 
			obj.WORD_SUM = MoneyToString.toWords(Number(obj.PAID_SUM)); // replaced from SUM to PAID_SUM
			obj.SUMOUTDISCOUNT = data.sumoutdiscount.toFixed(2);
			obj.WORD_SUMOUTDISCOUNT = MoneyToString.toWords(Number(obj.SUMOUTDISCOUNT));
			obj.PDV = "";
			obj.SUM_WITH_PDV = ((Number(data.sumoutdiscount.toFixed(2))) - Number(obj.DISCOUNT_AMOUNT)).toFixed(2);
			obj.WORD_SUM_WITH_PDV = MoneyToString.toWords(Number(obj.SUM_WITH_PDV));
			obj.SUMOUTDISCOUNT_WITH_PDV = (Number(obj.SUM_WITH_PDV) + Number(obj.PDV)).toFixed(2);
			obj.WORD_SUMOUTDISCOUNT_WITH_PDV = MoneyToString.toWords(Number(obj.SUMOUTDISCOUNT_WITH_PDV));
			if(ClientSettings.includePDVInInvoice){
				obj.SUM_WITH_PDV = ((Number(data.sumoutdiscount.toFixed(2))) - Number(obj.DISCOUNT_AMOUNT)).toFixed(2);
				obj.WORD_SUM_WITH_PDV = MoneyToString.toWords(Number(obj.SUM_WITH_PDV));
				obj.PDV = (Number(obj.SUM_WITH_PDV)*0.2).toFixed(2)
				obj.SUMOUTDISCOUNT_WITH_PDV = (Number(obj.SUM_WITH_PDV) + Number(obj.PDV)).toFixed(2);
				obj.WORD_SUMOUTDISCOUNT_WITH_PDV = MoneyToString.toWords(Number(obj.SUMOUTDISCOUNT_WITH_PDV));
			}
			obj.PHONEANDFAX = "";
			if(data.clinic_fax == null && data.clinic_phone != null){
				obj.PHONEANDFAX = "тел. "+data.clinic_phone;
			}if(data.clinic_fax != null && data.clinic_phone == null){
				obj.PHONEANDFAX = "факс "+data.clinic_fax;
			}if(data.clinic_fax != null && data.clinic_phone != null){
				obj.PHONEANDFAX = "тел. "+data.clinic_phone + " факс "+data.clinic_fax;
			}
			
			var qName:QName = new QName("ClinicAndInvoiceInfo");
			xmlEncoder.encodeValue(obj, qName, xmlDoc);
			
			obj = null;
			var qNameProc:QName = new QName("Procedures");
			for(var i:int = 0; i < data.procedures.length; i++){
				obj = data.procedures[i];
				obj.SUMMWITHDISCOUNT = (Number(obj.SUMMWITHDISCOUNT)).toFixed(2); 
				xmlEncoder.encodeValue(obj, qNameProc, xmlDoc);
			}
			
			
			var xml: XML = new XML("<AccountModule>"+xmlDoc.toString()+"</AccountModule>");
			var str: String = xml.toXMLString();
			str = StringFunc.replaceAllNoCase(str, "&lt;", "<");
			str = StringFunc.replaceAllNoCase(str, "&gt;", ">");
			
			var fileToSave: File = File.applicationDirectory;
			if(!data.is_cashpayment){
				fileToSave.nativePath = fileToSave.nativePath + File.separator+"reports"+File.separator+"Invoices"+File.separator+"CashOrder"+File.separator+"CashOrder.xml";
			}else{
				fileToSave.nativePath = fileToSave.nativePath + File.separator+"reports"+File.separator+"Invoices"+File.separator+"Invoice"+File.separator+"Invoice.xml";
			}
			
			var stream: FileStream = new FileStream();
			stream.open(fileToSave, FileMode.WRITE);
			stream.writeMultiByte(str, "utf-8");
			stream.close();
			
			var stimulsoftReportViewer:StimulsoftReportViewer = new StimulsoftReportViewer();
			if(!data.is_cashpayment){
				stimulsoftReportViewer.reportFile = "reports"+File.separator+"Invoices"+File.separator+"CashOrder"+File.separator+"CashOrder.mrt";
			}else{
				stimulsoftReportViewer.reportFile = "reports"+File.separator+"Invoices"+File.separator+"Invoice"+File.separator+"Invoice.mrt";
			}
			PopUpManager.addPopUp(stimulsoftReportViewer, parent, true);
			
		}
	}
}