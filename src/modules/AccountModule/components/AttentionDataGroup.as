package modules.AccountModule.components
{
	import spark.components.DataGroup;
	import spark.components.List;
	
	public class AttentionDataGroup extends List
	{
		
		[bindable]
		public var isEnableAttention:Boolean = true;
		
		public function AttentionDataGroup()
		{
			super();
		}
	}
}