package modules.AccountModule.events
{
	import flash.events.Event;
	
	public class AccountEvent extends Event
	{
		
		public static const SHOWPATIENT_ACCOUNTHISTORY:String = "showPatientAccountHistory";
		
		
		public var patientId:String;
		
		public function AccountEvent(type:String, patientId:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.patientId = patientId;
		}
		
		public override function clone():Event
		{
			return new AccountEvent(type, patientId);
		}
	}
}