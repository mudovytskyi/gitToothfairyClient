package modules.AccountModule.accountflow.components.createinvoicewindow.events
{
	import flash.events.Event;

	
	public class ClosePrintInvoiceEvent extends Event
	{
		public static const CLOSE_PRINTINVOICE_EVENT:String = "closePrintInvoiceEvent";
		
		public var invoice_id:Number;
		public var order_id:Number;
		
		public function ClosePrintInvoiceEvent(type:String, patient_id:Number=Number.NaN, order_id:Number=Number.NaN)
		{
			super(type);
			this.invoice_id = patient_id;
			this.order_id = order_id;
		}
		
		public override function clone():Event
		{
			return new ClosePrintInvoiceEvent(type, invoice_id, order_id);
		}
	}
}