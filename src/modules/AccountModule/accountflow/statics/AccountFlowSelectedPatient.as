package modules.AccountModule.accountflow.statics
{
	import valueObjects.AccountModule_PatientObject;

	[Bindable]
	public class AccountFlowSelectedPatient
	{
		public static var patient:AccountModule_PatientObject;
		
		
		public static function clear():void
		{
			patient = null;
		}
	}
}