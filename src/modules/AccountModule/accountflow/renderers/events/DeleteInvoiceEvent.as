package modules.AccountModule.accountflow.renderers.events
{
	import flash.events.Event;
	
	import valueObjects.AccountModule_FlowObject;

	
	public class DeleteInvoiceEvent extends Event
	{
		public static const DELETE_INVOICE_EVENT:String = "deleteInvoiceEvent";
		
		public var invoice:AccountModule_FlowObject;
		
		public function DeleteInvoiceEvent(type:String, invoice:AccountModule_FlowObject)
		{
			super(type);
			this.invoice = invoice;
		}
		
		public override function clone():Event
		{
			return new DeleteInvoiceEvent(type, invoice);
		}
	}
}