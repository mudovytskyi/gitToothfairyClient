package modules.AccountModule.accountflow.renderers.events
{
	import flash.events.Event;
	
	import valueObjects.AccountModule_FlowObject;
	
	public class ReceiptPrintEvent extends Event
	{
		public static const PRINT_RECEIPT_EVENT:String = "printReceiptEvent";
		
		public var invoice:AccountModule_FlowObject;
		
		public function ReceiptPrintEvent(type:String, invoice:AccountModule_FlowObject)
		{
			super(type);
			this.invoice = invoice;
		}
		
		public override function clone():Event
		{
			return new ReceiptPrintEvent(type, invoice);
		}
		
	}
}