package modules.AccountModule.accountflow.renderers.events
{
	import flash.events.Event;

	
	public class RendererEvent extends Event
	{
		public static const SHOW_CASHHISTORY_EVENT:String = "showCashHistoryEvent";
		public static const SHOW_INVOICEHISTORY_EVENT:String = "showInvoiceHistory";
		public static const NAVIGATE_TO_PATIENT:String = "navigateToPatient";
		
		public var patientId:String;
		
		public function RendererEvent(type:String, patientId:String)
		{
			super(type);
			this.patientId = patientId;
		}
		
		public override function clone():Event
		{
			return new RendererEvent(type, patientId);
		}
	}
}