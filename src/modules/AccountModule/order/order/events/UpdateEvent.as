package modules.AccountModule.order.order.events
{
	import flash.events.Event;
	
	public class UpdateEvent extends Event
	{
		public static const UPDATE_ORDER:String = "clickButtonEvent";
		
		public function UpdateEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new UpdateEvent(type, bubbles, cancelable);
		}
	}
}