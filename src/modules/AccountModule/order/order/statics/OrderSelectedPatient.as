package modules.AccountModule.order.order.statics
{
	import valueObjects.OrderModulePatient;

	[Bindable]
	public class OrderSelectedPatient
	{
		public static var patient:OrderModulePatient
		
		public static function clear():void{
			patient=null;
		}
	}
}