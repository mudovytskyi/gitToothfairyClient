package modules.testmodule.components
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	import spark.collections.Sort;
	import spark.collections.SortField;

	[Bindable]
	public class DGSort
	{
		public static function arrayCollectionSort(ar:ArrayCollection,field:String):void 
		{
			var s:Sort = new Sort();
			s.compareFunction = SectionCompareFunction(field);
			ar.sort = s;
			ar.refresh();
		} 
		
		private static function SectionCompareFunction(field:String):Function
		{
			return function(a:Object, b:Object, fields:Array=null):int
			{
				var arA:Array=(a[field]).split('.'); //разбиваем на массив подпунктов
				var arB:Array=(b[field]).split('.'); //разбиваем на массив подпунктов
				var i:int=0;
				while(parseInt(arA[i])==parseInt(arB[i])) //пока значения подпунктов равны наращиваем счетчик
				{ 
					i++;
					if(i>(arA.length-1)||i>(arB.length-1)) 	//если счётчик за пределами одного из массивов
					{
						if(arA.length==arB.length) //если глубины подпунктов одинаковые
							return ObjectUtil.numericCompare(a['id'],b['id']); //сортируем по id
						return ObjectUtil.numericCompare(arA.length,arB.length);	//сортируем по field
					}
								
						
				}
				
				return ObjectUtil.numericCompare(parseInt(arA[i]),parseInt(arB[i])); //сортируем по field

			}
		}	
		
	}
}