package modules
{
	public class EndpointObject
	{
		public var endpoint:String;
		public var port:String;
		
		public function getEndpoint():String{
			var result:String = "http://"+this.endpoint;
			if(this.port != null && this.port != ''){
				result += ":"+this.port;
			}
			result += "/toothfairy/gateway.php";
			return result;
		}
	}
}