package modules.PriceListModule.assets
{
	

	public class AssetsPriceList
	{
		[Bindable]
		[Embed(source="modules/PriceListModule/assets/defaultLeafIcon.png")]
		public static var defaultLeafIcon:Class;
		[Bindable]
		[Embed(source="modules/PriceListModule/assets/folderClosedIcon.png")]
		public static var folderClosedIcon:Class;
		[Bindable]
		[Embed(source="modules/PriceListModule/assets/folderOpenIcon.png")]
		public static var folderOpenIcon:Class;
		
	}
}