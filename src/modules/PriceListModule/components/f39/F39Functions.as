package modules.PriceListModule.components.f39
{
	import mx.collections.ArrayCollection;
	
	import valueObjects.Form039Module_F39_Record;
	import valueObjects.Form039Module_F39_Val;

	public class F39Functions
	{
		
		public static function unSelectWithChildren(item:Form039Module_F39_Record):void
		{
			item.SELECTED=false;
			if(item.TYPE>0)
			{
				for each (var child:Form039Module_F39_Record in item.CHILDREN) 
				{
					unSelectWithChildren(child);
				}
				
			}
		}
		
		public static function fillF39Fields(items:ArrayCollection, vals:ArrayCollection):ArrayCollection
		{
			for each (var val:Form039Module_F39_Val in vals) 
			{
				for each (var item:Form039Module_F39_Record in items) 
				{
					if(item.ID==val.ID)
					{
						item.SELECTED=!val.UNSIGNED;
						item.OLDSELECTED=!val.UNSIGNED;
						break;
					}
					else if(item.TYPE>0)
					{
						if(selectWithChildren(val,item))
						{			
							break;
						}
					}
				}
			}
			return items;
	
		}
		
		public static function selectWithChildren(val:Form039Module_F39_Val, rec:Form039Module_F39_Record):Boolean
		{
				for each (var item:Form039Module_F39_Record in rec.CHILDREN) 
				{
					if(item.ID==val.ID)
					{
						item.SELECTED=!val.UNSIGNED;
						item.OLDSELECTED=!val.UNSIGNED;
						return true;
					}
					else if(item.TYPE>0)
					{
						if(selectWithChildren(val,item))
						{
							return true;
						}
					}
				}
				return false;
		}
		
		public static function getF39Vals(items:ArrayCollection):ArrayCollection
		{
			var vals:ArrayCollection=new ArrayCollection
			var val:Form039Module_F39_Val;
			for each (var item:Form039Module_F39_Record in items) 
			{
				if(item.SELECTED!=item.OLDSELECTED)
				{
					val=new Form039Module_F39_Val();
					val.ID=item.ID;
					val.UNSIGNED=!item.SELECTED;
					vals.addItem(val);
				}
				if(item.TYPE>0)
				{
					vals.addAll(getF39Vals(item.CHILDREN));
				}
			}
			return vals;
			
		}
	}
}