package modules.PriceListModule.components.f39.components
{
	import spark.components.DataGroup;
	import spark.components.RadioButtonGroup;
	
	public class DataRadioGroup extends DataGroup
	{
		[Bindable]
		public var radioGroup:RadioButtonGroup;
		
		public function DataRadioGroup()
		{
			super();
			radioGroup=new RadioButtonGroup;
			
		}
	}
}