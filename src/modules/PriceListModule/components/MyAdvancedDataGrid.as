package modules.PriceListModule.components
{
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.AdvancedDataGrid;
	import mx.controls.listClasses.IListItemRenderer;
	
	public class MyAdvancedDataGrid extends AdvancedDataGrid
	{
		public function MyAdvancedDataGrid()
		{
			super();
		}
		
		public function getMousePosition(event:MouseEvent):Point
		{
			var r: IListItemRenderer;							
			
			r = mouseEventToItemRenderer(event);	
			return itemRendererToIndices(r);
		}
	}
}