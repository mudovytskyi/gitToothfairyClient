package modules.MultiCalendar.events
{
	import flash.events.Event;
	
	public class CalendarContextEvent extends Event
	{
		public static const PASTE:String = "pasteEvent";
		
		public var date:Date;
		
		public function CalendarContextEvent(type:String, date:Date, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.date = date;
		}
		
		override public function clone():Event
		{
			return new CalendarContextEvent(type, date, bubbles, cancelable)
		}
		
	}
}