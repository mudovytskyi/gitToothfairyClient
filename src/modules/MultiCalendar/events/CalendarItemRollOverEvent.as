package modules.MultiCalendar.events
{
	import flash.events.Event;
	
	
	public class CalendarItemRollOverEvent extends Event
	{
		public static const ITEM_ROLLOVER_EVENT:String = "calendarItemRollOverEvent";
		
		public var item:*;
		
		public function CalendarItemRollOverEvent(type:String = 'calendarItemRollOverEvent', item:* = null, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable)
			this.item = item;
		}
		
		override public function clone():Event
		{
			return new CalendarItemRollOverEvent(type, item, bubbles, cancelable)
		}
		
	}
}