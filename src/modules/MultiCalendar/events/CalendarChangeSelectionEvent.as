package modules.MultiCalendar.events
{
	import flash.events.Event;
	
	public class CalendarChangeSelectionEvent extends Event
	{
		public static const CHANGE_SELECTION_EVENT:String = "calendarChangeSelectionEvent";
		
		public var selectedItemsCount:int = 0;
		
		public function CalendarChangeSelectionEvent(type:String, selectedItemsCount:int)
		{
			super(type);
			this.selectedItemsCount = selectedItemsCount;
		}
		
		override public function clone():Event
		{
			return new CalendarChangeSelectionEvent(type, selectedItemsCount)
		}
		
	}
}