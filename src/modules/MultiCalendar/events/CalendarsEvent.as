package modules.MultiCalendar.events
{
	import flash.events.Event;
	
	public class CalendarsEvent extends Event
	{
		public static const SYNC_ON_TASK_UPDATE:String = "startSyncEvent";
		public static const SYNC_ON_TASK_VISIT_UPDATE:String = "startSyncVisitStatusEvent";
		
		
		public var syncDate:Date;
		public var addedTaskDocId:String;
		public var taskId:String;
		public var newInVisitStatus:String;
		public var newOutVisitStatus:String;
		
		public function CalendarsEvent(type:String, syncDate:Date, addedTaskDocId:String, taskId:String = null, newInVisitStatus:String = null, newOutVisitStatus:String = null, 
									   bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.syncDate = syncDate;
			this.addedTaskDocId = addedTaskDocId;
			this.taskId = taskId;
			this.newInVisitStatus = newInVisitStatus;
			this.newOutVisitStatus = newOutVisitStatus;
		}
		
		override public function clone():Event
		{
			return new CalendarsEvent(type, syncDate, addedTaskDocId, taskId, newInVisitStatus, newOutVisitStatus,  bubbles, cancelable)
		}
		
	}
}