package modules.MultiCalendar.events
{
	import flash.events.Event;
	
	
	public class CalendarItemRollOutEvent extends Event
	{
		public static const ITEM_ROLLOUT_EVENT:String = "calendarItemRollOutEvent";
		
		public var item:*;
		
		public function CalendarItemRollOutEvent(type:String = 'calendarItemRollOutEvent', item:* = null, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable)
			this.item = item;
		}
		
		override public function clone():Event
		{
			return new CalendarItemRollOutEvent(type, item, bubbles, cancelable)
		}
		
	}
}