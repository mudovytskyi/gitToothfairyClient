package modules.MultiCalendar
{
	import flash.display.Graphics;
	import flash.display.Shape;
	
	import mx.core.EdgeMetrics;
	import mx.graphics.RectangularDropShadow;
	import mx.utils.ColorUtil;
	import mx.utils.GraphicsUtil;
	
	import spark.components.Image;
	
	import ilog.calendar.CalendarItem;
	import ilog.calendar.CalendarItemVerticalRenderer;
	
	import toothfairy.configurations.ConfigurationSettings;
	import toothfairy.settings.colors.ColorConstants;
	
	/**
	 * This item renderer extends the vertical item renderer to add a shadow and an dary background under
	 * the start time label.
	 */   
	public class CustomVerticalRenderer extends CalendarItemVerticalRenderer
	{
		public function CustomVerticalRenderer() {
			super();                    
			
		}
		
		private var _shadow:RectangularDropShadow; 
		private var _smsIcon:Image;
		private var fillColor:uint;
		private var outlineColor:uint;
		
		override protected function createChildren():void {
			
			super.createChildren();
			
			if (_shadow == null) {      
				_shadow = new RectangularDropShadow();
				_shadow.brRadius = _shadow.blRadius = _shadow.tlRadius = _shadow.trRadius = getStyle("cornerRadius");
				_shadow.color = 0x404040;       
				_shadow.distance = 4;
			}
			
			if (drawing == null) {
				drawing = new Shape();
				addChild(drawing);
				drawing.x = 0;
				drawing.y = 0;
			}
			
			
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if (data == null) {
				return;
			}
			
			drawing.graphics.clear();
			
			
			var bm:EdgeMetrics = borderMetrics;
			var w:Number = unscaledWidth-bm.left-bm.right;
			var h:Number = unscaledHeight-bm.top-bm.bottom;
			var g:Graphics = drawing.graphics;
			
			if (startTimeLabel.visible) {
				
				
				var radius:Number = getStyle("cornerRadius");
				var height:Number = startTimeLabel.y + startTimeLabel.height; 
				
				var theDate:CalendarItem = CalendarItem(data);
				var color:uint = data.calendarControl.getItemColor(data);
				color = ColorUtil.adjustBrightness(color, -40);
				
				g.beginFill(color);
				
				if (radius == 0) {
					
					g.drawRect(bm.left, bm.top, w, height);
					
					if (endTimeLabel.visible) {            
						g.drawRect(bm.left, unscaledHeight-bm.bottom-height, w, height);
					}
					
				} else {                           
					GraphicsUtil.drawRoundRectComplex(g, bm.left, bm.top, w, height, radius, radius, 0, 0);
					
					if (endTimeLabel.visible) {
						GraphicsUtil.drawRoundRectComplex(g, bm.left, unscaledHeight-bm.bottom-height, w, height, 0, 0, radius, radius);
					}        
				}
				
				g.endFill();
				
				// Якщо не синхронізовано, то цього не видно
				if (ConfigurationSettings.isGoogleCalendarSynced) {
					g.lineStyle(2, outlineColor);
					g.beginFill(fillColor);
					g.drawRoundRect(bm.left + w - 17, 5, 10, 10, 2, 2);
					g.endFill();
				}
				
			} else {
				// Якщо не синхронізовано, то цього не видно
				if (ConfigurationSettings.isGoogleCalendarSynced) {
					g.lineStyle(2, outlineColor);
					g.beginFill(fillColor);
					g.drawRoundRect(bm.left + w - 25,  bm.top + (h - 15) >> 1, 15, 15, 3, 3);
					g.endFill();
				}
			}
			
			

			setChildIndex(drawing, 1); // 0 is the border
			
			graphics.clear();
			
			_shadow.drawShadow(graphics, 0, 0, unscaledWidth, unscaledHeight);
			
		}
		
		override public function set data(value:Object):void
		{
			if (ConfigurationSettings.isGoogleCalendarSynced && value is CalendarItem && value.data is XML) {
				// Синхронізована подія чи ні
				var isSynced:Boolean = Boolean(parseInt(value.data.@isgooglecalendareventsynced)); // Фактично можна прирівняти до рядка == '1'

					if (isSynced) {
					fillColor = ColorConstants.MALACHITE_COLOR;
					outlineColor = ColorConstants.WHITE_COLOR;
				} else {
					fillColor = ColorConstants.DUSTY_GRAY_COLOR;
					outlineColor = ColorConstants.ALTO_COLOR;
				}
			}
			
			super.data = value;
		}
	}
}
