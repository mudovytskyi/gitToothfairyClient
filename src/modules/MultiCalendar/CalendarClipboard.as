package modules.MultiCalendar
{
	public final class CalendarClipboard
	{
		private static var _instance:CalendarClipboard;
		public static function get instance():CalendarClipboard 
		{
			if (_instance == null) 
			{
				_instance = new CalendarClipboard();
			}
			return _instance;
		}
		
		private var eventData:XML;
		
		public function CalendarClipboard()
		{
			if (_instance)
				throw new Error("Static usage only");
		}
		
		public function put(eventData:XML):void 
		{
			if (eventData != null)
			{
				this.eventData = eventData.copy();
			}
			else
			{
				clear();
			}
		}
		
		public function getData():XML
		{
			return eventData;
		}
		
		public function clear():void
		{
			eventData = null;
		}
		
		public function get empty():Boolean 
		{
			return eventData == null;
		}
	}
}