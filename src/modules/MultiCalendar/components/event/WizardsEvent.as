package modules.MultiCalendar.components.event
{
	import flash.events.Event;
	
	public class WizardsEvent extends Event
	{
		public static const CLOSE_AFTERADDED_EVENT:String = "closeAfterAddedTaskWizard";
		
		public var addedTaskDate:Date;
		public var addedTaskDocId:String;
		
		public function WizardsEvent(type:String, addedTaskDate:Date, addedTaskDocId:String)
		{
			super(type);
			this.addedTaskDate = addedTaskDate;
			this.addedTaskDocId = addedTaskDocId;
		}
		
		override public function clone():Event
		{
			return new WizardsEvent(type, addedTaskDate, addedTaskDocId)
		}
		
	}
}