package modules.dictionaries.ultrasonic {
	internal class UltrasonicTemplateConsts {
		//---- Найменше і найбільше значення номеру стовпчика для дослідження у звіті----
		public static const MIN_ROW_INDEX:int = 3;
		public static const MAX_ROW_INDEX:int = 22;
	}
}