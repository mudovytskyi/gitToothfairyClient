package modules.chiefmap.com.modestmaps.mapproviders.google
{
	import modules.chiefmap.com.modestmaps.core.Coordinate;
	import modules.chiefmap.com.modestmaps.core.painter.GoogleTilePainter;
	import modules.chiefmap.com.modestmaps.core.painter.ITilePainter;
	import modules.chiefmap.com.modestmaps.core.painter.ITilePainterOverride;
	import modules.chiefmap.com.modestmaps.mapproviders.AbstractMapProvider;
	import modules.chiefmap.com.modestmaps.mapproviders.IMapProvider;

	public class GoogleMapProvider extends AbstractMapProvider implements IMapProvider, ITilePainterOverride
	{
		protected var tilePainter:GoogleTilePainter;
		
		public function GoogleMapProvider(tilePainter:GoogleTilePainter)
		{
			this.tilePainter = tilePainter
		}

		public function getTilePainter():ITilePainter
		{
			return tilePainter;
		}
		
		public function toString():String
		{
			return tilePainter.toString();
		}
		
		public function getTileUrls(coord:Coordinate):Array
		{
			return [];
		}
	}
}