package modules.chiefmap.com.modestmaps.core.painter
{
	public interface ITilePainterOverride
	{
		function getTilePainter():ITilePainter;
	}
}