package modules.WarehouseModule.store.orderwindow
{
	import flash.events.Event;
	
	public class OrderStoreEvent extends Event
	{
		
		public static const DELETED_ITEM:String = "deletedItemStoreFromOrder";
		
		
		public var itemID:String;
		
		public function OrderStoreEvent(type:String, itemID:String, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.itemID = itemID;
		}
		
		public override function clone():Event
		{
			return new OrderStoreEvent(type, itemID);
		}
	}
}