package modules.WarehouseModule.components
{
	import flash.events.KeyboardEvent;
	
	import mx.controls.DateField;
	import mx.controls.TextInput;
	
	import toothfairy.configurations.ConstantSettings;
	
	public class DateField3 extends DateField
	{
		
		public static function keyUpHandler(event:KeyboardEvent):void
		{
			var seporatorF:int = 2;
			var seporatorS:int = 5;
			if(ConstantSettings.TOOTHFAIRY_DATA_TYPE == 3)
			{
				seporatorF = 4;
				seporatorS = 7;
			}
			
			if(event.charCode>47 && event.charCode<58)
			{
				var dfInput:DateField3 = event.currentTarget as DateField3
				if(dfInput !=null && (dfInput.text.length==seporatorF || dfInput.text.length==seporatorS))
				{
					dfInput.text+=ConstantSettings.TOOTHFAIRY_DATA_SEPARATOR;
					dfInput.textField.setSelection(dfInput.text.length,dfInput.text.length);
				}
			}
		}
		
		public function DateField3()
		{
			super();
		}
		
		
		public function get textField():TextInput
		{
			return textInput as TextInput;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number,
													  unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			textInput.setActualSize(unscaledWidth, unscaledHeight);
		}
	}
}