package modules.WarehouseModule.components.events
{
	import flash.events.Event;
	
	public class FarmRequiredEvent extends Event
	{
		public static var FARM_REQUIRED_CHANGE:String = 'requiredFarmChange';

		public var farmId:String;
		public var newValue:Boolean;
		public function FarmRequiredEvent(type:String, farmId:String, newValue:Boolean, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.farmId=farmId;
			this.newValue=newValue;
		}
		
		public override function clone():Event
		{
			return new FarmRequiredEvent(type, farmId, newValue, bubbles, cancelable);
		}
	}
}