package modules.WarehouseModule.components.events
{
	import flash.events.Event;
	
	import valueObjects.WarehouseModuleSupplier;
	
	public class WarehouseModuleSupplierEvent extends Event
	{
		public static var SUPPLIER_ADD:String = 'AddSupplier';
		
		public var supplier:WarehouseModuleSupplier;
		public function WarehouseModuleSupplierEvent(type:String, supplier:WarehouseModuleSupplier=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.supplier = supplier;
		}
		
		public override function clone():Event
		{
			return new WarehouseModuleSupplierEvent(type, supplier, bubbles, cancelable);
		}
	}
}