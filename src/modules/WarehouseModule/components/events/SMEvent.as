package modules.WarehouseModule.components.events
{
	import flash.events.Event;
	
	public class SMEvent extends Event
	{
		public static var SM_FAULT:String = 'SMFault';
		public static var SM_RESULT:String = 'SMResult';
		
		public function SMEvent(type:String='SMFault', bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new SMEvent(type, bubbles, cancelable);
		}
	}
}