package modules.WarehouseModule.assets
{
	

	public class AssetsWarehouse
	{
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/prevNavigationArrowIcon.png")]
		public static var prevNavigationArrowIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/nextNavigationArrowIcon.png")]
		public static var nextNavigationArrowIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/refreshIcon.png")]
		public static var refreshIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/refreshIconRed.png")]
		public static var refreshIconRed:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/addIcon.png")]
		public static var addIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/addFarm.png")]
		public static var addFarm:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/addFarmMini.png")]
		public static var addFarmMini:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/editIconMini.png")]
		public static var editIconMini:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/deleteIconMini.png")]
		public static var deleteIconMini:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/editIcon.png")]
		public static var editIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/deleteIcon.png")]
		public static var deleteIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/inArrow.png")]
		public static var inArrow:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/outArrow.png")]
		public static var outArrow:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/insideArrow.png")]
		public static var insideArrow:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/FARM_NAME_ICON.png")]
		public static var FARM_NAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/FARMPRICE_PRICE_ICON.png")]
		public static var FARMPRICE_PRICE_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/FARMDOCUMENT_DOCNUMBER_ICON.png")]
		public static var FARMDOCUMENT_DOCNUMBER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/FARMDOCUMENT_DOCTIMESTAMP_ICON.png")]
		public static var FARMDOCUMENT_DOCTIMESTAMP_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/SUPPLIER_NAME_ICON.png")]
		public static var SUPPLIER_NAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/RECEIVER_ICON.png")]
		public static var RECEIVER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/SENDER_ICON.png")]
		public static var SENDER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/DOCTOR_SHORTNAME_ICON.png")]
		public static var DOCTOR_SHORTNAME_ICON:Class; 
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/PATIENT_SHORTNAME_ICON.png")]
		public static var PATIENT_SHORTNAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/HEALTHPROC_NAME_ICON.png")]
		public static var HEALTHPROC_NAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/FARM_NOMENCLATUREARTICLENUMBER_ICON.png")]
		public static var FARM_NOMENCLATUREARTICLENUMBER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/olap/TOTAL_ICON.png")]
		public static var TOTAL_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/inArrowMini.png")]
		public static var inArrowMini:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/outArrowMini.png")]
		public static var outArrowMini:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/insideArrowMini.png")]
		public static var insideArrowMini:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/resArrowMini.png")]
		public static var resArrowMini:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/exportIcon.png")]
		public static var exportIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/totalEnableIcon.png")]
		public static var totalEnableIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/expandAllIcon.png")]
		public static var expandAllIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/collapseAllIcon.png")]
		public static var collapseAllIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/saveIcon.png")]
		public static var saveIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/formatODSIcon.png")]
		public static var formatodsIcon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/formatXLSXIcon.png")]
		public static var formatxlsxIcon:Class;
		
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/balanceFromCommon.png")]
		public static var balanceFromCommon:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/balanceFromCurrent.png")]
		public static var balanceFromCurrent:Class;
		
		[Bindable]
		[Embed(source="modules/WarehouseModule/assets/balanceFromNew.png")]
		public static var balanceFromNew:Class;
		
		public static function iconByName(name:String):Class
		{
			var field:String=name+"_ICON";
			return AssetsWarehouse[field] as Class;
			
		}
		
		public static function iconByFormat(format:String):Class
		{
			var name:String='format'+format+"Icon";
			return AssetsWarehouse[name] as Class;
			
		}
	}
}