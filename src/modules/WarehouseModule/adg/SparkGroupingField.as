package modules.WarehouseModule.adg
{
	import mx.collections.ArrayCollection;

	public class SparkGroupingField
	{	
		public var dataField:String;	
		
		public var labelField:String;	
			
		public var coalesceValue:String;	
		
		public var groupSummaryFields:ArrayCollection;
		
		public var isNumericLabelField:Boolean=false;
		
		public var isNumericDataField:Boolean=false;
		
		public function SparkGroupingField()
		{
			
		}

	}
}