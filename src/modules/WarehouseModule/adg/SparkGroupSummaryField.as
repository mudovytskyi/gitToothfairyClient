package modules.WarehouseModule.adg
{
	public class SparkGroupSummaryField
	{
		public var dataField:String;
		
		public var labelField:String;
		
		public var summaryFunction:Function;
		
		public var count:int=0;
		
		public var value:Number=0;
		
		public var startValue:Number=0;
		
		public function SparkGroupSummaryField(dataField:String=null, labelField:String=null, summaryFunction:Function=null)
		{
			this.dataField=dataField;
			this.labelField=labelField;
			this.summaryFunction=summaryFunction;
		}
		
		public static function SUM(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Number
		{
			gsf.value=roundTo(gsf.value+rec[gsf.dataField],2);
			return gsf.value;
		}
		
		public static function AVG(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Number
		{
			gsf.count++;
			return SUM(obj, rec, gsf)/gsf.count;
		}
		
		public static function MIN(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Number
		{
			if(gsf.value>rec[gsf.dataField])
			{
				gsf.value=rec[gsf.dataField];
			}
			return gsf.value;
		}
		
		public static function MAX(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Number
		{
			if(gsf.value<rec[gsf.dataField])
			{
				gsf.value=rec[gsf.dataField];
			}
			return gsf.value;
		}
		
		public function calculate(obj:Object, rec:Object):Number
		{
			if(obj.hasOwnProperty(this.labelField)==false)
			{
				value=startValue;
				count=0;
			}
			if(isNaN(rec[this.dataField]))
			{
				return value;
			}
			return this.summaryFunction(obj, rec, this);
		}
		
		public static function roundTo(value:Number, accuracy:Number): Number
		{
			return Math.round(value*Math.pow(10,accuracy)) / Math.pow(10,accuracy);
		}
		
	}
}