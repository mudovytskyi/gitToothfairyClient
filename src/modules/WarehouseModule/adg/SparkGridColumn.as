package modules.WarehouseModule.adg
{
	import spark.components.gridClasses.GridColumn;
	
	public class SparkGridColumn extends GridColumn
	{
		
		public static var TYPE_STRING:String='string';
		
		public static var TYPE_NUMERIC:String='numeric';
		
		public static var TYPE_DATE:String='date';
		
		[Bindable]
		public var isGrouping:Boolean=false;
		
		[Inspectable(category="General", enumeration="string,numeric,date",
                                     defaultValue="string")]
		public var type:String=TYPE_STRING;
		
		private var _enabled:Boolean=true;
		
		[Bindable]
		private var oldVisible:Boolean;
		
		public function SparkGridColumn(columnName:String=null)
		{
			super(columnName);
		}
		
		[Bindable]
		public function get enabled():Boolean
		{
			return _enabled;
		}

		public function set enabled(value:Boolean):void
		{
			_enabled = value;
			if(value)
			{
				visible = oldVisible;
			}
			else
			{
				oldVisible = visible;
				visible = false;
			}
		}

	}
}