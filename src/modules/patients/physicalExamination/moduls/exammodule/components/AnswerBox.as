package modules.patients.physicalExamination.moduls.exammodule.components
{
	import flash.events.Event;
	
	import mx.collections.XMLListCollection;
	import mx.events.FlexEvent;
	
	import spark.components.CheckBox;
	import spark.components.DropDownList;
	import spark.components.FormItem;
	import spark.components.HGroup;
	import spark.components.Label;
	import spark.components.RadioButton;
	import spark.components.RadioButtonGroup;
	import spark.components.TextInput;
	import spark.components.VGroup;
	import spark.events.IndexChangeEvent;
	
	import valueObjects.NHPhysicalExamination_ExamQuestion;
	
	[Event(name="change", type="flash.events.Event")]
	
	public class AnswerBox extends VGroup
	{	
		private var questionXML:XML;
		private var sourceXML:XML;
		private var resultsXML:XML;
		private var type:String;
		public var isPatology:Boolean=false;
		private var questionID:String;
		
		public function AnswerBox()
		{
			super();
		}
		
		protected function dropdownlist_change_function(ddlist:DropDownList):Function
		{
			return function(event:IndexChangeEvent):void
			{
				isPatology=ddlist.selectedItem.@isPatology==1;
				if(resultsXML.toXMLString()=="") 
					resultsXML=new XML("<results><result>"+ddlist.selectedItem+"</result></results>");
				else
					resultsXML.setChildren(<result>{ddlist.selectedItem.toString()}</result>);
//					resultsXML.*=ddlist.selectedItem; // вставляє <answer>...</answer>, коли потрібен <result>...</result>
				dispatchEvent(new Event("change"));
			}
		}
		
		
		protected function radiobutton_change_function(rbgroup:RadioButtonGroup):Function
		{
			return function(event:FlexEvent):void
			{
				
				for each (var ans:XML in sourceXML.answer) {
					if(ans == rbgroup.selectedValue) {
						isPatology = (ans.@isPatology == 1);
					}
				}
				
				if(resultsXML.toXMLString()=="") 
					resultsXML=new XML("<results><result>"+rbgroup.selectedValue+"</result></results>");
				else
					resultsXML.result=rbgroup.selectedValue;
				dispatchEvent(new Event("change"));
			}
		}
		
		protected function checkbox_change_function():Function
		{
			return function(event:FlexEvent):void
			{
				isPatology=false;
				resultsXML=new XML;
				for (var i:int = 0; i < numChildren; i++) 
				{
					if((getChildAt(i) as CheckBox).selected)
						for each (var ans:XML in sourceXML.answer)
					{
						if(ans == (getChildAt(i) as CheckBox).label)
						{
							if(ans.@isPatology==1) isPatology=true;
							if(resultsXML.toXMLString()=="") 
								resultsXML=new XML("<results><result>"+(getChildAt(i) as CheckBox).label+"</result></results>");
							else
								resultsXML.appendChild((getChildAt(i) as CheckBox).label);
						}
					}					
				}
				
				dispatchEvent(new Event("change"));
			}
		}
		
		public function refresh(data:NHPhysicalExamination_ExamQuestion):void
		{
			if (!data) return;
			
			if(this.resultsXML==new XML(data.result))
			{
				return;
			}
			
			this.resultsXML=new XML(data.result);
			
			var isCorrect:Boolean=this.type==resultsXML.attribute("type");
			var child:XML;
			
			
			switch(sourceXML.attribute("type").toString())
			{
				case 'DropDownList':
				{
					var aDropDownList:DropDownList=DropDownList(this.getElementAt(0));
					
					if(resultsXML.toString()==""||!isCorrect)
					{
						aDropDownList.selectedIndex=-1;
						return;
					}
						
					for (var i:int = 0; i < aDropDownList.dataProvider.length; i++) 
					{
						if(aDropDownList.dataProvider[i].toString()==resultsXML.children()[0].toString()) 
						{
							aDropDownList.selectedIndex=i;
						}
					}
					break;
				}
					
				case 'RadioButton':
				{
					
					var aRadioButtonGroup:RadioButtonGroup=RadioButton(this.getElementAt(0)).group;
					if(resultsXML.toString()==""||!isCorrect)
					{
						aRadioButtonGroup.selectedValue=null;
						return;
					}
					if (isCorrect) 
					{
						aRadioButtonGroup.selectedValue=resultsXML.children()[0];
					}
					break;
				}
					
				case 'CheckBox':
				{
					var aCheckBox:CheckBox;
					for (var j:int = 0; j < this.numElements; j++) 
					{
						aCheckBox=CheckBox(this.getElementAt(j));
						if (isCorrect&&resultsXML.children().contains(aCheckBox.label)) 
						{
							aCheckBox.selected=true;
						}
						else
						{
							aCheckBox.selected=false;
						}
					}
					break;
				}
					
				case 'TextInput':
				{
					var aTextInput:TextInput=TextInput(this.getElementAt(0));					
					if (isCorrect) 
					{
						aTextInput.text=resultsXML.children()[0].toString();
					}
					else
					{
						aTextInput.text="";
					}
					break;
				}
					
				case 'LabelsTextInput':
				{
					var aTxtInput:TextInput;
					var aLabel:Label;
					var aVgroup:VGroup=VGroup(this.getElementAt(0));
					var aHGroup:HGroup;
					for (var k:int = 0; k < aVgroup.numElements; k++) 
					{
						aHGroup=HGroup(aVgroup.getElementAt(k));
						aTxtInput=TextInput(aHGroup.getElementAt(1));
						aLabel=Label(aHGroup.getElementAt(0));
						if (isCorrect)
						{
							aTxtInput.text=resultsXML.children().(attribute('label')==aLabel.text);
						}
						else
						{
							aTxtInput.text="";
						}
					}
					
					break;
				}	
			}
		}
		
		public function build(data:NHPhysicalExamination_ExamQuestion):void
		{	
			this.sourceXML=new XML(data.answers.replace('\n', ''));
			this.questionXML = <question/>
			questionXML.@id = data.id;
			questionXML.@number = data.number;
			questionXML.@question = data.question;
			questionXML.@template_id = data.template_id;
			questionXML.@isenabled = data.isenabled;
			
			this.resultsXML=new XML(data.result);
			this.questionID=data.id;
			
			var isCorrect:Boolean = sourceXML.attribute("type") == resultsXML.attribute("type");
			var child:XML;
			
			switch(sourceXML.attribute("type").toString())
			{
				case 'DropDownList':
				{
					var aDropDownList:DropDownList=new DropDownList();
					aDropDownList.dataProvider=new XMLListCollection(new XMLList(sourceXML.children()));
					aDropDownList.addEventListener(IndexChangeEvent.CHANGE, dropdownlist_change_function(aDropDownList));
					if (isCorrect) 
					{
						for (var i:int = 0; i < aDropDownList.dataProvider.length; i++) 
						{
							if(aDropDownList.dataProvider[i].toString()==resultsXML.children()[0].toString()) 
							{
								aDropDownList.selectedIndex=i;
							}
						}
					}
					
					this.addElement(aDropDownList);
					this.type="DropDownList";
					break;
				}
					
				case 'RadioButton':
				{
					var aRadioButtonGroup:RadioButtonGroup=new RadioButtonGroup();
					aRadioButtonGroup.addEventListener(FlexEvent.VALUE_COMMIT,radiobutton_change_function(aRadioButtonGroup));
					for each (child in sourceXML.children()) 
					{
						var aRadioButton:RadioButton=new RadioButton;
						aRadioButton.group=aRadioButtonGroup;
						aRadioButton.label=child.toString();
						this.addElement(aRadioButton);
					}
					this.type="RadioButton";
					if (isCorrect) 
					{
						aRadioButtonGroup.selectedValue=resultsXML.children()[0];
					}
					break;
				}
					
				case 'CheckBox':
				{
					
					for each (child in sourceXML.children()) 
					{
						var aCheckBox:CheckBox=new CheckBox;
						aCheckBox.addEventListener(FlexEvent.UPDATE_COMPLETE, checkbox_change_function(), false, 0, true)
						aCheckBox.label=child.toString();
						if (isCorrect&&resultsXML.children().contains(aCheckBox.label)) 
						{
							aCheckBox.selected=true;
						}
						this.addElement(aCheckBox);
					}
					this.type="CheckBox"
					break;
				}
					
				case 'TextInput':
				{
					
					var aTextInput:TextInput=new TextInput;
					aTextInput.height=50;
					aTextInput.width=500;
					
					if (isCorrect) 
					{
						aTextInput.text=resultsXML.children()[0].toString();
					}
					this.addElement(aTextInput);
					this.type="TextInput"
					break;
				}
					
				case 'LabelsTextInput':
				{
					var aTxtInput:TextInput;
//					var aLabel:Label;
					var aFormItem:FormItem;
					var aHGroup:HGroup;
					var aVgroup:VGroup = new VGroup();
					aVgroup.percentWidth = 100;
					for each (child in sourceXML.children()) 
					{
						aTxtInput = new TextInput();
						aTxtInput.percentWidth = 100;
						aTxtInput.setStyle("fontWeight", "normal");
//						aLabel = new Label();
//						aHGroup = new HGroup();
//						aHGroup.percentWidth = 100;
						aFormItem = new FormItem();
						aFormItem.percentWidth = 100;
						aFormItem.label = child.toString();
						aFormItem.setStyle("fontWeight", "bold");
//						aLabel.text = child.toString();
						if (isCorrect) {
//							aTxtInput.text = resultsXML.children().(attribute('label') == aLabel.text);
							aTxtInput.text = resultsXML.children().(attribute('label') == aFormItem.label);
						}
//						aHGroup.addElement(aLabel);
						aFormItem.addElement(aTxtInput);
//						aHGroup.addElement(aTxtInput);
//						aHGroup.addElement(aTxtInput);
						aVgroup.addElement(aFormItem);
//						aVgroup.addElement(aHGroup);
					}
					this.addElement(aVgroup);
					this.type="LabelsTextInput"
					break;
				}
					
				default:
				{
					this.type="Section";
					break;
				}
					
			}
			
			
		}
		
		public function getTestResult():String
		{
			if (!this.numElements) {
				resultsXML = <results/>;
			} else {
				switch(this.type)
				{
					case 'TextInput':
					{				
						resultsXML=new XML("<results><result>"+(this.getChildAt(0) as TextInput).text+"</result></results>");
						break;
					}
					case 'LabelsTextInput':
					{
						var res:String='';
						var parentGroup:VGroup = this.getChildAt(0) as VGroup;
						
						for (var j:int = 0; j < parentGroup.numChildren; j++) {
							var fi:FormItem = parentGroup.getChildAt(j) as FormItem;
							var ti:TextInput = fi.getElementAt(0) as TextInput;
							if (ti.text != '')
								res+='<result label="' + fi.label + '">' + ti.text+'</result>'
						}
//						for (var j:int = 0; j < VGroup(this.getChildAt(0)).numChildren; j++) 
//						{
//							if((TextInput((VGroup(this.getChildAt(0)).getChildAt(j) as HGroup).getChildAt(1))).text!='') res+='<result label="'+(Label((VGroup(this.getChildAt(0)).getChildAt(j) as HGroup).getChildAt(0))).text+'">'+(TextInput((VGroup(this.getChildAt(0)).getChildAt(j) as HGroup).getChildAt(1))).text+'</result>'
//						}
						resultsXML = new XML('<results>'+res+'</results>');
						break;
					}
					case 'CheckBox':
					{
						isPatology=false;
						resultsXML=new XML;
						for (var i:int = 0; i < numChildren; i++) 
						{
							if((getChildAt(i) as CheckBox).selected)
								for each (var ans:XML in sourceXML.answer)
							{
								if(ans == (getChildAt(i) as CheckBox).label)
								{
									if(ans.@isPatology==1) isPatology=true;
									if(resultsXML.toXMLString()=="") 
										resultsXML=new XML("<results><result>"+(getChildAt(i) as CheckBox).label+"</result></results>");
									else
										resultsXML.appendChild((getChildAt(i) as CheckBox).label);
								}
							}					
						}
						break;
					}
					case 'DropDownList': {
						var ddlist:DropDownList = DropDownList(this.getElementAt(0));
						resultsXML=new XML("<results><result>"+ddlist.selectedItem+"</result></results>");
						break;
					}
					case 'RadioButton': {
						var rbgroup:RadioButtonGroup = RadioButton(this.getElementAt(0)).group;
						resultsXML=new XML("<results><result>"+rbgroup.selectedValue+"</result></results>")
						break;
					}
				}			
			}
			
			resultsXML.@type=this.type;
			resultsXML.@id=this.questionID;
			if(this.isPatology) resultsXML.@isPatology=1 else delete resultsXML.@isPatology;
			return questionXML.copy().appendChild(this.sourceXML).appendChild(this.resultsXML).toXMLString();
		}
	}
}