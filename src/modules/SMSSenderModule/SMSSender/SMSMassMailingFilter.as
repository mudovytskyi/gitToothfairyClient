package modules.SMSSenderModule.SMSSender
{
	public class SMSMassMailingFilter
	{
		public static const MORE:int = 0;
		public static const MORE_EQUAL:int = 1;
		public static const LESS:int = 2;
		public static const LESS_EQUAL:int = 3;
		public static const EQUAL:int = 4;
		public static const LIKE:int = 5;
		public static const BETWEEN:int = 6;
		public static const IN:int = 7;
	}
}