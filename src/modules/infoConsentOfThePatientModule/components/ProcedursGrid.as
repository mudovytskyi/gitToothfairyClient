﻿package modules.infoConsentOfThePatientModule.components {
	
	import de.sbistram.controls.MatchDataGrid;
	import de.sbistram.utils.LogUtils;
	
	import flash.display.Sprite;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.ListEvent;
	import mx.logging.ILogger;
	import mx.resources.ResourceManager;
	import flash.net.SharedObject;
	
	[ResourceBundle("InfoConsentOfThePatientModule")]
	
	public class ProcedursGrid extends MatchDataGrid {
		
		private var _log:ILogger = LogUtils.getLogger(this);
		
		//
		// private constants
		//
		
		private static const DF_PROCNAME:String = "NAME";
		private static const DF_PROCTYPE:String	= "PROCTYPE";
		private static const DF_PROCPRICE:String	= "PRICE";
		
		private static const DF_ALL:Array = [DF_PROCNAME, DF_PROCTYPE, DF_PROCPRICE];
		
		/**
		 * Constructor
		 */
		public function ProcedursGrid() {
			super();
			ResourceManager.getInstance().localeChain = [ClientSettings.locale];
			columns = createColumns();
			decoratorFunction = decorator;
		}
		
		/**
		 * Create data for a combobox selection of one or all columns
		 * Add the 'All' entry for selecting all columns
		 * 
		 * @return Array of all columns headerText and dataField 
		 */	
		public function columnSelectProvider():Array {
			var provider:Array = [{ label:ResourceManager.getInstance().getString('InfoConsentOfThePatientModule', 'ProcedursGridVGroup_PDG_All'), dataField:null }];
			for (var i:int = 0; i < columns.length; i++) {
				provider.push({ label:columns[i].headerText, dataField:columns[i].dataField });
			}
			return provider;
		}
		
		/** 
		 * @see DataGridColumn#itemToLabel
		 * @return Array of DataGridColumn
		 */
		private function createColumns():Array {
			var columns:Array = [];
			var column:DataGridColumn;
			var df:String;
			
			// create all columns
			for (var i:int = 0; i < DF_ALL.length; i++) {
				column = new DataGridColumn();
				df = DF_ALL[i];
				
				// default values
				column.dataField = df;
				column.itemRenderer = _matchFactory;
				columns.push(column);
				
				// column specific values
				if (df == DF_PROCNAME) {
					column.headerText = ResourceManager.getInstance().getString('InfoConsentOfThePatientModule', 'NameDatagridColumn');
				} else if (df == DF_PROCTYPE) {
					column.headerText = ResourceManager.getInstance().getString('InfoConsentOfThePatientModule', 'TypeDatagridColumn');
					column.labelFunction = procTypeLabelFunction;
					column.width = 50;
				}  else if (df == DF_PROCPRICE) {
					column.headerText = ResourceManager.getInstance().getString('InfoConsentOfThePatientModule', 'PriceDatagridColumn');
					column.width = 50;
				} 
			}
			return columns;
		}
		
		private function procTypeLabelFunction(item:Object, column:DataGridColumn):String
		{
			if(item.PROCTYPE == 0)
				return resourceManager.getString('InfoConsentOfThePatientModule', 'therapy');
			if(item.PROCTYPE == 1)
				return resourceManager.getString('InfoConsentOfThePatientModule', 'ortoped');
			if(item.PROCTYPE == 2)
				return resourceManager.getString('InfoConsentOfThePatientModule', 'ortodont');
			if(item.PROCTYPE == 3)
				return resourceManager.getString('InfoConsentOfThePatientModule', 'childstom');
			else
				return resourceManager.getString('InfoConsentOfThePatientModule', 'other');
		}
		
		/**
		 * Decorator method to do some DataGrid decorator stuff:
		 * 
		 *  	- font: color, size, bold, italic, underline.
		 * 
		 * Note: Because the renderer is reused we have to set/reset the values!
		 * 		 And don't use the column index if dragging columns is enabled! Instead 
		 *       use the column like it's done here.
		 * 
		 * @param	r renderer for the current cell
		 * @param	w width of the current cell
		 * @param	h heigth of the current cell
		 */
		private function decorator(r:Object, w:Number, h:Number):void 
		{
			var column:DataGridColumn = columns[r.listData.columnIndex];
			var df:String = column.dataField;
			r.setStyle("fontWeight", df == DF_PROCTYPE ? "bold" : "normal");
		}
		
	}
}