package modules.components.AvatarComponent.AvatarComponentSettings
{
	import flash.media.Camera;
	import flash.net.SharedObject;
	public class AvatarSettingsInitializer
	{
		public static function SetDefaultSettings():void
		{
			if(Camera.getCamera())
			{
				if (SharedObject.getLocal("toothfairy").data["selectedWebCamera"] == null)
				{	
					SharedObject.getLocal("toothfairy").data["selectedWebCamera"] = "0";
				}
				else
				{
					if(Camera.getCamera(SharedObject.getLocal("toothfairy").data["selectedWebCamera"])==null)
					{
						SharedObject.getLocal("toothfairy").data["selectedWebCamera"] = "0";
					}
				}
			}
			else
			{
				SharedObject.getLocal("toothfairy").data["selectedWebCamera"] = "false";//non cameras
			}
			
		}
	}
}