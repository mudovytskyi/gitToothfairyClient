package modules.components
{
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.DataGrid;
	import mx.controls.listClasses.IListItemRenderer;
	
	public class MyDataGrid extends DataGrid
	{
		public function MyDataGrid()
		{
			super();
		}
		
		public function getMousePosition(event:MouseEvent):Point
		{
			var r:IListItemRenderer;							
			
			r=mouseEventToItemRenderer(event);	
			return itemRendererToIndices(r);
		}
	}
}