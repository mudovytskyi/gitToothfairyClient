package modules.reports.events
{
	import flash.events.Event;

	
	public class ReportPrintEvent extends Event
	{
		public static const PRINT_REPORT_EVENT:String = "printMozReportEvent";
		
		public static const F37_0:int = 370;
		public static const F37_1:int = 371;
		public static const F37_2:int = 372;
		public static const F39_2:int = 392;
		public static const F39_3:int = 393;
		public static const F39_4:int = 394;
		
		public var form:int;
		
		public var doctorId:String;
		
		public function ReportPrintEvent(doctorId:String, form:int, type:String = ReportPrintEvent.PRINT_REPORT_EVENT)
		{
			super(type);
			this.doctorId = doctorId;
			this.form = form;
		}
		
		public override function clone():Event
		{
			return new ReportPrintEvent(doctorId, form, type);
		}
	}
}