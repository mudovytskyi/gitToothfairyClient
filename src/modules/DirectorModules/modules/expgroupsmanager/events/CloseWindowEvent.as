package modules.DirectorModules.modules.groupsmanager.events
{
	import flash.events.Event;
	
	public class CloseWindowEvent extends Event
	{
		public static const REFRESH:String = "refreshEvent";
		
		public function CloseWindowEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}