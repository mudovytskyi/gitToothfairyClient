package modules.DirectorModules.modules.budgetmanager.events
{
	import flash.events.Event;
	
	import valueObjects.ChiefBudgetModule_BudgetData;
	
	public class BudgetGridEvent extends Event
	{
		public static const REFRESH:String = "refreshEvent";
		public static const DETALIZE_EXPENSE:String = "detalizeExpensesEvent";
		
		
		public var gridObject:ChiefBudgetModule_BudgetData;
		
		public function BudgetGridEvent(type:String, gridObject:ChiefBudgetModule_BudgetData=null, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.gridObject = gridObject;
		}
		
		
		public override function clone():Event
		{
			return new BudgetGridEvent(type, gridObject, bubbles, cancelable);
		}
	}
}