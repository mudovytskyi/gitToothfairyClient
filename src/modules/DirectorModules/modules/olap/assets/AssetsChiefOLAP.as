package modules.DirectorModules.modules.olap.assets
{
	

	public class AssetsChiefOLAP
	{
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/recordIcon.png")]
		public static var recordIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/prevNavigationArrowIcon.png")]
		public static var prevNavigationArrowIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/nextNavigationArrowIcon.png")]
		public static var nextNavigationArrowIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/refreshIcon.png")]
		public static var refreshIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/refreshIconRed.png")]
		public static var refreshIconRed:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/PATIENT_VILLAGEORCITY_ICON.png")]
		public static var PATIENT_VILLAGEORCITY_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/PATIENT_SEX_ICON.png")]
		public static var PATIENT_SEX_ICON:Class; 

		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/TREATMENTPROC_DAYCLOSE_ICON.png")]
		public static var TREATMENTPROC_DAYCLOSE_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/FARM_NAME_ICON.png")]
		public static var FARM_NAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/FARMPRICE_PRICE_ICON.png")]
		public static var FARMPRICE_PRICE_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/FARMDOCUMENT_DOCNUMBER_ICON.png")]
		public static var FARMDOCUMENT_DOCNUMBER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/TREATMENTPROC_NAME_ICON.png")]
		public static var TREATMENTPROC_NAME_ICON:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/TREATMENTPROC_HEALTHTYPE_ICON.png")]
		public static var TREATMENTPROC_HEALTHTYPE_ICON:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/DOCTOR_SHORTNAME_ICON.png")]
		public static var DOCTOR_SHORTNAME_ICON:Class; 
			
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/PATIENT_FULLNAME_ICON.png")]
		public static var PATIENT_FULLNAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/PATIENT_POPULATIONGROUPE_ICON.png")]
		public static var PATIENT_POPULATIONGROUPE_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/GROUP_DESCRIPTION_ICON.png")]
		public static var GROUP_DESCRIPTION_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/PATIENT_ISDISPENSARY.png")]
		public static var PATIENT_ISDISPENSARY_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/HEALTHPROC_NAME_ICON.png")]
		public static var HEALTHPROC_NAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/HEALTHPROC_PROCTYPE_ICON.png")]
		public static var HEALTHPROC_PROCTYPE_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/TREATMENTPROC_SHIFR_ICON.png")]
		public static var TREATMENTPROC_SHIFR_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/WHEREFROM_DESCRIPTION_ICON.png")]
		public static var WHEREFROM_DESCRIPTION_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/INVOICES_INVOICENUMBER_ICON.png")]
		public static var INVOICES_INVOICENUMBER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/ACCOUNTFLOW_ORDERNUMBER_ICON.png")]
		public static var ACCOUNTFLOW_ORDERNUMBER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/PATIENTSINSURANCE_POLICENUMBER_ICON.png")]
		public static var PATIENTSINSURANCE_POLICENUMBER_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/INSURANCECOMPANYS_NAME_ICON.png")]
		public static var INSURANCECOMPANYS_NAME_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/ACCOUNTFLOW_OPERATIONDAY_ICON.png")]
		public static var ACCOUNTFLOW_OPERATIONDAY_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/INVOICES_CREATE_DAY_ICON.png")]
		public static var INVOICES_CREATE_DAY_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/DOCTOR_SPECIALITY_ICON.png")]
		public static var DOCTOR_SPECIALITY_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/olap/TOTAL_ICON.png")]
		public static var TOTAL_ICON:Class; 
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/exportIcon.png")]
		public static var exportIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/totalEnableIcon.png")]
		public static var totalEnableIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/expandAllIcon.png")]
		public static var expandAllIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/collapseAllIcon.png")]
		public static var collapseAllIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/saveIcon.png")]
		public static var saveIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/formatODSIcon.png")]
		public static var formatodsIcon:Class;
		
		[Bindable]
		[Embed(source="modules/DirectorModules/modules/olap/assets/formatXLSXIcon.png")]
		public static var formatxlsxIcon:Class;
		
		

		
		public static function iconByName(name:String):Class
		{
			var field:String=name+"_ICON";
			return AssetsChiefOLAP[field] as Class;
			
		}
		
		public static function iconByFormat(format:String):Class
		{
			var name:String='format'+format+"Icon";
			return AssetsChiefOLAP[name] as Class;
			
		}
	}
}