package modules.DirectorModules.modules.olap.adg
{
	import mx.collections.ArrayCollection;

	public class SparkGroupingField
	{	
		[Bindable]
		public var dataField:String;	
		
		[Bindable]
		public var labelField:String;	
			
		[Bindable]
		public var coalesceValue:String;	
		
		public var groupSummaryFields:ArrayCollection;
		
		public var isNumericLabelField:Boolean=false;
		
		public var isNumericDataField:Boolean=false;
		
		public function SparkGroupingField()
		{
			
		}

	}
}