package modules.DirectorModules.modules.olap.adg
{
	import flash.sampler.startSampling;

	public class SparkGroupSummaryField
	{
		public var dataField:String;
		
		public var labelField:String;
		
		public var uniqueField:String;
		
		private var uniqueArray:Array;
		
		public var summaryFunction:Function;
		
		public var count:int=0;
		
		public var value:Object=0;
		
		public var summ:Number=0;
		
		public var startValue:Object=0;
		
		public static var TYPE_STRING:String='string';
		
		public static var TYPE_NUMERIC:String='numeric';
		
		[Inspectable(category="General", enumeration="string,numeric",
                                     defaultValue="numeric")]
		public var type:String=TYPE_NUMERIC;
		
		public function SparkGroupSummaryField(dataField:String=null, labelField:String=null, summaryFunction:Function=null)
		{
			this.dataField=dataField;
			this.labelField=labelField;
			this.summaryFunction=summaryFunction;
		}
		
		public static function SUM(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Object
		{
			gsf.value=SparkGroupSummaryField.roundTo(gsf.value+rec[gsf.dataField],2);
			return gsf.value;
		}
		
		public static function AVG(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Object
		{
			gsf.count++;
			gsf.summ=SparkGroupSummaryField.roundTo(gsf.summ+rec[gsf.dataField],2);
			gsf.value=SparkGroupSummaryField.roundTo(gsf.summ/gsf.count,2);
			return gsf.value;
		}

		public static function MIN(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Object
		{
			if(gsf.value>rec[gsf.dataField])
			{
				gsf.value=rec[gsf.dataField];
			}
			return gsf.value;
		}
		
		public static function MAX(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Object
		{
			if(gsf.value<rec[gsf.dataField])
			{
				gsf.value=rec[gsf.dataField];
			}
			return gsf.value;
		}
		public static function UNIQUE(obj:Object, rec:Object, gsf:SparkGroupSummaryField):Object
		{
			if(gsf.value==gsf.startValue)
			{
				gsf.value=rec[gsf.dataField];
			}
			else if(gsf.value==null||gsf.value!=rec[gsf.dataField])
			{
				gsf.value=null;
			}				
			return gsf.value;
		}
		public function calculate(obj:Object, rec:Object):Object
		{
			if(obj.hasOwnProperty(this.labelField)==false)
			{
				value=startValue;
				count=0;
				summ=0;
				uniqueArray=[];
			}

			if((this.type==TYPE_NUMERIC&&(isNaN(rec[this.dataField])))||uniqueArray.indexOf(this.uniqueField)!=-1)
			{
				return value;
			}
			if(this.uniqueField!=null)
			{
				if(uniqueArray.indexOf(rec[this.uniqueField])!=-1)
				{
					return value;
				}
				else
				{
					uniqueArray.push(rec[this.uniqueField]);
				}
			}
			
			return this.summaryFunction(obj, rec, this);
		}
		
		public static function roundTo(value:Number, accuracy:Number): Number
		{
			return Math.round(value*Math.pow(10,accuracy)) / Math.pow(10,accuracy);
		}
	}
}