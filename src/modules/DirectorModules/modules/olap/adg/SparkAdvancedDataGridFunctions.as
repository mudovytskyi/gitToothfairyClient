package modules.DirectorModules.modules.olap.adg
{
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.utils.ObjectUtil;
	
	public class SparkAdvancedDataGridFunctions
	{
		public static function getDataProvider(source:ArrayCollection, fields:ArrayCollection, totalField:SparkGroupingField=null, filter:Function=null):ArrayCollection
		{
			var res:ArrayCollection=new ArrayCollection();
			if(source==null||source.length==0)
			{
				return res;
			}
			var sort:Sort=new Sort;
			sort.fields=[];
			
			for each (var field:SparkGroupingField in fields) 
			{
				if(field.labelField!=field.dataField)
				{
					sort.fields.push(new SortField(field.labelField, true,false, field.isNumericLabelField));
				}
				sort.fields.push(new SortField(field.dataField, true,false, field.isNumericDataField));
			}
			
			source.sort=sort;
			source.refresh();
			
			var curLC:ArrayCollection;
			var ind:int;
			var totalObj:Object;
			if(totalField!=null)
			{
				totalObj=newSparkGroupingObject(null, totalField.dataField, null, totalField.labelField, totalField.coalesceValue);
			}
			for each (var rec:Object in source) 
			{
				curLC=res;
				for each (field in fields) 
				{
					ind=isLastItemHasField(rec,field.dataField,curLC);
					if(ind==-1)
					{
						ind=curLC.length;
						curLC.addItem(newSparkGroupingObject(rec[field.dataField], field.dataField, rec[field.labelField], field.labelField, field.coalesceValue));
					}
					
					for each (var gsf:SparkGroupSummaryField in field.groupSummaryFields) 
					{
						curLC.getItemAt(ind)[gsf.labelField]=gsf.calculate(curLC.getItemAt(ind),rec); 
					}
					curLC.getItemAt(ind).parrent=curLC;
					curLC=curLC.getItemAt(ind).children;
				}
				if(totalField!=null)
				{
					for each (var tgsf:SparkGroupSummaryField in totalField.groupSummaryFields) 
					{
						totalObj[tgsf.labelField]=tgsf.calculate(totalObj,rec); 
					}
				}
				if(filter==null)
				{
					curLC.addItem(rec);
				}
				else
				{
					if(filter(rec)==true)
					{
						curLC.addItem(rec);
					}
				}
			}
			if(totalField!=null)
			{
				res.addItem(totalObj);
			}
			return res;
		}
		
		public static function isLastItemHasField(val:Object,field:String,ac:ArrayCollection):int
		{
			if(ac.length>0)
			{
				var obj:Object=ac.getItemAt(ac.length-1);
				if(obj["data"]==val[field]||obj["data"]=="NA"&&val[field]==null)
				{
					return ac.length-1;
				}
			}
			return -1;
		}
		
		public static function newSparkGroupingObject(data:Object,dataField:String,label:Object,labelField:String,coalesceValue:String=''):Object
		{
			var sgo:Object=new Object;
			if(data==null)
			{
				sgo.data="NA";
			}
			else
			{
				sgo.data=String(data);
			}
			if(label==null)
			{
				sgo.label=coalesceValue;
			}
			else
			{
				sgo.label=String(label);
			}
			sgo.labelField=labelField;
			sgo.dataField=dataField;
			sgo.children=new ArrayCollection();
			return sgo;
		}
	}
}