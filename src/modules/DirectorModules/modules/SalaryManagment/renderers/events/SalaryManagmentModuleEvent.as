package modules.DirectorModules.modules.SalaryManagment.renderers.events
{
	import flash.events.Event;
	
	import valueObjects.ChiefSalaryModuleDoctorAwards;
	
	public class SalaryManagmentModuleEvent extends Event
	{
		public static const DELETE_AWARD_ITEM:String = 'SalaryManagmentModuleEvent_DELETE_AWARD_ITEM';
		public var closeDetail:int;
		public var awardItem:ChiefSalaryModuleDoctorAwards;
		
		public function SalaryManagmentModuleEvent(type:String, closeDetail:int, awardItem:ChiefSalaryModuleDoctorAwards,  bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.closeDetail = closeDetail;
			this.awardItem = awardItem;
		}
		
		
		public override function clone():Event
		{
			return new SalaryManagmentModuleEvent(type, closeDetail, awardItem, bubbles, cancelable);
		}
	}
}