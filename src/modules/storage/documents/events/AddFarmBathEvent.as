package modules.storage.documents.events
{
	import flash.events.Event;
	
	import valueObjects.Storage_FarmBath;
	
	public class AddFarmBathEvent extends Event
	{
		
		public static const EVENT_ADD:String = "AddFarmBathEvent";
		public static const EVENT_CANCEL:String = "CloseFarmBathEvent";
		
		public var farmBath:Storage_FarmBath;
		
		
		public function AddFarmBathEvent(farmBath:Storage_FarmBath, type:String=EVENT_ADD, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.farmBath = farmBath;
		}
		
		public override function clone():Event
		{
			return new AddFarmBathEvent(farmBath, type, bubbles, cancelable);
		}
	}
}