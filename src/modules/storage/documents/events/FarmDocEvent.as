package modules.storage.documents.events
{
	import flash.events.Event;
	
	import valueObjects.Storage_FarmDoc;
	
	public class FarmDocEvent extends Event
	{
		public static const EVENT_ADD:String = "SaveFarmDocEvent";
		public static const EVENT_CANCEL:String = "CloseFarmDocEvent";
		
		public var farmDoc:Storage_FarmDoc;
		public var isAccountflowDoc:Boolean;
		public var isCahspayment:int = 0;
		
		
		public function FarmDocEvent(farmDoc:Storage_FarmDoc, isAccountflowDoc:Boolean, isCahspayment:int, type:String=EVENT_ADD, bubbles:Boolean=true, cancelable:Boolean=true)
		{
			super(type, bubbles, cancelable);
			this.farmDoc = farmDoc;
			this.isAccountflowDoc = isAccountflowDoc;
			this.isCahspayment = isCahspayment;
		}
		
		public override function clone():Event
		{
			return new FarmDocEvent(farmDoc, isAccountflowDoc, isCahspayment, type, bubbles, cancelable);
		}
	}
}