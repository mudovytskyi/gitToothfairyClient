package modules.storage
{
	import mx.collections.ISummaryCalculator;
	import mx.collections.SummaryField2;
	
	public class FloatSummaryCalculator implements ISummaryCalculator
	{
		public static const OPERATION_SUM:String = "SUM";
		
		private var _summaryOperation:String;
		private var _precision:int;
		
		public function FloatSummaryCalculator(summaryOperation:String, precision:int = 3)
		{
			this._summaryOperation = summaryOperation;
			this._precision = precision;
		}
		
		public function calculateSummary(data:Object, field:SummaryField2, rowData:Object):void
		{
			var dataField:String = field.dataField;
			var value:Number = rowData[dataField];
			if (typeof(value) == "xml")
				value = Number(value.toString());
			
			switch (_summaryOperation)
			{
				case "SUM": if (!data.hasOwnProperty(dataField))
					data[dataField] = value ;
				else
					data[dataField] += value;
					break;
				case "MIN": if (!data.hasOwnProperty(dataField))
					data[dataField] = value;
				else
					data[dataField] =  data[dataField] < value ? data[dataField] : value;
					break;
				case "MAX": if (!data.hasOwnProperty(dataField))
					data[dataField] = value;
				else
					data[dataField] =  data[dataField] > value ? data[dataField] : value;
					break;
				case "COUNT": if (!data.hasOwnProperty(dataField))
				{
					data[dataField] = [rowData[dataField]];
					data[dataField + "Counter"] = 1;
				}
				else
				{
					data[dataField].push(rowData[dataField]);
					data[dataField + "Counter"] = data[dataField + "Counter"] + 1;
				}
					break;
				case "AVG": if (!data.hasOwnProperty(dataField))
				{
					data[dataField] = value;
					data[dataField + "Count"] = 1;
				}
				else
				{
					data[dataField] += value;
					data[dataField + "Count"] = data[dataField + "Count"] + 1;
				}
					break;
			}
		}
		
		public function calculateSummaryOfSummary(oldValue:Object, newValue:Object, field:SummaryField2):void
		{
			var p:String;
			switch (_summaryOperation)
			{
				case "AVG":
				case "SUM": for (p in newValue)
				{
					oldValue[p] += newValue[p];
				}
					break;
				case "MIN": for (p in newValue)
				{
					oldValue[p] = oldValue[p] < newValue[p] ? oldValue[p] : newValue[p];
				}
					break;
				case "MAX": for (p in newValue)
				{
					oldValue[p] = oldValue[p] > newValue[p] ? oldValue[p] : newValue[p];
				}
					break;
				case "COUNT": 	for (p in newValue)
				{
					if (oldValue[p] is Array)
						oldValue[p] = oldValue[p].concat(newValue[p]);
					else
						oldValue[p] += newValue[p];
				}
					break;
			}
		}
		
		public function returnSummary(data:Object, field:SummaryField2):Number
		{
			var summary:Number = 0;
			var dataField:String = field.dataField;
			switch (_summaryOperation)
			{
				case "SUM":
				case "MIN":
				case "MAX": summary = data[dataField];
					break;
				case "COUNT": 	summary = data[dataField + "Counter"];
					break;
				case "AVG": summary = data[dataField]/data[dataField + "Count"];
					break;
			}
			var factor:int = Math.pow(10, _precision);
			return Math.round(summary * factor) / factor;
		}
		
		public function returnSummaryOfSummary(oldValue:Object, field:SummaryField2):Number
		{
			var summary:Number = 0;
			var dataField:String = field.dataField;
			switch (_summaryOperation)
			{
				case "SUM":
				case "MIN":
				case "MAX": summary = oldValue[dataField];
					break;
				case "COUNT":   summary = oldValue[dataField + "Counter"];
					break;
				case "AVG": summary = oldValue[dataField]/oldValue[dataField + "Count"];
					break;
			}
			
			return summary;
		}
		
		public function summaryCalculationBegin(field:SummaryField2):Object
		{
			var dataField:String = field.dataField;
			var newObj:Object = {};
			switch (_summaryOperation)
			{
				case "SUM": newObj[dataField] = 0;
					break;
				case "MIN": newObj[dataField] = Number.MAX_VALUE;
					break;
				case "MAX": newObj[dataField] = -Number.MAX_VALUE;
					break;
				case "COUNT": newObj[dataField] = [];
					newObj[dataField + "Counter"] = 0;
					break;
				case "AVG": newObj[dataField] = 0;
					newObj[dataField + "Count"] = 0;
					break;
			}
			return newObj;
		}
		
		public function summaryOfSummaryCalculationBegin(value:Object, field:SummaryField2):Object
		{
			var newObj:Object = {};
			for (var p:String in value)
			{
				newObj[p] = value[p];
			}
			return newObj;
		}
	}
}