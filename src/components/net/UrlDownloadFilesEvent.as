package components.net
{
	import flash.events.Event;
	import flash.filesystem.File;
	
	public class UrlDownloadFilesEvent extends Event
	{
		public var percProgress:Number;
		public var errorString:String;
		public var file:File;
		
		
		public static var EVENT_ERROR:String = 'error';
		public static var EVENT_START:String = 'start';
		public static var EVENT_COMPLETE:String = 'complete';
		public static var EVENT_PROGRESS:String = 'progress';
		
		public function UrlDownloadFilesEvent(type:String, file:File=null, percProgress:Number = 0, errorString:String = '',  bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.file = file;
			this.percProgress = percProgress;
			this.errorString = errorString;
		}
		
		public override function clone():Event {
			return new UrlDownloadFilesEvent(type, file, percProgress, errorString);
		}
	}
}