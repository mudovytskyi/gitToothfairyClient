package components.net
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLStream;
	import flash.utils.ByteArray;
	
	import mx.utils.Base64Encoder;
	
	import services.ipClass;
	
	import toothfairy.configurations.ConstantSettings;

	public class UrlDownloadFile extends EventDispatcher
	{	
		public static const ERROR_TEMPLATE_NOTFOUND:String = 'TEMPLATE_NOTFOUND';
		
		
		private var urlStream:URLStream;  
		private var fileData:ByteArray = new ByteArray(); 
		private var fileName:String;
		private var fileLocal:File;
		
		public function UrlDownloadFile()
		{
		}
	
		
		public function getFileRemote(pathRemote:String, fileNameR:String):void  
		{  
			urlStream = new URLStream();
			var urlReq:URLRequest = new URLRequest(pathRemote);  
			urlStream.addEventListener(Event.COMPLETE, loaded, false, 0, true);  
			urlStream.addEventListener(Event.OPEN, onStart, false, 0, true);
			urlStream.addEventListener(ProgressEvent.PROGRESS, onProgress, false, 0, true);
			urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorListner, false, 0, true);
			urlStream.addEventListener(IOErrorEvent.IO_ERROR, errorListner, false, 0, true);
			urlStream.load(urlReq);
			
			fileName = fileNameR;
		}  
		
		public function getDocsRemote(pathRemote:String, fileNameR:String):void  
		{  
			urlStream = new URLStream();
			var urlReq:URLRequest = new URLRequest("http://"+ipClass.serverIp+"/toothfairy/services/docs/"+pathRemote);  
			
			var encoder:Base64Encoder = new Base64Encoder();        
			encoder.encode("admin:111777");  
			//encoder.encode("admin:adXr9ZKExg6Q2");
			
			var credsHeader:URLRequestHeader = new URLRequestHeader("Authorization", "Basic " + encoder.toString());
			urlReq.requestHeaders.push(credsHeader);
			
			urlStream.addEventListener(Event.COMPLETE, loaded, false, 0, true);  
			urlStream.addEventListener(Event.OPEN, onStart, false, 0, true);
			urlStream.addEventListener(ProgressEvent.PROGRESS, onProgress, false, 0, true);
			urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorListner, false, 0, true);
			urlStream.addEventListener(IOErrorEvent.IO_ERROR, errorListner, false, 0, true);
			urlStream.load(urlReq);
			
			fileName = fileNameR;
		}  
		
		private function onStart(event:Event):void
		{
			var startEvent:UrlDownloadFilesEvent = new UrlDownloadFilesEvent(UrlDownloadFilesEvent.EVENT_START);
			dispatchEvent(startEvent);
		}
		
		private function errorListner(event:ErrorEvent):void
		{
			urlStream = null;
			var errEvent:UrlDownloadFilesEvent = new UrlDownloadFilesEvent(UrlDownloadFilesEvent.EVENT_ERROR);
			switch(event)
			{
				case SecurityErrorEvent.SECURITY_ERROR:
				{
					errEvent.errorString='Ошибка безопасности: '+ event.errorID;
					break;
				}
				case IOErrorEvent.IO_ERROR:
				{
					errEvent.errorString='Ошибка ввода/вывода: '+ event.errorID;
					break;
				}
				default:
				{
					errEvent.errorString='Неизвестная ошибка: '+event.errorID;
					break;
				}
			}
			
			dispatchEvent(errEvent);
		}
		
		private function onProgress(event:ProgressEvent):void
		{
			var bytesTotal:Number = event.bytesTotal;
			var bytesLoaded:Number = event.bytesLoaded;
			
			var progEvent:UrlDownloadFilesEvent = new UrlDownloadFilesEvent(UrlDownloadFilesEvent.EVENT_PROGRESS);
			progEvent.percProgress = bytesLoaded*100/bytesTotal;
			dispatchEvent(progEvent);

		}
		
		private function loaded(event:Event):void  
		{  
			urlStream.readBytes (fileData, 0, urlStream.bytesAvailable);  
			resolveFile(fileName);  
		}  
		
		private function resolveFile(pathLocal:String):void  
		{  
			fileLocal = ConstantSettings.FOLDER_DOCUMENTS.resolvePath(pathLocal);  
			var fileStream:FileStream = new FileStream();  
			fileStream.addEventListener(Event.CLOSE, fileClosed, false, 0, true);  
			fileStream.openAsync(fileLocal, FileMode.WRITE);  
			fileStream.writeBytes(fileData, 0, fileData.length);  
			fileStream.close();  
		}  
		
		
		private function fileClosed(event:Event):void  
		{  
			var completeEvent:UrlDownloadFilesEvent = new UrlDownloadFilesEvent(UrlDownloadFilesEvent.EVENT_COMPLETE, fileLocal);
			dispatchEvent(completeEvent);
		}  

	}
}