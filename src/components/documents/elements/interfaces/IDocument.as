/********************************************
 title   : IDocument.as
 package : elements.pages.interfaces
 version : 1.0
 author  : Mykola Udovytskyi
 website : https://tasks.dev.com
 date    : Sep 5, 2019  time : 10:34:32 AM
 ********************************************/
//----------------------------------
//
// Package
//
//----------------------------------
package components.documents.elements.interfaces
{
	import components.documents.elements.PageGroup;

	//----------------------------------
	//
	// Interface
	//
	//----------------------------------
	public interface IDocument
	{
		//----------------------------------
		//
		// IMethods
		//
		//----------------------------------
		function getContent():void;
		function updateContent():void;
			
		//----------------------------------
		//
		// IGetters & ISetters
		//
		//----------------------------------
		function get pagesData():*;
		function set pagesData(value:*):void;
		
		function get orderNumber():String;
		function set orderNumber(value:String):void;
		
		function get dateFrom():String;
		function set dateFrom(value:String):void;
		
		function get formName():String;
		function set formName(value:String):void;
		
		function get formNumber():String;
		function set formNumber(value:String):void;

		function get content():PageGroup;
		function set content(value:PageGroup):void;
	}
}