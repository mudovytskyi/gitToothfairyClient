/********************************************
 title   : IPatientCard.as
 package : components.documents.elements.interfaces
 version : 1.0
 author  : Mykola Udovytskyi
 website : https://tasks.dev.com
 date    : Sep 5, 2019  time : 10:34:32 AM
 ********************************************/
//----------------------------------
//
// Package
//
//----------------------------------
package components.documents.elements.interfaces
{
	//----------------------------------
	//
	// Interface
	//
	//----------------------------------
//	[Bindable]
	public interface IPatientCard
	{
		//----------------------------------
		//
		// IMethods
		//
		//----------------------------------
		function addDocument():void;
		function openDocument():void;
		
		function get hasSelectedDoc():Boolean;
		function set hasSelectedDoc(value:Boolean):void;
		
		[Bindable(event="propertyChange")]
		function get numCards():int;
		function set numCards(value:int):void;
	}
}