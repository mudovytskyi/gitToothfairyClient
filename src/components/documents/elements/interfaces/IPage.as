/********************************************
 title   : IPage.as
 package : elements.pages.interfaces
 version : 1.0
 author  : mykol
 website : http://tasks.qs-dev.com
 date    : Sep 4, 2019  time : 2:42:56 PM
 ********************************************/
//---------------------------------
//
// Package
//
//---------------------------------
package components.documents.elements.interfaces
{
	//---------------------------------
	//
	// Libraries
	//
	//---------------------------------
	import mx.core.UIComponent;
	
	import spark.components.VGroup;

	//---------------------------------
	//
	// Interface
	//
	//---------------------------------
	public interface IPage
	{
		//---------------------------------
		//
		// IMethods
		//
		//---------------------------------
		function updateContentData():void;
		function getContentData():void;
		
		
		//---------------------------------
		//
		// IGetters & ISetters
		//
		//---------------------------------
		function get data():*;
		function set data(value:*):void;
		
		function get headerContent():UIComponent;
		function set headerContent(value:UIComponent):void;
		
		function get titleContent():UIComponent;
		function set titleContent(value:UIComponent):void;
		
		function get labelContent():UIComponent;
		function set labelContent(value:UIComponent):void;
		
		function get bodyContent():UIComponent;
		function set bodyContent(value:UIComponent):void;
		
		function get footerContent():UIComponent;
		function set footerContent(value:UIComponent):void;
		
		function get pageContainer():VGroup;
		function set pageContainer(value:VGroup):void;
	}
}