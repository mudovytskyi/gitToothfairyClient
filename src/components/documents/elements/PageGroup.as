package components.documents.elements
{
	import components.documents.elements.interfaces.IPage;

	[Bindable]
	public class PageGroup
	{
		//----------------------------------
		// Variables
		//----------------------------------
		public var recycle:Vector.<IPage>;
		public var pages:Vector.<IPage>;
		public var groupName:String;
		
		
		//----------------------------------
		//
		// Constructor
		//
		//----------------------------------
		public function PageGroup(pages:Array = null, name:String = "")
		{
			this.pages = Vector.<IPage>(pages || []);
			recycle = new Vector.<IPage>();
			
			groupName = name;
		}
		
		
		//---------------------------------
		//
		// Public methods
		//
		//---------------------------------
		public function updateGroup():void
		{
		}
		
		public function addPage(page:IPage):uint
		{
			return pages.push(page);
		}
		
		public function removePage(page:IPage):void
		{
			var number:Number = getPageNum(page); 
			if (number != -1) {
				recycle = recycle.concat(pages.splice(number, 1));
				updateGroup();
			}
		}
		
		public function clearGroup():void 
		{
			pages.length = 0;	
		}
		
		public function getPageNum(page:IPage):Number
		{
			return pages.indexOf(page); 
		}
		
		
		//---------------------------------
		//
		// Getters & Setters
		//
		//---------------------------------
		public function get totalPages():uint { return pages.length; }
	}
}