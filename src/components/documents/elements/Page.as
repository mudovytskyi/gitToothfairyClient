/********************************************
 title   : Page.as
 package : elements.pages
 version : 1.0
 author  : mykol
 website : https://toothfairy.dev.com
 date    : Sep 5, 2019  time : 9:43:50 AM
 ********************************************/
//---------------------------------
//
// Package
//
//---------------------------------
package components.documents.elements
{
	//---------------------------------
	//
	// Libraries
	//
	//---------------------------------
	import flash.errors.IllegalOperationError;
	
	import mx.core.UIComponent;
	import mx.graphics.SolidColor;
	
	import spark.components.Group;
	import spark.components.HGroup;
	import spark.components.VGroup;
	import spark.filters.DropShadowFilter;
	import spark.primitives.Rect;
	
	import components.documents.elements.interfaces.IDocument;
	import components.documents.elements.interfaces.IPage;
	
	
	//----------------------------------
	//
	// Class : Page.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements:
	 *   @use: abstract class
	 *
	 *   @author: Mykola Udovytskyi
	 *   @date: Sep 5, 2019 @time: 10:21:56 AM */
	//----------------------------------
	//----------------------------------
	// ABSTRACT CLASS
	//----------------------------------
	public class Page extends Group implements IPage	
	{
		//----------------------------------
		// Variables
		//----------------------------------
		private var _data:Object;

		private var _pageContent:VGroup;
		
		private var _header:UIComponent;
		private var _title:UIComponent;
		private var _label:UIComponent;
		private var _body:UIComponent;
		private var _footer:UIComponent;
		
		protected var documentRef:IDocument;
		

		//---------------------------------
		//
		// Constructor
		//
		//---------------------------------

		public function Page(documentRef:IDocument, data:Object = null)
		{
			this.width = 760; // було 800 при відступах зліва і справа у 40
			this.height = 1020;
			var shadow:DropShadowFilter = new DropShadowFilter(8, 45, 0x333333, 0.75, 10, 10);
			this.filters = [shadow];
			
			// Створюємо макет
			initPage();

			// Заповнювати дані лише після створення макету сторінки
			this.documentRef = documentRef;
			this.data = data;
		}
		
		
		//---------------------------------
		//
		// Private methods
		//
		//---------------------------------
		private function initPage():void
		{
			createPageCSS();
			createContent();
			createMockUp();
		}
		
		//---------------------------------
		//
		// Protected methods
		//
		//---------------------------------
		/**
		 * ABSTRACT
		 */
		protected function createContent():void
		{
			throw new IllegalOperationError("Abstract method of abstract class Page. " +
				"Please, use subclass with overrided method 'createContent'");	
		}
		
		protected function createMockUp():void
		{
			// Page
			pageContainer = new VGroup();
			pageContainer.percentWidth = 100;
			pageContainer.percentHeight = 100;
			
			pageContainer.gap = -3;
			pageContainer.paddingBottom = 20; // +20 footer
			pageContainer.paddingLeft = 20; // 40 було, коли ширина сторінки 800
			pageContainer.paddingRight = 20; // 40 було, коли ширина сторінки 800
			pageContainer.paddingTop = 20; // +20 header

			addElement(pageContainer);
			
			var headerContainer:HGroup = new LineGroup();
			var footerContainer:HGroup = new LineGroup();
			
			// Макет ------------------------------
			pageContainer.addElement(headerContainer);

			if (headerContent)
				headerContainer.addElement(headerContent);
			
			if (titleContent)
				pageContainer.addElement(titleContent);
			
			if (labelContent)
				pageContainer.addElement(labelContent);
			
			if (bodyContent)
				pageContainer.addElement(bodyContent);
			
			if (footerContent)
				footerContainer.addElement(footerContent);
			
			pageContainer.addElement(footerContainer);
			// Макет ------------------------------

			// Background
			var bg:Rect = new Rect();
			bg.left = 0;
			bg.right = 0;
			bg.top = 0;
			bg.height = height;
			bg.percentWidth = 100;
			bg.percentHeight = 100;
			bg.fill = new SolidColor(0xffffff);
			addElementAt(bg, 0);
		}
		
		protected function createPageCSS():void
		{
			// Fonts
			
			// Styles
//			var pageStyle:CSSStyleDeclaration = new CSSStyleDeclaration();
//			pageStyle.setStyle("fontSize", 11);
//			styleManager.setStyleDeclaration(".commonPageStyle", pageStyle, true);
		}

		//---------------------------------
		//
		// Public methods
		//
		//---------------------------------
		public function updateContentData():void
		{
			throw new IllegalOperationError("Abstract method of abstract class Page. " +
				"Please, use subclass with overrided method 'updateContentData'");
		}
		
		public function getContentData():void
		{
			throw new IllegalOperationError("Abstract method of abstract class Page. " +
				"Please, use subclass with overrided method 'getContentData'");
		}

		
		//---------------------------------
		//
		// Getters & Setters
		//
		//---------------------------------
		[Bindable]
		public function get data():* { return _data; }
		public function set data(value:*):void  {
			_data = value;
			if (_data) {
				updateContentData();
			}	
		}

		
		//----------------------------------
		// Розмітка сторінки
		//----------------------------------
		//---- Верхній колонтитул ----
		[Bindable]
		public function get headerContent():UIComponent { return _header; }
		public function set headerContent(value:UIComponent):void {
			_header = value;
		}
		
		//---- Заголовок ----
		[Bindable]
		public function get titleContent():UIComponent { return _title; }
		public function set titleContent(value:UIComponent):void {
			_title = value;
		}
		
		//---- Назва ----
		[Bindable]
		public function get labelContent():UIComponent { return _label; }
		public function set labelContent(value:UIComponent):void {
			_label = value;
		}
		
		//---- Вміст ----
		[Bindable]
		public function get bodyContent():UIComponent { return _body; }
		public function set bodyContent(value:UIComponent):void {
			_body = value;
		}
		
		//---- Нижній колонтитул ----
		[Bindable]
		public function get footerContent():UIComponent { return _footer; }
		public function set footerContent(value:UIComponent):void {
			_footer = value;
		}
		
		[Bindable]
		public function get pageContainer():VGroup { return _pageContent; }
		public function set pageContainer(value:VGroup):void {
			_pageContent = value;						
		}
	}
}