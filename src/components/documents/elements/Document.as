/********************************************
 title   : Doc_003.as
 package : elements.pages.hospital
 version : 1.0
 author  : Mykola Udovytskyi
 website : https://tasks.dev.com
 date    : Sep 5, 2019  time : 10:43:41 AM
 ********************************************/
//----------------------------------
//
// Package
//
//----------------------------------
package components.documents.elements
{
	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------
	import flash.errors.IllegalOperationError;
	
	import spark.components.VGroup;
	
	import components.documents.elements.interfaces.IDocument;
	import components.documents.elements.interfaces.IPage;
	

	//----------------------------------
	//
	// Class : Document.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements:
	 *   @use:
	 *
	 *   @author: Mykola Udovytskyi
	 *   @date: Sep 5, 2019 @time: 10:43:24 AM */
	//----------------------------------
	//----------------------------------
	// ABSTRACT CLASS
	//----------------------------------
	public class Document extends VGroup implements IDocument	
	{
		//----------------------------------
		// Variables
		//----------------------------------
		private var _pageGroup:PageGroup;
		private var _pagesData:*;
		private var _formNumber:String = "#FORM_NUMBER";
		private var _formName:String = "#FORM_NAME";
		private var _dateFrom:String = "#DATE_FROM";
		private var _orderNumber:String = "#ORDER_NUMBER";
		

		//----------------------------------
		//
		// Constructor
		//
		//----------------------------------
		public function Document() 
		{
			initData();
		}
		
		/**
		 * ABSTRACT
		 */
		protected function initData():void
		{
			//---------------------------------
			// STUB CODE: NEED TO OVERRIDE
			//---------------------------------
			// Stub data 
			_pageGroup = new PageGroup([], "#DOCUMENT_PAGE_GROUP");
			pagesData = { 
				formName: formName, 
				cardNumber: "#SOME_CARD_NUMBER",
				formNumber: formNumber,
				dateFrom: "01011970", // відлік від 1 січня 1970р.
				orderNumber: "#ORDER_NUMBER"
			};
			
			throw new IllegalOperationError("Abstract method of abstract class Page. " +
				"Please, use subclass with overrided method 'initData'");
		}		
		
		
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		public function updateContent():void
		{
			if (pagesData) {
				for each (var page:IPage in content.pages) {
					page.data = pagesData;
				}
			}
		}
		
		public function getContent():void
		{
			if (pagesData) {
				for each (var page:IPage in content.pages) {
					page.getContentData();					
				}
			}
		}
		
		
		//----------------------------------
		//
		// Getters & Setters
		//
		//----------------------------------
		public function get dateFrom():String { return _dateFrom; }
		public function set dateFrom(value:String):void {
			_dateFrom = value;
		}
		
		public function get orderNumber():String { return _orderNumber; }
		public function set orderNumber(value:String):void {
			_orderNumber = value;
		}

		public function get formNumber():String { return _formNumber; }
		public function set formNumber(value:String):void {
			_formNumber = value;
		}
		
		public function get formName():String { return _formName; }
		public function set formName(value:String):void {
			_formName = value;
		}
		
		public function get content():PageGroup { return _pageGroup; }
		public function set content(value:PageGroup):void {
			_pageGroup = value;
		}
		
		public function get pagesData():* { return _pagesData; }
		public function set pagesData(value:*):void {
			_pagesData = value;
			updateContent();
		}
	}
}