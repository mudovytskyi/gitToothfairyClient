package components.documents.pages.hospital
{
	import components.documents.elements.DocumentLabel;
	import components.documents.elements.DocumentTitle;
	import components.documents.elements.Page;
	import components.documents.elements.interfaces.IDocument;
	
	import valueObjects.HospitalCardDataFull;
	
	public class DocTitlePage_003_1 extends Page
	{
		public function DocTitlePage_003_1(documentRef:IDocument, data:Object = null)
		{
			super(documentRef, data);
		}
		
		//----------------------------------
		//
		// Overrided protected methods
		//
		//----------------------------------
		override protected function createContent():void
		{
			titleContent = new DocumentTitle();
			labelContent = new DocumentLabel();
			bodyContent = new DocBody_003_1();
		}
		
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		override public function getContentData():void
		{
			var _data:HospitalCardDataFull = data as HospitalCardDataFull;
			if (_data) {
				//---- Шапка-титул ----
				var docTitle:DocumentTitle = titleContent as DocumentTitle;
				_data.CLINIC_NAME = docTitle.clinicName.text;
				_data.CLINIC_ADDRESS = docTitle.clinicAddress.text;
				_data.CLINIC_REGISTRATION_CODE = docTitle.clinicCode.data;
				
				
				//---- Назва-номер ----
				var docLabel:DocumentLabel = labelContent as DocumentLabel;
				_data.CARD_NUMBER = docLabel.cardNumber.text;
				
				//---- Перша сторінка ----
				var docBody1:DocBody_003_1 = bodyContent as DocBody_003_1;
//				_data.CREATED 	=  docBody1.;
				_data.OPENED 				=  docBody1.openedDateBI.data + docBody1.openedTimeHoursBI.data + docBody1.openedTimeMinsBI.data;
				_data.CLOSED 				=  docBody1.closedDateBI.data + docBody1.closedTimeHoursBI.data;
				_data.PATIENT_SEX 			=  docBody1.sexBI.data;
				_data.PATIENT_FULL_NAME 	=  docBody1.fullNameTAWL.text;
				_data.PATIENT_BIRTH 		=  docBody1.birthBI.data;
				_data.PATIENT_AGE 			=  docBody1.ageTAWL.text;
				_data.PATIENT_DOCUMENT 		=  docBody1.documentTI.text;
				_data.PATIENT_DOCUMENT_NUMBER =  docBody1.documentNumberTI.text;
				_data.PATIENT_CITIZENSHIP 	=  docBody1.citizenshipTAWL.text;
				_data.HABITATION_TYPE 		=  docBody1.habitationTypeBI.data;
				_data.ADDRESS_PART_1 		=  docBody1.addressPart1TAWL.text;
				_data.ADDRESS_PART_2 		=  docBody1.addressPart2TAWL.text;
				_data.POSTAL_CODE 			=  docBody1.postalCodeTAWL.text;
				_data.WORKPLACE_PART_1 		= docBody1.workplacePart1TAWL.text;
				_data.WORKPLACE_PART_2 		=  docBody1.workplacePart2TAWL.text;
				_data.REFFERENT_CLINIC 		=  docBody1.refferentClinicTAWL.text;
				_data.REFFERENT_REGISTRATION_CODE =  docBody1.refferentClinicCodeBI.data;
				_data.INCOME_DIAGNOSIS 		=  docBody1.incomeDiagnosisTAWL.text;
				_data.INCOME_DIAGNOSIS_MKH 	=  docBody1.incomeDiagnosisCodeBI.data;
				_data.DEPARTMENT_CODE_INCOME =  docBody1.departmentIncomeCodeBI.data;
				_data.DEPARTMENT_CODE_OUTCOME =  docBody1.departmentOutcomeCodeBI.data;
				_data.HOSPITALIZATION_TYPE 		=  docBody1.hospitalizationTypeBI.data;
				_data.LAST_HIV_PROBE_DATE 	=  docBody1.hivProbeDateBI.data;
				_data.BLOOD_TYPE 			=  docBody1.bloodTypeTAWL.text;
				_data.BLOOD_RH 				=  docBody1.bloodRHTAWL.text;
				_data.LAST_VASSERMAN_REACTION_DATE 	=  docBody1.vassermanReactionDateBI.data;
				_data.FARM_ALLERGY_REACTIONS_PART_1 =  docBody1.farmAllergyPart1TAWL.text;
				_data.FARM_ALLERGY_REACTIONS_PART_2 =  docBody1.farmAllergyPart2TAWL.text;
				_data.HOSPITALIZATION_REPEATED_IN_YEAR 	=  docBody1.hospitalizationRepeatedInYearBI.data;
				_data.HOSPITALIZATION_REPEATED_IN_MONTH =  docBody1.hospitalizationRepeatedInMonthBI.data;
				_data.TREATED_BED_DAYS 				=  docBody1.treatedBedDaysTAWL.text;
			}
		}
		
		override public function updateContentData():void
		{
			var _data:HospitalCardDataFull = data as HospitalCardDataFull;
			if (_data) {
				var docTitle:DocumentTitle = titleContent as DocumentTitle;
				//----------------------------------
				// Технічні відомості
				//----------------------------------
				docTitle.formNumber.text = "№ " + documentRef.formNumber;
				docTitle.dateFrom.data = documentRef.dateFrom;
				docTitle.orderNumber.data = documentRef.orderNumber;
				//---- Технічні відомості ---
				
				//----------------------------------
				// Дані
				//----------------------------------
				//---- Шапка-титул ----
				docTitle.clinicName.text = _data.CLINIC_NAME;
				docTitle.clinicAddress.text = _data.CLINIC_ADDRESS;
				docTitle.clinicCode.data = _data.CLINIC_REGISTRATION_CODE;
				
				//---- Назва-номер ----
				var docLabel:DocumentLabel = labelContent as DocumentLabel;
				docLabel.docName.label = documentRef.formName + " №";				
				docLabel.cardNumber.text = _data.CARD_NUMBER;
				
				//---- Перша сторінка ----
				var docBody1:DocBody_003_1 = bodyContent as DocBody_003_1;
//				docBody1.openedDateBI.data  = _data.CREATED.substr(0, 6);
				docBody1.openedDateBI.data  		= _data.OPENED.substr(0, 6);
				docBody1.openedTimeHoursBI.data  	= _data.OPENED.substr(6, 2);
				docBody1.openedTimeMinsBI.data  	= _data.OPENED.substr(8, 2);
				docBody1.closedDateBI.data  		= _data.CLOSED.substr(0, 6);
				docBody1.closedTimeHoursBI.data  	= _data.CLOSED.substr(6, 2);
				docBody1.sexBI.data  				= _data.PATIENT_SEX;
				docBody1.fullNameTAWL.text  		= _data.PATIENT_FULL_NAME;
				docBody1.birthBI.data  				= _data.PATIENT_BIRTH;
				docBody1.ageTAWL.text  				= _data.PATIENT_AGE;
				docBody1.documentTI.text  			= _data.PATIENT_DOCUMENT;
				docBody1.documentNumberTI.text  	= _data.PATIENT_DOCUMENT_NUMBER;
				docBody1.citizenshipTAWL.text		= _data.PATIENT_CITIZENSHIP;
				docBody1.habitationTypeBI.data	    = _data.HABITATION_TYPE;
				docBody1.addressPart1TAWL.text	    = _data.ADDRESS_PART_1;
				docBody1.addressPart2TAWL.text		= _data.ADDRESS_PART_2;
				docBody1.postalCodeTAWL.text	    = _data.POSTAL_CODE;
				docBody1.workplacePart1TAWL.text    = _data.WORKPLACE_PART_1;
				docBody1.workplacePart2TAWL.text	= _data.WORKPLACE_PART_2;
				docBody1.refferentClinicTAWL.text   = _data.REFFERENT_CLINIC;
				docBody1.refferentClinicCodeBI.data = _data.REFFERENT_REGISTRATION_CODE;
				docBody1.incomeDiagnosisTAWL.text   = _data.INCOME_DIAGNOSIS;
				docBody1.incomeDiagnosisCodeBI.data = _data.INCOME_DIAGNOSIS_MKH;
				docBody1.departmentIncomeCodeBI.data = _data.DEPARTMENT_CODE_INCOME;
				docBody1.departmentOutcomeCodeBI.data = _data.DEPARTMENT_CODE_OUTCOME;
				docBody1.hospitalizationTypeBI.data		= _data.HOSPITALIZATION_TYPE;
				docBody1.hivProbeDateBI.data		= _data.LAST_HIV_PROBE_DATE;
				docBody1.bloodTypeTAWL.text			= _data.BLOOD_TYPE;
				docBody1.bloodRHTAWL.text		    = _data.BLOOD_RH;
				docBody1.vassermanReactionDateBI.data  = _data.LAST_VASSERMAN_REACTION_DATE;
				docBody1.farmAllergyPart1TAWL.text	= _data.FARM_ALLERGY_REACTIONS_PART_1;
				docBody1.farmAllergyPart2TAWL.text  = _data.FARM_ALLERGY_REACTIONS_PART_2;
				docBody1.hospitalizationRepeatedInYearBI.data  = _data.HOSPITALIZATION_REPEATED_IN_YEAR;
				docBody1.hospitalizationRepeatedInMonthBI.data = _data.HOSPITALIZATION_REPEATED_IN_MONTH;
				docBody1.treatedBedDaysTAWL.text	= _data.TREATED_BED_DAYS;
			}
		}
	}
}