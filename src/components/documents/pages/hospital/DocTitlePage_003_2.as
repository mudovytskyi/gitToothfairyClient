package components.documents.pages.hospital
{
	import components.documents.elements.DocHeader;
	import components.documents.elements.Page;
	import components.documents.elements.interfaces.IDocument;
	
	import valueObjects.HospitalCardDataFull;
	import valueObjects.HospitalCardDisabilityCertificate;
	
	
	public class DocTitlePage_003_2 extends Page
	{
		public function DocTitlePage_003_2(documentRef:IDocument, data:Object=null)
		{
			super(documentRef, data);
		}
		
		//----------------------------------
		//
		// Overrided protected methods
		//
		//----------------------------------
		override protected function createContent():void
		{
			headerContent = new DocHeader();
			bodyContent = new DocBody_003_2();

//			var footer:DocFooter_003 = new DocFooter();
//			footer.text = "*Відсутня – 1; місцева – 2; загальна – 3; інша – 4.";
//			footerContent = footer;
		}
		
		
		//----------------------------------
		//
		// Implemented methods
		//
		//----------------------------------
		override public function getContentData():void
		{
			var _data:HospitalCardDataFull = data as HospitalCardDataFull;
			if (_data) {
				//---- Друга сторінка ----
				var docBody:DocBody_003_2 = bodyContent as DocBody_003_2;
				_data.INJURY_TYPE					= docBody.injuryTypeBI.data;
				_data.ADDITIONAL_OUTCOME_DIAGNOSIS	= docBody.additionalOutcomeDiagnosisBI.data;
				_data.RESISTANTION_CATEGORY			= docBody.resistantionCategoryBI.data;
				_data.DOCTOR_FULL_NAME_DIAGNOS		= docBody.doctorDiagnosisFullNameTAWL.text;
				_data.DOCTOR_SIGN_DIAGNOS			= docBody.doctorDiagnosisSignTAWL.text;
				_data.DOCTOR_REGISTRATION_DIAGNOS	= docBody.doctorDiagnosisCodeTAWL.text;
				_data.DOCTOR_DATE_DIAGNOS			= docBody.doctorDiagnosisDateBI.data;
				_data.OTHER_TREATMENTS				= docBody.otherTreatmentsTAWL.text;
				_data.CANCER_TREATMENT_TYPE			= docBody.cancerTreatmentTypeBI.data;
				_data.EMPLOYABILITY					= docBody.emplyabilityBI.data;
				_data.EXPERTISE_CONCLUSION			= docBody.expertiseConclusionTAWL.text;
				_data.TREATMENT_RESULT				= docBody.treatmentResultBI.data;
				_data.ONCOLOGY_CHECKUP_DATE			= docBody.oncologyCheckupDateBI.data;
				_data.CHEST_ORGANS_CHECKUP_DATE		= docBody.chestOrgansCheckupDateBI.data;
				_data.INSURANCY						= docBody.insurancyTAWL.text;
				_data.DOCTOR_FULL_NAME_OUTCOME		= docBody.doctorOutcomeFullNameTAWL.text;
				_data.DOCTOR_SIGN_OUTCOME			= docBody.doctorOutcomeSignTAWL.text;
				_data.DOCTOR_REGISTRATION_OUTCOME	= docBody.doctorOutcomeCodeTAWL.text;
				_data.HEADDEPARTMENT_FULL_NAME		= docBody.headdepartmentFullNameTAWL.text;
				_data.HEADDEPARTMENT_SIGN			= docBody.headdepartmentSignTAWL.text;
				_data.HEADDEPARTMENT_REGISTRATION	= docBody.headdepartmentCodeTAWL.text;
				
				//---- Діагнози ----
				_data.hospitalDiagnosis;
				
				//---- Процедури ----
				_data.hospitalProcedueres;
				
				//---- Листки непрацездатності ----
				_data.hospitalDisabilityCerts.removeAll();
				
				var cert:HospitalCardDisabilityCertificate = new HospitalCardDisabilityCertificate();
				cert.CERTIFICATE_NUMBER = docBody.hospitalCert1NumberTAWL.text; 					
				cert.FROM = docBody.hospitalCert1NumberTAWL.text;					
				cert.TO = docBody.hospitalCert1NumberTAWL.text;					
				_data.hospitalDisabilityCerts.addItem(cert);
				
				cert = new HospitalCardDisabilityCertificate();
				
				cert.CERTIFICATE_NUMBER = docBody.hospitalCert2NumberTAWL.text; 					
				cert.FROM = docBody.hospitalCert2NumberTAWL.text;					
				cert.TO = docBody.hospitalCert2NumberTAWL.text;						
				_data.hospitalDisabilityCerts.addItem(cert);
			}
		}
		
		override public function updateContentData():void
		{
			var _data:HospitalCardDataFull = data as HospitalCardDataFull;
			if (_data) {
				//----------------------------------
				// Tехчніні відомості
				//----------------------------------
				//---- Верхній колонтитул ----
				var docHeader:DocHeader = headerContent as DocHeader;
				docHeader.header = documentRef.formNumber;
				
				//---- Друга сторінка ----
				var docBody:DocBody_003_2 = bodyContent as DocBody_003_2;
				docBody.injuryTypeBI.data 					= _data.INJURY_TYPE;
				docBody.additionalOutcomeDiagnosisBI.data 	= _data.ADDITIONAL_OUTCOME_DIAGNOSIS;
				docBody.resistantionCategoryBI.data 		= _data.RESISTANTION_CATEGORY;
				docBody.doctorDiagnosisFullNameTAWL.text 	= _data.DOCTOR_FULL_NAME_DIAGNOS;
				docBody.doctorDiagnosisSignTAWL.text	 	= _data.DOCTOR_SIGN_DIAGNOS;
				docBody.doctorDiagnosisCodeTAWL.text 		= _data.DOCTOR_REGISTRATION_DIAGNOS;
				docBody.doctorDiagnosisDateBI.data 			= _data.DOCTOR_DATE_DIAGNOS;
				docBody.otherTreatmentsTAWL.text 			= _data.OTHER_TREATMENTS;
				docBody.cancerTreatmentTypeBI.data 			= _data.CANCER_TREATMENT_TYPE;
				docBody.emplyabilityBI.data 				= _data.EMPLOYABILITY;
				docBody.expertiseConclusionTAWL.text 		= _data.EXPERTISE_CONCLUSION;
				docBody.treatmentResultBI.data 				= _data.TREATMENT_RESULT;
				docBody.oncologyCheckupDateBI.data 			= _data.ONCOLOGY_CHECKUP_DATE;
				docBody.chestOrgansCheckupDateBI.data 		= _data.CHEST_ORGANS_CHECKUP_DATE;
				docBody.insurancyTAWL.text 					= _data.INSURANCY;
				docBody.doctorOutcomeFullNameTAWL.text 		= _data.DOCTOR_FULL_NAME_OUTCOME;
				docBody.doctorOutcomeSignTAWL.text 			= _data.DOCTOR_SIGN_OUTCOME;
				docBody.doctorOutcomeCodeTAWL.text 			= _data.DOCTOR_REGISTRATION_OUTCOME;
				docBody.headdepartmentFullNameTAWL.text 	= _data.HEADDEPARTMENT_FULL_NAME;
				docBody.headdepartmentSignTAWL.text 		= _data.HEADDEPARTMENT_SIGN;
				docBody.headdepartmentCodeTAWL.text 		= _data.HEADDEPARTMENT_REGISTRATION;
				
				//---- Таблиці ----
				docBody.diagnosesDG.dataProvider 			= _data.hospitalDiagnosis;
				docBody.proceduresDG.dataProvider 			= _data.hospitalProcedueres;
				
				if (_data.hospitalDisabilityCerts && _data.hospitalDisabilityCerts.length) {
					var cert:HospitalCardDisabilityCertificate = _data.hospitalDisabilityCerts.getItemAt(0) as HospitalCardDisabilityCertificate;
					if (cert) {
						docBody.hospitalCert1NumberTAWL.text = cert.CERTIFICATE_NUMBER; 					
						docBody.hospitalCert1NumberTAWL.text = cert.FROM;					
						docBody.hospitalCert1NumberTAWL.text = cert.TO;					
					}
					
					cert = _data.hospitalDisabilityCerts.getItemAt(1) as HospitalCardDisabilityCertificate;

					if (cert) {
						docBody.hospitalCert2NumberTAWL.text = cert.CERTIFICATE_NUMBER; 					
						docBody.hospitalCert2NumberTAWL.text = cert.FROM;					
						docBody.hospitalCert2NumberTAWL.text = cert.TO;					
					}
				}
			}
		}
	}
}