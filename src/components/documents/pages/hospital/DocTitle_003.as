/********************************************
 title   : Doc_003.as
 package : elements.pages.hospital
 version : 1.0
 author  : Mykola Udovytskyi
 website : https://tasks.dev.com
 date    : Sep 5, 2019  time : 10:43:41 AM
 ********************************************/
//----------------------------------
//
// Package
//
//----------------------------------
package components.documents.pages.hospital
{
	import components.documents.elements.Document;
	import components.documents.elements.PageGroup;

	//----------------------------------
	//
	// Libraries
	//
	//----------------------------------

	//----------------------------------
	//
	// Class : Doc_003.as
	//
	//----------------------------------
	/** @extends: 
	 *   @implements:
	 *   @use:
	 *
	 *   @author: Mykola Udovytskyi
	 *   @date: Sep 5, 2019 @time: 10:43:24 AM */
	//----------------------------------
	public class DocTitle_003 extends Document	
	{
		//----------------------------------
		//
		// Constructor
		//
		//----------------------------------
		public function DocTitle_003() 
		{
			super();
		}
		
		//----------------------------------
		//
		// Overrided protected methods
		//
		//----------------------------------
		override protected function initData():void
		{
			formName = "МЕДИЧНА КАРТА СТАЦІОНАРНОГО ХВОРОГО";
			formNumber = "003/о";
			dateFrom = "21012016";
			orderNumber = "  29";
			
			content = new PageGroup([new DocTitlePage_003_1(this), new DocTitlePage_003_2(this)], "Doc_003_Title");
			
			// Stub data
			pagesData ||= { 
				cardNumber: "003_01"
			};
		}
	}
}