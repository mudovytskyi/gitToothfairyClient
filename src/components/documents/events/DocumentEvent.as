package components.documents.events
{
	import flash.events.Event;
	
	import mx.utils.StringUtil;
	
	public class DocumentEvent extends Event
	{
		public static const FILL_UP_DOCUMENT_FORM_EVENT:String = 'FillUpDocumentFormEvent';
		public static const CLEAN_UP_DOCUMENT_FORM_EVENT:String = 'CleanUpDocumentFormEvent';
		
		
		public function DocumentEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new DocumentEvent(type, bubbles, cancelable);
		}
		
		public override function toString():String
		{
			return StringUtil.substitute("components.documents.events.DocumentEvent::{0}", this.type);			
		}
	}
}