package components
{
	import com.adobe.fiber.runtime.lib.MathFunc;
	import com.adobe.fiber.runtime.lib.StringFunc;
	
	import flash.globalization.NumberFormatter;
	
	import spark.formatters.NumberFormatter;

	public class MoneyToString
	{	
		public function MoneyToString()
		{
		}
		
		public static function toWords (summ:Number):String
		{
			var result:String = new String();
			var left:int = int(summ);
			var right:int = int(Math.abs((summ - left) * 100).toFixed(0));
			
	
			//BigInteger [] divrem = summ.divideAndRemainder (hundred);
			if (left == 0) result += "Нуль ";
			//divrem = divrem [0].divideAndRemainder (thousand);
			var quotient:int  = left/1000;
			var remainder:int = left%1000;
			var group:int = 0;
			do {
				var value:int = remainder;
				result = groupToString(value, group) + result;
				// Для нулевой группы добавим в конец соответствующую валюту
				if (group == 0) {
					var rank10:int = (value % 100) / 10;
					var rank:int = value % 10;
					if (rank10 == 1) {
						result += "гривень";
					}
					else {
						switch (rank) {
							case 1: result += "гривня"; break;
							case 2:
							case 3:
							case 4: result += "гривні"; break;
							default: result += "гривень"; break;
						}
					}
				}
				var divrem:int = quotient;
				quotient  = divrem/1000;
				remainder = divrem%1000;
				group++;
			}
			while (!quotient==0 || !remainder==0);
			
			// Дробная часть суммы
			var s:String = alignSumm(right.toString());
			result += " " + s + " ";
			
			if (s.charAt (0) == '1') {
				result += "копійок";
			}
			else {
				switch (s.charAt(1)) {
					case '1': result += "копійка"; break;
					case '2':
					case '3':
					case '4': result += "копійки"; break;
					default:  result += "копійок"; break;
				}
			}
			// По правилам бухгалтерского учета первая буква строкового
			// представления должна быть в верхнем регистре
			result = result.substr(0,1).toUpperCase() + result.substr(1);
				//setCharAt (0, Character.toUpperCase (result.charAt (0)));
			
			// Вот ради этой строки все и затевалось: результат получен !!!
			return result;
		}
		
		private  static function alignSumm (s:String):String  {
			switch (s.length) {
				case 0: s = "00"; break;
				case 1: s = "0" + s; break;
			}
			return s;
		}
		
		// Преобразование группы цифр в строку
		private static function groupToString (value:int, group:int ):String {
			var result:String = new String();
			if (value == 0) 
			{
				return result.toString();
			}
			// Разбор числа по разрядам, начиная с сотен
			var rank:int = value / 100;
			switch (rank) 
			{
				case 1:  result += "сто ";       break;
				case 2:  result += "двісті ";    break;
				case 3:  result += "триста ";    break;
				case 4:  result += "чотириста "; break;
				case 5:  result += "п'ятсот ";   break;
				case 6:  result += "шістсот ";  break;
				case 7:  result += "сімсот ";   break;
				case 8:  result += "вісімсот "; break;
				case 9:  result+= "дев'ятсот "; break;
				default: break;
			}
			// Далее, десятки
			rank = (value % 100) / 10;
			switch (rank) 
			{
				default: break;
				case 2:  result += "двадцять ";    break;
				case 3:  result += "тридцять ";    break;
				case 4:  result += "сорок ";       break;
				case 5:  result += "п'ятдесят ";   break;
				case 6:  result += "шістдесят ";  break;
				case 7:  result += "сімдесят ";   break;
				case 8:  result += "вісімдесят "; break;
				case 9:  result += "дев'яносто ";   break;
			}
			// Если десятки = 1, добавить соответствующее значение. Иначе - единицы
			var	rank10:int = rank;
			rank = value % 10;
			if (rank10 == 1) {
				switch (rank) 
				{
					case 0: result += "десять ";       break;
					case 1: result += "одинадцять ";  break;
					case 2: result += "дванадцять ";   break;
					case 3: result += "тринадцять ";   break;
					case 4: result += "чотирнадцять "; break;
					case 5: result += "п'ятнадцять ";   break;
					case 6: result += "шістнадцять ";  break;
					case 7: result += "сімнадцять ";   break;
					case 8: result += "вісімнадцять "; break;
					case 9: result += "дев'ятнадцять "; break;
				}
			}
			else {
				switch (rank) 
				{
					case 1: 
						if (group == 1 || group == 0) // Тысячи
							result += "одна ";
						else
							result += "один ";
						break;
					case 2:
						if (group == 1 || group == 0) // Тысячи
							result += "дві ";
						else
							result += "два ";
						break;
					case 3: result += "три ";    break;
					case 4: result += "чотири "; break;
					case 5: result += "п'ять ";   break;
					case 6: result += "шість ";  break;
					case 7: result += "сім ";   break;
					case 8: result += "вісім "; break;
					case 9: result += "дев'ять "; break;
					default: break;
				}
			}
			// Значение группы: тысячи, миллионы и т.д.
			switch (group) {
				
				case 1: if (rank10 == 1)
							result += "тисяч ";
					else {
						switch (rank) {
							case 1:  result += "тисяча "; break;
							case 2:
							case 3:
							case 4:  result += "тисячі "; break;
							default: result += "тисяч ";  break;
						}
					}
					break;
				case 2: if (rank10 == 1)
							result += "мільйонів ";
					else {
						switch (rank) {
							case 1:  result += "мільйон ";   break;
							case 2:
							case 3:
							case 4:  result += "мільйона ";  break;
							default: result += "мільйонів "; break;
						}
					}
					break;
				case 3: if (rank10 == 1)
							result += "мільярдів ";
					else {
						switch (rank) {
							case 1:  result += "мільярд ";   break;
							case 2:
							case 3:
							case 4:  result += "мільярда ";  break;
							default: result += "мільярдів "; break;
						}
					}
					break;
				case 4: if (rank10 == 1)
							result += "трильйонів ";
					else {
						switch (rank) {
							case 1:  result += "трильйон ";   break;
							case 2:
							case 3:
							case 4:  result += "трильйона ";  break;
							default: result += "трильйонів "; break;
						}
					}
					break;
				default:	break;
			}
			
			return result;
		}
		}
	}
