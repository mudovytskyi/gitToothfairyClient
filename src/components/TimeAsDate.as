package components
{
	public class TimeAsDate
	{
		/**
		 * Преобразование в дату. День, месяц, год устанавливаются в 0
		 */ 
		public static function getTimeAsDate(timeCalendarInterval:String):Date
		{
			var arr:Array = timeCalendarInterval.split(":");
			var time:Date = new Date(0, 0, 0, int(arr[0]), int(arr[1]), (arr.length == 3) ? int(arr[2]) : 0);
			return time;
		}
	}
}