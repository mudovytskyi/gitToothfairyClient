package components.sqlite
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import components.date.ToolsForFirebird;
	
	import modules.PatientCardModule.dictionaries.fields.PatientCardDictionaryModule;
	import modules.TreatmentManagerModule.TreatmentManagerModule;
	
	import services.ipClass;
	import services.teethexammodule.TeethExamModule;
	
	import valueObjects.AWDDoctorModuleAssistant;
	import valueObjects.AWDDoctorModuleRoom;
	import valueObjects.AWDDoctorModuleWPalce;
	import valueObjects.AccountModule_Doctor;
	import valueObjects.AccountModule_ExpensesGroup;
	import valueObjects.AccountModule_Group;
	import valueObjects.AdminModuleAssistant;
	import valueObjects.AdminModuleAutorizeObject;
	import valueObjects.AdminModuleCabinet;
	import valueObjects.AdminModuleSubdivision;
	import valueObjects.AmbulatoryCardModuleComorbidities;
	import valueObjects.AmbulatoryCardModuleDoctor;
	import valueObjects.AmbulatoryCardModuleHygiene;
	import valueObjects.AmbulatoryCardModuleOcclusions;
	import valueObjects.AssistantsModuleAssistant;
	import valueObjects.CalendarModuleDoctor;
	import valueObjects.CalendarModuleRoom;
	import valueObjects.CalendarModuleWorkPlace;
	import valueObjects.ChiefBudgetModule_Group;
	import valueObjects.ChiefHRModule_Doctor;
	import valueObjects.ChiefOlapModuleHealthproc;
	import valueObjects.ChiefSalaryModuleAssistantSalaryObject;
	import valueObjects.ChiefSalaryModuleDoctorSalaryObject;
	import valueObjects.ChiefSalaryModule_ExpensesGroup;
	import valueObjects.CommonClinicInformationModuleClinicData;
	import valueObjects.ComorbiditiesModuleDictionaryObject;
	import valueObjects.ContactBookModule_Object;
	import valueObjects.DiscountModuleDictionaryObject;
	import valueObjects.DispanserModule_Disptype;
	import valueObjects.DispanserModule_Doctor;
	import valueObjects.DoctorCardModuleDoctor;
	import valueObjects.Form039Module_F39_Record;
	import valueObjects.Form039Module_F39_Val;
	import valueObjects.HREmployee_HRModule;
	import valueObjects.HealthPlanMKB10Assistant;
	import valueObjects.HealthPlanMKB10ClinicPersonal;
	import valueObjects.HealthPlanMKB10Doctor;
	import valueObjects.HealthPlanMKB10MKB;
	import valueObjects.HealthPlanMKB10MKBProtocol;
	import valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc;
	import valueObjects.HealthPlanMKB10ModuleTreatmentProc;
	import valueObjects.HealthPlanMKB10QuickF43;
	import valueObjects.HealthPlanMKB10Template;
	import valueObjects.HygieneModuleDictionaryObject;
	import valueObjects.InsuranceCompanyProfile;
	import valueObjects.MaterialsDictionaryModuleFarm;
	import valueObjects.MaterialsDictionaryModuleFarmGroup;
	import valueObjects.MaterialsDictionaryModuleFarmSupplier;
	import valueObjects.MaterialsDictionaryModuleMaterialCardWindowData;
	import valueObjects.MaterialsDictionaryModuleSupplier;
	import valueObjects.MaterialsDictionaryModuleUnitOfMeasure;
	import valueObjects.NHDiagnosProtocol;
	import valueObjects.NHDiagnosTemplate;
	import valueObjects.NHUltraTemplateModule_ExamQuestion;
	import valueObjects.OcclusionModuleDictionaryObject;
	import valueObjects.PatientCardDictionariesModuleDictionaryObject;
	import valueObjects.PatientCardModuleDoctorObject;
	import valueObjects.PatientCardModuleInsuranceCompanyObject;
	import valueObjects.PatientCardModuleSubdivisonObject;
	import valueObjects.PatientTestModule_TestQuestion;
	import valueObjects.PersonnelModuleCabinet;
	import valueObjects.PersonnelModuleData;
	import valueObjects.PersonnelModuleRule;
	import valueObjects.PersonnelModuleSchedule;
	import valueObjects.PriceListModuleRecord;
	import valueObjects.PriceListModuleRecordFarm;
	import valueObjects.ReportsModule_Doctor;
	import valueObjects.RoomsDictionaryRoom;
	import valueObjects.RoomsDictionaryWorkPlace;
	import valueObjects.ServerFileModule_Protocol;
	import valueObjects.Storage_Farm;
	import valueObjects.Storage_FarmStorage;
	import valueObjects.Storage_Supplier;
	import valueObjects.SubscriptionDictionaryObject;
	import valueObjects.SubscriptionTypeObject;
	import valueObjects.SuppliersDictionaryModuleSupplier;
	import valueObjects.TeethExamDictionariesModuleDictionaryObject;
	import valueObjects.TeethExamModuleAssistantForTooth;
	import valueObjects.TeethExamModuleDocForTooth;
	import valueObjects.TeethExamModuleF43;
	import valueObjects.TeethExamModuleToothSOut;
	import valueObjects.TreatmentManagerModuleDoctor;
	import valueObjects.TreatmentManagerModuleF43;
	import valueObjects.TreatmentManagerModuleProtocol;
	import valueObjects.TreatmentManagerModuleTemplateProcedurFarm;
	import valueObjects.TreatmentManagerModuleTemplateProcedure;
	import valueObjects.WarehouseModuleDoctor;
	import valueObjects.WarehouseModuleFarmUnit;
	import valueObjects.WarehouseModuleRoom;
	import valueObjects.WarehouseModuleSupplier;
	
	public class SQLiteServices
	{
		//____________________________________________________________________	AccountModule____________________________________________________________________///
		//-----------------------------------------------------		accountModule_getGroups---------------------------------------------------///
		public static function accountModule_getGroups(groupRules:int, groupTypes:int, isCashpayment:int, isSalary:Boolean, isEmptyGroup:Boolean, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ACCOUNTFLOW_GROUPS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"accountflow_groups.id as id, "+
					"accountflow_groups.name as name ,"+
					"accountflow_groups.group_type as group_type ,"+
					"accountflow_groups.rules as rules ,"+
					"accountflow_groups.is_cashpayment as is_cashpayment ,"+
					"accountflow_groups.monthly_is as monthly_is ,"+
					"accountflow_groups.monthly_defaultsumm as monthly_defaultsumm ,"+
					"accountflow_groups.color as group_color ,"+
					"accountflow_groups.gridsequence as group_sequince ,"+
					"accountflow_groups.isemployee as isEmployee ,"+
					"accountflow_groups.residue as group_residue "+
					"from main.DICTIONARY accountflow_groups ";
				
				var whereArray:Array=[];
				
				
				
				
				if(groupRules != -1)
				{
					whereArray.push("accountflow_groups.rules = "+groupRules.toString());
				}      
				
				if(isCashpayment != -1)
				{
					whereArray.push("accountflow_groups.is_cashpayment = "+isCashpayment.toString());
				}
				
				if(groupTypes != -1)
				{
					whereArray.push("accountflow_groups.group_type = "+groupTypes.toString());
				}
				if(whereArray.length>0)
				{
					sql+=" where "+whereArray.join(' and ')+" ";
				}
				sql+= " order by accountflow_groups.is_cashpayment asc, accountflow_groups.name asc";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AccountModule_Group,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				/*var storageGroup:AccountModule_Group = new AccountModule_Group;
				storageGroup.id = 'storage';
				storageGroup.name = 'storageGroup';
				resultArray.addItemAt(storageGroup,0); */
				
				if(isSalary)
				{
					var salaryGroup:AccountModule_Group = new AccountModule_Group;
					salaryGroup.id = 'salary';
					salaryGroup.name = 'salaryGroup';
					resultArray.addItemAt(salaryGroup,0); 
				}
				
				
				if(isEmptyGroup)
				{
					var emptyGroup:AccountModule_Group = new AccountModule_Group;
					emptyGroup.id = '-';
					emptyGroup.name = 'emptyGroup';
					resultArray.addItemAt(emptyGroup,0); 
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		accountModule_getGroups---------------------------------------------------///
		//-----------------------------------------------------		accountModule_getExpensesGroups---------------------------------------------------///
		public static function accountModule_getExpensesGroups(isEmptyGroup:Boolean, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ACCOUNTFLOW_TEXP_GROUPS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"aftg.ID as id, "+
					"aftg.NAME as name ,"+
					"aftg.IS_CASHPAYMENT as is_cashpayment ,"+
					"aftg.COLOR as group_color ,"+
					"aftg.GRIDSEQUENCE as group_sequince "+
					"from main.DICTIONARY aftg  order by aftg.GRIDSEQUENCE asc, aftg.NAME asc";
				
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AccountModule_ExpensesGroup,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				if(isEmptyGroup)
				{
					var emptyGroup:AccountModule_ExpensesGroup = new AccountModule_ExpensesGroup;
					emptyGroup.id = 'null';
					emptyGroup.name = 'emptyGroup';
					resultArray.addItemAt(emptyGroup,0); 
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		accountModule_getExpensesGroups---------------------------------------------------///
		//-----------------------------------------------------		accountModule_getDoctors---------------------------------------------------///
		public static function accountModule_getDoctors(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"doctors.id as id, "+
					"doctors.lastname as lname,"+
					"doctors.firstname as fname,"+
					"doctors.middlename as sname, "+
					"doctors.lastname||' '||doctors.firstname as shortname "+
					"from main.DICTIONARY doctors";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AccountModule_Doctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		accountModule_getDoctors---------------------------------------------------///
		//____________________________________________________________________	AccountModule____________________________________________________________________///
		
		//____________________________________________________________________	AdminService ____________________________________________________________________///
		//-----------------------------------------------------		adminService_getCabinetsList---------------------------------------------------///
		public static function adminService_getCabinetsList(spec:Array,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ROOMS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ROOMS.ID as id, " +
					"ROOMS.NAME as name, " +
					"ROOMS.ROOMCOLOR as color " +
					"from main.DICTIONARY ROOMS ";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AdminModuleCabinet,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					for each (var room:AdminModuleCabinet in sqlRes.data) 
					{
						resultArray.addItem(room);
					}
					
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		adminService_getCabinetsList---------------------------------------------------///
		//-----------------------------------------------------		adminService_getClients---------------------------------------------------///
		public static function adminService_getClients(spec:Array,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ACCOUNT",new ArrayCollection(["DOCTORS","ASSISTANTS","SUBDIVISION"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"a.id as ACCOUNT_ID, "+
					"a.accountuser as ACCOUNT_USER, "+
					"a.accountpassword as ACCOUNT_PASSWORD, "+
					"a.lastname as ACCOUNT_LASTNAME, "+
					"a.firstname as ACCOUNT_FIRSTNAME, "+
					"a.middlename as ACCOUNT_MIDDLENAME, "+
					"coalesce(d.id,\'1_\'||ass.id) as DOCTOR_ID, "+
					"coalesce(d.shortname,ass.shortname) as DOCTOR_SHORTNAME, "+
					"d.speciality as DOCTOR_SPECIALITY, "+
					"a.access as ACCOUNT_ACCESS, "+
					"a.ACCOUNTKEY as ACCOUNT_KEY, "+
					"a.SPEC as SPEC," +
					"a.SUBDIVISIONID as SUBDIVISIONID, "+
					"s.name as SUBDIVISIONNAME, "+
					"a.ACCOUNTTYPE as ACCOUNTTYPE, "+
					"a.TAX_ID as TAX_ID, "+
					"a.BIRTH_DATE as BIRTH_DATE, "+
					"a.BIRTH_PLACE as BIRTH_PLACE, "+
					"a.GENDER as GENDER, "+
					"a.EMAIL as EMAIL, "+
					"a.USER_POSITION as USER_POSITION, "+
					"a.PHONE_TYPE as PHONE_TYPE, "+
					"a.PHONE as PHONE, "+
					"a.DOC_TYPE as DOC_TYPE, "+
					"a.DOC_NUMBER as DOC_NUMBER, "+
					"a.IS_OWNER as IS_OWNER, "+
					"a.EH_STARTDATE as EH_STARTDATE, "+
					"a.EH_ENDDATE as EH_ENDDATE, "+
					"a.EH_USER_ID as EH_USER_ID "+
					"from main.DICTIONARY a " +
					"left join DOCTORS.DICTIONARY d on d.accountid=a.id " +
					"left join ASSISTANTS.DICTIONARY ass on ass.accountid=a.id " +
					"left join SUBDIVISION.DICTIONARY s on s.id=a.SUBDIVISIONID ";
				
				var whereArray:Array=[];
				
				if(spec!=null&&spec.length>0)
				{
					whereArray.push('a.SPEC in ('+spec.join(',')+')');
					
					
				}
				
				if(whereArray.length>0)
				{
					sql+=" where "+whereArray.join(' and ')+" ";
				}
				
				sql +=' order by  a.accountuser';
				
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AdminModuleAutorizeObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					for each (var obj:AdminModuleAutorizeObject in sqlRes.data) 
					{
						obj.ACCOUNT_SHORTNAME=shortName(obj.ACCOUNT_LASTNAME, obj.ACCOUNT_FIRSTNAME, obj.ACCOUNT_MIDDLENAME);
						resultArray.addItem(obj);
					}
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		adminService_getClients---------------------------------------------------///
		//-----------------------------------------------------		adminService_getSubdivisions---------------------------------------------------///
		public static function adminService_getSubdivisions(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("SUBDIVISION",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"s.id as ID, "+
					"s.name as NAME, "+
					"s.ordernumber_cash as ORDERNUMBER_CASH, "+
					"s.ordernumber_clearing as ORDERNUMBER_CLEARING, "+
					"s.invoicenumber as INVOICENUMBER, "+
					"s.city as CITY "+
					"from main.DICTIONARY s";
				
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AdminModuleSubdivision,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		adminService_getSubdivisions---------------------------------------------------///
		//-----------------------------------------------------		adminService_getAssistantsList---------------------------------------------------///
		public static function adminService_getAssistantsList(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ASSISTANTS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as assid, LASTNAME || ' ' || FIRSTNAME as name from DICTIONARY WHERE " +
					" isfired is null or isfired != 1";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AdminModuleAssistant,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		adminService_getAssistantsList---------------------------------------------------///
		//____________________________________________________________________	AdminService ____________________________________________________________________///
		
		//____________________________________________________________________	AmbulatoryCardModule ____________________________________________________________________///
		//-----------------------------------------------------		ambulatoryCardModule_getOcclusions---------------------------------------------------///
		public static function ambulatoryCardModule_getOcclusions(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("OCCLUSIONS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select DESCRIPTION as DESCRIPTION from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AmbulatoryCardModuleOcclusions,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		ambulatoryCardModule_getOcclusions---------------------------------------------------///
		//-----------------------------------------------------		ambulatoryCardModule_getComorbidities---------------------------------------------------///
		public static function ambulatoryCardModule_getComorbidities(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("COMORBIDITIES",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select DESCRIPTION as DESCRIPTION from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AmbulatoryCardModuleComorbidities,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		ambulatoryCardModule_getComorbidities---------------------------------------------------///
		//-----------------------------------------------------		ambulatoryCardModule_getHygieneDesript---------------------------------------------------///
		public static function ambulatoryCardModule_getHygieneDesript(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("HYGIENE",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as ID, NUMBER as NUMBER, HYGDESCRIPT as HYGDESCRIPT from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AmbulatoryCardModuleHygiene,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		ambulatoryCardModule_getHygieneDesript---------------------------------------------------///
		//-----------------------------------------------------		ambulatoryCardModule_getDoctors---------------------------------------------------///
		public static function ambulatoryCardModule_getDoctors(nowdate:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"doctors.ID as ID, "+
					"doctors.SHORTNAME as SHORTNAME "+
					"from main.DICTIONARY doctors " +
					"where doctors.dateworkend is null or doctors.dateworkend>='"+nowdate+"'";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AmbulatoryCardModuleDoctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		ambulatoryCardModule_getDoctors---------------------------------------------------///
		
		//____________________________________________________________________	AmbulatoryCardModule ____________________________________________________________________///
		
		//____________________________________________________________________	AssistantsModule ____________________________________________________________________///
		//-----------------------------------------------------assistantsModule_getAssistantsFiltered---------------------------------------------------///
		public static function assistantsModule_getAssistantsFiltered(lastname:String, _isFiredFilter:int, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ASSISTANTS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"ID as ID, "+
					"TIN as TIN, "+
					"FIRSTNAME as FIRSTNAME, "+
					"MIDDLENAME as MIDDLENAME, "+
					"LASTNAME as LASTNAME, "+
					"FULLNAME as FULLNAME, "+
					"SHORTNAME as SHORTNAME, "+
					"BIRTHDAY as BIRTHDAY, "+
					"SEX as SEX, "+
					"DATEWORKSTART as DATEWORKSTART, "+
					"DATEWORKEND as DATEWORKEND, "+
					"MOBILEPHONE as MOBILEPHONE, "+
					"HOMEPHONE as HOMEPHONE, "+
					"EMAIL as EMAIL, "+
					"SKYPE as SKYPE, "+
					"HOMEADRESS as HOMEADRESS, "+
					"POSTALCODE as POSTALCODE, "+
					"STATE as STATE, "+
					"REGION as REGION, "+
					"CITY as CITY, "+
					"BARCODE as BARCODE," +
					"ISFIRED as isFired, "+
					"FINGERID as fingerid "+
					"from main.DICTIONARY ";
				var whereArray:Array=[];
				if(_isFiredFilter==0)
				{
					whereArray.push('ISFIRED=0');
				}
				if(lastname != null && lastname != "")
				{
					whereArray.push(generateSearch("LASTNAME", lastname));
				}    
				if(whereArray.length>0)
				{
					sql+=" where "+whereArray.join(' and ')+" ";
				}
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AssistantsModuleAssistant,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------assistantsModule_getAssistantsFiltered---------------------------------------------------///
		
		//____________________________________________________________________	AWDDoctorModule ____________________________________________________________________///
		//-----------------------------------------------------aWDoctorModule_getRooms---------------------------------------------------///
		public static function aWDoctorModule_getRooms(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ROOMS",new ArrayCollection(["WORKPLACES"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ROOMS.ID as ID, " +
					"ROOMS.NUMBER as roomNumber, " +
					"ROOMS.NAME as roomName " +
					"from main.DICTIONARY ROOMS ";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AWDDoctorModuleRoom,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					for each (var room:AWDDoctorModuleRoom in sqlRes.data) 
					{
						room.workPlaces=aWDoctorModule_getWorkpalces(connection,room.ID,faultHandler);
						resultArray.addItem(room);
					}
					
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		public static function aWDoctorModule_getWorkpalces(connection:SQLConnection, roomId:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select WORKPLACES.ID as ID, " +
				"WORKPLACES.NUMBER as wplaceNumber, " +
				"WORKPLACES.DESCRIPTION as wplaceName " +
				"from WORKPLACES.DICTIONARY WORKPLACES " +
				"where WORKPLACES.roomid ="+roomId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AWDDoctorModuleWPalce,faultHandler);
			var workPlaces:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				workPlaces=new ArrayCollection(sqlRes.data);
			}
			return workPlaces;
		}
		//-----------------------------------------------------aWDoctorModule_getRooms---------------------------------------------------///
		//-----------------------------------------------------		aWDoctorModule_getAssistantsList---------------------------------------------------///
		public static function aWDoctorModule_getAssistantsList(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ASSISTANTS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as assid, LASTNAME || ' ' || FIRSTNAME as name from DICTIONARY WHERE " +
					"(DATEWORKEND >= date('NOW') or DATEWORKEND IS null)";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,AWDDoctorModuleAssistant,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		aWDoctorModule_getAssistantsList---------------------------------------------------///
		//____________________________________________________________________	AWDDoctorModule ____________________________________________________________________///
		
		//____________________________________________________________________	CalendarModule ____________________________________________________________________///
		//-----------------------------------------------------		calendarModule_getDoctors---------------------------------------------------///
		public static function calendarModule_getDoctors(nowdate:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"doctors.ID as ID, "+
					"doctors.SHORTNAME as SHORTNAME "+
					"from main.DICTIONARY doctors " +
					"where doctors.isfired is null or doctors.isfired != 1";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,CalendarModuleDoctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		calendarModule_getDoctors---------------------------------------------------///
		//-----------------------------------------------------calendarModule_getRooms---------------------------------------------------///
		public static function calendarModule_getRooms(unvisibleRoomIds:ArrayCollection, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection = openLocal("ROOMS", null, faultHandler);
			
			if (connection != null) {
				var sql:String = "select ROOMS.ID as ID, " +
					"ROOMS.NUMBER as NUMBER, " +
					"ROOMS.NAME as NAME, " +
					"ROOMS.ROOMCOLOR as ROOMCOLOR, " +
					"ROOMS.GOOGLEROOMCOLORID as GOOGLEROOMCOLORID " +
					"from main.DICTIONARY ROOMS ";
				
				var whereArray:Array = [];
				
				if (unvisibleRoomIds != null && unvisibleRoomIds.length > 0) {
					whereArray.push("ROOMS.ID not in (" + unvisibleRoomIds.source.join(", ") + ")");
				}
				
				if (whereArray.length > 0) {
					sql += " where " + whereArray.join(' and ') + " ";
				}
				
				sql += "order by ROOMS.sequincenumber, ROOMS.id";
				
				var resultArray:ArrayCollection = new ArrayCollection();
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, CalendarModuleRoom, faultHandler);
				
				if (sqlRes != null && sqlRes.data != null) {	
					resultArray = new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				
				var resEvent:ResultEvent = new ResultEvent(ResultEvent.RESULT, false, true, resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------calendarModule_getRooms---------------------------------------------------///
		//-----------------------------------------------------calendarModule_getWorkPlaces---------------------------------------------------///
		
		public static function calendarModule_getWorkPlaces(roomId:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("WORKPLACES",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select WORKPLACES.ID as ID, " +
					"WORKPLACES.ROOMID as ROOMID, " +
					"WORKPLACES.NUMBER as NUMBER " +
					"from main.DICTIONARY WORKPLACES " +
					"where WORKPLACES.roomid ="+roomId;
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,CalendarModuleWorkPlace,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------calendarModule_getWorkPlaces---------------------------------------------------///
		//____________________________________________________________________	CalendarModule ____________________________________________________________________///
		//____________________________________________________________________	ChiefBudgetModule ____________________________________________________________________///
		//-----------------------------------------------------		chiefBudgetModule_getGroups---------------------------------------------------///
		public static function chiefBudgetModule_getGroups(groupRules:int, groupTypes:int, isCashpayment:int, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ACCOUNTFLOW_GROUPS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"accountflow_groups.id as id, "+
					"accountflow_groups.name as name ,"+
					"accountflow_groups.group_type as group_type ,"+
					"accountflow_groups.rules as rules ,"+
					"accountflow_groups.is_cashpayment as is_cashpayment ,"+
					"accountflow_groups.monthly_is as monthly_is ,"+
					"accountflow_groups.monthly_defaultsumm as monthly_defaultsumm ,"+
					"accountflow_groups.color as group_color ,"+
					"accountflow_groups.gridsequence as group_sequince ,"+
					"accountflow_groups.residue as group_residue, "+
					"residue.name as group_residue_name "+
					"from main.DICTIONARY accountflow_groups " +
					"left join main.DICTIONARY as residue on accountflow_groups.residue = residue.id ";
				
				var whereArray:Array=[];
				
				if(groupRules != -1)
				{
					whereArray.push("accountflow_groups.rules = "+groupRules.toString());
				}      
				
				if(isCashpayment != -1)
				{
					whereArray.push("accountflow_groups.is_cashpayment = "+isCashpayment.toString());
				}
				
				if(groupTypes != -1)
				{
					whereArray.push("accountflow_groups.group_type = "+groupTypes.toString());
				}
				if(whereArray.length>0)
				{
					sql+=" where "+whereArray.join(' and ')+" ";
				}
				sql+= " order by accountflow_groups.gridsequence";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ChiefBudgetModule_Group,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		chiefBudgetModule_getGroups---------------------------------------------------///
		//____________________________________________________________________	ChiefBudgetModule ____________________________________________________________________///
		
		//____________________________________________________________________	ChiefHRModule ____________________________________________________________________///
		//-----------------------------------------------------		chiefHRModule_getDoctors---------------------------------------------------///
		public static function chiefHRModule_getDoctors(startTime:String,endTime:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",new ArrayCollection(["ASSISTANTS"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID, SHORTNAME, IS_ASSISTANT from " +
					"(select ass.ID, ass.SHORTNAME, 1 as IS_ASSISTANT, DATEWORKEND  from ASSISTANTS.DICTIONARY ass " +
					"union " +
					"select d.ID, d.SHORTNAME, 0 as IS_ASSISTANT, DATEWORKEND from main.DICTIONARY d)" +
					" WHERE (DATEWORKEND >= '"+startTime+"' or DATEWORKEND IS null) and (DATEWORKSTART >= '"+endTime+"' or DATEWORKSTART IS null) order by IS_ASSISTANT desc";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ChiefHRModule_Doctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		chiefHRModule_getDoctors---------------------------------------------------///
		//____________________________________________________________________	ChiefHRModule ____________________________________________________________________///
		
		//____________________________________________________________________	ChiefOlapModule ____________________________________________________________________///
		//-----------------------------------------------------		chiefOlapModule_searchHealthproc---------------------------------------------------///
		public static function chiefOlapModule_searchHealthproc(search:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("HEALTHPROC",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select hp.id as ID, hp.PLANNAME as NAME " +
					"from  main.DICTIONARY hp ";
				var whereArray:Array=[];
				if(search!=null&&search.length>0)
				{
					whereArray.push(generateSearch("hp.name||' '||hp.PLANNAME", search)); 
				}
				
				if(whereArray.length>0)
				{
					sql+=" where "+whereArray.join(' and ');
				}
				sql+=" order by LOWER(hp.name) limit 10";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ChiefOlapModuleHealthproc,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		chiefOlapModule_searchHealthproc---------------------------------------------------///
		//____________________________________________________________________	ChiefOlapModule ____________________________________________________________________///
		
		//____________________________________________________________________	ChiefSalaryModule ____________________________________________________________________///
		//-----------------------------------------------------chiefSalaryModule_getDoctorSalary---------------------------------------------------///
		
		public static function chiefSalaryModule_getDoctorSalarySchema(doctorId:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS_SALARY_SCHEMA",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"doctors_salary_schema.doctorid as doctorId, " +
					"doctors_salary_schema.salaryschema as salarySchema, " +
					"doctors_salary_schema.createdate as createDate, " +
					"doctors_salary_schema.enabledfromdate as enabledFromDate, " +
					"doctors_salary_schema.id as schema_id " +
					"from main.DICTIONARY doctors_salary_schema	" +
					"where doctors_salary_schema.doctorid = "+doctorId+" " +
					"order by doctors_salary_schema.enabledfromdate desc";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ChiefSalaryModuleDoctorSalaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------chiefSalaryModule_getDoctorSalary---------------------------------------------------///
		//-----------------------------------------------------chiefSalaryModule_getAssistantSalarySchema---------------------------------------------------///
		
		public static function chiefSalaryModule_getAssistantSalarySchema(assistantId:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ASSISTANTS_SALARY_SCHEMA",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"assistants_salary_schema.assistantid as assistantId, " +
					"assistants_salary_schema.salaryschema as salarySchema, " +
					"assistants_salary_schema.createdate as createDate, " +
					"assistants_salary_schema.enabledfromdate as enabledFromDate, " +
					"assistants_salary_schema.id as schema_id " +
					"from main.DICTIONARY assistants_salary_schema " +
					"where assistants_salary_schema.assistantid = "+assistantId;
				sql+=" order by assistants_salary_schema.enabledfromdate desc";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ChiefSalaryModuleAssistantSalaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------chiefSalaryModule_getAssistantSalarySchema---------------------------------------------------///
		//-----------------------------------------------------		chiefSalaryModule_getExpensesGroups---------------------------------------------------///
		public static function chiefSalaryModule_getExpensesGroups(isEmptyGroup:Boolean, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ACCOUNTFLOW_TEXP_GROUPS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"accountflow_texp_groups.id as id, "+
					"accountflow_texp_groups.name as name, "+
					"accountflow_texp_groups.is_cashpayment as is_cashpayment, "+
					"accountflow_texp_groups.color as group_color, "+
					"accountflow_texp_groups.gridsequence as group_sequince "+
					"from main.DICTIONARY accountflow_texp_groups " +
					"order by accountflow_texp_groups.gridsequence asc, accountflow_texp_groups.name asc";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ChiefSalaryModule_ExpensesGroup,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				if(isEmptyGroup)
				{
					var emptyGroup:ChiefSalaryModule_ExpensesGroup = new ChiefSalaryModule_ExpensesGroup;
					emptyGroup.id = 'null';
					emptyGroup.name = 'emptyGroup';
					resultArray.addItemAt(emptyGroup,0); 
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		chiefSalaryModule_getExpensesGroups---------------------------------------------------///
		//____________________________________________________________________	ChiefSalaryModule ____________________________________________________________________///
		
		//____________________________________________________________________	CommonClinicInformationModule ____________________________________________________________________///
		//-----------------------------------------------------		commonClinicInformationModule_getClinicRegistrationData---------------------------------------------------///
		public static function commonClinicInformationModule_getClinicRegistrationData(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("CLINICREGISTRATIONDATA",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"clinicregistrationdata.id as ID, "+
					"clinicregistrationdata.name as NAME, "+
					"clinicregistrationdata.legalname as LEGALNAME, "+
					"clinicregistrationdata.address as ADDRESS, "+
					"clinicregistrationdata.organizationtype as ORGANIZATIONTYPE, "+
					"clinicregistrationdata.code as CODE, "+
					"clinicregistrationdata.director as DIRECTOR, "+
					"clinicregistrationdata.telephonenumber as TELEPHONENUMBER, "+
					"clinicregistrationdata.fax as FAX, "+
					"clinicregistrationdata.sitename as SITENAME, "+
					"clinicregistrationdata.email as EMAIL, "+
					"clinicregistrationdata.icq as ICQ, "+
					"clinicregistrationdata.skype as SKYPE, "+
					"clinicregistrationdata.logo as LOGO, "+
					"clinicregistrationdata.chiefaccountant as CHIEFACCOUNTANT, "+
					"clinicregistrationdata.legaladdress as LEGALADDRESS, "+
					"clinicregistrationdata.bank_name as BANK_NAME, "+
					"clinicregistrationdata.bank_mfo as BANK_MFO, "+
					"clinicregistrationdata.bank_currentaccount as BANK_CURRENTACCOUNT, "+
					"clinicregistrationdata.license_number as LICENSE_NUMBER, "+
					"clinicregistrationdata.license_series as LICENSE_SERIES, "+
					"clinicregistrationdata.license_issued as LICENSE_ISSUED, "+
					"clinicregistrationdata.license_startdate as LICENSE_STARTDATE, "+
					"clinicregistrationdata.license_enddate as LICENSE_ENDDATE, "+
					"clinicregistrationdata.authority as AUTHORITY, "+
					"clinicregistrationdata.city as CITY, "+
					"clinicregistrationdata.SHORT_NAME as SHORT_NAME, "+
					"clinicregistrationdata.MSP_TYPE as MSP_TYPE, "+
					"clinicregistrationdata.OWNER_PROPERTY_TYPE as OWNER_PROPERTY_TYPE, "+
					"clinicregistrationdata.LEGAL_FORM as LEGAL_FORM, "+
					"clinicregistrationdata.KVEDS as KVEDS, "+
					"clinicregistrationdata.ADDRESSLEGAL_ZIP as ADDRESSLEGAL_ZIP, "+
					"clinicregistrationdata.ADDRESSLEGAL_TYPE as ADDRESSLEGAL_TYPE, "+
					"clinicregistrationdata.ADDRESSLEGAL_COUNTRY as ADDRESSLEGAL_COUNTRY, "+
					"clinicregistrationdata.ADDRESSLEGAL_AREA as ADDRESSLEGAL_AREA, "+
					"clinicregistrationdata.ADDRESSLEGAL_REGION as ADDRESSLEGAL_REGION, "+
					"clinicregistrationdata.ADDRESSLEGAL_SETTLEMENT as ADDRESSLEGAL_SETTLEMENT, "+
					"clinicregistrationdata.ADDRESSLEGAL_SETTLEMENT_TYPE as ADDRESSLEGAL_SETTLEMENT_TYPE, "+
					"clinicregistrationdata.ADDRESSLEGAL_SETTLEMENT_ID as ADDRESSLEGAL_SETTLEMENT_ID, "+
					"clinicregistrationdata.ADDRESSLEGAL_STREET_TYPE as ADDRESSLEGAL_STREET_TYPE, "+
					"clinicregistrationdata.ADDRESSLEGAL_STREET as ADDRESSLEGAL_STREET, "+
					"clinicregistrationdata.ADDRESSLEGAL_BUILDING as ADDRESSLEGAL_BUILDING, "+
					"clinicregistrationdata.ADDRESSLEGAL_APRT as ADDRESSLEGAL_APRT, "+
					"clinicregistrationdata.PHONE_TYPE as PHONE_TYPE, "+
					"clinicregistrationdata.PHONE as PHONE, "+
					"clinicregistrationdata.ACR_CATEGORY as ACR_CATEGORY, "+
					"clinicregistrationdata.ACR_ISSUED_DATE as ACR_ISSUED_DATE, "+
					"clinicregistrationdata.ACR_EXPIRY_DATE as ACR_EXPIRY_DATE, "+
					"clinicregistrationdata.ACR_ORDER_NO as ACR_ORDER_NO, "+
					"clinicregistrationdata.ACR_ORDER_DATE as ACR_ORDER_DATE, "+
					"clinicregistrationdata.CONSENT_SIGN as CONSENT_SIGN, "+
					"clinicregistrationdata.EH_ID as EH_ID, "+
					"clinicregistrationdata.EH_STATUS as EH_STATUS, "+
					"clinicregistrationdata.EH_CREATED_BY_MIS_CLIENT_ID as EH_CREATED_BY_MIS_CLIENT_ID "+
					
					
					"from main.DICTIONARY clinicregistrationdata";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,CommonClinicInformationModuleClinicData,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		commonClinicInformationModule_getClinicRegistrationData---------------------------------------------------///
		//____________________________________________________________________	CommonClinicInformationModule ____________________________________________________________________///
		
		//____________________________________________________________________	ComorbiditiesModule ____________________________________________________________________///
		//-----------------------------------------------------		comorbiditiesModule_getComorbidities---------------------------------------------------///
		public static function comorbiditiesModule_getComorbidities(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("COMORBIDITIES",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as ID, NUMBER as NUMBER, DESCRIPTION as DESCRIPTION from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ComorbiditiesModuleDictionaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		comorbiditiesModule_getComorbidities---------------------------------------------------///
		//____________________________________________________________________	ComorbiditiesModule ____________________________________________________________________///
		
		//____________________________________________________________________	ContactBookModule ____________________________________________________________________///
		//-----------------------------------------------------		contactBookModule_getContacts---------------------------------------------------///
		public static function contactBookModule_getContacts(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("CONTACTS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"contacts.id as id, "+
					"contacts.name as name, "+
					"contacts.comment as comment, "+
					"contacts.phones as phones, "+
					"contacts.address as address "+
					"from main.DICTIONARY contacts";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ContactBookModule_Object,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		contactBookModule_getContacts---------------------------------------------------///
		//____________________________________________________________________	ContactBookModule ____________________________________________________________________///
		
		//____________________________________________________________________	DiscountModule ____________________________________________________________________///
		//-----------------------------------------------------		discountModule_getDiscountDictionary---------------------------------------------------///
		public static function discountModule_getDiscountDictionary(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DISCOUNT_SCHEMA",null,faultHandler);
			if(connection!=null)
			{
				
				var sql:String = "select ds.ID as ID, " +
					"ds.SUMM as SUMM, " +
					"ds.PERCENT as PERCENT " +
					"from main.DICTIONARY ds order by ds.SUMM";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,DiscountModuleDictionaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		discountModule_getDiscountDictionary---------------------------------------------------///
		//____________________________________________________________________	DiscountModule ____________________________________________________________________///
		
		//____________________________________________________________________	DispanserModule ____________________________________________________________________///
		//-----------------------------------------------------		dispanserModule_getDisptypes---------------------------------------------------///
		public static function dispanserModule_getDisptypes(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PATIENTCARDDICTIONARIES",null,faultHandler);
			if(connection!=null)
			{
				
				var sql:String = "select pcd.ID as id, " +
					"pcd.DESCRIPTION as name, " +
					"pcd.COLOR as color " +
					"from main.DICTIONARY pcd where pcd.TYPE="+PatientCardDictionaryModule.TYPE_DISPANCER.toString()+" order by pcd.NUMBER";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,DispanserModule_Disptype,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		dispanserModule_getDisptypes---------------------------------------------------///
		//-----------------------------------------------------		dispanserModule_getDoctors---------------------------------------------------///
		public static function dispanserModule_getDoctors(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				
				var sql:String = "SELECT ID as id, " +
					"SHORTNAME as name, " +
					"HEALTHTYPE as healthtype from main.DICTIONARY " +
					"WHERE isfired is null or isfired != 1 " +
					"order by SHORTNAME";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,DispanserModule_Doctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				var emptyDoc:DispanserModule_Doctor=new DispanserModule_Doctor;
				emptyDoc.id = "empty";
				emptyDoc.name = '-';
				resultArray.addItemAt(emptyDoc,0);
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		dispanserModule_getDoctors---------------------------------------------------///
		//____________________________________________________________________	DispanserModule ____________________________________________________________________///
		
		
		//____________________________________________________________________	DoctorCardModule ____________________________________________________________________///
		//-----------------------------------------------------doctorCardModule_getDoctorsFiltered---------------------------------------------------///
		public static function doctorCardModule_getDoctorsFiltered(lastname:String,_isFiredFilter:int,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"ID as ID, "+
					"TIN as TIN, "+
					"FIRSTNAME as FIRSTNAME, "+
					"MIDDLENAME as MIDDLENAME, "+
					"LASTNAME as LASTNAME, "+
					"FULLNAME as FULLNAME, "+
					"SHORTNAME as SHORTNAME, "+
					"BIRTHDAY as BIRTHDAY, "+
					"SEX as SEX, "+
					"SPECIALITY as SPECIALITY, "+
					"DATEWORKSTART as DATEWORKSTART, "+
					"DATEWORKEND as DATEWORKEND, "+
					"MOBILEPHONE as MOBILEPHONE, "+
					"HOMEPHONE as HOMEPHONE, "+
					"EMAIL as EMAIL, "+
					"SKYPE as SKYPE, "+
					"HOMEADRESS as HOMEADRESS, "+
					"POSTALCODE as POSTALCODE, "+
					"STATE as STATE, "+
					"REGION as REGION, "+
					"CITY as CITY, "+
					"BARCODE as BARCODE, "+
					"ISFIRED as isFired, "+
					"FINGERID as fingerid "+
					"from main.DICTIONARY ";
				var whereArray:Array=[];
				if(_isFiredFilter==0)
				{
					whereArray.push('ISFIRED=0');
				}
				
				if(lastname != null && lastname != "")
				{
					whereArray.push(generateSearch("LASTNAME",lastname));
				}    
				if(whereArray.length>0)
				{
					sql+=" where "+whereArray.join(' and ')+" ";
				}
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,DoctorCardModuleDoctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------doctorCardModule_getDoctorsFiltered---------------------------------------------------///
		//____________________________________________________________________	DoctorCardModule ____________________________________________________________________///
		
		//____________________________________________________________________	Form039Module ____________________________________________________________________///
		//-----------------------------------------------------		form039Module_get039Fields---------------------------------------------------///
		public static function form039Module_get039Fields(type:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("F39",null,faultHandler);
			if(connection!=null)
			{
				var f39List:ArrayCollection=form039Module_get039FieldsRecoursive(connection, null, faultHandler);
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,f39List);
				resultHandler(resEvent);
			}
			
		}
		public static function form039Module_get039FieldsRecoursive(connection:SQLConnection, parrentId:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select f39.id as ID, " +
				"f39.number as NUMBER, " +
				"f39.description as DESCRIPTION, " +
				"f39.TYPE as TYPE " +
				"FROM main.DICTIONARY f39 " +
				"where f39.PARRENT "+(parrentId==null?"is null":("= "+parrentId))+" and f39.TYPE>-1";
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,Form039Module_F39_Record,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				var f39List:ArrayCollection=new ArrayCollection();
				for each (var f39Item:Form039Module_F39_Record in sqlRes.data) 
				{
					if(f39Item.TYPE>0)
					{
						f39Item.CHILDREN=form039Module_get039FieldsRecoursive(connection, f39Item.ID, faultHandler);
					}
					f39List.addItem(f39Item);
				}
				return f39List;
			}
			else
			{
				return new ArrayCollection;
			}
		}
		//-----------------------------------------------------		form039Module_get039Fields---------------------------------------------------///
		//-----------------------------------------------------		form039Module_get039ProcValues---------------------------------------------------///
		
		public static function form039Module_get039ProcValues(connection:SQLConnection, procId:String, faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select F39ID as ID, UNSIGNED FROM F39HPROC.DICTIONARY where HEALTHPROCID="+procId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,Form039Module_F39_Val,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				var f39s:ArrayCollection=new ArrayCollection(sqlRes.data);
				return f39s;
			}
			else
			{
				return new ArrayCollection;
			}
		}
		//-----------------------------------------------------		form039Module_get039ProcValues---------------------------------------------------///
		//____________________________________________________________________	Form039Module ____________________________________________________________________///
		
		//____________________________________________________________________	HealthPlanMKB10Module ____________________________________________________________________///
		//-----------------------------------------------------		healthPlanMKB10Module_getDoctors---------------------------------------------------///
		public static function healthPlanMKB10Module_getDoctors(nowdate:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",new ArrayCollection(["ASSISTANTS"]),faultHandler);
			if(connection!=null)
			{
				var personal:HealthPlanMKB10ClinicPersonal=new HealthPlanMKB10ClinicPersonal;
				personal.doctors=healthPlanMKB10Module_getPersonalDoctors(connection, nowdate, faultHandler);
				personal.assistants=healthPlanMKB10Module_getPersonalAssistants(connection, nowdate, faultHandler);
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,personal);
				resultHandler(resEvent);
			}
			
		}
		public static function healthPlanMKB10Module_getPersonalDoctors(connection:SQLConnection, nowdate:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select "+
				"doctors.ID as docid, "+
				"doctors.SHORTNAME as name, "+
				"doctors.HEALTHTYPE as healthtype "+
				"from main.DICTIONARY doctors " +
				"where doctors.isfired is null or doctors.isfired != 1";
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10Doctor,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		public static function healthPlanMKB10Module_getPersonalAssistants(connection:SQLConnection, nowdate:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select "+
				"ass.ID as assid, "+
				"ass.SHORTNAME as name "+
				"from ASSISTANTS.DICTIONARY ass " +
				"where ass.isfired is null or ass.isfired != 1";
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10Assistant,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		//-----------------------------------------------------		healthPlanMKB10Module_getDoctors---------------------------------------------------///
		//-----------------------------------------------------healthPlanMKB10Module_getProtocolText---------------------------------------------------///
		public static function healthPlanMKB10Module_getProtocolText(protocolId:int,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS",null,faultHandler);
			if(connection!=null)
			{
				
				var sql:String = "select pr.protocoltxt as PROTOCOLTXT " +
					"from main.DICTIONARY pr where pr.id="+protocolId.toString();
				
				var resultString:String;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,null,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
				{	
					resultString=sqlRes.data[0].PROTOCOLTXT.toString();
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultString);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------healthPlanMKB10Module_getProtocolText---------------------------------------------------///
		//-----------------------------------------------------		healthPlanMKB10Module_getTreatmentProcArrayForDictionary---------------------------------------------------///
		public static function healthPlanMKB10Module_getTreatmentProcArrayForDictionary(joinId:int, querymode:String, searchText:String, isGrouping:Boolean, priceIndex:int, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("HEALTHPROC",new ArrayCollection(["HEALTHPROCFARM","TEMPLATEPROC","TEMPLATE","FARM","FARMGROUP","UNITOFMEASURE","F39HPROC"]),faultHandler);
			if(connection!=null)
			{
				var whereArray:Array=[];
				if(searchText!=null)
				{
					whereArray.push(generateSearch("p.planname||' '||p.shifr",searchText)); 
				}
				if(querymode == 'protocol')//protocols procedures
				{
					whereArray.push("p.nodetype = 1");
					whereArray.push("t.protocol  = "+joinId.toString());
				}
				else if(querymode == 'template')//template procedures
				{
					whereArray.push("p.nodetype = 1");
					whereArray.push("tp.template  = "+joinId.toString());
				}
				else if(querymode == 'general')
				{
					whereArray.push("p.nodetype = 1");
					whereArray.push("p.TOOTHUSE = 0");
				}
				else if(isNaN(parseInt(querymode))==false)//procedures by healthtype
				{
					whereArray.push("'|'||coalesce(p.HEALTHTYPES,'')||'|'||coalesce(p.HEALTHTYPE,'')||'|' like '%|"+querymode+"|%'");  
					if(isGrouping==false)
					{
						whereArray.push("p.nodetype = 1");
					}
				}
				else if(isGrouping==false)
				{
					whereArray.push("p.nodetype = 1");
				}
				
				var whereStr:String="";
				if(whereArray.length>0)
				{
					whereStr=whereArray.join(' and ');
					
					if(isGrouping)
					{
						var parentIDs:ArrayCollection=SQLiteServices.priceListModule_getParrentIdsRecoursive(" and "+whereStr,connection,faultHandler);
						
						if(parentIDs!=null&&parentIDs.length>0)
						{	
							whereStr =" (p.id in ("+parentIDs.source.join(", ")+") or ("+whereStr+"))";
						}
					}
				}
				
				
				var priceList:ArrayCollection=healthPlanMKB10Module_getTreatmentProcArrayForDictionaryRecursive(connection,null,whereStr,joinId,querymode,isGrouping,priceIndex,faultHandler);
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,priceList);
				resultHandler(resEvent);
				
			}
			
		}
		
		public static function healthPlanMKB10Module_getTreatmentProcArrayForDictionaryRecursive(connection:SQLConnection, parentID:String, whereStr:String, diagnos2id:int, querymode:String, isGrouping:Boolean, priceIndex:int, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select distinct(p.ID) as healthproc_fk, " +
				"p.SHIFR as procShifr, " +
				"p.NAME as courseName, " +
				"p.PLANNAME as planName, " +
				"p.UOP as procUOP, " +
				"p.PROCTIME as procTime, " +
				"p.PRICE"+sql_checkPriceIndex(priceIndex)+" as procPrice, "+
				(querymode == 'template'?" tp.visit":"1")+" as visit, " +
				"p.TOOTHUSE as toothUse, " +
				"p.HEALTHTYPE as procHealthtype, " +
				"p.EXPENSES as procExpenses, " +
				"1 as proc_count, " +
				"0 as isSaved, " +
				"0 as isComplete, " +
				"p.DISCOUNT_FLAG as discount_flag, " +
				"p.SALARY_SETTINGS as salary_settings, "+
				"p.COMPLETEEXAMDIAG as complete_examdiag, " +
				"p.HEALTHTYPES as procHealthtypes, " +
				"p.nodetype as NODETYPE " +
				"from main.DICTIONARY p ";
			
			
			if(querymode == 'protocol')//protocols procedures
			{
				sql+= "inner join TEMPLATEPROC.DICTIONARY tp on tp.healthproc = p.id " +
					"inner join TEMPLATE.DICTIONARY t on t.id = tp.template ";
				
			}
			else if(querymode == 'template')
			{
				sql+= "inner join TEMPLATEPROC.DICTIONARY tp on tp.healthproc = p.id ";
			}
			
			if(querymode == 'protocol')//protocols procedures
			{ 
				if(whereStr!=null&&whereStr.length>0)
				{
					sql+=" where ";
				}
				
				sql+=whereStr+" order by p.PARENTID, p.SEQUINCENUMBER";
			}
			else if(querymode == 'template')
			{
				if(whereStr!=null&&whereStr.length>0)
				{
					sql+=" where ";
				}
				sql+=whereStr+" order by tp.visit, tp.stacknumber";
			}
			else if(querymode == 'general')
			{
				if(whereStr!=null&&whereStr.length>0)
				{
					sql+=" where ";
				}
				sql+=whereStr+" order by p.PARENTID, p.SEQUINCENUMBER";
			}
			else//procedures by healthtype and all
			{
				if(isGrouping)
				{
					if ((parentID == null) || (parentID == ""))
					{
						sql+= " where p.PARENTID is null";                  
					}			
					else 
					{
						
						sql+= " where p.PARENTID = "+parentID;
					}
					if(whereStr!=null&&whereStr.length>0)
					{
						sql+=" and ";
					}
				}
				else if(whereStr!=null&&whereStr.length>0)
				{
					sql+=" where ";
				}
				sql+=whereStr+" order by p.SEQUINCENUMBER";
			}
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10ModuleTreatmentProc,faultHandler);
			
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				var priceList:ArrayCollection=new ArrayCollection();
				for each (var priceItem:HealthPlanMKB10ModuleTreatmentProc in sqlRes.data) 
				{
					priceItem.label=priceItem.planName;
					if(priceItem.procHealthtypes != null && priceItem.procHealthtypes != "-1"){
						priceItem.procHealthtype = int(priceItem.procHealthtypes.charAt(0));
					}
					else{
						priceItem.procHealthtype = -1;
					}
					if(priceItem.NODETYPE=="0"&&isGrouping)
					{
						priceItem.children=healthPlanMKB10Module_getTreatmentProcArrayForDictionaryRecursive(connection,priceItem.healthproc_fk,whereStr,diagnos2id,querymode,isGrouping,priceIndex, faultHandler);
						
					}
					else
					{
						priceItem.F39=form039Module_get039ProcValues(connection,priceItem.healthproc_fk,faultHandler);
						priceItem.farms=healthPlanMKB10Module_getFarms(connection,priceItem.healthproc_fk,faultHandler);
					}
					priceList.addItem(priceItem);
				}
				return priceList;
			}
			else
			{
				return new ArrayCollection();
			}
		}
		public static function healthPlanMKB10Module_getFarms(connection:SQLConnection, procId:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select " +
				"FARM.ID as farmID, " +
				"FARM.NAME as farmName, " +
				"FARMGROUP.CODE as farmGroup, " +
				"HEALTHPROCFARM.QUANTITY as farmQuantity, " +
				"0 as farmPrice, " +
				"UNITOFMEASURE.NAME as farmUnitofmeasure "+	
				"from HEALTHPROCFARM.DICTIONARY HEALTHPROCFARM " +
				"left join FARM.DICTIONARY FARM on HEALTHPROCFARM.FARM = FARM.ID " +
				"left join UNITOFMEASURE.DICTIONARY UNITOFMEASURE on (FARM.ID = UNITOFMEASURE.FARMID  and UNITOFMEASURE.isbase = 1) " +
				"left join FARMGROUP.DICTIONARY FARMGROUP on FARM.FARMGROUPID = FARMGROUP.ID " +
				"where HEALTHPROCFARM.HEALTHPROC="+procId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10ModuleTreatmentFarmsProc,faultHandler);
			var farms:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				farms=new ArrayCollection(sqlRes.data);
			}
			return farms;
		}
		//-----------------------------------------------------		healthPlanMKB10Module_getTreatmentProcArrayForDictionary---------------------------------------------------///
		//-----------------------------------------------------		healthPlanMKB10Module_getFarmsForProcDictionary---------------------------------------------------///
		public static function healthPlanMKB10Module_getFarmsForProcDictionary(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("FARM",new ArrayCollection(["FARMGROUP","UNITOFMEASURE"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"FARM.ID as farmID, " +
					"FARM.NAME as farmName, " +
					"FARMGROUP.CODE as farmGroup, " +
					"UNITOFMEASURE.NAME as farmUnitofmeasure, "+	
					"0 as farmPrice, " +
					"1 as farmQuantity " +
					"from main.DICTIONARY FARM " +
					"left join UNITOFMEASURE.DICTIONARY UNITOFMEASURE on (FARM.ID = UNITOFMEASURE.FARMID  and UNITOFMEASURE.isbase = 1) " +
					"left join FARMGROUP.DICTIONARY FARMGROUP on FARM.FARMGROUPID = FARMGROUP.ID " +
					"where FARM.nodetype = 1 and UNITOFMEASURE.isbase = 1 ";
				
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10ModuleTreatmentFarmsProc,faultHandler);
				var resultArray:ArrayCollection=new ArrayCollection;
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		healthPlanMKB10Module_getFarmsForProcDictionary---------------------------------------------------///
		//-----------------------------------------------------healthPlanMKB10Module_searchProtocols---------------------------------------------------///
		public static function healthPlanMKB10Module_searchProtocols(search:String,specialities:ArrayCollection,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS",new ArrayCollection(["PROTOCOLS_F43","TEMPLATE","MKB10", "HEALTHPROC","TEMPLATEPROC"]),faultHandler);
			if(connection!=null)
			{
				
				var sql:String = "select pr.ID as ID, " +
					"pr.name as NAME, " +
					"pr.healthtype as healthtype " +
					"from main.DICTIONARY pr " +
					"left join PROTOCOLS_F43.DICTIONARY p43 on (pr.id = p43.protocol_fk) " +
					"where p43.protocol_fk is null ";
				if(search!=null&&search.length>0)
				{
					sql+="and "+generateSearch("pr.name",search);
				}
				if(specialities!=null&&specialities.length>0)
				{
					
					var whereArray:Array=[];
					for each (var spec:Object in specialities) 
					{
						whereArray.push("pr.healthtype="+spec.id.toString());
					}
					
					if(whereArray.length>0)
					{
						whereArray.push("pr.healthtype is null");
						sql+=" and ("+whereArray.join(' or ')+")";
					}
				}  
				sql+=" order by pr.name";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10MKBProtocol,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					for each (var protocol:HealthPlanMKB10MKBProtocol in sqlRes.data) 
					{
						protocol.MKB10=healthPlanMKB10Module_getProtocolsMKB10(protocol.ID,connection,faultHandler);
						protocol.Templates=new ArrayCollection;
						var templates:ArrayCollection=healthPlanMKB10Module_getProtocolsTemplates(protocol,connection,faultHandler);
						protocol.Templates=templates;
						protocol.F43list=new ArrayCollection;
						resultArray.addItem(protocol);
					}
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		public static function healthPlanMKB10Module_getProtocolsMKB10(protocolId:int,connection:SQLConnection,faultHandler:Function=null):HealthPlanMKB10MKB
		{
			var sql:String = "select  mkb10.id as ID, " +
				"mkb10.shifr as SHIFR, " +
				"mkb10.name as NAME " +
				"from main.DICTIONARY pr " +
				"left join mkb10.DICTIONARY mkb10 on (mkb10.id = pr.mkb10) " +
				"where pr.id ="+protocolId.toString();
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10MKB,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				return sqlRes.data[0] as HealthPlanMKB10MKB;
			}
			else
			{
				return null;
			}
			
			
		}
		
		public static function healthPlanMKB10Module_getProtocolsTemplates(protocol:HealthPlanMKB10MKBProtocol,connection:SQLConnection,faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select t.id as ID, " +
				"t.name as NAME, " +
				"sum(hp.price) as cost, " +
				"group_concat('<font size=\"13\"><b>'||tp.visit||'</b></font> : '||hp.planname||'<br/>','') as toolTip " +
				"from TEMPLATE.DICTIONARY t " +
				"left join TEMPLATEPROC.DICTIONARY tp on (tp.template = t.id) " +
				"left join HEALTHPROC.DICTIONARY hp on (hp.id = tp.healthproc) " +
				"where t.protocol = "+protocol.ID.toString()+" group by  t.id, t.name, t.protocol";
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10Template,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection;
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				for each (var template:HealthPlanMKB10Template in sqlRes.data) 
				{
					if(!isNaN(template.cost)&&template.cost!=0)
					{
						template.toolTip+='<font size="15"><b>∑'+template.cost+'</b></font>';
					}
					template.Protocol=protocol;
					resultArray.addItem(template);
				}
			}
			return resultArray;
			
			
			
		}
		//-----------------------------------------------------healthPlanMKB10Module_searchProtocols---------------------------------------------------///
		
		//-----------------------------------------------------healthPlanMKB10Module_getF43---------------------------------------------------///
		public static function healthPlanMKB10Module_getF43(isQuick:Boolean, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("F43",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select f43.ID as ID, " +
					"f43.F43 as F43, " +
					"SIGN||' - '||NAME as NAME "+
					"from main.DICTIONARY f43 " +
					(isQuick?"where ISQUICK=1 ":"")+
					"order by SEQNUMBER";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10QuickF43,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		healthPlanMKB10Module_getF43---------------------------------------------------///
		
		
		
		
		//-----------------------------------------------------		nhDiagnos_getLazyProtocols---------------------------------------------------///
		public static function nhDiagnos_getLazyProtocols(searchText:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select p.ID as protocol_id, p.NAME as protocol_name from main.DICTIONARY p";
				if(searchText!=null && searchText.length>0)
				{
					sql+=" where "+generateSearch("name",searchText);
				}
				sql+=" order by name";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, NHDiagnosProtocol, faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		nhDiagnos_getLazyProtocols---------------------------------------------------///
		
		
		//-----------------------------------------------------		nhDiagnos_getTemplatesByProtocolId---------------------------------------------------///
		public static function nhDiagnos_getTemplatesByProtocolId(protocolId:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("TEMPLATE",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select id as template_id, name as template_name from main.DICTIONARY where protocol = " + protocolId + " order by name";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, NHDiagnosTemplate, faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		nhDiagnos_getTemplatesByProtocolId---------------------------------------------------///
		
		
		//-----------------------------------------------------healthPlanMKB10Module_getQuickProtocols---------------------------------------------------///
		public static function healthPlanMKB10Module_getQuickProtocols(f43Id:int, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS_F43",new ArrayCollection(["PROTOCOLS"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"p.id as ID, "+
					"p.name as NAME "+
					"from main.DICTIONARY pf "+
					"left join PROTOCOLS.DICTIONARY p on p.id=pf.protocol_fk "+
					"where pf.f43_fk="+f43Id.toString()+" and p.isquick=1";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10MKBProtocol,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		healthPlanMKB10Module_getQuickProtocols---------------------------------------------------///
		//-----------------------------------------------------healthPlanMKB10Module_getQuickTemplates---------------------------------------------------///
		public static function healthPlanMKB10Module_getQuickTemplates(protocolId:int, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("TEMPLATE",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select t.id as ID, " +
					"t.name as NAME " +
					"from main.DICTIONARY t " +
					"where t.protocol="+protocolId.toString();
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10Template,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		healthPlanMKB10Module_getQuickTemplates---------------------------------------------------///
		//-----------------------------------------------------		healthPlanMKB10Module_getProtocolWithASR---------------------------------------------------///
		public static function healthPlanMKB10Module_getProtocolWithASR(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS",new ArrayCollection(["PROTOCOLS_F43"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select t.id as ID, " +
					"t.name as NAME, " +
					"t.anamnestxt as ANAMNESTXT, " +
					"t.statustxt as STATUSTXT, " +
					"t.recomendtxt as RECOMENDTXT, " +
					"t.epicrisistxt as EPICRISISTXT " +
					
					"from main.DICTIONARY t " +
					"where (select count(*) from PROTOCOLS_F43.DICTIONARY protocols_f43 where t.id = protocols_f43.protocol_fk) > 0 " +
					"order by t.name";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HealthPlanMKB10MKBProtocol,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		healthPlanMKB10Module_getProtocolWithASR---------------------------------------------------///
		//____________________________________________________________________	HealthPlanMKB10Module ____________________________________________________________________///
		
		//____________________________________________________________________	HRModule ____________________________________________________________________///
		//-----------------------------------------------------hRModule_getEmployeeLazyList---------------------------------------------------///
		public static function hRModule_getEmployeeLazyList(search:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",new ArrayCollection(["ASSISTANTS"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"employee_id, " +
					"employee_fname, " +
					"employee_sname, " +
					"employee_lname, " +
					"employee_fio," +
					"employee_label," +
					"employee_type " +
					"from " +
					"(select " +
					"assistants.id as employee_id, " +
					"assistants.firstname as employee_fname, " +
					"assistants.middlename as employee_sname, " +
					"assistants.lastname as employee_lname, " +
					"assistants.fullname as employee_fio, " +
					"'A: '||assistants.fullname as employee_label, " +
					"'assist' as employee_type " +
					"  from ASSISTANTS.DICTIONARY assistants where (assistants.isfired is null or assistants.isfired != 1)" +
					"union " +
					"select " +
					"d.id as employee_id, " +
					"d.firstname as employee_fname, " +
					"d.middlename as employee_sname, " +
					"d.lastname as employee_lname, " +
					"d.fullname as employee_fio, " +
					"'D: '||d.fullname as employee_label, " +
					"'doc' as employee_type " +
					"from main.DICTIONARY d where (d.isfired is null or d.isfired != 1))";
				if(search!=null&&search.length>0)
				{
					sql+=" where "+generateSearch("employee_fio",search);
				}
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HREmployee_HRModule,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------hRModule_getEmployeeLazyList---------------------------------------------------///
		
		//____________________________________________________________________	HygieneModule ____________________________________________________________________///
		//-----------------------------------------------------		hygieneModule_getHygiene---------------------------------------------------///
		public static function hygieneModule_getHygiene(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("HYGIENE",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as ID, NUMBER as NUMBER, HYGDESCRIPT as HYGDESCRIPT from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,HygieneModuleDictionaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		hygieneModule_getHygiene---------------------------------------------------///
		//____________________________________________________________________	HygieneModule ____________________________________________________________________///
		
		//____________________________________________________________________	InsuranceCompanyModule ____________________________________________________________________///
		//-----------------------------------------------------		insuranceCompanyModule_getCompanys---------------------------------------------------///
		public static function insuranceCompanyModule_getCompanys(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("INSURANCECOMPANYS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as Id, " +
					"NAME as Name, " +
					"PHONE as Phone, " +
					"ADDRESS as Address, " +
					"IDENTYNUM as IdentyNum, " +
					"NAMELEGAL as NameLegal, " +
					"ADDRESSLEGAL as AddressLegal, " +
					"ACCOUNTINGNUMBER as AccountingNumber, " +
					"ACCOUNTINGMFO as AccountingMFO, " +
					"ACCOUNTINGBANK as AccountingBank " +
					"from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,InsuranceCompanyProfile,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		insuranceCompanyModule_getCompanys---------------------------------------------------///
		//____________________________________________________________________	InsuranceCompanyModule ____________________________________________________________________///
		
		//____________________________________________________________________	MaterialsDictionaryModule ____________________________________________________________________///
		//-----------------------------------------------------		materialsDictionaryModule_getMaterialsDictionaryModuleData---------------------------------------------------///
		public static function materialsDictionaryModule_getMaterialsDictionaryModuleData(searchFields:String, searchText:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("FARM",new ArrayCollection(["FARMGROUP","UNITOFMEASURE"]),faultHandler);
			if(connection!=null)
			{
				var whereArray:Array=[];
				if(searchText!=null)
				{
					whereArray.push(generateSearch("f."+searchFields.replace("||' '||","||' '||f."),searchText));
				}
				
				
				var whereStr:String="";
				if(whereArray.length>0)
				{
					whereStr=whereArray.join(' and ');
					
					var parentIDs:ArrayCollection=SQLiteServices.materialsDictionaryModule_getParrentIdsRecoursive(" and "+whereStr,connection,faultHandler);
					
					if(parentIDs!=null&&parentIDs.length>0)
					{	
						whereStr =" (f.id in ("+parentIDs.source.join(", ")+") or ("+whereStr+"))";
					}
				}
				
				
				var resultArray:ArrayCollection=materialsDictionaryModule_getMaterialsDictionaryModuleDataRecursive(connection,null,whereStr,faultHandler);
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
				
			}
			
		}
		public static function materialsDictionaryModule_getParrentIdsRecoursive(whereStr:String, connection:SQLConnection, faultHandler:Function):ArrayCollection
		{
			var sql:String = "select  distinct f.PARENTID from main.DICTIONARY f where f.PARENTID is not null"+whereStr;
			
			var parentIDs:ArrayCollection=new ArrayCollection;
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,null,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{
				for each (var idStr:Object in sqlRes.data) 
				{
					parentIDs.addItem(idStr.PARENTID);
					parentIDs.addAll(SQLiteServices.materialsDictionaryModule_getParrentIdsRecoursive(" and f.id="+idStr.PARENTID, connection, faultHandler));
				}
				
			}
			
			return parentIDs;
		}
		public static function materialsDictionaryModule_getMaterialsDictionaryModuleDataRecursive(connection:SQLConnection, parentID:String, whereStr:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select " +
				"f.id as ID, "+
				"f.parentid as PARENTID, "+
				"f.nodetype as NODETYPE, "+
				"f.name as NAME, "+
				"UNITOFMEASURE.id as BASEUNITOFMEASUREID, "+
				"UNITOFMEASURE.name as BASEUNITOFMEASURENAME, "+
				"f.nomenclaturearticlenumber as NOMENCLATUREARTICLENUMBER, "+
				"f.remark as REMARK, "+
				"f.farmgroupid as FARMGROUPID, "+
				"FARMGROUP.code as FARMGROUPCODE, "+
				"FARMGROUP.name as FARMGROUPNAME, "+
				"f.minimumquantity as MINIMUMQUANTITY, "+
				"f.optimalquantity as OPTIMALQUANTITY "+
				"from main.DICTIONARY f " +
				"left join UNITOFMEASURE.DICTIONARY UNITOFMEASURE on (f.ID = UNITOFMEASURE.FARMID  and UNITOFMEASURE.isbase = 1) " +
				"left join FARMGROUP.DICTIONARY FARMGROUP on f.FARMGROUPID = FARMGROUP.ID " +
				"where ";
			
			if(parentID==null)
			{ 
				sql+="(f.PARENTID is null)"; 
			}			
			else 
			{ 
				sql+="(f.PARENTID = "+parentID+")"; 
			}
			
			if(whereStr!=null&&whereStr.length>0)
			{
				sql+=" and "+whereStr;
			}
			sql+="  order by f.name";
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,MaterialsDictionaryModuleFarm,faultHandler);
			
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				var resultArray:ArrayCollection=new ArrayCollection();
				for each (var farm:MaterialsDictionaryModuleFarm in sqlRes.data) 
				{
					farm.label=farm.NAME;
					if(farm.NODETYPE=="0")
					{
						farm.children=materialsDictionaryModule_getMaterialsDictionaryModuleDataRecursive(connection,farm.ID,whereStr,faultHandler);
						
					}
					else
					{
						farm.unitsOfMeasure=materialsDictionaryModule_getUnitsOfMeasure(connection,farm.ID,faultHandler);
					}
					resultArray.addItem(farm);
				}
				return resultArray;
			}
			else
			{
				return null;
			}
		}
		public static function materialsDictionaryModule_getUnitsOfMeasure(connection:SQLConnection, farmId:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select " +
				"ID as ID, "+
				"NAME as NAME, "+
				"COEFFICIENT as COEFFICIENT, "+
				"ISBASE as ISBASE "+
				"from UNITOFMEASURE.DICTIONARY " +
				"where FARMID="+farmId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,MaterialsDictionaryModuleUnitOfMeasure,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		//-----------------------------------------------------		materialsDictionaryModule_getMaterialsDictionaryModuleData---------------------------------------------------///
		//-----------------------------------------------------		materialsDictionaryModule_getMaterialCardWindowData---------------------------------------------------///
		public static function materialsDictionaryModule_getMaterialCardWindowData(farmId:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("FARMGROUP",new ArrayCollection(["UNITOFMEASURE","FARMSUPPLIER","SUPPLIER"]),faultHandler);
			if(connection!=null)
			{				
				var resultObject:MaterialsDictionaryModuleMaterialCardWindowData=new MaterialsDictionaryModuleMaterialCardWindowData;
				
				resultObject.unitsOfMeasure=materialsDictionaryModule_getUnitsOfMeasure(connection,farmId,faultHandler);
				resultObject.farmSuppliers=materialsDictionaryModule_getFarmSuppliers(connection,farmId,faultHandler);
				resultObject.suppliers=materialsDictionaryModule_getSuppliers(connection,faultHandler);
				resultObject.farmGroups=materialsDictionaryModule_getFarmGroups(connection,faultHandler);
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultObject);
				resultHandler(resEvent);
			}
			
		}
		
		public static function materialsDictionaryModule_getFarmSuppliers(connection:SQLConnection, farmId:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select " +
				"farmsupplier.id as ID, "+
				"farmsupplier.farmid as FARMID, "+
				"supplierid as SUPPLIERID, "+
				"supplier.name as SUPPLIERNAME, "+
				"supplier.address as SUPPLIERADDRESS, "+
				"supplier.telephonenumber as SUPPLIERTELEPHONENUMBER, "+
				"farmsupplier.price as PRICE "+
				"from FARMSUPPLIER.DICTIONARY farmsupplier " +
				"left join SUPPLIER.DICTIONARY supplier on (supplier.id = farmsupplier.supplierid) "+
				"where farmsupplier.FARMID="+farmId;
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,MaterialsDictionaryModuleFarmSupplier,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		
		public static function materialsDictionaryModule_getSuppliers(connection:SQLConnection, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select " +
				"supplier.id as ID, "+
				"supplier.name as NAME "+
				"from SUPPLIER.DICTIONARY supplier ";
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,MaterialsDictionaryModuleSupplier,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		public static function materialsDictionaryModule_getFarmGroups(connection:SQLConnection, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select ID as ID, NAME as NAME, CODE as CODE from main.DICTIONARY order by CODE";
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,MaterialsDictionaryModuleFarmGroup,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		//-----------------------------------------------------		materialsDictionaryModule_getMaterialCardWindowData---------------------------------------------------///
		//____________________________________________________________________	MaterialsDictionaryModule ____________________________________________________________________///
		
		//____________________________________________________________________	OcclusionModule ____________________________________________________________________///
		//-----------------------------------------------------		occlusionModule_getOcclusion---------------------------------------------------///
		public static function occlusionModule_getOcclusion(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("OCCLUSIONS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as ID, NUMBER as NUMBER, DESCRIPTION as DESCRIPTION from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,OcclusionModuleDictionaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		occlusionModule_getOcclusion---------------------------------------------------///
		//____________________________________________________________________	OcclusionModule ____________________________________________________________________///
		
		//____________________________________________________________________	PatientCardDictionariesModule ____________________________________________________________________///
		//-----------------------------------------------------		patientCardDictionariesModule_getPatientCardDictionary---------------------------------------------------///
		public static function patientCardDictionariesModule_getPatientCardDictionary(type:int,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PATIENTCARDDICTIONARIES",null,faultHandler);
			if(connection!=null)
			{
				
				var sql:String = "select pcd.ID as ID, " +
					"pcd.NUMBER as NUMBER, " +
					"pcd.DESCRIPTION as DESCRIPTION, " +
					"pcd.TYPE as TYPE, " +
					"pcd.COLOR as COLOR " +
					"from main.DICTIONARY pcd where pcd.TYPE="+type.toString()+" order by pcd.NUMBER";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PatientCardDictionariesModuleDictionaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		patientCardDictionariesModule_getPatientCardDictionary---------------------------------------------------///
		//____________________________________________________________________	PatientCardDictionariesModule ____________________________________________________________________///
		
		//____________________________________________________________________	PatientCardModule ____________________________________________________________________///
		//-----------------------------------------------------		patientCardModule_getInsuranceCompanys---------------------------------------------------///
		public static function patientCardModule_getInsuranceCompanys(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("INSURANCECOMPANYS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ID as Id, " +
					"NAME as Name, " +
					"PHONE as Phone, " +
					"ADDRESS as Address " +
					"from main.DICTIONARY";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PatientCardModuleInsuranceCompanyObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		patientCardModule_getInsuranceCompanys---------------------------------------------------///
		//-----------------------------------------------------		patientCardModule_getDoctors---------------------------------------------------///
		public static function patientCardModule_getDoctors(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"doctors.id as docid, "+
					"doctors.firstname as fname, "+
					"doctors.lastname as lname, "+
					"doctors.middlename as pname, "+
					"doctors.shortname as shortname "+
					"from main.DICTIONARY doctors " +
					"where doctors.isfired is null or doctors.isfired != 1";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PatientCardModuleDoctorObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		public static function patientCardModule_getSubdivisons(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("SUBDIVISION",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"subdivison.id as id, "+
					"subdivison.name as name "+
					"from main.DICTIONARY subdivison ";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PatientCardModuleSubdivisonObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		patientCardModule_getDoctors---------------------------------------------------///
		//____________________________________________________________________	PatientCardModule ____________________________________________________________________///
		
		//____________________________________________________________________	PatientTestModule ____________________________________________________________________///
		//-----------------------------------------------------		patientTestModule_getTestQuestions---------------------------------------------------///
		public static function patientTestModule_getTestQuestions(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("TEMPLATETEST",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"ID as id, " +
					"ISENABLED as isenabled, " +
					"SEQNUMBER as number, " +
					"QUESTION as question, " +
					"CONFIGXML as answers, " +
					"'' as  changes, " +
					"'' as  result " +
					"from main.DICTIONARY "
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PatientTestModule_TestQuestion,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		patientTestModule_getTestQuestions---------------------------------------------------///
		//____________________________________________________________________	PatientTestModule ____________________________________________________________________///
		
		//____________________________________________________________________	PersonnelModule ____________________________________________________________________///
		//-----------------------------------------------------		personnelModule_getPersonnelModuleData---------------------------------------------------///
		public static function personnelModule_getPersonnelModuleData(doctorID:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("OPERATINGSCHEDULE",new ArrayCollection(["OPERATINGSCHEDULERULES","ROOMS"]),faultHandler);
			if(connection!=null)
			{
				var resultObject:PersonnelModuleData=new PersonnelModuleData;
				resultObject.schedules=personnelModule_getSchedules(doctorID, false, connection, faultHandler);
				resultObject.cabinets=personnelModule_getRooms(connection, faultHandler);
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultObject);
				resultHandler(resEvent);
				
			}
			
		}
		//-----------------------------------------------------		personnelModule_getPersonnelModuleData---------------------------------------------------///
		//-----------------------------------------------------		personnelModule_getCurrentPersonnelModuleData---------------------------------------------------///
		public static function personnelModule_getCurrentPersonnelModuleData(doctorID:String, onlyCurrent:Boolean, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("OPERATINGSCHEDULE",new ArrayCollection(["OPERATINGSCHEDULERULES","ROOMS"]),faultHandler);
			if(connection!=null)
			{
				var resultObject:PersonnelModuleData=new PersonnelModuleData;
				resultObject.schedules=personnelModule_getSchedules(doctorID, onlyCurrent, connection, faultHandler);
				resultObject.cabinets=personnelModule_getRooms(connection, faultHandler);
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultObject);
				resultHandler(resEvent);
				
			}
			
		}
		public static function personnelModule_getSchedules(doctorID:String, onlyCurrent:Boolean, connection:SQLConnection, faultHandler:Function):ArrayCollection
		{
			var sql:String = "select " +
				"ID as ID, "+
				"DOCTORID as DOCTORID, "+
				"NAME as NAME, "+
				"STARTDATE as STARTDATE, "+
				"ENDDATE as ENDDATE, "+
				"ISCURRENT as ISCURRENT "+
				"from main.DICTIONARY where DOCTORID="+doctorID;
			if(onlyCurrent)
			{
				sql+=" and ISCURRENT = 1";
			}
			else
			{
				sql+=" order by ID desc";
			}
			var resultArray:ArrayCollection=new ArrayCollection;
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PersonnelModuleSchedule,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{
				for each (var schedule:PersonnelModuleSchedule in sqlRes.data) 
				{
					schedule.rulesInForSaveFormat=personnelModule_getScheduleRules(schedule.ID,connection,faultHandler);
					resultArray.addItem(schedule);
				}
				
			}
			
			return resultArray;
		}	
		public static function personnelModule_getScheduleRules(scheduleId:String,connection:SQLConnection,faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select " +
				"ID as ID, "+
				"OPERATINGSCHEDULEID as OPERATINGSCHEDULEID, "+
				"NAME as NAME, "+
				"ISWORKING as ISWORKING, "+
				"DAYSRANGE as DAYSRANGE, "+
				"REPEATTYPE as REPEATTYPE, "+
				"SKIPDAYSCOUNT as SKIPDAYSCOUNT, "+
				"STARTTIME as STARTTIME, "+
				"ENDTIME as ENDTIME, "+
				"CABINETID as CABINETID "+
				"from OPERATINGSCHEDULERULES.DICTIONARY where OPERATINGSCHEDULEID="+scheduleId;
			
			var resultArray:ArrayCollection=new ArrayCollection;
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PersonnelModuleRule,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			
			
			return resultArray;
		}
		public static function personnelModule_getRooms(connection:SQLConnection,faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select " +
				"ROOMS.ID as ID, " +
				"ROOMS.NAME as NAME " +
				"from ROOMS.DICTIONARY ROOMS ";
			
			var resultArray:ArrayCollection=new ArrayCollection;
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PersonnelModuleCabinet,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			
			
			return resultArray;
		}
		//-----------------------------------------------------		personnelModule_getCurrentPersonnelModuleData---------------------------------------------------///
		//____________________________________________________________________	PersonnelModule ____________________________________________________________________///
		
		//____________________________________________________________________	PriceListModule ____________________________________________________________________///
		//-----------------------------------------------------		priceListModule_getPriceListRecords---------------------------------------------------///
		
		public static function priceListModule_getPriceListRecords(parentID:String, searchFields:String, searchText:String, searchType:int, searchSpecialities:ArrayCollection,resultHandler:Function,faultHandler:Function=null):void
		{
			
			var connection:SQLConnection=openLocal("HEALTHPROC",new ArrayCollection(["HEALTHPROCFARM","FARM","FARMGROUP","UNITOFMEASURE","F39HPROC"]),faultHandler);
			if(connection!=null)
			{
				var whereArray:Array=[];
				if(searchText!=null&&searchText.length>0)
				{
					whereArray.push(generateSearch(searchFields.replace("||' '||","||' '||p."),searchText));				
				}
				if (searchType>-1)
				{
					whereArray.push(" p.PROCTYPE="+searchType.toString());                  
				}
				if(searchSpecialities.length>0)
				{
					var specStr:String='';
					for each (var spec:Object in searchSpecialities) 
					{
						if(specStr!='')
						{
							specStr+=' or ';
						}
						specStr += "'|'||coalesce(p.HEALTHTYPES,'')||'|'||coalesce(p.HEALTHTYPE,'')||'|' like '%|"+spec.id+"|%'";   
					}
					
					whereArray.push(" ("+specStr+" )");                     
				}
				var whereStr:String="";
				if(whereArray.length>0)
				{
					whereStr=whereArray.join(' and ');
					
					var parentIDs:ArrayCollection=SQLiteServices.priceListModule_getParrentIdsRecoursive(" and "+whereStr,connection,faultHandler);
					
					if(parentIDs!=null&&parentIDs.length>0)
					{	
						whereStr =" and (p.id in ("+parentIDs.source.join(", ")+") or ("+whereStr+"))";
					}
					else
					{
						whereStr=" and "+whereStr;
					}
				}
				
				
				var priceList:ArrayCollection=priceListModule_getPriceListRecordsRecursive(connection,null,0,whereStr,faultHandler);
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,priceList);
				resultHandler(resEvent);
				
			}
			
		}
		
		public static function priceListModule_getParrentIdsRecoursive(whereStr:String, connection:SQLConnection, faultHandler:Function):ArrayCollection
		{
			var sql:String = "select  distinct p.PARENTID from main.DICTIONARY p where p.PARENTID is not null"+whereStr;
			
			var parentIDs:ArrayCollection=new ArrayCollection;
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,null,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{
				for each (var idStr:Object in sqlRes.data) 
				{
					parentIDs.addItem(idStr.PARENTID);
					parentIDs.addAll(SQLiteServices.priceListModule_getParrentIdsRecoursive(" and p.id="+idStr.PARENTID, connection, faultHandler));
				}
				
			}
			
			return parentIDs;
		}
		
		public static function priceListModule_getPriceListRecordsRecursive(connection:SQLConnection, parentID:String, captionLevel:int, whereStr:String,faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select p.ID as ID,"+
				"p.PARENTID as PARENTID,"+
				"p.NODETYPE as NODETYPE,    "+
				"p.SHIFR as SHIFR,       "+
				"p.NAME as NAME,        "+
				"p.PLANNAME as PLANNAME,    "+
				"p.UOP as UOP,         "+
				"p.PROCTYPE as PROCTYPE,    "+
				"p.TOOTHUSE as TOOTHUSE,    "+
				"p.PRICE as PRICE,       "+
				"p.PRICE2 as PRICE2,       "+
				"p.PRICE3 as PRICE3,       "+
				"p.HEALTHTYPE as HEALTHTYPE,  "+
				"p.PROCTIME as PROCTIME, "+
				"p.SEQUINCENUMBER as SEQUINCENUMBER, "+
				"p.EXPENSES as EXPENSES, "+
				"p.FARMSCOUNT as FARMSCOUNT, "+
				"p.DISCOUNT_FLAG as discount_flag, "+
				"p.SALARY_SETTINGS as salary_settings, "+
				"p.COMPLETEEXAMDIAG as complete_examdiag, "+
				"p.HEALTHTYPES as HEALTHTYPES, " +
				"p.NAME48 as NAME48, " +
				"p.REGCODE as regCode " +
				" from main.DICTIONARY p";
			
			if (parentID=="null"|| parentID == ""||parentID==null)
			{
				sql+= " where p.PARENTID is null";                  
			}			
			else 
			{
				
				sql+= " where p.PARENTID = "+parentID;
			}
			sql+=whereStr+" order by p.SEQUINCENUMBER";
			
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PriceListModuleRecord,faultHandler);
			if(sqlRes!=null)
			{	
				var priceList:ArrayCollection=new ArrayCollection();
				for each (var priceItem:PriceListModuleRecord in sqlRes.data) 
				{
					priceItem.label=priceItem.PLANNAME;
					priceItem.PARENTID=parentID;
					if(priceItem.NODETYPE=="0")
					{
						priceItem.CAPTIONLEVEL=captionLevel.toString();
						priceItem.children=priceListModule_getPriceListRecordsRecursive(connection,priceItem.ID,captionLevel + 1, whereStr,faultHandler);
					}
					else
					{
						priceItem.CAPTIONLEVEL="0";
						priceItem.F39=form039Module_get039ProcValues(connection,priceItem.ID,faultHandler);
						priceItem.farms=priceListModule_getFarms(connection,priceItem.ID,faultHandler);
					}
					priceList.addItem(priceItem);
				}
				return priceList;
			}
			else
			{
				return new ArrayCollection();
			}
		}
		
		public static function priceListModule_getFarms(connection:SQLConnection, procId:String, faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select " +
				"HEALTHPROCFARM.ID as ID, " +
				"FARM.ID as farmID, " +
				"FARM.NAME as farmName, " +
				"FARMGROUP.CODE as farmGroup, " +
				"HEALTHPROCFARM.QUANTITY as farmQuantity, " +
				"0 as farmPrice, " +
				"UNITOFMEASURE.NAME as farmUnitofmeasure "+	
				"from HEALTHPROCFARM.DICTIONARY HEALTHPROCFARM " +
				"left join FARM.DICTIONARY FARM on HEALTHPROCFARM.FARM = FARM.ID " +
				"left join UNITOFMEASURE.DICTIONARY UNITOFMEASURE on (FARM.ID = UNITOFMEASURE.FARMID  and UNITOFMEASURE.isbase = 1) " +
				"left join FARMGROUP.DICTIONARY FARMGROUP on FARM.FARMGROUPID = FARMGROUP.ID " +
				"where HEALTHPROCFARM.HEALTHPROC="+procId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,PriceListModuleRecordFarm,faultHandler);
			var farms:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				farms=new ArrayCollection(sqlRes.data);
				
			}
			return farms;
		}
		//-----------------------------------------------------		priceListModule_getPriceListRecords---------------------------------------------------///
		//-----------------------------------------------------		priceListModule_getFarmsForProcDictionary---------------------------------------------------///
		public static function priceListModule_getFarmsForProcDictionary(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection = openLocal("FARM", new ArrayCollection(["FARMGROUP", "UNITOFMEASURE"]), faultHandler);
			
			if (connection != null) {
				var sql:String = "select " +
										"FARM.ID as farmID, " +
										"FARM.NAME as farmName, " +
										"FARMGROUP.CODE as farmGroup, " +
										"UNITOFMEASURE.NAME as farmUnitofmeasure, "+	
										"0 as farmPrice, " +
										"1 as farmQuantity " +
								 "from main.DICTIONARY FARM " +
										"left join UNITOFMEASURE.DICTIONARY UNITOFMEASURE on FARM.ID = UNITOFMEASURE.FARMID " +
										"left join FARMGROUP.DICTIONARY FARMGROUP on FARM.FARMGROUPID = FARMGROUP.ID " +
								 "where FARM.nodetype = 1 and UNITOFMEASURE.isbase = 1 ";
				
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, PriceListModuleRecordFarm, faultHandler);
				var resultArray:ArrayCollection = new ArrayCollection();
				
				if (sqlRes != null && sqlRes.data != null) {	
					resultArray = new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		priceListModule_getFarmsForProcDictionary---------------------------------------------------///
		//____________________________________________________________________	PriceListModule ____________________________________________________________________///
		
		//____________________________________________________________________	ReportsModule ____________________________________________________________________///
		//-----------------------------------------------------		reportsModule_getDoctors---------------------------------------------------///
		public static function reportsModule_getDoctors(doctorId:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"doctors.id as id, "+
					"doctors.firstname as fname, "+
					"doctors.lastname as lname, "+
					"doctors.middlename as sname, "+
					"doctors.healthtype as healthtype "+
					"from main.DICTIONARY doctors ";
				if(doctorId!=null)
				{
					sql+="where doctors.id="+doctorId+" and (doctors.isfired is null or doctors.isfired != 1)";
				}
				else
				{
					sql+="where doctors.isfired is null or doctors.isfired != 1";
				}
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ReportsModule_Doctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		reportsModule_getDoctors---------------------------------------------------///
		//____________________________________________________________________	ReportsModule ____________________________________________________________________///
		
		//____________________________________________________________________	RoomsDictionaryModule ____________________________________________________________________///
		//-----------------------------------------------------		roomsDictionaryModule_getRooms---------------------------------------------------///
		public static function roomsDictionaryModule_getRooms(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ROOMS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select ROOMS.ID as ID, " +
					"ROOMS.NUMBER as NUMBER, " +
					"ROOMS.NAME as NAME, " +
					"ROOMS.DESCRIPTION as DESCRIPTION, " +
					"(case when (ROOMS.ROOMCOLOR is null or ROOMS.ROOMCOLOR=0) then 16777215 else ROOMS.ROOMCOLOR end) as ROOMCOLOR, " +
					"ROOMS.sequincenumber as sequincenumber, " +
					"(case when (ROOMS.GOOGLEROOMCOLORID is null or ROOMS.GOOGLEROOMCOLORID = 0) then 8 else ROOMS.GOOGLEROOMCOLORID end) as GOOGLEROOMCOLORID " +
					"from main.DICTIONARY ROOMS ";
				
				sql += "order by ROOMS.sequincenumber, ROOMS.id";
				
				var resultArray:ArrayCollection = new ArrayCollection();
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, RoomsDictionaryRoom, faultHandler);
				
				if (sqlRes != null && sqlRes.data != null) {	
					resultArray = new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				
				var resEvent:ResultEvent = new ResultEvent(ResultEvent.RESULT, false, true, resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		roomsDictionaryModule_getRooms---------------------------------------------------///
		//-----------------------------------------------------roomsDictionaryModule_getWorkPlaces---------------------------------------------------///
		
		public static function roomsDictionaryModule_getWorkPlaces(roomId:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("WORKPLACES",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select WORKPLACES.ID as ID, " +
					"WORKPLACES.ROOMID as ROOMID, " +
					"WORKPLACES.DESCRIPTION as DESCRIPTION, " +
					"WORKPLACES.NUMBER as NUMBER " +
					"from main.DICTIONARY WORKPLACES " +
					"where WORKPLACES.roomid ="+roomId;
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,RoomsDictionaryWorkPlace,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------roomsDictionaryModule_getWorkPlaces---------------------------------------------------///
		//____________________________________________________________________	RoomsDictionaryModule ____________________________________________________________________///
		
		//____________________________________________________________________	ServerFileModule ____________________________________________________________________///
		//-----------------------------------------------------		serverFileModule_getProtocols---------------------------------------------------///
		public static function serverFileModule_getProtocols(search:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"p.ID as id, " +
					"p.NAME as name " +
					"from main.DICTIONARY p ";
				if(search!=null&&search.length>0)
				{
					sql+=" where "+generateSearch("p.name",search); 
				}
				sql+=" limit 5";
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ServerFileModule_Protocol,faultHandler);
				
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		public static function serverFileModule_getProtocolById(prId:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"p.ID as id, " +
					"p.NAME as name " +
					"from main.DICTIONARY p " +
					"where p.ID="+prId;
				var resultObj:ServerFileModule_Protocol;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,ServerFileModule_Protocol,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
				{	
					resultObj=sqlRes.data[0] as ServerFileModule_Protocol;
				}
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultObj);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		serverFileModule_getProtocols---------------------------------------------------///		
		//____________________________________________________________________	ServerFileModule ____________________________________________________________________///
		
		//____________________________________________________________________	SubscriptionModule ____________________________________________________________________///
		//-----------------------------------------------------		subscriptionModule_getSubscriptionDictionary ---------------------------------------------------///
		public static function subscriptionModule_getSubscriptionDictionary(resultHandler:Function, faultHandler:Function=null):void
		{
			var connection:SQLConnection = openLocal("SUBSCRIPTION", null, faultHandler);
			
			if (connection != null) {
				var sql:String = "select ss.ID as ID, " +
					"ss.NAME as NAME, " +
					"ss.PRICE as PRICE " +
					"ss.MAX_VISITS_PER_MONTH as MAX_VISITS_PER_MONTH, " +
					"ss.TYPE_ID, " +
					"st.NAME as typeName, " +
					"st.N_MONTHS as nMonths, " +
					"sp.PROCEDURE_ID as procId " +
					"from main.DICTIONARY ss " +
					"inner join SUBSCRIPTION_TYPE_DICT.DICTIONARY st on st.id = ss.type_id " +
					"inner join SUBSCRIPTION_PROCEDURES.DICTIONARY sp on sp.subscription_id = ss.id " +
					"order by ss.ID";
				
				
				
				var resultArray:ArrayCollection = new ArrayCollection();
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, SubscriptionDictionaryObject, faultHandler);
				
				if (sqlRes != null && sqlRes.data != null) {	
					resultArray = new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				
				var resEvent:ResultEvent = new ResultEvent(ResultEvent.RESULT, false, true, resultArray);
				resultHandler(resEvent);
			}
		}
		
		public static function subscriptionModule_getSubscriptionTypesDictionary(resultHandler:Function, faultHandler:Function=null):void
		{
			var connection:SQLConnection = openLocal("SUBSCRIPTION_TYPE_DICT", null, faultHandler);
			
			if (connection != null) {
				var sql:String = "select st.ID as ID, " +
					"st.NAME as NAME, " +
					"st.N_MONTH as N_MONTH " +
					"from main.DICTIONARY st order by st.ID";
				
				var resultArray:ArrayCollection = new ArrayCollection();
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, SubscriptionTypeObject, faultHandler);
				
				if (sqlRes != null && sqlRes.data != null) {	
					resultArray = new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				
				var resEvent:ResultEvent = new ResultEvent(ResultEvent.RESULT, false, true, resultArray);
				resultHandler(resEvent);
			}
		}
		
		public static function subscriptionModule_getSubscriptionCards(patientID:String, resultHandler:Function, faultHandler:Function = null):void
		{
			var connection:SQLConnection = openLocal("SUBSCRIPTION_CARDS", null, faultHandler);
			
			//---------------------------------
			// TODO: опрацювати підтягування карток
			//---------------------------------
//			if (connection != null) {
//				var sql:String = "select ss.ID as ID, " +
//					"ss.NAME as NAME, " +
//					"ss.PRICE as PRICE " +
//					"ss.MAX_VISITS_PER_MONTH as MAX_VISITS_PER_MONTH, " +
//					"ss.TYPE_ID, " +
//					"st.NAME as typeName, " +
//					"st.N_MONTHS as nMonths, " +
//					"sp.PROCEDURE_ID as procId " +
//					"from main.DICTIONARY ss " +
//					"inner join SUBSCRIPTION_TYPE_DICT.DICTIONARY st on st.id = ss.type_id " +
//					"inner join SUBSCRIPTION_PROCEDURES.DICTIONARY sp on sp.subscription_id = ss.id " +
//					"order by ss.ID";
//				
//				
//				
//				var resultArray:ArrayCollection = new ArrayCollection();
//				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, SubscriptionDictionaryObject, faultHandler);
//				
//				if (sqlRes != null && sqlRes.data != null) {	
//					resultArray = new ArrayCollection(sqlRes.data);
//				}				
//				
//				connection.close();
//				
//				var resEvent:ResultEvent = new ResultEvent(ResultEvent.RESULT, false, true, resultArray);
//				resultHandler(resEvent);
//			}
		}
		
		//-----------------------------------------------------		subscriptionModule_getSubscriptionDictionary ---------------------------------------------------///
		//____________________________________________________________________	SubscriptionModule ____________________________________________________________________///
		
		//____________________________________________________________________	SuppliersDictionaryModule ____________________________________________________________________///
		//-----------------------------------------------------		suppliersDictionaryModule_getSuppliersDictionaryModuleData---------------------------------------------------///
		public static function suppliersDictionaryModule_getSuppliersDictionaryModuleData(searchText:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("SUPPLIER",null,faultHandler);
			if(connection!=null)
			{				
				var resultObject:ArrayCollection = suppliersDictionaryModule_getSuppliers(searchText, connection,faultHandler);
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultObject);
				resultHandler(resEvent);
			}
		}
		
		public static function suppliersDictionaryModule_getSuppliers(searchText:String, connection:SQLConnection, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select " +
				"ID as ID, "+
				"NAME as NAME, "+
				"LEGALNAME as LEGALNAME, "+
				"ADDRESS as ADDRESS, "+
				"ORGANIZATIONTYPE as ORGANIZATIONTYPE, "+
				"CODE as CODE, "+
				"DIRECTOR as DIRECTOR, "+
				"TELEPHONENUMBER as TELEPHONENUMBER, "+
				"FAX as FAX, "+
				"SITENAME as SITENAME, "+
				"EMAIL as EMAIL, "+
				"ICQ as ICQ, "+
				"SKYPE as SKYPE "+
				"from main.DICTIONARY ";
			if(searchText!=null && searchText!='')
			{
				sql+=" where " + generateSearch("NAME", searchText);//lower(NAME) like lower('%"+searchText+"%') ";
			}
			sql+=" order by NAME ";
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,SuppliersDictionaryModuleSupplier,faultHandler);
			var resultArray:ArrayCollection = new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray = new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		//-----------------------------------------------------		suppliersDictionaryModule_getSuppliersDictionaryModuleData---------------------------------------------------///
		//____________________________________________________________________	SuppliersDictionaryModule ____________________________________________________________________///
		
		//____________________________________________________________________	TeethExamDictionariesModule ____________________________________________________________________///
		//-----------------------------------------------------		teethExamDictionariesModule_getTeethExamDictionary---------------------------------------------------///
		public static function teethExamDictionariesModule_getTeethExamDictionary(type:int,specialities:ArrayCollection,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("TEETHEXAMDICTIONARIES",null,faultHandler);
			if(connection!=null)
			{
				
				var sql:String = "select ted.ID as ID, " +
					"ted.NUMBER as NUMBER, " +
					"ted.DESCRIPTION as DESCRIPTION, " +
					"ted.TYPE as TYPE, " +
					"ted.SPECIALITY as SPECIALITY " +
					"from main.DICTIONARY ted where ted.TYPE="+type.toString();
				
				if(specialities!=null&&specialities.length>0)
				{
					
					var whereArray:Array=[];
					for each (var spec:Object in specialities) 
					{
						whereArray.push("('|'||ted.SPECIALITY||'|' like '%|"+spec.id.toString()+"|%' or ted.SPECIALITY is null or ted.SPECIALITY='')");
					}
					
					if(whereArray.length>0)
					{
						sql+=" and ("+whereArray.join(' or ')+")";
					}
				}
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TeethExamDictionariesModuleDictionaryObject,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		teethExamDictionariesModule_getTeethExamDictionary---------------------------------------------------///
		//____________________________________________________________________	TeethExamDictionariesModule ____________________________________________________________________///
		
		//____________________________________________________________________	TeethExamModule ____________________________________________________________________///
		//-----------------------------------------------------		teethExamModule_getF43XML---------------------------------------------------///
		public static function teethExamModule_getF43XML(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("F43",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select f43.ID as ID, " +
					"f43.F43 as F43, " +
					"f43.NAME as NAME, " +
					"coalesce(f43.COMPLAINTS,'') as COMPLAINTS, " +
					"f43.SIGN as SIGN, " +
					"cast(f43.FLAGTOOTH as INTEGER) as FLAGTOOTH, " +
					"cast(f43.FLAGSURFACE as INTEGER) as FLAGSURFACE, " +
					"cast(f43.FLAGROOT as INTEGER) as FLAGROOT, " +
					"f43.SEQNUMBER as SEQNUMBER " +
					"from main.DICTIONARY f43 " +
					"order by SEQNUMBER";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TeethExamModuleF43,faultHandler);
				var xmlStr:String='<diagdb>';
				var buf:String;
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					for each (var f43:TeethExamModuleF43 in sqlRes.data) 
					{
						buf='' +
							'<label>'+(f43.SIGN!=null?(f43.SIGN+' - '):'')+f43.NAME+'</label>' +
							'<data>'+f43.NAME+'</data>' +
							'<num>'+f43.F43.toString()+'</num>' +
							'<complaints>'+f43.COMPLAINTS+'</complaints>' +
							'';
						if(f43.FLAGSURFACE)
						{
							xmlStr+=''+
								'<Obj id="'+f43.F43.toString()+'">'+buf+'</Obj>';
						}
						if(f43.FLAGTOOTH)
						{
							xmlStr+=''+
								'<ObjBack id="'+f43.F43.toString()+'">'+buf+'</ObjBack>';
						}
						if(f43.FLAGROOT)
						{
							xmlStr+=''+
								'<ObjR id="'+f43.F43.toString()+'">'+buf+'</ObjR>';
						}
					}
					
				}				
				xmlStr+='' +
					'</diagdb>';
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,xmlStr);
				resultHandler(resEvent);
			}
			
		}
		
		public static function teethExamModule_getF43(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("F43",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select f43.ID as ID, " +
					"f43.F43 as F43, " +
					"f43.NAME as NAME, " +
					"f43.COMPLAINTS as COMPLAINTS, " +
					"f43.SIGN as SIGN, " +
					"cast(f43.FLAGTOOTH as INTEGER) as FLAGTOOTH, " +
					"cast(f43.FLAGSURFACE as INTEGER) as FLAGSURFACE, " +
					"cast(f43.FLAGROOT as INTEGER) as FLAGROOT, " +
					"f43.SEQNUMBER as SEQNUMBER, " +
					"f43.ISQUICK as ISQUICK " +
					"from main.DICTIONARY f43 " +
					"order by SEQNUMBER";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TeethExamModuleF43,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		teethExamModule_getF43XML---------------------------------------------------///
		//-----------------------------------------------------		teethExamModule_getExam---------------------------------------------------///
		public static function teethExamModule_getExam(examId:int, nowDate:String, toothexamService:TeethExamModule, resultHandler:Function,faultHandler:Function=null):void
		{
			var cr:CallResponder=new CallResponder;
			if(faultHandler!=null)
			{
				cr.addEventListener(FaultEvent.FAULT, faultHandler);
			}
			cr.addEventListener(ResultEvent.RESULT, teethExamModule_getExamHandlerFunction(nowDate,resultHandler,faultHandler));
			cr.token=toothexamService.getExam(examId,nowDate,true);
		}
		public static function teethExamModule_getExamHandlerFunction(nowDate:String, resultHandler:Function,faultHandler:Function=null):Function
		{
			return function(event:ResultEvent):void
			{
				var resultObject:TeethExamModuleToothSOut=event.result as TeethExamModuleToothSOut;
				var connection:SQLConnection=openLocal("DOCTORS",new ArrayCollection(["ASSISTANTS"]),faultHandler);
				if(connection!=null)
				{				
					resultObject.docList=teethExamModule_getDoctors(connection, nowDate, faultHandler);
					resultObject.assistantsList=teethExamModule_getAssistants(connection, nowDate, faultHandler);
					connection.close();
					var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultObject);
					resultHandler(resEvent);
				}
			}
			
		}
		public static function teethExamModule_getDoctors(connection:SQLConnection, nowdate:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select "+
				"doctors.ID as docid, "+
				"doctors.LASTNAME || ' ' || doctors.FIRSTNAME as name, "+
				"doctors.SPECIALITY as spec "+
				"from main.DICTIONARY doctors " +
				"where doctors.isfired is null or doctors.isfired != 1";
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TeethExamModuleDocForTooth,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		public static function teethExamModule_getAssistants(connection:SQLConnection, nowdate:String, faultHandler:Function=null):ArrayCollection
		{
			
			var sql:String = "select "+
				"ass.ID as assid, "+
				"ass.LASTNAME || ' ' || ass.FIRSTNAME as name "+
				"from ASSISTANTS.DICTIONARY ass " +
				"where ass.isfired is null or ass.isfired != 1";
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TeethExamModuleAssistantForTooth,faultHandler);
			var resultArray:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				resultArray=new ArrayCollection(sqlRes.data);
			}
			return resultArray;
		}
		//-----------------------------------------------------		teethExamModule_getExam---------------------------------------------------///
		//____________________________________________________________________	TeethExamModule ____________________________________________________________________///
		
		//____________________________________________________________________	TreatmentManagerModule ____________________________________________________________________///
		//-----------------------------------------------------treatmentManagerModule_getProtocols---------------------------------------------------///
		public static function treatmentManagerModule_getProtocols(mode:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("PROTOCOLS",new ArrayCollection(["DOCTORS","PROTOCOLS_F43","TEMPLATE","F43"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"protocols.id as protocolID, " +
					"protocols.name as protocolName, " +
					"protocols.f43 as protocolF43, " +
					"protocols.healthtype as protocolType, " +
					"protocols.protocoltxt as protocolTXT, " +
					"protocols.anamnestxt as protocolAnamnesTXT, " +
					"protocols.statustxt as protocolStatusTXT, " +
					"protocols.recomendtxt as protocolRecomendTXT, " +
					"protocols.createdate as protocolCreateDate, " +
					"(select count(*) from TEMPLATE.DICTIONARY template where template.protocol = protocols.id) as templates_count, " +
					"protocols.epicrisistxt as protocolEpicrisisTXT, " +
					"protocols.rentgentxt as protocolRentgenTXT, " +
					"protocols.isquick as isQuick " +
					"from main.DICTIONARY protocols";
				
				if(mode == TreatmentManagerModule.PROTOCOLS_QUERY_ACTIVE)
				{
					sql += " where (select count(*) from PROTOCOLS_F43.DICTIONARY protocols_f43 where protocols.id = protocols_f43.protocol_fk) > 0";
				}
				else if(mode == TreatmentManagerModule.PROTOCOLS_QUERY_NONACTIVE)
				{
					sql += " where (select count(*) from PROTOCOLS_F43.DICTIONARY protocols_f43 where protocols.id = protocols_f43.protocol_fk) <= 0";
				}
				sql += " order by protocols.f43 desc, protocols.id";
				
				
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TreatmentManagerModuleProtocol);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					var protocols:ArrayCollection=new ArrayCollection();
					for each (var protocol:TreatmentManagerModuleProtocol in sqlRes.data) 
					{
						protocol.doctor=treatmentManagerModule_getProtocolsDoctor(protocol.protocolID,connection);
						if(mode != TreatmentManagerModule.PROTOCOLS_QUERY_NONACTIVE)
						{
							protocol.f43=treatmentManagerModule_getProtocolsF43(protocol.protocolID,connection);
						}
						protocols.addItem(protocol);
					}
					connection.close();
					var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,protocols);
					resultHandler(resEvent);
				}
			}
			
		}
		
		public static function treatmentManagerModule_getProtocolsDoctor(protocolId:String,connection:SQLConnection,faultHandler:Function=null):TreatmentManagerModuleDoctor
		{
			var sql:String = "select " +
				"doctors.id as doctorID, " +
				"doctors.firstname as doctorFName, " +
				"doctors.lastname as doctorLName, " +
				"doctors.middlename as doctorMName, " +
				"doctors.lastname||' '||doctors.firstname as doctorLabelField " +
				"from main.DICTIONARY protocols " +
				"left join DOCTORS.DICTIONARY doctors on doctors.id = protocols.doctor where protocols.id="+protocolId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TreatmentManagerModuleDoctor,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				var protocolDoctor:TreatmentManagerModuleDoctor=sqlRes.data[0] as TreatmentManagerModuleDoctor
				return protocolDoctor;
			}
			else
			{
				return null;
			}
		}
		
		public static function treatmentManagerModule_getProtocolsF43(protocolId:String,connection:SQLConnection,faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select f43.f43 as f43, " +
				"f43.id as f43_id, " +
				"protocols_f43.id as id, " +
				""+protocolId+" as protocol_id " +
				"from F43.DICTIONARY f43 " +
				"inner join PROTOCOLS_F43.DICTIONARY protocols_f43 on f43.id = protocols_f43.f43_fk " +
				"where protocols_f43.protocol_fk ="+protocolId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TreatmentManagerModuleF43,faultHandler);
			if(sqlRes!=null&&sqlRes.data!=null)
			{	
				var f43s:ArrayCollection=new ArrayCollection(sqlRes.data);
				return f43s;
			}
			else
			{
				return null;
			}
			
			
		}
		//-----------------------------------------------------treatmentManagerModule_getProtocols---------------------------------------------------///
		//-----------------------------------------------------treatmentManagerModule_getDoctors---------------------------------------------------///
		public static function treatmentManagerModule_getDoctors(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"doctors.id as doctorID, " +
					"doctors.firstname as doctorFName, " +
					"doctors.lastname as doctorLName, " +
					"doctors.middlename as doctorMName, " +
					"doctors.lastname||' '||doctors.firstname as doctorLabelField, " +
					"(doctors.dateworkend is not null and doctors.dateworkend<'"+ToolsForFirebird.dateToFirebirdDateString(new Date)+"') as doctorDismissedFlag " +
					"from main.DICTIONARY doctors " +
					" order by doctors.dateworkend asc";
				
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TreatmentManagerModuleDoctor,faultHandler);
				var resultArray:ArrayCollection=new ArrayCollection();
				if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
		}
		//-----------------------------------------------------treatmentManagerModule_getDoctors---------------------------------------------------///
		//-----------------------------------------------------		treatmentManagerModule_getDictionaryProcedures---------------------------------------------------///
		public static function treatmentManagerModule_getDictionaryProcedures(healthType:Number, priceIndex:int, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("HEALTHPROC",new ArrayCollection(["HEALTHPROCFARM","FARM","FARMGROUP","UNITOFMEASURE","F39HPROC"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"null as procedurID, "+
					"1 as procedurVisit, "+
					"0 as procedurStacknumber, "+
					"healthproc.id as healthprocID, "+
					"healthproc.name as procedurName, "+
					"healthproc.shifr as procedurShifr, "+
					"healthproc.price" + sql_checkPriceIndex(priceIndex) + " as procedurPrice, "+
					"healthproc.healthtype as healthType, "+
					"healthproc.healthtypes as healthTypes "+
					"from main.DICTIONARY healthproc ";
				
				var whereArray:Array=[];
				
				if(isNaN(healthType)==false&&healthType!=-1)//procedures by healthtype
				{
					whereArray.push("'|'||coalesce(healthproc.HEALTHTYPES,'')||'|'||coalesce(healthproc.HEALTHTYPE,'')||'|' like '%|"+healthType.toString()+"|%'");  
				}
				whereArray.push('healthproc.nodetype = 1');
				sql+=' where '+whereArray.join(' and ');
				
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TreatmentManagerModuleTemplateProcedure,faultHandler);
				var resultArray:ArrayCollection=new ArrayCollection();
				if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
				{	
					for each (var proc:TreatmentManagerModuleTemplateProcedure in sqlRes.data) 
					{
						proc.F39=form039Module_get039ProcValues(connection,proc.healthprocID,faultHandler);
						proc.farms=treatmentManagerModule_getFarms(connection,proc.healthprocID,faultHandler);
						resultArray.addItem(proc);
					}
				}
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
				
			}
			
		}
		
		public static function treatmentManagerModule_getFarms(connection:SQLConnection, procId:String, faultHandler:Function=null):ArrayCollection
		{
			var sql:String = "select " +
				"HEALTHPROCFARM.ID as ID, " +
				"FARM.ID as farmID, " +
				"FARM.NAME as farmName, " +
				"FARMGROUP.CODE as farmGroup, " +
				"HEALTHPROCFARM.QUANTITY as farmQuantity, " +
				"0 as farmPrice, " +
				"UNITOFMEASURE.NAME as farmUnitofmeasure, "+	
				"HEALTHPROCFARM.HEALTHPROC as farmHealthproc "+	
				"from HEALTHPROCFARM.DICTIONARY HEALTHPROCFARM " +
				"left join FARM.DICTIONARY FARM on HEALTHPROCFARM.FARM = FARM.ID " +
				"left join UNITOFMEASURE.DICTIONARY UNITOFMEASURE on (FARM.ID = UNITOFMEASURE.FARMID  and UNITOFMEASURE.isbase = 1) " +
				"left join FARMGROUP.DICTIONARY FARMGROUP on FARM.FARMGROUPID = FARMGROUP.ID " +
				"where HEALTHPROCFARM.HEALTHPROC="+procId;
			
			var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,TreatmentManagerModuleTemplateProcedurFarm,faultHandler);
			var farms:ArrayCollection=new ArrayCollection();
			if(sqlRes!=null&&sqlRes.data!=null&&sqlRes.data.length>0)
			{	
				farms=new ArrayCollection(sqlRes.data);
			}
			return farms;
		}
		//-----------------------------------------------------		treatmentManagerModule_getDictionaryProcedures---------------------------------------------------///
		//____________________________________________________________________	TreatmentManagerModule ____________________________________________________________________///
		
		//____________________________________________________________________	UltrasonicExamModule  ____________________________________________________________________///
		//-----------------------------------------------------		ultrasonicExamModule_getExamTemplateQuestions---------------------------------------------------///
		public static function ultrasonicExamModule_getExamTemplateQuestions(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection = openLocal("NH_ULTRASONOGRAPHY_DICT",null,faultHandler);
			if(connection != null)
			{
				var sql:String = "select " +
					"ID as id, " +
					"ID_TEMPLATE as id_template, " +
					"ISENABLED as isenabled, " +
					"SEQNUMBER as number, " +
					"QUESTION as question, " +
					"CONFIGXML as answers, " +
					"'' as  changes, " +
					"'' as  result " +
					"from main.DICTIONARY ";
				
				var resultArray:ArrayCollection = new ArrayCollection();
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql, NHUltraTemplateModule_ExamQuestion, faultHandler);
				
				if(sqlRes != null && sqlRes.data != null) {	
					resultArray = new ArrayCollection(sqlRes.data);
				}
				
				connection.close();
				
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT, false, true, resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		patientTestModule_getTestQuestions---------------------------------------------------///
		//____________________________________________________________________	UltrasonicExamModule  ____________________________________________________________________///
		
		
		//____________________________________________________________________	WarehouseModule ____________________________________________________________________///
		//-----------------------------------------------------		warehouseModule_getFarmUnits---------------------------------------------------///
		public static function warehouseModule_getFarmUnits(farmId:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("UNITOFMEASURE",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select" +
					"u.id as ID, " +
					"u.name as NAME, "+
					"u.coefficient as COEFFICIENT, "+
					"u.isbase as ISBASIC "+
					"from main.DICTIONARY u " +
					"where u.farmid="+farmId+
					" order by u.isbase desc";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,WarehouseModuleFarmUnit,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		warehouseModule_getFarmUnits---------------------------------------------------///
		//-----------------------------------------------------		warehouseModule_getRooms---------------------------------------------------///
		public static function warehouseModule_getRooms(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("ROOMS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"r.id as ID, " +
					"r.name as NAME "+
					"from main.DICTIONARY r order by LOWER(r.name)";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,WarehouseModuleRoom,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		warehouseModule_getRooms---------------------------------------------------///
		
		
		//-----------------------------------------------------		storage_getSuppliers---------------------------------------------------///
		public static function storage_getSuppliers(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("SUPPLIER",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"s.id as ID, " +
					"s.name as NAME "+
					"from main.DICTIONARY s order by LOWER(s.name)";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,Storage_Supplier,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		storage_getSuppliers---------------------------------------------------///
		//-----------------------------------------------------		storage_getFarms---------------------------------------------------///
		public static function storage_getFarms(search:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("FARM", null ,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select " +
					"FARM.ID as id, " +
					"FARM.NAME as name "+
					
					"from main.DICTIONARY FARM " +
					"where FARM.nodetype = 1 " +
					"and " + generateSearch("name", search) +
					"order by lower(name)";
				
				
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,Storage_Farm,faultHandler);
				var resultArray:ArrayCollection=new ArrayCollection;
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		storage_getFarms---------------------------------------------------///
		//-----------------------------------------------------		storage_getFarmsUnits---------------------------------------------------///
		public static function storage_getFarmsUnits(farmId:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("UNITOFMEASURE", null ,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select  GROUP_CONCAT('<unit><name>'||UNITOFMEASURE.name||'</name><coefficient>'||UNITOFMEASURE.coefficient||'</coefficient><isbase>'||UNITOFMEASURE.isbase||'</isbase></unit>', '') as unitXml " +
					"from main.DICTIONARY UNITOFMEASURE " +
					"where UNITOFMEASURE.farmid = " + farmId;
				
				
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql, Storage_Farm, faultHandler);
				var resultArray:ArrayCollection=new ArrayCollection;
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true, resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		storage_getFarmsUnits---------------------------------------------------///
		//-----------------------------------------------------		storage_getFarmsStorages---------------------------------------------------///
		public static function storage_getFarmsStorages(subdivId:String, resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("FARMSTORAGE",new ArrayCollection(["DOCTORS","SUBDIVISION"]),faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"s.ID as id, " +
					"s.STORAGELABEL as label, "+
					"s.STORAGECOLOR as color, "+
					"s.DOCTOR_FK as doctor_fk, "+
					"s.SUBDIVISION_FK as subdivision_fk, " +
					"d.shortname as doctor_shortname, " +
					"sb.name as subdivision_name " +
					
					"from main.DICTIONARY s " +
						"left join DOCTORS.DICTIONARY d on d.ID = s.DOCTOR_FK " +
						"left join SUBDIVISION.DICTIONARY sb on sb.ID = s.SUBDIVISION_FK ";
				if(subdivId != '0' && subdivId != null && subdivId != '-')
				{
					sql += "where s.SUBDIVISION_FK = " + subdivId;
				}
				sql += " order by LOWER(s.STORAGELABEL)";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection, sql,Storage_FarmStorage, faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		storage_getFarmsStorages---------------------------------------------------///
		
		
		//-----------------------------------------------------		warehouseModule_getSuppliers---------------------------------------------------///
		public static function warehouseModule_getSuppliers(resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("SUPPLIER",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"s.id as ID, " +
					"s.name as NAME "+
					"from main.DICTIONARY s order by LOWER(s.name)";
				
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,WarehouseModuleSupplier,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		warehouseModule_getSuppliers---------------------------------------------------///
		//-----------------------------------------------------		warehouseModule_getDoctors---------------------------------------------------///
		public static function warehouseModule_getDoctors(nowdate:String,resultHandler:Function,faultHandler:Function=null):void
		{
			var connection:SQLConnection=openLocal("DOCTORS",null,faultHandler);
			if(connection!=null)
			{
				var sql:String = "select "+
					"doctors.ID as ID, "+
					"doctors.SHORTNAME as SHORTNAME "+
					"from main.DICTIONARY doctors ";
				if(nowdate!=null)
				{
					sql+="where doctors.dateworkend is null or doctors.dateworkend>='"+nowdate+"'";
				}
				sql+=" order by doctors.shortname";
				var resultArray:ArrayCollection=new ArrayCollection;
				var sqlRes:SQLResult = SqliteFunctions.executeStatement(connection,sql,WarehouseModuleDoctor,faultHandler);
				if(sqlRes!=null&&sqlRes.data!=null)
				{	
					resultArray=new ArrayCollection(sqlRes.data);
				}
				
				
				connection.close();
				var resEvent:ResultEvent=new ResultEvent(ResultEvent.RESULT,false,true,resultArray);
				resultHandler(resEvent);
			}
			
		}
		//-----------------------------------------------------		warehouseModule_getDoctors---------------------------------------------------///
		//____________________________________________________________________	WarehouseModule ____________________________________________________________________///
		
		public static function openLocal(dbFileName:String,attachDBNames:ArrayCollection, faultHandler:Function=null):SQLConnection
		{
			var dbFile:File=File.applicationStorageDirectory.resolvePath(ipClass.serverKey+File.separator+dbFileName+".sqlite");
			var attachDBs:ArrayCollection=null;
			if(attachDBNames!=null)
			{
				attachDBs=new ArrayCollection;
				for each (var attachDBName:String in attachDBNames) 
				{
					attachDBs.addItem(File.applicationStorageDirectory.resolvePath(ipClass.serverKey+File.separator+attachDBName+".sqlite"));
				}
			}
			
			return SqliteFunctions.openDB(dbFile,attachDBs,faultHandler);
		}
		
		public static function shortName(last:String,first:String,middle:String):String
		{
			var short:String='';
			if(last!=null&&last.length>0)
			{ 
				short = last; 
				if(first!=null&&first.length>0)
				{
					short+=' '+first.charAt(0)+".";
					if(middle!=null&&middle.length>0)
					{
						short+=' '+middle.charAt(0)+".";
					}
				}
			}
			return short;
		}
		
		public static function generateSearch(param:String, search:String):String
		{
			return " (lower("+param+") like lower('%"+search+"%') or lower("+param+") like lower('%"+search.toUpperCase()+"%') or lower("+param+") like lower('%"+search.charAt(0).toLocaleUpperCase()+search.substr(1)+"%')) ";
		}
		
		private static function sql_checkPriceIndex(priceIndex:int):String
		{
			if(priceIndex == 0 || priceIndex == 1)
			{
				return "";
			}
			else
			{
				return priceIndex.toString();
			}
		}
	}
}