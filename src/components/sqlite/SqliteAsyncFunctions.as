package components.sqlite
{
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	
	public class SqliteAsyncFunctions
	{
		
		public static function openDB(dbFile:File,resultHandler:Function=null,faultHandler:Function=null,attachDBs:ArrayCollection=null):void
		{
			var connection:SQLConnection=new SQLConnection;
			if(resultHandler!=null||attachDBs!=null)
			{
				connection.addEventListener(SQLEvent.OPEN, attachDBFunction(resultHandler,faultHandler,attachDBs)); 
			}
			if(faultHandler!=null)
			{
				connection.addEventListener(SQLErrorEvent.ERROR, faultHandler);
			}
			connection.openAsync(dbFile);
			
			
		}
		
		public static function attachDBFunction(resultHandler:Function=null,faultHandler:Function=null,attachDBs:ArrayCollection=null):Function
		{
			return function(event:SQLEvent):void
			{
				if(attachDBs!=null)
				{
					var connection:SQLConnection=event.target as SQLConnection;
					if(attachDBs.length>0)
					{
						var atachDB:File=attachDBs.removeItemAt(0) as File;
						connection.addEventListener(SQLEvent.ATTACH,attachDBFunction(resultHandler,faultHandler,attachDBs));
						if(faultHandler!=null)
						{
							connection.addEventListener(SQLErrorEvent.ERROR, faultHandler);
						}
						
						connection.attach(atachDB.name.substr( 0, atachDB.name.lastIndexOf( '.' ) ),atachDB);
					}
					else
					{
						resultHandler(event);
					}
					
				}
				else if(resultHandler!=null)
				{
					resultHandler(event);
				}
				
			}
		}
		
		
		public static function executeStatement(query:String, resultHandler:Function=null, faultHandler:Function=null,itemClass:Class=null):Function
		{
			return function(event:SQLEvent):void
			{
				var stmt:SQLStatement;
				stmt = new SQLStatement(); 
				stmt.sqlConnection = event.target as SQLConnection; 
				if(itemClass!=null)
				{
					stmt.itemClass=itemClass;
				}
				stmt.text = query;
				if(resultHandler!=null)
				{
					stmt.addEventListener(SQLEvent.RESULT, resultHandler); 
				}
				if(faultHandler!=null)
				{
					stmt.addEventListener(SQLErrorEvent.ERROR, faultHandler);
				}
				stmt.execute();
			}
		}
		
		public static function openAndExecute(dbFile:File, query:String, resultHandler:Function=null,faultHandler:Function=null,faultDBHandler:Function=null,attachDBs:ArrayCollection=null,itemClass:Class=null):void
		{
			openDB(dbFile,executeStatement(query,resultHandler,faultHandler,itemClass),faultDBHandler,attachDBs);
		}
		
	}
}