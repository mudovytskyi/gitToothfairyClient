package components.sqlite
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.errors.SQLError;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Fault;
	import mx.rpc.events.FaultEvent;
	
	public class SqliteFunctions
	{
		
		public static function openDB(dbFile:File,attachDBs:ArrayCollection=null,faultHandler:Function=null):SQLConnection
		{
			var connection:SQLConnection=new SQLConnection;
			try
			{
				connection.open(dbFile);				
			} 
			catch(error:SQLError) 
			{
				connection.close();
				if(faultHandler!=null)
				{
					var fault:Fault=new Fault(error.name,error.message, error.details);
					var errorEvent:FaultEvent=new FaultEvent(FaultEvent.FAULT,false,true,fault);
					faultHandler(errorEvent);
				}
				return null;
			}
			
			if(attachDBs!=null&&attachDBs.length>0)
			{
				for each (var atachDB:File in attachDBs) 
				{
					try
					{
						connection.attach(atachDB.name.substr( 0, atachDB.name.lastIndexOf( '.' ) ),atachDB);
					} 
					catch(error:SQLError) 
					{
						connection.close();
						if(faultHandler!=null)
						{
							fault=new Fault(error.name,error.message, error.details);
							errorEvent=new FaultEvent(FaultEvent.FAULT,false,true,fault);
							faultHandler(errorEvent);
						}
						return null;
					}
				}
			}
			return connection;
			
		}
		
		
		public static function executeStatement(connection:SQLConnection, query:String, itemClass:Class=null, faultHandler:Function=null):SQLResult
		{
			var stmt:SQLStatement;
			stmt = new SQLStatement(); 
			stmt.sqlConnection = connection; 
			if(itemClass!=null)
			{
				stmt.itemClass=itemClass;
			}
			stmt.text = query;
			try
			{
				stmt.execute();
			} 
			catch(error:SQLError) 
			{
				connection.close();
				if(faultHandler!=null)
				{
					var fault:Fault=new Fault(error.name,error.message, error.details);
					var errorEvent:FaultEvent=new FaultEvent(FaultEvent.FAULT,false,true,fault);
					faultHandler(errorEvent);
				}
				return null;
			}
			
			return stmt.getResult();
			
		}
		
	}
}