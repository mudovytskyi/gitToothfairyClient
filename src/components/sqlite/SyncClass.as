package components.sqlite
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.errors.SQLError;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.net.Responder;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	
	import services.ipClass;
	
	import valueObjects.SyncDictionaryModuleSumm;
	
	public class SyncClass
	{
		public static const DICTIONARY_NAMES:Array=[
			'PATIENTCARDDICTIONARIES', 
			'PROTOCOLS', 
			'PROTOCOLS_F43', 
			//'MKB10',
			'ASSISTANTS', 
			'DOCTORS', 
			'TEMPLATE', 
			'TEMPLATEPROC', 
			'ROOMS', 
			'WORKPLACES', 
			'TEETHEXAMDICTIONARIES', 
			'F43', 
			'F39', 
			'HEALTHPROC', 
			'HEALTHPROCFARM', 
			'TEMPLATETEST', 
			'FARM', 
			'F39HPROC',
			'UNITOFMEASURE',
			'FARMGROUP',
			'ACCOUNTFLOW_GROUPS',
			'ACCOUNTFLOW_TEXP_GROUPS',
			'ACCOUNT',
			'OCCLUSIONS',
			'COMORBIDITIES',
			'HYGIENE',
			'DOCTORS_SALARY_SCHEMA',
			'ASSISTANTS_SALARY_SCHEMA',
			'CLINICREGISTRATIONDATA',
			'INSURANCECOMPANYS',
			'FARMSUPPLIER',
			'SUPPLIER',
			//'SUPPLIERADDTAGS',
			//'SUPPLIERADDTAGSRECORDS',
			'OPERATINGSCHEDULE',
			'OPERATINGSCHEDULERULES',
			'CONTACTS',
			'DISCOUNT_SCHEMA',
			'SUBDIVISION',
			'FARMSTORAGE'//,
//			'NH_ULTRASONOGRAPHY_DICT'//,
			//'SUBSCRIPTION',
			//'SUBSCRIPTION_CARDS',
			//'SUBSCRIPTION_PROCEDURES',
			//'SUBSCRIPTION_TYPE_DICT'
		];
		
		private static var dictionaryNames:Array=[];
		private static var summsArray:ArrayCollection=new ArrayCollection;
		private static var index:int=-1;
		private static var resultHandler:Function=null;
		public static function getSummsArray(resultHandler:Function,dictionaryNames:Array=null):void
		{
			
			SyncClass.resultHandler=resultHandler;
			SyncClass.index=-1;
			summsArray=new ArrayCollection;
			if(dictionaryNames==null)
			{
				SyncClass.dictionaryNames=SyncClass.DICTIONARY_NAMES;
			}
			else
			{
				SyncClass.dictionaryNames=dictionaryNames;
			}
			getLocalSumms();
		}
		private static function getLocalSumms():void
		{
			SyncClass.index++;
			if(SyncClass.index<dictionaryNames.length)
			{
				getLocalSumm(dictionaryNames[index] as String);
			}
			else
			{
				resultHandler(summsArray);
			}
		}
		
		private static function getLocalSumm(dictionaryName:String):void
		{
			var dbFile:File = File.applicationStorageDirectory.resolvePath(ipClass.serverKey+File.separator+dictionaryName+".sqlite");
			var sql:String = "SELECT sum(version)*1000000 + sum(id) as summ FROM DICTIONARY";
			SqliteAsyncFunctions.openAndExecute(dbFile,sql,getLocalSummResult, getLocalSummError, errorLocalDBHandler);
			
		}
		
		private static function addSummNext(summ:int=NaN):void
		{
			var syncSumm:SyncDictionaryModuleSumm=new SyncDictionaryModuleSumm;
			syncSumm.NAME=dictionaryNames[index] as String;
			syncSumm.SUMM=summ;
			summsArray.addItem(syncSumm);
			getLocalSumms();
		}
		private static  function errorLocalDBHandler(event:SQLErrorEvent):void 
		{ 
			(event.target as SQLConnection).close();
			addSummNext();
			
		}
		
		private static var responderClose:Responder = new Responder(resultCloseHandler, errorCloseHandler);
		private static var tmpSumm:int = 0;
		
		private static var t:Timer;
		private static var con:SQLConnection;
		private static function getLocalSummResult(event:SQLEvent):void
		{ 
			var result:SQLResult = (event.target as SQLStatement).getResult();
			//var summ:int=0;
			tmpSumm = 0;
			if(result.data[0].summ != null)
			{
				//summ=int(result.data[0].summ);
				tmpSumm=int(result.data[0].summ);
			}
			//(event.target as SQLStatement).sqlConnection.close(responderClose);
			//addSummNext(summ); 
			con = (event.target as SQLStatement).sqlConnection;
			t = new Timer(100, 1);
			t.addEventListener(TimerEvent.TIMER, CloseDB);
			t.start();
		}
		
		private static function CloseDB(event:TimerEvent):void
		{
			t.stop();
			con.close(responderClose);
		}
		
		private static function resultCloseHandler(event:SQLEvent):void
		{
			addSummNext(tmpSumm);
		}
		private static function errorCloseHandler(event:SQLError):void
		{
			addSummNext(tmpSumm);
		}
		
		private static function getLocalSummError(event:SQLErrorEvent):void
		{
			(event.target as SQLStatement).sqlConnection.close();
			addSummNext();
		}
	}
}