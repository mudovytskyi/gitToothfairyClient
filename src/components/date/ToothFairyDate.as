package components.date
{
	import mx.formatters.DateFormatter;
	
	import toothfairy.configurations.ConstantSettings;
	
	[Bindable]
	public class ToothFairyDate
	{
		/**
		 * Return Formatted String (ToothFairy Date) from Date
		 * */
		public static function dateToToothFairyDateString(indate:Date):String
		{
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ConstantSettings.TOOTHFAIRY_DATA_FORMAT;
			return dateFormatter.format(indate);
		}
		/**
		 * Return Formatted String (ToothFairy Timestamp) from Date
		 * */
		public static function dateToToothFairyTimestampString(date:Date):String
		{
			//var df:DateTimeFormatter = new DateTimeFormatter();
			//df.dateTimePattern = "yyyy-MM-dd HH:mm:ss";
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ConstantSettings.TOOTHFAIRY_TIMESTAMP_FORMAT;
			return dateFormatter.format(date);	
		}
		
		
		
		
		
		
		/**
		 * Return Formatted String (ToothFairy Date) from Firebird String(Date)
		 * */
		public static function stringFirebirdDateToToothFairyStringDate(inputStringDate:String):String
		{
			return ToothFairyDate.dateToToothFairyDateString(ToolsForFirebird.stringFirebirdDateToFlexDate(inputStringDate)); 
		}
		
		/**
		 * Return Formatted String (ToothFairy Date) from Firebird String(Date)
		 * */
		public static function stringFirebirdDateToToothFairyDate(inputStringDate:String):Date
		{
			if(inputStringDate != null && inputStringDate != '')
				return ToolsForFirebird.stringFirebirdDateToFlexDate(inputStringDate);
			else
				return null;
		}
		
		/**
		 * Return Formatted String (ToothFairy Timestamp) from Firebird String(Timestamp)
		 * */
		public static function stringFirebirdTimestampToToothFairyStringTimestamp(inputStringTimestamp:String):String
		{
			if((inputStringTimestamp != null)&&(inputStringTimestamp != ''))
			{
				return ToothFairyDate.dateToToothFairyTimestampString(ToolsForFirebird.stringFirebirdTimestampToFlexDate(inputStringTimestamp)); 
			}
			else
			{
				return '';
			}
		}
		/**
		 * Return Formatted String (ToothFairy Date) from Firebird String(Timestamp)
		 * */
		public static function stringFirebirdTimestampToToothFairyStringDate(inputStringTimestamp:String):String
		{
			if((inputStringTimestamp != null)&&(inputStringTimestamp != ''))
			{
				return ToothFairyDate.dateToToothFairyDateString(ToolsForFirebird.stringFirebirdTimestampToFlexDate(inputStringTimestamp)); 
			}
			else
			{
				return '';
			}
		}
		/**
		 * Return Formatted String (ToothFairy Date) from Firebird String(Timestamp)
		 * */
		public static function stringFirebirdTimestampToToothFairyDate(inputStringTimestamp:String):Date
		{
			if((inputStringTimestamp != null)&&(inputStringTimestamp != ''))
			{
				return ToolsForFirebird.stringFirebirdTimestampToFlexDate(inputStringTimestamp); 
			}
			else
			{
				return null;
			}
		}
	}
}