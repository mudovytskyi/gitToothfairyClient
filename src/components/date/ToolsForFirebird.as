package components.date
{
	import mx.controls.DateField;
	import mx.formatters.DateFormatter;
	
	import toothfairy.configurations.ConstantSettings;
	
	[Bindable]
	public class ToolsForFirebird
	{
		/**
		 * Return String(DATE) for Firebird Database from Flex Date
		 * YYYY-MM-DD
		 * */
		public static function dateToFirebirdDateString(date:Date):String
		{
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ConstantSettings.FIREBIRD_DATA_FORMAT;
			return dateFormatter.format(date);	
		}
		
		/**
		 * Return Date from Firebird Database Date String
		 * from YYYY-MM-DD
		 * */
		public static function stringFirebirdDateToFlexDate(stringDate:String):Date
		{
			return DateField.stringToDate(stringDate,ConstantSettings.FIREBIRD_DATA_FORMAT);
		}
		
		
		
		/**
		 * Return TIMESTAMP String for Firebird Database from Date
		 * YYYY-MM-DD JJ:NN:SS
		 * */
		public static function dateToFirebirdTimestampString(date:Date):String
		{
			//var df:DateTimeFormatter = new DateTimeFormatter();
			//df.dateTimePattern = "yyyy-MM-dd HH:mm:ss";
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ConstantSettings.FIREBIRD_TIMESTAMP_FORMAT;
			return dateFormatter.format(date);	
		}
		
		/**
		 * Return DATE from Firebird Database TIMESTAMP String.
		 * Input string must by strongly YYYY-MM-DD JJ:MM:SS
		 * 
		 * isReturningNowDate - if TRUE returning NULL if String incorrct
		 *                      if FALSE returning now date
		 * */
		public static function stringFirebirdTimestampToFlexDate(str:String, isReturningNowDate:Boolean = false):Date
		{
			if(str != null)
			{
				var matches : Array = str.match(/(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/);
				if(matches != null)
				{
					var d : Date = new Date();
					d.setFullYear(int(matches[1]), int(matches[2]) - 1, int(matches[3]));
					d.setHours(int(matches[4]), int(matches[5]), int(matches[6]), 0);
					return d;
				}
				else
				{
					if(isReturningNowDate)
					{
						return new Date();
					}
					else
					{
						return null;
					}
				}
			}
			else
			{
				if(isReturningNowDate)
				{
					return new Date();
				}
				else
				{
					return null;
				}
			}
		}
	}
}