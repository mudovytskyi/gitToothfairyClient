package components.date
{
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;

	public class DateFormater
	{
		/**
		 * convert dates in the genitive case
		 * format example:16 Березня 2012
		 * */
		public static function dateToGenitiveCase(date:Date):String{
			var monthGenetiveCase:Array = new Array("Січня","Лютого","Березня","Квітня","Травня","Червня","Липня","Серпня","Вересня","Жовтня","Листопада","Грудня");
			var df:DateFormatter = new DateFormatter();
			df.formatString = "DD "+monthGenetiveCase[date.month]+" YYYY"
			return df.format(date);
		}
		
		/**
		 * convert dates in month string
		 * format example:Березнень
		 * */
		public static function dateToMonthString(date:Date):String
		{
			if(date != null)
			{
				var a:String = ResourceManager.getInstance().getString('components_DateFormatter', 'm1');
				var monthStrings:Array = 
					new Array(
						ResourceManager.getInstance().getString('components_DateFormatter', 'm1'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm2'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm3'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm4'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm5'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm6'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm7'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm8'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm9'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm10'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm11'),
						ResourceManager.getInstance().getString('components_DateFormatter', 'm12') );
				return monthStrings[date.month];
			}
			else
				return null;
		}
	}
}