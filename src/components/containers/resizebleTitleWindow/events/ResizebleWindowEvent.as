package components.containers.resizebleTitleWindow.events
{
	import flash.events.Event;
	
	import valueObjects.ChiefSalaryModuleDoctorAwards;
	
	public class ResizebleWindowEvent extends Event
	{
		public static const CLOSE_EVENT:String = 'closeResizebleWindow';
		
		public static const CLOSE:uint = 0;
		public static const REFRESH:uint = 1;
		
		public var closeDetail:uint;
		
		public function ResizebleWindowEvent(closeDetail:uint=CLOSE, type:String=CLOSE_EVENT,  bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.closeDetail = closeDetail;
		}
		
		
		public override function clone():Event
		{
			return new ResizebleWindowEvent(closeDetail, type, bubbles, cancelable);
		}
	}
}