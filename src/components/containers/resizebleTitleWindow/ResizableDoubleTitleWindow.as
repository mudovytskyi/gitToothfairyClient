package components.containers.resizebleTitleWindow
{
	public class ResizableDoubleTitleWindow extends ResizableTitleWindow
	{
		[Bindable]
		public var secondTitle:String='';
		
		public function ResizableDoubleTitleWindow()
		{
			super();
		}
	}
}