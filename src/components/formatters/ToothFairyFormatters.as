package components.formatters
{
	import mx.formatters.PhoneFormatter;
	import mx.utils.StringUtil;
	
	import toothfairy.configurations.ConfigurationSettings;

	[Bindable]
	public class ToothFairyFormatters
	{
		public static function mobilephoneFormat(input:String):String
		{
			var mobilephoneFormmater:PhoneFormatter;
			mobilephoneFormmater = new PhoneFormatter;
			mobilephoneFormmater.formatString="+"+StringUtil.repeat('#',ConfigurationSettings.countryPhoneCode.length)+"(###) ###-##-##";
			//mobilephoneFormmater.formatString="+##(###) ###-##-##";
			return mobilephoneFormmater.format(input);
		}
	}
}