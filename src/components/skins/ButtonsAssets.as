package components.skins
{
	[Bindable]
	public class ButtonsAssets
	{
		[Embed(source="components/skins/assets/okIcon8.png")]
		public static var ICON_OK_8:Class;
		[Embed(source="components/skins/assets/okIcon15.png")]
		public static var ICON_OK_15:Class;
		[Embed(source="components/skins/assets/okIcon20.png")]
		public static var ICON_OK_20:Class;
		
		
		[Embed(source="components/skins/assets/cancelIcon8.png")]
		public static var ICON_CANCEL_8:Class;
		[Embed(source="components/skins/assets/cancelIcon15.png")]
		public static var ICON_CANCEL_15:Class;
		[Embed(source="components/skins/assets/cancelIcon20.png")]
		public static var ICON_CANCEL_20:Class;
		
		
		[Embed(source="components/skins/assets/refreshIcon8.png")]
		public static var ICON_REFRESH_8:Class;
		[Embed(source="components/skins/assets/refreshIcon15.png")]
		public static var ICON_REFRESH_15:Class;
		[Embed(source="components/skins/assets/refreshIcon20.png")]
		public static var ICON_REFRESH_20:Class;
		
		
		[Embed(source="components/skins/assets/addIcon35.png")]
		public static var ICON_ADD_35:Class;
		[Embed(source="components/skins/assets/editIcon35.png")]
		public static var ICON_EDIT_35:Class;
		[Embed(source="components/skins/assets/deleteIcon35.png")]
		public static var ICON_DELETE_35:Class;
		
		
		[Embed(source="components/skins/assets/searchIcon20.png")]
		public static var ICON_SEARCH_20:Class;
		[Embed(source="components/skins/assets/searchIcon35.png")]
		public static var ICON_SEARCH_35:Class;
		
		
		[Embed(source="components/skins/assets/printInvoice20.png")]
		public static var ICON_PRINT_20:Class;
		
	}
}