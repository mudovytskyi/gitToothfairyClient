package components.string
{
	import spark.components.TextInput;
	import spark.events.TextOperationEvent;

	[Bindable]
	public class TextLib
	{
		
		public static function upperFirstChar(event:TextOperationEvent):void
		{
			var textinputcontrol:TextInput = event.target as TextInput;
			if(textinputcontrol.text)
			{
				var stringtxt:String = textinputcontrol.text;
				var firstLetter:String = stringtxt.charAt(0).toUpperCase();
				if (stringtxt.length > 0)
					var restWord:String = stringtxt.substr(1, stringtxt.length);
				textinputcontrol.text = firstLetter + restWord;
				//textinputcontrol.appendText(firstLetter + restWord);
				textinputcontrol.selectRange(stringtxt.length, stringtxt.length);
			}
		}
		
		/**
		 * Transliterating from Cyrillic(RU,UA) (String) to English (String)
		 * */
		public static function translit2Latin(s:String):String
		{
			var arr:Array = new Array(["А","A"],
				["Б","B"],
				["В","V"],
				["Г","G"],
				["Д","D"],
				["Е","E"],
				["Ё","Yo"],
				["Є","Ye"],
				["Ж","Zh"],
				["З","Z"],
				["І","I"],
				["Ї","Yi"],
				["И","I"],
				["Й","J"],
				["К","K"],
				["Л","L"],
				["М","M"],
				["Н","N"],
				["О","O"],
				["П","P"],
				["Р","R"],
				["С","S"],
				["Т","T"],
				["У","U"],
				["Ф","F"],
				["Х","Kh"],
				["Ц","Ts"],
				["Ч","Ch"],
				["Ш","Sh"],
				["Щ","Shch"],
				["Ъ",'"'],
				["Ы","Y"],
				["Ь","'"],
				["Э","E"],
				["Ю","Yu"],
				["Я","Ya"],
				["а","a"],
				["б","b"],
				["в","v"],
				["г","g"],
				["д","d"],
				["е","e"],
				["ё","yo"],
				["є","ye"],
				["ж","zh"],
				["з","z"],
				["і","i"],
				["и","i"],
				["й","j"],
				["к","k"],
				["ї","yi"],
				["л","l"],
				["м","m"],
				["н","n"],
				["о","o"],
				["п","p"],
				["р","r"],
				["с","s"],
				["т","t"],
				["у","u"],
				["ф","f"],
				["х","kh"],
				["ц","ts"],
				["ч","ch"],
				["ш","sh"],
				["щ","shch"],
				["ъ",'"'],
				["ы","y"],
				["ь","'"],
				["э","e"],
				["ю","yu"],
				["я","ya"]);
			var r:String = "";
			var b:String = "";
			var lenS:uint = s.length;
			var lenA:uint = arr.length;
			var i:uint = 0;
			var j:uint;
			while (i < lenS)
			{
				b = s.substr(i,1);
				j = 0;
				while (j < lenA)
				{
					if (b == arr[j][0])
					{
						b = arr[j][1];
					}
					j++;
				}
				r +=  b;
				i++;
			}
			return r;
		}
		
		/**
		 * Transliterating from Cyrillic(RU,UA) (String) to English (String) without apostrophes
		 * */
		public static function translit2LatinWA(s:String):String
		{
			var arr:Array = new Array(["А","A"],
				["Б","B"],
				["В","V"],
				["Г","G"],
				["Д","D"],
				["Е","E"],
				["Ё","Yo"],
				["Є","Ye"],
				["Ж","Zh"],
				["З","Z"],
				["І","I"],
				["Ї","Yi"],
				["И","I"],
				["Й","J"],
				["К","K"],
				["Л","L"],
				["М","M"],
				["Н","N"],
				["О","O"],
				["П","P"],
				["Р","R"],
				["С","S"],
				["Т","T"],
				["У","U"],
				["Ф","F"],
				["Х","Kh"],
				["Ц","Ts"],
				["Ч","Ch"],
				["Ш","Sh"],
				["Щ","Shch"],
				["Ъ",''],
				["Ы","Y"],
				["Ь","'"],
				["Э","E"],
				["Ю","Yu"],
				["Я","Ya"],
				["а","a"],
				["б","b"],
				["в","v"],
				["г","g"],
				["д","d"],
				["е","e"],
				["ё","yo"],
				["є","ye"],
				["ж","zh"],
				["з","z"],
				["і","i"],
				["и","i"],
				["й","j"],
				["к","k"],
				["ї","yi"],
				["л","l"],
				["м","m"],
				["н","n"],
				["о","o"],
				["п","p"],
				["р","r"],
				["с","s"],
				["т","t"],
				["у","u"],
				["ф","f"],
				["х","kh"],
				["ц","ts"],
				["ч","ch"],
				["ш","sh"],
				["щ","shch"],
				["ъ",''],
				["ы","y"],
				["ь","'"],
				["э","e"],
				["ю","yu"],
				["я","ya"]);
			var r:String = "";
			var b:String = "";
			var lenS:uint = s.length;
			var lenA:uint = arr.length;
			var i:uint = 0;
			var j:uint;
			while (i < lenS)
			{
				b = s.substr(i,1);
				j = 0;
				while (j < lenA)
				{
					if (b == arr[j][0])
					{
						b = arr[j][1];
					}
					j++;
				}
				r +=  b;
				i++;
			}
			return r;
		}
	}
}