package components
{
	import mx.collections.ArrayCollection;

	public class AutoCompleteUtils
	{
		public static function parseDataFromDB(data:String):ArrayCollection
		{
			if(data != null && data.length>0)
			{
				var outArray:ArrayCollection = new ArrayCollection;
				var dataArray:Array = data.split("|");
				for (var i:int = 0; i < dataArray.length; i++) 
				{
					outArray.addItem(dataArray[i]);
				}
				return outArray;
			}
			return null;
		}
		
		public static function parseDataToDB(dataArray:ArrayCollection):String
		{
			var outString:String = '';
			for (var j:int = 0; j < dataArray.length; j++) 
			{
				outString += String(dataArray[j]);
				if(j != dataArray.length-1)
					outString += '|';
			}
			return outString;
		}
	}
}