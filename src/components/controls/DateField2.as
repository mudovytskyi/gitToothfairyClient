package components.controls
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	import mx.controls.DateField;
	import mx.controls.TextInput;
	import mx.events.CalendarLayoutChangeEvent;
	
	import components.date.ToolsForFirebird;
	
	import toothfairy.configurations.ConstantSettings;
	
	
	public class DateField2 extends DateField
	{
		public static function keyUpHandler(event:KeyboardEvent):void
		{
			var seporatorF:int = 2;
			var seporatorS:int = 5;
			if(ConstantSettings.TOOTHFAIRY_DATA_TYPE == 3)
			{
				seporatorF = 4;
				seporatorS = 7;
			}
			
			if(event.charCode>47 && event.charCode<58)
			{
				var dfInput:DateField2 = event.currentTarget as DateField2
				if(dfInput !=null && (dfInput.text.length==seporatorF || dfInput.text.length==seporatorS))
				{
					dfInput.text+=ConstantSettings.TOOTHFAIRY_DATA_SEPARATOR;
					dfInput.textField.setSelection(dfInput.text.length,dfInput.text.length);
				}
			}
		}
		
		public function DateField2()
		{
			super();
			this.addEventListener(CalendarLayoutChangeEvent.CHANGE,dateChangeHandler);
		}
		
		private function dateChangeHandler(event:CalendarLayoutChangeEvent):void
		{
			dispatchEvent(new Event("firebirdStringChange"));
		}

		[Bindable(event="firebirdStringChange")]
		public function get firebirdString():String
		{
			return ToolsForFirebird.dateToFirebirdDateString(this.selectedDate);
		}

		public function set firebirdString(value:String):void
		{

			this.selectedDate=ToolsForFirebird.stringFirebirdDateToFlexDate(value);
			dispatchEvent(new Event("firebirdStringChange"));
		}
		
		override public function set selectedDate(value:Date):void
		{
			super.selectedDate = value;
			dispatchEvent(new Event("firebirdStringChange"))
		}	
		
		public function get textField():TextInput
		{
			return textInput as TextInput;
		}
	}
}