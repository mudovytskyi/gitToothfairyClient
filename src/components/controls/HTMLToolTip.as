/*
To replace the default ToolTip class by the custom HTMLToolTip class, 
just call (in the creationComplete() handler of your application for example):
ToolTipManager.toolTipClass = HTMLToolTip;

Tooltip text can then include the HTML tags supported by Flex.

Example:
var tip:String;
tip = "<font color='#076baa' size='+4'><b>Easily style your tooltips:</b></font><br><br>";
tip += "<i>Italic</i>, <b>Bold</b>, <i><b>Bold+Italic</b></i>, <u>Underlined</u>, ...<br>";
tip += "<br><font color='#FF0000'><b>Choose your favorite color</b></font><br>";
tip += "<br><font size='24'>Use a big font size</font><br>";
tip += "<font size='6'>or a very small one</font>";            
myObject.toolTip = tip;

*/

package components.controls
{
	import mx.controls.ToolTip;
	
	public class HTMLToolTip extends ToolTip
	{
		public function HTMLToolTip()
		{    super(); }
		
		/***
		 * commitProperties is called whenever the tooltip text is changed
		 * Just copy the new text in the htmlText property of the IUITextField
		 * embedded in ToolTip object and you're done!
		 ***/
		
		override protected function commitProperties():void{
			super.commitProperties();
			textField.htmlText = text;
		}
	}
}
