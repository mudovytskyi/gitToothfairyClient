/**
 *  Add a function to GridColumn that will be used to compute what is displayed
 *  in the footer
 */
package components.controls.datagrid
{
    import spark.components.gridClasses.GridColumn;
    
    public class FooterGridColumn extends GridColumn
    {
        public function FooterGridColumn(columnName:String=null)
        {
            super(columnName);
        }
        
        /**
         *  summaryFunction(dataProvider, columnName):Object;  // usually number or string
         */
        public var summaryFunction:Function;
    }
}