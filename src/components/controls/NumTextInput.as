package components.controls
{
	import flash.events.Event;
	
	import mx.events.ValidationResultEvent;
	
	import spark.components.TextInput;
	import spark.events.TextOperationEvent;
	import spark.validators.NumberValidator;
	
	import flashx.textLayout.operations.InsertTextOperation;
	
	[Event(name="validValue", type="flash.events.Event")]
	public class NumTextInput extends TextInput
	{
		
		private var validator:NumberValidator;
		

		[Inspectable(category="General", enumeration="int,real",
		                                     defaultValue="real")]
		[Bindable]
		public function get validatorDomain():String
		{
			return validator.domain;;
		}

		public function set validatorDomain(value:String):void
		{
			validator.domain=validatorDomain;
		}

		

		[Bindable]
		public function get allowNegative():Boolean
		{
			return validator.allowNegative;
		}

		public function set allowNegative(value:Boolean):void
		{
			validator.allowNegative = value;
		}

		

		[Bindable]
		public function get required():Boolean
		{
			return validator.required;
		}

		public function set required(value:Boolean):void
		{
			validator.required = value;
		}

		
		public function NumTextInput()
		{
			super();
			this.addEventListener(TextOperationEvent.CHANGING, onChanging);
			validator=new NumberValidator();
			validator.source=this;
			validator.property="text";
			validator.trigger=this;
			validator.triggerEvent="change";			
			validator.addEventListener(ValidationResultEvent.VALID, valid);
		}
		
		protected function onChanging(event:TextOperationEvent):void
		{
			if(event.operation is InsertTextOperation)
			{
				(event.operation as InsertTextOperation).text = (event.operation as InsertTextOperation).text.replace(',', '.')
			}
			
		}
		
		[Bindable(event="numValueChange")]
		public function get numValue():Number
		{
			return parseFloat(text);
		}

		public function set numValue(value:Number):void
		{
			if( text != value.toFixed(fractionalDigits))
			{
				text = value.toFixed(fractionalDigits)
				dispatchEvent(new Event("numValueChange"));
			}
		}
		
		
		[Bindable(event="intValueChange")]
		public function get intValue():Number
		{
			return parseInt(text);
		}
		
		public function set intValue(value:Number):void
		{
			if( text != value.toFixed(0))
			{
				text = value.toFixed(0)
				dispatchEvent(new Event("intValueChange"));
			}
		}
		
		private function valid(event:ValidationResultEvent):void
		{
			dispatchEvent(new Event("validValue"));
			dispatchEvent(new Event("intValueChange"));
			dispatchEvent(new Event("numValueChange"));
		}
		
		public function get requiredFieldError():String
		{
			return validator.requiredFieldError;
		}
		
		public function set requiredFieldError(value:String):void
		{
			validator.requiredFieldError = value;
		}
		
		
		public function get invalidCharError():String
		{
			return validator.invalidCharError;
		}
		
		public function set invalidCharError(value:String):void
		{
			validator.invalidCharError = value;
		}
		
		
		public function get lessThanMinError():String
		{
			return validator.lessThanMinError;
		}
		
		public function set lessThanMinError(value:String):void
		{
			validator.lessThanMinError = value;
		}
		
		public function get fractionalDigits():int
		{
			return validator.fractionalDigits;
		}
		
		public function set fractionalDigits(value:int):void
		{
			validator.fractionalDigits = value;
		}
		
	}
}