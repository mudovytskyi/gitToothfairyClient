﻿package components.controls.dockmenu{
	
	import com.greensock.TweenLite;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import spark.components.Button;
	
	public class DockItem extends Button{
		
		
		private var _initPosition:Number;
		public var maxXDistance:Number;
		public var maxYDistance:Number;
		public var maxScale:Number;
		
		public function DockItem($maxXDistance:Number=60,$maxYDistance:Number=30,$maxScale:Number=2):void
		{
			maxXDistance=$maxXDistance;
			maxYDistance=$maxYDistance;
			maxScale=$maxScale;
			if(stage) 
			{
				init();
			}
			else 
			{
				addEventListener(Event.ADDED_TO_STAGE,init);
			}
			addEventListener(Event.REMOVED_FROM_STAGE,end);
		}
	
		private function init(e:Event=null):void
		{
			_initPosition = Number(horizontalCenter);
			stage.addEventListener(MouseEvent.MOUSE_MOVE,mouseMove);
			stage.addEventListener(Event.MOUSE_LEAVE,mouseLeave);
		}
		
		private function mouseLeave(e:Event):void
		{
			TweenLite.to(this,.3,{horizontalCenter:_initPosition,scaleX:1,scaleY:1});
		}
		
		private function mouseMove(e:MouseEvent):void
		{
			var yDistance:Number=Math.abs(parent.mouseY-y);
			if(yDistance>maxYDistance)
			{
				if(_initPosition==horizontalCenter) 
				{
					return;
				}
				else
				{
					TweenLite.to(this,.3,{horizontalCenter:_initPosition,scaleX:1,scaleY:1});
					return;
				}
			}
			
			//get the difference between the parent mouse x position and the initial position of the object
			var xDistance:Number = parent.mouseX-parent.width/2-_initPosition;
			
			//check if the distance of the mouse from the object is more than max distance, it can't be bigger...
			xDistance = xDistance > maxXDistance ? maxXDistance : xDistance;
			
			//check if the distance is lower than the negative of the max distance, it can't be lower...
			xDistance = xDistance < -maxXDistance ? -maxXDistance : xDistance;
			
			//create a variable for the position, assuming that the x position must be the initial position plus the distance of the mouse, but it can't be more than the max distance.
			var posX:Number = _initPosition - xDistance;
			
			//we get the scale proportion here, it goes from 0 to maxScale variable
			var scale:Number = (maxXDistance - Math.abs(xDistance)) / maxXDistance;
			
			//the minimum scale is 1, the original size, and the max scale will be maxScale variable + 1
			scale = 1 + (maxScale * scale);
			
			//here we use a Tween to set the new position according to the mouse position
			TweenLite.to(this,.3,{horizontalCenter:posX,scaleX:scale,scaleY:scale});
		}
		
		private function end(e:Event=null):void{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE,mouseMove);
			stage.removeEventListener(Event.MOUSE_LEAVE,mouseLeave);
		}
	}	
}