﻿package components.controls.dockmenu{
	
	import flash.geom.Rectangle;
	
	public class DockButton extends DockItem{
		
		
		
		public function DockButton():void{
			super(70, 60, 2);
			buttonMode=true;
			mouseChildren=false;
			var bounds:Rectangle=getBounds(this);
			this.graphics.beginFill(0,0);
			this.graphics.drawRect(bounds.x,bounds.y,bounds.width,bounds.height);
		}
		
		[Bindable]
		public var isSelected:Boolean;
		
		[Bindable]
		public var iconButton:Class;
		
	}
}