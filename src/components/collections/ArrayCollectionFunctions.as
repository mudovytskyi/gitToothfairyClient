package components.collections
{
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.utils.ObjectUtil;

	public class ArrayCollectionFunctions
	{
		/*
		* Фукнкция поска объекта в ArrayCollection по определенному свойству
		* findInCollection(AC,findFunction('dateday', 1)
		*/
		public static function findInCollection(c:ArrayCollection, findFunction:Function):Array
		{
			var matches : Array = c.source.filter( findFunction );
			return matches;
		}
		
		/*
		* Фукнкция поска по определенному свойству
		*/
		public static function findFunction(propertyName:String,propertyValue:*):Function
		{
			return function( element : *, index : int, array : Array ) : Boolean
			{
				return element[propertyName] == propertyValue;
			}
		}
		
		public static function arrayCollectionDateSort(ar:ArrayCollection,field:String):void 
		{
			var s:Sort = new Sort();
			s.compareFunction = DateCompareFunction(field);
			ar.sort = s;
			ar.refresh();
		} 
		
		private static function DateCompareFunction(field:String):Function
		{
			return function(a:Object, b:Object, fields:Array=null):int
			{
				
				return ObjectUtil.dateCompare(a[field],b[field]); //сортируем по field
				
			}
		}
	}
}