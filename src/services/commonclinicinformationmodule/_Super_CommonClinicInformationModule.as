/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - CommonClinicInformationModule.as.
 */
package services.commonclinicinformationmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.CommonClinicInformationModuleAddressData;
import valueObjects.CommonClinicInformationModuleClinicData;
import valueObjects.CommonClinicInformationModuleLicenseData;
import valueObjects.CommonClinicInformationModulePhone;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_CommonClinicInformationModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_CommonClinicInformationModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.CommonClinicInformationModuleClinicData._initRemoteClassAlias();
        valueObjects.CommonClinicInformationModuleLicenseData._initRemoteClassAlias();
        valueObjects.CommonClinicInformationModuleAddressData._initRemoteClassAlias();
        valueObjects.CommonClinicInformationModulePhone._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClinicRegistrationData");
         operation.resultElementType = valueObjects.CommonClinicInformationModuleClinicData;
        operations["getClinicRegistrationData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addClinicRegistrationData");
         operation.resultType = Boolean;
        operations["addClinicRegistrationData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateClinicRegistrationData");
         operation.resultType = Boolean;
        operations["updateClinicRegistrationData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getLicenses");
         operation.resultElementType = valueObjects.CommonClinicInformationModuleLicenseData;
        operations["getLicenses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveLicenses");
         operation.resultType = valueObjects.CommonClinicInformationModuleLicenseData;
        operations["saveLicenses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteLicenseByID");
         operation.resultType = Boolean;
        operations["deleteLicenseByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAddresses");
         operation.resultElementType = valueObjects.CommonClinicInformationModuleAddressData;
        operations["getAddresses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveAddress");
         operation.resultType = valueObjects.CommonClinicInformationModuleAddressData;
        operations["saveAddress"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteAddressByID");
         operation.resultType = Boolean;
        operations["deleteAddressByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPhones");
         operation.resultElementType = valueObjects.CommonClinicInformationModulePhone;
        operations["getPhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "savePhones");
         operation.resultType = valueObjects.CommonClinicInformationModulePhone;
        operations["savePhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePhoneByID");
         operation.resultType = Boolean;
        operations["deletePhoneByID"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "CommonClinicInformationModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "CommonClinicInformationModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.CommonClinicInformationModuleClinicData, p2:valueObjects.CommonClinicInformationModuleLicenseData, p3:valueObjects.CommonClinicInformationModuleAddressData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClinicRegistrationData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClinicRegistrationData() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClinicRegistrationData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addClinicRegistrationData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addClinicRegistrationData(clinicRegistrationData:valueObjects.CommonClinicInformationModuleClinicData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addClinicRegistrationData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(clinicRegistrationData) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateClinicRegistrationData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateClinicRegistrationData(clinicRegistrationData:valueObjects.CommonClinicInformationModuleClinicData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateClinicRegistrationData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(clinicRegistrationData) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getLicenses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getLicenses(clinic_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getLicenses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(clinic_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveLicenses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveLicenses(license:valueObjects.CommonClinicInformationModuleLicenseData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveLicenses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(license) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteLicenseByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteLicenseByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteLicenseByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAddresses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAddresses(clinic_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAddresses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(clinic_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveAddress' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveAddress(address:valueObjects.CommonClinicInformationModuleAddressData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveAddress");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(address) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteAddressByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteAddressByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteAddressByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPhones(clinic_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(clinic_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'savePhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function savePhones(phone:valueObjects.CommonClinicInformationModulePhone) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("savePhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(phone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePhoneByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePhoneByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePhoneByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
}

}
