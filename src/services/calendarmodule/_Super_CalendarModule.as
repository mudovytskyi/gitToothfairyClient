/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - CalendarModule.as.
 */
package services.calendarmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.CalendarModuleDaySchedule;
import valueObjects.CalendarModuleDoctor;
import valueObjects.CalendarModuleInsuranceCompanyObject;
import valueObjects.CalendarModuleInsurancePolisObject;
import valueObjects.CalendarModuleInterval;
import valueObjects.CalendarModuleIntervalItem;
import valueObjects.CalendarModulePatient;
import valueObjects.CalendarModulePatientDOB;
import valueObjects.CalendarModuleRequestData;
import valueObjects.CalendarModuleResponseData;
import valueObjects.CalendarModuleRoom;
import valueObjects.CalendarModuleSchedule;
import valueObjects.CalendarModuleScheduleRule;
import valueObjects.CalendarModuleTableTask;
import valueObjects.CalendarModuleTaskItem;
import valueObjects.CalendarModuleWorkPlace;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_CalendarModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_CalendarModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.CalendarModuleTableTask._initRemoteClassAlias();
        valueObjects.CalendarModulePatient._initRemoteClassAlias();
        valueObjects.CalendarModuleDoctor._initRemoteClassAlias();
        valueObjects.CalendarModuleRoom._initRemoteClassAlias();
        valueObjects.CalendarModuleWorkPlace._initRemoteClassAlias();
        valueObjects.CalendarModulePatientDOB._initRemoteClassAlias();
        valueObjects.CalendarModuleSchedule._initRemoteClassAlias();
        valueObjects.CalendarModuleResponseData._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTableTasksFilteredMultiDate");
         operation.resultElementType = valueObjects.CalendarModuleTableTask;
        operations["getTableTasksFilteredMultiDate"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addTableTask");
         operation.resultType = String;
        operations["addTableTask"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addTableTaskWizard");
         operation.resultType = String;
        operations["addTableTaskWizard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTableTask");
         operation.resultType = Boolean;
        operations["updateTableTask"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "clearSMSsenderForTask");
         operation.resultType = Boolean;
        operations["clearSMSsenderForTask"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteTableTasks");
         operation.resultType = Boolean;
        operations["deleteTableTasks"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatients");
         operation.resultElementType = valueObjects.CalendarModulePatient;
        operations["getPatients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientById");
         operation.resultElementType = valueObjects.CalendarModulePatient;
        operations["getPatientById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientByIdWizard");
         operation.resultElementType = valueObjects.CalendarModulePatient;
        operations["getPatientByIdWizard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.CalendarModuleDoctor;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getRooms");
         operation.resultElementType = valueObjects.CalendarModuleRoom;
        operations["getRooms"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getWorkPlaces");
         operation.resultElementType = valueObjects.CalendarModuleWorkPlace;
        operations["getWorkPlaces"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsDOB");
         operation.resultElementType = valueObjects.CalendarModulePatientDOB;
        operations["getPatientsDOB"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctor");
         operation.resultType = valueObjects.CalendarModuleDoctor;
        operations["getDoctor"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getRoom");
         operation.resultElementType = valueObjects.CalendarModuleRoom;
        operations["getRoom"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getScheduleOnCabinets");
         operation.resultElementType = valueObjects.CalendarModuleSchedule;
        operations["getScheduleOnCabinets"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getScheduleOnDoctors");
         operation.resultElementType = Object;
        operations["getScheduleOnDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getScheduleOnDoctorsWizard");
         operation.resultElementType = Object;
        operations["getScheduleOnDoctorsWizard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getCalendarData");
         operation.resultElementType = valueObjects.CalendarModuleResponseData;
        operations["getCalendarData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "sendBirthDayMessage");
         operation.resultType = int;
        operations["sendBirthDayMessage"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTableTaskNoticed");
         operation.resultType = Boolean;
        operations["updateTableTaskNoticed"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTableTaskVisited");
         operation.resultType = Boolean;
        operations["updateTableTaskVisited"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTableTaskOutcome");
         operation.resultType = Boolean;
        operations["updateTableTaskOutcome"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTableTaskSubscription");
         operation.resultType = Boolean;
        operations["updateTableTaskSubscription"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printPatientsDOB");
         operation.resultType = String;
        operations["printPatientsDOB"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePatientsDOBPrint");
         operation.resultType = Boolean;
        operations["deletePatientsDOBPrint"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setApplicationSetting");
         operation.resultType = Boolean;
        operations["setApplicationSetting"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getApplicationSettings");
         operation.resultElementType = Object;
        operations["getApplicationSettings"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "CalendarModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "CalendarModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.CalendarModuleWorkPlace, p2:valueObjects.CalendarModuleRoom, p3:valueObjects.CalendarModuleDoctor, p4:valueObjects.CalendarModulePatientDOB, p5:valueObjects.CalendarModulePatient, p6:valueObjects.CalendarModuleTableTask, p7:valueObjects.CalendarModuleInsuranceCompanyObject, p8:valueObjects.CalendarModuleInsurancePolisObject, p9:valueObjects.CalendarModuleScheduleRule, p10:valueObjects.CalendarModuleDaySchedule, p11:valueObjects.CalendarModuleSchedule, p12:valueObjects.CalendarModuleRequestData, p13:valueObjects.CalendarModuleResponseData, p14:valueObjects.CalendarModuleInterval, p15:valueObjects.CalendarModuleSchedule, p16:valueObjects.CalendarModuleTaskItem, p17:valueObjects.CalendarModuleIntervalItem) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTableTasksFilteredMultiDate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTableTasksFilteredMultiDate(taskdatestart:String, taskdateend:String, patientid:String, doctorid:String, roomid:String, workplaceid:String, noticed:String, factofvisit:String, workdescription:String, comment:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTableTasksFilteredMultiDate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(taskdatestart,taskdateend,patientid,doctorid,roomid,workplaceid,noticed,factofvisit,workdescription,comment) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addTableTask' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addTableTask(task:valueObjects.CalendarModuleTableTask) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addTableTask");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addTableTaskWizard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addTableTaskWizard(task:valueObjects.CalendarModuleTableTask) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addTableTaskWizard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTableTask' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTableTask(task:valueObjects.CalendarModuleTableTask) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTableTask");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'clearSMSsenderForTask' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function clearSMSsenderForTask(taskId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("clearSMSsenderForTask");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(taskId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteTableTasks' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteTableTasks(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteTableTasks");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatients(isFullName:Boolean, isLazyLoading:Boolean, findCharacters:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isFullName,isLazyLoading,findCharacters) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientById(isFullName:Boolean, patientId:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isFullName,patientId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientByIdWizard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientByIdWizard(isFullName:Boolean, patientId:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientByIdWizard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isFullName,patientId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors(nowdate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(nowdate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getRooms' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getRooms(unvisibleRoomIds:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getRooms");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(unvisibleRoomIds) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getWorkPlaces' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getWorkPlaces(RoomID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getWorkPlaces");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(RoomID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsDOB' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsDOB(startDate:String, endDate:String, withoutArchive:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsDOB");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,withoutArchive) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctor' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctor(id:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctor");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getRoom' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getRoom(id:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getRoom");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getScheduleOnCabinets' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getScheduleOnCabinets(cabinetIDs:ArrayCollection, startDate:String, endDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getScheduleOnCabinets");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(cabinetIDs,startDate,endDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getScheduleOnDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getScheduleOnDoctors(doctorIDs:ArrayCollection, startDate:String, endDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getScheduleOnDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorIDs,startDate,endDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getScheduleOnDoctorsWizard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getScheduleOnDoctorsWizard(doctorIDs:ArrayCollection, startDate:String, endDate:String, min_diap:int, clinic_schedule:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getScheduleOnDoctorsWizard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorIDs,startDate,endDate,min_diap,clinic_schedule) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getCalendarData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getCalendarData(request:valueObjects.CalendarModuleRequestData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getCalendarData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(request) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'sendBirthDayMessage' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function sendBirthDayMessage(ids:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("sendBirthDayMessage");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ids) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTableTaskNoticed' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTableTaskNoticed(task_id:String, noticed:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTableTaskNoticed");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task_id,noticed) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTableTaskVisited' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTableTaskVisited(task_id:String, visited:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTableTaskVisited");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task_id,visited) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTableTaskOutcome' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTableTaskOutcome(task_id:String, outcome:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTableTaskOutcome");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task_id,outcome) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTableTaskSubscription' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTableTaskSubscription(task_id:String, subscription_card_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTableTaskSubscription");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task_id,subscription_card_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printPatientsDOB' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printPatientsDOB(doc_type:String, startTime:String, endTime:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printPatientsDOB");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doc_type,startTime,endTime) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePatientsDOBPrint' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePatientsDOBPrint(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePatientsDOBPrint");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setApplicationSetting' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setApplicationSetting(paramName:String, paramValue:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setApplicationSetting");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramName,paramValue) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getApplicationSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getApplicationSettings(paramNames:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getApplicationSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramNames) ;
        return _internal_token;
    }
     
}

}
