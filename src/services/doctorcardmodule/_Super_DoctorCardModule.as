/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - DoctorCardModule.as.
 */
package services.doctorcardmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.DoctorCardModuleDoctor;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_DoctorCardModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_DoctorCardModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.DoctorCardModuleDoctor._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorsFiltered");
         operation.resultElementType = valueObjects.DoctorCardModuleDoctor;
        operations["getDoctorsFiltered"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addDoctor");
         operation.resultType = int;
        operations["addDoctor"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateDoctor");
         operation.resultType = Boolean;
        operations["updateDoctor"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteDoctors");
         operation.resultType = Boolean;
        operations["deleteDoctors"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "DoctorCardModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "DoctorCardModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.DoctorCardModuleDoctor) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorsFiltered' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorsFiltered(lastname:String, firedFilter:int, subdivId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorsFiltered");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(lastname,firedFilter,subdivId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addDoctor' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addDoctor(newDoctor:valueObjects.DoctorCardModuleDoctor) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addDoctor");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newDoctor) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateDoctor' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateDoctor(newDoctor:valueObjects.DoctorCardModuleDoctor) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateDoctor");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newDoctor) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteDoctors(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
}

}
