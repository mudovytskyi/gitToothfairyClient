/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - ServerFileModule.as.
 */
package services.serverfilemodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.ServerFileModule_OrthancPatientData;
import valueObjects.ServerFileModule_Protocol;
import valueObjects.ServerFileModule_ServerFile;
import valueObjects.ServerFileModule_TagValue;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_ServerFileModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_ServerFileModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.ServerFileModule_ServerFile._initRemoteClassAlias();
        valueObjects.ServerFileModule_Protocol._initRemoteClassAlias();
        valueObjects.ServerFileModule_TagValue._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addServerFile");
         operation.resultType = Boolean;
        operations["addServerFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateServerFile");
         operation.resultType = Boolean;
        operations["updateServerFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "renameServerFile");
         operation.resultType = Boolean;
        operations["renameServerFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteServerFile");
         operation.resultType = Boolean;
        operations["deleteServerFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getServerFile");
         operation.resultType = valueObjects.ServerFileModule_ServerFile;
        operations["getServerFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getServerFiles");
         operation.resultElementType = valueObjects.ServerFileModule_ServerFile;
        operations["getServerFiles"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProtocols");
         operation.resultElementType = valueObjects.ServerFileModule_Protocol;
        operations["getProtocols"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProtocolById");
         operation.resultType = valueObjects.ServerFileModule_Protocol;
        operations["getProtocolById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTagValues");
         operation.resultElementType = valueObjects.ServerFileModule_TagValue;
        operations["getTagValues"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateServerFileTagValues");
         operation.resultType = Boolean;
        operations["updateServerFileTagValues"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getServerFileContent");
         operation.resultType = Object;
        operations["getServerFileContent"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "putServerFileContent");
         operation.resultType = Object;
        operations["putServerFileContent"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getOrthancPatientData");
         operation.resultType = valueObjects.ServerFileModule_OrthancPatientData;
        operations["getOrthancPatientData"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "ServerFileModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "ServerFileModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.ServerFileModule_ServerFile, p2:valueObjects.ServerFileModule_TagValue, p3:valueObjects.ServerFileModule_Protocol) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addServerFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addServerFile(sf:valueObjects.ServerFileModule_ServerFile) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addServerFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(sf) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateServerFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateServerFile(sf:valueObjects.ServerFileModule_ServerFile) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateServerFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(sf) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'renameServerFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function renameServerFile(sfId:String, sfName:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("renameServerFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(sfId,sfName) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteServerFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteServerFile(sfId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteServerFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(sfId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getServerFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getServerFile(whereValue:String, whereField:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getServerFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(whereValue,whereField) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getServerFiles' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getServerFiles(patientId:String, searchText:String, tagId:String, byVals:Boolean, tooth:String, protocolid:String, dateFilter:String, typeFilter:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getServerFiles");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId,searchText,tagId,byVals,tooth,protocolid,dateFilter,typeFilter) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProtocols' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProtocols(searchText:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProtocols");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(searchText) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProtocolById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProtocolById(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProtocolById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTagValues' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTagValues(fileId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTagValues");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(fileId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateServerFileTagValues' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateServerFileTagValues(sfVals:ArrayCollection, idsToDelete:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateServerFileTagValues");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(sfVals,idsToDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getServerFileContent' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getServerFileContent(serverName:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getServerFileContent");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(serverName) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'putServerFileContent' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function putServerFileContent(serverName:String, data:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("putServerFileContent");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(serverName,data) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getOrthancPatientData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getOrthancPatientData(patientId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getOrthancPatientData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId) ;
        return _internal_token;
    }
     
}

}
