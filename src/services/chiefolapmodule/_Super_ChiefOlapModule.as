/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - ChiefOlapModule.as.
 */
package services.chiefolapmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.ChiefOlapModuleCongestionRecord;
import valueObjects.ChiefOlapModuleHealthproc;
import valueObjects.ChiefOlapModuleRecord;
import valueObjects.ChiefOlapModuleRoom;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_ChiefOlapModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_ChiefOlapModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.ChiefOlapModuleRecord._initRemoteClassAlias();
        valueObjects.ChiefOlapModuleRoom._initRemoteClassAlias();
        valueObjects.ChiefOlapModuleHealthproc._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getOLAP");
         operation.resultElementType = valueObjects.ChiefOlapModuleRecord;
        operations["getOLAP"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getCabinetCongestion");
         operation.resultElementType = valueObjects.ChiefOlapModuleRoom;
        operations["getCabinetCongestion"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "searchHealthproc");
         operation.resultElementType = valueObjects.ChiefOlapModuleHealthproc;
        operations["searchHealthproc"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getCabinetCongestionRecords");
         operation.resultElementType = valueObjects.ChiefOlapModuleRoom;
        operations["getCabinetCongestionRecords"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printOLAP");
         operation.resultType = String;
        operations["printOLAP"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "classVarString");
         operation.resultType = Object;
        operations["classVarString"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "valuesString");
         operation.resultType = Object;
        operations["valuesString"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "ChiefOlapModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "ChiefOlapModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.ChiefOlapModuleRecord, p2:valueObjects.ChiefOlapModuleRoom, p3:valueObjects.ChiefOlapModuleCongestionRecord, p4:valueObjects.ChiefOlapModuleHealthproc) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getOLAP' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getOLAP(rangeField:String, startDate:String, endDate:String, startBirthDay:String, endBirthDay:String, healthprocId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getOLAP");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(rangeField,startDate,endDate,startBirthDay,endBirthDay,healthprocId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getCabinetCongestion' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getCabinetCongestion(startDate:String, endDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getCabinetCongestion");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'searchHealthproc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function searchHealthproc(search:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("searchHealthproc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getCabinetCongestionRecords' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getCabinetCongestionRecords(startDate:String, endDate:String, roomId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getCabinetCongestionRecords");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,roomId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printOLAP' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printOLAP(fields:ArrayCollection, fieldNames:ArrayCollection, olapData:ArrayCollection, docType:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printOLAP");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(fields,fieldNames,olapData,docType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'classVarString' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function classVarString(className:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("classVarString");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(className) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'valuesString' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function valuesString(className:Object, object:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("valuesString");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(className,object) ;
        return _internal_token;
    }
     
}

}
