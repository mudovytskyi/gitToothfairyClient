/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - PatientHospitalCardModule.as.
 */
package services.patienthospitalcardmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.HospitalCardDataBrief;
import valueObjects.HospitalCardDataFull;
import valueObjects.HospitalCardDiagnosis;
import valueObjects.HospitalCardDisabilityCertificate;
import valueObjects.HospitalCardProcedure;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_PatientHospitalCardModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_PatientHospitalCardModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.HospitalCardDataBrief._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createUpdateHospitalCard");
         operation.resultType = int;
        operations["createUpdateHospitalCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteHospitalCard");
         operation.resultType = Boolean;
        operations["deleteHospitalCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientHospitalCards");
         operation.resultElementType = valueObjects.HospitalCardDataBrief;
        operations["getPatientHospitalCards"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientHospitalCardByID");
         operation.resultType = valueObjects.HospitalCardDataFull;
        operations["getPatientHospitalCardByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPrefilledHospitalCardData");
         operation.resultType = valueObjects.HospitalCardDataFull;
        operations["getPrefilledHospitalCardData"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "PatientHospitalCardModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "PatientHospitalCardModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.HospitalCardDataBrief, p2:valueObjects.HospitalCardDataFull, p3:valueObjects.HospitalCardProcedure, p4:valueObjects.HospitalCardDiagnosis, p5:valueObjects.HospitalCardDisabilityCertificate) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createUpdateHospitalCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createUpdateHospitalCard(item:valueObjects.HospitalCardDataFull) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createUpdateHospitalCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteHospitalCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteHospitalCard(ids:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteHospitalCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ids) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientHospitalCards' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientHospitalCards(patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientHospitalCards");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientHospitalCardByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientHospitalCardByID(patientID:String, id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientHospitalCardByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID,id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPrefilledHospitalCardData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPrefilledHospitalCardData(patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPrefilledHospitalCardData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID) ;
        return _internal_token;
    }
     
}

}
