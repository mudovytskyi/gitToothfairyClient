/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - AWDoctorModule.as.
 */
package services.awdoctormodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.AWDDoctorModuleAssistant;
import valueObjects.AWDDoctorModuleRoom;
import valueObjects.AWDDoctorModuleTableTask;
import valueObjects.AWDDoctorModuleTaskPatient;
import valueObjects.AWDDoctorModuleTasksTimes;
import valueObjects.AWDDoctorModuleWPalce;
import valueObjects.AWDDoctorModule_mobilephone;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_AWDoctorModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_AWDoctorModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.AWDDoctorModuleTaskPatient._initRemoteClassAlias();
        valueObjects.AWDDoctorModuleTasksTimes._initRemoteClassAlias();
        valueObjects.AWDDoctorModuleRoom._initRemoteClassAlias();
        valueObjects.AWDDoctorModuleAssistant._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsFiltered");
         operation.resultElementType = valueObjects.AWDDoctorModuleTaskPatient;
        operations["getPatientsFiltered"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorsPatientsFiltered");
         operation.resultElementType = valueObjects.AWDDoctorModuleTaskPatient;
        operations["getDoctorsPatientsFiltered"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAllPatientsFiltered");
         operation.resultElementType = valueObjects.AWDDoctorModuleTaskPatient;
        operations["getAllPatientsFiltered"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorTasks");
         operation.resultElementType = valueObjects.AWDDoctorModuleTasksTimes;
        operations["getDoctorTasks"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addTableTask");
         operation.resultType = Boolean;
        operations["addTableTask"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getRooms");
         operation.resultElementType = valueObjects.AWDDoctorModuleRoom;
        operations["getRooms"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAssistantsList");
         operation.resultElementType = valueObjects.AWDDoctorModuleAssistant;
        operations["getAssistantsList"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorsPatientsCount");
         operation.resultType = int;
        operations["getDoctorsPatientsCount"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "AWDoctorModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "AWDoctorModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.AWDDoctorModuleTaskPatient, p2:valueObjects.AWDDoctorModuleTasksTimes, p3:valueObjects.AWDDoctorModuleTableTask, p4:valueObjects.AWDDoctorModuleRoom, p5:valueObjects.AWDDoctorModuleWPalce, p6:valueObjects.AWDDoctorModuleAssistant, p7:valueObjects.AWDDoctorModule_mobilephone) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsFiltered' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsFiltered(lastname:String, doctorID:String, selecteddate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsFiltered");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(lastname,doctorID,selecteddate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorsPatientsFiltered' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorsPatientsFiltered(lastname:String, doctorID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorsPatientsFiltered");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(lastname,doctorID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAllPatientsFiltered' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAllPatientsFiltered(lastname:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAllPatientsFiltered");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(lastname) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorTasks' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorTasks(doctorId:String, selectedDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorTasks");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorId,selectedDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addTableTask' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addTableTask(task:valueObjects.AWDDoctorModuleTableTask) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addTableTask");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(task) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getRooms' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getRooms() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getRooms");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAssistantsList' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAssistantsList() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAssistantsList");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorsPatientsCount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorsPatientsCount(doctorID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorsPatientsCount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorID) ;
        return _internal_token;
    }
     
}

}
