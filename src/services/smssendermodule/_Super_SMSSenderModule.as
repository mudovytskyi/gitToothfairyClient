/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - SMSSenderModule.as.
 */
package services.smssendermodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.SMSSenderModuleFilter;
import valueObjects.SMSSenderModuleMessage;
import valueObjects.SMSSenderModulePatient;
import valueObjects.SMSSenderSetting;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_SMSSenderModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_SMSSenderModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.SMSSenderModuleMessage._initRemoteClassAlias();
        valueObjects.SMSSenderModulePatient._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerType");
         operation.resultType = Object;
        operations["registerType"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "sendSMS");
         operation.resultType = int;
        operations["sendSMS"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSMSSettings");
         operation.resultType = valueObjects.SMSSenderSetting;
        operations["getSMSSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSMSSettings");
         operation.resultType = String;
        operations["saveSMSSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "checkBalance");
         operation.resultType = String;
        operations["checkBalance"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSMSStatusByTaskId");
         operation.resultType = int;
        operations["getSMSStatusByTaskId"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSMSMessages");
         operation.resultElementType = valueObjects.SMSSenderModuleMessage;
        operations["getSMSMessages"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "sendMassMessage");
         operation.resultType = int;
        operations["sendMassMessage"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "prepareMassMailing");
         operation.resultElementType = Object;
        operations["prepareMassMailing"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSMSServiceStatus");
         operation.resultType = Boolean;
        operations["getSMSServiceStatus"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setSMSServiceStatus");
         operation.resultType = Object;
        operations["setSMSServiceStatus"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteSMS");
         operation.resultType = Object;
        operations["deleteSMS"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "clearTrunSMS");
         operation.resultType = Boolean;
        operations["clearTrunSMS"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "resendSMS");
         operation.resultType = Object;
        operations["resendSMS"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "changePhoneNumber");
         operation.resultType = Boolean;
        operations["changePhoneNumber"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatients");
         operation.resultElementType = valueObjects.SMSSenderModulePatient;
        operations["getPatients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "translit");
         operation.resultType = Object;
        operations["translit"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "sendSMSGate");
         operation.resultType = Object;
        operations["sendSMSGate"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "SMSSenderModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "SMSSenderModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerType' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerType(p1:valueObjects.SMSSenderSetting, p2:valueObjects.SMSSenderModuleMessage, p3:valueObjects.SMSSenderModulePatient, p4:valueObjects.SMSSenderModuleFilter) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerType");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'sendSMS' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function sendSMS(phone:String, message:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("sendSMS");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(phone,message) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSMSSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSMSSettings() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSMSSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSMSSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSMSSettings(settings:valueObjects.SMSSenderSetting) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSMSSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(settings) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'checkBalance' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function checkBalance(login:String, password:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("checkBalance");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(login,password) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSMSStatusByTaskId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSMSStatusByTaskId(taskid:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSMSStatusByTaskId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(taskid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSMSMessages' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSMSMessages(fromDate:String, toDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSMSMessages");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(fromDate,toDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'sendMassMessage' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function sendMassMessage(template:String, filters:ArrayCollection, lastdate:int, healthtype:int, ageMin:int, ageMax:int, summMin:String, summMax:String, docIds:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("sendMassMessage");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(template,filters,lastdate,healthtype,ageMin,ageMax,summMin,summMax,docIds) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'prepareMassMailing' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function prepareMassMailing(filters:ArrayCollection, lastdate:int, healthtype:int, ageMin:int, ageMax:int, summMin:String, summMax:String, docIds:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("prepareMassMailing");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(filters,lastdate,healthtype,ageMin,ageMax,summMin,summMax,docIds) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSMSServiceStatus' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSMSServiceStatus() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSMSServiceStatus");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setSMSServiceStatus' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setSMSServiceStatus(status:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setSMSServiceStatus");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(status) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteSMS' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteSMS(IDs:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteSMS");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(IDs) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'clearTrunSMS' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function clearTrunSMS() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("clearTrunSMS");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'resendSMS' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function resendSMS(IDs:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("resendSMS");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(IDs) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'changePhoneNumber' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function changePhoneNumber(id:int, phone:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("changePhoneNumber");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id,phone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatients(search:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'translit' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function translit(string:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("translit");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(string) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'sendSMSGate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function sendSMSGate(login:String, pass:Object, gateIp:Object, gatePort:Object, gateAdditional:Object, sms:Object, trans:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("sendSMSGate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(login,pass,gateIp,gatePort,gateAdditional,sms,trans) ;
        return _internal_token;
    }
     
}

}
