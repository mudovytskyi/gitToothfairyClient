/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - TreatmentManagerModule.as.
 */
package services.treatmentmanagermodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.TreatmentManagerModuleDoctor;
import valueObjects.TreatmentManagerModuleF43;
import valueObjects.TreatmentManagerModuleProtocol;
import valueObjects.TreatmentManagerModuleTemplate;
import valueObjects.TreatmentManagerModuleTemplateProcedurFarm;
import valueObjects.TreatmentManagerModuleTemplateProcedure;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_TreatmentManagerModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_TreatmentManagerModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.TreatmentManagerModuleF43._initRemoteClassAlias();
        valueObjects.TreatmentManagerModuleProtocol._initRemoteClassAlias();
        valueObjects.TreatmentManagerModuleTemplate._initRemoteClassAlias();
        valueObjects.TreatmentManagerModuleTemplateProcedure._initRemoteClassAlias();
        valueObjects.TreatmentManagerModuleDoctor._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getF43");
         operation.resultElementType = valueObjects.TreatmentManagerModuleF43;
        operations["getF43"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addF43toProtocol");
         operation.resultElementType = valueObjects.TreatmentManagerModuleF43;
        operations["addF43toProtocol"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "removeF43fromProtocol");
         operation.resultElementType = valueObjects.TreatmentManagerModuleF43;
        operations["removeF43fromProtocol"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProtocols");
         operation.resultElementType = valueObjects.TreatmentManagerModuleProtocol;
        operations["getProtocols"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveProtocol");
         operation.resultType = String;
        operations["saveProtocol"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteProtocol");
         operation.resultType = Boolean;
        operations["deleteProtocol"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveProtocolF43");
         operation.resultType = int;
        operations["saveProtocolF43"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveProtocolType");
         operation.resultType = int;
        operations["saveProtocolType"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveProtocolTextFields");
         operation.resultType = Boolean;
        operations["saveProtocolTextFields"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProtocolTemplates");
         operation.resultElementType = valueObjects.TreatmentManagerModuleTemplate;
        operations["getProtocolTemplates"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteTemplate");
         operation.resultType = Boolean;
        operations["deleteTemplate"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveTemplate");
         operation.resultType = valueObjects.TreatmentManagerModuleTemplate;
        operations["saveTemplate"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "savePastedTemplates");
         operation.resultElementType = valueObjects.TreatmentManagerModuleTemplate;
        operations["savePastedTemplates"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTemplateProcedures");
         operation.resultElementType = valueObjects.TreatmentManagerModuleTemplateProcedure;
        operations["getTemplateProcedures"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.TreatmentManagerModuleDoctor;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDictionaryProcedures");
         operation.resultElementType = valueObjects.TreatmentManagerModuleTemplateProcedure;
        operations["getDictionaryProcedures"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printTemplateAsPlan");
         operation.resultType = String;
        operations["printTemplateAsPlan"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteTemplateAsPlan");
         operation.resultType = Boolean;
        operations["deleteTemplateAsPlan"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTemplate");
         operation.resultType = valueObjects.TreatmentManagerModuleTemplate;
        operations["getTemplate"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "TreatmentManagerModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "TreatmentManagerModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.TreatmentManagerModuleProtocol, p2:valueObjects.TreatmentManagerModuleTemplate, p3:valueObjects.TreatmentManagerModuleTemplateProcedure, p4:valueObjects.TreatmentManagerModuleTemplateProcedurFarm, p5:valueObjects.TreatmentManagerModuleDoctor, p6:valueObjects.TreatmentManagerModuleF43) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getF43' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getF43(protocolId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getF43");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(protocolId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addF43toProtocol' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addF43toProtocol(itemF43:valueObjects.TreatmentManagerModuleF43, protocolId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addF43toProtocol");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(itemF43,protocolId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'removeF43fromProtocol' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function removeF43fromProtocol(itemF43:valueObjects.TreatmentManagerModuleF43, protocolId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("removeF43fromProtocol");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(itemF43,protocolId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProtocols' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProtocols(mode:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProtocols");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(mode) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveProtocol' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveProtocol(protocol:valueObjects.TreatmentManagerModuleProtocol) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveProtocol");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(protocol) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteProtocol' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteProtocol(protocolID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteProtocol");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(protocolID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveProtocolF43' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveProtocolF43(diagnos:int, protocoID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveProtocolF43");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnos,protocoID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveProtocolType' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveProtocolType(healthtype:int, protocoID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveProtocolType");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(healthtype,protocoID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveProtocolTextFields' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveProtocolTextFields(sqlFieldLabel:String, text:String, protocoID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveProtocolTextFields");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(sqlFieldLabel,text,protocoID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProtocolTemplates' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProtocolTemplates(protocolID:String, doctorID:String, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProtocolTemplates");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(protocolID,doctorID,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteTemplate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteTemplate(templatelID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteTemplate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(templatelID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveTemplate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveTemplate(template:valueObjects.TreatmentManagerModuleTemplate) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveTemplate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(template) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'savePastedTemplates' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function savePastedTemplates(pastedtemplates:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("savePastedTemplates");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(pastedtemplates) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTemplateProcedures' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTemplateProcedures(templateID:String, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTemplateProcedures");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(templateID,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDictionaryProcedures' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDictionaryProcedures(healthType:Number, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDictionaryProcedures");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(healthType,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printTemplateAsPlan' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printTemplateAsPlan(docType:String, templateId:String, patientId:String, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printTemplateAsPlan");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,templateId,patientId,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteTemplateAsPlan' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteTemplateAsPlan(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteTemplateAsPlan");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTemplate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTemplate(templateId:String, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTemplate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(templateId,priceIndex) ;
        return _internal_token;
    }
     
}

}
