/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - StorageWriteOff.as.
 */
package services.storagewriteoff
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.StorageWriteOff_FarmIDs;
import valueObjects.StorageWriteOff_OLAPData;
import valueObjects.Storage_FarmBath;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_StorageWriteOff extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_StorageWriteOff()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.StorageWriteOff_OLAPData._initRemoteClassAlias();
        valueObjects.Storage_FarmBath._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getOLAPData");
         operation.resultElementType = valueObjects.StorageWriteOff_OLAPData;
        operations["getOLAPData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmsToWriteOff");
         operation.resultElementType = valueObjects.Storage_FarmBath;
        operations["getFarmsToWriteOff"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setProceduresfarmsDocFK");
         operation.resultType = Object;
        operations["setProceduresfarmsDocFK"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "StorageWriteOff";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "StorageWriteOff";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.StorageWriteOff_OLAPData, p2:valueObjects.StorageWriteOff_FarmIDs) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getOLAPData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getOLAPData(startDate:String, endDate:String, doctorID:String, cabinetID:String, subdivID:String, searchText:String, storageID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getOLAPData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,doctorID,cabinetID,subdivID,searchText,storageID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmsToWriteOff' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmsToWriteOff(farms:ArrayCollection, subd_id:String, storage_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmsToWriteOff");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farms,subd_id,storage_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setProceduresfarmsDocFK' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setProceduresfarmsDocFK(procfarms:ArrayCollection, farmDocID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setProceduresfarmsDocFK");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(procfarms,farmDocID) ;
        return _internal_token;
    }
     
}

}
