/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - OrthancService.as.
 */
package services.orthancservice
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_OrthancService extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_OrthancService()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "getPatientInstances");
         operation.resultType = Object;
        operations["getPatientInstances"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsList");
         operation.resultType = Object;
        operations["getPatientsList"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "savePatientInstanceImagesToFileManagerDirectory");
         operation.resultType = Boolean;
        operations["savePatientInstanceImagesToFileManagerDirectory"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "linkPatientWithOrthanc");
         operation.resultType = Boolean;
        operations["linkPatientWithOrthanc"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "OrthancService";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "OrthancService";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'getPatientInstances' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientInstances(dicomServerPatientId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientInstances");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(dicomServerPatientId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsList' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsList() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsList");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'savePatientInstanceImagesToFileManagerDirectory' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function savePatientInstanceImagesToFileManagerDirectory(patientId:String, dicomServerPatientIds:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("savePatientInstanceImagesToFileManagerDirectory");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId,dicomServerPatientIds) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'linkPatientWithOrthanc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function linkPatientWithOrthanc(patientId:String, orthancPatientId:String, orthancPatientName:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("linkPatientWithOrthanc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId,orthancPatientId,orthancPatientName) ;
        return _internal_token;
    }
     
}

}
