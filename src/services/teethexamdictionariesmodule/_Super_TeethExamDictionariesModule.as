/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - TeethExamDictionariesModule.as.
 */
package services.teethexamdictionariesmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.TeethExamDictionariesModuleDictionaryObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_TeethExamDictionariesModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_TeethExamDictionariesModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.TeethExamDictionariesModuleDictionaryObject._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTeethExamDictionary");
         operation.resultElementType = valueObjects.TeethExamDictionariesModuleDictionaryObject;
        operations["getTeethExamDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addTeethExamDictionary");
         operation.resultType = Boolean;
        operations["addTeethExamDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTeethExamDictionary");
         operation.resultType = Boolean;
        operations["updateTeethExamDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteTeethExamDictionary");
         operation.resultType = Boolean;
        operations["deleteTeethExamDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTeethExamDictionaryNumber");
         operation.resultType = Boolean;
        operations["updateTeethExamDictionaryNumber"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "TeethExamDictionariesModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "TeethExamDictionariesModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.TeethExamDictionariesModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTeethExamDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTeethExamDictionary(type:int, specialities:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTeethExamDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(type,specialities) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addTeethExamDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addTeethExamDictionary(ovo:valueObjects.TeethExamDictionariesModuleDictionaryObject, type:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addTeethExamDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo,type) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTeethExamDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTeethExamDictionary(ovo:valueObjects.TeethExamDictionariesModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTeethExamDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteTeethExamDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteTeethExamDictionary(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteTeethExamDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTeethExamDictionaryNumber' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTeethExamDictionaryNumber(ovo:valueObjects.TeethExamDictionariesModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTeethExamDictionaryNumber");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo) ;
        return _internal_token;
    }
     
}

}
