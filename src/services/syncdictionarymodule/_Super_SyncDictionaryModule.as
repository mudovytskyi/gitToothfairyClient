/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - SyncDictionaryModule.as.
 */
package services.syncdictionarymodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.SQLite3;
import valueObjects.SyncDictionaryModuleSumm;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_SyncDictionaryModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_SyncDictionaryModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "syncDictionaries");
         operation.resultType = Object;
        operations["syncDictionaries"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "syncDictionary");
         operation.resultType = Object;
        operations["syncDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFirebirdDictionary");
         operation.resultElementType = Object;
        operations["getFirebirdDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSQLiteSumm");
         operation.resultType = int;
        operations["getSQLiteSumm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSQLiteIdString");
         operation.resultType = String;
        operations["getSQLiteIdString"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "clearSQLiteDictionary");
         operation.resultType = Object;
        operations["clearSQLiteDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setSQLiteDictionary");
         operation.resultType = Object;
        operations["setSQLiteDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFirebirdSumm");
         operation.resultType = int;
        operations["getFirebirdSumm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFirebirdIdString");
         operation.resultType = String;
        operations["getFirebirdIdString"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "classVarString");
         operation.resultType = Object;
        operations["classVarString"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "valuesString");
         operation.resultType = Object;
        operations["valuesString"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "SyncDictionaryModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "SyncDictionaryModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.SyncDictionaryModuleSumm) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'syncDictionaries' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function syncDictionaries(clientSumms:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("syncDictionaries");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(clientSumms) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'syncDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function syncDictionary(clientSumm:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("syncDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(clientSumm) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFirebirdDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFirebirdDictionary(className:String, sqliteIdStr:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFirebirdDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(className,sqliteIdStr) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSQLiteSumm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSQLiteSumm(db:valueObjects.SQLite3) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSQLiteSumm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(db) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSQLiteIdString' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSQLiteIdString(db:valueObjects.SQLite3) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSQLiteIdString");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(db) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'clearSQLiteDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function clearSQLiteDictionary(db:valueObjects.SQLite3, firebirdIdStr:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("clearSQLiteDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(db,firebirdIdStr) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setSQLiteDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setSQLiteDictionary(db:valueObjects.SQLite3, className:String, res:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setSQLiteDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(db,className,res) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFirebirdSumm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFirebirdSumm(className:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFirebirdSumm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(className) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFirebirdIdString' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFirebirdIdString(className:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFirebirdIdString");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(className) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'classVarString' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function classVarString(className:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("classVarString");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(className) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'valuesString' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function valuesString(className:Object, object:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("valuesString");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(className,object) ;
        return _internal_token;
    }
     
}

}
