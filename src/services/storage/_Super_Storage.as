/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - Storage.as.
 */
package services.storage
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.Storage_Farm;
import valueObjects.Storage_FarmBath;
import valueObjects.Storage_FarmDoc;
import valueObjects.Storage_FarmRemains;
import valueObjects.Storage_FarmStorage;
import valueObjects.Storage_Patient;
import valueObjects.Storage_Supplier;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_Storage extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_Storage()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.Storage_FarmDoc._initRemoteClassAlias();
        valueObjects.Storage_Supplier._initRemoteClassAlias();
        valueObjects.Storage_Farm._initRemoteClassAlias();
        valueObjects.Storage_FarmRemains._initRemoteClassAlias();
        valueObjects.Storage_Patient._initRemoteClassAlias();
        valueObjects.Storage_FarmBath._initRemoteClassAlias();
        valueObjects.Storage_FarmStorage._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveFarmDoc");
         operation.resultType = valueObjects.Storage_FarmDoc;
        operations["saveFarmDoc"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmDocs");
         operation.resultElementType = valueObjects.Storage_FarmDoc;
        operations["getFarmDocs"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSuppliers");
         operation.resultElementType = valueObjects.Storage_Supplier;
        operations["getSuppliers"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarms");
         operation.resultElementType = valueObjects.Storage_Farm;
        operations["getFarms"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmsRemains");
         operation.resultElementType = valueObjects.Storage_FarmRemains;
        operations["getFarmsRemains"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientLazyList");
         operation.resultElementType = valueObjects.Storage_Patient;
        operations["getPatientLazyList"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "removeFarmDoc");
         operation.resultType = Boolean;
        operations["removeFarmDoc"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmDocBaths");
         operation.resultElementType = valueObjects.Storage_FarmBath;
        operations["getFarmDocBaths"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmStorages");
         operation.resultElementType = valueObjects.Storage_FarmStorage;
        operations["getFarmStorages"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveFarmStorages");
         operation.resultElementType = valueObjects.Storage_FarmStorage;
        operations["saveFarmStorages"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteFarmStoragesByID");
         operation.resultType = int;
        operations["deleteFarmStoragesByID"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "Storage";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "Storage";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.Storage_FarmDoc, p2:valueObjects.Storage_FarmBath, p3:valueObjects.Storage_Supplier, p4:valueObjects.Storage_Farm, p5:valueObjects.Storage_FarmRemains, p6:valueObjects.Storage_Patient, p7:valueObjects.Storage_FarmStorage) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveFarmDoc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveFarmDoc(farmDoc:valueObjects.Storage_FarmDoc, isAccountflowDoc:Boolean, isCashpayment:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveFarmDoc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farmDoc,isAccountflowDoc,isCashpayment) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmDocs' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmDocs(docType:String, startDate:String, endDate:String, supplierId:String, farmstorageId:String, searchText:String, subdivId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmDocs");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,startDate,endDate,supplierId,farmstorageId,searchText,subdivId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSuppliers' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSuppliers() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSuppliers");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarms' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarms(search:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarms");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmsRemains' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmsRemains(search:String, farmsInDoc:ArrayCollection, isSpacer:Boolean, isLowerOptimal:Boolean, isLowerMinimum:Boolean, subd_id:String, storage_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmsRemains");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search,farmsInDoc,isSpacer,isLowerOptimal,isLowerMinimum,subd_id,storage_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientLazyList' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientLazyList(fio:String, includePhoneSearch:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientLazyList");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(fio,includePhoneSearch) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'removeFarmDoc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function removeFarmDoc(farmDoc:valueObjects.Storage_FarmDoc) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("removeFarmDoc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farmDoc) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmDocBaths' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmDocBaths(docId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmDocBaths");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmStorages' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmStorages(subdivId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmStorages");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveFarmStorages' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveFarmStorages(subdivId:String, storage:valueObjects.Storage_FarmStorage) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveFarmStorages");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivId,storage) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteFarmStoragesByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFarmStoragesByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFarmStoragesByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
}

}
