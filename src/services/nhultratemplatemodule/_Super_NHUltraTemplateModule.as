/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - NHUltraTemplateModule.as.
 */
package services.nhultratemplatemodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.NHUltraTemplateModule_ExamQuestion;
import valueObjects.NHUltraTemplateModule_ExamQuestionPrintData;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_NHUltraTemplateModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_NHUltraTemplateModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.NHUltraTemplateModule_ExamQuestion._initRemoteClassAlias();
        valueObjects.NHUltraTemplateModule_ExamQuestionPrintData._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExamQuestions");
         operation.resultElementType = valueObjects.NHUltraTemplateModule_ExamQuestion;
        operations["getExamQuestions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExamTemplateQuestions");
         operation.resultElementType = valueObjects.NHUltraTemplateModule_ExamQuestion;
        operations["getExamTemplateQuestions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setExamTemplateQuestions");
         operation.resultType = Boolean;
        operations["setExamTemplateQuestions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExamQuestionsPrintData");
         operation.resultElementType = valueObjects.NHUltraTemplateModule_ExamQuestionPrintData;
        operations["getExamQuestionsPrintData"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "NHUltraTemplateModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "NHUltraTemplateModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.NHUltraTemplateModule_ExamQuestion, p2:valueObjects.NHUltraTemplateModule_ExamQuestionPrintData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExamQuestions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExamQuestions(isTest:Boolean, id_patient:String, id_exam:String, id_template:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExamQuestions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTest,id_patient,id_exam,id_template) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExamTemplateQuestions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExamTemplateQuestions(id_template:String, onlyActive:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExamTemplateQuestions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id_template,onlyActive) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setExamTemplateQuestions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setExamTemplateQuestions(id_template:String, questions:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setExamTemplateQuestions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id_template,questions) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExamQuestionsPrintData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExamQuestionsPrintData(exam_results_blob:String, doc_type:Object, templateOnly:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExamQuestionsPrintData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(exam_results_blob,doc_type,templateOnly) ;
        return _internal_token;
    }
     
}

}
