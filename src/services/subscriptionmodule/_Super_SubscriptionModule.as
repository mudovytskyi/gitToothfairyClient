/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - SubscriptionModule.as.
 */
package services.subscriptionmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.SubscriptionCardObject;
import valueObjects.SubscriptionCardVisitsData;
import valueObjects.SubscriptionDictionaryObject;
import valueObjects.SubscriptionDictionaryRegistratorRecord;
import valueObjects.SubscriptionPatientObject;
import valueObjects.SubscriptionProcedureObject;
import valueObjects.SubscriptionTypeObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_SubscriptionModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_SubscriptionModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.SubscriptionDictionaryObject._initRemoteClassAlias();
        valueObjects.SubscriptionCardObject._initRemoteClassAlias();
        valueObjects.SubscriptionDictionaryRegistratorRecord._initRemoteClassAlias();
        valueObjects.SubscriptionCardVisitsData._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubscriptionTypes");
         operation.resultElementType = Object;
        operations["getSubscriptionTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubscriptionByID");
         operation.resultType = valueObjects.SubscriptionDictionaryObject;
        operations["getSubscriptionByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubscriptionDictionaries");
         operation.resultElementType = valueObjects.SubscriptionDictionaryObject;
        operations["getSubscriptionDictionaries"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSubscriptionDictionary");
         operation.resultType = String;
        operations["saveSubscriptionDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteProcedureFromSubscriptionDictionary");
         operation.resultType = Boolean;
        operations["deleteProcedureFromSubscriptionDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteSubscriptionDictionary");
         operation.resultType = Boolean;
        operations["deleteSubscriptionDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientSubscriptionCards");
         operation.resultElementType = valueObjects.SubscriptionCardObject;
        operations["getPatientSubscriptionCards"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addPatientSubscriptionCard");
         operation.resultType = String;
        operations["addPatientSubscriptionCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePatientSubscriptionCard");
         operation.resultType = Boolean;
        operations["deletePatientSubscriptionCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "usePatientSubscriptionCard");
         operation.resultType = Boolean;
        operations["usePatientSubscriptionCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getRecordsForRegistrator");
         operation.resultElementType = valueObjects.SubscriptionDictionaryRegistratorRecord;
        operations["getRecordsForRegistrator"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubscriptionVisitsData");
         operation.resultElementType = valueObjects.SubscriptionCardVisitsData;
        operations["getSubscriptionVisitsData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createOrderReport");
         operation.resultType = String;
        operations["createOrderReport"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteFile");
         operation.resultType = Object;
        operations["deleteFile"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "SubscriptionModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "SubscriptionModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.SubscriptionTypeObject, p2:valueObjects.SubscriptionDictionaryObject, p3:valueObjects.SubscriptionCardObject, p4:valueObjects.SubscriptionProcedureObject, p5:valueObjects.SubscriptionPatientObject, p6:valueObjects.SubscriptionCardVisitsData, p7:valueObjects.SubscriptionDictionaryRegistratorRecord) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubscriptionTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubscriptionTypes() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubscriptionTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubscriptionByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubscriptionByID(subID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubscriptionByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubscriptionDictionaries' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubscriptionDictionaries() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubscriptionDictionaries");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSubscriptionDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSubscriptionDictionary(ovo:valueObjects.SubscriptionDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSubscriptionDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteProcedureFromSubscriptionDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteProcedureFromSubscriptionDictionary(ids:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteProcedureFromSubscriptionDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ids) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteSubscriptionDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteSubscriptionDictionary(ids:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteSubscriptionDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ids) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientSubscriptionCards' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientSubscriptionCards(patientID:String, valid:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientSubscriptionCards");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID,valid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addPatientSubscriptionCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addPatientSubscriptionCard(scvo:valueObjects.SubscriptionCardObject, clientLocale:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addPatientSubscriptionCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(scvo,clientLocale) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePatientSubscriptionCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePatientSubscriptionCard(ids:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePatientSubscriptionCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ids) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'usePatientSubscriptionCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function usePatientSubscriptionCard(trProcIDs:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("usePatientSubscriptionCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(trProcIDs) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getRecordsForRegistrator' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getRecordsForRegistrator() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getRecordsForRegistrator");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubscriptionVisitsData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubscriptionVisitsData(patient_id:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubscriptionVisitsData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createOrderReport' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createOrderReport(subCardID:String, docType:String, payType:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createOrderReport");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subCardID,docType,payType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFile(file_name:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(file_name) ;
        return _internal_token;
    }
     
}

}
