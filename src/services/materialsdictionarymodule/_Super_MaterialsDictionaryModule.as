/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - MaterialsDictionaryModule.as.
 */
package services.materialsdictionarymodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.MaterialsDictionaryModuleFarm;
import valueObjects.MaterialsDictionaryModuleFarmGroup;
import valueObjects.MaterialsDictionaryModuleFarmSupplier;
import valueObjects.MaterialsDictionaryModuleMaterialCardWindowData;
import valueObjects.MaterialsDictionaryModuleSupplier;
import valueObjects.MaterialsDictionaryModuleUnitOfMeasure;
import valueObjects.MaterialsDictionaryModuleUpdateMaterialsDictionaryModuleFarmResult;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_MaterialsDictionaryModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_MaterialsDictionaryModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.MaterialsDictionaryModuleFarm._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getMaterialsDictionaryModuleData");
         operation.resultElementType = valueObjects.MaterialsDictionaryModuleFarm;
        operations["getMaterialsDictionaryModuleData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateMaterialsDictionaryModuleFarm");
         operation.resultType = valueObjects.MaterialsDictionaryModuleUpdateMaterialsDictionaryModuleFarmResult;
        operations["updateMaterialsDictionaryModuleFarm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteMaterialsDictionaryModuleRecords");
         operation.resultType = Boolean;
        operations["deleteMaterialsDictionaryModuleRecords"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getMaterialCardWindowData");
         operation.resultType = valueObjects.MaterialsDictionaryModuleMaterialCardWindowData;
        operations["getMaterialCardWindowData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateMaterialsDictionaryModuleFarmGroup");
         operation.resultType = valueObjects.MaterialsDictionaryModuleFarmGroup;
        operations["updateMaterialsDictionaryModuleFarmGroup"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateMaterialsDictionaryModuleParentId");
         operation.resultType = Boolean;
        operations["updateMaterialsDictionaryModuleParentId"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "MaterialsDictionaryModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "MaterialsDictionaryModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.MaterialsDictionaryModuleFarm, p2:valueObjects.MaterialsDictionaryModuleSupplier, p3:valueObjects.MaterialsDictionaryModuleUnitOfMeasure, p4:valueObjects.MaterialsDictionaryModuleFarmSupplier, p5:valueObjects.MaterialsDictionaryModuleMaterialCardWindowData, p6:valueObjects.MaterialsDictionaryModuleFarmGroup, p7:valueObjects.MaterialsDictionaryModuleUpdateMaterialsDictionaryModuleFarmResult) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getMaterialsDictionaryModuleData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getMaterialsDictionaryModuleData(parentID:String, searchFields:String, searchText:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getMaterialsDictionaryModuleData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(parentID,searchFields,searchText) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateMaterialsDictionaryModuleFarm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateMaterialsDictionaryModuleFarm(newMaterialsDictionaryModuleFarm:valueObjects.MaterialsDictionaryModuleFarm, unitOfMeasureArrayCollection:ArrayCollection, unitOfMeasureToDeleteArrayCollection:ArrayCollection, farmSupplierArrayCollection:ArrayCollection, farmSupplierToDeleteArrayCollection:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateMaterialsDictionaryModuleFarm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newMaterialsDictionaryModuleFarm,unitOfMeasureArrayCollection,unitOfMeasureToDeleteArrayCollection,farmSupplierArrayCollection,farmSupplierToDeleteArrayCollection) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteMaterialsDictionaryModuleRecords' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteMaterialsDictionaryModuleRecords(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteMaterialsDictionaryModuleRecords");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getMaterialCardWindowData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getMaterialCardWindowData(farmID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getMaterialCardWindowData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farmID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateMaterialsDictionaryModuleFarmGroup' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateMaterialsDictionaryModuleFarmGroup(newMaterialsDictionaryModuleFarmGroup:valueObjects.MaterialsDictionaryModuleFarmGroup) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateMaterialsDictionaryModuleFarmGroup");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newMaterialsDictionaryModuleFarmGroup) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateMaterialsDictionaryModuleParentId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateMaterialsDictionaryModuleParentId(farmId:String, newParentId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateMaterialsDictionaryModuleParentId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farmId,newParentId) ;
        return _internal_token;
    }
     
}

}
