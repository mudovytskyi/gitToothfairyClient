/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - DiagnosisDictionaryModule.as.
 */
package services.diagnosisdictionarymodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.DiagnosisDictionaryModuleDictionaryObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_DiagnosisDictionaryModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_DiagnosisDictionaryModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.DiagnosisDictionaryModuleDictionaryObject._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDiagnosis");
         operation.resultElementType = valueObjects.DiagnosisDictionaryModuleDictionaryObject;
        operations["getDiagnosis"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addDiagnosis");
         operation.resultType = Boolean;
        operations["addDiagnosis"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateDiagnosis");
         operation.resultType = Boolean;
        operations["updateDiagnosis"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteDiagnosis");
         operation.resultType = Boolean;
        operations["deleteDiagnosis"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateDiagnosisNumber");
         operation.resultType = Boolean;
        operations["updateDiagnosisNumber"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "DiagnosisDictionaryModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "DiagnosisDictionaryModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.DiagnosisDictionaryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDiagnosis' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDiagnosis() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDiagnosis");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addDiagnosis' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addDiagnosis(ovo:valueObjects.DiagnosisDictionaryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addDiagnosis");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateDiagnosis' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateDiagnosis(ovo:valueObjects.DiagnosisDictionaryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateDiagnosis");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteDiagnosis' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteDiagnosis(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteDiagnosis");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateDiagnosisNumber' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateDiagnosisNumber(ovo:valueObjects.DiagnosisDictionaryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateDiagnosisNumber");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo) ;
        return _internal_token;
    }
     
}

}
