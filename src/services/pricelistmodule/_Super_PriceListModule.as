/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - PriceListModule.as.
 */
package services.pricelistmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.PriceListModuleRecord;
import valueObjects.PriceListModuleRecordFarm;
import valueObjects.PriceListModuleRecordRegistrator;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_PriceListModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_PriceListModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.PriceListModuleRecordFarm._initRemoteClassAlias();
        valueObjects.PriceListModuleRecord._initRemoteClassAlias();
        valueObjects.PriceListModuleRecordRegistrator._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPriceListRecordsRecursive");
         operation.resultType = Object;
        operations["getPriceListRecordsRecursive"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveFarm");
         operation.resultType = valueObjects.PriceListModuleRecordFarm;
        operations["saveFarm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateFarm");
         operation.resultType = Boolean;
        operations["updateFarm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteFarm");
         operation.resultType = Boolean;
        operations["deleteFarm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmsForProcDictionary");
         operation.resultElementType = valueObjects.PriceListModuleRecordFarm;
        operations["getFarmsForProcDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPriceListRecords");
         operation.resultElementType = valueObjects.PriceListModuleRecord;
        operations["getPriceListRecords"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getParrentIdsRecoursive");
         operation.resultType = Object;
        operations["getParrentIdsRecoursive"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updatePriceListRecord");
         operation.resultType = Object;
        operations["updatePriceListRecord"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePriceListRecords");
         operation.resultType = Boolean;
        operations["deletePriceListRecords"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updatePriceListRecords2");
         operation.resultType = Boolean;
        operations["updatePriceListRecords2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateSalarySettingsInTreatmentProcs");
         operation.resultType = Boolean;
        operations["updateSalarySettingsInTreatmentProcs"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "GetRecordsForRegistrator");
         operation.resultElementType = valueObjects.PriceListModuleRecordRegistrator;
        operations["GetRecordsForRegistrator"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "PriceListModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "PriceListModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.PriceListModuleRecord, p2:valueObjects.PriceListModuleRecordRegistrator) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPriceListRecordsRecursive' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPriceListRecordsRecursive(trans:Object, parentID:Object, captionLevel:Object, whereStr:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPriceListRecordsRecursive");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(trans,parentID,captionLevel,whereStr) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveFarm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveFarm(priceListRecordFarm:valueObjects.PriceListModuleRecordFarm, healthprocID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveFarm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(priceListRecordFarm,healthprocID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateFarm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateFarm(priceListRecordFarm:valueObjects.PriceListModuleRecordFarm) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateFarm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(priceListRecordFarm) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteFarm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFarm(healthprocfarmID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFarm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(healthprocfarmID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmsForProcDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmsForProcDictionary() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmsForProcDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPriceListRecords' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPriceListRecords(parentID:String, searchFields:String, searchText:String, searchType:int, searchSpecialities:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPriceListRecords");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(parentID,searchFields,searchText,searchType,searchSpecialities) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getParrentIdsRecoursive' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getParrentIdsRecoursive(whereStr:Object, trans:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getParrentIdsRecoursive");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(whereStr,trans) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updatePriceListRecord' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updatePriceListRecord(priceListItem:valueObjects.PriceListModuleRecord) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updatePriceListRecord");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(priceListItem) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePriceListRecords' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePriceListRecords(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePriceListRecords");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updatePriceListRecords2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updatePriceListRecords2(input:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updatePriceListRecords2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(input) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateSalarySettingsInTreatmentProcs' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateSalarySettingsInTreatmentProcs(healthproc_id:String, salary_settings:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateSalarySettingsInTreatmentProcs");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(healthproc_id,salary_settings) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'GetRecordsForRegistrator' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function GetRecordsForRegistrator() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("GetRecordsForRegistrator");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
}

}
