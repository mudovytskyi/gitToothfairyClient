/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - WarehouseModule.as.
 */
package services.warehousemodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.WarehouseModuleDoctor;
import valueObjects.WarehouseModuleFarm;
import valueObjects.WarehouseModuleFarmCard;
import valueObjects.WarehouseModuleFarmDocument;
import valueObjects.WarehouseModuleFarmFlow;
import valueObjects.WarehouseModuleFarmPrice;
import valueObjects.WarehouseModuleFarmUnit;
import valueObjects.WarehouseModuleOLAPRecord;
import valueObjects.WarehouseModuleRoom;
import valueObjects.WarehouseModuleSupplier;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_WarehouseModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_WarehouseModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.WarehouseModuleFarmDocument._initRemoteClassAlias();
        valueObjects.WarehouseModuleDoctor._initRemoteClassAlias();
        valueObjects.WarehouseModuleSupplier._initRemoteClassAlias();
        valueObjects.WarehouseModuleRoom._initRemoteClassAlias();
        valueObjects.WarehouseModuleFarmFlow._initRemoteClassAlias();
        valueObjects.WarehouseModuleFarmCard._initRemoteClassAlias();
        valueObjects.WarehouseModuleFarm._initRemoteClassAlias();
        valueObjects.WarehouseModuleOLAPRecord._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setApplicationSetting");
         operation.resultType = Boolean;
        operations["setApplicationSetting"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getApplicationSettings");
         operation.resultElementType = Object;
        operations["getApplicationSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmDocuments");
         operation.resultElementType = valueObjects.WarehouseModuleFarmDocument;
        operations["getFarmDocuments"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "upsertFarmDocument");
         operation.resultType = Boolean;
        operations["upsertFarmDocument"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "insertSupplier");
         operation.resultType = valueObjects.WarehouseModuleSupplier;
        operations["insertSupplier"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "insertFarmFlow");
         operation.resultType = valueObjects.WarehouseModuleFarmFlow;
        operations["insertFarmFlow"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "triggerTPFarms");
         operation.resultType = Boolean;
        operations["triggerTPFarms"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteFarmDocument");
         operation.resultType = Boolean;
        operations["deleteFarmDocument"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "insertFarm");
         operation.resultType = valueObjects.WarehouseModuleFarm;
        operations["insertFarm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "insertFarmUnit");
         operation.resultType = valueObjects.WarehouseModuleFarmUnit;
        operations["insertFarmUnit"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.WarehouseModuleDoctor;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctor");
         operation.resultType = valueObjects.WarehouseModuleDoctor;
        operations["getDoctor"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSuppliers");
         operation.resultElementType = valueObjects.WarehouseModuleSupplier;
        operations["getSuppliers"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getRooms");
         operation.resultElementType = valueObjects.WarehouseModuleRoom;
        operations["getRooms"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getBalanceFlows");
         operation.resultElementType = valueObjects.WarehouseModuleFarmFlow;
        operations["getBalanceFlows"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "searchFarmCards");
         operation.resultElementType = valueObjects.WarehouseModuleFarmCard;
        operations["searchFarmCards"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "searchFarms");
         operation.resultElementType = valueObjects.WarehouseModuleFarm;
        operations["searchFarms"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setFarmRequired");
         operation.resultType = Boolean;
        operations["setFarmRequired"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printOLAP");
         operation.resultType = String;
        operations["printOLAP"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteOLAP");
         operation.resultType = Boolean;
        operations["deleteOLAP"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getOLAP");
         operation.resultElementType = valueObjects.WarehouseModuleOLAPRecord;
        operations["getOLAP"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "upsertFarmPrice");
         operation.resultType = valueObjects.WarehouseModuleFarmPrice;
        operations["upsertFarmPrice"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmFlows");
         operation.resultElementType = valueObjects.WarehouseModuleFarmFlow;
        operations["getFarmFlows"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getOLAPCount");
         operation.resultType = int;
        operations["getOLAPCount"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "WarehouseModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "WarehouseModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.WarehouseModuleFarmDocument, p2:valueObjects.WarehouseModuleFarmFlow, p3:valueObjects.WarehouseModuleDoctor, p4:valueObjects.WarehouseModuleSupplier, p5:valueObjects.WarehouseModuleFarmPrice, p6:valueObjects.WarehouseModuleFarm, p7:valueObjects.WarehouseModuleFarmCard, p8:valueObjects.WarehouseModuleOLAPRecord) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setApplicationSetting' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setApplicationSetting(paramName:String, paramValue:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setApplicationSetting");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramName,paramValue) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getApplicationSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getApplicationSettings(paramNames:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getApplicationSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramNames) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmDocuments' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmDocuments(stype:String, rtype:String, start_date:String, end_date:String, doctor_id:String, supplier_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmDocuments");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(stype,rtype,start_date,end_date,doctor_id,supplier_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'upsertFarmDocument' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function upsertFarmDocument(farmDocument:valueObjects.WarehouseModuleFarmDocument) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("upsertFarmDocument");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farmDocument) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'insertSupplier' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function insertSupplier(supplier:valueObjects.WarehouseModuleSupplier) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("insertSupplier");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(supplier) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'insertFarmFlow' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function insertFarmFlow(farmFlow:valueObjects.WarehouseModuleFarmFlow) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("insertFarmFlow");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farmFlow) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'triggerTPFarms' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function triggerTPFarms(ids:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("triggerTPFarms");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ids) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteFarmDocument' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFarmDocument(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFarmDocument");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'insertFarm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function insertFarm(farm:valueObjects.WarehouseModuleFarm) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("insertFarm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farm) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'insertFarmUnit' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function insertFarmUnit(unit:valueObjects.WarehouseModuleFarmUnit) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("insertFarmUnit");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(unit) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors(nowDate:String, withbalance:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(nowDate,withbalance) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctor' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctor(id:String, withbalance:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctor");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id,withbalance) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSuppliers' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSuppliers() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSuppliers");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getRooms' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getRooms() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getRooms");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getBalanceFlows' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getBalanceFlows(doctor_id:String, from_type:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getBalanceFlows");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctor_id,from_type) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'searchFarmCards' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function searchFarmCards(search:String, doctor_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("searchFarmCards");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search,doctor_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'searchFarms' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function searchFarms(search:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("searchFarms");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setFarmRequired' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setFarmRequired(farm_id:String, required:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setFarmRequired");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farm_id,required) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printOLAP' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printOLAP(fields:ArrayCollection, fieldNames:ArrayCollection, olapData:ArrayCollection, docType:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printOLAP");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(fields,fieldNames,olapData,docType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteOLAP' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteOLAP(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteOLAP");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getOLAP' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getOLAP(doctor_id:String, farmprice_id:String, start_date:String, end_date:String, supplier_id:String, type:int, param:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getOLAP");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctor_id,farmprice_id,start_date,end_date,supplier_id,type,param) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'upsertFarmPrice' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function upsertFarmPrice(farmPrice:valueObjects.WarehouseModuleFarmPrice) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("upsertFarmPrice");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(farmPrice) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmFlows' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmFlows(docid:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmFlows");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getOLAPCount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getOLAPCount(doctor_id:String, farmprice_id:String, start_date:String, end_date:String, supplier_id:String, type:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getOLAPCount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctor_id,farmprice_id,start_date,end_date,supplier_id,type) ;
        return _internal_token;
    }
     
}

}
