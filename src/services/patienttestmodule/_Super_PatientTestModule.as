/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - PatientTestModule.as.
 */
package services.patienttestmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.PatientTestModule_TestQuestion;
import valueObjects.PatientTestModule_TestQuestionPrintData;
import valueObjects.PatientTestModule_TestResult;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_PatientTestModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_PatientTestModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.PatientTestModule_TestQuestionPrintData._initRemoteClassAlias();
        valueObjects.PatientTestModule_TestQuestion._initRemoteClassAlias();
        valueObjects.PatientTestModule_TestResult._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTestQuestionsPrintData");
         operation.resultElementType = valueObjects.PatientTestModule_TestQuestionPrintData;
        operations["getTestQuestionsPrintData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printTestQuestions");
         operation.resultType = String;
        operations["printTestQuestions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTestQuestions");
         operation.resultElementType = valueObjects.PatientTestModule_TestQuestion;
        operations["getTestQuestions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setTestQuestions");
         operation.resultType = Boolean;
        operations["setTestQuestions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientTest");
         operation.resultElementType = valueObjects.PatientTestModule_TestResult;
        operations["getPatientTest"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setPatientTest");
         operation.resultType = valueObjects.PatientTestModule_TestResult;
        operations["setPatientTest"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePatientTest");
         operation.resultType = Boolean;
        operations["deletePatientTest"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteTestDoc");
         operation.resultType = Boolean;
        operations["deleteTestDoc"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "PatientTestModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "PatientTestModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.PatientTestModule_TestQuestionPrintData, p2:valueObjects.PatientTestModule_TestQuestion, p3:valueObjects.PatientTestModule_TestResult) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTestQuestionsPrintData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTestQuestionsPrintData(doc_type:String, id_patient:String, id_test:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTestQuestionsPrintData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doc_type,id_patient,id_test) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printTestQuestions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printTestQuestions(doc_type:String, id_patient:String, id_test:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printTestQuestions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doc_type,id_patient,id_test) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTestQuestions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTestQuestions(isTest:Boolean, id_patient:String, id_test:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTestQuestions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTest,id_patient,id_test) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setTestQuestions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setTestQuestions(questions:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setTestQuestions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(questions) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientTest' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientTest(id_patient:String, id_test:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientTest");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id_patient,id_test) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setPatientTest' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setPatientTest(id_patient:String, test_result:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setPatientTest");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id_patient,test_result) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePatientTest' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePatientTest(id_test:String, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePatientTest");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id_test,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteTestDoc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteTestDoc(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteTestDoc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
}

}
