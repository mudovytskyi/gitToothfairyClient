/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - AdminService.as.
 */
package services.adminservice
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.AdminModuleAccountEducation;
import valueObjects.AdminModuleAccountPhone;
import valueObjects.AdminModuleAccountQualification;
import valueObjects.AdminModuleAccountScienceDegree;
import valueObjects.AdminModuleAccountSpeciality;
import valueObjects.AdminModuleAssistant;
import valueObjects.AdminModuleAutorizeObject;
import valueObjects.AdminModuleCabinet;
import valueObjects.AdminModuleClientColorSetting;
import valueObjects.AdminModuleClientSetting;
import valueObjects.AdminModuleDoctorAccessObject;
import valueObjects.AdminModuleDoctorAccessObjectToSet;
import valueObjects.AdminModuleHR;
import valueObjects.AdminModuleJournalFilter;
import valueObjects.AdminModuleJournalRecord;
import valueObjects.AdminModuleSubdivision;
import valueObjects.AdminModuleSubdivisionAddress;
import valueObjects.AdminModuleSubdivisionPhone;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_AdminService extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_AdminService()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.AdminModuleDoctorAccessObject._initRemoteClassAlias();
        valueObjects.AdminModuleHR._initRemoteClassAlias();
        valueObjects.AdminModuleJournalRecord._initRemoteClassAlias();
        valueObjects.AdminModuleJournalFilter._initRemoteClassAlias();
        valueObjects.AdminModuleAutorizeObject._initRemoteClassAlias();
        valueObjects.AdminModuleClientSetting._initRemoteClassAlias();
        valueObjects.AdminModuleClientColorSetting._initRemoteClassAlias();
        valueObjects.AdminModuleAssistant._initRemoteClassAlias();
        valueObjects.AdminModuleSubdivision._initRemoteClassAlias();
        valueObjects.AdminModuleCabinet._initRemoteClassAlias();
        valueObjects.AdminModuleAccountPhone._initRemoteClassAlias();
        valueObjects.AdminModuleAccountEducation._initRemoteClassAlias();
        valueObjects.AdminModuleAccountQualification._initRemoteClassAlias();
        valueObjects.AdminModuleAccountSpeciality._initRemoteClassAlias();
        valueObjects.AdminModuleAccountScienceDegree._initRemoteClassAlias();
        valueObjects.AdminModuleSubdivisionAddress._initRemoteClassAlias();
        valueObjects.AdminModuleSubdivisionPhone._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "savePassword");
         operation.resultType = Boolean;
        operations["savePassword"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorUsers");
         operation.resultElementType = valueObjects.AdminModuleDoctorAccessObject;
        operations["getDoctorUsers"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHRs");
         operation.resultElementType = valueObjects.AdminModuleHR;
        operations["getHRs"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setDoctorUsers");
         operation.resultType = Boolean;
        operations["setDoctorUsers"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "loginDoctorFunction");
         operation.resultElementType = Object;
        operations["loginDoctorFunction"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "syncAndClearJornal");
         operation.resultType = Boolean;
        operations["syncAndClearJornal"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "exitAutorization");
         operation.resultType = Boolean;
        operations["exitAutorization"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "checkAutorization");
         operation.resultType = valueObjects.AdminModuleAutorizeObject;
        operations["checkAutorization"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getJournal");
         operation.resultElementType = valueObjects.AdminModuleJournalRecord;
        operations["getJournal"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getRecordString");
         operation.resultType = String;
        operations["getRecordString"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getJournalFilters");
         operation.resultElementType = valueObjects.AdminModuleJournalFilter;
        operations["getJournalFilters"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "insertJournalRecords");
         operation.resultType = Boolean;
        operations["insertJournalRecords"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClients");
         operation.resultElementType = valueObjects.AdminModuleAutorizeObject;
        operations["getClients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveClientAndSettings");
         operation.resultType = Boolean;
        operations["saveClientAndSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveClient");
         operation.resultType = Boolean;
        operations["saveClient"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientSettings");
         operation.resultElementType = valueObjects.AdminModuleClientSetting;
        operations["getClientSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAccountClientSettings");
         operation.resultElementType = valueObjects.AdminModuleClientSetting;
        operations["getAccountClientSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setClientSettings");
         operation.resultType = Boolean;
        operations["setClientSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setAccountClientSettings");
         operation.resultType = Boolean;
        operations["setAccountClientSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientColorSettings");
         operation.resultElementType = valueObjects.AdminModuleClientColorSetting;
        operations["getClientColorSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setClientColorSettings");
         operation.resultType = Boolean;
        operations["setClientColorSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAssistantsList");
         operation.resultElementType = valueObjects.AdminModuleAssistant;
        operations["getAssistantsList"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setApplicationSetting");
         operation.resultType = Boolean;
        operations["setApplicationSetting"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getApplicationSettings");
         operation.resultElementType = Object;
        operations["getApplicationSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubdivisions");
         operation.resultElementType = valueObjects.AdminModuleSubdivision;
        operations["getSubdivisions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSubdivision");
         operation.resultType = Boolean;
        operations["saveSubdivision"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteSubdivision");
         operation.resultType = Boolean;
        operations["deleteSubdivision"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setApplicationSettings");
         operation.resultType = Boolean;
        operations["setApplicationSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "resetSubdivInvoicesSeq");
         operation.resultType = Boolean;
        operations["resetSubdivInvoicesSeq"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setSubdivInvoicesSeq");
         operation.resultType = Boolean;
        operations["setSubdivInvoicesSeq"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getCabinetsList");
         operation.resultElementType = valueObjects.AdminModuleCabinet;
        operations["getCabinetsList"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getF43");
         operation.resultType = Object;
        operations["getF43"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "checkPasswords");
         operation.resultType = String;
        operations["checkPasswords"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "validatePassword");
         operation.resultType = Boolean;
        operations["validatePassword"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPhones");
         operation.resultElementType = valueObjects.AdminModuleAccountPhone;
        operations["getPhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "savePhones");
         operation.resultType = valueObjects.AdminModuleAccountPhone;
        operations["savePhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePhoneByID");
         operation.resultType = Boolean;
        operations["deletePhoneByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getEducations");
         operation.resultElementType = valueObjects.AdminModuleAccountEducation;
        operations["getEducations"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveEducation");
         operation.resultType = valueObjects.AdminModuleAccountEducation;
        operations["saveEducation"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteEducationByID");
         operation.resultType = Boolean;
        operations["deleteEducationByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getQualifications");
         operation.resultElementType = valueObjects.AdminModuleAccountQualification;
        operations["getQualifications"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveQualification");
         operation.resultType = valueObjects.AdminModuleAccountQualification;
        operations["saveQualification"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteQualificationByID");
         operation.resultType = Boolean;
        operations["deleteQualificationByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSpecialities");
         operation.resultElementType = valueObjects.AdminModuleAccountSpeciality;
        operations["getSpecialities"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSpeciality");
         operation.resultType = valueObjects.AdminModuleAccountSpeciality;
        operations["saveSpeciality"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteSpecialityByID");
         operation.resultType = Boolean;
        operations["deleteSpecialityByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getScienceDegrees");
         operation.resultElementType = valueObjects.AdminModuleAccountScienceDegree;
        operations["getScienceDegrees"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveScienceDegree");
         operation.resultType = valueObjects.AdminModuleAccountScienceDegree;
        operations["saveScienceDegree"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteScienceDegreeByID");
         operation.resultType = Boolean;
        operations["deleteScienceDegreeByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubdivisionAddresses");
         operation.resultElementType = valueObjects.AdminModuleSubdivisionAddress;
        operations["getSubdivisionAddresses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSubdivisionAddress");
         operation.resultType = valueObjects.AdminModuleSubdivisionAddress;
        operations["saveSubdivisionAddress"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteSubdivisionAddressByID");
         operation.resultType = Boolean;
        operations["deleteSubdivisionAddressByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubdivisionPhones");
         operation.resultElementType = valueObjects.AdminModuleSubdivisionPhone;
        operations["getSubdivisionPhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSubdivisionPhones");
         operation.resultType = valueObjects.AdminModuleSubdivisionPhone;
        operations["saveSubdivisionPhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteSubdivisionPhoneByID");
         operation.resultType = Boolean;
        operations["deleteSubdivisionPhoneByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setSubdivisionMain");
         operation.resultType = Boolean;
        operations["setSubdivisionMain"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getToken");
         operation.resultType = String;
        operations["getToken"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "AdminService";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "AdminService";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.AdminModuleDoctorAccessObject, p2:valueObjects.AdminModuleDoctorAccessObjectToSet, p3:valueObjects.AdminModuleJournalRecord, p4:valueObjects.AdminModuleClientSetting, p5:valueObjects.AdminModuleAutorizeObject, p6:valueObjects.AdminModuleClientColorSetting, p7:valueObjects.AdminModuleAssistant, p8:valueObjects.AdminModuleHR, p9:valueObjects.AdminModuleJournalFilter, p10:valueObjects.AdminModuleCabinet, p11:valueObjects.AdminModuleAccountPhone, p12:valueObjects.AdminModuleAccountEducation, p13:valueObjects.AdminModuleAccountQualification, p14:valueObjects.AdminModuleAccountSpeciality, p15:valueObjects.AdminModuleAccountScienceDegree, p16:valueObjects.AdminModuleSubdivisionAddress, p17:valueObjects.AdminModuleSubdivisionPhone) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'savePassword' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function savePassword(oldpass:String, newpass:String, passType:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("savePassword");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(oldpass,newpass,passType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorUsers' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorUsers() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorUsers");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHRs' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHRs() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHRs");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setDoctorUsers' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setDoctorUsers(doctors:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setDoctorUsers");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctors) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'loginDoctorFunction' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function loginDoctorFunction(doctorslogin:String, doctorspassw:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("loginDoctorFunction");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorslogin,doctorspassw) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'syncAndClearJornal' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function syncAndClearJornal() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("syncAndClearJornal");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'exitAutorization' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function exitAutorization() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("exitAutorization");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'checkAutorization' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function checkAutorization(user:String, password:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("checkAutorization");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(user,password) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getJournal' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getJournal(accountid:String, startDateTime:String, endDateTime:String, tablename:String, fieldname:String, fieldvalue:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getJournal");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(accountid,startDateTime,endDateTime,tablename,fieldname,fieldvalue) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getRecordString' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getRecordString(tablename:String, fieldid:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getRecordString");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(tablename,fieldid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getJournalFilters' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getJournalFilters() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getJournalFilters");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'insertJournalRecords' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function insertJournalRecords(records:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("insertJournalRecords");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(records) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClients(spec:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(spec) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveClientAndSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveClientAndSettings(account:valueObjects.AdminModuleAutorizeObject, settings:ArrayCollection, saveHr:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveClientAndSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(account,settings,saveHr) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveClient' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveClient(account:valueObjects.AdminModuleAutorizeObject, saveHr:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveClient");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(account,saveHr) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientSettings() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAccountClientSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAccountClientSettings(accountId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAccountClientSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(accountId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setClientSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setClientSettings(settings:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setClientSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(settings) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setAccountClientSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setAccountClientSettings(settings:ArrayCollection, accountId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setAccountClientSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(settings,accountId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientColorSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientColorSettings() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientColorSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setClientColorSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setClientColorSettings(settings:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setClientColorSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(settings) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAssistantsList' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAssistantsList() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAssistantsList");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setApplicationSetting' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setApplicationSetting(paramName:String, paramValue:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setApplicationSetting");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramName,paramValue) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getApplicationSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getApplicationSettings(paramNames:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getApplicationSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramNames) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubdivisions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubdivisions() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubdivisions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSubdivision' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSubdivision(subdivision:valueObjects.AdminModuleSubdivision) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSubdivision");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivision) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteSubdivision' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteSubdivision(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteSubdivision");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setApplicationSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setApplicationSettings(settings:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setApplicationSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(settings) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'resetSubdivInvoicesSeq' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function resetSubdivInvoicesSeq(subdivId:String, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("resetSubdivInvoicesSeq");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivId,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setSubdivInvoicesSeq' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setSubdivInvoicesSeq(subdivId:String, ordernumberCash:int, ordernumberClearing:int, invoiceNumber:int, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setSubdivInvoicesSeq");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivId,ordernumberCash,ordernumberClearing,invoiceNumber,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getCabinetsList' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getCabinetsList() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getCabinetsList");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getF43' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getF43() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getF43");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'checkPasswords' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function checkPasswords() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("checkPasswords");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'validatePassword' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function validatePassword(pass:String, passType:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("validatePassword");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(pass,passType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPhones(account_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(account_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'savePhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function savePhones(phone:valueObjects.AdminModuleAccountPhone) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("savePhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(phone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePhoneByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePhoneByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePhoneByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getEducations' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getEducations(account_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getEducations");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(account_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveEducation' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveEducation(education:valueObjects.AdminModuleAccountEducation) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveEducation");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(education) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteEducationByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteEducationByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteEducationByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getQualifications' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getQualifications(account_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getQualifications");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(account_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveQualification' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveQualification(qualification:valueObjects.AdminModuleAccountQualification) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveQualification");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(qualification) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteQualificationByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteQualificationByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteQualificationByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSpecialities' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSpecialities(account_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSpecialities");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(account_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSpeciality' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSpeciality(speciality:valueObjects.AdminModuleAccountSpeciality) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSpeciality");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(speciality) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteSpecialityByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteSpecialityByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteSpecialityByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getScienceDegrees' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getScienceDegrees(account_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getScienceDegrees");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(account_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveScienceDegree' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveScienceDegree(scienceDegree:valueObjects.AdminModuleAccountScienceDegree) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveScienceDegree");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(scienceDegree) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteScienceDegreeByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteScienceDegreeByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteScienceDegreeByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubdivisionAddresses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubdivisionAddresses(subdivision_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubdivisionAddresses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivision_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSubdivisionAddress' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSubdivisionAddress(address:valueObjects.AdminModuleSubdivisionAddress) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSubdivisionAddress");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(address) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteSubdivisionAddressByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteSubdivisionAddressByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteSubdivisionAddressByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubdivisionPhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubdivisionPhones(subdivision_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubdivisionPhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivision_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSubdivisionPhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSubdivisionPhones(phone:valueObjects.AdminModuleSubdivisionPhone) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSubdivisionPhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(phone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteSubdivisionPhoneByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteSubdivisionPhoneByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteSubdivisionPhoneByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setSubdivisionMain' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setSubdivisionMain(subdivisionId:String, isMain:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setSubdivisionMain");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subdivisionId,isMain) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getToken' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getToken(accountId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getToken");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(accountId) ;
        return _internal_token;
    }
     
}

}
