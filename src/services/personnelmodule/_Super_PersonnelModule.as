/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - PersonnelModule.as.
 */
package services.personnelmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.PersonnelModuleCabinet;
import valueObjects.PersonnelModuleData;
import valueObjects.PersonnelModuleRule;
import valueObjects.PersonnelModuleSchedule;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_PersonnelModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_PersonnelModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPersonnelModuleData");
         operation.resultType = valueObjects.PersonnelModuleData;
        operations["getPersonnelModuleData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getCurrentPersonnelModuleData");
         operation.resultType = valueObjects.PersonnelModuleData;
        operations["getCurrentPersonnelModuleData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateOrInsertPersonnelModuleSchedule");
         operation.resultType = valueObjects.PersonnelModuleSchedule;
        operations["updateOrInsertPersonnelModuleSchedule"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePersonnelModuleRecords");
         operation.resultType = Boolean;
        operations["deletePersonnelModuleRecords"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setScheduleAsCurrent");
         operation.resultType = Boolean;
        operations["setScheduleAsCurrent"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "PersonnelModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "PersonnelModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.PersonnelModuleCabinet, p2:valueObjects.PersonnelModuleSchedule, p3:valueObjects.PersonnelModuleRule, p4:valueObjects.PersonnelModuleData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPersonnelModuleData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPersonnelModuleData(doctorID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPersonnelModuleData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getCurrentPersonnelModuleData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getCurrentPersonnelModuleData(doctorID:String, onlyCurrent:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getCurrentPersonnelModuleData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorID,onlyCurrent) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateOrInsertPersonnelModuleSchedule' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateOrInsertPersonnelModuleSchedule(schedule:valueObjects.PersonnelModuleSchedule, rulesToDeleteArrayCollection:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateOrInsertPersonnelModuleSchedule");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(schedule,rulesToDeleteArrayCollection) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePersonnelModuleRecords' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePersonnelModuleRecords(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePersonnelModuleRecords");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setScheduleAsCurrent' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setScheduleAsCurrent(ID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setScheduleAsCurrent");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
}

}
