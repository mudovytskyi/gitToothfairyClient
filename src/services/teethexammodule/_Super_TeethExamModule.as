/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - TeethExamModule.as.
 */
package services.teethexammodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.TeethExamModuleAssistantForTooth;
import valueObjects.TeethExamModuleDocForTooth;
import valueObjects.TeethExamModuleF43;
import valueObjects.TeethExamModuleToothIn;
import valueObjects.TeethExamModuleToothOut;
import valueObjects.TeethExamModuleToothSOut;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_TeethExamModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_TeethExamModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.TeethExamModuleF43._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getF43");
         operation.resultElementType = valueObjects.TeethExamModuleF43;
        operations["getF43"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getF43XML");
         operation.resultType = String;
        operations["getF43XML"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateF43");
         operation.resultType = Boolean;
        operations["updateF43"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveExam");
         operation.resultType = Boolean;
        operations["saveExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteExam");
         operation.resultType = Object;
        operations["deleteExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExam");
         operation.resultType = valueObjects.TeethExamModuleToothSOut;
        operations["getExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createConsultationReport");
         operation.resultType = String;
        operations["createConsultationReport"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createReport");
         operation.resultType = String;
        operations["createReport"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteFile");
         operation.resultType = Boolean;
        operations["deleteFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "validate_email");
         operation.resultType = Object;
        operations["validate_email"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveAdditionalForToothExam");
         operation.resultType = Boolean;
        operations["saveAdditionalForToothExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteLastExam");
         operation.resultType = int;
        operations["deleteLastExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "AgeToStr");
         operation.resultType = Object;
        operations["AgeToStr"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "TeethExamModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "TeethExamModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.TeethExamModuleToothOut, p2:valueObjects.TeethExamModuleToothIn, p3:valueObjects.TeethExamModuleToothSOut, p4:valueObjects.TeethExamModuleDocForTooth, p5:valueObjects.TeethExamModuleAssistantForTooth, p6:valueObjects.TeethExamModuleF43) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getF43' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getF43() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getF43");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getF43XML' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getF43XML() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getF43XML");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateF43' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateF43(ovo:valueObjects.TeethExamModuleF43) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateF43");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ovo) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveExam(item:valueObjects.TeethExamModuleToothIn, isUpdate:Boolean, treatmentcourseId:String, addAfterExam:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,isUpdate,treatmentcourseId,addAfterExam) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteExam(item:int, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExam(id:int, nowdate:String, isLite:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id,nowdate,isLite) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createConsultationReport' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createConsultationReport(examId:String, examByteArray:String, docType:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createConsultationReport");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examId,examByteArray,docType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createReport' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createReport(examId:String, examByteArray:String, isEmailSend:Boolean, docType:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createReport");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examId,examByteArray,isEmailSend,docType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFile(file_name:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(file_name) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'validate_email' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function validate_email(email:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("validate_email");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(email) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveAdditionalForToothExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveAdditionalForToothExam(toothexamId:String, text:String, type:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveAdditionalForToothExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(toothexamId,text,type) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteLastExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteLastExam(item:int, passForDelete:String, securityCode:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteLastExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,passForDelete,securityCode) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'AgeToStr' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function AgeToStr(Age:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("AgeToStr");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Age) ;
        return _internal_token;
    }
     
}

}
