package services
{
	import flash.net.SharedObject;
	
	import mx.utils.UIDUtil;
	
	[Bindable]
	public class ipClass
	{
			
		public static const URL_ONLINE:String = 'http://server2.vikisoft.com.ua/toothfairydemo/salus/toothfairy/toothfairyonline/ToothfairyOnlineRegistration.html';
		public static const URL_VIKISOFT:String = 'http://vikisoft.kiev.ua';
		public static const URL_RI:String = 'http://royal.co.ua/';
		
		public static const URL_VIKISOFT_MANUAL:String = 'http://vikisoft.kiev.ua/how2nhealth/';
		public static const URL_RI_MANUAL:String = 'http://royal.co.ua/%D0%BF%D1%80%D0%BE%D1%86%D0%B5%D0%B4%D1%83%D1%80%D0%B0-%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%B0/ekzameny-po-metodologii-vnedreniya/';
		
		
		public static const URL_VK:String = 'http://vk.com/club33176090';
		public static const URL_FB:String = 'http://www.facebook.com/pages/Vikisoft/183504798363790';
		public static const URL_GG:String = 'https://plus.google.com/u/0/112058186346271479357';
		public static const URL_TG:String = 'https://t.me/tmj2019';
		public static const URL_YT:String = 'https://www.youtube.com/channel/UCoa7VBe1g_m6Peoc0bQj32Q';
		
		
		public static const STORE_ENDPOINT:String = 'http://server2.vikisoft.com.ua/toothfairyhost/store/gateway.php';
		public static var SYNCS_ENDPOINT:String = 'http://server2.vikisoft.com.ua:8082/toothfairysync';
		
		//public static const RELEASES_ENDPOINT:String = 'http://localhost/toothfairy/gateways.php';
		public static const RELEASES_ENDPOINT:String = 'http://server2.vikisoft.com.ua/toothfairyhost/gateway.php';
		public static const VIKISOFT_SATATISTIC_URL:String = "http://server2.vikisoft.com.ua:82/toothfairylicenses/statistics.php";
		public static const VIKISOFT_SETTINGS_URL:String = "http://server2.vikisoft.com.ua:82/toothfairylicenses/tfsettings.php";
		
		public static const ENDPOINT_SERVICES:String = 'http://server2.vikisoft.com.ua:82/toothfairylicenses/gateway.php';
	
		
		public static function initEndPoints():void
		{
			ipClass._serverIp = SharedObject.getLocal("toothfairy").data["main_activeServer"];
		}
		public static function get serverIp():String
		{
			if(ipClass._serverIp == null)
			{
				ipClass._serverIp = SharedObject.getLocal("toothfairy").data["main_activeServer"];
			}
			return ipClass._serverIp;
		}
		public static function set serverIp(ip:String):void
		{
			SharedObject.getLocal("toothfairy").data["main_activeServer"] = ipClass._serverIp = ip;
		}
		
		public static function get serverKey():String
		{
			if(ipClass._serverKey == null)
			{
				ipClass._serverKey = SharedObject.getLocal("toothfairy").data["main_activeServer_key"];
			}
			return ipClass._serverKey;
		}
		public static function set serverKey(key:String):void
		{
			SharedObject.getLocal("toothfairy").data["main_activeServer_key"] = ipClass._serverKey = key;
		}

		public static function get tfClientId():String
		{
			if(ipClass._tfClientId == null)
			{	
				if(SharedObject.getLocal("toothfairy").data["tf_client_id"]==null)
				{
					SharedObject.getLocal("toothfairy").data["tf_client_id"]= UIDUtil.createUID();
				}
				ipClass._tfClientId = SharedObject.getLocal("toothfairy").data["tf_client_id"];
			}
			return ipClass._tfClientId;
		}
		private static var _tfClientId:String;
		private static var _serverIp:String;
		private static var _serverKey:String;
		
		public static function get endpointhost():String
		{
			return "http://"+ipClass.serverIp+"/toothfairy/gateway.php?"+ipClass.tfClientId;
		}
		public static function get endpointUpdater():String
		{
			return "http://"+ipClass.serverIp+"/toothfairy/updater/update.xml";
		}
	}
}