/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - NhealthReports.as.
 */
package services.nhealthreports
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_NhealthReports extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_NhealthReports()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "deleteFile");
         operation.resultType = Object;
        operations["deleteFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createExtract");
         operation.resultType = String;
        operations["createExtract"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create014");
         operation.resultType = String;
        operations["create014"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create003");
         operation.resultType = String;
        operations["create003"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create066");
         operation.resultType = String;
        operations["create066"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create007");
         operation.resultType = String;
        operations["create007"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create028");
         operation.resultType = String;
        operations["create028"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create039");
         operation.resultType = String;
        operations["create039"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create039_8");
         operation.resultType = String;
        operations["create039_8"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create039Month");
         operation.resultType = String;
        operations["create039Month"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create039_8Month");
         operation.resultType = String;
        operations["create039_8Month"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create063_0");
         operation.resultType = String;
        operations["create063_0"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create063_2");
         operation.resultType = String;
        operations["create063_2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create064");
         operation.resultType = String;
        operations["create064"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create064Month");
         operation.resultType = String;
        operations["create064Month"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create074");
         operation.resultType = String;
        operations["create074"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create074Month");
         operation.resultType = String;
        operations["create074Month"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create079");
         operation.resultType = String;
        operations["create079"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create086");
         operation.resultType = String;
        operations["create086"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create112");
         operation.resultType = String;
        operations["create112"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "NhealthReports";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "NhealthReports";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'deleteFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFile(file_name:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(file_name) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createExtract' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createExtract(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createExtract");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create014' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create014(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create014");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create003' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create003(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create003");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create066' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create066(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create066");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create007' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create007(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create007");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create028' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create028(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create028");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create039' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create039(docType:String, month:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create039");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,month,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create039_8' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create039_8(docType:String, month:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create039_8");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,month,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create039Month' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create039Month(docType:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create039Month");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create039_8Month' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create039_8Month(docType:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create039_8Month");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create063_0' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create063_0(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create063_0");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create063_2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create063_2(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create063_2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create064' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create064(docType:String, month:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create064");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,month,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create064Month' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create064Month(docType:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create064Month");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create074' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create074(docType:String, month:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create074");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,month,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create074Month' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create074Month(docType:String, year:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create074Month");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,year) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create079' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create079(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create079");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create086' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create086(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create086");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create112' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create112(docType:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create112");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,patientID) ;
        return _internal_token;
    }
     
}

}
