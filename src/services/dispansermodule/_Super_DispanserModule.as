/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - DispanserModule.as.
 */
package services.dispansermodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.DispanserModule_Disptype;
import valueObjects.DispanserModule_Doctor;
import valueObjects.DispanserModule_Patient;
import valueObjects.DispanserModule_PatientDispancerRecord;
import valueObjects.DispanserModule_PatientRecord;
import valueObjects.DispanserModule_Task;
import valueObjects.DispanserModule_Tasks;
import valueObjects.DispanserModule_mobilephone;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_DispanserModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_DispanserModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.DispanserModule_Doctor._initRemoteClassAlias();
        valueObjects.DispanserModule_Disptype._initRemoteClassAlias();
        valueObjects.DispanserModule_PatientRecord._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.DispanserModule_Doctor;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDisptypes");
         operation.resultElementType = valueObjects.DispanserModule_Disptype;
        operations["getDisptypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientData");
         operation.resultType = valueObjects.DispanserModule_Patient;
        operations["getPatientData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addDispancerTask");
         operation.resultType = Boolean;
        operations["addDispancerTask"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDispancerTasks");
         operation.resultType = valueObjects.DispanserModule_Tasks;
        operations["getDispancerTasks"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateDispancerTasksDate");
         operation.resultType = Boolean;
        operations["updateDispancerTasksDate"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateDispancerTasksComment");
         operation.resultType = Boolean;
        operations["updateDispancerTasksComment"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateDispancerTasksType");
         operation.resultType = Boolean;
        operations["updateDispancerTasksType"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addTaskKeyToDispancerTask");
         operation.resultType = Boolean;
        operations["addTaskKeyToDispancerTask"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteDispancerTask");
         operation.resultType = Boolean;
        operations["deleteDispancerTask"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDispancerPatients");
         operation.resultElementType = valueObjects.DispanserModule_PatientRecord;
        operations["getDispancerPatients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateDispancerTasksSMSStatus");
         operation.resultType = Boolean;
        operations["updateDispancerTasksSMSStatus"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "DispanserModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "DispanserModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.DispanserModule_Task, p2:valueObjects.DispanserModule_mobilephone, p3:valueObjects.DispanserModule_Tasks, p4:valueObjects.DispanserModule_Doctor, p5:valueObjects.DispanserModule_Disptype, p6:valueObjects.DispanserModule_PatientDispancerRecord, p7:valueObjects.DispanserModule_mobilephone, p8:valueObjects.DispanserModule_PatientRecord) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDisptypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDisptypes() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDisptypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientData(patinet_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patinet_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addDispancerTask' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addDispancerTask(patient_id:String, doctor_id:String, task_comment:String, taskEachPeriod:int, taskModePeriod:String, taskCount:int, disptype_id:String, smsstatus:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addDispancerTask");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient_id,doctor_id,task_comment,taskEachPeriod,taskModePeriod,taskCount,disptype_id,smsstatus) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDispancerTasks' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDispancerTasks(daysRemined:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDispancerTasks");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(daysRemined) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateDispancerTasksDate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateDispancerTasksDate(date:String, distaskId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateDispancerTasksDate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(date,distaskId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateDispancerTasksComment' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateDispancerTasksComment(comnt:String, distaskId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateDispancerTasksComment");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(comnt,distaskId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateDispancerTasksType' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateDispancerTasksType(type_fk:String, distaskId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateDispancerTasksType");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(type_fk,distaskId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addTaskKeyToDispancerTask' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addTaskKeyToDispancerTask(distaskId:String, taskId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addTaskKeyToDispancerTask");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(distaskId,taskId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteDispancerTask' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteDispancerTask(distaskId:String, patientId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteDispancerTask");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(distaskId,patientId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDispancerPatients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDispancerPatients(startDate:String, endDate:String, searchText:String, dispType_id:String, doc_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDispancerPatients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,searchText,dispType_id,doc_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateDispancerTasksSMSStatus' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateDispancerTasksSMSStatus(smsstatus:Boolean, distaskId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateDispancerTasksSMSStatus");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(smsstatus,distaskId) ;
        return _internal_token;
    }
     
}

}
