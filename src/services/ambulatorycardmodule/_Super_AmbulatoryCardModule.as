/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - AmbulatoryCardModule.as.
 */
package services.ambulatorycardmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.AmbulatoryCardModuleAmbcard;
import valueObjects.AmbulatoryCardModuleComorbidities;
import valueObjects.AmbulatoryCardModuleDoctor;
import valueObjects.AmbulatoryCardModuleHygcontrol;
import valueObjects.AmbulatoryCardModuleHygiene;
import valueObjects.AmbulatoryCardModuleOcclusions;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_AmbulatoryCardModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_AmbulatoryCardModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.AmbulatoryCardModuleAmbcard._initRemoteClassAlias();
        valueObjects.AmbulatoryCardModuleHygcontrol._initRemoteClassAlias();
        valueObjects.AmbulatoryCardModuleOcclusions._initRemoteClassAlias();
        valueObjects.AmbulatoryCardModuleComorbidities._initRemoteClassAlias();
        valueObjects.AmbulatoryCardModuleHygiene._initRemoteClassAlias();
        valueObjects.AmbulatoryCardModuleDoctor._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAmbCard");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleAmbcard;
        operations["getAmbCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHygcontrol");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleHygcontrol;
        operations["getHygcontrol"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateHygNavch0");
         operation.resultType = Boolean;
        operations["updateHygNavch0"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addAmbcard");
         operation.resultType = String;
        operations["addAmbcard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "insHygcontrol");
         operation.resultType = Boolean;
        operations["insHygcontrol"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getOcclusions");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleOcclusions;
        operations["getOcclusions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getComorbidities");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleComorbidities;
        operations["getComorbidities"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHygieneDesript");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleHygiene;
        operations["getHygieneDesript"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleDoctor;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addHygcontrol0");
         operation.resultType = Boolean;
        operations["addHygcontrol0"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHygNavch");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleHygcontrol;
        operations["getHygNavch"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateAmb");
         operation.resultType = Boolean;
        operations["updateAmb"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHygcon1");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleHygcontrol;
        operations["getHygcon1"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHygcon0");
         operation.resultElementType = valueObjects.AmbulatoryCardModuleHygcontrol;
        operations["getHygcon0"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteHygcontrol");
         operation.resultType = Boolean;
        operations["deleteHygcontrol"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "AmbulatoryCardModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "AmbulatoryCardModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.AmbulatoryCardModuleHygcontrol, p2:valueObjects.AmbulatoryCardModuleAmbcard, p3:valueObjects.AmbulatoryCardModuleDoctor, p4:valueObjects.AmbulatoryCardModuleOcclusions, p5:valueObjects.AmbulatoryCardModuleComorbidities, p6:valueObjects.AmbulatoryCardModuleHygiene) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAmbCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAmbCard(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAmbCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHygcontrol' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHygcontrol(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHygcontrol");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateHygNavch0' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateHygNavch0(hygnavchVO:valueObjects.AmbulatoryCardModuleHygcontrol) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateHygNavch0");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(hygnavchVO) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addAmbcard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addAmbcard(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addAmbcard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'insHygcontrol' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function insHygcontrol(hygcontrolVO:valueObjects.AmbulatoryCardModuleHygcontrol) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("insHygcontrol");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(hygcontrolVO) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getOcclusions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getOcclusions() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getOcclusions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getComorbidities' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getComorbidities() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getComorbidities");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHygieneDesript' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHygieneDesript() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHygieneDesript");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors(nowdate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(nowdate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addHygcontrol0' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addHygcontrol0(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addHygcontrol0");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHygNavch' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHygNavch(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHygNavch");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateAmb' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateAmb(ambcardVO:valueObjects.AmbulatoryCardModuleAmbcard) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateAmb");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ambcardVO) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHygcon1' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHygcon1(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHygcon1");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHygcon0' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHygcon0(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHygcon0");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteHygcontrol' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteHygcontrol(hygcontrolVO:valueObjects.AmbulatoryCardModuleHygcontrol, ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteHygcontrol");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(hygcontrolVO,ID) ;
        return _internal_token;
    }
     
}

}
