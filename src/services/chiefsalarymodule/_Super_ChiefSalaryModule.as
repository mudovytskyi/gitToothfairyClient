/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - ChiefSalaryModule.as.
 */
package services.chiefsalarymodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.ChiefSalaryModuleAssistantSalaryObject;
import valueObjects.ChiefSalaryModuleDoctorAwards;
import valueObjects.ChiefSalaryModuleDoctorSalaryObject;
import valueObjects.ChiefSalaryModuleDoctorSalarySummaryObject;
import valueObjects.ChiefSalaryModuleOLAPData;
import valueObjects.ChiefSalaryModule_ExpensesGroup;
import valueObjects.ChiefSalaryModule_ExpensesGroupSplit;
import valueObjects.ChiefSalaryModule_SalaryProc;
import valueObjects.ChiefSalaryModule_TreatmentExpensesObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_ChiefSalaryModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_ChiefSalaryModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.ChiefSalaryModuleOLAPData._initRemoteClassAlias();
        valueObjects.ChiefSalaryModuleDoctorAwards._initRemoteClassAlias();
        valueObjects.ChiefSalaryModuleDoctorSalaryObject._initRemoteClassAlias();
        valueObjects.ChiefSalaryModuleAssistantSalaryObject._initRemoteClassAlias();
        valueObjects.ChiefSalaryModule_TreatmentExpensesObject._initRemoteClassAlias();
        valueObjects.ChiefSalaryModule_ExpensesGroup._initRemoteClassAlias();
        valueObjects.ChiefSalaryModule_SalaryProc._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printOLAPData");
         operation.resultType = String;
        operations["printOLAPData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getOLAPData");
         operation.resultElementType = valueObjects.ChiefSalaryModuleOLAPData;
        operations["getOLAPData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateProcedurExpensesValue");
         operation.resultType = Boolean;
        operations["updateProcedurExpensesValue"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addDoctorAward");
         operation.resultType = Boolean;
        operations["addDoctorAward"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorAwards");
         operation.resultElementType = valueObjects.ChiefSalaryModuleDoctorAwards;
        operations["getDoctorAwards"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteDoctorAward");
         operation.resultType = Boolean;
        operations["deleteDoctorAward"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorSalarySchema");
         operation.resultElementType = valueObjects.ChiefSalaryModuleDoctorSalaryObject;
        operations["getDoctorSalarySchema"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteDoctorSalarySchemaById");
         operation.resultType = Boolean;
        operations["deleteDoctorSalarySchemaById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveDoctorSalarySchema");
         operation.resultType = Boolean;
        operations["saveDoctorSalarySchema"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctorSalary");
         operation.resultType = valueObjects.ChiefSalaryModuleDoctorSalarySummaryObject;
        operations["getDoctorSalary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAssistantSalarySchema");
         operation.resultElementType = valueObjects.ChiefSalaryModuleAssistantSalaryObject;
        operations["getAssistantSalarySchema"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteAssistantSalarySchemaById");
         operation.resultType = Boolean;
        operations["deleteAssistantSalarySchemaById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveAssistantSalarySchema");
         operation.resultType = Boolean;
        operations["saveAssistantSalarySchema"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTreatmentExpenses");
         operation.resultElementType = valueObjects.ChiefSalaryModule_TreatmentExpensesObject;
        operations["getTreatmentExpenses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveTreatmentExpenses");
         operation.resultType = Boolean;
        operations["saveTreatmentExpenses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateTreatmentExpenses");
         operation.resultType = Boolean;
        operations["updateTreatmentExpenses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteTreatmentExpenses");
         operation.resultType = Boolean;
        operations["deleteTreatmentExpenses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExpensesGroups");
         operation.resultElementType = valueObjects.ChiefSalaryModule_ExpensesGroup;
        operations["getExpensesGroups"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "removeExpensesGroup");
         operation.resultType = Boolean;
        operations["removeExpensesGroup"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveExpensesGroup");
         operation.resultType = valueObjects.ChiefSalaryModule_ExpensesGroup;
        operations["saveExpensesGroup"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "splitExpensesGroups");
         operation.resultType = Boolean;
        operations["splitExpensesGroups"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSalaryProcedures");
         operation.resultElementType = valueObjects.ChiefSalaryModule_SalaryProc;
        operations["getSalaryProcedures"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSalaryProcedures_ParentIdsRecoursive");
         operation.resultType = Object;
        operations["getSalaryProcedures_ParentIdsRecoursive"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSalaryProcedures_Recursive");
         operation.resultType = Object;
        operations["getSalaryProcedures_Recursive"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteSalaryManagmentDoc");
         operation.resultType = Boolean;
        operations["deleteSalaryManagmentDoc"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSalaryProcedures");
         operation.resultType = Boolean;
        operations["saveSalaryProcedures"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveSalaryPosition");
         operation.resultType = Boolean;
        operations["saveSalaryPosition"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSalaryPosition");
         operation.resultType = Number;
        operations["getSalaryPosition"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "ChiefSalaryModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "ChiefSalaryModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.ChiefSalaryModuleOLAPData, p2:valueObjects.ChiefSalaryModuleDoctorSalaryObject, p3:valueObjects.ChiefSalaryModuleDoctorAwards, p4:valueObjects.ChiefSalaryModuleAssistantSalaryObject, p5:valueObjects.ChiefSalaryModule_TreatmentExpensesObject, p6:valueObjects.ChiefSalaryModule_ExpensesGroup, p7:valueObjects.ChiefSalaryModule_ExpensesGroupSplit, p8:valueObjects.ChiefSalaryModule_SalaryProc) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printOLAPData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printOLAPData(doc_type:String, startDate:String, endDate:String, startAwardDate:String, endAwardDate:String, moduleworktype:String, payidInProceduresMode:Boolean, payidInProceduresPrecents:Number, isBookkeppingExpenses:Boolean, byInvoiceDate:Boolean, considerDiscount:Boolean, isCoursename:Boolean, isFactExpenses:Boolean, isSalaryPriceScheme:Boolean, doctorIDinDoctorSum:int, isDiscountingProcprice:Boolean, subdivId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printOLAPData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doc_type,startDate,endDate,startAwardDate,endAwardDate,moduleworktype,payidInProceduresMode,payidInProceduresPrecents,isBookkeppingExpenses,byInvoiceDate,considerDiscount,isCoursename,isFactExpenses,isSalaryPriceScheme,doctorIDinDoctorSum,isDiscountingProcprice,subdivId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getOLAPData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getOLAPData(startDate:String, endDate:String, startAwardDate:String, endAwardDate:String, moduleworktype:String, payidInProceduresMode:Boolean, payidInProceduresPrecents:Number, isBookkeppingExpenses:Boolean, byInvoiceDate:Boolean, considerDiscount:Boolean, isCoursename:Boolean, isFactExpenses:Boolean, isSalaryPriceScheme:Boolean, isDiscountingProcprice:Boolean, subdivId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getOLAPData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,startAwardDate,endAwardDate,moduleworktype,payidInProceduresMode,payidInProceduresPrecents,isBookkeppingExpenses,byInvoiceDate,considerDiscount,isCoursename,isFactExpenses,isSalaryPriceScheme,isDiscountingProcprice,subdivId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateProcedurExpensesValue' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateProcedurExpensesValue(procedureId:int, procedureExpensesValue:Number) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateProcedurExpensesValue");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(procedureId,procedureExpensesValue) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addDoctorAward' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addDoctorAward(doctorId:int, awardValue:Number, awardNotice:String, awardDate:String, awardFoundDate:String, moduleworktype:String, isAddToFlow:Boolean, isCashpayment:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addDoctorAward");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorId,awardValue,awardNotice,awardDate,awardFoundDate,moduleworktype,isAddToFlow,isCashpayment) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorAwards' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorAwards(doctorId:int, startDate:String, endDate:String, isPlus:Boolean, moduleworktype:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorAwards");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorId,startDate,endDate,isPlus,moduleworktype) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteDoctorAward' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteDoctorAward(awardId:int, moduleworktype:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteDoctorAward");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(awardId,moduleworktype) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorSalarySchema' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorSalarySchema(doctorID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorSalarySchema");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteDoctorSalarySchemaById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteDoctorSalarySchemaById(schemaID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteDoctorSalarySchemaById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(schemaID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveDoctorSalarySchema' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveDoctorSalarySchema(schemaObject:valueObjects.ChiefSalaryModuleDoctorSalaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveDoctorSalarySchema");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(schemaObject) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctorSalary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctorSalary(doctorID:String, reportDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctorSalary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorID,reportDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAssistantSalarySchema' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAssistantSalarySchema(assistantID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAssistantSalarySchema");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(assistantID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteAssistantSalarySchemaById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteAssistantSalarySchemaById(schemaID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteAssistantSalarySchemaById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(schemaID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveAssistantSalarySchema' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveAssistantSalarySchema(schemaObject:valueObjects.ChiefSalaryModuleAssistantSalaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveAssistantSalarySchema");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(schemaObject) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTreatmentExpenses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTreatmentExpenses(treatmentprocId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTreatmentExpenses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(treatmentprocId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveTreatmentExpenses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveTreatmentExpenses(treatmentprocExpenses:valueObjects.ChiefSalaryModule_TreatmentExpensesObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveTreatmentExpenses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(treatmentprocExpenses) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateTreatmentExpenses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateTreatmentExpenses(treatmentprocExpenses:valueObjects.ChiefSalaryModule_TreatmentExpensesObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateTreatmentExpenses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(treatmentprocExpenses) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteTreatmentExpenses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteTreatmentExpenses(expensesId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteTreatmentExpenses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(expensesId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExpensesGroups' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExpensesGroups(isEmptyGroup:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExpensesGroups");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isEmptyGroup) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'removeExpensesGroup' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function removeExpensesGroup(groupId:String, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("removeExpensesGroup");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(groupId,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveExpensesGroup' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveExpensesGroup(group:valueObjects.ChiefSalaryModule_ExpensesGroup) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveExpensesGroup");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(group) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'splitExpensesGroups' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function splitExpensesGroups(groups:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("splitExpensesGroups");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(groups) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSalaryProcedures' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSalaryProcedures(doctorId:int, searchText:String, isGrouping:Boolean, priceIndex:int, healthType:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSalaryProcedures");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorId,searchText,isGrouping,priceIndex,healthType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSalaryProcedures_ParentIdsRecoursive' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSalaryProcedures_ParentIdsRecoursive(whereStr:Object, trans:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSalaryProcedures_ParentIdsRecoursive");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(whereStr,trans) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSalaryProcedures_Recursive' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSalaryProcedures_Recursive(trans:Object, parentID:Object, whereStr:Object, isGrouping:Object, priceIndex:Object, doctorId:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSalaryProcedures_Recursive");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(trans,parentID,whereStr,isGrouping,priceIndex,doctorId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteSalaryManagmentDoc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteSalaryManagmentDoc(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteSalaryManagmentDoc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSalaryProcedures' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSalaryProcedures(salaryProcedures:ArrayCollection, doctorId:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSalaryProcedures");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(salaryProcedures,doctorId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveSalaryPosition' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveSalaryPosition(doctorId:int, amount:Number) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveSalaryPosition");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorId,amount) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSalaryPosition' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSalaryPosition(doctorId:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSalaryPosition");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(doctorId) ;
        return _internal_token;
    }
     
}

}
