/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - EHealthModule.as.
 */
package services.ehealthmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.EHealthModule_Declaration;
import valueObjects.EHealthModule_Doctor;
import valueObjects.EHealthModule_Subdivison;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_EHealthModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_EHealthModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.EHealthModule_Doctor._initRemoteClassAlias();
        valueObjects.EHealthModule_Subdivison._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDeclarationByPatientId");
         operation.resultType = valueObjects.EHealthModule_Declaration;
        operations["getDeclarationByPatientId"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveDeclaration");
         operation.resultType = valueObjects.EHealthModule_Declaration;
        operations["saveDeclaration"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getEhDoctors");
         operation.resultElementType = valueObjects.EHealthModule_Doctor;
        operations["getEhDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getEhSubdivisions");
         operation.resultElementType = valueObjects.EHealthModule_Subdivison;
        operations["getEhSubdivisions"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printForm");
         operation.resultType = String;
        operations["printForm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePrintedForm");
         operation.resultType = Boolean;
        operations["deletePrintedForm"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "registerDeclarationByPatientId");
         operation.resultType = Object;
        operations["registerDeclarationByPatientId"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "approveDeclarationByCode");
         operation.resultType = String;
        operations["approveDeclarationByCode"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "resendDeclarationVerificationCode");
         operation.resultType = String;
        operations["resendDeclarationVerificationCode"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "otp_initializeVerificationByPhone");
         operation.resultType = Object;
        operations["otp_initializeVerificationByPhone"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "otp_copmleteVerificationByCode");
         operation.resultType = Object;
        operations["otp_copmleteVerificationByCode"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "EHealthModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "EHealthModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.EHealthModule_Declaration, p2:valueObjects.EHealthModule_Doctor, p3:valueObjects.EHealthModule_Subdivison) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDeclarationByPatientId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDeclarationByPatientId(patientId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDeclarationByPatientId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveDeclaration' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveDeclaration(data:valueObjects.EHealthModule_Declaration, patientChanged:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveDeclaration");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(data,patientChanged) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getEhDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getEhDoctors() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getEhDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getEhSubdivisions' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getEhSubdivisions() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getEhSubdivisions");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printForm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printForm(docType:String, colpsID:String, colpsDate:String, patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printForm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,colpsID,colpsDate,patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePrintedForm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePrintedForm(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePrintedForm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'registerDeclarationByPatientId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerDeclarationByPatientId(user_token:String, legal_entity_id:String, pat_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerDeclarationByPatientId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(user_token,legal_entity_id,pat_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'approveDeclarationByCode' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function approveDeclarationByCode(verification_code:String, declaration_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("approveDeclarationByCode");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(verification_code,declaration_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'resendDeclarationVerificationCode' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function resendDeclarationVerificationCode(declaration_id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("resendDeclarationVerificationCode");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(declaration_id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'otp_initializeVerificationByPhone' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function otp_initializeVerificationByPhone(user_token:String, legal_entity_id:String, phone:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("otp_initializeVerificationByPhone");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(user_token,legal_entity_id,phone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'otp_copmleteVerificationByCode' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function otp_copmleteVerificationByCode(user_token:String, legal_entity_id:String, phone:String, code:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("otp_copmleteVerificationByCode");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(user_token,legal_entity_id,phone,code) ;
        return _internal_token;
    }
     
}

}
