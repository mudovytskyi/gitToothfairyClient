/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - NHDiagnos.as.
 */
package services.nhdiagnos
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.NHDiagnosInspectionExamForDoc;
import valueObjects.NHDiagnosInspectionExamIn;
import valueObjects.NHDiagnosInspectionExamOut;
import valueObjects.NHDiagnosInspectionExamOutAddon;
import valueObjects.NHDiagnosInspectionExamSOut;
import valueObjects.NHDiagnosProtocol;
import valueObjects.NHDiagnosTemplate;
import valueObjects.NHDiagnos_DairyRecord;
import valueObjects.NHDiagnos_FarmReceptionMethod;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_NHDiagnos extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_NHDiagnos()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.NHDiagnos_FarmReceptionMethod._initRemoteClassAlias();
        valueObjects.NHDiagnosProtocol._initRemoteClassAlias();
        valueObjects.NHDiagnosTemplate._initRemoteClassAlias();
        valueObjects.NHDiagnos_DairyRecord._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "printExam");
         operation.resultType = String;
        operations["printExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveExam");
         operation.resultType = Boolean;
        operations["saveExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateExamAddonInfo");
         operation.resultType = Object;
        operations["updateExamAddonInfo"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateFarmReceptionMethods");
         operation.resultType = Object;
        operations["updateFarmReceptionMethods"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteExam");
         operation.resultType = Boolean;
        operations["deleteExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteCheck");
         operation.resultType = Boolean;
        operations["deleteCheck"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExam");
         operation.resultType = valueObjects.NHDiagnosInspectionExamSOut;
        operations["getExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExamAddonInfo");
         operation.resultType = valueObjects.NHDiagnosInspectionExamOutAddon;
        operations["getExamAddonInfo"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmReceptionMethods");
         operation.resultElementType = valueObjects.NHDiagnos_FarmReceptionMethod;
        operations["getFarmReceptionMethods"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getLazyProtocols");
         operation.resultElementType = valueObjects.NHDiagnosProtocol;
        operations["getLazyProtocols"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTemplatesByProtocolId");
         operation.resultElementType = valueObjects.NHDiagnosTemplate;
        operations["getTemplatesByProtocolId"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSelectedProtocolsByTreatmentId");
         operation.resultElementType = valueObjects.NHDiagnosProtocol;
        operations["getSelectedProtocolsByTreatmentId"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDairyByPatientId");
         operation.resultElementType = valueObjects.NHDiagnos_DairyRecord;
        operations["getDairyByPatientId"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveDairy");
         operation.resultType = valueObjects.NHDiagnos_DairyRecord;
        operations["saveDairy"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "removeDairy");
         operation.resultType = Boolean;
        operations["removeDairy"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveAdditionalForToothExam");
         operation.resultType = Boolean;
        operations["saveAdditionalForToothExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteFile");
         operation.resultType = Boolean;
        operations["deleteFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExamByID");
         operation.resultType = valueObjects.NHDiagnosInspectionExamOut;
        operations["getExamByID"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "NHDiagnos";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "NHDiagnos";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.NHDiagnosInspectionExamOut, p2:valueObjects.NHDiagnosInspectionExamIn, p3:valueObjects.NHDiagnosInspectionExamSOut, p4:valueObjects.NHDiagnosInspectionExamForDoc, p5:valueObjects.NHDiagnosProtocol, p6:valueObjects.NHDiagnosTemplate, p7:valueObjects.NHDiagnos_DairyRecord, p8:valueObjects.NHDiagnos_FarmReceptionMethod, p9:valueObjects.NHDiagnosInspectionExamOutAddon) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8,p9) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'printExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function printExam(examId:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("printExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveExam(item:valueObjects.NHDiagnosInspectionExamIn, isUpdate:Boolean, addAfterExam:Boolean, protocols:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,isUpdate,addAfterExam,protocols) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateExamAddonInfo' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateExamAddonInfo(item:valueObjects.NHDiagnosInspectionExamIn, examid:int, isUpdate:Boolean, addAfterExam:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateExamAddonInfo");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,examid,isUpdate,addAfterExam) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateFarmReceptionMethods' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateFarmReceptionMethods(item:valueObjects.NHDiagnosInspectionExamIn, examid:int, isUpdate:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateFarmReceptionMethods");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,examid,isUpdate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteExam(item:int, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteCheck' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteCheck(passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteCheck");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExam(id:int, isLite:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id,isLite) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExamAddonInfo' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExamAddonInfo(examid:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExamAddonInfo");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmReceptionMethods' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmReceptionMethods(examid:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmReceptionMethods");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getLazyProtocols' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getLazyProtocols(searchText:String, specialities:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getLazyProtocols");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(searchText,specialities) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTemplatesByProtocolId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTemplatesByProtocolId(protocolId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTemplatesByProtocolId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(protocolId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSelectedProtocolsByTreatmentId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSelectedProtocolsByTreatmentId(treatId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSelectedProtocolsByTreatmentId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(treatId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDairyByPatientId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDairyByPatientId(patientId:String, isFullHistory:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDairyByPatientId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId,isFullHistory) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveDairy' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveDairy(data:valueObjects.NHDiagnos_DairyRecord) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveDairy");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(data) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'removeDairy' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function removeDairy(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("removeDairy");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveAdditionalForToothExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveAdditionalForToothExam(toothexamId:String, text:String, type:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveAdditionalForToothExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(toothexamId,text,type) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFile(file_name:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(file_name) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExamByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExamByID(id:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExamByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
}

}
