/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - PatientCardModule.as.
 */
package services.patientcardmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.PatientCardModuleAddPatientObject;
import valueObjects.PatientCardModuleClinicRegistrationData;
import valueObjects.PatientCardModuleDataGridPatientObject;
import valueObjects.PatientCardModuleDoctorObject;
import valueObjects.PatientCardModuleInsuranceCompanyObject;
import valueObjects.PatientCardModuleInsurancePolisObject;
import valueObjects.PatientCardModulePatientForLegalGuardian;
import valueObjects.PatientCardModulePatientTasksHistoryObject;
import valueObjects.PatientCardModulePatientTestCard;
import valueObjects.PatientCardModulePrimaryDataGridPatientObject;
import valueObjects.PatientCardModuleSubdivisonObject;
import valueObjects.PatientCardModuleTreatmentBalanceData;
import valueObjects.PatientCardModuleTreatmentBalanceObject;
import valueObjects.PatientCardModule_Address;
import valueObjects.PatientCardModule_Phone;
import valueObjects.PatientCardModule_mobilephone;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_PatientCardModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_PatientCardModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.PatientCardModule_mobilephone._initRemoteClassAlias();
        valueObjects.PatientCardModuleDataGridPatientObject._initRemoteClassAlias();
        valueObjects.PatientCardModuleDoctorObject._initRemoteClassAlias();
        valueObjects.PatientCardModulePatientTasksHistoryObject._initRemoteClassAlias();
        valueObjects.PatientCardModulePrimaryDataGridPatientObject._initRemoteClassAlias();
        valueObjects.PatientCardModuleInsurancePolisObject._initRemoteClassAlias();
        valueObjects.PatientCardModuleInsuranceCompanyObject._initRemoteClassAlias();
        valueObjects.PatientCardModulePatientForLegalGuardian._initRemoteClassAlias();
        valueObjects.PatientCardModuleSubdivisonObject._initRemoteClassAlias();
        valueObjects.PatientCardModule_Phone._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setApplicationSetting");
         operation.resultType = Boolean;
        operations["setApplicationSetting"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getApplicationSettings");
         operation.resultElementType = Object;
        operations["getApplicationSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatient");
         operation.resultType = valueObjects.PatientCardModuleAddPatientObject;
        operations["getPatient"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getMobilePhoneByPatientID");
         operation.resultElementType = valueObjects.PatientCardModule_mobilephone;
        operations["getMobilePhoneByPatientID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsLazyFiltered");
         operation.resultElementType = valueObjects.PatientCardModuleDataGridPatientObject;
        operations["getPatientsLazyFiltered"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateReturningCardByPatientID");
         operation.resultType = Boolean;
        operations["updateReturningCardByPatientID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsTreatmentBalanceHistoryByID");
         operation.resultType = valueObjects.PatientCardModuleTreatmentBalanceData;
        operations["getPatientsTreatmentBalanceHistoryByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsDoctorHistoryByID");
         operation.resultElementType = valueObjects.PatientCardModuleDoctorObject;
        operations["getPatientsDoctorHistoryByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsTaskHistoryByID");
         operation.resultElementType = valueObjects.PatientCardModulePatientTasksHistoryObject;
        operations["getPatientsTaskHistoryByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addPatient");
         operation.resultType = int;
        operations["addPatient"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updatePatient");
         operation.resultType = Boolean;
        operations["updatePatient"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updatePatientForCalendar");
         operation.resultType = String;
        operations["updatePatientForCalendar"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePatients");
         operation.resultType = Boolean;
        operations["deletePatients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getNextCardNumber");
         operation.resultType = Number;
        operations["getNextCardNumber"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "AddUpdateTestCard");
         operation.resultType = Boolean;
        operations["AddUpdateTestCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTestCard");
         operation.resultType = valueObjects.PatientCardModulePatientTestCard;
        operations["getTestCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClinicRegistrationData");
         operation.resultType = valueObjects.PatientCardModuleClinicRegistrationData;
        operations["getClinicRegistrationData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPrimaryPatients");
         operation.resultElementType = valueObjects.PatientCardModulePrimaryDataGridPatientObject;
        operations["getPrimaryPatients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deletePrimayPatients");
         operation.resultType = Boolean;
        operations["deletePrimayPatients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getInsurancePoliceByPatientId");
         operation.resultElementType = valueObjects.PatientCardModuleInsurancePolisObject;
        operations["getInsurancePoliceByPatientId"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getInsuranceCompanys");
         operation.resultElementType = valueObjects.PatientCardModuleInsuranceCompanyObject;
        operations["getInsuranceCompanys"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addInsuranceToPatient");
         operation.resultType = Boolean;
        operations["addInsuranceToPatient"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateInsuranceToPatient");
         operation.resultType = Boolean;
        operations["updateInsuranceToPatient"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteInsuranceToPatient");
         operation.resultType = Boolean;
        operations["deleteInsuranceToPatient"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.PatientCardModuleDoctorObject;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsForLegalGuardian");
         operation.resultElementType = valueObjects.PatientCardModulePatientForLegalGuardian;
        operations["getPatientsForLegalGuardian"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientForLegalGuardianByID");
         operation.resultType = valueObjects.PatientCardModulePatientForLegalGuardian;
        operations["getPatientForLegalGuardianByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateArchivedByPatientID");
         operation.resultType = Boolean;
        operations["updateArchivedByPatientID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getSubdivisons");
         operation.resultElementType = valueObjects.PatientCardModuleSubdivisonObject;
        operations["getSubdivisons"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "exportPatientsData");
         operation.resultType = String;
        operations["exportPatientsData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteFile");
         operation.resultType = Boolean;
        operations["deleteFile"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "mpi_getPhones");
         operation.resultElementType = valueObjects.PatientCardModule_Phone;
        operations["mpi_getPhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "mpi_savePhones");
         operation.resultType = valueObjects.PatientCardModule_Phone;
        operations["mpi_savePhones"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "mpi_deletePhoneByID");
         operation.resultType = Boolean;
        operations["mpi_deletePhoneByID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "mpi_getAddresses");
         operation.resultElementType = Object;
        operations["mpi_getAddresses"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "mpi_saveAddress");
         operation.resultType = valueObjects.PatientCardModule_Address;
        operations["mpi_saveAddress"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "mpi_deleteAddressByID");
         operation.resultType = Boolean;
        operations["mpi_deleteAddressByID"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "PatientCardModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "PatientCardModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.PatientCardModuleAddPatientObject, p2:valueObjects.PatientCardModuleClinicRegistrationData, p3:valueObjects.PatientCardModuleDataGridPatientObject, p4:valueObjects.PatientCardModulePatientTestCard, p5:valueObjects.PatientCardModulePrimaryDataGridPatientObject, p6:valueObjects.PatientCardModuleInsurancePolisObject, p7:valueObjects.PatientCardModuleInsuranceCompanyObject, p8:valueObjects.PatientCardModulePatientTasksHistoryObject, p9:valueObjects.PatientCardModuleDoctorObject, p10:valueObjects.PatientCardModuleTreatmentBalanceObject, p11:valueObjects.PatientCardModuleTreatmentBalanceData, p12:valueObjects.PatientCardModule_mobilephone, p13:valueObjects.PatientCardModulePatientForLegalGuardian, p14:valueObjects.PatientCardModuleSubdivisonObject, p15:valueObjects.PatientCardModule_Phone, p16:valueObjects.PatientCardModule_Address) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setApplicationSetting' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setApplicationSetting(paramName:String, paramValue:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setApplicationSetting");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramName,paramValue) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getApplicationSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getApplicationSettings(paramNames:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getApplicationSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramNames) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatient' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatient(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatient");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getMobilePhoneByPatientID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getMobilePhoneByPatientID(patient_id:String, isAddObject:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getMobilePhoneByPatientID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient_id,isAddObject) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsLazyFiltered' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsLazyFiltered(searchLabel:String, searchText:String, includePhoneSearch:Boolean, pageSize:int, pageIndex:int, patientID:String, includeArchived:Boolean, nhTreatType:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsLazyFiltered");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(searchLabel,searchText,includePhoneSearch,pageSize,pageIndex,patientID,includeArchived,nhTreatType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateReturningCardByPatientID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateReturningCardByPatientID(patient_id:String, is_returned:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateReturningCardByPatientID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient_id,is_returned) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsTreatmentBalanceHistoryByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsTreatmentBalanceHistoryByID(patientID:String, isCoursename:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsTreatmentBalanceHistoryByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID,isCoursename) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsDoctorHistoryByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsDoctorHistoryByID(patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsDoctorHistoryByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsTaskHistoryByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsTaskHistoryByID(patientID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsTaskHistoryByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addPatient' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addPatient(newPatient:valueObjects.PatientCardModuleAddPatientObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addPatient");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newPatient) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updatePatient' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updatePatient(newPatient:valueObjects.PatientCardModuleAddPatientObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updatePatient");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newPatient) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updatePatientForCalendar' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updatePatientForCalendar(newPatient:valueObjects.PatientCardModuleAddPatientObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updatePatientForCalendar");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newPatient) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePatients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePatients(passForDel:String, Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePatients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(passForDel,Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getNextCardNumber' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getNextCardNumber(currentYear:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getNextCardNumber");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(currentYear) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'AddUpdateTestCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function AddUpdateTestCard(newPatientTestCard:valueObjects.PatientCardModulePatientTestCard) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("AddUpdateTestCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(newPatientTestCard) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTestCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTestCard(patientID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTestCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClinicRegistrationData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClinicRegistrationData(patientID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClinicRegistrationData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPrimaryPatients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPrimaryPatients() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPrimaryPatients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deletePrimayPatients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deletePrimayPatients(Arguments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deletePrimayPatients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Arguments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getInsurancePoliceByPatientId' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getInsurancePoliceByPatientId(patientId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getInsurancePoliceByPatientId");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getInsuranceCompanys' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getInsuranceCompanys() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getInsuranceCompanys");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addInsuranceToPatient' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addInsuranceToPatient(insurance:valueObjects.PatientCardModuleInsurancePolisObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addInsuranceToPatient");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(insurance) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateInsuranceToPatient' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateInsuranceToPatient(insurance:valueObjects.PatientCardModuleInsurancePolisObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateInsuranceToPatient");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(insurance) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteInsuranceToPatient' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteInsuranceToPatient(insuranceId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteInsuranceToPatient");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(insuranceId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsForLegalGuardian' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsForLegalGuardian(search:String, id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsForLegalGuardian");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search,id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientForLegalGuardianByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientForLegalGuardianByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientForLegalGuardianByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateArchivedByPatientID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateArchivedByPatientID(patient_id:String, is_archived:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateArchivedByPatientID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient_id,is_archived) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getSubdivisons' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getSubdivisons() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getSubdivisons");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'exportPatientsData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function exportPatientsData(isLastname:Boolean, isFirstname:Boolean, isMiddlename:Boolean, isSex:Boolean, isPhone:Boolean, isAge:Boolean, isBirthday:Boolean, isCity:Boolean, isAddress:Boolean, isDiscount_amount:Boolean, isCarddate:Boolean, docType:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("exportPatientsData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isLastname,isFirstname,isMiddlename,isSex,isPhone,isAge,isBirthday,isCity,isAddress,isDiscount_amount,isCarddate,docType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteFile' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteFile(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteFile");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'mpi_getPhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function mpi_getPhones(patient_id:String, typePhone:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("mpi_getPhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient_id,typePhone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'mpi_savePhones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function mpi_savePhones(phone:valueObjects.PatientCardModule_Phone) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("mpi_savePhones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(phone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'mpi_deletePhoneByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function mpi_deletePhoneByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("mpi_deletePhoneByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'mpi_getAddresses' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function mpi_getAddresses(patient_id:String, addressType:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("mpi_getAddresses");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient_id,addressType) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'mpi_saveAddress' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function mpi_saveAddress(address:valueObjects.PatientCardModule_Address) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("mpi_saveAddress");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(address) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'mpi_deleteAddressByID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function mpi_deleteAddressByID(id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("mpi_deleteAddressByID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
}

}
