/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - HealthPlanMKB10Module.as.
 */
package services.healthplanmkb10module
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.HealthPlanMKB10Assistant;
import valueObjects.HealthPlanMKB10ClinicPersonal;
import valueObjects.HealthPlanMKB10Comment;
import valueObjects.HealthPlanMKB10Diagnos2;
import valueObjects.HealthPlanMKB10Doctor;
import valueObjects.HealthPlanMKB10F43;
import valueObjects.HealthPlanMKB10MKB;
import valueObjects.HealthPlanMKB10MKBProtocol;
import valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc;
import valueObjects.HealthPlanMKB10ModuleTreatmentProc;
import valueObjects.HealthPlanMKB10Patient;
import valueObjects.HealthPlanMKB10QuickF43;
import valueObjects.HealthPlanMKB10Template;
import valueObjects.HealthPlanMKB10ToothExam;
import valueObjects.HealthPlanMKB10TreatmentAdditions;
import valueObjects.HealthPlanMKB10TreatmentCourse;
import valueObjects.HealthPlanMKB10TreatmentDiagnos;
import valueObjects.HealthPlanMKB10TreatmentObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_HealthPlanMKB10Module extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_HealthPlanMKB10Module()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.HealthPlanMKB10Patient._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10MKBProtocol._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10TreatmentDiagnos._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10Diagnos2._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10Comment._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10TreatmentCourse._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10ModuleTreatmentProc._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10QuickF43._initRemoteClassAlias();
        valueObjects.HealthPlanMKB10Template._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExam");
         operation.resultType = valueObjects.HealthPlanMKB10ToothExam;
        operations["getExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatients");
         operation.resultElementType = valueObjects.HealthPlanMKB10Patient;
        operations["getPatients"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProtocols");
         operation.resultElementType = valueObjects.HealthPlanMKB10MKBProtocol;
        operations["getProtocols"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDiagnosTreatment");
         operation.resultElementType = valueObjects.HealthPlanMKB10TreatmentDiagnos;
        operations["getDiagnosTreatment"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultType = valueObjects.HealthPlanMKB10ClinicPersonal;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createDiagnos2");
         operation.resultElementType = valueObjects.HealthPlanMKB10Diagnos2;
        operations["createDiagnos2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "specificationDiagnos");
         operation.resultElementType = valueObjects.HealthPlanMKB10Diagnos2;
        operations["specificationDiagnos"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDiagnos2Comments");
         operation.resultElementType = valueObjects.HealthPlanMKB10Comment;
        operations["getDiagnos2Comments"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveDiagnosComments");
         operation.resultType = Boolean;
        operations["saveDiagnosComments"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveDiagnos2");
         operation.resultType = String;
        operations["saveDiagnos2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTreatmentCourse");
         operation.resultElementType = valueObjects.HealthPlanMKB10TreatmentCourse;
        operations["getTreatmentCourse"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updDiagnos2");
         operation.resultType = Boolean;
        operations["updDiagnos2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "delDiagnos2");
         operation.resultType = Boolean;
        operations["delDiagnos2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updDiagnos2Template");
         operation.resultElementType = valueObjects.HealthPlanMKB10ModuleTreatmentProc;
        operations["updDiagnos2Template"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updDiagnos2SubscriptionCard");
         operation.resultElementType = valueObjects.HealthPlanMKB10ModuleTreatmentProc;
        operations["updDiagnos2SubscriptionCard"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addAddition");
         operation.resultType = Boolean;
        operations["addAddition"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "removeAddition");
         operation.resultType = Boolean;
        operations["removeAddition"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProtocolText");
         operation.resultType = String;
        operations["getProtocolText"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTreatmentProc");
         operation.resultElementType = valueObjects.HealthPlanMKB10ModuleTreatmentProc;
        operations["getTreatmentProc"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTreatmentProcArrayForDictionary");
         operation.resultElementType = valueObjects.HealthPlanMKB10ModuleTreatmentProc;
        operations["getTreatmentProcArrayForDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getParrentIdsRecoursive");
         operation.resultType = Object;
        operations["getParrentIdsRecoursive"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getTreatmentProcArrayForDictionaryRecursive");
         operation.resultType = Object;
        operations["getTreatmentProcArrayForDictionaryRecursive"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getFarmsForProcDictionary");
         operation.resultElementType = valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc;
        operations["getFarmsForProcDictionary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHealthProcedures");
         operation.resultElementType = valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc;
        operations["getHealthProcedures"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveTreatmentProc");
         operation.resultType = Boolean;
        operations["saveTreatmentProc"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveOrder");
         operation.resultType = Boolean;
        operations["saveOrder"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "confirmationUpdate");
         operation.resultType = Boolean;
        operations["confirmationUpdate"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "checkDiagnos2");
         operation.resultType = int;
        operations["checkDiagnos2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "nhealth_addTreatmentcourse");
         operation.resultType = Boolean;
        operations["nhealth_addTreatmentcourse"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateAdditionDate");
         operation.resultType = Boolean;
        operations["updateAdditionDate"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "searchProtocols");
         operation.resultElementType = valueObjects.HealthPlanMKB10MKBProtocol;
        operations["searchProtocols"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getF43");
         operation.resultElementType = valueObjects.HealthPlanMKB10QuickF43;
        operations["getF43"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getQuickProtocols");
         operation.resultElementType = valueObjects.HealthPlanMKB10MKBProtocol;
        operations["getQuickProtocols"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getQuickTemplates");
         operation.resultElementType = valueObjects.HealthPlanMKB10Template;
        operations["getQuickTemplates"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveQuickTreatmentCourse");
         operation.resultType = Boolean;
        operations["saveQuickTreatmentCourse"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getQuickTreatmentCourseFarms");
         operation.resultElementType = valueObjects.HealthPlanMKB10QuickF43;
        operations["getQuickTreatmentCourseFarms"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProtocolWithASR");
         operation.resultElementType = valueObjects.HealthPlanMKB10MKBProtocol;
        operations["getProtocolWithASR"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteDiagnos2");
         operation.resultType = Object;
        operations["deleteDiagnos2"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteConfirmDIagnos2");
         operation.resultType = int;
        operations["deleteConfirmDIagnos2"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "HealthPlanMKB10Module";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "HealthPlanMKB10Module";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.HealthPlanMKB10MKB, p2:valueObjects.HealthPlanMKB10TreatmentDiagnos, p3:valueObjects.HealthPlanMKB10Diagnos2, p4:valueObjects.HealthPlanMKB10Doctor, p5:valueObjects.HealthPlanMKB10Patient, p6:valueObjects.HealthPlanMKB10ToothExam, p7:valueObjects.HealthPlanMKB10Comment, p8:valueObjects.HealthPlanMKB10MKBProtocol, p9:valueObjects.HealthPlanMKB10TreatmentAdditions, p10:valueObjects.HealthPlanMKB10Template, p11:valueObjects.HealthPlanMKB10TreatmentCourse, p12:valueObjects.HealthPlanMKB10TreatmentObject, p13:valueObjects.HealthPlanMKB10ModuleTreatmentFarmsProc, p14:valueObjects.HealthPlanMKB10ModuleTreatmentProc, p15:valueObjects.HealthPlanMKB10ClinicPersonal, p16:valueObjects.HealthPlanMKB10Assistant, p17:valueObjects.HealthPlanMKB10F43, p18:valueObjects.HealthPlanMKB10QuickF43) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExam(id:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatients' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatients() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatients");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProtocols' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProtocols(F43ID:int, specialities:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProtocols");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(F43ID,specialities) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDiagnosTreatment' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDiagnosTreatment(trcourse:int, trsumma:Boolean, istoothsort:Boolean, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDiagnosTreatment");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(trcourse,trsumma,istoothsort,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors(nowDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(nowDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createDiagnos2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createDiagnos2(examId:int, specialities:Object, istoothsort:Object, priceIndex:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createDiagnos2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examId,specialities,istoothsort,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'specificationDiagnos' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function specificationDiagnos(diags:ArrayCollection, all:Boolean, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("specificationDiagnos");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diags,all,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDiagnos2Comments' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDiagnos2Comments(diagnosid:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDiagnos2Comments");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnosid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveDiagnosComments' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveDiagnosComments(comments:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveDiagnosComments");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(comments) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveDiagnos2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveDiagnos2(diagnoses:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveDiagnos2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnoses) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTreatmentCourse' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTreatmentCourse(patient:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTreatmentCourse");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patient) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updDiagnos2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updDiagnos2(diag:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updDiagnos2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diag) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'delDiagnos2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function delDiagnos2(diagId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("delDiagnos2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updDiagnos2Template' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updDiagnos2Template(templateID:String, diagID:String, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updDiagnos2Template");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(templateID,diagID,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updDiagnos2SubscriptionCard' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updDiagnos2SubscriptionCard(subCardID:String, diagID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updDiagnos2SubscriptionCard");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(subCardID,diagID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addAddition' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addAddition(addition:valueObjects.HealthPlanMKB10TreatmentAdditions) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addAddition");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(addition) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'removeAddition' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function removeAddition(addition:valueObjects.HealthPlanMKB10TreatmentAdditions, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("removeAddition");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(addition,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProtocolText' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProtocolText(id:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProtocolText");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTreatmentProc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTreatmentProc(diagnos2id:int, toothuse:int, priceIndex:int, skipCount:int, getCount:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTreatmentProc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnos2id,toothuse,priceIndex,skipCount,getCount) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTreatmentProcArrayForDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTreatmentProcArrayForDictionary(diagnos2id:int, querymode:String, searchText:String, isGrouping:Boolean, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTreatmentProcArrayForDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnos2id,querymode,searchText,isGrouping,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getParrentIdsRecoursive' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getParrentIdsRecoursive(whereStr:Object, trans:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getParrentIdsRecoursive");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(whereStr,trans) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getTreatmentProcArrayForDictionaryRecursive' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getTreatmentProcArrayForDictionaryRecursive(trans:Object, parentID:Object, whereStr:Object, diagnos2id:Object, querymode:Object, isGrouping:Object, priceIndex:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getTreatmentProcArrayForDictionaryRecursive");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(trans,parentID,whereStr,diagnos2id,querymode,isGrouping,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getFarmsForProcDictionary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getFarmsForProcDictionary() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getFarmsForProcDictionary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHealthProcedures' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHealthProcedures(isLazyLoading:Boolean, findCharacters:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHealthProcedures");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isLazyLoading,findCharacters) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveTreatmentProc' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveTreatmentProc(diagnos2id:int, treatmentprocs:ArrayCollection, treatprocsDel:ArrayCollection, patientId:String, isAfterExamsEnabled:Boolean, priceIndex:int, cabinetid:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveTreatmentProc");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnos2id,treatmentprocs,treatprocsDel,patientId,isAfterExamsEnabled,priceIndex,cabinetid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveOrder' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveOrder(diagnos_id:int, doctorid:int, patientid:int, treatmentprocs:ArrayCollection, assistantid:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveOrder");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnos_id,doctorid,patientid,treatmentprocs,assistantid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'confirmationUpdate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function confirmationUpdate(passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("confirmationUpdate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'checkDiagnos2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function checkDiagnos2(patId:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("checkDiagnos2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'nhealth_addTreatmentcourse' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function nhealth_addTreatmentcourse(patId:String, docId:String, assistId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("nhealth_addTreatmentcourse");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patId,docId,assistId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateAdditionDate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateAdditionDate(addition_id:int, addition_date:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateAdditionDate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(addition_id,addition_date) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'searchProtocols' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function searchProtocols(search:String, specialities:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("searchProtocols");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(search,specialities) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getF43' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getF43(isQuick:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getF43");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isQuick) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getQuickProtocols' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getQuickProtocols(f43Id:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getQuickProtocols");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(f43Id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getQuickTemplates' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getQuickTemplates(protocolId:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getQuickTemplates");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(protocolId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveQuickTreatmentCourse' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveQuickTreatmentCourse(diagnos2:valueObjects.HealthPlanMKB10Diagnos2, patientId:String, assistantId:String, isAfterExamsEnabled:Boolean, priceIndex:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveQuickTreatmentCourse");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(diagnos2,patientId,assistantId,isAfterExamsEnabled,priceIndex) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getQuickTreatmentCourseFarms' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getQuickTreatmentCourseFarms(healthproc_fk:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getQuickTreatmentCourseFarms");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(healthproc_fk) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProtocolWithASR' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProtocolWithASR() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProtocolWithASR");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteDiagnos2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteDiagnos2(item:int, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteDiagnos2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteConfirmDIagnos2' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteConfirmDIagnos2(item:int, passForDelete:String, securityCode:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteConfirmDIagnos2");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(item,passForDelete,securityCode) ;
        return _internal_token;
    }
     
}

}
