/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - ChiefBudgetModule.as.
 */
package services.chiefbudgetmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.ChiefBudgetModule_BudgetData;
import valueObjects.ChiefBudgetModule_BudgetStatistics;
import valueObjects.ChiefBudgetModule_Group;
import valueObjects.ChiefBudgetModule_GroupSplit;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_ChiefBudgetModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_ChiefBudgetModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.ChiefBudgetModule_Group._initRemoteClassAlias();
        valueObjects.ChiefBudgetModule_BudgetStatistics._initRemoteClassAlias();
        valueObjects.ChiefBudgetModule_BudgetData._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getGroups");
         operation.resultElementType = valueObjects.ChiefBudgetModule_Group;
        operations["getGroups"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveGroup");
         operation.resultType = valueObjects.ChiefBudgetModule_Group;
        operations["saveGroup"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "splitGroups");
         operation.resultType = Boolean;
        operations["splitGroups"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "removeGroup");
         operation.resultType = Boolean;
        operations["removeGroup"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getBudgetStatistics");
         operation.resultElementType = valueObjects.ChiefBudgetModule_BudgetStatistics;
        operations["getBudgetStatistics"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getBudgetData");
         operation.resultElementType = valueObjects.ChiefBudgetModule_BudgetData;
        operations["getBudgetData"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "ChiefBudgetModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "ChiefBudgetModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.ChiefBudgetModule_Group, p2:valueObjects.ChiefBudgetModule_BudgetData, p3:valueObjects.ChiefBudgetModule_BudgetStatistics, p4:valueObjects.ChiefBudgetModule_GroupSplit) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getGroups' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getGroups(groupRules:int, groupTypes:int, isCashpayment:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getGroups");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(groupRules,groupTypes,isCashpayment) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveGroup' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveGroup(group:valueObjects.ChiefBudgetModule_Group) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveGroup");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(group) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'splitGroups' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function splitGroups(groups:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("splitGroups");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(groups) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'removeGroup' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function removeGroup(groupId:String, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("removeGroup");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(groupId,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getBudgetStatistics' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getBudgetStatistics(startDate:String, endDate:String, period:String, accumulationMode:Boolean, dynamicExpenses:Boolean, byInvoiceDate:Boolean, payidInProceduresMode:Boolean, payidInProceduresPrecents:Number) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getBudgetStatistics");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,period,accumulationMode,dynamicExpenses,byInvoiceDate,payidInProceduresMode,payidInProceduresPrecents) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getBudgetData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getBudgetData(startDate:String, endDate:String, considerDiscount:Boolean, doctorsDynamic:Boolean, considerExpenses:Boolean, dynamicExpenses:Boolean, byInvoiceDate:Boolean, payidInProceduresMode:Boolean, payidInProceduresPrecents:Number) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getBudgetData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,considerDiscount,doctorsDynamic,considerExpenses,dynamicExpenses,byInvoiceDate,payidInProceduresMode,payidInProceduresPrecents) ;
        return _internal_token;
    }
     
}

}
