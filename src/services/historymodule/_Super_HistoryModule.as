/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - HistoryModule.as.
 */
package services.historymodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.HistoryModuleClinicRegistrationData;
import valueObjects.HistoryModuleDateDoctors;
import valueObjects.HistoryModuleDictionaryObject;
import valueObjects.HistoryModuleDoctor;
import valueObjects.HistoryModuleExam;
import valueObjects.HistoryModuleExamTooth;
import valueObjects.HistoryModuleIllHistory;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_HistoryModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_HistoryModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.HistoryModuleDoctor._initRemoteClassAlias();
        valueObjects.HistoryModuleExam._initRemoteClassAlias();
        valueObjects.HistoryModuleDateDoctors._initRemoteClassAlias();
        valueObjects.HistoryModuleIllHistory._initRemoteClassAlias();
        valueObjects.HistoryModuleDictionaryObject._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registerTypes");
         operation.resultType = Object;
        operations["registerTypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.HistoryModuleDoctor;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getToothExam");
         operation.resultElementType = valueObjects.HistoryModuleExam;
        operations["getToothExam"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDateDoctors");
         operation.resultElementType = valueObjects.HistoryModuleDateDoctors;
        operations["getDateDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getIllHistory");
         operation.resultElementType = valueObjects.HistoryModuleIllHistory;
        operations["getIllHistory"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addIllHistory");
         operation.resultType = Boolean;
        operations["addIllHistory"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updIllHistory");
         operation.resultType = Boolean;
        operations["updIllHistory"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAnamnes");
         operation.resultElementType = valueObjects.HistoryModuleDictionaryObject;
        operations["getAnamnes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addAnamnes");
         operation.resultType = Boolean;
        operations["addAnamnes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getStatus");
         operation.resultElementType = valueObjects.HistoryModuleDictionaryObject;
        operations["getStatus"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addStatus");
         operation.resultType = Boolean;
        operations["addStatus"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDiagnos");
         operation.resultElementType = valueObjects.HistoryModuleDictionaryObject;
        operations["getDiagnos"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addDiagnos");
         operation.resultType = Boolean;
        operations["addDiagnos"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPlan");
         operation.resultElementType = valueObjects.HistoryModuleDictionaryObject;
        operations["getPlan"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addPlan");
         operation.resultType = Boolean;
        operations["addPlan"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getConsult");
         operation.resultElementType = valueObjects.HistoryModuleDictionaryObject;
        operations["getConsult"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addConsult");
         operation.resultType = Boolean;
        operations["addConsult"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHealth");
         operation.resultElementType = valueObjects.HistoryModuleDictionaryObject;
        operations["getHealth"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addHealth");
         operation.resultType = Boolean;
        operations["addHealth"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClinicRegistrationData");
         operation.resultType = valueObjects.HistoryModuleClinicRegistrationData;
        operations["getClinicRegistrationData"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "HistoryModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "HistoryModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registerTypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registerTypes(p1:valueObjects.HistoryModuleDictionaryObject, p2:valueObjects.HistoryModuleDateDoctors, p3:valueObjects.HistoryModuleExamTooth, p4:valueObjects.HistoryModuleExam, p5:valueObjects.HistoryModuleIllHistory, p6:valueObjects.HistoryModuleDoctor, p7:valueObjects.HistoryModuleClinicRegistrationData) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registerTypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors(nowdate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(nowdate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getToothExam' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getToothExam(examID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getToothExam");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDateDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDateDoctors(ID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDateDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(ID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getIllHistory' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getIllHistory(examID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getIllHistory");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(examID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addIllHistory' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addIllHistory(IllVO:valueObjects.HistoryModuleIllHistory) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addIllHistory");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(IllVO) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updIllHistory' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updIllHistory(IllVO:valueObjects.HistoryModuleIllHistory) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updIllHistory");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(IllVO) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAnamnes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAnamnes() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAnamnes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addAnamnes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addAnamnes(Anm:valueObjects.HistoryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addAnamnes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Anm) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getStatus' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getStatus() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getStatus");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addStatus' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addStatus(Sts:valueObjects.HistoryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addStatus");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Sts) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDiagnos' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDiagnos() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDiagnos");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addDiagnos' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addDiagnos(Ds:valueObjects.HistoryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addDiagnos");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Ds) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPlan' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPlan() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPlan");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addPlan' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addPlan(Pl:valueObjects.HistoryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addPlan");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Pl) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getConsult' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getConsult() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getConsult");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addConsult' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addConsult(Cons:valueObjects.HistoryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addConsult");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Cons) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHealth' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHealth() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHealth");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addHealth' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addHealth(Hl:valueObjects.HistoryModuleDictionaryObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addHealth");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(Hl) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClinicRegistrationData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClinicRegistrationData(patientID:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClinicRegistrationData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID) ;
        return _internal_token;
    }
     
}

}
