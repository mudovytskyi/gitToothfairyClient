/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - GoogleCalendarModule.as.
 */
package services.googlecalendarmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.GoogleAccountObject;
import valueObjects.GoogleCalendar;
import valueObjects.GoogleCalendarColorObject;
import valueObjects.GoogleCalendarEventDateTimeObject;
import valueObjects.GoogleCalendarEventObject;
import valueObjects.GoogleCalendarObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_GoogleCalendarModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_GoogleCalendarModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.GoogleCalendarColorObject._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientAuthUrl");
         operation.resultType = String;
        operations["getClientAuthUrl"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "saveClientAuthToken");
         operation.resultType = Object;
        operations["saveClientAuthToken"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientSyncCalendars");
         operation.resultType = valueObjects.GoogleAccountObject;
        operations["getClientSyncCalendars"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createClientSyncCalendar");
         operation.resultType = valueObjects.GoogleCalendarObject;
        operations["createClientSyncCalendar"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientSyncCalendar");
         operation.resultType = valueObjects.GoogleCalendarObject;
        operations["getClientSyncCalendar"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateClientCalendar");
         operation.resultType = String;
        operations["updateClientCalendar"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteClientSyncCalendar");
         operation.resultType = Object;
        operations["deleteClientSyncCalendar"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addClientCalendarEvent");
         operation.resultType = String;
        operations["addClientCalendarEvent"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addWizardClientCalendarEvent");
         operation.resultType = String;
        operations["addWizardClientCalendarEvent"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateClientCalendarEvent");
         operation.resultType = String;
        operations["updateClientCalendarEvent"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteClientCalendarEvent");
         operation.resultType = Object;
        operations["deleteClientCalendarEvent"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientCalendarColors");
         operation.resultElementType = valueObjects.GoogleCalendarColorObject;
        operations["getClientCalendarColors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientCalendarEventColors");
         operation.resultElementType = valueObjects.GoogleCalendarColorObject;
        operations["getClientCalendarEventColors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getClientCalendarTimeZones");
         operation.resultElementType = Object;
        operations["getClientCalendarTimeZones"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "GoogleCalendarModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "GoogleCalendarModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.GoogleCalendar, p2:valueObjects.GoogleAccountObject, p3:valueObjects.GoogleCalendarObject, p4:valueObjects.GoogleCalendarEventObject, p5:valueObjects.GoogleCalendarEventDateTimeObject, p6:valueObjects.GoogleCalendarColorObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientAuthUrl' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientAuthUrl(isTF:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientAuthUrl");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'saveClientAuthToken' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function saveClientAuthToken(isTF:Boolean, verificationCode:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("saveClientAuthToken");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,verificationCode) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientSyncCalendars' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientSyncCalendars(isTF:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientSyncCalendars");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createClientSyncCalendar' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createClientSyncCalendar(isTF:Boolean, calendarName:String, calendarTimeZone:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createClientSyncCalendar");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarName,calendarTimeZone) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientSyncCalendar' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientSyncCalendar(isTF:Boolean, calendarId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientSyncCalendar");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateClientCalendar' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateClientCalendar(isTF:Boolean, calendarDTO:valueObjects.GoogleCalendarObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateClientCalendar");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarDTO) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteClientSyncCalendar' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteClientSyncCalendar(isTF:Boolean, calendarId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteClientSyncCalendar");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addClientCalendarEvent' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addClientCalendarEvent(isTF:Boolean, calendarId:String, newEvent:valueObjects.GoogleCalendarEventObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addClientCalendarEvent");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarId,newEvent) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addWizardClientCalendarEvent' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addWizardClientCalendarEvent(isTF:Boolean, calendarId:String, newEvent:valueObjects.GoogleCalendarEventObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addWizardClientCalendarEvent");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarId,newEvent) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateClientCalendarEvent' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateClientCalendarEvent(isTF:Boolean, calendarId:String, updEventVO:valueObjects.GoogleCalendarEventObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateClientCalendarEvent");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarId,updEventVO) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteClientCalendarEvent' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteClientCalendarEvent(isTF:Boolean, calendarId:String, eventId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteClientCalendarEvent");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF,calendarId,eventId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientCalendarColors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientCalendarColors(isTF:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientCalendarColors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientCalendarEventColors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientCalendarEventColors(isTF:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientCalendarEventColors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isTF) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getClientCalendarTimeZones' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getClientCalendarTimeZones() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getClientCalendarTimeZones");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
}

}
