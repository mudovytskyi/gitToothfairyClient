/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - HRModule.as.
 */
package services.hrmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.HRData_HRModule;
import valueObjects.HREmployee_HRModule;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_HRModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_HRModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.HRData_HRModule._initRemoteClassAlias();
        valueObjects.HREmployee_HRModule._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getHRData");
         operation.resultElementType = valueObjects.HRData_HRModule;
        operations["getHRData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "fixHRData");
         operation.resultType = String;
        operations["fixHRData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getEmployeeLazyList");
         operation.resultElementType = valueObjects.HREmployee_HRModule;
        operations["getEmployeeLazyList"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteHRDataById");
         operation.resultType = Boolean;
        operations["deleteHRDataById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setEmployeeBarcode");
         operation.resultType = Boolean;
        operations["setEmployeeBarcode"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "HRModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "HRModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.HRData_HRModule, p2:valueObjects.HREmployee_HRModule) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getHRData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getHRData(startDate:String, endDate:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getHRData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'fixHRData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function fixHRData(barcode:String, empid:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("fixHRData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(barcode,empid) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getEmployeeLazyList' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getEmployeeLazyList(fio:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getEmployeeLazyList");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(fio) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteHRDataById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteHRDataById(id:String, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteHRDataById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(id,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setEmployeeBarcode' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setEmployeeBarcode(employee_id:String, employee_type:String, employee_barcode:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setEmployeeBarcode");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(employee_id,employee_type,employee_barcode) ;
        return _internal_token;
    }
     
}

}
