/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - Form043Module.as.
 */
package services.form043module
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_Form043Module extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_Form043Module()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "create043Course");
         operation.resultType = String;
        operations["create043Course"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "create043Diary");
         operation.resultType = String;
        operations["create043Diary"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createReport");
         operation.resultType = String;
        operations["createReport"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteForm");
         operation.resultType = Boolean;
        operations["deleteForm"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "Form043Module";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "Form043Module";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'create043Course' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create043Course(docType:String, id_course:String, printTestPage:Boolean, includePriceInReport:Boolean, includeMaterialsInReport:Boolean, includeDoctorSignatureInReport:Boolean, includeNoncompleteProcedures043Flag:Boolean, includeShifrProcedures043Flag:Boolean, includeCountProcedures043Flag:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create043Course");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,id_course,printTestPage,includePriceInReport,includeMaterialsInReport,includeDoctorSignatureInReport,includeNoncompleteProcedures043Flag,includeShifrProcedures043Flag,includeCountProcedures043Flag) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'create043Diary' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function create043Diary(docType:String, id_patient:int, start_date:String, end_date:String, doctor_shortname:String, printTestPage:Boolean, includePriceInReport:Boolean, includeMaterialsInReport:Boolean, includeDoctorSignatureInReport:Boolean, includeNoncompleteProcedures043Flag:Boolean, includeShifrProcedures043Flag:Boolean, includeCountProcedures043Flag:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("create043Diary");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,id_patient,start_date,end_date,doctor_shortname,printTestPage,includePriceInReport,includeMaterialsInReport,includeDoctorSignatureInReport,includeNoncompleteProcedures043Flag,includeShifrProcedures043Flag,includeCountProcedures043Flag) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createReport' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createReport(docType:String, id_patient:String, printTestPage:Boolean, includePriceInReport:Boolean, includeMaterialsInReport:Boolean, includeDoctorSignatureInReport:Boolean, includeNoncompleteProcedures043Flag:Boolean, includeShifrProcedures043Flag:Boolean, includeCountProcedures043Flag:Boolean, is043_1:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createReport");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(docType,id_patient,printTestPage,includePriceInReport,includeMaterialsInReport,includeDoctorSignatureInReport,includeNoncompleteProcedures043Flag,includeShifrProcedures043Flag,includeCountProcedures043Flag,is043_1) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteForm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteForm(form_path:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteForm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(form_path) ;
        return _internal_token;
    }
     
}

}
