/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - AccountModule.as.
 */
package services.accountmodule
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper;
import com.adobe.serializers.utility.TypeUtility;
import mx.collections.ArrayCollection;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.Operation;
import mx.rpc.remoting.RemoteObject;
import valueObjects.AccountModuleDebtorObject;
import valueObjects.AccountModuleProcedureObject;
import valueObjects.AccountModuleReceiptData;
import valueObjects.AccountModuleReceiptProcedurData;
import valueObjects.AccountModule_AccountData;
import valueObjects.AccountModule_AccountObject;
import valueObjects.AccountModule_Doctor;
import valueObjects.AccountModule_ExpensesGroup;
import valueObjects.AccountModule_FlowObject;
import valueObjects.AccountModule_Group;
import valueObjects.AccountModule_InvoiceCashObject;
import valueObjects.AccountModule_PatientInsurances;
import valueObjects.AccountModule_PatientObject;
import valueObjects.AccountModule_PrintAccountInfo;
import valueObjects.AccountModule_ProcedureObject;
import valueObjects.AccountModule_mobilephone;

import mx.collections.ItemResponder;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

[ExcludeClass]
internal class _Super_AccountModule extends com.adobe.fiber.services.wrapper.RemoteObjectServiceWrapper
{

    // Constructor
    public function _Super_AccountModule()
    {
        // initialize service control
        _serviceControl = new mx.rpc.remoting.RemoteObject();

        // initialize RemoteClass alias for all entities returned by functions of this service
        valueObjects.AccountModule_FlowObject._initRemoteClassAlias();
        valueObjects.AccountModule_ProcedureObject._initRemoteClassAlias();
        valueObjects.AccountModuleProcedureObject._initRemoteClassAlias();
        valueObjects.AccountModuleDebtorObject._initRemoteClassAlias();
        valueObjects.AccountModule_PatientObject._initRemoteClassAlias();
        valueObjects.AccountModule_Group._initRemoteClassAlias();
        valueObjects.AccountModule_Doctor._initRemoteClassAlias();
        valueObjects.AccountModule_InvoiceCashObject._initRemoteClassAlias();
        valueObjects.AccountModule_PatientInsurances._initRemoteClassAlias();
        valueObjects.AccountModule_ExpensesGroup._initRemoteClassAlias();

        var operations:Object = new Object();
        var operation:mx.rpc.remoting.Operation;

        operation = new mx.rpc.remoting.Operation(null, "registertypes");
         operation.resultType = Object;
        operations["registertypes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAccountFlow");
         operation.resultElementType = valueObjects.AccountModule_FlowObject;
        operations["getAccountFlow"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProceduresToInvoiceByPatientID");
         operation.resultElementType = valueObjects.AccountModule_ProcedureObject;
        operations["getProceduresToInvoiceByPatientID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateProcedureDiscountFlag");
         operation.resultType = int;
        operations["updateProcedureDiscountFlag"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setPatientDiscount");
         operation.resultType = Number;
        operations["setPatientDiscount"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "createInvoice");
         operation.resultType = Object;
        operations["createInvoice"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getProcedursForReceipt");
         operation.resultType = valueObjects.AccountModuleReceiptData;
        operations["getProcedursForReceipt"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteInvoiceById");
         operation.resultType = Boolean;
        operations["deleteInvoiceById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getInvoiceProceduresByInvoiceID");
         operation.resultElementType = valueObjects.AccountModuleProcedureObject;
        operations["getInvoiceProceduresByInvoiceID"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDebtors");
         operation.resultElementType = valueObjects.AccountModuleDebtorObject;
        operations["getDebtors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getLenders");
         operation.resultElementType = valueObjects.AccountModuleDebtorObject;
        operations["getLenders"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getAccountData");
         operation.resultType = valueObjects.AccountModule_AccountData;
        operations["getAccountData"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addMoneyToAccount");
         operation.resultType = Boolean;
        operations["addMoneyToAccount"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "addPatientMoney");
         operation.resultType = Number;
        operations["addPatientMoney"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientLazyList");
         operation.resultElementType = valueObjects.AccountModule_PatientObject;
        operations["getPatientLazyList"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientObjectById");
         operation.resultType = valueObjects.AccountModule_PatientObject;
        operations["getPatientObjectById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "deleteAccountObjectById");
         operation.resultType = Boolean;
        operations["deleteAccountObjectById"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "separateProcedureCount");
         operation.resultType = Boolean;
        operations["separateProcedureCount"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "uniteProcedureCount");
         operation.resultType = Boolean;
        operations["uniteProcedureCount"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getGroups");
         operation.resultElementType = valueObjects.AccountModule_Group;
        operations["getGroups"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getDoctors");
         operation.resultElementType = valueObjects.AccountModule_Doctor;
        operations["getDoctors"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "transactionAccount");
         operation.resultType = Boolean;
        operations["transactionAccount"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getInvoicesCashes");
         operation.resultElementType = valueObjects.AccountModule_InvoiceCashObject;
        operations["getInvoicesCashes"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getPatientsInsurances");
         operation.resultElementType = valueObjects.AccountModule_PatientInsurances;
        operations["getPatientsInsurances"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "changeAccountFlowCashType");
         operation.resultType = Boolean;
        operations["changeAccountFlowCashType"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateAccountObject");
         operation.resultType = Boolean;
        operations["updateAccountObject"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getExpensesGroups");
         operation.resultElementType = valueObjects.AccountModule_ExpensesGroup;
        operations["getExpensesGroups"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateInvoiceDateNumber");
         operation.resultType = Boolean;
        operations["updateInvoiceDateNumber"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateFlowDate");
         operation.resultType = Boolean;
        operations["updateFlowDate"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "updateFlowOrderNumber");
         operation.resultType = Boolean;
        operations["updateFlowOrderNumber"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "setApplicationSetting");
         operation.resultType = Boolean;
        operations["setApplicationSetting"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "getApplicationSettings");
         operation.resultElementType = Object;
        operations["getApplicationSettings"] = operation;
        operation = new mx.rpc.remoting.Operation(null, "cancelPatientDebtAccount");
         operation.resultType = Boolean;
        operations["cancelPatientDebtAccount"] = operation;

        _serviceControl.operations = operations;
        _serviceControl.convertResultHandler = com.adobe.serializers.utility.TypeUtility.convertResultHandler;
        _serviceControl.source = "AccountModule";
        _serviceControl.endpoint = "http://localhost/toothfairy_build/gateway.php";


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
        destination = "AccountModule";
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'registertypes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function registertypes(p1:valueObjects.AccountModuleReceiptProcedurData, p2:valueObjects.AccountModuleReceiptData, p3:valueObjects.AccountModuleProcedureObject, p4:valueObjects.AccountModule_FlowObject, p5:valueObjects.AccountModule_ProcedureObject, p6:valueObjects.AccountModule_PrintAccountInfo, p7:valueObjects.AccountModuleDebtorObject, p8:valueObjects.AccountModule_AccountObject, p9:valueObjects.AccountModule_AccountData, p10:valueObjects.AccountModule_PatientObject, p11:valueObjects.AccountModule_Group, p12:valueObjects.AccountModule_InvoiceCashObject, p13:valueObjects.AccountModule_PatientInsurances, p14:valueObjects.AccountModule_Doctor, p15:valueObjects.AccountModule_mobilephone, p16:valueObjects.AccountModule_ExpensesGroup) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("registertypes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAccountFlow' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAccountFlow(date:String, patientId:String, isEnabledInCash:Boolean, flow_isCashpayment:int, accountId:String, onlyCurrentSubdivision:int, subdivision:String, userIDOnly:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAccountFlow");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(date,patientId,isEnabledInCash,flow_isCashpayment,accountId,onlyCurrentSubdivision,subdivision,userIDOnly) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProceduresToInvoiceByPatientID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProceduresToInvoiceByPatientID(patientID:String, insuranceID:String, isCoursename:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProceduresToInvoiceByPatientID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID,insuranceID,isCoursename) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateProcedureDiscountFlag' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateProcedureDiscountFlag(procedureID:String, discount_flag:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateProcedureDiscountFlag");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(procedureID,discount_flag) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setPatientDiscount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setPatientDiscount(patientID:String, discount:Number, indDiscount:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setPatientDiscount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID,discount,indDiscount) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'createInvoice' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function createInvoice(invoiceNumber:String, invoiceDate:String, patient_id:String, discount_amount:Number, procedure_ids:ArrayCollection, paidincash:Number, is_cashpayment:int, doctor_id:String, operationnote:String, payerId:String, cardId:String, certificateId:String, invoiceSumm:Number, is_fiscal:int, cashDiscount:Number) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("createInvoice");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(invoiceNumber,invoiceDate,patient_id,discount_amount,procedure_ids,paidincash,is_cashpayment,doctor_id,operationnote,payerId,cardId,certificateId,invoiceSumm,is_fiscal,cashDiscount) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getProcedursForReceipt' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getProcedursForReceipt(invoiceID:String, isCoursename:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getProcedursForReceipt");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(invoiceID,isCoursename) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteInvoiceById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteInvoiceById(invoiceId:String, passForDelete:String, accountflowsids:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteInvoiceById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(invoiceId,passForDelete,accountflowsids) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getInvoiceProceduresByInvoiceID' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getInvoiceProceduresByInvoiceID(invoiceID:String, isCoursename:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getInvoiceProceduresByInvoiceID");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(invoiceID,isCoursename) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDebtors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDebtors(patientId:String, flow_isCashpayment:int, disregardDebtValue:Number, disregardMaxDebtValue:Number, errorsCheck:int, subdivId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDebtors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId,flow_isCashpayment,disregardDebtValue,disregardMaxDebtValue,errorsCheck,subdivId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getLenders' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getLenders(patientId:String, flow_isCashpayment:int, disregardDebtValue:Number, disregardMaxDebtValue:Number, errorsCheck:int, subdivId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getLenders");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId,flow_isCashpayment,disregardDebtValue,disregardMaxDebtValue,errorsCheck,subdivId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getAccountData' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getAccountData(startDate:String, endDate:String, patientId:String, flow_isCashpayment:int, groupRules:int, isDirector:Boolean, filterGroupId:String, accountId:String, onlyCurrentSubdivision:int, subdivision:String, userIDOnly:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getAccountData");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(startDate,endDate,patientId,flow_isCashpayment,groupRules,isDirector,filterGroupId,accountId,onlyCurrentSubdivision,subdivision,userIDOnly) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addMoneyToAccount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addMoneyToAccount(summ:Number, operationnote:String, timestamp:String, is_cashpayment:int, group_id:String, cardId:String, certificateId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addMoneyToAccount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(summ,operationnote,timestamp,is_cashpayment,group_id,cardId,certificateId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'addPatientMoney' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function addPatientMoney(patientID:String, summ:Number, operationnote:String, timestamp:String, invoiceID:String, is_cashpayment:int, cardId:String, certificateId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("addPatientMoney");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientID,summ,operationnote,timestamp,invoiceID,is_cashpayment,cardId,certificateId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientLazyList' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientLazyList(fio:String, includePhoneSearch:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientLazyList");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(fio,includePhoneSearch) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientObjectById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientObjectById(patientId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientObjectById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'deleteAccountObjectById' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function deleteAccountObjectById(accountObjectId:String, passForDelete:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("deleteAccountObjectById");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(accountObjectId,passForDelete) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'separateProcedureCount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function separateProcedureCount(procId:String, separatedCount:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("separateProcedureCount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(procId,separatedCount) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'uniteProcedureCount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function uniteProcedureCount(first_Id:String, second_Id:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("uniteProcedureCount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(first_Id,second_Id) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getGroups' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getGroups(groupRules:int, groupTypes:int, isCashpayment:int, isSalary:Boolean, isEmptyGroup:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getGroups");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(groupRules,groupTypes,isCashpayment,isSalary,isEmptyGroup) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getDoctors' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getDoctors() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getDoctors");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'transactionAccount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function transactionAccount(summ_from:String, summ_to:String, summ_residue:String, groupId_transaction:String, groupId_residue:String, operationnote:String, timestamp:String, is_cashpayment:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("transactionAccount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(summ_from,summ_to,summ_residue,groupId_transaction,groupId_residue,operationnote,timestamp,is_cashpayment) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getInvoicesCashes' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getInvoicesCashes(invoiceId:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getInvoicesCashes");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(invoiceId) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getPatientsInsurances' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getPatientsInsurances(patientId:String, isActual:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getPatientsInsurances");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientId,isActual) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'changeAccountFlowCashType' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function changeAccountFlowCashType(flowID:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("changeAccountFlowCashType");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(flowID) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateAccountObject' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateAccountObject(accountObject:valueObjects.AccountModule_AccountObject) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateAccountObject");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(accountObject) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getExpensesGroups' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getExpensesGroups(isEmptyGroup:Boolean) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getExpensesGroups");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(isEmptyGroup) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateInvoiceDateNumber' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateInvoiceDateNumber(invoice_id:String, invoice_number:String, invoice_date:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateInvoiceDateNumber");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(invoice_id,invoice_number,invoice_date) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateFlowDate' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateFlowDate(flow_id:String, flow_date:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateFlowDate");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(flow_id,flow_date) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'updateFlowOrderNumber' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function updateFlowOrderNumber(flow_id:String, flow_ordernumber:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("updateFlowOrderNumber");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(flow_id,flow_ordernumber) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'setApplicationSetting' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function setApplicationSetting(paramName:String, paramValue:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("setApplicationSetting");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramName,paramValue) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'getApplicationSettings' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function getApplicationSettings(paramNames:ArrayCollection) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("getApplicationSettings");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(paramNames) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'cancelPatientDebtAccount' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function cancelPatientDebtAccount(patientid:String, summ:Number, operationnote:String, timestamp:String, is_cashpayment:int) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("cancelPatientDebtAccount");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(patientid,summ,operationnote,timestamp,is_cashpayment) ;
        return _internal_token;
    }
     
}

}
